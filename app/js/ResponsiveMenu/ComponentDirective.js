var angular = require('angular');


module.exports = function ComponentDirective($window) {

    var open = function open($elem) {
        // close the search input if body is clicked
        function bodyFn(event) {
            console.log('body clicked', event.target);
            close($elem);
            $(this).off('click', bodyFn);
            $(this).off('touchstart', bodyFn);
        }

        $elem.addClass('toggled');

        // add events
        $('.content').on('click', bodyFn);
        $('.content').on('touchstart', bodyFn);
    };

    var close = function close($elem) {
        $elem.removeClass('toggled');
        $('html,body').scrollTop(0);
    };

    var linker = function linker(scope, elem, attrs) {

        function initMenuFn(event) {
            event.stopPropagation();

            // only do this on phones...
            var mq = $window.matchMedia("(max-width: 767px)");
            if (mq.matches) {
                if (!$menu.hasClass('toggled')) {
                    event.preventDefault();
                    open($menu);
                    scope.handleClick('menu:opened');
                }
                else if ($menu.hasClass('toggled')) {
                    event.preventDefault();
                    close($menu);
                }

                if (!$(event.target).is('a')) {
                    //return false;
                }
            }
        }

        var $menu = elem.children('ul'),
            $pull = $('#pull');

        scope.menu = $menu;

        // add events
        $pull.on('click', initMenuFn);
        $pull.on('touchstart', initMenuFn);
    };

    var controller = function ($scope, eventBusService) {
        $scope.handleClick = function (msg) {
            eventBusService.prepForBroadcast(msg);
        };

        $scope.$on('handleBroadcast', function () {
            if (eventBusService.message == 'search:opened') {
                close($scope.menu);
            }
        });
    };


    return {
        restrict  : 'A',
        link      : linker,
        controller: ['$scope', 'eventBusService', controller]
    };

};