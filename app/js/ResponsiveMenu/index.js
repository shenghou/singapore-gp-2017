var angular = require('angular');

module.exports = angular
    .module('responsiveMenuComponent', [])
    .factory('eventBusService', ['$rootScope', require('../EventBus')])
    .directive('responsiveMenu', ['$window', require('./ComponentDirective')]);