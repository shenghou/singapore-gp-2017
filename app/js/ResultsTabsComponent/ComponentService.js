var angular = require('angular');

module.exports = function ComponentService($http, $q, $angularCacheFactory) {

    var baseUrl = '/';
    var resultsCache = $angularCacheFactory('resultsCache');

    var getDrivers = function () {
        var deferred = $q.defer();
        var url = baseUrl + 'results?_t=drivers';

        $http({method: 'GET', url: url, cache: resultsCache})
            .success(deferred.resolve)
            .error(deferred.reject);

        return deferred;
    };

    var getTeams = function () {
        var deferred = $q.defer();
        var url = baseUrl + 'results?_t=teams';

        $http({method: 'GET', url: url, cache: resultsCache})
            .success(deferred.resolve)
            .error(deferred.reject);

        return deferred;
    };

    var getRace = function () {
        var deferred = $q.defer();
        var url = baseUrl + 'results?_t=race';

        $http({method: 'GET', url: url, cache: resultsCache})
            .success(deferred.resolve)
            .error(deferred.reject);

        return deferred;
    };

    return {
        getRace   : getRace,
        getDrivers: getDrivers,
        getTeams  : getTeams
    };

};

