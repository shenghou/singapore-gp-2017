var angular = require('angular');

module.exports = angular
    .module('sgpDirectives', [])
    .constant('template', require('./template.html'))
    .directive('resultsTabs', ['$compile', 'template', require('./ComponentDirective')])
    .controller('RaceResultsController', ['$scope', 'RaceResultsService', require('./ComponentController')]);