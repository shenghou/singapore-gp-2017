var angular = require('angular');

module.exports = function ComponentDirective($compile, template) {

    var linker = function (scope, elem, attrs) {
        var el = angular.element(template);
        el = $compile(el)(scope);
        angular.element(elem[0]).append(el);
    };

    return {
        restrict: 'A',
        replace : true,
        link    : linker
    };

}