module.exports = function RaceResultsController($scope, RaceResultsService) {

    $scope.currentTab = 0;
    $scope.tab1Name = 'Race';
    $scope.tab2Name = 'Driver';
    $scope.tab3Name = 'Team';

    $scope.isTab1 = function () {
        return $scope.currentTab == 1;
    };

    $scope.isTab2 = function () {
        return $scope.currentTab == 2;
    };

    $scope.isTab3 = function () {
        return $scope.currentTab == 3;
    };

    $scope.setCurrentTab = function (tabIndex) {
        $scope.currentTab = tabIndex;

        if ($scope.deferred) {
            $scope.deferred.reject(new Error('Cancelled'));
        }

        switch (tabIndex) {
            case 1:
                $scope.deferred = RaceResultsService.getRace();
                $scope.deferred.promise
                    .then(function (result) {
                        result = (result || result !== 'null') ? result : {};

                        $scope.resultsData = {
                            colgrps: ['15%', '55%', '30%'],
                            headers: ['Pos', 'Driver', 'Time'],
                            results: result
                        };
                        //$scope.title = result[0].raceCity;
                        $scope.title = "EUROPE";
                        $scope.url_link = 'http://www.formula1.com/content/fom-website/en/results.html/2016/races/958/europe/race-result.html';

                        $scope.deferred = null;
                    }, function (reason) {
                        console.error(reason);
                        $scope.deferred = null;
                    });

                break;
            case 2:
                $scope.deferred = RaceResultsService.getDrivers();
                $scope.deferred.promise
                    .then(function (result) {
                        result = (result || result !== 'null') ? result : {};

                        $scope.resultsData = {
                            colgrps: ['15%', '35%', '35%', '15%'],
                            headers: ['Pos', 'Driver', 'Team', 'Points'],
                            results: result
                        };
                        $scope.title = 'FORMULA 1™ Season 2016 - Driver';
                        $scope.url_link = 'http://www.formula1.com/content/fom-website/en/championship/results/2016-driver-standings.html';

                        $scope.deferred = null;
                    }, function (reason) {
                        console.error(reason);
                        $scope.deferred = null;
                    });

                break;
            case 3:
                $scope.deferred = RaceResultsService.getTeams();
                $scope.deferred.promise
                    .then(function (result) {
                        result = (result || result !== 'null') ? result : {};

                        $scope.resultsData = {
                            colgrps: ['15%', '70%', '15%'],
                            headers: ['Pos', 'Team', 'Points'],
                            results: result
                        };
                        $scope.title = 'FORMULA 1™ Season 2016 - Team';
                        $scope.url_link = 'http://www.formula1.com/content/fom-website/en/championship/results/2016-constructor-standings.html';

                        $scope.deferred = null;
                    }, function (reason) {
                        console.error(reason);
                        $scope.deferred = null;
                    });
                break;
        }
    };

    $scope.setCurrentTab(1);

};