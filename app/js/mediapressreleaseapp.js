function parseQuery(query) {
    var Params = new Object();
    if (!query) return Params; // return empty object
    var Pairs = query.split(/[;&]/);
    for (var i = 0; i < Pairs.length; i++) {
        var KeyVal = Pairs[i].split('=');
        if (!KeyVal || KeyVal.length != 2) continue;
        var key = decodeURI(KeyVal[0]);
        var val = decodeURI(KeyVal[1]);
        val = val.replace(/\+/g, ' ');
        Params[key] = val;
    }
    return Params;
}
var scripts = document.getElementsByTagName('script');
var myScript = scripts[ scripts.length - 1 ];
var queryString = myScript.src.replace(/^[^\?]+\??/, '');
var params = parseQuery(queryString);

// ng
var angular = require('angular');

angular.module('mediaPressReleaseApp', [
        'ngSanitize',
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .controller('pressReleaseController', ['$scope', '$window', function ($scope, $window) {

        // year
        var year = new Date().getFullYear(), // set current year
            hash = $window.location.hash,
            tmp;

        if (hash.length > 0) {
            tmp = Number(window.location.hash.substr(1));
            year = isNaN(tmp) ? year : Math.min(Math.max(tmp, 2007), year); // clamp year
        }

        // breadcrumb
        var pathComponent = $window.location.pathname.split('/'),
            segment,
            segmentUrl,
            breadcrumbs = '';

        segmentUrl = '/';

        for (var i = 0; i < pathComponent.length; i++) {
            segment = pathComponent[i];

            if (segment.length > 0) {
                segmentUrl += segment + '/';
                segment = segment.split('-').join(' ').replace(' and ', ' &amp; ');

                if (i < pathComponent.length - 1) {
                    segment = "<a href='" + segmentUrl + "'>" + segment + "</a>";
                }

                // params.title for details page...
                if (params.title && i == (pathComponent.length - 1)) {
                    breadcrumbs += params.title;
                } else {
                    breadcrumbs += segment;
                }

                if (i < (pathComponent.length - 1)) {
                    breadcrumbs += '<span>&rang;</span>';
                }
            }
        }

        $scope.breadcrumbs = breadcrumbs;

        // model
        $scope.pressRelease = {
            year: year
        };

        // for press release details
        $scope.jumpToYear = function () {
            $window.location.href = "/media/press-release#" + $scope.pressRelease.year;
        };

        $scope.isSameYear = function (yr) {
            return yr == $scope.pressRelease.year;
        }
    }]);