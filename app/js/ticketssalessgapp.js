var angular = require('angular');

angular.module('sgSalesApp', [
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .controller('sgSalesController',
                [
                    '$scope',
                    function ($scope) {

                        $scope.currentRegion = 'north';

                        $scope.isActive = function (region) {
                            return region == $scope.currentRegion;
                        };

                        $scope.setRegion = function (region) {
                            $scope.currentRegion = region;
                        }

                    }
                ]);