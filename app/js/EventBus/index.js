var angular = require('angular');

module.exports = function ($rootScope) {
    var eventBus = {};

    eventBus.message = '';

    eventBus.prepForBroadcast = function (msg) {
        this.message = msg;
        this.broadcastItem();
    };

    eventBus.broadcastItem = function () {
        $rootScope.$broadcast('handleBroadcast');
    };

    return eventBus;
};