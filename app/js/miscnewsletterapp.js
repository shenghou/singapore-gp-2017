/**
 *   _____         _____    ____      _____        __
 *  / ___/______ _/ _/ /_  / __/___  / ___/__  ___/ /__
 * / /__/ __/ _ `/ _/ __/  > _/_ _/ / /__/ _ \/ _  / -_)
 * \___/_/  \_,_/_/ \__/  |_____/   \___/\___/\_,_/\__/
 *
 *
 * @author: geraldyeo
 * Date: 19/4/14
 */

function parseQuery(query) {
    var Params = new Object();
    if (!query) return Params; // return empty object
    var Pairs = query.split(/[;&]/);
    for (var i = 0; i < Pairs.length; i++) {
        var KeyVal = Pairs[i].split('=');
        if (!KeyVal || KeyVal.length != 2) continue;
        var key = decodeURI(KeyVal[0]);
        var val = decodeURI(KeyVal[1]);
        val = val.replace(/\+/g, ' ');
        Params[key] = val;
    }
    return Params;
}
var scripts = document.getElementsByTagName('script');
var myScript = scripts[ scripts.length - 1 ];
var queryString = myScript.src.replace(/^[^\?]+\??/, '');
var params = parseQuery(queryString);

// ng
var angular = require('angular');

angular.module('newsletterApp', [
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .controller('newsletterController', ['$scope', '$http', function ($scope, $http) {

        $scope.submitted = false;
        $scope.yearsRunning = [2014, 2013, 2012, 2011, 2010, 2009, 2008];
        $scope.newsletter = {
            txtName              : '',
            txtEmail             : '',
            sYear_of_Birth       : '',
            sCountry             : '',
            attend_which_year    : [],
            with_who             : [],
            with_who_other       : '',
            looking_forward      : [],
            looking_forward_other: ''
        };

        $scope.toggleSelection = function (list, value) {
            var idx = $scope.newsletter[list].indexOf(value);

            if (idx > -1) {
                // is currently selected
                $scope.newsletter[list].splice(idx, 1);
            }
            else {
                // is newly selected
                $scope.newsletter[list].push(value);
            }
        }

        $scope.submitForm = function () {
            if ($scope.newsletterForm.$valid) {
                $http({
                          method : 'POST',
                          url    : params.postUrl,
                          data   : $.param($scope.newsletter),
                          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
                      })
                    .success(function (data) {
                                 console.log(data);
                                 $scope.submitted = data.success;
                             });
            }
        };

    }]);