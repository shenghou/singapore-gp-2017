var angular = require('angular');

module.exports = angular
    .module('searchBarComponent', [])
    .factory('eventBusService', ['$rootScope', require('../EventBus')])
    .directive('searchBar', [require('./ComponentDirective')]);