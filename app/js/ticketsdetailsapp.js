var angular = require('angular');

angular.module('ticketsDetailsApp', [
        require('./PanoramaViewer').name,
        require('./PhotoGallery').name,
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .factory('TicketStatusService', ['$q', require('./services/TicketStatusService')])
    .directive('confirmBuy', [function () {
        var linker = function (scope, element, attrs) {
            element.fancybox({
                                 autoSize: false,
                                 width   : 600,
                                 height  : 400
                             });
        }

        return {
            restrict: 'A',
            link    : linker
        };
    }])
    .directive('ticketsStatuses', ['$window', 'TicketStatusService', function ($window, TicketStatusService) {
        var liveHostname = /singaporegp\.sg/;
        var isLive = liveHostname.test($window.location.hostname);

        /**
         1 Buy Now
         2 Limited Availability
         3 Selling Fast
         4 Sold Out
         **/
        var linker = function (scope, element, attrs) {
            var paths = $window.location.pathname.split('/'),
                catId, tixId;

            var doNothing = function (event) {
                event.preventDefault();
                event.stopPropagation();
            };

            var success = function (results) {
                console.log(results);

                if (results.length > 0) {

                    // Looop whole result since it's easy to catch one in between 7 or 8 or 9
                    // It doesn't affect to others as they might have only one result
                    for(var i=0; i<results.length; i++){

                        var ticket = results[i],
                            status = isLive ? ticket.get('live_status') : ticket.get('status'),
                            tid = ticket.get('ticket_id'),
                            cid = ticket.get('category_id'),
                            toption = ticket.get('ticket_option') === undefined ? 0 : ticket.get('ticket_option'),

                            tix = '#tix-' + cid + '-' + tid + '-' + toption,
                            tix2 = '#tix2-' + cid + '-' + tid + '-' + toption;

                            // button states
    //                    scope.buyButtons['tix_' + cid + '_' + tid] = {
                            scope.buyButtons['tix_' + cid + '_' + tid + '_' + toption] = {
                                status: status
                            };
                        // pricing table
    //                    scope.buyButtons['tix2_' + cid + '_' + tid] = {
                        scope.buyButtons['tix2_' + cid + '_' + tid + '_' + toption] = {

                            status: status
                        };

                        console.log('tix_' + cid + '_' + tid + '_' + toption);


                        if (status === 4) {
                            // disable link
                            element.find(tix).off('click').on('click', doNothing);
                            $(tix2).off('click').on('click', doNothing);
                        }
                    }

                }
            };

            var failure = function (reason) {
                console.error(reason);
            };

            scope.buyButtons = {};

            if (paths.length == 5 || paths.length == 6) {
                catId = paths[3];
                tixId = paths[4];
            }

            var d = TicketStatusService.getTicket(catId, tixId);
            d.promise.then(success, failure);
        };

        return {
            restrict: 'A',
            link    : linker
        };
    }])
    .directive('zoneOverlay', [function () {
        var linker = function (scope, element, attrs) {
            element.fancybox({
                                 fitToView: false,
                                 autoSize : false,
                                 width    : 800,
                                 height   : 500
                             });
        }

        return {
            restrict: 'A',
            link    : linker
        };
    }])

    .directive('fancyGallery', [function () {
        var linker = function (scope, element, attrs) {
            element.find('.fancybox').fancybox();
        }

        return {
            restrict: 'A',
            link    : linker
        };
    }])
    .controller('categorySelectController',
                [
                    '$scope',
                    '$window',
                    function ($scope, $window) {
                        var pathComponents = $window.location.pathname.split('/');

                        $scope.ticket = {
                            categoryId: pathComponents.length == 6 ? pathComponents[pathComponents.length - 3] : pathComponents[pathComponents.length - 2]
                        };

                        $scope.setTicketCategoryId = function () {
                            $window.location.href = '/tickets/listing/' + $scope.ticket.categoryId;
                        };
                    }
                ])
    .controller('ticketDetailContentController',['$scope','$window','$location',function($scope,$window,$location){
        var $ = angular.element;
        $('.iframes').each(function(){

            // find zone.php?id=9
            var re = /(zone|zone_cn)\.php\?id=[0-9]/;
            var str = $(this).attr('href');
            var m;

            // if found
            if(re.test(str)){

                // get zone id from url
                m = re.exec(str);
                var rce = /[0-9]{1,2}/;
                var result = rce.exec(m[0]);

                // replace existing old one with new one
                var newUrl = $location.protocol()+'://'+$location.host()+'/tickets/zones/'+result[0];

                // inject fancybox
                $(this).addClass('fancyboxzones fancybox.iframe').attr('data-zone-overlay','').attr('href',newUrl).fancybox();
            }
        });
    }]);