var angular = require('angular');

angular.module('ticketsHotelsTravelApp', [
    require('./ResponsiveMenu').name,
    require('./SearchBar').name
]);