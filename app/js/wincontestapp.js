function parseQuery(query) {
    var Params = new Object();
    if (!query) return Params; // return empty object
    var Pairs = query.split(/[;&]/);
    for (var i = 0; i < Pairs.length; i++) {
        var KeyVal = Pairs[i].split('=');
        if (!KeyVal || KeyVal.length != 2) continue;
        var key = unescape(KeyVal[0]);
        var val = unescape(KeyVal[1]);
        val = val.replace(/\+/g, ' ');
        Params[key] = val;
    }
    return Params;
}
var scripts = document.getElementsByTagName('script');
var myScript = scripts[ scripts.length - 1 ];
var queryString = myScript.src.replace(/^[^\?]+\??/, '');
var params = parseQuery(queryString);

// ng
var angular = require('angular');

angular.module('winContestApp', [
        'ngSanitize',
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .controller('contestController',
                [
                    '$scope',
                    '$window',
                    '$http',
                    function ($scope, $window, $http) {

                        $scope.submitted = false;
                        $scope.contest = {
                            from_where: 'website'
                        };

                        $scope.submitForm = function () {
                            if ($scope.contestForm.$valid) {
                                $http({
                                          method : 'POST',
                                          url    : params.postUrl,
                                          data   : $.param($scope.contest),
                                          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
                                      })
                                    .success(function (data) {
                                                 console.log(data);
                                                    if((data !== undefined) && (data.success))
                                                        $scope.submitted = data.success;
                                                    else
                                                    {
                                                        alert(data.errors);
                                                    }

                                             });
                            }
                        };

                        // breadcrumbs
                        var pathComponent = $window.location.pathname.split('/'),
                            segment,
                            segmentUrl,
                            breadcrumbs = '';

                        segmentUrl = '/';

                        for (var i = 0; i < pathComponent.length; i++) {
                            segment = pathComponent[i];

                            if (segment.length > 0) {
                                segmentUrl += segment + '/';
                                segment = decodeURI(segment.split('-').join(' ').replace(' and ', ' &amp; '));

                                if (i < pathComponent.length - 1) {
                                    segment = "<a href='" + segmentUrl + "'>" + segment + "</a>";
                                }

                                breadcrumbs += segment;

                                if (i < (pathComponent.length - 1)) {
                                    breadcrumbs += '<span>&rang;</span>';
                                }
                            }
                        }

                        $scope.breadcrumbs = breadcrumbs;

                        $scope.contest.option = null; // red

                        $scope.artistes = [
                                'Maroon 5',"Pharrell Williams","Spandau Ballet"
                            ];
                        $scope.artistesGetUpClose = [
                                "Bon Jovi","Jimmy Cliff","Dirty Loops"
                            ];
                        $scope.contest.description = null; 


                    }
                ]);