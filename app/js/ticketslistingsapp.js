var angular = require('angular');

angular.module('ticketsListingsApp', [
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .factory('TicketStatusService', ['$q', require('./services/TicketStatusService')])
    .directive('ticketsStatuses', ['$window', 'TicketStatusService', function ($window, TicketStatusService) {
        var liveHostname = /singaporegp\.sg/;
        var isLive = liveHostname.test($window.location.hostname);

        /**
         1 Buy Now
         2 Limited Availability
         3 Selling Fast
         4 Sold Out
         **/
        var linker = function (scope, element, attrs) {
            var success = function (results) {
                for (var i = 0; i < results.length; i++) {
                    var ticket = results[i],
                        status = isLive ? ticket.get('live_status') : ticket.get('status'),
                        tid = ticket.get('ticket_id'),
                        cid = ticket.get('category_id'),
                        toption = ticket.get('ticket_option') === undefined ? 0 : ticket.get('ticket_option'),
                        tix = '#tix-' + cid + '-' + tid + '-' + toption;

                    // button states

                    /**
                     * _0 is appended at the end to cater for Zone4 Single Day since its different from others
                     * If zone4 single day (fri, sat, sun), path will be like this => tix_21_9_7, tix_21_9_8 and tix_21_9_9
                     * For the rest of the tickets, tix_99_9_0
                     * So `0` was used as a differentiator
                     */
                    scope.buyButtons['tix_' + cid + '_' + tid + '_' + toption] = {
                        status: status
                    };

                    console.log(scope.buyButtons);

                    if (status === 4) {
                        // disable link
                        element.find(tix)
                            .off('click')
                            .on('click', function (event) {
                                    event.preventDefault();
                                });
                    }
                }
            };

            var failure = function (reason) {
                console.error(reason);
            };

            scope.buyButtons = {};

            var d = TicketStatusService.getTickets();
            d.promise.then(success, failure);
        };

        return {
            restrict: 'A',
            link    : linker
        };
    }])
    .directive('zoneOverlay', [function () {
        var linker = function (scope, element, attrs) {
            element.fancybox({
                                 fitToView: false,
                                 autoSize : false,
                                 width    : 800,
                                 height   : 500
                             });
        }

        return {
            restrict: 'A',
            link    : linker
        };
    }])
    .controller('categorySelectController',
                [
                    '$scope',
                    '$window',
                    function ($scope, $window) {
                        var pathComponents = $window.location.pathname.split('/');

                        $scope.ticket = {
                            categoryId: pathComponents.length == 4 ? pathComponents[pathComponents.length - 1] : 'all'
                        };

                        $scope.setTicketCategoryId = function () {
                            $window.location.href = '/tickets/listing/' + $scope.ticket.categoryId;
                        };
                    }
                ]);