/**
 *   _____         _____    ____      _____        __
 *  / ___/______ _/ _/ /_  / __/___  / ___/__  ___/ /__
 * / /__/ __/ _ `/ _/ __/  > _/_ _/ / /__/ _ \/ _  / -_)
 * \___/_/  \_,_/_/ \__/  |_____/   \___/\___/\_,_/\__/
 *
 *
 * @author: geraldyeo
 * Date: 20/4/14
 */

function parseQuery(query) {
    var Params = new Object();
    if (!query) return Params; // return empty object
    var Pairs = query.split(/[;&]/);
    for (var i = 0; i < Pairs.length; i++) {
        var KeyVal = Pairs[i].split('=');
        if (!KeyVal || KeyVal.length != 2) continue;
        var key = decodeURI(KeyVal[0]);
        var val = decodeURI(KeyVal[1]);
        val = val.replace(/\+/g, ' ');
        Params[key] = val;
    }
    return Params;
}
var scripts = document.getElementsByTagName('script');
var myScript = scripts[ scripts.length - 1 ];
var queryString = myScript.src.replace(/^[^\?]+\??/, '');
var params = parseQuery(queryString);

// ng
var angular = require('angular');

angular.module('galleryApp', [
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .directive('fancyGallery', [function () {
        var linker = function (scope, element, attrs) {
            element.find('.fancybox').fancybox({
                                                   margin : [20, 60, 20, 60]
                                               });
        }

        return {
            restrict: 'A',
            link    : linker
        };
    }])
    .controller('galleryFiltersController',
                [
                    '$scope',
                    '$window',
                    function ($scope, $window) {

                        var pathComponents = $window.location.pathname.split('/'),
                            qsYear, qsType, qsCats;

                        if (pathComponents.length > 3 && pathComponents.length < 7) {
                            qsYear = pathComponents[3];
                            qsType = pathComponents[4];
                            qsCats = pathComponents[5];
                        }

                        $scope.gallery = {
                            selectedYear    : qsYear || 2013, //new Date().getFullYear()
                            selectedType    : qsType || 'photos',
                            selectedCategory: qsCats || $('select[name="gallery-cat"] option:first').val()
                        };

                        $scope.filterGallery = function () {
                            $window.location.href = params.route + '/' + $scope.gallery.selectedYear + '/' + $scope.gallery.selectedType + '/' + $scope.gallery.selectedCategory;
                        };
                    }
                ]);