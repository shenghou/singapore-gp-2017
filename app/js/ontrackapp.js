var angular = require('angular');

angular.module('onTrackApp', [
    require('./ResponsiveMenu').name,
    require('./SearchBar').name
]);