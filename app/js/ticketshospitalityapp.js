function parseQuery(query) {
    var Params = new Object();
    if (!query) return Params; // return empty object
    var Pairs = query.split(/[;&]/);
    for (var i = 0; i < Pairs.length; i++) {
        var KeyVal = Pairs[i].split('=');
        if (!KeyVal || KeyVal.length != 2) continue;
        var key = decodeURI(KeyVal[0]);
        var val = decodeURI(KeyVal[1]);
        val = val.replace(/\+/g, ' ');
        Params[key] = val;
    }
    return Params;
}
var scripts = document.getElementsByTagName('script');
var myScript = scripts[ scripts.length - 1 ];
var queryString = myScript.src.replace(/^[^\?]+\??/, '');
var params = parseQuery(queryString);

// ng
var angular = require('angular');

angular.module('ticketsHospitalityApp', [
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .controller('hospitalityController',
                [
                    '$scope',
                    '$http',
                    function ($scope, $http) {

                        $scope.submitted = false;
                        $scope.contactData = {};

                        $scope.submitForm = function () {
                            if ($scope.contactForm.$valid) {
                                // honeytrap caught a bot!
                                if ($scope.contactData.machine) {
                                    return;
                                }

                                $http({
                                          method : 'POST',
                                          url    : params.postUrl,
                                          data   : $.param($scope.contactData),
                                          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
                                      })
                                    .success(function (data) {
                                                 console.log(data);
                                                 $scope.submitted = data.success;
                                             });
                            }
                        };

                    }
                ]);