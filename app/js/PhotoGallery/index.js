var angular = require('angular');

module.exports = angular
    .module('photoGalleryComponent', [])
    .directive('photoGallery', require('./ComponentDirective'));