var angular = require('angular');
var TweenMax = require('TweenMax');
var FancyBox = require('FancyBox');

module.exports = function ComponentDirective() {
    var current = 0;

    var limit = function limit(value, min, max) {
        return Math.min(Math.max(min, value), max);
    };

    var move = function move($el, containerWidth) {
        var xMin = containerWidth - $el.width(),
            xMax = 0,
            xTarget = limit(current * -containerWidth, xMin, xMax);

        TweenMax.to($el[0], .5, {marginLeft: xTarget, ease: Quint.easeOut});
    };

    var resize = function resize($ul, $li, elemWidth) {
        $li.width(elemWidth);
        $ul.width($li.length * elemWidth);
        move($ul, elemWidth);
    }

    var linker = function linker(scope, elem, attrs) {
        var $ul = elem.find('ul'),
            $li = $ul.find('li'),
            $next = elem.find('a.next'),
            $prev = elem.find('a.prev');

        // fancybox
        $ul.find('.fancybox').fancybox();

        // onresize
        $(window).resize(function () {
            resize($ul, $li, elem.width());
        });
        $(window).resize();

        // pagination
        $next.click(function () {
            if (current < $li.length - 1) {
                current += 1;
            }
            else {
                current = 0;
            }
            move($ul, elem.width());
        });

        $prev.click(function () {
            if (current > 0) {
                current -= 1;
            }
            else {
                current = $li.length - 1;
            }
            move($ul, elem.width());
        });
    };

    return {
        restrict: 'A',
        link    : linker
    };

}

function clickonie4(){
    if (event.button==2){
        return false;
    }
}
function clickon(e){
    if (document.layers||document.getElementById&&!document.all){
        if (e.which==2||e.which==3){
            return false;
        }
    }
}
if (document.layers){
    document.captureEvents(Event.MOUSEDOWN);
    document.onmousedown=clickon;
}
else if (document.all&&!document.getElementById){
    document.onmousedown=clickonie4;
}
document.oncontextmenu=new Function("return false")

