/**
 *   _____         _____    ____      _____        __
 *  / ___/______ _/ _/ /_  / __/___  / ___/__  ___/ /__
 * / /__/ __/ _ `/ _/ __/  > _/_ _/ / /__/ _ \/ _  / -_)
 * \___/_/  \_,_/_/ \__/  |_____/   \___/\___/\_,_/\__/
 *
 *
 * @author: geraldyeo
 * Date: 17/4/14
 */

function parseQuery(query) {
    var Params = new Object();
    if (!query) return Params; // return empty object
    var Pairs = query.split(/[;&]/);
    for (var i = 0; i < Pairs.length; i++) {
        var KeyVal = Pairs[i].split('=');
        if (!KeyVal || KeyVal.length != 2) continue;
        var key = decodeURI(KeyVal[0]);
        var val = decodeURI(KeyVal[1]);
        val = val.replace(/\+/g, ' ');
        Params[key] = val;
    }
    return Params;
}
var scripts = document.getElementsByTagName('script');
var myScript = scripts[ scripts.length - 1 ];
var queryString = myScript.src.replace(/^[^\?]+\??/, '');
var params = parseQuery(queryString);

// ng
var angular = require('angular');

angular.module('mediaEntryFormApp', [
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .directive('datePicker', [function () {
        var linker = function (scope, element, attrs, ngModel) {

            element.datepicker({
                                  changeYear: true,
                                  dateFormat: 'yy-mm-dd',
                                  onSelect  : function (date) {
                                      scope.$apply(function () {
                                          ngModel.$setViewValue(date);
                                      });
                                  }
                              });

        };

        return {
            restrict: 'A',
            require : '?ngModel',
            link    : linker
        };
    }])
    .directive('dateTimePicker', [function () {
        var linker = function (scope, element, attrs, ngModel) {

            element.datetimepicker({
                                       dateFormat: 'yy-mm-dd',
                                       onSelect  : function (date) {
                                           scope.$apply(function () {
                                               ngModel.$setViewValue(date);
                                           });
                                       }
                                   });

        };

        return {
            restrict: 'A',
            require : '?ngModel',
            link    : linker
        };
    }])
    .controller('entryFormController', ['$scope', '$http', function ($scope, $http) {

        // model
        $scope.numberOfPersonnel = 1;
        $scope.entryData = {
            formType   : params.formType,
            persons    : [
                {
                    full_name              : '',
                    passport_number        : '',
                    passport_expiry_date   : '',
                    passport_issue_date    : '',
                    country_of_origin      : '',
                    news_organisation      : '',
                    designation            : '',
                    flight_arrival_number  : '',
                    flight_departure_number: '',
                    flight_arrival_date    : '',
                    flight_departure_date  : ''
                }
            ],
            avEquipment: {
                country_from: '',
                person      : {
                    full_name        : '',
                    passport_number  : '',
                    news_organisation: ''
                },
                equipment   : [
                    {
                        item         : '',
                        quantity     : '',
                        approx_value : '',
                        serial_number: ''
                    }
                ]
            }
        };

        $scope.submitted = false;

        $scope.updatePersons = function () {
            var diff = $scope.entryData.persons.length - $scope.numberOfPersonnel,
                len = Math.abs(diff);

            if (diff < 0) {
                for (var i = 0; i < len; i++) {
                    $scope.entryData.persons.push({
                                                      full_name              : '',
                                                      passport_number        : '',
                                                      passport_expiry_date   : '',
                                                      passport_issue_date    : '',
                                                      country_of_origin      : '',
                                                      news_organisation      : '',
                                                      designation            : '',
                                                      flight_arrival_number  : '',
                                                      flight_departure_number: '',
                                                      flight_arrival_date    : '',
                                                      flight_departure_date  : ''
                                                  });
                }
            }
            else {
                for (var i = 0; i < len; i++) {
                    $scope.entryData.persons.pop();
                }
            }
        };

        $scope.addEquipment = function ($event) {
            $scope.entryData.avEquipment.equipment.push({
                                                            item         : '',
                                                            quantity     : '',
                                                            approx_value : '',
                                                            serial_number: ''
                                                        });
            $event.preventDefault();
        };

        $scope.removeEquipment = function ($event, $index) {
            $scope.entryData.avEquipment.equipment.splice($index, 1);
            $event.preventDefault();
        };

        $scope.submitForm = function () {
            if ($scope.entryForm.$valid) {
                $http({
                          method : 'POST',
                          url    : params.postUrl,
                          data   : $.param($scope.entryData),
                          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
                      })
                    .success(function (data) {
                                 console.log(data);
                                 $scope.submitted = data.success;
                             });
            }
        };

    }]);