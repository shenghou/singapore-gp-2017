/**
 *   _____         _____    ____      _____        __
 *  / ___/______ _/ _/ /_  / __/___  / ___/__  ___/ /__
 * / /__/ __/ _ `/ _/ __/  > _/_ _/ / /__/ _ \/ _  / -_)
 * \___/_/  \_,_/_/ \__/  |_____/   \___/\___/\_,_/\__/
 *
 *
 * @author: geraldyeo
 * Date: 28/4/14
 */

var angular = require('angular');

angular.module('ticketsHomeApp', [
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .directive('masonLink', ['$window', function ($window) {
        var linker = function (scope, element, attrs) {
            element.on('click', function (event) {
                $window.location.href = attrs['link'];
            });
        }

        return {
            restrict: 'A',
            link    : linker
        };
    }]);