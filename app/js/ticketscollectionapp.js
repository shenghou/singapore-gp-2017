/**
 *   _____         _____    ____      _____        __
 *  / ___/______ _/ _/ /_  / __/___  / ___/__  ___/ /__
 * / /__/ __/ _ `/ _/ __/  > _/_ _/ / /__/ _ \/ _  / -_)
 * \___/_/  \_,_/_/ \__/  |_____/   \___/\___/\_,_/\__/
 *
 *
 * @author: geraldyeo
 * Date: 21/4/14
 */

var angular = require('angular');

angular.module('ticketsCollectionApp', [
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .directive('googleMap', [function () {

        // Define your locations: HTML content for the info window, latitude, longitude
        // To add additional addresses in array. Just copy and paste entire current h4 tags, get coordinates from http://itouchmap.com/latlong.html and edit new address.
        var locations = [
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">ANG MO KIO CENTRAL</span> <br> <span style="color:#000000;">Blk 727 Ang Mo Kio Ave 6 <br> #01-4246 <br> Singapore 560727 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=Blk+727+Ang+Mo+Kio+Ave+6&ie=UTF-8&hq=&hnear=0x31da16e7e69624e1:0x8b55948b091a6d0d,727+Ang+Mo+Kio+Avenue+6,+560727&gl=sg&ei=VynnUvT4NMP3rQfv5IDoAw&ved=0CCcQ8gEwAA" target="_blank">View Map</a> </span></h4>', 1.373029, 103.845959],
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">BRAS BASAH</span> <br> <span style="color:#000000;">Blk 231 Bain Street <br> #01-03 Bras Basah Complex <br> Singapore 180231 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=Blk+231+Bain+Street+%2301-03+Bras+Basah+Complex+Singapore+180231&ie=UTF-8&ei=4CvnUomDHsSXrgfPj4HQBw&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.297587, 103.853392],
            // ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">CHANGE ALLEY</span> <br> <span style="color:#000000;">16 Collyer Quay <br> #02-02 <br> Singapore 049318 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=16+Collyer+Quay+%2302-02++Singapore+049318&ie=UTF-8&ei=8jDnUpHvFomCrAf4t4GoBw&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.284220, 103.852451],
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">CLEMENTI CENTRAL</span> <br> <span style="color:#000000;">3155 Commonwealth Ave West <br> #05-23/24/25/26 <br> The Clementi Mall <br> Singapore 129588 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=3155+Commonwealth+Ave+West+%2305-23/24/25/26++The+Clementi+Mall++Singapore+129588&ie=UTF-8&ei=XzHnUsjYCYH-rAeO6IDgDA&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.314918, 103.764309],
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">HARBOURFRONT CENTRE</span> <br> <span style="color:#000000;">1 Maritime Square <br> #03-22 HarbourFront Centre <br> Singapore 099253 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=1+Maritime+Square++%2303-22+HarbourFront+Centre+Singapore+099253&ie=UTF-8&ei=ejHnUraFE8qCrgfQ94H4BQ&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.264119, 103.820007],
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">JURONG POINT</span> <br> <span style="color:#000000;">1 Jurong West Central 2 <br> B1-45 Jurong Point (JP1) <br> Singapore 648886 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=1+Jurong+West+Central+2+B1-45+Singapore+648886&ie=UTF-8&ei=rDHnUs3KJIuYrger4IGQDA&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.339341, 103.705387],
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">KILLINEY ROAD</span> <br> <span style="color:#000000;">1 Killiney Road <br> Singapore 239518</span> <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=1+Killiney+Road+Singapore+239518&ie=UTF-8&ei=wTHnUpyfJYH8rAf2-ICICg&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </h4>', 1.300239, 103.840721],
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">ORCHARD</span> <br> <span style="color:#000000;">2 Orchard Turn <br> #B2 -62 ION Orchard <br> Singapore 238801 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=2+Orchard+Turn+%23B2+-62+ION+Orchard+Singapore+238801&ie=UTF-8&ei=5jHnUuqsJcTqrAeY94H4CA&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.304169, 103.831722],
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">RAFFLES PLACE</span> <br> <span style="color:#000000;">10 Collyer Quay <br> #B1-11 Ocean Financial Centre <br> Singapore 049315 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=10+Collyer+Quay+%23B1-11+Singapore+049315&ie=UTF-8&ei=Ilj0UqXtF4ewiAfM_IH4DQ&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.282964, 103.852073],
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">ROBINSON ROAD</span> <br> <span style="color:#000000;">79 Robinson Road <br> #01-06 CPF Building <br> Singapore 068897 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=79+Robinson+Road+%2301-06+CPF+Building+Singapore+068897&ie=UTF-8&ei=BTLnUoLJNoXzrQe2u4HYDw&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.277230, 103.847986],
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">SIGLAP</span> <br> <span style="color:#000000;">10 Palm Avenue <br> Singapore 456532 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=10+Palm+Avenue+Singapore+456532&ie=UTF-8&ei=HjLnUt6jF4eOrQfZvICYCg&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.313425, 103.930083],
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">PAYA LEBAR POST OFFICE</span> <br> <span style="color:#000000;">11 Tanjong Katong Road<br/>#03-08/09 One KM Mall<br/>Singapore 437157<br> <a style="color:tomato;" href="https://www.google.com.sg/maps/place/OneKM,+11+Tanjong+Katong+Rd,+Singapore+436950" target="_blank">View Map</a> </span></h4>',1.314957,103.894744]
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">TAMPINES CENTRAL</span> <br> <span style="color:#000000;">1 Tampines Central 5 <br> #01-03 CPF Tampines Building <br> Singapore 529508 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=1+Tampines+Central+5++%2301-03+CPF+Tampines+Building++Singapore+529508&ie=UTF-8&ei=YzPnUo3ENMjDrAfbm4DoDA&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.353112, 103.943709],
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">TANGLIN</span> <br> <span style="color:#000000;">56 Tanglin Road <br> Singapore 247964 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=56+Tanglin+Road+Singapore+247964&ie=UTF-8&ei=eTPnUoPBCNDjrAfh3YCQBA&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.305625, 103.822939],
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">WOODLANDS CENTRAL</span> <br> <span style="color:#000000;">900 South Woodlands Drive <br> #03-05/06 Woodlands Civic Centre <br> Singapore 730900 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=900+South+Woodlands+Drive+%2303-05/06+Woodlands+Civic+Centre+Singapore+730900&ie=UTF-8&ei=sTPnUpHwGI6Krgf2loCwCQ&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.435152, 103.787195],
            ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">RAFFLES CITY CONVENTION CENTRE</span> <br> <span style="color:#000000;"> Level 4<br> Swissotel the Stamford<br> 2 Stamford Rd, 178882 <br/> Bras Basah Room <br> <a style="color:tomato;" href="https://www.google.com/maps/place/Swissotel+The+Stamford+Singapore/@1.2935544,103.8534273,17z/data=!3m2!4b1!5s0x31da19a428a16ab3:0xb22ccd0fb009c220!4m2!3m1!1s0x31da19a68764237f:0xf32fa2fffc1d7b8c&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.2935544, 103.8535598]
        ];

        // Setup the different icons and shadows
        var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';

        // Red > red-dot.png - North
        // Green > green -dot.png - South
        // Blue > blue-dot.png - East
        // Orange > orange-dot.png - West
        // Purple > purple-dot.png - Central

        // Note: The array is arranged by counter logic "0 + 1 + ect & reset when reached counter length. So therefore the google markers are arranged sequentially.
        // Add relevant future ticket collection outlet locations accordingly on their geographical location on the Singapore map.

        var icons = [
            iconURLPrefix + 'red-dot.png',  // AMK
            iconURLPrefix + 'purple-dot.png', // Bras Basah
            //iconURLPrefix + 'purple-dot.png', // Change Alley
            iconURLPrefix + 'orange-dot.png', // Clementi Central
            iconURLPrefix + 'purple-dot.png', // Harbourfront
            iconURLPrefix + 'orange-dot.png', // Jurong Point
            iconURLPrefix + 'purple-dot.png', // Killiney Road
            iconURLPrefix + 'purple-dot.png', // Orchard
            iconURLPrefix + 'purple-dot.png', // Raffles Place
            iconURLPrefix + 'purple-dot.png', // Robinson Road
            iconURLPrefix + 'blue-dot.png', // Siglap
            iconURLPrefix + 'blue-dot.png', // Singapore Post Centre
            iconURLPrefix + 'blue-dot.png', // Tampines Central
            iconURLPrefix + 'purple-dot.png', // Tanglin
            iconURLPrefix + 'red-dot.png', // Woodlands Central
            iconURLPrefix + 'purple-dot.png', // Woodlands Central
        ]
        var icons_length = icons.length;


        var shadow = {
            anchor: new google.maps.Point(15, 33),
            url   : iconURLPrefix + 'msmarker.shadow.png'
        };

        var linker = function (scope, element, attrs) {

            var map = new google.maps.Map(element[0], {
                zoom              : 1,
                mapTypeId         : google.maps.MapTypeId.ROADMAP,
                mapTypeControl    : false,
                streetViewControl : false,
                panControl        : false,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                }
            });

            // Control Marker info window's width when clicked
            var infowindow = new google.maps.InfoWindow({
                                                            maxWidth: 500
                                                        });

            var marker;
            var markers = new Array();

            var iconCounter = 0;

            // Add the markers and infowindows to the map
            for (var i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                                                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                                                    map     : map,
                                                    icon    : icons[iconCounter],
                                                    shadow  : shadow
                                                });

                markers.push(marker);

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        map.panTo(this.getPosition());
                        map.setZoom(17);
                    }
                })(marker, i));

                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));

                iconCounter++;
                // Reset icon colours.
                if (iconCounter >= icons_length) {
                    iconCounter = 0;
                }
            }

            function AutoCenter() {
                //  Create a new viewpoint bound
                var bounds = new google.maps.LatLngBounds();
                //  Go through each...
                $.each(markers, function (index, marker) {
                    bounds.extend(marker.position);
                });
                //  Fit these bounds to the map
                map.fitBounds(bounds);
            }

            AutoCenter();
        };

        return {
            restrict: 'A',
            link    : linker
        };

    }]);