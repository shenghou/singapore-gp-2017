var angular = require('angular');
var TweenMax = require('TweenMax');

module.exports = function ComponentDirective() {
    var lastX = 0;

    var limit = function limit(value, min, max) {
        return Math.min(Math.max(min, value), max);
    };

    var move = function move(dir, $img, panoWidth) {
        var wImg = $img.width(),
            xMin = panoWidth - wImg,
            xMax = 0,
            xOffset = dir * (panoWidth / 2),
            xTarget = limit(lastX + xOffset, xMin, xMax);

        lastX = xTarget;
        TweenMax.to($img[0], .5, {marginLeft: xTarget, ease: Quint.easeOut});
    };

    var resize = function resize($img, elemWidth) {
        move(1, $img, elemWidth);
    }

    var linker = function linker(scope, elem, attrs) {
        var $img = elem.find('img');
        var $next = elem.find('a.next');
        var $prev = elem.find('a.prev');

        // create new offscreen image to test
        var theImage = new Image();
        theImage.onload = function () {
            // get accurate measurements from that.
            $img.width(this.width);
            $img.height(348);
        };
        theImage.src = $img.attr("src");

        // onresize
        $(window).resize(function () {
            resize($img, elem.width());
        });
        $(window).resize();

        // pagination
        $next.click(function () {
            move(-1, $img, elem.width());
        });

        $prev.click(function () {
            move(1, $img, elem.width());
        });
    };

    return {
        restrict: 'A',
        link    : linker
    };

}