var angular = require('angular');

module.exports = angular
    .module('panoramaViewerComponent', [])
    .directive('panoramaViewer', require('./ComponentDirective'));