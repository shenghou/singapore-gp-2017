/**
 *   _____         _____    ____      _____        __
 *  / ___/______ _/ _/ /_  / __/___  / ___/__  ___/ /__
 * / /__/ __/ _ `/ _/ __/  > _/_ _/ / /__/ _ \/ _  / -_)
 * \___/_/  \_,_/_/ \__/  |_____/   \___/\___/\_,_/\__/
 *
 *
 * @author: geraldyeo
 * Date: 28/4/14
 */

var angular = require('angular');

angular.module('offtrackHeadlinersApp', [
        'ngSanitize',
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .directive('fancyGallery', [function () {
        var linker = function (scope, element, attrs) {
            element.find('.fancybox').fancybox();
        }

        return {
            restrict: 'A',
            link    : linker
        };
    }])
    .controller('headlinersController', [
        '$scope',
        '$window',
        function ($scope, $window) {
            var pathComponent = $window.location.pathname.split('/'),
                segment,
                segmentUrl,
                breadcrumbs = '';

            segmentUrl = '/';

            for (var i = 0; i < pathComponent.length; i++) {
                segment = pathComponent[i];

                if (segment.length > 0) {
                    segmentUrl += segment + '/';
                    if (i > 1) {
                        segment = segment.split('-').join(' ').replace(' and ', ' &amp; ');
                    }

                    if (i < pathComponent.length - 1) {
                        segment = "<a href='" + segmentUrl + "'>" + segment + "</a>";
                    }

                    breadcrumbs += segment;

                    if (i < (pathComponent.length - 1)) {
                        breadcrumbs += '<span>&rang;</span>';
                    }
                }
            }

            $scope.breadcrumbs = breadcrumbs;
        }
    ]);