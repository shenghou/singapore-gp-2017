function parseQuery(query) {
    var Params = new Object();
    if (!query) return Params; // return empty object
    var Pairs = query.split(/[;&]/);
    for (var i = 0; i < Pairs.length; i++) {
        var KeyVal = Pairs[i].split('=');
        if (!KeyVal || KeyVal.length != 2) continue;
        var key = unescape(KeyVal[0]);
        var val = unescape(KeyVal[1]);
        val = val.replace(/\+/g, ' ');
        Params[key] = val;
    }
    return Params;
}
var scripts = document.getElementsByTagName('script');
var myScript = scripts[ scripts.length - 1 ];
var queryString = myScript.src.replace(/^[^\?]+\??/, '');
var params = parseQuery(queryString);

// ng
var angular = require('angular');

var app = angular.module('revUpFormApp', [
        'ngSanitize',
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .directive('datePicker', [function () {
        var linker = function (scope, element, attrs, ngModel) {

            element.datepicker({
                                    yearRange: '-150:+0',
                                    changeYear: true,
                                    dateFormat: 'yy-mm-dd',
                                    onSelect  : function (date) {
                                        scope.$apply(function () {
                                            ngModel.$setViewValue(date);
                                        });
                                    }
                                });

        };

        return {
            restrict: 'A',
            require : '?ngModel',
            link    : linker
        };
    }])
    .controller('revUpFormController',
                [
                    '$scope',
                    '$window',
                    '$http',
                    function ($scope, $window, $http) {

                        $scope.submitted = false;
                        $scope.error = false;
                        $scope.submitParse = function (classString) {
                            if ($scope.revupForm.$valid) {
                                $scope.error = false;
                                var f = Parse.Object.extend(classString);
                                var obj = new f();
                                obj.save($scope.formValues, {
                                    success: function(o) {
                                        $scope.submitted = true;
                                        $scope.$apply();
                                    },
                                    error: function(o, error) {
                                        $scope.error = error.message;
                                        $scope.$apply();
                                    }
                                })
                            }
                        };
                        $scope.submitForm = function () {

                            if ($scope.revupForm.$valid) {
                                $http({
                                          method : 'POST',
                                          url    : params.postUrl,
                                          data   : $.param($scope.formValues),
                                          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
                                      })
                                    .success(function (data) {
                                                 console.log(data);
                                                 $scope.submitted = data.success;
                                             });
                            }
                        };

                    }
                ]);

app.directive('parseDynamicSelect', ['$http', function($q) {
    return {
        template: '<select ng-change="model = _model.name" ng-model="_model" ng-options="c.name for c in results track by c.name"></select>',
        scope: {
            data: '=',
            model: '='
        },
        restrict: 'E',
        link: function (scope, elem, attrs, ctrl) {
            scope.results = [];
            var options = [];
            var Obj = Parse.Object.extend(attrs.data);
            var obj = new Parse.Query(Obj);
            obj.ascending("sort");
            obj.find({
                success: function(result) {
                    for (var i = 0; i < result.length; i++) {
                        var r = result[i];
                        if (r.get('current') < r.get('close_at')) {
                            options.push({
                                name: r.get('name'),
                                value: r.get('value')
                            });
                        };
                    };
                    scope.results = options;
                    scope.$apply();
                    console.log(attrs.model)
                    console.log(scope[attrs.model])
                }
            });
        }
    }
}]);

app.directive('nricValidate', function() {
    return {
        // restrict to an attribute type.
        restrict: 'A',
        
        // element must have ng-model attribute.
        require: 'ngModel',


        // scope = the parent scope
        // elem = the element the directive is on
        // attr = a dictionary of attributes on the element
        // ctrl = the controller for ngModel.
        link: function(scope, elem, attr, ctrl) {
            scope.validateNric = function (value) {
                
                if (value == undefined || value == '') return false;
                var first = value[0].toUpperCase();
                if(first == 'S' || first == 'T') {
                    var multiples = [2, 7, 6, 5, 4, 3, 2];
                    if (value.length != 9) return false;

                    var total = 0;
                    var count = 0;
                    var numericNric = 0;

                    var last = value[value.length - 1].toUpperCase();

                    if (first !== 'S' && first !== 'T') return false;

                    var numericNric = value.substr(1, value.length - 2);

                    if (isNaN(parseInt(numericNric))) return false;

                    while (numericNric != 0) {
                        total += (numericNric % 10) * multiples[multiples.length - (1 + count++)];
                        numericNric /= 10;
                        numericNric = Math.floor(numericNric);
                    }

                    outputs = '';
                    if (first == "S") {
                        outputs = ['J', 'Z', 'I', 'H', 'G', 'F', 'E', 'D', 'C', 'B', 'A'];
                    } else {
                        outputs = ['G', 'F', 'E', 'D', 'C', 'B', 'A', 'J', 'Z', 'I', 'H'];
                    }
                    
                    return (last == outputs[total % 11]);

                } else if(first == 'F' || first == 'G') {
                    
                    var multiples = [2, 7, 6, 5, 4, 3, 2];
                    if (value.length != 9) return false;

                    total = 0;
                    count = 0;
                    numericNric = 0;
                    first = value[0].toUpperCase();
                    last = value[value.length - 1].toUpperCase();

                    if (first !== 'F' && first !== 'G') return false;

                    var numericNric = value.substr(1, value.length - 2);

                    if (isNaN(parseInt(numericNric))) return false;

                    while (numericNric != 0)
                    {
                        total += (numericNric % 10) * multiples[multiples.length - (1 + count++)];
                        numericNric /= 10;
                        numericNric = Math.floor(numericNric);
                    }

                    outputs = [];
                    if (first == "F") {
                        outputs = ['X', 'W', 'U', 'T', 'R', 'Q', 'P', 'N', 'M', 'L', 'K'];
                    } else {
                        outputs = ['R', 'Q', 'P', 'N', 'M', 'L', 'K', 'X', 'W', 'U', 'T'];
                    }

                    return (last == outputs[total % 11]);
                }
                return false;
            }
            ctrl.$parsers.unshift(function(val) {
                var valid = scope.validateNric(val);
                ctrl.$setValidity('nricValidate', valid)
                return valid ? val : undefined
            });
            ctrl.$formatters.unshift(function(val) {
                var valid = scope.validateNric(val);
                ctrl.$setValidity('nricValidate', valid)
                return val;
            });
        }
    };
});

app.directive('secretValidate', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, elem, attr, ctrl) {
            scope.validateSecret = function (value) {
                if(value)
                    return (value.toLowerCase() == 'nightrace2014');
                else
                    return false;
            }
            ctrl.$parsers.unshift(function(val) {
                var valid = scope.validateSecret(val);
                ctrl.$setValidity('secretValidate', valid)
                return valid ? val : undefined
            });
            ctrl.$formatters.unshift(function(val) {
                var valid = scope.validateSecret(val);
                ctrl.$setValidity('secretValidate', valid)
                return val;
            });
        }
    };
});
