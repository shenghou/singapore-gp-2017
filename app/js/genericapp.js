var angular = require('angular');

angular.module('genericApp', [
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ]);