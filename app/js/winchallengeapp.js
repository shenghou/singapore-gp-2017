var angular = require('angular');

angular.module('winChallengeApp', [
        'ngSanitize',
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .controller('challengeController',
                [
                    '$scope',
                    '$window',
                    function ($scope, $window) {

                        var pathComponent = $window.location.pathname.split('/'),
                            segment,
                            segmentUrl,
                            breadcrumbs = '';

                        segmentUrl = '/';

                        for (var i = 0; i < pathComponent.length; i++) {
                            segment = pathComponent[i];

                            if (segment.length > 0) {
                                segmentUrl += segment + '/';
                                segment = segment.split('-').join(' ').replace(' and ', ' &amp; ');

                                if (i < pathComponent.length - 1) {
                                    segment = "<a href='" + segmentUrl + "'>" + segment + "</a>";
                                }

                                breadcrumbs += segment;

                                if (i < (pathComponent.length - 1)) {
                                    breadcrumbs += '<span>&rang;</span>';
                                }
                            }
                        }

                        $scope.breadcrumbs = breadcrumbs;

                    }
                ]);