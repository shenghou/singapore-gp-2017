/**
 *   _____         _____    ____      _____        __
 *  / ___/______ _/ _/ /_  / __/___  / ___/__  ___/ /__
 * / /__/ __/ _ `/ _/ __/  > _/_ _/ / /__/ _ \/ _  / -_)
 * \___/_/  \_,_/_/ \__/  |_____/   \___/\___/\_,_/\__/
 *
 *
 * @author: geraldyeo
 * Date: 23/4/14
 */

function parseQuery(query) {
    var Params = new Object();
    if (!query) return Params; // return empty object
    var Pairs = query.split(/[;&]/);
    for (var i = 0; i < Pairs.length; i++) {
        var KeyVal = Pairs[i].split('=');
        if (!KeyVal || KeyVal.length != 2) continue;
        var key = decodeURI(KeyVal[0]);
        var val = decodeURI(KeyVal[1]);
        val = val.replace(/\+/g, ' ');
        Params[key] = val;
    }
    return Params;
}
var scripts = document.getElementsByTagName('script');
var myScript = scripts[ scripts.length - 1 ];
var queryString = myScript.src.replace(/^[^\?]+\??/, '');
var params = parseQuery(queryString);



var angular = require('angular');

angular.module('offtrackHeadlinersApp', [
        'ngSanitize',
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .controller('headlinersController', [
        '$scope',
        '$window',
        function ($scope, $window) {
            var pathComponent = $window.location.pathname.split('/'),
                segment,
                segmentUrl,
                breadcrumbs = '';

            segmentUrl = '/';

            for (var i = 0; i < pathComponent.length; i++) {
                segment = pathComponent[i];

                if (segment.length > 0) {
                    segmentUrl += segment + '/';
                    
                    if (i > 1) {
                        segment = segment.split('-').join(' ').replace(' and ', ' &amp; ');
                    }

                    if (i < pathComponent.length - 1) {
                        segment = "<a href='" + segmentUrl + "'>" + segment + "</a>";
                    }

                    breadcrumbs += segment;

                    if (i < (pathComponent.length - 1)) {
                        breadcrumbs += '<span>&rang;</span>';
                    }
                }
            }

            $scope.breadcrumbs = breadcrumbs;


            /**
             * Created with JetBrains PhpStorm.
             * User: weiyang
             * Date: 3/7/14
             * Time: 5:49 PM
             * To change this template use File | Settings | File Templates.
             */
            // year
            var category = 'none';

            // model
            $scope.headliner = {
                cat: category
            };

            $scope.isSameCat = function (cat) {
                return cat == $scope.headliner.cat;
            }

            $scope.getCat = function (){
                alert($scope.headliner.cat);
                return $scope.headliner.cat;
            }

            $scope.filterCategory = function(){



                if($scope.headliner.cat == 'all' || $scope.headliner.cat == 'none' )
                {
                    angular.element('.headliner').show();
                }
                else if($scope.headliner.cat.substring(0,2) == 'In')
                {
                    angular.element('.headliner').hide();

                    angular.element('.headliner[myname="IMA"]').show();
                }
                else if($scope.headliner.cat.substring(0,2) == 'Wo')
                {
                    angular.element('.headliner').hide();

                    angular.element('.headliner[myname="WRA"]').show();
                }
                else
                {
//                    alert('section is Else');
                    angular.element('.headliner').hide();
                    angular.element('.headliner[myname="'+$scope.headliner.cat+'"]').show();

                }
            }
        }


    ]);