/**
 * @author: geraldyeo
 * Date: 15/3/14
 */

require('flipclock');
var angular = require('angular');
var angularCache = require('angularCache');
var clock;

// flipclock
$(function () {
    // Grab the current date
    var currentDate = new Date();

    // Set some date in the future. In this case, it's always Jan 1
    var futureDate = new Date(2016, 8, 18, 20);

    // Calculate the difference in seconds between the future and current date
    var diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;

    // Instantiate a coutdown FlipClock
    clock = $('.clock').FlipClock(diff, {
        clockFace  : 'DailyCounter',
        countdown  : true,
        showSeconds: false
    });
});


// angular app
angular.module('homeApp', [
        'jmdobry.angular-cache',
        require('./ResultsTabsComponent').name,
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .config([
                '$angularCacheFactoryProvider',
                function ($angularCacheFactoryProvider) {
                    $angularCacheFactoryProvider.setCacheDefaults({
                                                                      maxAge            : 120,
                                                                      cacheFlushInterval: 120,
                                                                      storageMode       : 'localStorage'
                                                                  });
                }
            ])
    .factory('RaceResultsService',
             [
                 '$http',
                 '$q',
                 '$angularCacheFactory',
                 require('./ResultsTabsComponent/ComponentService')
             ]);
