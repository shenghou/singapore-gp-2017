function parseQuery(query) {
    var Params = new Object();
    if (!query) return Params; // return empty object
    var Pairs = query.split(/[;&]/);
    for (var i = 0; i < Pairs.length; i++) {
        var KeyVal = Pairs[i].split('=');
        if (!KeyVal || KeyVal.length != 2) continue;
        var key = decodeURI(KeyVal[0]);
        var val = decodeURI(KeyVal[1]);
        val = val.replace(/\+/g, ' ');
        Params[key] = val;
    }
    return Params;
}
var scripts = document.getElementsByTagName('script');
var myScript = scripts[ scripts.length - 1 ];
var queryString = myScript.src.replace(/^[^\?]+\??/, '');
var params = parseQuery(queryString);

// ng
var angular = require('angular');

angular.module('intSalesApp', [
        'ngSanitize',
        require('./ResponsiveMenu').name,
        require('./SearchBar').name
    ])
    .controller('internationalSalesController',
        [
            '$scope',
            function ($scope) {

                // region & country tracker
                if (params.langmap) {
                    $scope.langMap = JSON.parse(params.langmap);
                }

                $scope.currentRegion = '';
                $scope.currentCountry = '';
                $scope.breadcrumbs = '';

                // models for selects
                $scope.anzac = {
                    region   : 'australia | new zealand',
                    country  : 'australia', // default show on load
                    isDefault: function () {
                        return this.country == this.region;
                    }
                };

                $scope.asia = {
                    region   : 'asia',
                    country  : 'asia',
                    isDefault: function () {
                        return this.country == this.region;
                    }
                };

                $scope.africa = {
                    region   : 'africa',
                    country  : 'africa',
                    isDefault: function () {
                        return this.country == this.region;
                    }
                };

                $scope.middleeast = {
                    region   : 'middleeast',
                    country  : 'middleeast',
                    isDefault: function () {
                        return this.country == this.region;
                    }
                };

                $scope.europe = {
                    region   : 'europe',
                    country  : 'europe',
                    isDefault: function () {
                        return this.country == this.region;
                    }
                };

                $scope.northamerica = {
                    region   : 'northamerica',
                    country  : 'northamerica',
                    isDefault: function () {
                        return this.country == this.region;
                    }
                };


                // functions on scope
                $scope.isCountryActive = function (country) {
                    return country == $scope.currentCountry;
                };

                $scope.isRegionActive = function (region) {
                    return region == $scope.currentRegion;
                }

                $scope.selectCountry = function (region) {
                    if (region != 'anzac') $scope.anzac.country = $scope.anzac.region;
                    if (region != 'asia') $scope.asia.country = $scope.asia.region;
                    if (region != 'africa') $scope.africa.country = $scope.africa.region;
                    if (region != 'middleeast') $scope.middleeast.country = $scope.middleeast.region;
                    if (region != 'europe') $scope.europe.country = $scope.europe.region;
                    if (region != 'northamerica') $scope.northamerica.country = $scope.northamerica.region;

                    $scope.currentRegion = region;
                    $scope.currentCountry = $scope[region].country;

                    if ($scope.langMap) {
                        $scope.breadcrumbs = $scope.langMap[$scope[region].region] + ' <span>&rang;</span> ' + $scope.langMap[$scope.currentCountry];
                    } else {
                        var regionName = $scope[region].region;
                        if (regionName == 'middleeast') regionName = 'middle east';
                        if (regionName == 'northamerica') regionName = 'usa';
                        $scope.breadcrumbs = regionName + ' <span>&rang;</span> ' + $scope.currentCountry;
                    }

                }

                $scope.selectCountry('anzac');

            }
        ]);