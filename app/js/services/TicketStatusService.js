/**
 *   _____         _____    ____      _____        __
 *  / ___/______ _/ _/ /_  / __/___  / ___/__  ___/ /__
 * / /__/ __/ _ `/ _/ __/  > _/_ _/ / /__/ _ \/ _  / -_)
 * \___/_/  \_,_/_/ \__/  |_____/   \___/\___/\_,_/\__/
 *
 *
 * @author: geraldyeo
 * Date: 1/5/14
 */

var angular = require('angular');

module.exports = function TicketStatusService($q) {
    Parse.initialize("ZR824TlIcihwkq4hHXjZmftak8wIzqFmq4EkU2Va", "QsnntqPa1o5S0a2l4zyMvWAJw6aFAwjddMake0yk");

    var getTickets = function getTickets() {
        var deferred = $q.defer();
        var GameScore = Parse.Object.extend("Tickets");
        var query = new Parse.Query(GameScore);

        query.ascending("ticket_id");
        query.find({
                       success: deferred.resolve,
                       error  : deferred.reject
                   });

        return deferred;
    };

    var getTicket = function getTicket(categoryId, ticketId) {
        var deferred = $q.defer();
        var GameScore = Parse.Object.extend("Tickets");
        var query = new Parse.Query(GameScore);

        query.equalTo("ticket_id", parseInt(ticketId));
        query.equalTo("category_id", parseInt(categoryId));
        query.find({
                       success: deferred.resolve,
                       error  : deferred.reject
                   });

        return deferred;
    };

    return {
        getTickets: getTickets,
        getTicket : getTicket
    };
};