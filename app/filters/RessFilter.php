<?php

/**
 * Class RessFilter
 *
 * The RESS filter is responsible for detecting the client device and
 * setting session accordingly.
 */
class RessFilter
{
    private $key = '_Modernizr';


    public function filter($route)
    {
        if (Input::has('cookie') && Input::get('cookie') == '0') {
            return 'damn';
        }
        else if (isset($_COOKIE) && isset($_COOKIE[$this->key])) {
            /*
             |--------------------------------------------------------------------------
             | Don't use Laravel's Cookie::get() since it is encrypted.
             | @see: http://laravel.com/docs/requests#cookies
             |--------------------------------------------------------------------------
             */
            $modernizr = $this->parseCookie($_COOKIE[$this->key]);
            Session::put($this->key, $modernizr);
        } else if (!Session::get($this->key)) {
            // Redirect to RESS test page if no prior session or cookie found
            return Redirect::to('ress')->with('referrer_uri', $route->getPath());
        }
    }


    private function parseCookie($cookie)
    {
        $modernizr = new stdClass();
        foreach (explode('|', $cookie) as $feature) {
            list($name, $value) = explode(':', $feature, 2);
            if ($value[0] == '/') {
                $value_object = new stdClass();
                foreach (explode('/', substr($value, 1)) as $sub_feature) {
                    list($sub_name, $sub_value) = explode(':', $sub_feature, 2);
                    $value_object->$sub_name = $sub_value;
                }
                $modernizr->$name = $value_object;
            } else {
                $modernizr->$name = $value;
            }
        }
        return $modernizr;
    }
} 