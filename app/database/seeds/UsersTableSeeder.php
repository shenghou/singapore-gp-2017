<?php
class UsersTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();
        User::create(array('username' => 'james', 'password' => Hash::make('p@ssw0rd')));
    }

}