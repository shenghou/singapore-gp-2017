<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRaceGuides extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('raceguides', function($table) {
			$table->increments('id');
			$table->unsignedInteger('collection_id');
			$table->string('slug');
			$table->string('title');
			$table->string('img');
			$table->text('description');
			$table->text('left_panel');
			$table->text('right_panel');
			$table->boolean('hidden');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('raceguides');
	}

}
