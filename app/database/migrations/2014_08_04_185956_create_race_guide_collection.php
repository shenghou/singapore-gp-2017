<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRaceGuideCollection extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('raceguide_collection', function($table) {
			$table->increments('id');
			$table->string('title');
			$table->string('link');
			$table->string('slug');
			$table->string('img');
			$table->integer('sort');
			$table->boolean('hidden');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('raceguide_collection');
	}

}
