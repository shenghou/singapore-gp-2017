<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiRaceguide extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('api_raceguide', function($table) {
            $table->increments('id');
            $table->integer('collection_id');
            $table->string('title');
            $table->string('slug');
            $table->string('description');
            $table->string('img');
            $table->integer('sort');
            $table->boolean('hidden');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_raceguide');
    }

}
