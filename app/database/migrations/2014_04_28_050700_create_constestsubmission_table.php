<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstestsubmissionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contest_submissions', function($table) {
			$table->increments('id');
			$table->string('media');
			$table->string('transaction_number');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email');
            $table->unsignedInteger('contest_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contest_submissions');
	}

}
