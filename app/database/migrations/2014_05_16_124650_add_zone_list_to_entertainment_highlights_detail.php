<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddZoneListToEntertainmentHighlightsDetail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('entertainment_highlights_detail', function(Blueprint $table)
		{
            $table->string('zone_list');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('entertainment_highlights_detail', function(Blueprint $table)
		{
            $table->dropColumn('zone_list');
		});
	}

}
