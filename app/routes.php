<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
 * Routing from old links to new links
 */

/**
 * To redirect old ticket with category id
 * to new one
 */
Route::any("ticket/ticket-details.php", function(){
    if (Input::get('category_id'))
    {
        return Redirect::to('tickets/listing/'.Input::get('category_id'));
    }else{
        return Redirect::to('tickets/all');
    }
});

Route::any("tickets/details/ticket/ticket-details.php", function(){
    if (Input::get('category_id'))
    {
        return Redirect::to('tickets/listing/'.Input::get('category_id'));
    }else{
        return Redirect::to('tickets/all');
    }
});

/*
|--------------------------------------------------------------------------
| Geo IP
|--------------------------------------------------------------------------
*/
Route::get('geoip/country/{ip}', array(
    'as' => 'geoip.country',
    'uses' => 'GeoipController@country'
));

Route::get('geoip/your-country', array(
    'as' => 'geoip.your-country',
    'uses' => 'GeoipController@yourCountry'
));

/*
|--------------------------------------------------------------------------
| Terms and Conditions
|--------------------------------------------------------------------------
*/
Route::get('terms', array(
    'as' => 'terms',
    'uses' => 'TermsController@showWelcome'
));

/*
|--------------------------------------------------------------------------
| Newsletter Facebook Iframe
|--------------------------------------------------------------------------
*/
Route::get('iframe/newsletter', array(
    'as' => 'newsletterIframe',
    'uses' => 'MiscellaneousController@showNewsletterIframe'
));

Route::post('iframe/newsletter', array(
    'as' => 'newsletterIframe.post',
    'uses' => 'MiscellaneousController@postToNewsletterIframe'
));

/*
|--------------------------------------------------------------------------
| Campaign
|--------------------------------------------------------------------------
*/

Route::get('contests/mobile-app', array(
    'as' => 'campaign',
    'uses' => 'CampaignController@showWelcome'
));

Route::get('campaign/contest/{year}/{month}', array(
    'as' => 'campaign.contest',
    'uses' => 'CampaignController@contest'
));

Route::post('campaign/contest', array(
    'as' => 'campaign.contest.post',
    'uses' => 'CampaignController@contestPost'
));

Route::get('campaign/contest/submit', array(
    'as' => 'campaign.contest.submit',
    'uses' => 'CampaignController@contestSubmit'
));

Route::get('campaign/contest/hsp-contest', array(
    'as' => 'campaign.contest.hspcontest',
    'uses' => 'CampaignController@hspContest'
));


// ----Old URL-----
Route::get('campaigIn/contest/landing/{year}/{month}', array(
    'as' => 'campaign.contest.landing',
    'uses' => 'CampaignController@contestLanding'
));

// ----Old URL-----
Route::get('campaign/grand-draw/landing/{year}/{month}', array(
    'as' => 'campaign.granddraw.landing',
    'uses' => 'CampaignController@grandDrawLanding'
));

// ----New URL-----
Route::get('contests/mobile-app/monthly/{year}/{month}', array(
    'as' => 'campaign.contest.landing',
    'uses' => 'CampaignController@contestLanding'
));

// ----New URL-----
Route::get('contests/mobile-app/grand-draw/{year}/{month}', array(
    'as' => 'campaign.granddraw.landing',
    'uses' => 'CampaignController@grandDrawLanding'
));

Route::get('campaign/contest/how-to-participate', array(
    'as' => 'campaign.contest.howToParticipateContest',
    'uses' => 'CampaignController@howToParticipateContest'
));

Route::get('campaign/grand-draw/how-to-participate', array(
    'as' => 'campaign.contest.howToParticipateGrandDraw',
    'uses' => 'CampaignController@howToParticipateGrandDraw'
));

Route::get('/campaign/contest/terms', array(
    'as' => 'campaign.contest.contestTerms',
    'uses' => 'CampaignController@contestTerms'
));

Route::get('/campaign/grand-draw/terms', array(
    'as' => 'campaign.contest.grandDrawTerms',
    'uses' => 'CampaignController@grandDrawTerms'
));

Route::get('/campaign/contest/terms_raw', array(
    'as' => 'campaign.contest.contestTermsRaw',
    'uses' => 'CampaignController@contestTermsRaw'
));

Route::get('/campaign/grand-draw/terms_raw', array(
    'as' => 'campaign.contest.grandDrawTermsRaw',
    'uses' => 'CampaignController@grandDrawTermsRaw'
));

Route::post('/campaign/newsletter', array(
    'as' => 'campaign.newsletter',
    'uses' => 'CampaignController@subscribeNewsletter'
));


//Route::group(array('before' => 'auth.basic'), function() {

/*
|--------------------------------------------------------------------------
| Home Routes
|--------------------------------------------------------------------------
*/
Route::get('/', array(
    'as' => 'home',
    'uses' => 'HomeController@showWelcome'
));

/*
|--------------------------------------------------------------------------
| On-Track Routes
|--------------------------------------------------------------------------
*/
Route::get('on-track', array(
    'as' => 'ontrack',
    'uses' => 'OnTrackController@showWelcome'
));

Route::get('on-track/2016-race-calendar', array(
    'as' => 'ontrack.2016Calendar',
    'uses' => 'OnTrackController@showCalendar'
));

Route::get('on-track/season-results', array(
    'as' => 'ontrack.seasonResults',
    'uses' => 'OnTrackController@showSeasonResults'
));

Route::get('on-track/driver-standings', array(
    'as' => 'ontrack.driverStandings',
    'uses' => 'OnTrackController@showDriverStandings'
));

// 2015 Insights
Route::get('on-track/F1-Insights', array(
    'as' => 'ontrack.f1insights',
    'uses' => 'OnTrackController@showInsights'
));

Route::get('on-track/F1-Insights/Pre-Season-Test-1', array(
    'as' => 'ontrack.f1insights.preseasson',
    'uses' => 'OnTrackController@showPreSeason'
));

Route::get('on-track/F1-Insights/Rule-Change-Power-Units', array(
    'as' => 'ontrack.f1insights.rulechange',
    'uses' => 'OnTrackController@showRuleChange'
));

Route::get('on-track/F1-Insights/Rule-Change-increase-f1-car-weight', array(
    'as' => 'ontrack.f1insights.rulechangeincrease',
    'uses' => 'OnTrackController@showRuleChangeIncrease'
));

Route::get('on-track/F1-Insights/Penalties', array(
    'as' => 'ontrack.f1insights.penalties',
    'uses' => 'OnTrackController@showPenalties'
));

Route::get('on-track/F1-Insights/Rule-Change-pre-race-parc-ferme', array(
    'as' => 'ontrack.f1insights.rulechangeprerace',
    'uses' => 'OnTrackController@showRuleChangePreRace'
));

Route::get('on-track/F1-Insights/pre-season-test-2', array(
    'as' => 'ontrack.f1insights.preseasontest2',
    'uses' => 'OnTrackController@showPreSeasonTest2'
));

Route::get('on-track/F1-Insights/suspending-a-race', array(
    'as' => 'ontrack.f1insights.suspendingarace',
    'uses' => 'OnTrackController@showSuspending'
));

Route::get('on-track/F1-Insights/next-generation-of-stars', array(
    'as' => 'ontrack.f1insights.nextgeneration',
    'uses' => 'OnTrackController@showNextGeneration'
));

Route::get('on-track/F1-Insights/pre-season-test-3', array(
    'as' => 'ontrack.f1insights.preseasontest3',
    'uses' => 'OnTrackController@showPreSeasonTest3'
));

Route::get('on-track/F1-Insights/photographer-mark-sutton', array(
    'as' => 'ontrack.f1insights.photographermarksutton',
    'uses' => 'OnTrackController@showPhotographerMarkSutton'
));

Route::get('on-track/F1-Insights/2015-australian-gp-qualifying-report', array(
    'as' => 'ontrack.f1insights.qualifyingreport',
    'uses' => 'OnTrackController@showQualifyingReport'
));

Route::get('on-track/F1-Insights/2015-malaysian-gp-qualifying-report', array(
    'as' => 'ontrack.f1insights.malaysianqualifyingreport',
    'uses' => 'OnTrackController@showQualifyingReportMalaysian'
));

Route::get('on-track/F1-Insights/2015-chinese-gp-qualifying-report', array(
    'as' => 'ontrack.f1insights.chinesequalifyingreport',
    'uses' => 'OnTrackController@showQualifyingReportChinese'
));

Route::get('on-track/F1-Insights/2015-bahrain-gp-qualifying-report', array(
    'as' => 'ontrack.f1insights.bahrainqualifyingreport',
    'uses' => 'OnTrackController@showQualifyingReportBahrain'
));

Route::get('on-track/F1-Insights/2015-spanish-gp-qualifying-report', array(
    'as' => 'ontrack.f1insights.spanishqualifyingreport',
    'uses' => 'OnTrackController@showQualifyingReportSpanish'
));

Route::get('on-track/F1-Insights/2015-monaco-gp-qualifying-report', array(
    'as' => 'ontrack.f1insights.monacoqualifyingreport',
    'uses' => 'OnTrackController@showQualifyingReportMonaco'
));

Route::get('on-track/F1-Insights/2015-canada-gp-qualifying-report', array(
    'as' => 'ontrack.f1insights.canadaqualifyingreport',
    'uses' => 'OnTrackController@showQualifyingReportCanada'
));

Route::get('on-track/F1-Insights/2015-austrian-gp-qualifying-report', array(
    'as' => 'ontrack.f1insights.austrianqualifyingreport',
    'uses' => 'OnTrackController@showQualifyingReportAustrian'
));

Route::get('on-track/F1-Insights/2015-australian-gp-post-race-report', array(
    'as' => 'ontrack.f1insights.postracereport',
    'uses' => 'OnTrackController@showPostRaceReport'
));

Route::get('on-track/F1-Insights/2015-malaysian-gp-post-race-report', array(
    'as' => 'ontrack.f1insights.postracereportmalaysian',
    'uses' => 'OnTrackController@showPostRaceReportMalaysian'
));

Route::get('on-track/F1-Insights/2015-chinese-gp-post-race-report', array(
    'as' => 'ontrack.f1insights.postracereportchinese',
    'uses' => 'OnTrackController@showPostRaceReportChinese'
));

Route::get('on-track/F1-Insights/2015-bahrain-gp-post-race-report', array(
    'as' => 'ontrack.f1insights.postracereportbahrain',
    'uses' => 'OnTrackController@showPostRaceReportBahrain'
));

Route::get('on-track/F1-Insights/2015-spanish-gp-post-race-report', array(
    'as' => 'ontrack.f1insights.postracereportspanish',
    'uses' => 'OnTrackController@showPostRaceReportSpanish'
));

Route::get('on-track/F1-Insights/2015-monaco-gp-post-race-report', array(
    'as' => 'ontrack.f1insights.postracereportmonaco',
    'uses' => 'OnTrackController@showPostRaceReportMonaco'
));

Route::get('on-track/F1-Insights/2015-canada-gp-post-race-report', array(
    'as' => 'ontrack.f1insights.postracereportcanada',
    'uses' => 'OnTrackController@showPostRaceReportCanada'
));

Route::get('on-track/F1-Insights/2015-austrian-gp-post-race-report', array(
    'as' => 'ontrack.f1insights.postracereportaustrian',
    'uses' => 'OnTrackController@showPostRaceReportAustrian'
));

Route::get('on-track/F1-Insights/2015-british-gp-post-race-report', array(
    'as' => 'ontrack.f1insights.postracereportbritish',
    'uses' => 'OnTrackController@showPostRaceReportBritish'
));

Route::get('on-track/F1-Insights/virtual-safety-car-future', array(
    'as' => 'ontrack.f1insights.virtualsafetycar',
    'uses' => 'OnTrackController@showVirtualsafetycar'
));

Route::get('on-track/F1-Insights/FIA-plans-to-bring-back-F1-refuelling-and-plans-faster-cars-for-2017', array(
    'as' => 'ontrack.f1insights.fiaplanstobring',
    'uses' => 'OnTrackController@showFiaPlansToBring'
));

Route::get('on-track/F1-Insights/Rookies-show-their-mettle-in-Austrian-F1-test', array(
    'as' => 'ontrack.f1insights.rookiesaustrianf1',
    'uses' => 'OnTrackController@showRookiesAustrianF1'
));

Route::get('on-track/F1-Insights/2015-british-gp-qualifying-report', array(
    'as' => 'ontrack.f1insights.silverstoneracepreview',
    'uses' => 'OnTrackController@showSilverstoneracepreview'
));

Route::get('on-track/F1-Insights/hungariangpqualifyingreport', array(
    'as' => 'ontrack.f1insights.hungariangpqualifyingreport',
    'uses' => 'OnTrackController@hungariangpqualifyingreport'
));
Route::get('on-track/F1-Insights/hungariangppostracereport', array(
    'as' => 'ontrack.f1insights.hungariangppostracereport',
    'uses' => 'OnTrackController@hungariangppostracereport'
));

Route::get('on-track/F1-Insights/shootlikeapro', array(
    'as' => 'ontrack.f1insights.shootlikeapro',
    'uses' => 'OnTrackController@shootlikeapro'
));

Route::get('on-track/F1-Insights/2015belgiangpqualifyingreport', array(
    'as' => 'ontrack.f1insights.2015belgiangpqualifyingreport',
    'uses' => 'OnTrackController@belgiangpqualifyingreport'
));

Route::get('on-track/F1-Insights/2015belgiangppost-racereport', array(
    'as' => 'ontrack.f1insights.2015belgiangppost-racereport',
    'uses' => 'OnTrackController@belgiangppostracereport2015'
));

Route::get('on-track/F1-Insights/2015a-look-into-formula-one-drivers-gym-workouts', array(
    'as' => 'ontrack.f1insights.2015a-look-into-formula-one-drivers-gym-workouts',
    'uses' => 'OnTrackController@alookintoformulaonedriversgymworkouts'
));

Route::get('on-track/F1-Insights/2015monzagpqualifyingreport', array(
    'as' => 'ontrack.f1insights.2015monzagpqualifyingreport',
    'uses' => 'OnTrackController@monzagpqualifyingreport2015'
));

Route::get('on-track/F1-Insights/2015monzagppostracereport', array(
    'as' => 'ontrack.f1insights.2015monzagppostracereport',
    'uses' => 'OnTrackController@monzagppostracereport2015'
));

Route::get('on-track/F1-Insights/2015qaawithalanjones', array(
    'as' => 'ontrack.f1insights.2015qaawithalanjones',
    'uses' => 'OnTrackController@qaawithalanjones2015'
));
// singapore 
Route::get('on-track/F1-Insights/2015formula1singaporeairlinessingaporegrandprixpreview', array(
    'as' => 'ontrack.f1insights.2015formula1singaporeairlinessingaporegrandprixpreview',
    'uses' => 'OnTrackController@formula1singaporeairlinessingaporegrandprixpreview2015'
));

Route::get('on-track/F1-Insights/2015singaporegpracereport', array(
    'as' => 'ontrack.f1insights.2015singaporegpracereport',
    'uses' => 'OnTrackController@singaporegpracereport2015'
));

Route::get('on-track/F1-Insights/2015singaporegpsuzukaracepreview', array(
    'as' => 'ontrack.f1insights.2015singaporegpsuzukaracepreview',
    'uses' => 'OnTrackController@singaporegpsuzukaracepreview2015'
));

Route::get('on-track/F1-Insights/2015suzukapostracereport', array(
    'as' => 'ontrack.f1insights.2015singaporegpsuzukapostracereport',
    'uses' => 'OnTrackController@singaporegpsuzukapostracereport2015'
));

Route::get('on-track/F1-Insights/2015russiangpqualifyingreport', array(
    'as' => 'ontrack.f1insights.2015russiangpqualifyingreport',
    'uses' => 'OnTrackController@russiangpqualifyingreport2015'
));

Route::get('on-track/F1-Insights/threethingsmissedatusgp2015', array(
    'as' => 'ontrack.f1insights.2015usgpqualifyingreport',
    'uses' => 'OnTrackController@usgpqualifyingreport2015'
));

Route::get('on-track/F1-Insights/2015usgppostracereport', array(
    'as' => 'ontrack.f1insights.2015uspostracereport',
    'uses' => 'OnTrackController@uspostracereport2015'
));

Route::get('on-track/F1-Insights/2015mexicogpqualifyingreport', array(
    'as' => 'ontrack.f1insights.2015mexicogpqualifyingreport',
    'uses' => 'OnTrackController@mexicogpqualifyingreport'
));

Route::get('on-track/F1-Insights/2015mexicogppostracereport', array(
    'as' => 'ontrack.f1insights.2015mexicogppostracereport',
    'uses' => 'OnTrackController@mexicogppostracereport'
));

Route::get('on-track/F1-Insights/2015braziliangpqualifyingreport', array(
    'as' => 'ontrack.f1insights.2015braziliangpqualifyingreport',
    'uses' => 'OnTrackController@braziliangpqualifyingreport'
));

Route::get('on-track/F1-Insights/2015braziliangppostracereport', array(
    'as' => 'ontrack.f1insights.2015braziliangppostracereport',
    'uses' => 'OnTrackController@braziliangppostracereport'
));

Route::get('on-track/F1-Insights/2015abudhabigpqualifyingreport', array(
    'as' => 'ontrack.f1insights.2015abudhabigpqualifyingreport',
    'uses' => 'OnTrackController@abudhabigpqualifyingreport'
));

Route::get('on-track/F1-Insights/2015abudhabigppostracereport', array(
    'as' => 'ontrack.f1insights.2015abudhabigppostracereport',
    'uses' => 'OnTrackController@abudhabigppostracereport'
));

Route::get('on-track/F1-Insights/five_highlights_of_the_2015_Formula_One_Season', array(
    'as' => 'ontrack.f1insights.fivehighlightsof2015f1season',
    'uses' => 'OnTrackController@fivehighlightsof2015f1season'
));

Route::get('on-track/F1-Insights/five_things_to_watch_out_for_in_the_2016_f1_season', array(
    'as' => 'ontrack.f1insights.fiveThingstowatchoutforinthe2016f1season',
    'uses' => 'OnTrackController@fiveThingstowatchoutforinthe2016f1season'
));

Route::get('on-track/F1-Insights/formula_1_barcelona_test_1_report', array(
    'as' => 'ontrack.f1insights.formula1barcelonatest1report',
    'uses' => 'OnTrackController@formula1barcelonatest1report'
));

Route::get('on-track/F1-Insights/5_things_we_learnt_from_the_barcelona_test', array(
    'as' => 'ontrack.f1insights.fiveThingsWeLearntFromTheBarcelonaTest',
    'uses' => 'OnTrackController@fiveThingsWeLearntFromTheBarcelonaTest'
));
// 2015 Insights End

// 2016 Insights

Route::get('on-track/F1-Insights/New_Elimination-style_Formula_1_Qualifying', array(
    'as' => 'ontrack.f1insights.newEliminationStyleFormula1Qualifying',
    'uses' => 'OnTrackController@newEliminationStyleFormula1Qualifying'
));

Route::get('on-track/F1-Insights/five_things_you_missed_at_the_australian_gp_2016', array(
    'as' => 'ontrack.f1insights.fiveThingsYouMissedAtTheAustralianGP',
    'uses' => 'OnTrackController@fiveThingsYouMissedAtTheAustralianGP'
));

Route::get('on-track/F1-Insights/qa_with_martin brundle', array(
    'as' => 'ontrack.f1insights.qaWithMartinBrundle',
    'uses' => 'OnTrackController@qaWithMartinBrundle'
));

Route::get('on-track/F1-Insights/five_things_you_missed_at_the_bahrain_gp', array(
    'as' => 'ontrack.f1insights.fivethingsyoumissedatthebahraingp',
    'uses' => 'OnTrackController@fivethingsyoumissedatthebahraingp'
));

Route::get('on-track/F1-Insights/chinese_grand_prix_highlights', array(
    'as' => 'ontrack.f1insights.chinesegrandprixhighlights',
    'uses' => 'OnTrackController@chinesegrandprixhighlights'
));

Route::get('on-track/F1-Insights/highlights_of_formula1_russian_grand_prix', array(
    'as' => 'ontrack.f1insights.russiangrandprixhighlights',
    'uses' => 'OnTrackController@russiangrandprixhighlights'
));

Route::get('on-track/F1-Insights/thrills_spills_and_a_victorious_verstappen', array(
    'as' => 'ontrack.f1insights.thrillsspillsandavictoriousverstappen',
    'uses' => 'OnTrackController@thrillsspillsandavictoriousverstappen'
));

Route::get('on-track/F1-Insights/highlights_of_the_formula1_monaco_grand_prix', array(
    'as' => 'ontrack.f1insights.highlightsoftheformula1monacograndprix',
    'uses' => 'OnTrackController@highlightsoftheformula1monacograndprix'
));

Route::get('on-track/F1-Insights/three_key_moments_of_the_formula1_canadian_grand_prix', array(
    'as' => 'ontrack.f1insights.threekeymomentsoftheformula1canadiangrandprix',
    'uses' => 'OnTrackController@threekeymomentsoftheformula1canadiangrandprix'
));

Route::get('on-track/F1-Insights/behind_the_scene_building_the_marina_bay_street_circuit', array(
    'as' => 'ontrack.f1insights.behindthescenebuildingthemarinabaystreetcircuit',
    'uses' => 'OnTrackController@behindthescenebuildingthemarinabaystreetcircuit'
));


Route::get('on-track/F1-Insights/where_to_get_the_most_out_of_the_singapore_gp', array(
    'as' => 'ontrack.f1insights.wheretogetthemostoutofthesingaporegp',
    'uses' => 'OnTrackController@wheretogetthemostoutofthesingaporegp'
));


Route::get('on-track/F1-Insights/will_redbull_or_ferrari_be_the_first_to_beat_mercedes_on_a_regular_basis', array(
    'as' => 'ontrack.f1insights.willredbullorferraribethefirsttobeatmercedesonaregularbasis',
    'uses' => 'OnTrackController@willredbullorferraribethefirsttobeatmercedesonaregularbasis'
));


Route::get('on-track/F1-Insights/europegphighlights2016', array(
    'as' => 'ontrack.f1insights.europegphighlights2016',
    'uses' => 'OnTrackController@europegphighlights2016'
));

Route::get('on-track/F1-Insights/austriangphighlights2016', array(                                                                'as' => 'ontrack.f1insights.austriangphighlights2016',
    'uses' => 'OnTrackController@austriangphighlights2016'
));

Route::get('on-track/F1-Insights/britishgphighlights2016', array(                                                                'as' => 'ontrack.f1insights.britishgphighlights2016',
    'uses' => 'OnTrackController@britishgphighlights2016'
));

Route::get('on-track/F1-Insights/hungarygphighlights2016', array(
     'as' => 'ontrack.f1insights.hungarygphighlights2016',
     'uses' => 'OnTrackController@hungarygphighlights2016'
));

Route::get('on-track/F1-Insights/germangphighlights2016', array(
     'as' => 'ontrack.f1insights.germangphighlights2016',
     'uses' => 'OnTrackController@germangphighlights2016'
));

Route::get('on-track/F1-Insights/belgiumgphighlights2016', array(
    'as' => 'ontrack.f1insights.belgiumgphighlights2016',
     'uses' => 'OnTrackController@belgiumgphighlights2016'
));

Route::get('on-track/F1-Insights/italygphighlights2016', array(
    'as' => 'ontrack.f1insights.italyphighlights2016',
    'uses' => 'OnTrackController@italygphighlights2016'
));

Route::get('on-track/F1-Insights/james-allen-feedback', array(
    'as' => 'ontrack.f1insights.jamesallenfeedback2016',
    'uses' => 'OnTrackController@jamesallenfeedback2016'
));

Route::get('on-track/F1-Insights/malaysiangphighlights2016', array(
    'as' => 'ontrack.f1insights.malaysiangphighlights2016',
    'uses' => 'OnTrackController@malaysiangphighlights2016'
));

// 2016 Insights End

Route::get('on-track/sgp-highlights', array(
    'as' => 'ontrack.sgpHighlights',
    'uses' => 'OnTrackController@showSgpHighlights'
));

Route::get('on-track/teams-and-drivers', array(
    'as' => 'ontrack.teamsAndDrivers',
    'uses' => 'OnTrackController@showTeamsAndDrivers'
));

Route::get('on-track/teams-and-drivers/{team}/{driver?}', array(
    'as' => 'ontrack.teamDetails',
    'uses' => 'OnTrackController@showTeam'
//    function() {
//        return Redirect::route('404');
//    }
));

Route::get('on-track/circuit-park-map', array(
    'as' => 'ontrack.circuitParkMap',
    'uses' => 'OnTrackController@showCircuitParkMap'
));

Route::get('on-track/gallery/{year?}/{type?}/{cats?}', array(
    'as' => 'ontrack.gallery',
    'uses' => 'OnTrackController@showGallery'
));

Route::get('on-track/video/{id}', array(
    'as' => 'ontrack.video.byId',
    'uses' => 'OnTrackController@showVideo'
));

// Route::get('on-track/2014-race-schedule', array(
//     'as' => 'ontrack.raceschedule',
//     'uses' => 'OnTrackController@showRaceSchedule'
// ));
Route::get('on-track/2014-race-schedule', array('as' => 'ontrack.raceschedule', function() {
    return Redirect::route('home');
}));

/*
|--------------------------------------------------------------------------
| Off-Track Routes
|--------------------------------------------------------------------------
*/
Route::get('off-track', array(
    'as' => 'offtrack',
    'uses' => 'OffTrackController@showWelcome'
));

//Route::get('off-track/headliners', array(
//    'as' => 'offtrack.headliners',
//    'uses' => 'OffTrackController@showHeadliners'
//));

Route::get('off-track/headliners', array(
    'as' => 'offtrack.headliners',
    'uses' => 'OffTrackController@showHeadliners'
));

// Route::get('off-track/headliners', array('as' => 'offtrack.headliners', function() {
//     return Redirect::route('offtrack.pastHeadliners');
// }));

Route::get('off-track/headliner/{headliner?}', array(
    'as' => 'offtrack.headliner',
    'uses' => 'OffTrackController@showHeadlinerDetails'
));

Route::get('off-track/past-headliners', array(
    'as' => 'offtrack.pastHeadliners',
    'uses' => 'OffTrackController@showPastHeadliners'
));

Route::get('off-track/gallery/{year?}/{type?}/{cats?}', array(
    'as' => 'offtrack.gallery',
    'uses' => 'OffTrackController@showGallery'
));

Route::get('off-track/video/{id}', array(
    'as' => 'offtrack.video.byId',
    'uses' => 'OffTrackController@showVideo'
));


/*
|--------------------------------------------------------------------------
| Tickets Routes
|--------------------------------------------------------------------------
*/
Route::get('tickets', array(
    'as' => 'tickets',
    'uses' => 'TicketsController@showWelcome'
));

Route::get('tickets/listings', array(
    'as' => 'tickets.listings',
    'uses' => 'TicketsController@showListings'
));

Route::get('tickets/map', array(
    'as' => 'tickets.map',
    'uses' => 'TicketsController@showMap'
));

Route::get('tickets/en/listing/{type}', array(

    'as' => 'tickets.listing.byType',
    'uses' => 'TicketsController@showListingByTypeEN'
));

Route::get('tickets/cn/listing/{type}', array(

    'as' => 'tickets.listing.byType',
    'uses' => 'TicketsController@showListingByTypeCN'
));

Route::get('tickets/listing/{type}', array(
    'as' => 'tickets.listing.byType',
    'uses' => 'TicketsController@showListingByType'
));

Route::get('tickets/zones/{id}', array(
    'as' => 'tickets.zones.byId',
    'uses' => 'TicketsController@showZones'
));

Route::get('tickets/details', array(
    'as' => 'tickets.details',
    'uses' => 'TicketsController@showDetails'
));

// Route::get('tickets/specials', array(
//     'as' => 'tickets.ticketspecials',
//     'uses' => 'TicketsController@showTicketSpecial'
// ));

/*
 * Super Early Bird
 * -- disable this to disable SEB --
 */
if (AppConfig::$isSuperEarlyBirdOn) {
    Route::get('tickets/listings/super-early-bird/{lang?}', array(
        'as' => 'tickets.listing.seb',
        'uses' => 'TicketsController@showListingSEB_HC'
    ));
    Route::get('tickets/details/20/{infoId}/{lang?}', array(
        'as' => 'tickets.details.byId.seb',
        'uses' => 'TicketsController@showDetailsByIdSEB_HC'
    ));

}else{

    Route::get('tickets/listings/super-early-bird', array(
        'as' => 'tickets.listing.seb',
        function(){ return Redirect::route('tickets'); }
    ));

    Route::get('tickets/details/20/{infoId}', array(
        'as' => 'tickets.details.byId.seb',
        function(){ return Redirect::route('tickets'); }
    ));

    Route::get('tickets/listings/super-early-bird/{lang?}', array(

        function() { return Redirect::route('404'); }
    ));
    Route::get('tickets/details/20/{infoId}/{lang?}', array(

        function() { return Redirect::route('404'); }
    ));
}
/** -- end -- **/

Route::get('tickets/details/{categoryId}/{infoId}/{ticketTypeId?}', array(
    'as' => 'tickets.details.byId',
    'uses' => 'TicketsController@showDetailsById'
));

Route::get('tickets/details/singaporespecials', array(
    'as' => 'tickets.ticketspecials',
    'uses' => 'TicketsController@showTicketSpecial'
));

Route::get('tickets/sales-channels', array(
    'as' => 'tickets.sales',
    'uses' => 'TicketsController@showSales'
));
Route::get('tickets/sales-channels/singapore', array(
    'as' => 'tickets.sales.singapore',
    'uses' => 'TicketsController@showSalesSingapore'
));
Route::get('tickets/sales-channels/international', array(
    'as' => 'tickets.sales.international',
    'uses' => 'TicketsController@showSalesInternational'
));

Route::get('tickets/packages/hotels-and-travel', array(
    'as' => 'tickets.packages.hotelsAndTravel',
    'uses' => 'TicketsController@showHotelsAndTravelPackages'
));
Route::get('tickets/packages/hospitality', array(
    'as' => 'tickets.packages.hospitality',
    'uses' => 'TicketsController@showHospitalityPackages'
));
Route::post('tickets/packages/hospitality', array(
    'as' => 'tickets.packages.hospitality.post',
    'uses' => 'TicketsController@postToHospitalityPackages'
));

Route::get('tickets/self-collection-venue', array(
    'as' => 'tickets.selfCollectionVenue',
    'uses' => 'TicketsController@showSelfCollection'
));

Route::get('tickets/self-collection-venue/app', array(
    'as' => 'tickets.selfCollectionVenueApp',
    'uses' => 'TicketsController@showSelfCollectionForApp'
));

/*
|--------------------------------------------------------------------------
| Media Routes
|--------------------------------------------------------------------------
*/
Route::get('media', array(
    'as' => 'media',
    'uses' => 'MediaController@showWelcome'
));

Route::get('media/mobile-app', array(
    'as' => 'media.mobileApp',
    'uses' => 'MediaController@showMobileApp'
));

Route::get('media/press-release', array(
    'as' => 'media.pressRelease',
    'uses' => 'MediaController@showPressRelease'
));

Route::get('media/press-release/{id}', array(
    'as' => 'media.pressRelease.byId',
    'uses' => 'MediaController@showPressReleaseById'
));

Route::get('media/media-site', array(
    'as' => 'media.mediaSite',
    'uses' => 'MediaController@showMediaSite'
));

/*Route::get('media/accreditations', array(
    'as' => 'media.accreditation',
    'uses' => 'MediaController@showAccreditation'
));

Route::get('media/accreditations/show-form/{formType}', array(
    'as' => 'media.accreditation.show-form',
    'uses' => 'MediaController@showMediaAccreditationForm'
));

Route::post('media/accreditations/media-fia-form', array(
    'as' => 'media.accreditation.postAccreditationFiaForm',
    'uses' => 'MediaController@postAccreditationFiaForm'
));

Route::post('media/accreditations/media-entertainement-form', array(
    'as' => 'media.accreditation.postAccreditationEntertainmentForm',
    'uses' => 'MediaController@postAccreditationEntertainmentForm'
));

Route::get('media/accreditations/submit', array (
    'as' => 'media.accreditation.submit',
    'uses' => 'MediaController@showAccreditationSubmit'
));

Route::get('media/accreditations-entertainment/submit', array (
    'as' => 'media.accreditation.entertainment.submit',
    'uses' => 'MediaController@showAccreditationEntertainmentSubmit'
));

Route::get('media/accreditations/entertainment', array(
    'as' => 'media.accreditation.entertainment',
    'uses' => 'MediaController@showEntertainmentForm'
));

Route::get('media/entertainment-accreditation', array(
    'as' => 'media.entertainment-accreditation',
    'uses' => 'MediaController@showEntertainmentAccreditation'
));
*/

Route::get('media/entry-form', array(
    'as' => 'media.chooseEntryForm',
    'uses' => 'MediaController@showWelcome'
    /* 'uses' => 'MediaController@showChooseEntryForm'*/
));
Route::post('media/entry-form', array(
    'as' => 'media.postToEntryForm',
    'uses' => 'MediaController@showWelcome'
    /* 'uses' => 'MediaController@postEntryForm' */
));
Route::get('media/entry-form/{formType}', array(
    'as' => 'media.showEntryForm.byType',
    'uses' => 'MediaController@showEntryForm'
));


/*
|--------------------------------------------------------------------------
| Fanzone Routes
|--------------------------------------------------------------------------
*/
//Route::get('fanzone', array(
//    'as' => 'fanzone',
//    'uses' => 'FanzoneController@showWelcome'
//));

Route::get('fanzone/merchandise', array(
    'as' => 'fanzone.merchandise',
    'uses' => 'FanzoneController@fanzoneMerchandise'
));

/*
|--------------------------------------------------------------------------
| Rev Up Singapore Routes
|--------------------------------------------------------------------------
*/


Route::get('revupsingapore', array(
    'as' => 'revUp',
    // redirect to home page
    'uses' => 'HomeController@showWelcome'
    /*'uses' => 'RevUpController@showWelcome'*/
));

Route::get('revupsingapore/{slug}', array(
    'as' => 'revUp.content',
    'uses' => 'HomeController@showWelcome'
   /* 'uses' => 'RevUpController@showRevUp' */
));

Route::post('revupsingapore/{slug}', array(
    'as' => 'revUp.content.post',
    'uses' => 'HomeController@showWelcome'
   /* 'uses' => 'RevUpController@postRevUp' */
));

Route::get('revupsingapore/{slug}/export', array(
    'as' => 'revUp.contests.export',
    'before' => 'auth',
    'uses' => 'RevUpController@exportContestCsv'
));

Route::get('revupsingapore/kartnival/pdf/{fullname}/{nirc}/{timeslot}',array(
    'as' => 'revUp.kartnival.pdf',
    'uses' => 'RevUpController@exportKartnivalPdf'
), function($fullname,$nirc,$timeslot){

}
);

//Shop
Route::get('shop', array(
    'as' => 'merchandiseShop',
   // 'uses' => 'MerchandiseShopController@showWelcome'
    // redirect to home.
    'uses' => 'HomeController@showWelcome'
));


/*
|--------------------------------------------------------------------------
| Contest Routes
|--------------------------------------------------------------------------
*/
Route::get('fanzone', array(
    'as' => 'win',
    'uses' => 'FanzoneController@showWelcome'
));

Route::get('contests', array(
    'as' => 'contests',
    'uses' => 'PromotionsAndContestsController@showWelcome'
));

Route::get('fanzone/pit-stop-challenge', array(
    'as' => 'win.challenge.pitStop',
    'uses' => 'PromotionsAndContestsController@showPitStopChallenge'
//        'uses' => 'PromotionsAndContestsController@showWelcome'
));

Route::get('fanzone/wallpapers', array(
    'as' => 'win.wallpapers',
    'uses' => 'PromotionsAndContestsController@showWallpapers'
));

Route::get('fanzone/upgrade-to-green-room', array(
    'as' => 'win.contests',
    //'uses' => 'PromotionsAndContestsController@showForeignContest'
    'uses' => 'PromotionsAndContestsController@showWelcome'
));

Route::post('fanzone/upgrade-to-green-room', array(
    'as' => 'win.postForeignContests',
    //'uses' => 'PromotionsAndContestsController@postForeignContest'
    'uses' => 'PromotionsAndContestsController@showWelcome'
));

Route::get('contests/2017-super-early-bird-ticket-contest', array(
    'as' => 'win.contests.seb',
    'uses' => 'PromotionsAndContestsController@showSEBContest'
    //'uses' => 'PromotionsAndContestsController@showWelcome'
));

Route::post('contests/2017-super-early-bird-ticket-contest', array(
    'as' => 'win.postForeignContests',
    'uses' => 'PromotionsAndContestsController@postSEBContest'
    //'uses' => 'PromotionsAndContestsController@showWelcome'
));

Route::post('contests/are-you-daniel-ricciardos-biggest-fan', array(

    'as' => 'win.postDanielRicardoContests',
    'uses' => 'PromotionsAndContestsController@postDanielRicardoContest'
    // 'uses' => 'PromotionsAndContestsController@showWelcome'
));

Route::post('contests/max-verstappen-meet-and-greet-contest', array(

    'as' => 'win.postMaxVerstappenContests',
    'uses' => 'PromotionsAndContestsController@postMaxVerstappenContest'
    // 'uses' => 'PromotionsAndContestsController@showWelcome'
));

Route::get('contests/straits-times-contest', array(
    'as' => 'win.contests',
    //'uses' => 'PromotionsAndContestsController@showStraitstimescontest'
    'uses' => 'PromotionsAndContestsController@showWelcome'
));

Route::post('contests/straits-times-contest', array(
    'as' => 'win.postDanielRicardoContests',
    //'uses' => 'PromotionsAndContestsController@postStraitstimescontest'
    'uses' => 'PromotionsAndContestsController@showWelcome'
));

Route::get('contests/{slug}', array(
    'as' => 'win.contests',
    'uses' => 'PromotionsAndContestsController@showContest'
    //  'uses' => 'PromotionsAndContestsController@showWelcome'
));

Route::post('contests/{slug}', array(
    'as' => 'win.postContests',
    'uses' => 'PromotionsAndContestsController@postContest'
    // 'uses' => 'PromotionsAndContestsController@showWelcome'
));

Route::get('contests/{slug}/app', array(
    'as' => 'win.contests.app',
    //'uses' => 'PromotionsAndContestsController@showContestApp'
    'uses' => 'PromotionsAndContestsController@showWelcome'
));
Route::get('contests/{slug}/export', array(
    'as' => 'win.contests.export',
    'before' => 'auth',
    'uses' => 'PromotionsAndContestsController@exportContestCsv'
));

/*
|--------------------------------------------------------------------------
| Race Guide
|--------------------------------------------------------------------------
*/

//Route::get('raceguide', array(
//   'as' => 'raceguide',
//    'uses' => 'RaceGuidesController@index'
// ));



Route::get('eventguide/', array(
    'as' => 'raceguides.index',
    'uses' => 'RaceGuidesController@showWelcome'
));

Route::get('eventguide/tips', array(
    'as' => 'raceguides.tips',
    'uses' => 'RaceGuidesController@raceguideTips'
));

//Route::get('raceguide', array(
//    'as' => 'raceguide',
//    'uses' => 'RaceGuidesController@eventguide'
//));

Route::get('raceguide/{slug?}', function() {
    return Redirect::route('home');
});

Route::get('raceguide/2014-on-off-track-schedule/pdf', array(
    'as' => 'onofftrackschedule.pdf',
    'uses' => 'RaceGuidesController@OnOffTrackSchedulePdf'
));

Route::get('raceguide/2014-on-off-track-schedule/{date?}', array(
    'as' => 'onofftrackschedule',
    'uses' => 'RaceGuidesController@OnOffTrackSchedule'
));

Route::get('getting_around', array(
  'as' => 'raceguides.gettingaround',
   'uses' => 'RaceGuidesController@gettingaround'
));


// Hardcoded Guides

//Route::get('raceguide/collection/paddock_club', function() {
//    return Redirect::to('raceguide/paddock_club');
//});

Route::get('raceguide/collection/transport-guide', function() {
    return Redirect::to('raceguide/transport-guide');
});

//Route::get('raceguides/collection/tips', function() {
//    return Redirect::to('raceguides/tips');
//});

//Route::get('raceguide/tips/{app?}', array(
//    'as' => 'layouts.raceguides.tips',
//    'uses' => 'RaceGuidesController@raceguideTips'
//));

Route::get('paddockclub', array(
    'as' => 'raceguide.collection.paddockclub',
    'uses' => 'RaceGuidesController@paddockLanding'
));

Route::get('raceguide/transport-guide/{app?}', array(
    'as' => 'raceguide.transportguide',
    'uses' => 'RaceGuidesController@transportguide'
));


Route::get('raceguide/paddock_club', array(
    'as' => 'raceguide.collection.paddockLanding',
    'uses' => 'RaceGuidesController@paddockLanding'
));

Route::get('raceguide/paddock_club/app', array(
    'as' => 'raceguide.collection.paddockLandingApp',
    'uses' => 'RaceGuidesController@paddockLandingApp'
));

Route::get('raceguide/paddock_club/maps', array(
    'as' => 'raceguide.collection.paddockLanding.maps',
    'uses' => 'RaceGuidesController@paddockMaps'
));

Route::get('raceguide/paddock_club/useful_info/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.useful_info',
    'uses' => 'RaceGuidesController@paddockUseful'
));


Route::get('raceguide/paddock_club/activities/foodandbeverages/jean-georges/{app?}',
    array(
        'as' => 'raceguide.collection.paddockLanding.activities.the-world-of-jean-georges',
        'uses' => 'RaceGuidesController@paddockActivitiesTWOJG'
    ));

Route::get('raceguide/paddock-club-ticketholders/useful_info/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.useful_info',
    'uses' => 'RaceGuidesController@paddockUseful'
));


// Detail pages for Activities in Paddock


Route::get('raceguide/paddock_club/activities/app', array(
    'as' => 'raceguide.collection.paddockLanding.activitiesApp',
    'uses' => 'RaceGuidesController@paddockActivitiesApp'
));
Route::get('raceguide/paddock_club/schedules/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.schedules',
    'uses' => 'RaceGuidesController@paddockSchedules'
));
Route::get('raceguide/paddock_club/directions/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.directions',
    'uses' => 'RaceGuidesController@paddockDirections'
));
// Detail pages for Activities in Paddock


Route::get('raceguide/paddock_club/activities/foodandbeverages/itsuki-izakaya/{app?}',
    array(
        'as' => 'raceguide.collection.paddockLanding.activities.itsuki-izakaya',
        'uses' => 'RaceGuidesController@paddockActivitiesItsukiIzakaya'
    ));

Route::get('raceguide/paddock_club/activities/foodandbeverages/janice-wong/{app?}',
    array(
        'as' => 'raceguide.collection.paddockLanding.activities.janice-wong',
        'uses' => 'RaceGuidesController@paddockActivitiesJanicewong'
    ));

Route::get('raceguide/paddock_club/activities/foodandbeverages/flutes/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.flutes',
    'uses' => 'RaceGuidesController@paddockActivitiesflutes'
));

Route::get('raceguide/paddock_club/activities/foodandbeverages/librarybar/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.librarybar',
    'uses' => 'RaceGuidesController@paddockActivitieslibrarybar'
));

Route::get('raceguide/paddock_club/activities/foodandbeverages/nobu/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.nobu',
    'uses' => 'RaceGuidesController@paddockActivitiesnobu'
));

Route::get('raceguide/paddock_club/activities/foodandbeverages/tarte/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.tarte',
    'uses' => 'RaceGuidesController@paddockActivitiesTarte'
));

Route::get('raceguide/paddock_club/activities/foodandbeverages/basils/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.basils',
    'uses' => 'RaceGuidesController@paddockActivitiesBasils'
));

Route::get('raceguide/paddock_club/activities/foodandbeverages/como-cuisine/{app
    ?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.como-cuisine',
    'uses' => 'RaceGuidesController@paddockActivitiesCOMO'
));
Route::get('raceguide/paddock_club/activities/foodandbeverages/marina-bay-seafood
    /{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.marina-bay-seafood',
    'uses' => 'RaceGuidesController@paddockActivitiesMBSeafood'
));
Route::get('raceguide/paddock_club/activities/foodandbeverages/marina-bay-creamery
    /{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.marina-bay-creamery',
    'uses' => 'RaceGuidesController@paddockActivitiesMBCreamery'
));
Route::get('raceguide/paddock_club/activities/singapore-flyer/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.singapore-flyer',
    'uses' => 'RaceGuidesController@paddockActivitiesSingaporeFlyer'
));
Route::get('raceguide/paddock_club/activities/pit-lane-walk/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.pit-lane-walk',
    'uses' => 'RaceGuidesController@paddockActivitiesPitlanewalk'
));
Route::get('raceguide/paddock_club/activities/merchandise-booth/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.merchandise-booth',
    'uses' => 'RaceGuidesController@paddockActivitiesMerchandiseBooth'
));
Route::get('raceguide/paddock_club/activities/photo-opportunities/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.photo-opportunities',
    'uses' => 'RaceGuidesController@paddockActivitiesPhotoopportunities'
));
Route::get('raceguide/paddock_club/activities/digital-caricature/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.digital-caricature',
    'uses' => 'RaceGuidesController@paddockActivitiesDigitalCaricature'
));
Route::get('raceguide/paddock_club/activities/calligraphy/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.calligraphy',
    'uses' => 'RaceGuidesController@paddockActivitiesCalligraphy'
));
Route::get('raceguide/paddock_club/activities/palmistry/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.palmistry',
    'uses' => 'RaceGuidesController@paddockActivitiesPalmistry'
));
Route::get('raceguide/paddock_club/activities/sillhoutteartist/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.sillhouttes',
    'uses' => 'RaceGuidesController@paddockActivitiessillhouttes'
));
Route::get('raceguide/paddock_club/activities/entertainment-stage/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.entertainment-stage',
    'uses' => 'RaceGuidesController@paddockActivitiesEntertainmentStage'
));
Route::get('raceguide/paddock_club/activities/marina-bay-spa/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.marina-bay-spa',
    'uses' => 'RaceGuidesController@paddockActivitiesMarinaBaySpa'
));

Route::get('raceguide/paddock_club/activities/paddock-club-lifestyle-stage/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.paddock-club-lifestyle-stage',
    'uses' => 'RaceGuidesController@paddockActivitiesPaddockClubLifestyleStage'
));

Route::get('raceguide/paddock_club/activities/roving-artistes/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.roving-artistes',
    'uses' => 'RaceGuidesController@paddockActivitiesRovingArtistes'
));

/*Route::get('raceguide/paddock_club/activities/{category?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities',
    'uses' => 'RaceGuidesController@paddockActivities'
));*/

Route::get('raceguide/paddock_club/activities', array(
    'as' => 'raceguide.collection.paddockLanding.activities',
    'uses' => 'RaceGuidesController@paddockActivities'
));


Route::get('raceguide/paddock_club/activities/foodandbeverages/', array(
    'as' => 'raceguide.collection.paddockLanding.activities.fnb',
    'uses' => 'RaceGuidesController@paddockActivitiesFnb'
));

Route::get('raceguide/paddock_club/activities/activity_kiosks', array(
    'as' => 'raceguide.collection.paddockLanding.activities.activitykiosks',
    'uses' => 'RaceGuidesController@paddockActivitiesactivitykiosks'
));

Route::get('raceguide/paddock_club/activities/entertainment', array(
    'as' => 'raceguide.collection.paddockLanding.activities.entertainment',
    'uses' => 'RaceGuidesController@paddockActivitiesentertainment'
));

Route::get('raceguide/paddock_club/activities/paddock-club-lifestyle-stage/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.paddock-club-lifestyle-stage',
    'uses' => 'RaceGuidesController@paddockActivitiesPaddockClubLifestyleStage'
));

Route::get('raceguide/paddock_club/activities/roving-artistes/{app?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities.roving-artistes',
    'uses' => 'RaceGuidesController@paddockActivitiesRovingArtistes'
));

/*Route::get('raceguide/paddock_club/activities/{category?}', array(
    'as' => 'raceguide.collection.paddockLanding.activities',
    'uses' => 'RaceGuidesController@paddockActivities'
));*/

Route::get('raceguide/paddock_club/activities', array(
    'as' => 'raceguide.collection.paddockLanding.activities',
    'uses' => 'RaceGuidesController@paddockActivities'
));


Route::get('raceguide/paddock_club/activities/foodandbeverages/', array(
    'as' => 'raceguide.collection.paddockLanding.activities.fnb',
    'uses' => 'RaceGuidesController@paddockActivitiesFnb'
));

Route::get('raceguide/paddock_club/activities/activity_kiosks', array(
    'as' => 'raceguide.collection.paddockLanding.activities.activitykiosks',
    'uses' => 'RaceGuidesController@paddockActivitiesactivitykiosks'
));

Route::get('raceguide/paddock_club/activities/entertainment', array(
    'as' => 'raceguide.collection.paddockLanding.activities.entertainment',
    'uses' => 'RaceGuidesController@paddockActivitiesentertainment'
));

Route::get('hospitalitysuites', array(
    'as' => 'raceguide.collection.hospitalitysuites',
    'uses' => 'RaceGuidesController@hospitalityLanding'
));

Route::get('raceguide/hospitality-suites-ticketholders', array(
    'as' => 'raceguide.collection.hospitalityLanding',
    'uses' => 'RaceGuidesController@hospitalityLanding'
));

Route::get('raceguide/hospitality-suites-ticketholders/app', array(
    'as' => 'raceguide.collection.hospitalityLandingApp',
    'uses' => 'RaceGuidesController@hospitalityLandingApp'
));


Route::get('raceguide/hospitality-suites-ticketholders/useful_info/{app?}', array(
    'as' => 'raceguide.collection.hospitalityLanding.useful_info',
    'uses' => 'RaceGuidesController@hospitalityUseful'
));

Route::get('raceguide/hospitality-suites-ticketholders/schedules/{app?}', array(
    'as' => 'raceguide.collection.hospitalityLanding.schedules',
    'uses' => 'RaceGuidesController@hospitalitySchedules'
));
Route::get('raceguide/hospitality-suites-ticketholders/directions/{app?}', array(
    'as' => 'raceguide.collection.hospitalityLanding.directions',
    'uses' => 'RaceGuidesController@hospitalityDirections'
));

// Dynamic Guides
Route::get('raceguide/collection/{collectionSlug}', array(
    'as' => 'raceguide.collection',
    'uses' => 'RaceGuidesController@collection'
));

Route::get('raceguide/{guideSlug}', array(
    'as' => 'raceguide.collection.guide',
    'uses' => 'RaceGuidesController@guide'
));

Route::get('raceguide/{guideSlug}/app', array(
    'as' => 'raceguide.collection.guide.app',
    'uses' => 'RaceGuidesController@guideApp'
));

/*
|--------------------------------------------------------------------------
| Miscellaneous Routes
|--------------------------------------------------------------------------
*/

Route::get('job-opportunities', array(
    'as' => 'jobOpportunities',
    'uses' => 'MiscellaneousController@showJobOpportunities'
));

/*Route::get('pitstopatorchard', array(
    'as' => 'pitstopatorchard',
    'uses' => 'MiscellaneousController@showpitstopatorchard'
));*/

Route::get('eventguide/event-guide-2016', array(
    'as' => 'eventguide2016',
    'uses' => 'MiscellaneousController@showEventguide2016'
));

Route::get('eventguide/raceschedule', array(
    'as' => 'raceschedule',
    'uses' => 'MiscellaneousController@showRaceschedule'
));

Route::get('newsletter', array(
    'as' => 'newsletter',
    'uses' => 'MiscellaneousController@showNewsletter'
));
Route::post('newsletter', array(
    'as' => 'newsletter.post',
    'uses' => 'MiscellaneousController@postToNewsletter'
));

Route::get('contact-us', array(
    'as' => 'contactUs',
    'uses' => 'MiscellaneousController@showContactUs'
));

Route::get('tenders', array(
    'as' => 'tenders',
    'uses' => 'MiscellaneousController@showTenders'
));

Route::get('privacy-policy', array(
    'as' => 'privacyPolicy',
    'uses' => 'MiscellaneousController@showPrivacyPolicy'
));

Route::get('haze-report', array(
    'as' => 'haze-report',
    'uses' => 'MiscellaneousController@showHazeReport'
));

Route::get('privacy-policy/app', array(
    'as' => 'privacyPolicyApp',
    'uses' => 'MiscellaneousController@showPrivacyPolicyApp'
));

Route::get('download-file/{path}', array(
    'as' => 'download.file',
    'uses' => 'MiscellaneousController@downloadFile'
));

//Route::get('hospitality',function(){
//    return Redirect::to('/tickets/packages/hospitality',301);
//});

/*
|--------------------------------------------------------------------------
| Orphan Pages
|--------------------------------------------------------------------------
*/

Route::get('gallery/sgp2013-highlights.php', array(
    'as' => 'download.file',
    'uses' => 'OrphanController@show2013highlights'
));

/*
|--------------------------------------------------------------------------
| Surveys
|--------------------------------------------------------------------------
*/

Route::get('survey/paddockclub-survey', array(
    'as' => 'survey.paddockclub-survey',
    'uses' => 'SurveyController@paddockClubSurvey'
));

Route::get('survey/paddockclub-survey/export', array(
    'as' => 'survey.paddockclub-survey.export',
    'before' => 'auth',
    'uses' => 'SurveyController@paddockClubSurveyExport'
));

Route::get('survey/paddockclub-experience-survey', array(
    'as' => 'survey.paddockclub-experience-survey',
    'uses' => 'SurveyController@paddockclubexperiencesurvey'
));

Route::get('survey/paddockclub-experience-survey/export', array(
    'as' => 'survey.paddockclub-experience-survey.export',
    'before' => 'auth',
    'uses' => 'SurveyController@paddockClubExperienceSurveyExport'
));

Route::get('survey/zone4-survey', array(
    'as' => 'survey.zone4-survey',
    'uses' => 'SurveyController@zone4Survey'
));

Route::get('survey/zone4-survey/export', array(
    'as' => 'survey.zone4-survey.export',
    'before' => 'auth',
    'uses' => 'SurveyController@zone4SurveyExport'
));

Route::post('survey/paddockclub-survey', array(
    'as' => 'survey.paddockclub-survey.post',
    'uses' => 'SurveyController@paddockClubSurveyPost'
));

Route::post('survey/paddockclub-experience-survey', array(
    'as' => 'survey.paddockclub-experience-survey.post',
    'uses' => 'SurveyController@paddockClubExperienceSurveyPost'
));
Route::post('survey/zone4-survey', array(
    'as' => 'survey.zone4-survey.post',
    'uses' => 'SurveyController@zone4SurveyPost'
));

Route::get('survey/grandstand-walkabout-survey/export', array(
    'as' => 'survey.grandstand-walkabout-survey.export',
    'before' => 'auth',
    'uses' => 'SurveyController@grandstandsWalkaboutsExport'
));

/*
|--------------------------------------------------------------------------
| Helper Routes
|--------------------------------------------------------------------------
*/
Route::get('ress', 'BaseController@showRESS');

Route::get('logout', 'BaseController@logout');

Route::get('language/{lang}', array(
    'as' => 'language.select',
    'uses' => 'LanguageController@select'
));

/*
|--------------------------------------------------------------------------
| Race Results API
|--------------------------------------------------------------------------
*/
Route::controller('results', 'RaceResultsController');

//});


/*
|--------------------------------------------------------------------------
| New SGP Admin Panel
|--------------------------------------------------------------------------
*/
Route::get('sgp_admin_new', array(
    'as' => 'login',
    'uses' => 'AuthController@showLogin'
));


//Route::post('postLogin', array(
//    'uses' => 'AuthController@postLogin'
//));
//
//Route::post('postRegister', array(
//    'uses' => 'AuthController@postRegister'
//));
//
//Route::get('getLogout', array(
//   'uses' => 'AuthController@getLogout'
//));

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
*/

Route::get('login', array(
    'as' => 'login',
    'uses' => 'AuthController@showLogin'
));

Route::post('login', array(
    'as' => 'login',
    'uses' => 'AuthController@postLogin'
));

Route::get('sitemap.xml', array(
    'as' => 'sitemap',
    'uses' => 'SitemapController@generateSitemap'
));

Route::any('404', array(
    'as' => '404',
    function(){
        return Response::view('layouts.error', array(), 404);
    }
));

App::missing(function($exception)
{
    return Response::view('layouts.error', array(), 404);
});
