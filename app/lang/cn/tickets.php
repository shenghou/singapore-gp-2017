    <?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Tickets Language Lines
    |--------------------------------------------------------------------------
    | '' => '',
    */

    // Level 1
    'home' => array(
        'title' => '于今年九月领略全球独一无二的 F1 夜间赛事，感受惊心动魄的时刻！',
        'desc' => '下列看台门票、走动区门票、自选套票、优等门票及特别礼遇供您随心挑选。',

        'special-desc' => '这些特别定制的自选套票可让观众在三天时间内从街道赛道的不同区域自由观赛。',
        'see-all' => '查看全部',
        'view-more' => '查看更多',
        'book-now' => 'Book now',
        'visit-now' => '访问',
        'browse-tickets' => '浏览门票',
        'all-tickets' => '所有门票',
        'grandstands' => '看台门票',
        'walkabouts' => '走动区门票',
        'combination' => '自选套票',
        'hospitality' => '特别礼遇及优等套票',
        'circuit-map' => '赛场互动地图',
        'ticketing-agents' => '授权票务代售处',
        'hotels-travel' => 'Hotels & Travel Packages',
        'single-day-walkabouts' => '单日走动区',
        'three-day-walkabouts' => '走动区',
    ),

    'listing' => array(

        'fri' => '星期五',
        'sat' => '星期六',
        'sun' => '星期日',
        'auth-agents' => '授权票务代售处',
        'ent-highlights' => '娱乐亮点',
        'tic-category' => '票类',
        'zone-access' => '区域通道',
        'early-bird' => '预售票价 ',
        'early-bird-period' => '(至2016年5月2日)',
        'reg-price' => '普通票价',
//        'reg-period' => '2015年5月 至 比赛周',
        'reg-period' => '(2016年5月3日至赛事周)',
       // 'reg-period-now' => 'Now - Race Week',
        'reg-period-now' => '2016年5月3日至赛事周',
        'super-early-bird' => '2015 Super Early Bird',
        'super-early-bird-price' => '2015 Super Early Bird Price',
        'super-early-bird-period' => 'Now Till 5 Oct 2015',
        'availability' => '余票',
        'faq' => '常见问题',
        'zone' => '区',
        'zones' => '区',
        'buy-now' => '立即购买',
        'sold-out' => '售完',
        'limited-availability' => '仅剩最后几张票',
        'selling-fast' => '仅剩少量票',
        'limited-availability2' => '仅剩最后几张票',
        'view-other' => '查看其票类',
        'view-details' => '查看详情',
        'hospitality' => '了解关于现有礼遇方案的更多信息，请致电+65 6731 5900或发送电邮至<a href="mailto:hospitality@singaporegp.sg">hospitality@singaporegp.sg</a>联系企业销售团队。',
    ),

    'details' => array(

        'price-details' => 'Pricing Details',
        'pano-view' => '全景照',
        'interior-view' => '内景图',
        'related-photos' => '相关照片',
        'other-photos' => '精选图片',
        'seating-plans' => '区域通道 与 座位分布',
        'confirm-title' => 'You are about to buy:',
        'confirm-proceed' => 'Proceed',

    ),

    'fineprint' => array(

        'first' => '如需了解门票销售与通行权条款与使用条件，请点击<a href="http://web2.singaporegp.sg/Uploaded/pdf/2016_T&C.pdf" target="_blank">此处</a>查看.',
        'second' => '团队预订特价 <br/>一次购买四张或以上门票， 即可享受团队预订特价，为您的家人和朋友节省预算。该优惠活动仅限于 Pit、Connaught、Stamford、Esplanade Waterfront、Padang、Bay 热门票 及 Bay 看台。只有在一次购买中完成相同看台的团队预订才能享受该优惠。折扣自动折算并在结账时直接抵扣。团队预订特价仅限于新加坡大奖赛官网（<a href="http://www.singaporegp.sg">www.singaporegp.sg</a>）、票务热线（+65 6738 6738）及所有 SISTIC 售票处。',
        'third' => '报价为新加坡元含商品与消费税（GST）。',
        'fourth' => '查看<a href="http://www.oanda.com/convert/classic">货币转换器</a>. 汇率仅供参考。',

    ),

    'sales' => array(

        'sg-title' => '新加坡授权F1票务合作伙伴',
        'sg-desc' => '所有 SISTIC 门店均有售票。 点击下列清单，查看门店地址。电话预订, 请致电 + 65 6738 6738。',
        'sg' => '新加坡',
        'north' => '北部',
        'central' => '中部',
        'east' => '东部',
        'west' => '西部',

        'intl-title' => '新加坡授权F1票务合作伙伴与经销商',
        'intl-desc' => 'Singapore GP在新加坡境外主要市场已指定授权售票门店与销售商。您可从以下清单中的指定合作伙伴处购买门票或旅行配套。点击下方查看清单',
        'intl' => '国际',

    )

);