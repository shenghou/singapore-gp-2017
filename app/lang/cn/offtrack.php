<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Off Track Language Lines
    |--------------------------------------------------------------------------
    | '' => '',
    */

    // Level 1
    'headliner' => array(

        'title' => '娱乐亮点',
        'desc' => '期待着赛场内特别精选的精彩娱乐阵容<br><br>
                    请定期返回该网站，查阅本页面以了解2016年娱乐阵容的最新信息。
                   <br>艺人阵容和演出时间可能有变。',

        //更多精彩演出即将揭晓。

        'buybutton' => '立即购买',

        'enterguide' => '娱乐指南',
        'watchvideo' => '观看视频',
        'selectcate' => '选择类别',
        'allcate' => '所有类别',
        'headliners' => '领衔表演',
        'music' => '音乐演出',
        'local' => '音乐演出 (本地)',
        'international' => '音乐演出 (国际)',
        'theatricalandcircusacts' => '戏剧与杂技',
        'action' => '动作类演出',
        'roving' => '巡回演出',
        'schedule' => '表演时间表',
        '2015headliners' => '领衔表演',
        'regularly' => '请定期返回该网站，查阅本页面以了解2016年娱乐阵容的最新信息。',
        'performance' => '艺人阵容和演出时间可能有变。',
        'InternationalMusicActs' => '国际音乐表演'
    ),

    'en' => 'EN',
    'cn' => '中文',

);