<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Navigation Language Lines
    |--------------------------------------------------------------------------
    */
 
    // Level 1
    'home' => 'Home',
    'raceguide' => 'Race Guide',
    'ontrack' => 'On-track',
    'offtrack' => 'Off-track',
    'tickets' => 'Tickets',
    'media' => 'Media',
    'fanzone' => 'Fanzone',
    'revUp' => 'Rev Up Singapore!',
    'win' => 'Contests',

    // Level 2
    'ontrack.f1insights' => 'F1 Insights',
    'ontrack.2016Calendar' => '2016 Race Calendar',
    'ontrack.raceschedule' => '2016 Race Schedule',
    'ontrack.seasonResults' => 'Season Results',
    'ontrack.driverStandings' => 'Driver Standings',
    'ontrack.sgpHighlights' => 'Singapore Grand Prix Highlights',
    'ontrack.teamsAndDrivers' => 'Teams and Drivers',
    'ontrack.circuitParkMap' => 'Circuit Park Map',
    'ontrack.gallery' => 'Gallery',
    'ontrack.raceofficials' => 'Race Officials',

    'offtrack.headliners' => 'Entertainment Highlights',
    'offtrack.pastHeadliners' => 'Past Headliners',
    'offtrack.gallery' => 'Gallery',

    'listing.all' => '2017 Tickets',
    'listing.seb' => '2017 Tickets',
    'listing.grandstands' => 'Grandstands',
    'listing.walkabouts' => 'Walkabouts',
    'listing.combinations' => 'Combination Packages',
    'listing.hospitality' => 'Corporate Hospitality',
    'listing.wheelchair' => 'Wheelchair Accessible Platforms',
    'sales.singapore' => 'Authorised SISTIC Outlets in Singapore',
    'sales.international' => 'Authorised International Agents',
    'packages.hotelsAndTravel' => 'Hotels and Travel Packages',
    'packages.hospitality' => 'Hospitality and Executive Packages',

    'media.pressRelease' => 'Press Release',
    'media.mediaSite' => 'Media Site',
    'media.accreditation' => 'Media Accreditation',
    'media.entertainment-accreditation' => 'Entertainment Accreditation',
    'media.chooseEntryForm' => 'Singapore Entry Form',

    'win.autograph' => 'Win Drivers\' Autograph Session Passes',
    'win.mayday' => 'Get up-close to Mayday',
    'win.challenge' => 'Pit Stop Challenge',
    'win.wallpapers' => 'Wallpaper Gallery',

    'en' => 'EN',
    'cn' => '中文',
 
);