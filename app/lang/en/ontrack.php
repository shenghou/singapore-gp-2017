<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | On Track Language Lines
    |--------------------------------------------------------------------------
    | '' => '',
    */

    // Level 1
    'map' => array(

        'title' => 'INTERACTIVE CIRCUIT <br/>PARK MAP',
        'desc' => 'Click on the map below to access the Interactive Circuit Park map or <br/>you can download the PDF version.',

        'download-pdf' => 'Download MAP',
        'cta' => 'Access Interactive <br/> Circuit Park Map',

    ),


);