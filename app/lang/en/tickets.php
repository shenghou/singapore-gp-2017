<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Tickets Language Lines
    |--------------------------------------------------------------------------
    | '' => '',
    */

    // Level 1
    'home' => array(

        'title' => 'GEAR-UP FOR THE WORLD’S ONLY FORMULA ONE<sup>®</sup> NIGHT RACE THIS SEPTEMBER!',
        'desc' => 'Choose from the various Grandstand, Walkabout, Combination Packages, Executive and Hospitality options below.',

        'special-desc' => 'These specially customised Combination Packages enable spectators to experience different parts of the street circuit over the three days.',
        'see-all' => 'See all',
        'view-more' => 'View more',
        'book-now' => 'Book now',
        'visit-now' => 'Visit now',
        'browse-tickets' => 'Browse tickets',
        'all-tickets' => 'All Tickets',
        'grandstands' => 'Grandstands',
        'walkabouts' => 'Walkabouts',
        'combination' => 'Combination Packages',
        'hospitality' => 'Hospitality and Executive Packages',
        'circuit-map' => 'Interactive Map',
        'ticketing-agents' => 'Authorised Ticketing Resellers',
        'hotels-travel' => 'Hotels & Travel Packages',
        'single-day-walkabouts' => 'Single-Day Walkabouts',
        'three-day-walkabouts' => '3-Day Walkabouts'
    ),

    'listing' => array(

        'fri' => 'FRI',
        'sat' => 'SAT',
        'sun' => 'SUN',
        'auth-agents' => 'OUTLETS AND <br/> AUTHORISED RESELLERS',
        'ent-highlights' => 'Entertainment Highlights',
        'tic-category' => 'Ticket Category',
        'zone-access' => 'Zone Access',
        'early-bird' => 'Early Bird Price',
        'early-bird-period' => '(Until 2 May 2016)',
        'reg-price' => 'Regular Price',
//        'reg-period' => 'May 2015 - Race Week',
        'reg-period' => '(From 3 May 2016 to Race Week)',
//        'reg-period-now' => 'Now - Race Week',
        'reg-period-now' => '(3 May 2016 to Race Week)',
        'super-early-bird' => 'Super Early Bird',
        'super-early-bird-price' => '2015 Super Early Bird Price',
        'super-early-bird-period' => 'Now till 3 Oct 2015',
        'availability' => 'Availability',
        'faq' => 'FAQ',
        'zone' => 'Zone',
        'zones' => 'Zones',
        //'buy-now' => 'Buy Now',
        'buy-now' => ' ',
        'sold-out' => 'Sold Out',
        'limited-availability' => 'Last Chance',
        'selling-fast' => 'Selling Fast',
        'limited-availability2' => 'Limited Availability',
        'view-other' => 'View Other Tickets',
        'view-details' => 'View Details',
        'hospitality' => 'To find out more about the Hospitality Packages available, please contact the Corporate Sales team by calling <a href="tel:+6567315900">+65 6731 5900</a> or email <a href="mailto:hospitality@singaporegp.sg">hospitality@singaporegp.sg</a>',

    ),

    'details' => array(

        'price-details' => 'Pricing details',
        'pano-view' => 'Panoramic view',
        'interior-view' => 'Interior view',
        'other-photos' => 'Featured photo',
        'related-photos' => 'Related photos',
        'seating-plans' => 'Seating plans and zone access',
        'confirm-title' => 'You are about to buy:',
        'confirm-proceed' => 'Proceed',

    ),

    'fineprint' => array(

        'first' => 'For Terms and Conditions of Tickets Sale and Entry, click <a href="http://web2.singaporegp.sg/Uploaded/pdf/2016_T&C.pdf" target="_blank">here</a> to view.',
        'second' => 'Group Booking Special <br/>The Group Booking Special offers fantastic savings to family and friends who purchase four or more tickets in a single transaction. This incentive is only available for the Pit, Connaught, Stamford, Esplanade Waterfront, Padang, Bay Grandstands and Bay Grandstand Hot Tix. <br/><br/>To be eligible for this offer, a complete group booking must be made in a single transaction for the same grandstand. Discounts will be auto-calculated and applied upon checkout. This group booking offer is available on the official Singapore GP website (<a href="http://www.singaporegp.sg">www.singaporegp.sg</a>), via the ticketing hotline (+65 6738 6738) and at all SISTIC outlets.',
        'third' => 'Prices quoted are in Singapore Dollars and include Goods and Services Tax (GST).',
        'fourth' => 'View <a href="http://www.oanda.com/convert/classic">Currency Converter</a>. The exchange rates are for reference only.',

    ),

    'sales' => array(

        'sg-title' => 'AUTHORISED SINGAPORE GP INTERNATIONAL F1 TICKETS OUTLETS & RESELLERS',
        'sg-desc' => 'Tickets are available via Online Booking, Phone Booking (+65 6738 6738) and at all SISTIC Outlets. Click on the list below for the various SISTIC locations.',
        'sg' => 'Singapore',
        'north' => 'North',
        'central' => 'Central',
        'east' => 'East',
        'west' => 'West',

        'intl-title' => 'AUTHORISED SINGAPORE GP INTERNATIONAL F1 TICKETS OUTLETS & RESELLERS',
        'intl-desc' => 'For your convenience, you can also purchase official FORMULA 1 SINGAPORE GRAND PRIX tickets and travel packages from
any of Singapore GP’s authorised resellers in key markets outside of Singapore.<br> <br>
Tickets sold via resellers not listed here may potentially be fraudulent. Any ticket found to be obtained from an unauthorised source sold online (such as Carousell, craigslist, eBay, Ticketbis and Viagogo) or outside of the Circuit Park will be made void. If in doubt, check if the reseller you are purchasing from is listed below or write to us at <a href="mailto:tickets@singaporegp.sg" style="color:#0f88fa" title="tickets@singaporegp.sg">tickets@singaporegp.sg</a>.',
        'intl' => 'International',

    )


);