<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | On Track Language Lines
    |--------------------------------------------------------------------------
    | '' => '',
    */

    // Level 1
    'headliner' => array(

        'title' => 'ENTERTAINMENT HIGHLIGHTS',
        'desc' => 'The action off the track hits all new levels of excitement.<br><br>
                    Check back here regularly for the latest
                    announcements on the 2016 Entertainment.
                    Artist line-up and performance times are subject to change.
                    ',

        'buybutton' => 'Buy Tickets',

        'enterguide' => 'ENTERTAINMENT GUIDE',
        'watchvideo' => 'WATCH VIDEO',
        'selectcate' => 'SELECT CATEGORY',
        'allcate' => 'ALL CATEGORIES',
        'music' => 'Music',
        'headliners' => 'Headlining Acts',
        'local' => 'Music (Local)',
        'international' => 'Music (International)',
        //'theatricalandcircusacts' => 'Theatrical and Circus Acts',
        'roving' => 'Roving Acts',
        //'headliners' => 'HEADLINERS',
        'schedule' => 'SCHEDULE',
        '2015headliners' => '2015 Headliners',
        'regularly' => 'Check back here regularly for the latest announcements on the 2016 Entertainment.',
        'performance' => 'Artist line-up and performance times are subject to change.',
        'InternationalMusicActs' => 'International Music Acts'
    ),

    'en' => 'EN',
    'cn' => '中文',


);