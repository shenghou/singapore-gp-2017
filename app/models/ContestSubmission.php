<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class ContestSubmission extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contest_submissions';

	public $timestamps = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('transaction_number', 'first_name', 'last_name', 'email', 'contact_number','latest_news','latest_news_partners','option');

	public function contest()
	{
		return $this->belongsTo('Contest', 'contest_id');
	}
}