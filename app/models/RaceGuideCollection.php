<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class RaceGuideCollection extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'raceguide_collection';

	public $timestamps = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('slug', 'img', 'title', 'sort');

	public function guides()
	{
		return $this->hasMany('RaceGuide', 'collection_id');
	}
}