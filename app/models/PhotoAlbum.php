<?php

class PhotoAlbum extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'photo_album';

	protected $fillable = array();

	public function photos()
	{
		return $this->hasMany('Photo');
	}

	public function getCoverPhotoAttribute()
	{
		$coverPhoto = Photo::whereRaw('photo_album_id = ? and PhotoAlbumCover = ?', array($this->id, 1))->first();
		if (! $coverPhoto) {
			$coverPhoto = Photo::whereRaw('photo_album_id = ?', array($this->id, 1))->first();
		}
		return $coverPhoto;
	}
}