<?php

class Photo extends \Eloquent {
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'photo_gallery';

	public $timestamps = false;

	protected $fillable = array('photo_album_id', 'sort', 'creator', 'image_path_thumb', 'image_path_large');

	public function album()
	{
		return $this->belongsTo('PhotoAlbum', 'photo_album_id');
	}

	public function category()
	{
		return $this->belongsTo('PhotoCategory', 'CategoryID');
	}

	public function getImagePathThumbFilenameAttribute()
	{
		 $temp = explode('/', $this->image_path_thumb);
		 return $temp[count($temp) - 1];
	}

	public function getImagePathLargeFilenameAttribute()
	{
		$temp = explode('/', $this->image_path_large);
		 return $temp[count($temp) - 1];
	}
}