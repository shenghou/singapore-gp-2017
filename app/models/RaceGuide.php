<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class RaceGuide extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'raceguides';

	public $timestamps = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('slug', 'img', 'title', 'left_panel', 'right_panel');

	public function collection()
	{
		return $this->belongsTo('RaceGuideCollection', 'collection_id');
	}
}