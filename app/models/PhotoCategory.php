<?php

class PhotoCategory extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'category_gallery';

	public $timestamps = false;
	// Add your validation rules here
	public static $rules = array();

	// Don't forget to fill this array
	protected $fillable = array('category', 'description', 'slug');

	public function photos()
	{
		return $this->hasMany('Photo', 'CategoryId');
	}
}