<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class RevUpSingapore extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'revupsingapore';

	public $timestamps = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('type', 'title', 'subtitle', 'body', 'slug', 'image', 'hidden', 'sort');

	public function forms()
	{
		return $this->hasMany('RevUpSingaporeForm', 'revup_id');
	}
}