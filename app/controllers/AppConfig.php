<?php

class AppConfig {

    /**
     * @var bool
     * Set `True` if you want to see Two columns of pricing.
     * Set `False` if you want to see only one column of pricing.
     */
    public static $isEarlyBirdOn = false;

    //Super Early Bird Page Display On/Off
    public static $isSuperEarlyBirdOn = true;

    //Homepage backup page On/Off
    public static $isHomePageChange = false;

}