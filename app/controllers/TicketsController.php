<?php

class TicketsController extends BaseController {
    private $minutes = 10;

    private $_regularPhase = 5;
    private $_earlyBirdPhase = 4;

    private $_earlyBirdPriceIds = array(3,9,6,10,25);
    private $_normalPriceIds = array(1,2,3,4,5,6,7,8,9,10);

    private $isRegularPriceNow = true;
    private $phaseId = 4;
    private $selectedTicketForNormalPrice = array();

    private $sebCat = 20;

    function __construct() {
        parent::__construct();
        $this->phaseId = AppConfig::$isEarlyBirdOn ? $this->_earlyBirdPhase : $this->_regularPhase; // 5 for Regular Price and 4 for Early bird tickets
        //$this->selectedTicketForNormalPrice = AppConfig::$isEarlyBirdOn ? $this->_earlyBirdPriceIds : $this->_normalPriceIds;
        $this->selectedTicketForNormalPrice = AppConfig::$isEarlyBirdOn ? $this->_earlyBirdPriceIds : $this->_normalPriceIds;
        $this->isRegularPriceNow = !AppConfig::$isEarlyBirdOn;
    }

    /*
    |--------------------------------------------------------------------------
    | Tickets Controller
    |--------------------------------------------------------------------------
    |
    | Ticketing page
    |
    */

    public function showWelcome() {
        $this->data['angularApp'] = 'ticketsHomeApp';
        return View::make('layouts.tickets.index', $this->data);
    }

    public function showTicketSpecial(){
        return View::make('layouts.tickets.ticketspecials', $this->data);
    }

    public function showListings() {
        return $this->showListingByType('all');
    }

    public function showListingByTypeEN($type = 'all') {
        App::setLocale('en');
        return $this->showListingByType($type, 'en');
    }

    public function showListingByTypeCN($type = 'all') {
        App::setLocale('cn');
        return $this->showListingByType($type, 'cn');
    }

    public function showListingByType($type = 'all', $locale = 'default') {

        $phaseId =  $this->phaseId;

        // get all categories
        $ticketCategories = $this->getTicketCategories();
        $allTicketsByGroup = array();

        // set locale if empty
        if ($locale != 'default') {
            $this->data['locale'] = $locale;
        }

        // loop thru each category and get the tickets
        foreach ($ticketCategories as $category) {

            if ($this->data['locale'] == 'cn') {
                $category->ticketing_category = $category->ticketing_category_chinese;
            }

            // some categories and ticket types are not required to show multi-column for EarlyBird since their prices are same for both regular & earlybird
            if(in_array($category->id,$this->selectedTicketForNormalPrice) ){

                //display early bird price
                //$phaseId = $this->_regularPhase;
//                $phaseId = 4;
                $category->isRegularPriceNow = $this->isRegularPriceNow;
            }else{
                $phaseId = $this->_earlyBirdPhase;
            }

            if ($type == 'all' || $type == $category->id) {
                $category->phaseId = $phaseId;

                $allTicketsByGroup[$category->id] = array();

                $categoryTickets = $this->getTicketsInfoForCategory($category->id);
                // loop thru each ticket and get their prices
                foreach ($categoryTickets as $ticket) {
                    switch ($category->ticketing_type) {
                        case '3-Day Ticket':
                            $table = 'ticketing_three_day';
                            break;
                        case '3-Day Combination Ticket':
                            $table = 'ticketing_three_day_combination';
                            break;
                        case 'Single Day Ticket':
                            $table = 'ticketing_single_day';
                            break;
                    }
                    $phasePrices = $this->getPricesForTicket($table, $ticket->id, 4);
                    $regularPrices = $this->getPricesForTicket($table, $ticket->id, 5);

                    if (empty($phasePrices)) {
                        $phasePrices = $regularPrices;
                    }

                    if (TicketsController::startsWith(strtoupper($ticket->ticket_subcategory), strtoupper("Red Bull Racing"))) {
                        $ticket->ticket_subcategory = "RED BULL RACING <br> TURBO SEATS <br> (PIT GRANDSTAND)";
                    }

                    if (TicketsController::startsWith(strtoupper($ticket->ticket_subcategory), strtoupper("Bay Grandstand Hot"))) {
                        $ticket->ticket_subcategory = "Bay Grandstand <br> Hot Tix";
                    }

                    if (TicketsController::startsWith(strtoupper($ticket->ticket_subcategory), strtoupper("Formula One Paddock"))) {
                        $ticket->ticket_subcategory = "FORMULA ONE <br> PADDOCK CLUB";
                    }

                    if ($this->data['locale'] == 'cn') {
                        $ticket->ticket_subcategory = $ticket->ticket_subcategory_chinese;

                        if (TicketsController::startsWith(strtoupper($ticket->ticket_subcategory), strtoupper("Red Bull Racing"))) {
                            $ticket->ticket_subcategory = "RED BULL RACING <br> TURBO SEATS <br> (PIT 看台)";
                        }

                        if (TicketsController::startsWith(strtoupper($ticket->ticket_subcategory), strtoupper("Formula One Paddock"))) {
                            $ticket->ticket_subcategory = "Formula One <br> Paddock Club <br> 名流包厢";
                        }

                        foreach ($phasePrices as $price) {
                            $price->ticketing_price = $price->ticketing_price_chinese;
                            $price->ticketing_description = $price->ticketing_description_chinese;
                            $price->ticketing_link = $price->ticketing_link_chinese;

                            if ($table == 'ticketing_three_day_combination') {
                                $price->location = $price->location_chinese;
                            }
                        }

                        foreach ($regularPrices as $price) {
                            $price->ticketing_price = $price->ticketing_price_chinese;
                            $price->ticketing_description = $price->ticketing_description_chinese;
                            $price->ticketing_link = $price->ticketing_link_chinese;

                            if ($table == 'ticketing_three_day_combination') {
                                $price->location = $price->location_chinese;
                            }
                        }
                    }

                    if (TicketsController::startsWith($ticket->ticket_subcategory, "Red Bull Racing")) {
                        $ticket->ticket_subcategory = "Red Bull Racing Turbo Seat <br/> (Pit Grandstand)";
                    }

                    // to show 1 column if price is same
                    $ticketPriceMatch = false;

                    if (isset($phasePrices[0])){
                        if($phasePrices[0]->ticketing_price == $regularPrices[0]->ticketing_price)
                            $ticketPriceMatch = true;
                    }

                    $ticket->phaseId = $phaseId;
                    $ticket->ticketType = $table;
                    $ticket->phasePrices = $phasePrices;
                    $ticket->regularPrices = $regularPrices;
                    $ticket->ticketPriceMatch = $ticketPriceMatch; // to show 1 column if price is same

                    $allTicketsByGroup[$category->id][] = $ticket;
                }
            }
        }


        // data
        $this->data['type'] = $type;
        $this->data['ticketCategories'] = $ticketCategories;
        $this->data['allTickets'] = $allTicketsByGroup;
        $this->data['filteredId'] = array(16,17);
        $this->data['listZone'] = $this->getListZone();
        $this->data['priceTableColumn'] = AppConfig::$isEarlyBirdOn ? 2 : 1;
        $this->data['shouldShowEarlyBirdTableTitle'] = AppConfig::$isEarlyBirdOn;
        $this->data['angularApp'] = 'ticketsListingsApp';

        // make view
        return View::make('layouts.tickets.listings', $this->data);
    }

    public function getListZone(){
        $data = array();
        for($i = 1; $i < 5; $i++){
            $zone = $this->getZoneInfo($i);
            $zone = $zone[0];

            if ($this->data['locale'] == 'cn') {
                $zone->zone_title = $zone->zone_title_chinese;
                $zone->zone_writeup = $zone->zone_writeup_chinese;
                $zone->zone_short_writeup = $zone->zone_short_writeup_chinese;
            }
            $data[] = $zone;
        }
        return $data;
    }

    public function showZones($id) {
        $zone = $this->getZoneInfo($id);
        $zone = $zone[0];

        if ($this->data['locale'] == 'cn') {
            $zone->zone_title = $zone->zone_title_chinese;
            $zone->zone_writeup = $zone->zone_writeup_chinese;
            $zone->zone_short_writeup = $zone->zone_short_writeup_chinese;
        }

        $this->data['zone'] = $zone;

        return View::make('layouts.tickets.zone', $this->data);
    }

    public function showDetails() {
        return Redirect::route('tickets');
    }

    /**
     * @param $categoryId - Ticket Category Id
     * @param $infoId - Ticket Info Id
     * @param null $ticketTypeId - If single day type, the id
     * @return \Illuminate\View\View
     */
    public function showDetailsById($categoryId, $infoId, $ticketTypeId = null) {

        $phaseId = $this->phaseId;

        // get all categories
        $ticketCategories = $this->getTicketCategories();
        // loop thru each category and get the tickets
        foreach ($ticketCategories as $category) {
            // some categories and ticket types are not required to show multi-column for EarlyBird since their prices are same for both regular & earlybird
            if( in_array($category->id,$this->selectedTicketForNormalPrice) ){
                //$phaseId = $this->_regularPhase;
//                $phaseId = 4;
                $category->isRegularPriceNow = $this->isRegularPriceNow;
            }else{
                $phaseId = $this->_earlyBirdPhase;
            }

            if ($category->id == $categoryId) {
                $category->phaseId = $phaseId;
                $ticketCategory = $category;
                $categoryTickets = $this->getTicketsInfoForCategory($categoryId);
                // loop thru each ticket and get their prices
                foreach ($categoryTickets as $ticket) {
                    if ($ticket->id == $infoId) {
                        switch ($category->ticketing_type) {
                            case '3-Day Ticket':
                                $table = 'ticketing_three_day';
                                break;
                            case '3-Day Combination Ticket':
                                $table = 'ticketing_three_day_combination';
                                break;
                            case 'Single Day Ticket':
                                $table = 'ticketing_single_day';
                                break;
                        }
                        $phasePrices = $this->getPricesForTicket($table, $ticket->id, 4);
                        $regularPrices = $this->getPricesForTicket($table, $ticket->id, 5);

                        if (empty($phasePrices) || !AppConfig::$isEarlyBirdOn) {
                            $phasePrices = $regularPrices;
                        }

                        if ($this->data['locale'] == 'cn') {
                            $ticket->ticket_subcategory = $ticket->ticket_subcategory_chinese;
                            $ticket->description = $ticket->description_chinese;

                            foreach ($phasePrices as $price) {
                                $price->ticketing_price = $price->ticketing_price_chinese;
                                $price->ticketing_description = $price->ticketing_description_chinese;
                                $price->ticketing_link = $price->ticketing_link_chinese;

                                if ($table == 'ticketing_three_day_combination') {
                                    $price->location = $price->location_chinese;
                                }
                            }

                            foreach ($regularPrices as $price) {
                                $price->ticketing_price = $price->ticketing_price_chinese;
                                $price->ticketing_description = $price->ticketing_description_chinese;
                                $price->ticketing_link = $price->ticketing_link_chinese;

                                if ($table == 'ticketing_three_day_combination') {
                                    $price->location = $price->location_chinese;
                                }
                            }
                        }

                        // to show 1 column if price is same
                        $ticketPriceMatch = false;
                        if($phasePrices[0]->ticketing_price == $regularPrices[0]->ticketing_price)
                            $ticketPriceMatch = true;

                        $ticket->phaseId = $phaseId;
                        $ticket->ticketType = $table;
                        $ticket->phasePrices = $phasePrices;
                        $ticket->regularPrices = $regularPrices;
                        $ticket->relatedPhotos = $this->getPhotosForGallery($ticket->id);
                        $ticket->ticketTypeId = $ticketTypeId;
                        $ticket->mapPath = "http://singaporegp.sg/ticket/map_3d_2014/map_" . $ticket->id . ".jpg";
                        $ticket->ticketPriceMatch = $ticketPriceMatch; // to show 1 column if price is same

                        $ticketDetails = $ticket;
                    }

                    $ticket->ticket_subcategory_raw = $ticket->ticket_subcategory;

                    if (TicketsController::startsWith(strtoupper($ticket->ticket_subcategory), strtoupper("Red Bull Racing")))   {
                        $ticket->ticket_subcategory = "RED BULL RACING <br> TURBO SEATS <br> (PIT GRANDSTAND)";
                    }

                    if (TicketsController::startsWith(strtoupper($ticket->ticket_subcategory), strtoupper("Bay Grandstand Hot"))) {
                        $ticket->ticket_subcategory = "Bay Grandstand <br> Hot Tix";
                    }

                    if (TicketsController::startsWith(strtoupper($ticket->ticket_subcategory), strtoupper("Formula One Paddock"))) {
                        $ticket->ticket_subcategory = "FORMULA ONE <br> PADDOCK CLUB";
                    }

                    if ($this->data['locale'] == 'cn') {
                        $ticket->ticket_subcategory_raw = $ticket->ticket_subcategory_chinese;

                        if (TicketsController::startsWith(strtoupper($ticket->ticket_subcategory), strtoupper("Red Bull Racing"))) {
                            $ticket->ticket_subcategory = "RED BULL RACING <br> TURBO SEATS <br> (PIT 看台)";
                        }

                        if (TicketsController::startsWith(strtoupper($ticket->ticket_subcategory), strtoupper("FORMULA ONE <br> PADDOCK"))) {
                            $ticket->ticket_subcategory = "Formula One <br> Paddock Club <br> 名流包厢";
                        }
                    }
                }
            }
        }

        $this->data['ticketCategories'] = $ticketCategories;
        if (isset($ticketCategory)) {
            $this->data['category'] = $ticketCategory;
        }
        else {
            return Redirect::to('http://www.singaporegp.sg/tickets');
        }
        if (isset($ticketDetails)) {
            $this->data['ticket'] = $ticketDetails;
        }
        else {
            return Redirect::to('http://www.singaporegp.sg/tickets');
        }
        $this->data['listZone'] = $this->getListZone();
        $this->data['filteredId'] = array(16,17);
        $this->data['angularApp'] = 'ticketsDetailsApp';
        $this->data['priceTableColumn'] = AppConfig::$isEarlyBirdOn ? 2 : 1;
        $this->data['shouldShowEarlyBirdTableTitle'] = AppConfig::$isEarlyBirdOn;

        return View::make('layouts.tickets.details', $this->data);
    }


    /**
     * Super Early bird
     */

    public function showListingSEB($type = 1) {

        if(!$this->data['isSuperEarlyBirdOn'])
            $this->show404();

        $phaseId = 4;

        // get all categories
        $ticketCategories = $this->getTicketCategoriesSEB();

        $allTicketsByGroup = array();
        // loop thru each category and get the tickets
        foreach ($ticketCategories as $category) {
            if ($type == 'all' || $type == $category->id) {
                $category->phaseId = $phaseId;
                if ($this->data['locale'] == 'cn') {
                    $category->ticketing_category = $category->ticketing_category_chinese;
                }
                $allTicketsByGroup[$category->id] = array();

                $categoryTickets = $this->getTicketsInfoForCategorySEB($category->id);
                // loop thru each ticket and get their prices
                foreach ($categoryTickets as $ticket) {
                    switch ($category->ticketing_type) {
                        case '3-Day Ticket':
                            $table = 'ticketing_three_day';
                            break;
                        case '3-Day Combination Ticket':
                            $table = 'ticketing_three_day_combination';
                            break;
                        case 'Single Day Ticket':
                            $table = 'ticketing_single_day';
                            break;
                    }
                    $phasePrices = $this->getTicketSEBCategories($ticket->id);
                    $regularPrices = $this->getPricesForTicket($table, $ticket->id, 5);

                    // convert from Array to Object
                    $phasePrices = json_decode(json_encode($phasePrices), FALSE);

                    if ($this->data['locale'] == 'cn') {
                        $ticket->ticket_subcategory = $ticket->ticket_subcategory_chinese;

                        foreach ($phasePrices as $price) {
                            $price->ticketing_price = $price->ticketing_price_chinese;
                            $price->ticketing_description = $price->ticketing_description_chinese;
                            $price->ticketing_link = $price->ticketing_link_chinese;

                            if ($table == 'ticketing_three_day_combination') {
                                $price->location = $price->location_chinese;
                            }
                        }

                        foreach ($regularPrices as $price) {
                            $price->ticketing_price = $price->ticketing_price_chinese;
                            $price->ticketing_description = $price->ticketing_description_chinese;
                            $price->ticketing_link = $price->ticketing_link_chinese;

                            if ($table == 'ticketing_three_day_combination') {
                                $price->location = $price->location_chinese;
                            }
                        }
                    }

                    $ticket->phaseId = $phaseId;
                    $ticket->ticketType = $table;
                    $ticket->phasePrices = $phasePrices;
                    $ticket->regularPrices  = $regularPrices;
                    $ticket->ticketing_category_id = $this->sebCat;

                    $allTicketsByGroup[$category->id][] = $ticket;
                }
            }
        }

        // data
        $this->data['type'] = $type;
        $this->data['ticketCategories'] = $ticketCategories;
        $this->data['allTickets'] = $allTicketsByGroup;
        $this->data['isInSuperEarlyBirdPage'] = true;
        $this->data['angularApp'] = 'ticketsListingsApp';

        // make view
        return View::make('layouts.tickets.listingsSEB', $this->data);
    }

    /*** this is additional ****/
    public function showListingSEB_HC($lang='en') {

        if(!$this->data['isSuperEarlyBirdOn'])
            $this->show404();
		$this->data['angularApp'] = 'ticketsDetailsApp';
		$this->data['ticketId'] = 1;
        $this->data['lang'] = $lang;
        return View::make('layouts.ticketSEB.master_seb_listing', $this->data);
    }
    public function showDetailsByIdSEB_HC($infoId,$lang='en') {

        if(!$this->data['isSuperEarlyBirdOn'])
            $this->show404();
        $this->data['angularApp'] = 'ticketsDetailsApp';
        $this->data['ticketId'] = $infoId;
        $this->data['lang'] = $lang;
        return View::make('layouts.ticketSEB.master_seb', $this->data);
    }
    public function showDetailsByIdSEB($infoId) {

        if(!$this->data['isSuperEarlyBirdOn'])
            $this->show404();

        $phaseId = 7;
        $categoryId = 1;

        // get all categories
        $ticketCategories = $this->getTicketCategoriesSEB();
        $ticketCategoriesFull = $this->getTicketCategories();

        // loop thru each category and get the tickets
        foreach ($ticketCategories as $category) {
            if ($this->data['locale'] == 'cn') {
                $category->ticketing_category = $category->ticketing_category_chinese;
            }

            if ($category->id == $categoryId) {
                $category->phaseId = $phaseId;
                $ticketCategory = $category;
                $categoryTickets = $this->getTicketsInfoForCategorySEB($category->id);
                // loop thru each ticket and get their prices
                foreach ($categoryTickets as $ticket) {
                    if ($ticket->id == $infoId) {
                        switch ($category->ticketing_type) {
                            case '3-Day Ticket':
                                $table = 'ticketing_three_day';
                                break;
                            case '3-Day Combination Ticket':
                                $table = 'ticketing_three_day_combination';
                                break;
                            case 'Single Day Ticket':
                                $table = 'ticketing_single_day';
                                break;
                        }
                        $phasePrices = $this->getTicketSEBCategories($ticket->id);
                        $regularPrices = $this->getPricesForTicket($table, $ticket->id, 5);

                        // convert from Array to Object
                        $phasePrices = json_decode(json_encode($phasePrices), FALSE);

                        if ($this->data['locale'] == 'cn') {
                            $ticket->ticket_subcategory = $ticket->ticket_subcategory_chinese;
                            $ticket->description = $ticket->description_chinese;

                            foreach ($phasePrices as $price) {
                                $price->ticketing_price = $price->ticketing_price_chinese;
                                $price->ticketing_description = $price->ticketing_description_chinese;
                                $price->ticketing_link = $price->ticketing_link_chinese;

                                if ($table == 'ticketing_three_day_combination') {
                                    $price->location = $price->location_chinese;
                                }
                            }

                            foreach ($regularPrices as $price) {
                                $price->ticketing_price = $price->ticketing_price_chinese;
                                $price->ticketing_description = $price->ticketing_description_chinese;
                                $price->ticketing_link = $price->ticketing_link_chinese;

                                if ($table == 'ticketing_three_day_combination') {
                                    $price->location = $price->location_chinese;
                                }
                            }
                        }

                        $ticket->phaseId = $phaseId;
                        $ticket->ticketType = $table;
                        $ticket->phasePrices = $phasePrices;
                        $ticket->regularPrices = $regularPrices;
                        $ticket->relatedPhotos = $this->getPhotosForGallery($ticket->id);
                        $ticket->ticketTypeId = null;
                        $ticket->mapPath = "http://singaporegp.sg/ticket/map_3d_2014/map_" . $ticket->id . ".jpg";

                        $ticket->ticketing_category_id = $this->sebCat;

                        $ticketDetails = $ticket;
                    }
                }
            }
        }

        $this->data['ticketCategories'] = $ticketCategories;
        $this->data['category'] = $ticketCategory;
        $this->data['ticket'] = $ticketDetails;
        $this->data['isInSuperEarlyBirdPage'] = true;
        $this->data['angularApp'] = 'ticketsDetailsApp';

        return View::make('layouts.tickets.details', $this->data);
    }


    public function showSales() {
        return Redirect::route('tickets.sales.singapore');
    }

    public function showSalesSingapore() {
        $allAgents = array(
            'CENTRAL' => $this->getTicketingAgentsSingapore('CENTRAL'),
            'EAST' => $this->getTicketingAgentsSingapore('EAST'),
            'NORTH' => $this->getTicketingAgentsSingapore('NORTH'),
            'WEST' => $this->getTicketingAgentsSingapore('WEST')
        );

        $this->data['regions'] = $allAgents;
        $this->data['angularApp'] = 'sgSalesApp';

        return View::make('layouts.tickets.salessg', $this->data);
    }

    public function showSalesInternational() {
        $regions = $this->getInternationalRegions();
        $allAgents = array();
        $lang = array();

        // loop regions
        foreach ($regions as $region) {
            if ($this->data['locale'] == 'cn') {
                $region->region_label = $region->region_chinese;
            }
            else {
                $region->region_label = $region->region;
            }
            $lang[strtolower($region->region)] = $region->region_label;

            $dat = array(
                'region' => $region,
                'countries' => array()
            );
            $countries = $this->getCountriesForRegion($region->id);

            // loop countries
            foreach ($countries as $country) {
                if ($this->data['locale'] == 'cn') {
                    $country->country_label = $country->country_chinese;
                }
                else {
                    $country->country_label = $country->country;
                }
                $lang[strtolower($country->country)] = $country->country_label;

                $country->agents = $this->getTicketingAgentsForRegionAndCountry($region->id, $country->id);
                $dat['countries'][$country->country] = $country;
            }

            $allAgents[$region->region] = $dat;
        }

        $this->data['regions'] = $allAgents;
        $this->data['langMap'] = json_encode($lang);
        $this->data['angularApp'] = 'intSalesApp';

        return View::make('layouts.tickets.salesi11l', $this->data);
    }

    public function showHotelsAndTravelPackages() {
        $this->data['angularApp'] = 'ticketsHotelsTravelApp';

        return View::make('layouts.tickets.hotelsandtravel', $this->data);
    }

    public function showHospitalityPackages() {
        $this->data['angularApp'] = 'ticketsHospitalityApp';

        return View::make('layouts.tickets.hospitality', $this->data);
    }

    public function showWheelChairPackages(){
        $this->data['angularApp'] = 'ticketsHospitalityApp';

        return View::make('layouts.tickets.hospitality', $this->data);
    }

    public function postToHospitalityPackages() {
        $data = array();
        $rules = array();
        $input = Input::all();

        $rules["name"] = 'required';
        $rules["organisation"] = 'required';
        $rules["contact_number"] = 'required';
        $rules["work_email"] = 'required';

        $validator = Validator::make($input, $rules);

        if ($validator->passes()) {
            $this->insertIntoHospitality($input);
            $contactData = array(
                'name' => $input['name'],
                'organisation' => $input['organisation'],
                'contact_number' => $input['contact_number'],
                'work_email' => $input['work_email']
            );
            $this->sendMail($contactData);
            $data['success'] = true;
        }
        else {
            $data['success'] = false;
            $data['errors'] = $validator->failed();
        }

        return json_encode($data);
    }

    public function showSelfCollection() {
        $this->data['angularApp'] = 'ticketsCollectionApp';
        $this->data['collectionVenues'] = $this->getCollectionVenues();
        return View::make('layouts.tickets.collectionvenues', $this->data);
    }

    public function showSelfCollectionForApp() {
        $this->data['collectionVenues'] = $this->getCollectionVenues();
        return View::make('layouts.tickets.collectionvenuesapp', $this->data);
    }

    public function showMap() {
        return View::make('layouts.tickets.map');
    }

    private function sendMail($data){
        $mail = $data['work_email'];
        Mail::send('emails.tickets.welcome', array(
            'name' => $data['name'],
            'organisation' => $data['organisation'],
            'contact_number' => $data['contact_number'],
            'work_email' => $data['work_email']
        ), function($message){
            $message->to('hospitality@singaporegp.sg', 'Corporate Hospitality Enquiry')->subject('Corporate Hospitality Enquiry');
        });
    }

    /*
    |--------------------------------------------------------------------------
    | DB CALLS
    |--------------------------------------------------------------------------
    */

    ///// tickets /////

    private function getTicketCategories() {
        if ($this->phaseId == 4){
            return DB::table('ticketing_category')
                ->select('id', 'ticketing_category', 'ticketing_category_chinese', 'ticketing_type')
                ->where('status', '=', 'Active')
                ->where('id','!=','9')
                ->orderBy('sort')
                ->get();
        }
        else{
            return DB::table('ticketing_category')
                ->select('id', 'ticketing_category', 'ticketing_category_chinese', 'ticketing_type')
                ->where('status', '=', 'Active')
                ->orderBy('sort')
                ->get();
        }
    }

    private function getTicketsInfoForCategory($id) {
        return DB::table('ticketing_info')
            ->where('ticketing_category_id', '=', $id)
            ->where('ticketing_year', '=', '2014')
            ->where('status', '=', 'Active')
            ->orderBy('sort')
            ->get();
    }

    private function getTicketsInfoForCategorySEB($id) {
        return DB::table('ticketing_info')
            ->where('ticketing_category_id', '=', $id)
            ->where('ticketing_year', '=', '2014')
            ->whereIn('id',array_keys($this->getTicketInfoSEB()))
            ->where('status', '=', 'Active')
            ->orderBy('sort')
            ->get();
    }

    private function getPricesForTicket($table, $id, $phase) {
        return DB::table($table)
            ->where('ticketing_info_id', '=', $id)
            ->where('ticketing_phase_id', '=', $phase)
            ->orderBy('sort')
            ->get();
    }

    private function getPhotosForGallery($id) {
        return DB::table('ticketing_related_photo')
            ->where('ticketing_info_id', '=', $id)
            ->orderBy('sort')
            ->get();
    }

    private function getZoneInfo($id) {
        return DB::table('zone_writeup')
            ->where('id', '=', $id)
            ->get();
    }

    ///// sales channels /////

    //SELECT region,location_name,additional_info,location_address,operating_hours FROM ticketing_agent_singapore WHERE sort <> 999 ORDER BY region, sort;
    private function getTicketingAgentsSingapore($region) {
        return DB::table('ticketing_agent_singapore')
            ->select('location_name', 'additional_info', 'location_address', 'operating_hours')
            ->where('region', '=', $region)
            ->where('sort', '<>', 999)
            ->orderBy('sort')
            ->get();
    }

    //SELECT region,id FROM ticketing_agent_inter_region ORDER BY id
    private function getInternationalRegions() {
        return DB::table('ticketing_agent_inter_region')
            ->orderBy('id')
            ->get();
    }

    //SELECT id,country FROM ticketing_agent_inter_country WHERE region_id=region_id ORDER BY country
    private function getCountriesForRegion($rid) {
        return DB::table('ticketing_agent_inter_country')
            ->where('region_id', '=', $rid)
            ->orderBy('country')
            ->get();
    }

    //SELECT region_id,country_id,agency_name,additional_info,contact_person,contact_no1,contact_no2,email1,email2,website1,website2,website_word1,website_word2,email_word1,email_word2 FROM ticketing_agent_international WHERE region_id=".(int)mysql_real_escape_string($row['id'])." AND country_id=".(int)mysql_real_escape_string($row1['id'])." AND sort!=999 ORDER BY sort
    private function getTicketingAgentsForRegionAndCountry($rid, $cid) {
        return DB::table('ticketing_agent_international')
            ->where('region_id', '=', $rid)
            ->where('country_id', '=', $cid)
            ->where('sort', '<>', 999)
            ->orderBy('agency_name')
            ->get();
    }

    private function insertIntoHospitality($contact) {
        $contactData = array(
            'name' => $contact['name'],
            'organisation' => $contact['organisation'],
            'contact_number' => $contact['contact_number'],
            'work_email' => $contact['work_email']
        );

        DB::connection('corp_brochure')
            ->table('corp_brochure')
            ->insert($contactData);
    }

    private function getCollectionVenues() {
        return CollectionVenue::where('hidden', '=', 0)->orderBy('title')->get();
    }

    /**
     * Super Early Bird
     */
    private function getTicketSEBCategories($ticketId) {
        $tickets = $this->getTicketInfoSEB();
        return isset($tickets[$ticketId]) ? $tickets[$ticketId] : array();
    }

    private function getTicketInfoSEB(){
        $tickets = array(
            1 => array(
                array(
                    "id" => "1",
                    "ticketing_info_id" => "1",
                    "ticketing_phase_id" => "7",
                    "ticketing_group_booking" => "0",
                    "ticketing_price" => "$898",
                    "ticketing_price_chinese" => "$898",
                    "ticketing_description" => "",
                    "ticketing_description_chinese" => "",
                    "ticketing_status            " => "",
                    "ticketing_link" => "https://tickets.singaporegp.sg/F1WebApp/SeatAvailability.do?eventCode=epit180915s&language=en_SG",
                    "ticketing_link_chinese" => "https://tickets.singaporegp.sg/F1WebApp/SeatAvailability.do?eventCode=epit180915s&language=zh_SG",
                    "ticketing_link_garuda" => "",
                    "ticketing_link_hilton" => "",
                    "ticketing_note" => "",
                    "ticketing_note_chinese" => "",
                    "sort" => "",
                    "updater" => "",
                    "updated_date" => "",

                )
            ),
            3 => array(
                array(

                    "id" => "2",
                    "ticketing_info_id" => "3",
                    "ticketing_phase_id" => "7",
                    "ticketing_group_booking" => "0",
                    "ticketing_price" => "$418",
                    "ticketing_price_chinese" => "$418",
                    "ticketing_description" => "",
                    "ticketing_description_chinese" => "",
                    "ticketing_status            " => "",
                    "ticketing_link" => "https://tickets.singaporegp.sg/F1WebApp/SeatAvailability.do?eventCode=epad180915s&language=en_SG",
                    "ticketing_link_chinese" => "https://tickets.singaporegp.sg/F1WebApp/SeatAvailability.do?eventCode=epad180915s&language=zh_SG",
                    "ticketing_link_garuda" => "",
                    "ticketing_link_hilton" => "",
                    "ticketing_note" => "",
                    "ticketing_note_chinese" => "",
                    "sort" => "",
                    "updater" => "",
                    "updated_date" => "",
                )
            ),
            4 => array(
                array(
                    "id" => "3",
                    "ticketing_info_id" => "4",
                    "ticketing_phase_id" => "7",
                    "ticketing_group_booking" => "0",
                    "ticketing_price" => "$228",
                    "ticketing_price_chinese" => "$228",
                    "ticketing_description" => "",
                    "ticketing_description_chinese" => "",
                    "ticketing_status            " => "",
                    "ticketing_link" => "https://tickets.singaporegp.sg/F1WebApp/SeatAvailability.do?eventCode=ebay180915s&language=en_SG",
                    "ticketing_link_chinese" => "https://tickets.singaporegp.sg/F1WebApp/SeatAvailability.do?eventCode=ebay180915s&language=zh_SG",
                    "ticketing_link_garuda" => "",
                    "ticketing_link_hilton" => "",
                    "ticketing_note" => "",
                    "ticketing_note_chinese" => "",
                    "sort" => "",
                    "updater" => "",
                    "updated_date" => "",
                )
            ),
            7 => array(
                array(
                    "id" => "4",
                    "ticketing_info_id" => "7",
                    "ticketing_phase_id" => "7",
                    "ticketing_group_booking" => "0",
                    "ticketing_price" => "$418",
                    "ticketing_price_chinese" => "$418",
                    "ticketing_description" => "",
                    "ticketing_description_chinese" => "",
                    "ticketing_status            " => "",
                    "ticketing_link" => "https://tickets.singaporegp.sg/F1WebApp/SeatAvailability.do?eventCode=estag180915s&language=en_SG",
                    "ticketing_link_chinese" => "https://tickets.singaporegp.sg/F1WebApp/SeatAvailability.do?eventCode=estag180915s&language=zh_SG",
                    "ticketing_link_garuda" => "",
                    "ticketing_link_hilton" => "",
                    "ticketing_note" => "",
                    "ticketing_note_chinese" => "",
                    "sort" => "",
                    "updater" => "",
                    "updated_date" => "",
                )
            )
        );
        return $tickets;
    }

    private function getTicketCategoriesSEB(){
        $arr = array(
            array(
            "id" => "1",
            "ticketing_category" => "SUPER EARLY BIRD 2016",
            "ticketing_category_chinese" => "SUPER EARLY BIRD 2016",
            "ticketing_type" => "3-Day Ticket",
            "phaseId" => 7
        ));
        return json_decode(json_encode($arr)); // need this since the caller is expecting as object
    }

    private function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }
}