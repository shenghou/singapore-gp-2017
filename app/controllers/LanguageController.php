<?php

class LanguageController extends Controller
{
    public function select($lang)
    {
        Session::put('lang', $lang);
        return Redirect::back();
    }
}