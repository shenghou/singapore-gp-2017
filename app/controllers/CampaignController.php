<?php

class CampaignController extends BaseController {

    //Constant variable
    const server_web_service = "http://localhost"; //Host to call web service

    public function contestTerms() {
        return View::make('layouts.campaign.contest.terms', $this->data);
    }

    public function grandDrawTerms() {
        return View::make('layouts.campaign.granddraw.terms', $this->data);
    }

    public function contestTermsRaw() {
        return View::make('layouts.campaign.contest.terms_popout', $this->data);
    }

    public function grandDrawTermsRaw() {
        return View::make('layouts.campaign.granddraw.terms_popout', $this->data);
    }

    public function showWelcome() {
        return View::make('layouts.campaign.index', $this->data);
    }

    public function contest($year, $month) {
        //Fetch countries
        $countries = Countries::getList("name");

        //Make singapore the first option
        $singapore = $countries[702];
        unset($countries[702]);
        array_unshift($countries , $singapore);
        $this->data['countriesList'] = $countries;

        //2016 Contest
        if ($year == 2016) {
            switch ($month) {
                case 2 : {
                    $this->setContestDetails($this->getContest($year, $month));
                    return View::make('layouts.campaign.contest.contest_feb', $this->data);
                }
                case 3 : {
                    $this->setContestDetails($this->getContest($year, $month));
                    return View::make('layouts.campaign.contest.contest_march', $this->data);
                }
                case 4 : {
                    $this->setContestDetails($this->getContest($year, $month));
                    return View::make('layouts.campaign.contest.contest_april', $this->data);
                }
                case 5 : {
                    $this->setContestDetails($this->getContest($year, $month));
                    return View::make('layouts.campaign.contest.contest_may', $this->data);
                }
                case 6 : {
                    $this->setContestDetails($this->getContest($year, $month));
                    return View::make('layouts.campaign.contest.contest_june', $this->data);
                }
                case 7 : {
                    $this->setContestDetails($this->getContest($year, $month));
                    return View::make('layouts.campaign.contest.contest_july', $this->data);
                }
                case 8 : {
                    $this->setContestDetails($this->getContest($year, $month));
                    return View::make('layouts.campaign.contest.contest_august', $this->data);
                }
                default : {
                    Response::view('layouts.error', array(), 404);
                }
            }
        }
        return Response::view('layouts.error', array(), 404);
    }

    private function setContestDetails($contest){
        $this->data['title'] = $contest['results']['contest']['title'];
        $this->data['description'] = $contest['results']['contest']['desc'];
        $this->data['status'] = $contest['results']['contest']['status'];
        $this->data['prize'] = $contest['results']['contest']['prize'];
        $this->data['prize_image_url'] = $contest['results']['contest']['prize_image_url'];
        $this->data['prize_disclaimer'] = $contest['results']['contest']['prize_disclaimer'];
        $this->data['share_title'] = $contest['results']['contest']['share_title'];
        $this->data['share_desc'] = $contest['results']['contest']['share_desc'];
        $this->data['share_url'] = $contest['results']['contest']['share_url'];
        $this->data['contest_end'] = "This checkpoint challenge has ended. <br>Thank you for your participation! <br>Winners will be announced shortly in this page.";
    }

    private function setGrandDrawDetails($contest){
        $this->data['title'] = $contest['results']['grand_draw']['title'];
        $this->data['description'] = $contest['results']['grand_draw']['desc'];
        $this->data['prize_image_url'] = $contest['results']['grand_draw']['thumbnail_url'];
        $this->data['share_title'] = $contest['results']['grand_draw']['share_title'];
        $this->data['share_desc'] = $contest['results']['grand_draw']['share_desc'];
        $this->data['share_url'] = $contest['results']['grand_draw']['share_url'];
    }

    public function contestPost()
    {
        //Get input
        $data = Input::all();

        //Validate contest answer
        $data['contest_data_list'] = $this->validContestAnswer($data['contest_month'], $data['contest_data_list']);

        //Call Submit API web service
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, CampaignController::server_web_service . "/mobile-api-v2/index.php/campaign/submitContest");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);

        //Convert JSON response to array
        $array = json_decode(trim($output), TRUE);

        if (!empty($array['error'])) {
            return "An error has occur while submiting contest. Error message: " . $array['error_message'];
        }

        if (empty($array['success'])) {
            return "An error has occur. This are the message <br/>" . json_encode($array);
        }

        //Add user to subscription list if checked
        if (!empty($data['receive_newsletter'])) {
            $this->insertNewsletter($data);
        }
        elseif (!empty($data['receive_newsletter_from_partner'])) {
            $this->insertNewsletter($data);
        }

        //Generate reponse screen
        $return = array(
            'success' => 1,
            'email' => $data['email'],
        );
        return Redirect::route('campaign.contest.submit', $return)->with('success', true)->with('contest_result', $data['contest_data_list']);
    }

    public function contestSubmit() {
        return View::make('layouts.campaign.contest.submit_success', $this->data);
    }

    public function hspContest() {
        return View::make('layouts.campaign.contest.hsp_contest_answers');
    }

    public function contestLanding($year, $month) {
        //2016 Contest
        if ($year == 2016) {
            switch ($month) {
                case 2 :
                case 3 :
                case 4 :
                case 5 :
                case 6 :
                case 7 :
                case 8 : {
                    $this->setContestDetails($this->getContest($year, $month));
                    break;
                }
                default : {
                    Response::view('layouts.error', array(), 404);
                }
            }

            return View::make('layouts.campaign.index', $this->data);
        }
        return Response::view('layouts.error', array(), 404);
    }

    public function grandDrawLanding($year, $month) {
        //2016 Contest Grandraw
        if ($year == 2016) {
            switch ($month) {
                case 3 :
                case 4 :
                case 5 :
                case 6 :
                case 7 :
                case 8 : {
                    $this->setGrandDrawDetails($this->getGrandDraw($year, $month));
                    break;
                }
                default : {
                    Response::view('layouts.error', array(), 404);
                }
            }

            return View::make('layouts.campaign.index', $this->data);
        }
        return Response::view('layouts.error', array(), 404);
    }

    public function howToParticipateContest() {
        return View::make('layouts.campaign.contest.how_to_participate', $this->data);
    }

    public function howToParticipateGrandDraw() {
        return View::make('layouts.campaign.granddraw.how_to_participate', $this->data);
    }

    public function subscribeNewsletter() {
        $data = Input::all();
        $data['receive_newsletter_from_partner'] = $data['subscribe_newsletter_partner'];
        $this->insertNewsletter($data);
    }

    private function insertNewsletter($newsletter) {
        $newsletterData = array(
            'txtName' => $newsletter['name_first'] . ' ' . $newsletter['name_last'],
            'txtEmail' => $newsletter['email'],
            'sYear_of_Birth' => '',
            'sCountry' => $newsletter['country'],
            'attend_which_year' => isset($newsletter['attesond_which_year']) ? implode(',', $newsletter['attend_which_year']) : '',
            'with_who' => isset($newsletter['with_who']) ? implode(',', $newsletter['with_who']) : '',
            'with_who_other' => '',
            'looking_forward' => isset($newsletter['looking_forward']) ? implode(',', $newsletter['looking_forward']) : '',
            'looking_forward_other' => '',
            'receive_from_partner' => empty($newsletter['receive_newsletter_from_partner']) ? 0 : 1
        );

        DB::connection('newsletter')
            ->table('newsletter')
            ->insert($newsletterData);
    }

    private function getContest($year, $month) {
        //Call Submit API web service
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, CampaignController::server_web_service . "/mobile-api-v2/index.php/campaign/contest/" . $year . "/" . $month);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        //Return
        return $array = json_decode(trim($output), TRUE);
    }

    private function getGrandDraw($year, $month) {
        //Call Submit API web service
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, CampaignController::server_web_service . "/mobile-api-v2/index.php/campaign/grandDraw/" . $year . "/" . $month);
        //        curl_setopt($ch, CURLOPT_URL, "localhost/mobile-api-v2/index.php/campaign/grandDraw/" . $year . "/" . $month);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        //Return
        return $array = json_decode(trim($output), TRUE);
    }

    //2016 Contest
    private function validContestAnswer($contest_month, $contest_data_list){

        switch ($contest_month) {
            //Feb Contest
            case 2 : {
                $contest_data_list[0]['status'] = ($contest_data_list[0]['data'] == 'C') ? true : false;
                $contest_data_list[1]['status'] = ($contest_data_list[1]['data'] == 'D') ? true : false;
                $contest_data_list[2]['status'] = ($contest_data_list[2]['data'] == 'A') ? true : false;
                $contest_data_list[3]['status'] = ($contest_data_list[3]['data'] == 'B') ? true : false;
                $contest_data_list[4]['status'] = ($contest_data_list[4]['data'] == 'C') ? true : false;
                break;
            }
            //March Contest
            case 3 : {
                $contest_data_list[0]['status'] = ($contest_data_list[0]['data'] == 'C') ? true : false;
                $contest_data_list[1]['status'] = ($contest_data_list[1]['data'] == 'D') ? true : false;
                $contest_data_list[2]['status'] = ($contest_data_list[2]['data'] == 'A') ? true : false;
                $contest_data_list[3]['status'] = ($contest_data_list[3]['data'] == 'B') ? true : false;
                $contest_data_list[4]['status'] = ($contest_data_list[4]['data'] == 'A') ? true : false;
                break;
            }
            //May Contest
            case 5 : {
                $contest_data_list[0]['status'] = ($contest_data_list[0]['data'] == 'C') ? true : false;
                $contest_data_list[1]['status'] = ($contest_data_list[1]['data'] == 'D') ? true : false;
                $contest_data_list[2]['status'] = ($contest_data_list[2]['data'] == 'D') ? true : false;
                $contest_data_list[3]['status'] = ($contest_data_list[3]['data'] == 'C') ? true : false;
                break;
            }
            //June Contest
            case 6 : {
                $contest_data_list[0]['status'] = false;
                $contest_data_list[1]['status'] = false;
                $contest_data_list[2]['status'] = false;
                break;
            }
            //August Contest
            case 8 : {
                $contest_data_list[0]['status'] = false;
                $contest_data_list[1]['status'] = false;
                $contest_data_list[2]['status'] = false;
                $contest_data_list[3]['status'] = false;
                $contest_data_list[4]['status'] = false;
                break;
            }
        }
        return $contest_data_list;
    }
}