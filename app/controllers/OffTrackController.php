<?php

class OffTrackController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Off-Track Controller
    |--------------------------------------------------------------------------
    |
    | Off-Track page and its sub-pages
    |
    */
    public function showWelcome() {
        return Redirect::route('offtrack.headliners');
    }


    public function showHeadliners()
    {

        return Redirect::route('offtrack.pastHeadliners'); //added by shenghou

        $this->data['angularApp'] = 'offtrackHeadlinersApp';
        $headliners = $this->modifyHeadlinerData($this->getCurrentHeadliners());
        $hlCat = array('HEADLINING ACTS', 'MUSIC (LOCAL)', 'MUSIC (INTERNATIONAL)', 'ROVING ACTS');
//
//        $hls = $this->modifyHeadlinerData($this->getCurrentHeadliners());
//        $headlinerCategories = array();
//        $headliners = array();

//        foreach ($hlCat as $hlC) {
//
//            $aHeadlinerCategory = $hlC->performance_category;
//            $headlinerCategories[] = $aHeadlinerCategory;
////            $cat = $hlC->performance_category;
//
//
////            $this->data['sections'][] = array(
////                'title' => '2014',
////                'subtitle' => $aHeadlinerCategory,
////                'records' => array_merge($this->modifyHeadlinerData($this->getCurrentHeadlinersByCategory($aHeadlinerCategory)))
////            );
//
//
//        }


        // $this->data['featured'] = clone $headliners[0];
        // unset($headliners[0]);
        $this->data['sections'][] = array(
            'title' => '2016',
            'tag' => 'HEADLINING ACTS',
            'subtitle' => 'HEADLINING ACTS',
            'records' => array_merge($this->modifyHeadlinerData($this->getCurrentHeadlinersByCategory('HEADLINING ACTS')))
            //'records' => $headliners
        );
        $this->data['sections'][] = array(
            'title' => '2016',
            'tag' => 'MUSIC (LOCAL)',
            'subtitle' => 'MUSIC (LOCAL)',
            'records' => array_merge($this->modifyHeadlinerData($this->getCurrentHeadlinersByCategory('MUSIC (LOCAL)')))
        );

        $this->data['sections'][] = array(
            'title' => '2016',
            'tag' => 'MUSIC (INTERNATIONAL)',
            'subtitle' => 'MUSIC (INTERNATIONAL)',
            'records' => array_merge($this->modifyHeadlinerData($this->getCurrentHeadlinersByCategory('MUSIC (INTERNATIONAL)')))
        );
        $this->data['sections'][] = array(
            'title' => '2016',
            'tag' => 'ROVING ACTS',
            'subtitle' => 'ROVING ACTS',
            'records' => array_merge($this->modifyHeadlinerData($this->getCurrentHeadlinersByCategory('ROVING ACTS')))
        );

        $this->data['listofcategories'] = $hlCat;
//        $this->data['headliners'] = $headliners;


        return View::make('layouts.offtrack.headliners', $this->data);

    }
    private function getHeadlinerCategories() {

        return DB::table('entertainment_highlights')
            ->select('performance_category')
            ->groupBy('performance_category')
            ->orderBy('performance_category', 'desc')
            ->get();


    }

    public function showPastHeadliners() {
        $result = $this->getPastHeadliners();
        $headliners = array();

        foreach ($result as $headliner) {
            $year = ($headliner->the_year) ? $headliner->the_year : '2015';
            $key = 'y' . $year;
            if (!isset($headliners[$key])) {
                $headliners[$key] = array();
            }

            // split year... wth
            $artistNameParts = explode(':', $headliner->artist);
            if (count($artistNameParts) == 2) {
                $headliner->artist = $artistNameParts[1];
            }

            // split name & country... sigh
            $start = strpos($headliner->artist, '(') + 1;
            $end = strpos($headliner->artist, ')');

            $headliner->country = substr($headliner->artist, $start, $end - $start);
            $headliner->artist = substr($headliner->artist, 0, $start - 1);

            $headliners[$key][] = $headliner;
        }
        krsort($headliners);

        $this->data['angularApp'] = 'offtrackHeadlinersApp';
        $this->data['headliners'] = $headliners;

        return View::make('layouts.offtrack.pastheadliners', $this->data);
    }

    public function showHeadlinerDetails($headliner = null) {
        if (!$headliner) {
            return Redirect::route('offtrack.headliners');
        }

        $artist = $this->getHeadlinerDetails($headliner);
        // $artist = $artist[0];

        if (isset($artist[0])) {

            if ($artist[0]->status != 'Active') {
                return Redirect::route('offtrack.headliners');
            }


            // split year... wth
            $artistNameParts = explode(':', $artist[0]->artist);
            if (count($artistNameParts) == 2) {
                $artist[0]->artist = $artistNameParts[1];
            }

            $gallery = $this->getHeadlinerGallery($artist[0]->entertainment_id);

            $this->data['artist'] = $artist;
            $this->data['gallery'] = $gallery;

            $this->data['angularApp'] = 'offtrackHeadlinersApp';
            return View::make('layouts.offtrack.headlinerdetails', $this->data);
        }
    }


    public function showGallery($year = 2015, $type = 'photos', $cats = '5,6') {
        $gallery = array();

        // years
        $yearObjs = $this->getPhotoGalleryYears();
        $years = array();
        foreach ($yearObjs as $yr) {
            $years[] = $yr->year;
        }

        if (!is_numeric($year) || !in_array($year, $years)) {
            return Redirect::route('offtrack.gallery');
        }

        if ($type == 'photos') {
            $cats = explode(',', $cats);
            $photos = $this->getPhotoGallery($year, $cats);

            foreach ($photos as $photo) {
                $key = 'y' . $photo->year;

                // year
                if (!isset($gallery[$key])) {
                    $gallery[$key] = array();
                }

                // date
                if (!isset($gallery[$key][$photo->Date_variable])) {
                    $gallery[$key][$photo->Date_variable] = array();
                }

                // pathing
                $photo->image_path_thumb = "http://web2.singaporegp.sg/sgp_admin/Uploaded/" . $photo->image_path_thumb;
                $photo->image_path_large = "http://web2.singaporegp.sg/sgp_admin/Uploaded/" . $photo->image_path_large;

                $gallery[$key][$photo->Date_variable][] = $photo;
            }
        }
        else if ($type == 'videos') {
            $gallery = $this->getVideoGallery();
        }
        else {
            return Redirect::route('offtrack.gallery');
        }

        $this->data['angularApp'] = 'galleryApp';
        $this->data['type'] = $type;
        $this->data['years'] = $years;
        $this->data['gallery'] = $gallery;

        return View::make('layouts.offtrack.gallery', $this->data);
    }

    public function showVideo($id) {
        $video = $this->getVideo($id);
        $video = $video[0];

        if (!empty($video->video_youtube_link)) {
            $urlParts = parse_url($video->video_youtube_link);
            parse_str($urlParts['query'], $queryString);
            $video->yt_id = $queryString['v'];
        }

        $this->data['item'] = $video;

        return View::make('layouts.jwplayer', $this->data);
    }

    /*
    |--------------------------------------------------------------------------
    | DB CALLS
    |--------------------------------------------------------------------------
    */

    private function getCurrentHeadliners() {
        $if = 'if(eh.performance_category like "%Performances on Stage%", 1,'
            .'	if (eh.performance_category like "%Roving Performances%", 2, 3)'
            .')';

        return DB::table('entertainment_highlights AS eh')
            ->leftJoin('entertainment_highlights_detail AS ehd', 'eh.id', '=', 'ehd.entertainment_id')
            ->select('eh.id', 'eh.artist', 'eh.additional_label','eh.image_thumb', 'eh.image', 'eh.url', DB::raw("GROUP_CONCAT(DISTINCT ehd.date SEPARATOR ', ') AS date"), 'ehd.time', 'ehd.zone', 'ehd.zone_link', 'ehd.zone_list')
            ->where('eh.year', '=', 'Current')
            ->where('eh.status', '=', 'Active')
            ->where(function ($query)
            {
                $query->where('eh.performance_category', 'like', '%Roving Performances%')
                    ->orWhere('eh.performance_category', 'like', '%Performances on Stage%');
            })
            ->groupBy('eh.id')
            ->orderByRaw($if)
            ->orderBy('eh.sort')
            ->get();
    }

    private function getCurrentHeadlinersByCategory($performance_category) {
        $qb = DB::table('entertainment_highlights AS eh')
            ->leftJoin('entertainment_highlights_detail AS ehd', 'eh.id', '=', 'ehd.entertainment_id')
            //->select('eh.id', 'eh.artist', 'eh.additional_label', 'eh.image_thumb', 'eh.image', 'eh.url', DB::raw("GROUP_CONCAT(DISTINCT ehd.date SEPARATOR ', ') AS date"), 'ehd.time', 'ehd.zone', 'ehd.zone_link', 'ehd.zone_list')
            ->select('eh.id', 'eh.artist', 'eh.additional_label', 'eh.image_thumb', 'eh.image', 'eh.url', DB::raw("GROUP_CONCAT(ehd.date ORDER BY ehd.date ASC SEPARATOR ', ') AS date"), 'ehd.time', 'ehd.zone', 'ehd.zone_link', 'ehd.zone_list')
            ->where('eh.year', '=', 'Current')
            ->where('eh.status', '=', 'Active')
            ->where('eh.performance_category', '=', $performance_category);

        return $qb->groupBy('eh.id')
            ->orderBy('eh.sort')
            ->get();
    }

    // sql statement from old php file
    private function getPastHeadliners() {
        return DB::table('entertainment_highlights AS eh')
            ->select('eh.id', 'eh.artist', 'eh.additional_label', 'eh.image_thumb', 'eh.image', 'eh.url', 'eh.the_year')
            ->orWhere(function($query) {
                $query->whereNotNull('eh.the_year');
                $query->where('eh.the_year', '!=', '2016');
            })
            ->orWhere(function($query) {
                $query->where('eh.year', '=', 'Past');
                $query->where('eh.status', '=', 'Active');
                $query->where('eh.the_year', '=', '2016');
                /*$query->where(function ($query) {
                    $query->where('eh.performance_category', 'like', '%Roving Performances%')
                        ->orWhere('eh.performance_category', 'like', '%Performances on Stage%');
                });*/
            })
            ->orderBy('eh.sort')
            ->get();
    }

    private function modifyHeadlinerData($result) {
        $headliners = array();
        if (count($result) > 0) {
            foreach ($result as $headliner) {
                $pos = strpos($headliner->artist, 'coming soon');
                if ($pos === false && $pos >= 0) {
                    // split name & country... sigh
                    $start = strpos($headliner->artist, '(') + 1;
                    $end = strpos($headliner->artist, ')');

                    $headliner->country = substr($headliner->artist, $start, $end - $start);
                    $headliner->artist = substr($headliner->artist, 0, $start - 1);
                    $headliner->zone_list = $headliner->zone_list == NULL ? array() : explode(',',$headliner->zone_list);
                }

                $headliners[] = $headliner;
            }
        }

        return $headliners;
    }


    private function getHeadlinerDetails($url) {
        return DB::table('entertainment_highlights AS eh')
            ->leftJoin('entertainment_highlights_detail AS ehd', 'eh.id', '=', 'ehd.entertainment_id')
            // ->select('eh.*', DB::raw("GROUP_CONCAT(DISTINCT ehd.date SEPARATOR ', ') AS date"), 'ehd.time', 'ehd.zone', 'ehd.zone_link')
            ->where('eh.url', '=', $url)
            ->orderBy('ehd.date', 'asc')
            ->orderBy('ehd.time', 'asc')
            ->get();
    }

    private function getHeadlinerGallery($id) {
        return DB::table('entertainment_highlights_gallery')
            ->where('entertainment_id', '=', $id)
            ->get();
    }

    private function getPhotoGalleryYears() {
        return DB::table('photo_album')
            ->select('year')
            ->groupBy('year')
            ->orderBy('year', 'desc')
            ->get();
    }

    private function getPhotoGallery($year, $cats) {
        return DB::table('photo_gallery AS pg')
            ->leftJoin('photo_album AS pa', 'pg.photo_album_id', '=', 'pa.id')
            ->select('pg.*', 'pa.Date_variable', 'pa.year')
            ->whereIn('pg.CategoryID', $cats)
            ->where('pa.year', '=', $year)
            ->orderBy('pg.dated', 'desc')
            ->get();
    }

    private function getVideoGallery() {
        return DB::table('video_gallery_mp4')
            ->select('thumbnail_link', 'video_flv_link', 'title', 'video_link', 'id')
            ->orderBy('sort')
            ->get();
    }

    private function getVideo($id) {
        return DB::table('video_gallery_mp4')
            ->select('thumbnail_link', 'video_flv_link', 'title', 'video_link', 'video_ogg_link', 'id', 'video_youtube_link')
            ->where('id', '=', $id)
            ->get();
    }
}
