<?php

class MediaController extends BaseController {
    const server = "http://www.singaporegp.sg"; //Host name

    public function showWelcome() {
        return Redirect::route('media.pressRelease');
    }

    public function showPressRelease() {
        // massage data
        $prYears = $this->getPressReleaseYears();
        $prs = $this->getPressReleaseTitles();
        $pressReleaseYears = array();
        $pressReleaseTitles = array();

        foreach ($prYears as $prYear) {
            $pressReleaseYears[] = $prYear->press_release_year;
            $idx = 'y' . $prYear->press_release_year;
            $pressReleaseTitles[$idx] = array();

            foreach ($prs as $pr) {
                $pr->title = strip_tags($pr->title);
                if ($pr->press_release_year == $prYear->press_release_year) {
                    $pressReleaseTitles[$idx][] = $pr;
                }
            }
        }

        // pass to view
        $this->data['angularApp'] = 'mediaPressReleaseApp';
        $this->data['selectYears'] = $pressReleaseYears;
        $this->data['pressReleases'] = $pressReleaseTitles;

        return View::make('layouts.media.pressrelease', $this->data);
    }

    public function showPressReleaseById($id) {
        // massage data
        $prYears = $this->getPressReleaseYears();
        $pressReleaseYears = array();

        foreach ($prYears as $prYear) {
            $pressReleaseYears[] = $prYear->press_release_year;
        }

        $pr = $this->getPressRelease($id);

        $this->data['angularApp'] = 'mediaPressReleaseApp';
        $this->data['selectYears'] = $pressReleaseYears;
        $this->data['pressRelease'] = $pr[0];

        return View::make('layouts.media.pressreleasedetails', $this->data);
    }


    public function showMediaSite() {
        return Redirect::to('http://media.singaporegp.sg');
    }


    public function showAccreditation() {

        $this->data['angularApp'] = 'revupapp';
        return View::make('layouts.media.accreditation', $this->data);
    }

    public function showEntertainmentAccreditation() {

        $this->data['angularApp'] = 'revupapp';
        return View::make('layouts.media.entertainment-accreditation', $this->data);
    }

    public function showChooseEntryForm() {

        $this->data['angularApp'] = 'revupapp';

        return View::make('layouts.media.chooseentryform', $this->data);
    }

    public function showEntryForm($formType) {
        if ($formType != 'journalist' && $formType != 'photographer-tvcrew') {
            return Redirect::route('media.chooseEntryForm');
        }

        $this->data['angularApp'] = 'mediaEntryFormApp';
        $this->data['formType'] = $formType;
        $this->data['countriesList'] = Countries::getList();

        return View::make('layouts.media.entryform', $this->data);
    }

    public function showMediaAccreditationForm($formType) {

        $this->data['formType'] = $formType;
        //$this->data['angularApp'] = 'mediaEntryFormApp';

        if ($formType == 'fia') {
            $this->data['countriesList'] = Countries::getList();
            return View::make('layouts.media.fia-form', $this->data);
        }else if($formType == 'entertainment'){
            //echo 'abc';
            $concert1 = new MediaController();
            $concert1->value = 'day1_kylie';
            $concert1->text = 'DAY 1 FRIDAY Kylie Minogue <br/>(after last track activity)';

            $concert2 = new MediaController();
            $concert2->value = 'day2_kc_sunshine';
            $concert2->text = 'DAY 2 SATURDAY KC and The Sunshine Band (Pre Formula 1<sup>&reg;</sup> Qualifying)';

            $concert3 = new MediaController();
            $concert3->value = 'day2_bastille';
            $concert3->text = 'DAY 2 SATURDAY Bastille <br/>(Pre Formula 1<sup>&reg;</sup> Qualifying)';

            $concert4 = new MediaController();
            $concert4->value = 'day2_queen_adam_lambert';
            $concert4->text = 'DAY 2 SATURDAY Queen + Adam Lambert <br/>(after last track activity)';

            $concert5 = new MediaController();
            $concert5->value = 'day3_pentatonix';
            $concert5->text = 'DAY 3 SUNDAY Pentatonix <br/>(Pre Formula 1<sup>&reg;</sup> Race)';

            $concert6 = new MediaController();
            $concert6->value = 'day3_halsey';
            $concert6->text = 'DAY 3 SUNDAY Halsey <br/>(Pre Formula 1<sup>&reg;</sup> Race)';

            $concert7 = new MediaController();
            $concert7->value = 'day3_inmagine_dragons';
            $concert7->text = 'DAY 3 SUNDAY Imagine Dragons <br/>(after last track activity)';


            $concerts = array($concert1, $concert2, $concert3, $concert4, $concert5, $concert6, $concert7);

            $this->data['concertList'] = $concerts;
            $this->data['countriesList'] = Countries::getList();

                /*<select name="event_type" data-ng-model="person.event_type">
                                    <option value="" disabled>Events</option>
                                    <option value="day1_kylie">DAY 1 FRIDAY Kylie</option>
                                    <option value="day1_kc_sunshine">DAY 1 FRIDAY KC and The Sunshine Band</option>
                                    <option value="day2_bastille">DAY 2 SATURDAY Bastille </option>
                                    <option value="day2_pentatonix">DAY 2 SATURDAY Pentatonix </option>
                                    <option value="day2_queen_adam_lambert">DAY 2 SATURDAY Queen + Adam Lambert </option>
                                    <option value="day3_halsey">DAY 3 SUNDAY Halsey  </option>
                                    <option value="day3_inmagine_dragons">DAY 3 SUNDAY Imagine Dragons </option>
                                </select>*/
            return View::make('layouts.media.entertainment-form', $this->data);
        }
    }

    public function postAccreditationFiaForm() {


        try {
            $data = array();
            $input = Input::all();
            $foldername = 'FIA';

            $passport_photo_64 = Input::get('passport_photo_64');
            $input['passport_photo'] = $this->uploadFile($input['passport_photo'], $passport_photo_64, '2016Fia-PP-', $foldername);

            $press_card_64 = Input::get('press_card_64');
            $input['press_card'] = $this->uploadFile($input['press_card'], $press_card_64, '2016Fia-PC-', $foldername);

            $supporting_document_64 = Input::get('supporting_document_64');
            $input['supporting_document'] = $this->uploadFile($input['supporting_document'], $supporting_document_64, '2016Fia-SD-', $foldername);

            //echo Input::get('print_media_listenership');
            if(Input::get('media_type') == 'print_media') {
                //print media
                $print_media_publication_64 = Input::get('print_media_publication_64');
                $input['print_media_publication'] = $this->uploadFile($input['print_media_publication'], $print_media_publication_64, '2016Fia-PMP-', $foldername);

                $print_media_coverage_64 = Input::get('print_media_coverage_64');
                $input['print_media_coverage'] = $this->uploadFile($input['print_media_coverage'], $print_media_coverage_64, '2016Fia-PMC-', $foldername);

                $print_media_pre_coverage_64 = Input::get('print_media_pre_coverage_64');
                $input['print_media_pre_coverage'] = $this->uploadFile($input['print_media_pre_coverage'], $print_media_pre_coverage_64, '2016Fia-PMPC-', $foldername);

                $personData = array(
                    'representative_name' => Input::get('representative_name'),
                    'representative_designation' => Input::get('representative_designation'),
                    'representative_phone' => Input::get('representative_phone'),
                    'representative_fax' => Input::get('representative_fax'),
                    'representative_email' => Input::get('representative_email'),
                    'passport_photo' => $input['passport_photo'],
                    'press_card' => $input['press_card'],
                    'publication_name' => Input::get('publication_name'),
                    'country' => Input::get('country'),
                    'editor_name' => Input::get('editor_name'),
                    'editor_phone' => Input::get('editor_phone'),
                    'editor_fax' => Input::get('editor_fax'),
                    'editor_email' => Input::get('editor_email'),
                    'supporting_document' => $input['supporting_document'],
                    'media_type' => 'Print Media',
                    'print_media_circulation' => Input::get('print_media_circulation'),
                    'print_media_listenership' => Input::get('print_media_listenership'),
                    'print_media_frequency' => Input::get('print_media_frequency'),
                    'print_media_publication' => $input['print_media_publication'],
                    'print_media_coverage' => $input['print_media_coverage'],
                    'print_media_pre_coverage' => $input['print_media_pre_coverage'],
                    'created_at' => date('Y-m-d H:i:s'),
                );
            }else if(Input::get('media_type') == 'radio_station') {
                //radio station
                $radio_station_audio_event_64 = Input::get('radio_station_audio_event_64');
                $input['radio_station_audio_event'] = $this->uploadFile($input['radio_station_audio_event'], $radio_station_audio_event_64, '2016Fia-RSAE-', $foldername);

                $radio_station_audio_pre_event_64 = Input::get('radio_station_audio_pre_event_64');
                $input['radio_station_audio_pre_event'] = $this->uploadFile($input['radio_station_audio_pre_event'], $radio_station_audio_pre_event_64, '2016Fia-RSAPE-', $foldername);

                $personData = array(


                    'representative_name' => Input::get('representative_name'),
                    'representative_designation' => Input::get('representative_designation'),
                    'representative_phone' => Input::get('representative_phone'),
                    'representative_fax' => Input::get('representative_fax'),
                    'representative_email' => Input::get('representative_email'),
                    'passport_photo' => $input['passport_photo'],
                    'press_card' => $input['press_card'],
                    'publication_name' => Input::get('publication_name'),
                    'country' => Input::get('country'),
                    'editor_name' => Input::get('editor_name'),
                    'editor_phone' => Input::get('editor_phone'),
                    'editor_fax' => Input::get('editor_fax'),
                    'editor_email' => Input::get('editor_email'),
                    'supporting_document' => $input['supporting_document'],
                    'media_type' => 'Radio Station',
                    'radio_station_listenership' => Input::get('radio_station_listenership'),
                    'radio_station_frequency' => Input::get('radio_station_frequency'),
                    'radio_station_audio_event' => $input['radio_station_audio_event'],
                    'radio_station_audio_pre_event' => $input['radio_station_audio_pre_event'],
                    'created_at' => date('Y-m-d H:i:s'),
                );
            }else {
                //website
                $website_traffic_figure_64 = Input::get('website_traffic_figure_64');
                $input['website_traffic_figure'] = $this->uploadFile($input['website_traffic_figure'], $website_traffic_figure_64, '2016Fia-WTF-', $foldername);

                $website_news_coverage_64 = Input::get('website_news_coverage_64');
                $input['website_news_coverage'] = $this->uploadFile($input['website_news_coverage'], $website_news_coverage_64, '2016Fia-WNC-', $foldername);

                $website_link_64 = Input::get('website_link_64');
                $input['website_link'] = $this->uploadFile($input['website_link'], $website_link_64, '2016Fia-WL-', $foldername);

                $personData = array(


                    'representative_name' => Input::get('representative_name'),
                    'representative_designation' => Input::get('representative_designation'),
                    'representative_phone' => Input::get('representative_phone'),
                    'representative_fax' => Input::get('representative_fax'),
                    'representative_email' => Input::get('representative_email'),
                    'passport_photo' => $input['passport_photo'],
                    'publication_name' => Input::get('publication_name'),
                    'press_card' => $input['press_card'],
                    'country' => Input::get('country'),
                    'editor_name' => Input::get('editor_name'),
                    'editor_phone' => Input::get('editor_phone'),
                    'editor_fax' => Input::get('editor_fax'),
                    'editor_email' => Input::get('editor_email'),
                    'supporting_document' => $input['supporting_document'],
                    'media_type' => 'Website',
                    'website_traffic_figure' => $input['website_traffic_figure'],
                    'website_news_coverage'=> $input['website_news_coverage'],
                    'website_link' => $input['website_link'],
                    'created_at' => date('Y-m-d H:i:s'),
                );
            }


            $this->insertAccreditedFia($personData);
            //Generate reponse screen
            $return = array(
                'success' => 1
            );

            return Redirect::route('media.accreditation.submit', $return)->with('success', true);

        }catch(Exception $e) {
            return 'Oops there is an error in submitting your form: ' . $e->getMessage();;
        }
    }

    public function postAccreditationEntertainmentForm() {
        try {
            $data = array();
            $input = Input::all();
            $foldername = 'Entertainment';

            $formal_letter_64 = Input::get('formal_letter_64');
            $input['formal_letter'] = $this->uploadFile($input['formal_letter'], $formal_letter_64, '2016Fia-FL-', $foldername);

            $sgp_pre_coverage_64 = Input::get('sgp_pre_coverage_64');
            $input['sgp_pre_coverage'] = $this->uploadFile($input['sgp_pre_coverage'], $sgp_pre_coverage_64, '2016Fia-SPC-', $foldername);

            $entertainmentData = array(
                'publication_name' => Input::get('publication_name'),
                'country' => Input::get('country'),
                'editor_name' => Input::get('editor_name'),
                'editor_phone' => Input::get('editor_phone'),
                'editor_email' => Input::get('editor_email'),
                'circulation_readership_frequency' => Input::get('circulation_readership_frequency'),
                'frequency' => Input::get('frequency'),
                'description' => Input::get('description'),
                'created_at' => date('Y-m-d H:i:s'),
                'formal_letter'=> $input['formal_letter'],
                'sgp_pre_coverage' => $input['sgp_pre_coverage'],
            );

            $accredited_entertainment_form_id = $this->insertAccreditedEntertainment($entertainmentData);
            $concert_checked = Input::get('concert');

            $concert = '';

            if(is_array($concert_checked)) {
                $entertainmentDetailList = [];
                for ($i = 0; $i < count($concert_checked); $i++) {
                    $concert_code =  $concert_checked[$i];

                    $journalist_passport_64 = Input::get($concert_code . '_journalist_passport_64');
                    $input[$concert_code . '_journalist_passport'] =  $this->uploadFile($input[$concert_code . '_journalist_passport'], $journalist_passport_64, '2016Fia-JPP-', $foldername);

                    $photographer_passport_64 = Input::get($concert_code . '_photographer_passport_64');
                    $input[$concert_code . '_photographer_passport'] = $this->uploadFile($input[$concert_code . '_photographer_passport'], $photographer_passport_64, '2016Fia-JPP-', $foldername);

                    $event_type = '';

                    if($concert_code == 'day1_kylie') {
                        $concert = 'DAY 1 FRIDAY Kylie';
                    }else if($concert_code == 'day2_kc_sunshine') {
                        $concert = 'DAY 2 SATURDAY KC and The Sunshine Band';
                    }else if($concert_code == 'day2_bastille') {
                        $concert = 'DAY 2 SATURDAY Bastille ';
                    }else if($concert_code == 'day2_queen_adam_lambert') {
                        $concert = 'DAY 2 SATURDAY Queen + Adam Lambert ';
                    }else if($concert_code == 'day3_pentatonix') {
                        $concert = 'DAY 3 SUNDAY Pentatonix ';
                    }else if($concert_code  == 'day3_halsey') {
                        $concert = 'DAY 3 SUNDAY Halsey';
                    }else if($concert_code == 'day3_inmagine_dragons') {
                        $concert = 'DAY 3 SUNDAY Imagine Dragons';
                    }

                    $entertainmentDetailData = array(
                        'accredited_entertainment_form_id' => $accredited_entertainment_form_id,
                        'concert' => $concert,
                        'journalist_name' => Input::get($concert_code . '_journalist_name'),
                        'journalist_designation' => Input::get($concert_code . '_journalist_designation'),
                        'journalist_mobile' => Input::get($concert_code . '_journalist_mobile'),
                        'journalist_email' => Input::get($concert_code . '_journalist_email'),
                        'journalist_passport' => $input[$concert_code . '_journalist_passport'],
                        'photographer_name' => Input::get($concert_code . '_photographer_name'),
                        'photographer_designation' => Input::get($concert_code . '_photographer_designation'),
                        'photographer_mobile' => Input::get($concert_code . '_photographer_mobile'),
                        'photographer_email' => Input::get($concert_code . '_photographer_email'),
                        'photographer_passport' => $input[$concert_code . '_photographer_passport'],
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    array_push($entertainmentDetailList, $entertainmentDetailData);
                }
                $this->insertAccreditedEntertainmentDetail($entertainmentDetailList);
            }

            //Generate reponse screen
            $return = array(
                'success' => 1
            );

            return Redirect::route('media.accreditation.entertainment.submit', $return)->with('success', true);

        }catch(Exception $e) {
            echo 'Message: ' .$e->getMessage();
            $data['success'] = false;
            return 'Oops there is an error in submitting your form';
        }
    }


    private function uploadFile($input, $input64, $prefix, $foldername) {
        if($input) {
            $accreditationPath = "media/accreditation";
            $uploadPath = "/public/" . $accreditationPath . "/" . $foldername;
            $target_file = basename($input);
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
            $filename = uniqid($prefix, $more_entropy = true) . "." .$imageFileType;

            $path = dirname(dirname(dirname(__FILE__)));
            $dir = $path .$uploadPath;
            $desDir = $dir . '/' . $filename;

            try {
                if (preg_match('/data:([^;]*);base64,(.*)/', $input64, $matches)) {
                    $output_file = $this->base64_to_img($matches[2], $desDir);
                    return MediaController::server . "/" . $accreditationPath . "/" . $foldername . "/" . $filename;
                }else {
                    return '';
                }
            }

            catch(Exception $e) {
                echo 'Message: ' .$e->getMessage();
                return '';
            }
        }else {
            return '';
        }

    }

    protected function base64_to_img($base64_string, $output_file){
        if(!file_exists($output_file)){
            touch($output_file);
            chmod($output_file, 0777);
        }
        $ifp = fopen($output_file, "wb");
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        return $output_file;
    }

    private function insertAccreditedFia($personData) {
        /*$time = date_default_timezone_get();
        $personData = array(
            'representative_name' => $fia['representative_name'],
            'representative_designation' => $fia['representative_designation'],
            'representative_phone' => $fia['representative_phone'],
            'representative_fax' => $fia['representative_fax'],
            'representative_email' => $fia['representative_email'],
            'passport_photo' => $fia['passport_photo'],
            'press_card' => $fia['press_card'],
            'editor_name' => $fia['editor_name'],
            'editor_phone' => $fia['editor_phone'],
            'editor_fax' => $fia['editor_fax'],
            'editor_email' => $fia['editor_email'],
            'supporting_document' => $fia['supporting_document'],
            'print_media_circulation' => $fia['print_media_circulation'],
            'print_media_listenership' => $fia['print_media_listenership'],
            'print_media_frequency' => $fia['print_media_frequency'],
            'print_media_publication' => $fia['print_media_publication'],
            'print_media_coverage' => $fia['print_media_coverage'],
            'print_media_pre_coverage' => $fia['print_media_pre_coverage'],
            'radio_station_listenership' => $fia['radio_station_listenership'],
            'radio_station_frequency' => $fia['radio_station_frequency'],
            'radio_station_audio_event' => $fia['radio_station_audio_event'],
            'radio_station_audio_pre_event' => $fia['radio_station_audio_pre_event'],
            'website_traffic_figure' => $fia['website_traffic_figure'],
            'website_news_coverage'=> $fia['website_news_coverage'],
            'website_link'  => $fia['website_link'],
            'created_at' => $time,
        );*/
        DB::connection('sgp_cms_latin')->table('accredited_fia_form')
            ->insert($personData);
    }

    private function insertAccreditedEntertainment($entertainmentData) {
        $id = DB::connection('sgp_cms_latin')->table('accredited_entertainment_form')
                ->insertGetId($entertainmentData);
        return $id;
    }

    private function insertAccreditedEntertainmentDetail($entertainmentDetailData) {
        DB::connection('sgp_cms_latin')->table('accredited_entertainment_detail_form')
            ->insert($entertainmentDetailData);
    }

    public function showAccreditationSubmit(){
        return View::make('layouts.media.accreditation-submit', $this->data);
    }

    public function showAccreditationEntertainmentSubmit(){
        return View::make('layouts.media.accreditation-entertainment-submit', $this->data);
    }

    public function postEntryForm() {
        $data = array();
        $rules = array();
        $input = Input::all();

        // names validation
        //for ($i = 0, $c = count($input['persons']); $i < $c; $i++) {
        //    $rules["persons.{$i}.full_name"] = 'required';
        //    $rules["persons.{$i}.passport_number"] = 'required';
        //    $rules["persons.{$i}.passport_expiry_date"] = 'required';
        //    $rules["persons.{$i}.passport_issue_date"] = 'required';
        //    $rules["persons.{$i}.country_of_origin"] = 'required';
        //    $rules["persons.{$i}.news_organisation"] = 'required';
        //    $rules["persons.{$i}.designation"] = 'required';
        //    $rules["persons.{$i}.flight_arrival_number"] = 'required';
        //    $rules["persons.{$i}.flight_departure_number"] = 'required';
        //    $rules["persons.{$i}.flight_arrival_date"] = 'required';
        //    $rules["persons.{$i}.flight_departure_date"] = 'required';
        //}

        //if ($input['formType'] != 'journalist') {
        //    // av validation
        //    $rules["avEquipment.country_from"] = 'required';
        //    $rules["avEquipment.person.full_name"] = 'required';
        //    $rules["avEquipment.person.passport_number"] = 'required';
        //    $rules["avEquipment.person.news_organisation"] = 'required';
        //    for ($i = 0, $c = count($input['avEquipment']['equipment']); $i < $c; $i++) {
        //        $rules["avEquipment.equipment.{$i}.item"] = 'required';
        //        $rules["avEquipment.equipment.{$i}.quantity"] = 'required';
        //        $rules["avEquipment.equipment.{$i}.approx_value"] = 'required';
        //        $rules["avEquipment.equipment.{$i}.serial_number"] = 'required';
        //    }
        //}

        $validator = Validator::make($input, $rules);

        if ($validator->passes()) {
            // loop persons and insert...
            if ($input['formType'] != 'journalist') {
                $code = substr(md5(time() . Request::server('REMOTE_ADDR')), 0, 12);
                $this->insertIntoAVCrew($input['persons'], $code);
                $this->insertIntoAVEquipment($input['avEquipment']['equipment'], $code);
                $this->insertIntoAVRepresentative($input['avEquipment']['person'], $input['avEquipment']['country_from'], $code);
            }
            else {
                $this->insertIntoJournalist($input['persons']);
            }

            $data['success'] = true;
        }
        else {
            $data['success'] = false;
            $data['errors'] = $validator->failed();
        }

        return json_encode($data);
    }

    public function showMobileApp() {
        return View::make('layouts.media.mobileapp', $this->data);
    }

    /*
    |--------------------------------------------------------------------------
    | DB CALLS
    |--------------------------------------------------------------------------
    */

    private function getPressReleaseYears() {
        return DB::connection('sgp_cms_latin')->table('press_release')
                 ->select('press_release_year')
                 ->groupBy('press_release_year')
                 ->orderBy('press_release_year', 'desc')
                 ->get();
    }

    private function getPressReleaseTitles() {
        return DB::connection('sgp_cms_latin')->table('press_release')
                 ->select('id', 'press_release_date', 'press_release_year', 'title')
                 ->orderBy('press_release_date', 'desc')
                 ->orderBy('id', 'desc')
                 ->get();
    }

    private function getPressRelease($id) {
        return DB::connection('sgp_cms_latin')->table('press_release')
                 ->select('id', 'press_release_date', 'title', 'subtitle', 'content', 'meta_keyword', 'meta_description')
                 ->where('id', '=', $id)
                 ->get();
    }

    private function insertIntoJournalist($persons) {
        foreach ($persons as $person) {
            $personData = array(
                'full_name' => $person['full_name'],
                'passport_number' => $person['passport_number'],
                'passport_expiry_date' => $person['passport_expiry_date'],
                'passport_issue_date' => $person['passport_issue_date'],
                'country_of_origin' => $person['country_of_origin'],
                'news_organisation' => $person['news_organisation'],
                'designation' => $person['designation'],
                'flight_arrival_number' => $person['flight_arrival_number'],
                'flight_departure_number' => $person['flight_departure_number'],
                'flight_arrival_date' => $person['flight_arrival_date'],
                'flight_departure_date' => $person['flight_departure_date']
            );

            DB::connection('sgp_cms_latin')->table('accredited_journalist_2013')
              ->insert($personData);
        }
    }

    private function insertIntoAVCrew($persons, $code) {
        foreach ($persons as $person) {
            $personData = array(
                'code' => $code,
                'full_name' => $person['full_name'],
                'passport_number' => $person['passport_number'],
                'passport_expiry_date' => $person['passport_expiry_date'],
                'passport_issue_date' => $person['passport_issue_date'],
                'country_of_origin' => $person['country_of_origin'],
                'news_organisation' => $person['news_organisation'],
                'designation' => $person['designation'],
                'flight_arrival_number' => $person['flight_arrival_number'],
                'flight_departure_number' => $person['flight_departure_number'],
                'flight_arrival_date' => $person['flight_arrival_date'],
                'flight_departure_date' => $person['flight_departure_date']
            );

            DB::connection('sgp_cms_latin')->table('accredited_photographer_crew_2013')
              ->insert($personData);
        }
    }

    private function insertIntoAVEquipment($equipment, $code) {
        foreach ($equipment as $eq) {
            $eqData = array(
                'code' => $code,
                'item' => $eq['item'],
                'quantity' => $eq['quantity'],
                'approx_value' => $eq['approx_value'],
                'serial_number' => $eq['serial_number']
            );

            DB::connection('sgp_cms_latin')->table('accredited_av_2013')
              ->insert($eqData);
        }
    }

    private function insertIntoAVRepresentative($person, $country, $code) {
        $personData = array(
            'code' => $code,
            'full_name' => $person['full_name'],
            'passport_number' => $person['passport_number'],
            'news_organisation' => $person['news_organisation'],
            'country_from' => $country,
        );

        DB::connection('sgp_cms_latin')->table('accredited_av_incharge_2013')
          ->insert($personData);
    }

} 