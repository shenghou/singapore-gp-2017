<?php
/**
 * Created by PhpStorm.
 * User: geraldyeo
 * Date: 19/3/14
 * Time: 9:00 AM
 */

class RevUpController extends BaseController {

    public function showWelcome() {
    	$this->data['revups'] = $this->getRevUps();
        $this->data['angularApp'] = 'revupapp';
        return View::make('layouts.revup.index', $this->data);
    }

    public function showRevUp($slug) {
        $revUp = $this->getRevUpBySlug($slug);
    	$this->data['revup'] = $revUp;
            $this->data['angularApp'] = 'revUpFormApp';

        return View::make('layouts.revup.content', $this->data);
    }

    public function postRevUp($slug) {
        $revUp = $this->getRevUpBySlug($slug);
        $input = Input::all();
        $this->insertIntoForm($input, $revUp['id']);
        $data['success'] = true;
        return json_encode($data);
    }

    public function exportContestCsv($slug) {
        $links = array('sgp_cms_191', 'sgp_cms_192', 'sgp_cms_193');
        
        header("Content-type: text/csv");
        $fileName = 'RevUp_' . date('Y_m_d___Hi') . '.csv';
        header(sprintf('Content-Disposition: attachment; filename="%s"', $fileName));
        header("Pragma: no-cache");
        header("Expires: 0");

        // Create a temporary file in the temporary
        // files directory using sys_get_temp_dir()
        $temp_file = tempnam(sys_get_temp_dir(), 'Tux');

        $handle = fopen($temp_file, 'r+');
        $i = 0;
        $handle = fopen('php://output', 'r+');
        $headers = null;
        $model = new RevUpSingapore();
        foreach ($links as $link) {
            try {
                $conn = DB::connection($link);
            } catch (\Exception $e) {
                continue;
            }            
            if($conn->getDatabaseName()) {
                $revup = DB::connection($link)->table('revupsingapore')
                            ->leftJoin('revupsingapore_forms', 'revupsingapore_forms.revup_id', '=', 'revupsingapore.id')
                            ->where('revupsingapore.slug', '=', $slug)
                            ->select('revupsingapore.id', 'revupsingapore_forms.*')->get();

                if (!$fileName) {
                    $fileName = $slug.".csv";
                }
                foreach ($revup as $form) {
                    $data = unserialize($form->data);
                    if (!$headers) {
                        $headers = array_keys($data);
                        fputcsv($handle, $headers);
                    }
                    $row = array();
                    foreach ($headers as $h) {
                        if (array_key_exists($h, $data)) {
                            $row[$h] = $data[$h];
                        } else {
                            $row[$h] = '';
                        }
                        
                    }
                    fputcsv($handle, $row);
                    $i++;
                };
                fputcsv($handle, array("END OF " . $link . " -- " . count($revup) . " RECORDS"));
                fputcsv($handle, array());
                fputcsv($handle, array());
            }
        }
        fclose($handle);

        // make a copy and remove the tmp file
        $file = file_get_contents($temp_file);
        unlink($temp_file);

        // create new file with {slug} name
        $newFile = sys_get_temp_dir().'/'.$fileName;
        file_put_contents($newFile,$file);

        // download
        // return Response::download($newFile,$fileName);
    }

    public function exportKartnivalPdf($fullname,$nirc,$timeslot){

        $html = <<<confirmation
<table>
<tr>
  <td width=55%><p><strong>Confirmation</strong></p>
<p> Rev Up Singapore! Kartnival<br/>
Thank you for booking your free karting session.<br/>
You have booked to kart in the following timeslot:</p>
<p>Name: {$fullname}<br/>
NRIC: {$nirc}<br/>
Timeslot: $timeslot</p>
<p>Sunday 31 August 2014<br/>
F1 Pit Building<br/>
Nearest MRT Station: CC4/DT15 Promenade
</p>

</td>
  <td width=45%><img src="http://128.199.250.199/images/revupsglogo.jpg" alt="Smiley face"></td>
</tr>
<tr>
  <td colspan=2><p>Do note that karting will be on a first-come, first-served basis. You are strongly encouraged to arrive on the hour to secure your slot. To ensure a fair amount of karting time for all participants, latecomers will strictly not be entertained.<br/><br/><strong><span style="color:red">Please bring along a printed copy of this page and a valid Singapore ID for verification purposes.</span></strong><br/><br/>You may wish to bring along up to 3 friends/family members to join in the fun as a spectator where they can enjoy a Formula One - themed mini carnival partake in interactive activities, enjoy carnival snacks, and try their hand at racing on Singapore GP simulators.<br/><br/>For any queries, please do not hesitate to email <a href="mailto:info@singaporegp.sg">info@singaporegp.sg</a><br/><br/>We look forward to welcoming you to the Marina Bay Street Circuit.</p></td>
</tr>

</table>
confirmation;

        $pdf = PDF::loadHTML($html);
        return $pdf->download('SingaporeGP-Kartnival-Confirmation.pdf');
    }

    private function insertIntoForm($input, $revupId) {
        $data = array(
            'revup_id' => $revupId,
            'data' => serialize($input),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
            );
        DB::table('revupsingapore_forms')
            ->insert($data);
    }

    private function getRevUps() {
        return RevUpSingapore::where('hidden', '=', 0)
                      ->orderBy('sort')
                      ->get();
    }

    private function getRevUpBySlug($slug) {
        return RevUpSingapore::where('slug', '=', $slug)
                      ->firstOrFail();
    }
} 
