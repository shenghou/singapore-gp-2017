<?php
/**
 * Created by PhpStorm.
 * User: Sheng Hou
 * Date: 12/8/2016
 * Time: 5:20 PM
 */

 class MerchandiseShopController extends BaseController {
    public function showWelcome() {
        	return View::make('layouts.shop.index', $this->data);
        }

 }