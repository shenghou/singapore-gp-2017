<?php

class SearchController extends Controller {

    public function googleSiteSearch($query) {
        $domain = 'www.singaporegp.sg';
        $query = 'http://www.google.com/search?q=' . urlencode('site:' . $domain . ' ' . $query);
        return Redirect::to($query);
    }

}