<?php

use MyApp\Parse\ParseAPI;

class HomeController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | Index and Latest News page
    |
    */

    public function showWelcome()
    {
        // device detected
        if ($this->clientProfile) {
            $this->data['bucketCaption'] = $this->clientProfile->deviceType;
        }

        // race result
        $this->data['angularApp'] = 'homeApp';

        // populdate ticket maintanence message
        date_default_timezone_set('Asia/Singapore');
        $date = new DateTime(date("Y-m-d H:i:s"));
        $start = new DateTime("2016-05-14 04:00:00");
        $end = new DateTime("2016-05-14 07:00:00");
        $this->data['show_maintenance_msg'] = ($date >= $start && $date <= $end);

        // populate ticket banner image url for early bird count down
//        date_default_timezone_set('Asia/Singapore');
//        $date1 = new DateTime(date("Y-m-d"));
//        $date2 = new DateTime("2016-05-03");
//        $diff = $date2->diff($date1)->format("%a");

        // default ticket image
//        switch ($diff) {
//            case 0 : $this->data['ticket_image_url'] = 'http://singaporegp.sg/Uploaded/homepage_imgs/seb_2015.png'; break;
//            case 1 : $this->data['ticket_image_url'] = 'http://singaporegp.sg/media/ticketing/earlybird/early-bird-0-days-left.jpg'; break;
//            case 2 : $this->data['ticket_image_url'] = 'http://singaporegp.sg/media/ticketing/earlybird/early-bird-1-days-left.jpg'; break;
//            case 3 : $this->data['ticket_image_url'] = 'http://singaporegp.sg/media/ticketing/earlybird/early-bird-2-days-left.jpg'; break;
//            case 4 : $this->data['ticket_image_url'] = 'http://singaporegp.sg/media/ticketing/earlybird/early-bird-3-days-left.jpg'; break;
//            case 5 : $this->data['ticket_image_url'] = 'http://singaporegp.sg/media/ticketing/earlybird/early-bird-4-days-left.jpg'; break;
//            default : $this->data['ticket_image_url'] = 'http://singaporegp.sg/Uploaded/homepage_imgs/seb_2015.png'; break;
//        }
        $this->data['ticket_image_url'] = 'http://singaporegp.sg/Uploaded/homepage_imgs/seb_2015.png';

        return View::make('layouts.home.index', $this->data);

        //DISABLE ALL APP PARSE RELATED API - 20160217
//
//        // homepage data from Parse.com
//        $homepage = json_decode(ParseAPI::Call("GET", "Homepage", array('order'=>'order')));
//
//        // offline data incase Parse.com is down
//        $temp_file = sys_get_temp_dir().'/ParseContentCache.txt';
//
//        if($homepage === null){
//
//            if(file_exists($temp_file)){
//                $offlineData = File::get($temp_file);
//                $this->data = unserialize($offlineData);
//
//                return View::make('layouts.home.index', $this->data);
//
//            }
//        }
//
//        $rowCounts = array(0 => 0, 1 => 0, 2 => 0);
//        $rowOne = array();
//        $rowTwo = array();
//        $rowThree = array();
//
////        return json_encode($homepage);
//
//        // count buckets per row...
//        foreach ($homepage->results as $bucket) {
//            switch ($bucket->row_number) {
//                case '1':
//                    $rowCounts[0] += 1;
//                    break;
//                case '2':
//                    $rowCounts[1] += 1;
//                    break;
//                case '3':
//                    $rowCounts[2] += 1;
//                    break;
//            }
//        }
//
//        foreach ($homepage->results as $bucket) {
//            switch ($bucket->row_number) {
//                case '1':
//                    $ary = array(
//                        "bucketCategory" => $bucket->category,
//                        "bucketCaption" => $bucket->title,
//                        "bucketReadMoreUrl" => $bucket->url_link
//                    );
//
//                    $imgType = 'image' . (4 - $rowCounts[0]);
//                    if (isset($bucket->{$imgType})) {
//                        $ary["bucketImgSrc"] = $bucket->{$imgType}->url;
//                    }
//
//                    $rowOne[] = $ary;
//
//                    break;
//                case '2':
//                    $ary = array(
//                        "bucketCategory" => $bucket->category,
//                        "bucketCaption" => $bucket->title,
//                        "bucketReadMoreUrl" => $bucket->url_link
//                    );
//
//                    $imgType = 'image' . (4 - $rowCounts[1]);
//                    if (isset($bucket->{$imgType})) {
//                        $ary["bucketImgSrc"] = $bucket->{$imgType}->url;
//                    }
//
//                    $rowTwo[] = $ary;
//
//                    break;
//                case '3':
//                    $ary = array(
//                        "bucketCategory" => $bucket->category,
//                        "bucketCaption" => $bucket->title,
//                        "bucketReadMoreUrl" => $bucket->url_link
//                    );
//
//                    $imgType = 'image' . (3 - $rowCounts[2]);
//                    if (isset($bucket->{$imgType})) {
//                        $ary["bucketImgSrc"] = $bucket->{$imgType}->url;
//                    }
//
//                    $rowThree[] = $ary;
//
//                    break;
//            }
//        }
//
//        $this->data['rowOne'] = $rowOne;
//        $this->data['rowTwo'] = $rowTwo;
//        $this->data['rowThree'] = $rowThree;
//
//        $this->data['angularApp'] = 'homeApp';
//
//        // make a copy of the data and store in temp file
//        $offlineData = serialize($this->data);
//        File::put($temp_file, $offlineData);
//        if (AppConfig::$isHomePageChange == true) {
//            return View::make('layouts.home.index_backup', $this->data);
//        }
//        else {
//
//            return View::make('layouts.home.index', $this->data);
//        }
    }

    public function showLatestNews()
    {
        return 'Latest News';
    }

}