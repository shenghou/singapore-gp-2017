<?php

use MyApp\CheckCountry;

class PromotionsAndContestsController extends BaseController {

    public function showWelcome() {
        $contests = $this->getContests();
        $this->data['angularApp'] = 'revupapp';
        // $contests = $this->removeContest($contests);
        $this->data['contests'] = $contests;
        // $this->data['contry'] = CheckCountry::isAustralian();

        // if(CheckCountry::isAustralian()) {
        //     $this->data['contests'][] = $this->getContestBySlug("daniel-ricciardo-meet-and-greet-contest");
        // }

        //return View::make('layouts.win.index', $this->data);
        return View::make('layouts.fanzone.contest');
    }

    /*
    THis shows the Upgrade to green room contest available only in India
    */
    public function showForeignContest() {

        if(CheckCountry::isIndian())
        {
            $contest = $this->getContestBySlug('upgrade-to-green-room');
            $this->data['angularApp'] = 'winContestApp';
            $this->data['contest'] = $contest;
            return View::make('layouts.win.foreign_contest', $this->data);
        }
        else
            echo 'Error 403: Forbidden Access';
    }

    public function showSEBContest() {

        $contest = $this->getContestBySlug('2017-super-early-bird-ticket-contest');
        $this->data['angularApp'] = 'winContestApp';
        $this->data['contest'] = $contest;
        return View::make('layouts.win.seb_contest', $this->data);
        ;
    }

    public function showStraitstimescontest() {

        $contest = $this->getContestBySlug('straits-times-contest');
        $this->data['angularApp'] = 'winContestApp';
        $this->data['contest'] = $contest;
        return View::make('layouts.win.straits_times_contest', $this->data);
    }

    public function postForeignContest() {
        $contest = $this->getContestBySlug('upgrade-to-green-room');

        $data = array();
        $rules = array();
        $input = Input::all();
        $input['contest_id'] = $contest['id'];

        $rules["transaction_number"] = 'required';
        $rules["first_name"] = 'required';
        $rules["last_name"] = 'required';
        $rules["email"] = 'required|email';
        //$rules["nationality"] = 'required';
        //$rules["country"] = 'required';

        $validator = Validator::make($input, $rules);

        if ($validator->passes()) {
            if($this->insertIntoForeignContest($input)){
                $data['success'] = true;
            }else{
                $data['success'] = false;
                $data['errors'] = 'Transaction number found';
            }
        }
        else {
            $data['success'] = false;
            $data['errors'] = $validator->failed();
        }

        return json_encode($data);
    }

    public function postStraitstimescontest() {
        $contest = $this->getContestBySlug('straits-times-contest');

        $data = array();
        $rules = array();
        $input = Input::all();
        $input['contest_id'] = $contest['id'];

        $rules["first_name"] = 'required';
        $rules["last_name"] = 'required';
        $rules["email"] = 'required|email';
        $rules["contact_number"] = 'required';

        $validator = Validator::make($input, $rules);

        if ($validator->passes()) {
            if($this->insertIntoStraitstimescontest($input)){
                $data['success'] = true;
            }else{
                $data['success'] = false;
                $data['errors'] = 'Invalid entry';
            }
        }
        else {
            $data['success'] = false;
            $data['errors'] = $validator->failed();
        }

        return json_encode($data);
    }

    public function postSEBContest() {
        $contest = $this->getContestBySlug('2017-super-early-bird-ticket-contest');

        $data = array();
        $rules = array();
        $input = Input::all();
        $input['contest_id'] = $contest['id'];

        $rules["transaction_number1"] = 'required';
        $rules["transaction_number2"] = 'required';
        $rules["first_name"] = 'required';
        $rules["last_name"] = 'required';
        $rules["email"] = 'required|email';
        $rules["contact_number"] = 'required';
        $rules["nationality"] = 'required';
        $rules["country"] = 'required';

        $validator = Validator::make($input, $rules);

        if ($validator->passes()) {
            if($this->insertIntoSEBContest($input)){
                $data['success'] = true;
            }else{
                $data['success'] = false;
                $data['errors'] = 'Transaction number found';
            }
        }
        else {
            $data['success'] = false;
            $data['errors'] = $validator->failed();
        }

        return json_encode($data);
    }
    
    public function postDanielRicardoContest() {
        $contest = $this->getContestBySlug("are-you-daniel-ricciardos-biggest-fan");

        $data = array();
        $rules = array();
        $input = Input::all();
        $input['contest_id'] = $contest['id'];

        $rules["description"] = 'required';
        $rules["transaction_number"] = 'required';
        $rules["first_name"] = 'required';
        $rules["last_name"] = 'required';
        $rules["email"] = 'required|email';
        // $rules["file"] = 'required';
        // $rules["country"] = 'required';


        $validator = Validator::make($input, $rules);

        if ($validator->passes()) {
            if($this->insertIntoContest($input)){
                $data['success'] = true;
            }else{
                $data['success'] = false;
                $data['errors'] = 'Invalid entry';
            }
        }
        else {
            $data['success'] = false;
            $data['errors'] = $validator->failed();
        }

        return json_encode($data);

    }

    public function postMaxVerstappenContest() {
        $contest = $this->getContestBySlug("max-verstappen-meet-and-greet-contest");

        $data = array();
        $rules = array();
        $input = Input::all();
        $input['contest_id'] = $contest['id'];

        $rules["description"] = 'required';
        $rules["transaction_number"] = 'required';
        $rules["first_name"] = 'required';
        $rules["last_name"] = 'required';
        $rules["email"] = 'required|email';
        // $rules["file"] = 'required';
        // $rules["country"] = 'required';

        $validator = Validator::make($input, $rules);

        if ($validator->passes()) {
            if($this->insertIntoContest($input)){
                $data['success'] = true;
            }else{
                $data['success'] = false;
                $data['errors'] = 'Invalid entry';
            }
        }
        else {
            $data['success'] = false;
            $data['errors'] = $validator->failed();
        }

        return json_encode($data);
    }

    public function showContest($slug) {
        $contest = $this->getContestBySlug($slug);
        $this->data['angularApp'] = 'winContestApp';
        $this->data['contest'] = $contest;

        if ($contest->hidden == 1) {
            return Redirect::route('contests');
        }

        if ($slug == 'meet-and-greet') {
            return View::make('layouts.win.meet_greet', $this->data);
        } elseif ($slug == "are-you-daniel-ricciardos-biggest-fan") {
            if(CheckCountry::isAustralian()){
                return View::make('layouts.win.daniel_ricardo', $this->data);
            }else {
                return View::make('layouts.fanzone.contest', $this->data);
            }
        } elseif($slug == "max-verstappen-meet-and-greet-contest") {
            if(CheckCountry::isNetherlands()){
                return View::make('layouts.win.max_verstappen', $this->data);
            }else {
                return View::make('layouts.fanzone.contest', $this->data);
            }
        } elseif ($slug == 'win-exclusive-passes-to-get-up-close') {
            return View::make('layouts.win.get_up_close', $this->data);
        } elseif ($slug == 'straits-times-contest') {
            return View::make('layouts.win.straits_times_contest', $this->data);
        } elseif ($slug == '2016-super-early-bird-ticket-contest') {
            return View::make('layouts.win.seb_contest', $this->data);
        } elseif ($slug == '2016-car-decal-contest') {
            return View::make('layouts.win.car_decal_contest', $this->data);
        }elseif ($slug == 'my-sgp-moment') {
            return View::make('layouts.win.my_sgp_moment', $this->data);
        }


        return View::make('layouts.win.form', $this->data);
    }

    public function showContestApp($slug) {
        $contest = $this->getContestBySlug($slug);
        $this->data['angularApp'] = 'winContestApp';
        $this->data['contest'] = $contest;
        if($slug == 'meet-and-greet') {
            return View::make('layouts.win.meet_greet_app',$this->data);
        } elseif($slug == "daniel-ricciardo-meet-and-greet-contest") {
            return View::make('layouts.win.daniel_ricardo_app', $this->data);
        }
        return View::make('layouts.win.form_app', $this->data);
    }

    public function postContest($slug) {
        $contest = $this->getContestBySlug($slug);

        $data = array();
        $rules = array();
        $input = Input::all();
        $input['contest_id'] = $contest['id'];

        /*if($slug != "win-a-money-can't-buy-experience" && $slug != "pitstopatorchard"){
            $rules["transaction_number"] = 'required';
        }
        */
       
        if($slug != "win-a-money-can't-buy-experience" ){
            $rules["transaction_number"] = 'required';
        }else {
            $input['transaction_number'] = '';
        }

        $rules["first_name"] = 'required';
        $rules["last_name"] = 'required';
        $rules["email"] = 'required|email';
        $rules["email"] = 'required|email';
        $validator = Validator::make($input, $rules);



        if ($validator->passes()) {
            if($slug == "win-a-money-can't-buy-experience" || $slug == 'SGP-fan-experience'){
                if($input['file']){
                    if (preg_match('/data:([^;]*);base64,(.*)/', $input['file'], $matches)) {
                        if($matches[1]=="image/png"){
                            $fileName = time().'.png';
                        }elseif ($matches[1]=="image/jpg") {
                            $fileName = time().'.jpg';
                        }else{
                            if($matches[1]=="image/jpeg"){
                                $fileName = time().'.jpg';
                            }else if ($matches[1]=="image/gif") {
                                $fileName = time().'.gif';
                            }else {
                                $data['success'] = false;
                                $data['errors'] = "Please attach file with format jpg/jpeg, png or gif only! ";

                                return json_encode($data);
                            }
                        }
                        $path = dirname(dirname(dirname(__FILE__)));
                        $dir = $path .'/public/images/contests/dont-money';
                        if($slug == 'SGP-fan-experience') {
                            $dir = $path .'/public/media/contests/sgp-fan-experience';
                        }
                        $desDir = $dir . '/' . $fileName;
                        $this->base64_to_img($matches[2], $desDir);
                        //urlImage = 'http://'.$_SERVER['HTTP_HOST'] . '/images/contests/dont-money/'.$fileName;
                        $input['file'] = $fileName;
                    }
                }
            }


            if($this->insertIntoContest($input)){
                $data['success'] = true;

            }else{
                $data['success'] = false;
                $data['errors'] = 'Transaction number found';
            }
        }
        else {
            $data['success'] = false;
            $data['errors'] = $validator->failed();
        }

        return json_encode($data);
    }


    public function showPitStopChallenge() {
        $this->data['angularApp'] = 'winChallengeApp';

        return View::make('layouts.win.challenge', $this->data);
    }

    public function showWallpapers() {
        $result = $this->getWallpapers();
        $wallpapers = array();

        foreach ($result as $wallpaper) {
            $idx = 'y' . $wallpaper->year;
            if (!isset($wallpapers[$idx])) {
                $wallpapers[$idx] = array();
            }

            $wallpapers[$idx][] = $wallpaper;
        }

        $this->data['angularApp'] = 'genericApp';
        $this->data['wallpapers'] = $wallpapers;

        return View::make('layouts.win.wallpapers', $this->data);
    }

    /*
    |--------------------------------------------------------------------------
    | DB CALLS
    |--------------------------------------------------------------------------
    */

    private function removeContest($list){
        if(!CheckCountry::isAustralian()) {
            foreach ($list as $key => $value) {
                if($value['slug'] == 'daniel-ricciardo-meet-and-greet-contest'){
                    unset($list[$key]);
                }
            }
        }
        return $list;
    }

    private function insertIntoContest($input) {
        $contestModel = new ContestSubmission;

        if(true){

            if(array_key_exists('latest_news', $input) && $input['latest_news'] == true)
            {
                $input['latest_news'] = 1;
            }
            if(array_key_exists('latest_news_partners', $input) && $input['latest_news_partners'] == true)
            {
                $input['latest_news_partners'] = 1;
            }

            $contestData = array(
                'transaction_number' => $input['transaction_number'],
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'email' => $input['email'],
                'contest_id' => $input['contest_id'],
                'created_at' => new DateTime('Asia/Singapore'),
                'contact_number' => isset($input['contact_number']) ? $input['contact_number'] : '',
                'latest_news' => (array_key_exists('latest_news', $input) && $input['latest_news'] == true) ? 1 : 0,
                'latest_news_partners' => (array_key_exists('latest_news_partners', $input) && $input['latest_news_partners'] == true) ? 1 : 0,
                'option'=> $input['option'],
                'description' => isset($input['description']) ? $input['description'] : '',
                'promo_code' => isset($input['promo_code']) ? $input['promo_code'] : '',
                'file' => $input['file'],
            );

            return $contestModel->insert($contestData);
        }else{
            return false;
        }
    }

    private function insertIntoStraitstimescontest($input) {
        $contestModel = new ContestSubmission;

        if(true){

            $contestData = array(
                'transaction_number' => 'Straits Times Contest',
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'email' => $input['email'],
                'contest_id' => $input['contest_id'],
                'created_at' => new \DateTime(),
                'contact_number' => isset($input['contact_number']) ? $input['contact_number'] : '',
                'latest_news' => (array_key_exists('latest_news', $input) && $input['latest_news'] == true) ? 1 : 0,
                'latest_news_partners' => (array_key_exists('latest_news_partners', $input) && $input['latest_news_partners'] == true) ? 1 : 0,
                'option'=> $input['option'],
            );

            return $contestModel->insert($contestData);
        }else{
            return false;
        }
    }


    private function insertIntoForeignContest($input) {

        $dup = ContestSubmission::where('transaction_number', $input['transaction_number'])
            ->where('contest_id', $input['contest_id'])
            ->first();

        if(!$dup){

            $contestData = array(
                'transaction_number' => $input['transaction_number'],
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'email' => $input['email'],
                //nationality' => (array_key_exists('nationality', $input)) ? $input['nationality'] : NULL,
                //'country' => (array_key_exists('country', $input)) ? $input['country'] : NULL,
                'contest_id' => $input['contest_id'],
                'created_at' => new \DateTime(),
                'description' => (array_key_exists('description', $input)) ? $input['description'] : NULL,
                'latest_news' => (array_key_exists('latest_news', $input) && $input['latest_news'] == true) ? 1 : 0,
                'latest_news_partners' => (array_key_exists('latest_news_partners', $input) && $input['latest_news_partners'] == true) ? 1 : 0,
                // 'file' => $input['file'],
            );

            return $contestModel->insert($contestData);
        }else{
            return false;
        }
    }
  
    private function insertIntoSEBContest($input) {

        $dup1 = ContestSubmission::where('transaction_number', $input['transaction_number1'].", ".$input['transaction_number2'])
            ->where('contest_id', $input['contest_id'])
            ->first();


        if(!$dup1){

            $contestData = array(
                'transaction_number' => $input['transaction_number1'].",".$input['transaction_number2'],
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'email' => $input['email'],
                'contact_number' => $input['contact_number'],
                'nationality' => (array_key_exists('nationality', $input)) ? $input['nationality'] : NULL,
                'country' => (array_key_exists('country', $input)) ? $input['country'] : NULL,
                'contest_id' => $input['contest_id'],
                'created_at' => new \DateTime(),
                'description' => (array_key_exists('description', $input)) ? $input['description'] : NULL,
                'latest_news' => (array_key_exists('latest_news', $input) && $input['latest_news'] == true) ? 1 : 0,
                'latest_news_partners' => (array_key_exists('latest_news_partners', $input) && $input['latest_news_partners'] == true) ? 1 : 0,
            );

            return DB::table('contest_submissions')
                ->insert($contestData);
        }else{
            return false;
        }
    }

    private function getContests() {
        return Contest::where('hidden', '=', 0)
            ->orderBy('sort')
            ->get();
    }

    private function getContestBySlug($slug) {
        return Contest::where('slug', '=', $slug)
            ->firstOrFail();
    }

    // SELECT * FROM wallpapers ORDER BY id DESC
    private function getWallpapers() {
        return DB::table('wallpapers')
            ->orderBy('year', 'desc')
            ->orderBy('id', 'desc')
            ->get();
    }

    public function exportContestCsv($slug) {
        // $links = array('sgp_cms');
        $links = array('sgp_cms_191', 'sgp_cms_192', 'sgp_cms_193');

        header("Content-type: text/csv");
        $fileName = 'Contests_' . $slug . '_' . date('Y_m_d___Hi') . '.csv';
        header(sprintf('Content-Disposition: attachment; filename="%s"', $fileName));
        header("Pragma: no-cache");
        header("Expires: 0");

        // Create a temporary file in the temporary
        // files directory using sys_get_temp_dir()
        $temp_file = tempnam(sys_get_temp_dir(), 'Tux2');

        $handle = fopen($temp_file, 'r+');
        $i = 0;
        $handle = fopen('php://output', 'r+');
        $headers = null;
        $model = new Contest();
        foreach ($links as $link) {
            try {
                $conn = DB::connection($link);
            } catch (\Exception $e) {
                continue;
            }
            if($conn->getDatabaseName()) {
                $submission = DB::connection($link)->table('contests')
                    ->leftJoin('contest_submissions', 'contest_submissions.contest_id', '=', 'contests.id')
                    ->where('contests.slug', '=', $slug)
                    ->select('contests.id', 'contest_submissions.*')->get();

                if (!$fileName) {
                    $fileName = $slug.".csv";
                }
                foreach ($submission as $form) {
                    $data = array(
                        'transaction_number' => $form->transaction_number,
                        'first_name' => $form->first_name,
                        'last_name' => $form->last_name,
                        'email' => $form->email,
                        'nationality' => $form->nationality,
                        'country' => $form->country,
                        'option' => $form->option,
                        'description' => $form->description
                    );
                    if (!$headers) {
                        $headers = array_keys($data);
                        fputcsv($handle, $headers);
                    }
                    $row = array();
                    foreach ($headers as $h) {
                        if (array_key_exists($h, $data)) {
                            $row[$h] = $data[$h];
                        } else {
                            $row[$h] = '';
                        }
                    }
                    fputcsv($handle, $row);
                    $i++;
                };
                fputcsv($handle, array("END OF " . $link . " -- " . count($submission) . " RECORDS"));
                fputcsv($handle, array());
                fputcsv($handle, array());
            }
        }
        fclose($handle);

        // make a copy and remove the tmp file
        $file = file_get_contents($temp_file);
        unlink($temp_file);

        // create new file with {slug} name
        $newFile = sys_get_temp_dir().'/'.$fileName;
        file_put_contents($newFile,$file);

        // download
    }

    protected function base64_to_img($base64_string, $output_file){
        if(!file_exists($output_file)){
            touch($output_file);
            chmod($output_file, 0777);
        }
        $ifp = fopen($output_file, "wb");
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        return $output_file;
    }

}