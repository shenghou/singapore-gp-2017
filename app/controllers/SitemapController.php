<?php
/**
 * Created by PhpStorm.
 * User: geraldyeo
 * Date: 19/3/14
 * Time: 9:00 AM
 */

class SitemapController extends BaseController {
	public function generateSitemap() {

    	// create new sitemap object
    	$sitemap = App::make("sitemap");

    	// set cache (key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean))
    	// by default cache is disabled
    	$sitemap->setCache('laravel.sitemap', 3600);

    	$date = '2014-05-27T23:30:00+08:00';
    	$priority = '1.0';
    	$freq = 'daily';

    	// add item to the sitemap (url, date, priority, freq)
    	$sitemap->add(URL::to('/'), $date, $priority, $freq);
    	$sitemap->add(URL::to('on-track'), $date, $priority, $freq);
    	$sitemap->add(URL::to('on-track/2014-race-calendar'), $date, $priority, $freq);
    	$sitemap->add(URL::to('on-track/season-results'), $date, $priority, $freq);
    	$sitemap->add(URL::to('on-track/driver-standings'), $date, $priority, $freq);
    	$sitemap->add(URL::to('on-track/sgp-highlights'), $date, $priority, $freq);
    	$sitemap->add(URL::to('on-track/circuit-park-map'), $date, $priority, $freq);

    	// Off Track
    	$sitemap->add(URL::to('off-track'), $date, $priority, $freq);
    	$sitemap->add(URL::to('off-track/headliners'), $date, $priority, $freq);
    	$sitemap->add(URL::to('off-track/past-headliners'), $date, $priority, $freq);

    	// Tickets
    	$sitemap->add(URL::to('tickets'), $date, $priority, $freq);
    	$sitemap->add(URL::to('tickets/listings'), $date, $priority, $freq);
    	$sitemap->add(URL::to('tickets/map'), $date, $priority, $freq);
    	$sitemap->add(URL::to('tickets/details'), $date, $priority, $freq);
    	$sitemap->add(URL::to('tickets/sales-channels'), $date, $priority, $freq);
    	$sitemap->add(URL::to('tickets/sales-channels/singapore'), $date, $priority, $freq);
    	$sitemap->add(URL::to('tickets/sales-channels/international'), $date, $priority, $freq);
    	$sitemap->add(URL::to('tickets/packages/hotels-and-travel'), $date, $priority, $freq);
    	$sitemap->add(URL::to('tickets/packages/hospitality'), $date, $priority, $freq);
    	$sitemap->add(URL::to('tickets/self-collection-venue'), $date, $priority, $freq);

    	$sitemap->add(URL::to('media'), $date, $priority, $freq);
    	$sitemap->add(URL::to('media/mobile-app'), $date, $priority, $freq);
    	$sitemap->add(URL::to('media/press-release'), $date, $priority, $freq);
    	$sitemap->add(URL::to('media/media-site'), $date, $priority, $freq);
    	$sitemap->add(URL::to('media/accreditation'), $date, $priority, $freq);
    	$sitemap->add(URL::to('media/entry-form'), $date, $priority, $freq);
    	
        
        // Rev Up
    	$sitemap->add(URL::to('rev-up-singapore'), $date, $priority, $freq);

        /* Pulled from RevUpController.php */
        $revUps = RevUpSingapore::where('hidden', '=', 0)->get();
        if (count($revUps) > 0) {
            foreach ($revUps as $revUp) {
                $sitemap->add(URL::to(sprintf('rev-up-singapore/%s', $revUp['slug'])), $date, $priority, $freq);
            }
        }
        // End Rev Up


        // Fanzone
        $fanzoneContests = Contest::where('hidden', '=', 0)->orderBy('sort')->get();
        if (count($fanzoneContests) > 0) {
            foreach ($fanzoneContests as $contest) {
                $sitemap->add(URL::to(sprintf('fanzone/%s', $contest['slug'])), $date, $priority, $freq);
            }
        }
        // End Fanzone


        // Teams and Drivers
        $sitemap->add(URL::to('on-track/teams-and-drivers'), $date, $priority, $freq);

        /* Pulled from OnTrackController.php */
        $drivers = DB::table('team')
            ->leftJoin('driver', function ($join) {
                $join->on('team.id', '=', 'driver.team_id')->where('driver.status', '=', 1);
            })
            ->select('team.full_name AS team_full_name', 'team.name AS team_name', 'team.photo AS team_photo', 'team.seo_url as team_url', 'driver.name AS driver_name', 'driver.photo AS driver_photo', 'driver.seo_url AS driver_url')
            ->where('team.status', '=', 1)
            ->orderBy('team.seq')
            ->orderBy('driver.lead_driver', 'desc')
            ->get();

        /* Pulled from OnTrackController.php and driversandteams.twig */
        if (count($drivers) > 0) {
            $teams = array();
            foreach ($drivers as $driver) {
                $index = str_replace('-', '', $driver->team_url);
                if (!isset($teams[$index])) {
                    $teams[$index] = array();
                }
                $teams[$index][] = $driver;
            }
            foreach ($teams as $team) {
                $leadDriver = $team[0];
                $secondaryDriver = $team[1];
                $sitemap->add(URL::to(sprintf('on-track/teams-and-drivers/%s', $leadDriver->team_url)), $date, $priority, $freq);
                $sitemap->add(URL::to(sprintf('on-track/teams-and-drivers/%s/%s', $leadDriver->team_url, $leadDriver->driver_url)), $date, $priority, $freq);
                $sitemap->add(URL::to(sprintf('on-track/teams-and-drivers/%s/%s', $leadDriver->team_url, $secondaryDriver->driver_url)), $date, $priority, $freq);
            }
        }
        // End Teams and Drivers


        // Headliners
        $sitemap->add(URL::to('off-track/headliners'), $date, $priority, $freq);
        $headliners = $this->getCurrentHeadliners();
        if (count($headliners) > 0) {
            foreach ($headliners as $headliner) {
                if (!empty($headliner->url)) {
                    $sitemap->add(URL::to(sprintf('off-track/headliner/%s', $headliner->url)), $date, $priority, $freq);
                }
                
            }
        }
        // End Headliners


        // Photo Gallery (Just show years)
        // End Photo Gallery

    	$sitemap->add(URL::to('fanzone'), $date, $priority, $freq);
    	$sitemap->add(URL::to('fanzone/pit-stop-challenge'), $date, $priority, $freq);
    	$sitemap->add(URL::to('fanzone/wallpapers'), $date, $priority, $freq);
    	$sitemap->add(URL::to('job-opportunities'), $date, $priority, $freq);
    	$sitemap->add(URL::to('newsletter'), $date, $priority, $freq);
    	$sitemap->add(URL::to('contact-us'), $date, $priority, $freq);
    	$sitemap->add(URL::to('tenders'), $date, $priority, $freq);
    	$sitemap->add(URL::to('privacy-policy'), $date, $priority, $freq);


    	// show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
    	return $sitemap->render('xml');
    }

    private function getCurrentHeadliners() {
        $if = 'if(eh.performance_category like "%Performances on Stage%", 1,'
            .'  if (eh.performance_category like "%Roving Performances%", 2, 3)'
            .')';

        return DB::table('entertainment_highlights AS eh')
                 ->leftJoin('entertainment_highlights_detail AS ehd', 'eh.id', '=', 'ehd.entertainment_id')
                 ->select('eh.id', 'eh.artist', 'eh.additional_label', 'eh.image_thumb', 'eh.image', 'eh.url', DB::raw("GROUP_CONCAT(DISTINCT ehd.date SEPARATOR ', ') AS date"), 'ehd.time', 'ehd.zone', 'ehd.zone_link', 'ehd.zone_list')
                 ->where('eh.year', '=', 'Current')
                 ->where('eh.status', '=', 'Active')
                 ->groupBy('eh.id')
                 ->orderByRaw($if)
                 ->orderBy('eh.sort')
                 ->get();
    }

    private function getPhotoGalleryYears() {
        return DB::table('photo_album')
                 ->select('year')
                 ->groupBy('year')
                 ->orderBy('year', 'desc')
                 ->get();
    }
} 