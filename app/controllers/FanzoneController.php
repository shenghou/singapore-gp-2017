<?php

use MyApp\CheckCountry;

class FanzoneController extends BaseController {

	public function showWelcome() {
        $this->data['angularApp'] = 'revupapp';
        $this->data['isIndian'] = CheckCountry::isIndian();
        return View::make('layouts.fanzone.index', $this->data);
    }



	public function fanzoneMerchandise()
	{
		return View::make('layouts.fanzone.merchandise', $this->data);
	}

} 
