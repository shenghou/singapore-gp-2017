<?php
use MyApp\CheckCountry;


class BaseController extends Controller {
    protected $data;
    protected $section;
    protected $clientProfile;


    function __construct() {
        // route filters
        //$this->beforeFilter('ress', array('on' => 'get', 'except' => 'showRESS'));

        // query strings
        if (Input::has('_s')) {
            $this->section = Input::get('_s');
        }

        // ua & features
        $this->clientProfile = new stdClass();

        if (Agent::isMobile()) {
            $this->clientProfile->deviceType = Agent::isTablet() ? 'tablet' : 'mobile';
        }
        else {
            $this->clientProfile->deviceType = 'desktop';
        }

        if (Session::has('_Modernizr')) {
            $this->clientProfile->browserFeatures = Session::get('_Modernizr');
        }

        // defaults
        $this->data = array(
            'currentRouteName' => Route::currentRouteName(),
            'currentParentRoute' => Request::segment(1),
            'currentUrl' => Request::url(),
            'locale' => App::getLocale(),
            'country' => CheckCountry::countryOfIP(),
            'isIndian' => CheckCountry::isIndian(),

            // control SEB here to turn of/on
            'isSuperEarlyBirdOn' => AppConfig::$isSuperEarlyBirdOn
        );
    }


    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout() {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }


    public function showRESS() {
        return View::make('ress')
                   ->with('referrer_uri', Session::get('referrer_uri'));
    }


    public function logout() {
        Session::flush();
        Redirect::to('/');
    }

    public function show404(){
        App::abort(404);
    }
}