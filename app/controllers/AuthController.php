<?php

use MyApp\Parse\ParseAPI;

class AuthController extends BaseController
{

    public function showLogin()
    {
        return View::make('layouts.auth.login');
    }

    public function postLogin() {

        if (Auth::attempt(array('username'=>Input::get('username'), 'password'=>Input::get('password')))) {
            return Redirect::intended();
        } else {
            return Redirect::to('login')
                ->with('message', 'Your username/password combination was incorrect')
                ->withInput();
        }
    }

    public function postRegister(){
        $plain = Input::get('password');
        $hash = Hash::make($plain);

        $user = new User;
        $user->username = Input::get('username');
        $user->password = $hash;
        $user->save();

        return 'successful';
    }

    public function getLogout(){
        Auth::logout();

        if(Auth::check()){
            return 'not successful';
        }else{
            return 'successfully logged out';
        }
    }

}