<?php

class RaceResultsController extends Controller
{
    private $minutes = 10;

    /*
    |--------------------------------------------------------------------------
    | Race Results Controller
    |--------------------------------------------------------------------------
    */
    public function getIndex()
    {
        $input = Input::all();
        $type = strtolower($input['_t']);

        switch ($type) {
            case 'driver':
            case 'drivers':
                $json = $this->getDrivers();
                break;
            case 'team':
            case 'teams':
                $json = $this->getTeams();
                break;
            case 'race':
                $json = $this->getRace();
                break;
        }

        return $json;
    }


    private function getDrivers()
    {
        $drivers = Cache::remember('drivers_result', $this->minutes, function () {
            return DB::table('drivers_result')
                ->select('driver as name', 'team', 'points')
                ->orderBy('points', 'desc')
                ->take(5)
                ->get();
        });

        return json_encode($drivers);
    }


    private function getTeams()
    {
        $teams = Cache::remember('teams_result', $this->minutes, function () {
            return DB::table('teams_result')
                ->select('team as name', 'points')
                ->orderBy('points', 'desc')
                ->take(5)
                ->get();
        });

        return json_encode($teams);
    }

    private function getRace()
    {
        $race = Cache::remember('race_result', $this->minutes, function () {
            return DB::table('race_result')
                ->select('title as raceCity', 'driver as name', 'time as points')
                ->orderBy('id')
                ->take(5)
                ->get();
        });

        return json_encode($race);
    }
} 