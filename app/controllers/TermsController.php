<?php

class TermsController extends BaseController {

    public function showWelcome() {
        return View::make('layouts.misc.terms', $this->data);
    }
}