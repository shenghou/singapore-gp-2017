<?php

class SurveyController extends BaseController {

    public function paddockClubSurvey() {
        return View::make('layouts.surveys.paddockclub_survey', $this->data);
    }

    public function paddockclubexperiencesurvey() {
        return View::make('layouts.surveys.paddockclub_experience_survey', $this->data);
    }

    public function zone4Survey() {
        return View::make('layouts.surveys.zone4_survey', $this->data);
    }

    public function paddockClubSurveyPost() {
        $input = Input::all();
        SurveySubmission::create(array(
                'survey' => 'Paddock Club Post Survey 2016',
                'data' => json_encode($input)
            )
        );
        $this->data['done'] = true;
        return View::make('layouts.surveys.paddockclub_survey', $this->data);
    }

    /*public function paddockClubSurveyPostExport() {
        $input = Input::all();
        SurveySubmission::find(array(
                'survey' => 'Paddock Club Survey 2016',
                'data' => json_encode($input)
            )
        );
        $this->data['done'] = true;
        return View::make('layouts.surveys.paddockclub_survey', $this->data);
    }*/

    public function paddockClubsurveyExport() {
        $links = array('sgp_cms_latin');

        header("Content-type: text/csv");
        $fileName = 'Surveys_paddockclub_post_survey_' . date('Y_m_d___Hi') . '.csv';
        header(sprintf('Content-Disposition: attachment; filename="%s"', $fileName));
        header("Pragma: no-cache");
        header("Expires: 0");


        // Create a temporary file in the temporary
        // files directory using sys_get_temp_dir()
        $temp_file = tempnam(sys_get_temp_dir(), 'Tux2');

        $handle = fopen($temp_file, 'r+');
        $i = 0;
        $handle = fopen('php://output', 'r+');
        //$headers = null;
        $headers = [];
        $model = new Contest();
        foreach ($links as $link) {
            try {
                $conn = DB::connection($link);
            } catch (\Exception $e) {
                continue;
            }
            if($conn->getDatabaseName()) {
                $submission = DB::connection($link)->table('survey_submissions')->where('survey_submissions.survey', '=', 'Paddock Club Post Survey 2016')->get();
                foreach ($submission as $form){
                    $data = json_decode($form->data, true);
                    $tempHeaders = array_keys($data);
                    for ($x = 0; $x < count($tempHeaders); $x++) {
                        if(!in_array($tempHeaders[$x], $headers)) {
                            array_push($headers, $tempHeaders[$x]);
                        }
                    }
                }

                fputcsv($handle, $headers);

                foreach ($submission as $form) {
                    $data = json_decode($form->data, true);

                    /*if (!$headers) {
                        $headers = array_keys($data);
                        fputcsv($handle, $headers);
                    }*/
                    $row = array();
                    foreach ($headers as $h) {
                        if (array_key_exists($h, $data)) {
                            $row[$h] = $data[$h];
                        } else {
                            $row[$h] = '';
                        }
                    }
                    fputcsv($handle, $row);
                    $i++;
                };
                fputcsv($handle, array("END OF " . $link . " -- " . count($submission) . " RECORDS"));
                fputcsv($handle, array());
                fputcsv($handle, array());
            }
        }
        fclose($handle);

        // make a copy and remove the tmp file
        $file = file_get_contents($temp_file);
        unlink($temp_file);

        // create new file with {slug} name
        $newFile = sys_get_temp_dir().'/'.$fileName;
        file_put_contents($newFile,$file);


        // download
    }

    public function paddockClubExperienceSurveyPost() {
        $input = Input::all();
        SurveySubmission::create(array(
                'survey' => 'Paddock Club Experience Survey 2016',
                'data' => json_encode($input)
            )
        );
        $this->data['done'] = true;
        return View::make('layouts.surveys.paddockclub_experience_survey', $this->data);
    }

    public function paddockClubExperienceSurveyExport() {
        $links = array('sgp_cms_latin');

        header("Content-type: text/csv");
        $fileName = 'Surveys_paddockclub_experience_survey_' . date('Y_m_d___Hi') . '.csv';
        header(sprintf('Content-Disposition: attachment; filename="%s"', $fileName));
        header("Pragma: no-cache");
        header("Expires: 0");


        // Create a temporary file in the temporary
        // files directory using sys_get_temp_dir()
        $temp_file = tempnam(sys_get_temp_dir(), 'Tux2');

        $handle = fopen($temp_file, 'r+');
        $i = 0;
        $handle = fopen('php://output', 'r+');
        //$headers = null;
        $headers = [];
        $model = new Contest();
        foreach ($links as $link) {
            try {
                $conn = DB::connection($link);
            } catch (\Exception $e) {
                continue;
            }
            if($conn->getDatabaseName()) {
                $submission = DB::connection($link)->table('survey_submissions')->where('survey_submissions.survey', '=', 'Paddock Club Experience Survey 2016')->get();
                foreach ($submission as $form){
                    $data = json_decode($form->data, true);
                    $tempHeaders = array_keys($data);
                    for ($x = 0; $x < count($tempHeaders); $x++) {
                        if(!in_array($tempHeaders[$x], $headers)) {
                            array_push($headers, $tempHeaders[$x]);
                        }
                    }
                }

                fputcsv($handle, $headers);

                foreach ($submission as $form) {
                    $data = json_decode($form->data, true);

                    /*if (!$headers) {
                        $headers = array_keys($data);
                        fputcsv($handle, $headers);
                    }*/
                    $row = array();
                    foreach ($headers as $h) {
                        if (array_key_exists($h, $data)) {
                            $row[$h] = $data[$h];
                        } else {
                            $row[$h] = '';
                        }
                    }
                    fputcsv($handle, $row);
                    $i++;
                };
                fputcsv($handle, array("END OF " . $link . " -- " . count($submission) . " RECORDS"));
                fputcsv($handle, array());
                fputcsv($handle, array());
            }
        }
        fclose($handle);

        // make a copy and remove the tmp file
        $file = file_get_contents($temp_file);
        unlink($temp_file);

        // create new file with {slug} name
        $newFile = sys_get_temp_dir().'/'.$fileName;
        file_put_contents($newFile,$file);

        // download
    }

    public function zone4SurveyPost() {
        $input = Input::all();
        SurveySubmission::create(array(
                'survey' => 'Zone 4 Survey 2016',
                'data' => json_encode($input)
            )
        );
        $this->data['done'] = true;
        return View::make('layouts.surveys.zone4_survey', $this->data);
    }

    public function zone4SurveyExport() {
        $links = array('sgp_cms_latin');

        header("Content-type: text/csv");
        $fileName = 'Surveys_Zone4_' . date('Y_m_d___Hi') . '.csv';
        header(sprintf('Content-Disposition: attachment; filename="%s"', $fileName));
        header("Pragma: no-cache");
        header("Expires: 0");


        // Create a temporary file in the temporary
        // files directory using sys_get_temp_dir()
        $temp_file = tempnam(sys_get_temp_dir(), 'Tux2');

        $handle = fopen($temp_file, 'r+');
        $i = 0;
        $handle = fopen('php://output', 'r+');
        //$headers = null;
        $headers = [];
        $model = new Contest();
        foreach ($links as $link) {
            try {
                $conn = DB::connection($link);
            } catch (\Exception $e) {
                continue;
            }
            if($conn->getDatabaseName()) {
                $submission = DB::connection($link)->table('survey_submissions')->where('survey_submissions.survey', '=', 'Zone 4 Survey 2016')->get();
                foreach ($submission as $form){
                    $data = json_decode($form->data, true);
                    $tempHeaders = array_keys($data);
                    for ($x = 0; $x < count($tempHeaders); $x++) {
                        if(!in_array($tempHeaders[$x], $headers)) {
                            array_push($headers, $tempHeaders[$x]);
                        }
                    }
                }
                fputcsv($handle, $headers);

                foreach ($submission as $form) {
                    $data = json_decode($form->data, true);

                   /* if (!$headers) {
                        $headers = array_keys($data);
                        fputcsv($handle, $headers);
                    }*/
                    $row = array();
                    foreach ($headers as $h) {
                        if (array_key_exists($h, $data)) {
                            $row[$h] = $data[$h];
                        } else {
                            $row[$h] = '';
                        }
                    }
                    fputcsv($handle, $row);
                    $i++;
                };
                fputcsv($handle, array("END OF " . $link . " -- " . count($submission) . " RECORDS"));
                fputcsv($handle, array());
                fputcsv($handle, array());
            }
        }
        fclose($handle);

        // make a copy and remove the tmp file
        $file = file_get_contents($temp_file);
        unlink($temp_file);

        // create new file with {slug} name
        $newFile = sys_get_temp_dir().'/'.$fileName;
        file_put_contents($newFile,$file);

        // download
    }


    public function grandstandsWalkaboutsPost() {
        $input = Input::all();
        SurveySubmission::create(array(
                'survey' => 'Grandstands and Walkabouts Survey 2016',
                'data' => json_encode($input)
            )
        );
        $this->data['done'] = true;
        return View::make('layouts.surveys.grandstand_walkabout_survey', $this->data);
    }

    public function grandstandsWalkaboutsExport() {
        $links = array('sgp_cms_latin');

        header("Content-type: text/csv");
        $fileName = 'Surveys_Grandstands_Walkabouts' . date('Y_m_d___Hi') . '.csv';
        header(sprintf('Content-Disposition: attachment; filename="%s"', $fileName));
        header("Pragma: no-cache");
        header("Expires: 0");


        // Create a temporary file in the temporary
        // files directory using sys_get_temp_dir()
        $temp_file = tempnam(sys_get_temp_dir(), 'Tux2');

        $handle = fopen($temp_file, 'r+');
        $i = 0;
        $handle = fopen('php://output', 'r+');
        $headers = null;
        $model = new Contest();
        foreach ($links as $link) {
            try {
                $conn = DB::connection($link);
            } catch (\Exception $e) {
                continue;
            }
            if($conn->getDatabaseName()) {
                $submission = DB::connection($link)->table('survey_submissions')->where('survey_submissions.survey', '=', 'Grandstands and Walkabouts Survey 2016')->get();

                foreach ($submission as $form) {
                    $data = json_decode($form->data, true);

                    if (!$headers) {
                        $headers = array_keys($data);
                        fputcsv($handle, $headers);
                    }
                    $row = array();
                    foreach ($headers as $h) {
                        if (array_key_exists($h, $data)) {
                            $row[$h] = $data[$h];
                        } else {
                            $row[$h] = '';
                        }
                    }
                    fputcsv($handle, $row);
                    $i++;
                };
                fputcsv($handle, array("END OF " . $link . " -- " . count($submission) . " RECORDS"));
                fputcsv($handle, array());
                fputcsv($handle, array());
            }
        }
        fclose($handle);

        // make a copy and remove the tmp file
        $file = file_get_contents($temp_file);
        unlink($temp_file);

        // create new file with {slug} name
        $newFile = sys_get_temp_dir().'/'.$fileName;
        file_put_contents($newFile,$file);

        // download
    }

} 