<?php

class RaceGuidesController extends BaseController {

	public function index()
	{
		$collections = RaceGuideCollection::where('hidden', false)->orderBy('sort', 'asc')->get();
		$this->data['collections'] = $collections;
		return View::make('layouts.raceguides.index', $this->data);
	}

	public function collection($collectionSlug)
	{
		$collection = RaceGuideCollection::where('slug', $collectionSlug)->firstOrFail();
		$guides = $collection->guides;
		if (count($guides) == 1) {
			return Redirect::route('raceguide.collection.guide', array('guideSlug' => $guides[0]->slug));
		};
		$this->data['collection'] = $collection;

		return View::make('layouts.raceguides.index', $this->data);
	}


    public function OnOffTrackSchedulePdf()
    {
            $html = '<h2>Friday</h2>';
            $friday = $this->OnOffTrackSchedule("friday", true);
            $html .= $friday;

            $saturday = $this->OnOffTrackSchedule("saturday", true);
            $html .= '<h2>Saturday</h2>'.$saturday;

            $sunday = $this->OnOffTrackSchedule("sunday", true);
            $html .= '<h2>Sunday</h2>'.$sunday;

            $this->data['html'] = $html;
            return View::make('layouts.raceguides.on_off_track_schedule_pdf', $this->data);
    }

    public function OnOffTrackSchedule($date = "friday", $returnHtml = false)
    {
		switch ($date){
			case "friday": $api_date = "2014-09-19"; break;
			case "saturday": $api_date = "2014-09-20"; break;
			case "sunday": $api_date = "2014-09-21"; break;
			default: $api_date = "all"; break;
		}
        $this->data['date'] = $date;

		$raw = $this->callOnTrackOffTrackAPI($api_date);
        $data = json_decode($raw);

        $clean = array();
//        $timeGroup = array('startTime'=>array(), 'endTime'=>array());
        $dateGroup = array();
        $dateGroupData = array();
        $timeGroup = array();

        foreach($data->results->listing as $row){
            if($row->time=="TBA" || empty($row->time) || $row->date == "0000-00-00" )
                continue;

            // record all `time`
//            $this->addWithoutDuplicate($timeGroup,$row->time);

            // record clean values without TBA
            array_push($clean,$row);
        }

        // Group by date
        foreach($clean as $row){
            $this->addWithoutDup($dateGroup,$row->date);
            $this->addWithoutDup($timeGroup,$row->time);
        }

        // Group data by date
        foreach($dateGroup as $row){
            if(!isset($dateGroupData[$row]))
                $dateGroupData[$row]  = array();

            // put `time` under `date`
            foreach($timeGroup as $time){
                if(!isset($dateGroupData[$row][$time]))
                    $dateGroupData[$row][$time] = array();
            }

            // insert data under each time of current $row date
            foreach($clean as $data){
                if($data->date != $row)
                    continue;

                array_push($dateGroupData[$row][$data->time],$data);
            }
        }

        asort($timeGroup);
//        asort($timeGroup['endTime']);

        $this->data['timeGroup'] = $timeGroup;
        $this->data['dateGroupData'] = $dateGroupData;

//        echo $data;
        $collections = RaceGuideCollection::where('hidden', false)->orderBy('sort', 'asc')->get();
        $this->data['collections'] = $collections;

		// ugly hack
		$data = json_decode($raw);
		$items = array();
		foreach ($data->results->listing as $r) {
			if (isset($r->start_date)) {
				$key = sprintf("%s %s %s", $r->start_date, $r->end_date, $r->type);
			} else {
				$key = sprintf("%s %s-%s", $r->date, $r->start_time, $r->type);
			}

			if (!isset($items[$key])) $items[$key] = array();
			$items[$key][] = $r;
		}

		ksort($items);

		$html = null;
		$cnt = 1;
		foreach ($items as $time => $slots) {
			$listing = null;
			$names = array();
			$types = array();
			$zones = array();
			foreach ($slots as $slot) {
				if ($slot->type == "offtrack") {
					list($name, $country) = explode("(", rtrim($slot->artist, ")"));
					$zone = preg_replace("/zone (\d)(.*)/i", "$1", $slot->zone);
					$clear_zone = preg_replace("/zone \d - /i", "", $slot->zone);
					$listing .= <<<html
                        <li>
                            <div class="ximg">
                                <img src="$slot->thumbnail">
                            </div>
                            <div class="xdesc">
                                <h4>$name</h4>
                                <p>$country</p>
                                <p>Performing In<br>$clear_zone</p>
                                <nav><a href="/off-track/headliner/$slot->url">Read More &gt;</a></nav>
                            </div>
                        </li>
html;
					$use_zone = "ZONE ";

				} else {
					$zone = "ON TRACK ACTIVITY";
					$name = $slot->title;
					$use_zone = null;
				}

				$names[] = $name;
				$types[] = $slot->type;
				$zones[] = $zone;
			}

            // convert from 11:22:00 to 11:22
            preg_match('/(\d{2}:\d{2}):\d{2} - (\d{2}:\d{2}):\d{2}/',$slot->time,$match);

            $slotTime = count($match) >= 3 ? $match[1].' - '.$match[2] : $slot->time;
            // ---

            // put ... at the end of line if too long
			$all_name = implode(", ", $names);
            // $all_name = strlen($all_name) > 70 ? substr_replace($all_name, '...', 70, -1) : $all_name; // transcate if longer than 70
            // ---

            // replace 'ontrack' with 'ON TRACK'
			$all_type = implode(" & ", array_unique($types));
            $all_type = $all_type == "offtrack" ? "OFF TRACK" : "ON TRACK";
            // ---

			sort($zones);
			$all_zone = implode(" & ", array_unique($zones));
			$css = $listing ? "clickable" : null;

            $trackType = $slot->type == "offtrack" ? "offTrack" : "onTrack";
            $triangle = $slot->type == "offtrack" ? "" : "triangle";

			$html .= <<<html
                <div class="row $slot->type">
                    <div class="scheduleBar col {$trackType}">

                        <div class="scheduleContent firstCol">
                            <div class="{$triangle}"></div>
                            {$slotTime}</div>
                        <div class="scheduleContent secondCol $css" onclick="expand_row($cnt)">{$all_name}</div>
                        <div class="scheduleContent thirdCol">{$all_type}</div>
                        <div class="scheduleContent fourthCol">{$use_zone}{$all_zone}</div>
                    </div>
                    <ul class="listing make-space" id="row-$cnt">$listing</ul>
                </div>
html;
			$cnt++;
		}


		$this->data["html"] = $html;
		if ($returnHtml) {
			return $html;
		}
        return View::make('layouts.raceguides.on_off_track_schedule', $this->data);
    }

    public function guide($guideSlug)
	{
		$guide = RaceGuide::where('slug', $guideSlug)->firstOrFail();
		$this->data['guide'] = $guide;
		return View::make('layouts.raceguides.guide', $this->data);
	}

    public function guideApp($guideSlug)
	{
		$guide = RaceGuide::where('slug', $guideSlug)->firstOrFail();
		$this->data['guide'] = $guide;
		return View::make('layouts.raceguides.guide_app', $this->data);
	}


    public function showWelcome() {
       // return View::make('layouts.raceguides.index', $this->data);   event guide landing page do not remove!
		return View::make('layouts.misc.eventguide2016', $this->data);
    }


	public function paddockLanding()
	{
		return View::make('layouts.raceguides.paddockclub.landing', $this->data);
	}

	public function paddockLandingApp()
	{
		$this->data['appVersion'] = 'app';
		return View::make('layouts.raceguides.paddockclub.landing', $this->data);
	}

	public function raceguideTips($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.tips', $this->data);
	}

	public function gettingaround()
	{
		return View::make('layouts.raceguides.gettingaround', $this->data);
	}

	public function transportguide($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.transportguide', $this->data);
	}

	public function paddockMaps()
	{
		return View::make('layouts.raceguides.paddockclub.maps', $this->data);
	}
	public function paddockDirections($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.directions', $this->data);
	}
	public function paddockActivities($category="highlights")
	{
		$this->data['category'] = $category;
		return View::make('layouts.raceguides.paddockclub.activities', $this->data);
	}

	public function paddockActivitiesFnb($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.fnb', $this->data);
	}

	public function paddockActivitiesactivitykiosks($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.activitykiosks', $this->data);
	}

	public function paddockActivitiesentertainment($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.entertainment', $this->data);
	}

	public function paddockActivitiesApp()
	{
		$this->data['appVersion'] = 'app';
		return View::make('layouts.raceguides.paddockclub.activities', $this->data);
	}

		public function paddockUseful($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.useful_info', $this->data);
	}
    public function paddockSchedules($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
        return View::make('layouts.raceguides.paddockclub.schedules', $this->data);
    }
    public function paddockActivitiesTWOJG($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.the-world-of-jean-georges',$this->data);
	}
	public function paddockActivitiesJeanGeorges($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.jean­georges',$this->data);
	}
    public function paddockActivitiesItsukiIzakaya($app = false)
    {
        $this->data['appVersion'] = ($app) ? 'app' : '';
        return View::make('layouts.raceguides.paddockclub.activities.itsuki-izakaya',$this->data);
    }
    public function paddockActivitiesJanicewong($app = false)
    {
        $this->data['appVersion'] = ($app) ? 'app' : '';
        return View::make('layouts.raceguides.paddockclub.activities.janice-wong',$this->data);
    }
	public function paddockActivitieslibrarybar	($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.librarybar',$this->data);
	}


	public function paddockActivitiesflutes($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.flutes',$this->data);
	}

	public function paddockActivitiesnobu($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.nobu',$this->data);
	}
	public function paddockActivitiesTarte($app = false)
    {
        $this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.tarte',$this->data);
    }
    public function paddockActivitiesBasils($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
        return View::make('layouts.raceguides.paddockclub.activities.basils',$this->data);
	}
    public function paddockActivitiesCOMO($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.como-cuisine', $this->data);
	}
	public function paddockActivitiesMBSeafood($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.marina-bay-seafood', $this->data);
	}
    public function paddockActivitiesPitlanewalk($app = false)
    {
        $this->data['appVersion'] = ($app) ? 'app' : '';
        return View::make('layouts.raceguides.paddockclub.activities.pit-lane-walk', $this->data);
    }
	public function paddockActivitiesMBCreamery($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.marina-bay-creamery', $this->data);
	}
	public function paddockActivitiesSingaporeFlyer($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.singapore-flyer', $this->data);
	}
	public function paddockActivitiesMerchandiseBooth($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.merchandise-booth', $this->data);
	}
	public function paddockActivitiesDigitalCaricature($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.digital-caricature', $this->data);
	}
	public function paddockActivitiesCalligraphy($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.calligraphy', $this->data);
	}
    public function paddockActivitiesPhotoopportunities($app = false)
    {
        $this->data['appVersion'] = ($app) ? 'app' : '';
        return View::make('layouts.raceguides.paddockclub.activities.photo-opportunities', $this->data);
    }
	public function paddockActivitiesPalmistry($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.palmistry', $this->data);
	}

	public function paddockActivitiesSillhouttes($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.sillhouttes', $this->data);
	}
	public function paddockActivitiesEntertainmentStage($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.entertainment-stage', $this->data);
	}
	public function paddockActivitiesMarinaBaySpa($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.marina-bay-spa', $this->data);
	}

	public function paddockActivitiesPaddockClubLifestyleStage($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.paddock-club-lifestyle-stage', $this->data);
	}
	public function paddockActivitiesRovingArtistes($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.paddockclub.activities.roving-artistes', $this->data);
	}

	public function hospitalityLanding()
	{
		return View::make('layouts.raceguides.hospitalitysuite.landing', $this->data);
	}

	public function hospitalityLandingApp()
	{
		$this->data['appVersion'] = 'app';
		return View::make('layouts.raceguides.hospitalitysuite.landing', $this->data);
	}

	public function hospitalityDirections($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.hospitalitysuite.directions', $this->data);
	}
	public function hospitalityUseful($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
		return View::make('layouts.raceguides.hospitalitysuite.useful_info', $this->data);
	}
    public function hospitalitySchedules($app = false)
	{
		$this->data['appVersion'] = ($app) ? 'app' : '';
        return View::make('layouts.raceguides.hospitalitysuite.schedules', $this->data);
    }


    private function callOnTrackOffTrackAPI($date = "all")
    {
        $curl = curl_init();
        $headers = array();

        if (Request::root() == 'http://f1.app.sg') {
        	$url = url(sprintf('/mobile-api-v2/index.php/schedule/type/overview/date/%s/zone/all', $date));
        } else {
        	// Live
        	$url = url(sprintf('http://localhost/mobile-api-v2/index.php/schedule/type/overview/date/%s/zone/all', $date));	
        }
        
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        return curl_exec($curl);
    }

    private function addWithoutDuplicate(&$array,$value){
        $two = explode('-',$value);
        if(count($two) < 2)
            return;

        if(!in_array($two[0],$array['startTime'])){
            array_push($array['startTime'],$two[0]);
        }
        if(!in_array($two[1],$array['endTime'])){
            array_push($array['endTime'],$two[1]);
        }
    }

    private function addWithoutDup(&$array,$value){
        if(!in_array($value,$array)){
            array_push($array,$value);
        }
    }

	public function pr($v)
	{
		echo '<pre>';print_r($v);
		exit;
	}
}