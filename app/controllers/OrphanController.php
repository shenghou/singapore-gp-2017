<?php

use MyApp\Parse\ParseAPI;

class OrphanController extends BaseController
{
    public function show2013highlights()
    {
        return View::make('layouts.orphans.sgp2013-highlights', $this->data);
    }

}