<?php

/*
|--------------------------------------------------------------------------
| Miscellaneous Controller
|--------------------------------------------------------------------------
|
| All other pages that don't belong...
|
*/

class MiscellaneousController extends BaseController {

    public function showPrivacyPolicy() {
        $this->data['angularApp'] = 'genericApp';

        return View::make('layouts.misc.privacypolicy', $this->data);
    }

    public function showPrivacyPolicyApp() {
        return View::make('layouts.misc.privacypolicy_app', $this->data);
    }

    public function showNewsletterIframe() {
        $this->data['angularApp'] = 'newsletterApp';
        $this->data['countriesList'] = Countries::getList("name");
        return View::make('layouts.misc.newsletter_iframe', $this->data);
    }

    public function postToNewsletterIframe() {
        $data = array();
        $rules = array();
        $input = Input::all();

        $rules["txtName"] = 'required';
        $rules["txtEmail"] = 'required';

        $validator = Validator::make($input, $rules);

        if ($validator->passes()) {
            $this->insertIntoNewsletter($input);
            $data['success'] = true;
            $data['input'] = $input;
        }
        else {
            $data['success'] = false;
            $data['errors'] = $validator->failed();
        }

        return json_encode($data);
    }

    public function showNewsletter() {
        $this->data['angularApp'] = 'newsletterApp';
        $this->data['countriesList'] = Countries::getList("name");
        return View::make('layouts.misc.newsletter', $this->data);
    }

    public function postToNewsletter() {
        $data = array();
        $rules = array();
        $input = Input::all();

        $rules["txtName"] = 'required';
        $rules["txtEmail"] = 'required';

        $validator = Validator::make($input, $rules);

        if ($validator->passes()) {
            $this->insertIntoNewsletter($input);
            $data['success'] = true;
            $data['input'] = $input;
        }
        else {
            $data['success'] = false;
            $data['errors'] = $validator->failed();
        }

        return json_encode($data);
    }

    public function showTenders() {
        $this->data['angularApp'] = 'genericApp';

        return View::make('layouts.misc.tenders', $this->data);
    }

    public function showHazeReport() {
        $this->data['angularApp'] = 'genericApp';

        return View::make('layouts.misc.haze', $this->data);
    }

    public function showContactUs() {
        $this->data['angularApp'] = 'genericApp';

        return View::make('layouts.misc.contactus', $this->data);
    }



    //@todo: Response::download image
    public function downloadFile($path) {

    }

    public function showJobOpportunities() {
        $this->data['angularApp'] = 'genericApp';

        return View::make('layouts.misc.jobopportunities', $this->data);
    }
    public function showEventguide2016() {
        return View::make('layouts.misc.eventguide2016', $this->data);
    }

    /*public function showpitstopatorchard() {
        return View::make('layouts.misc.pitstopatorchard', $this->data);
    }*/

    public function showRaceschedule() {
        return View::make('layouts.misc.raceschedule', $this->data);
    }

    /*
    |--------------------------------------------------------------------------
    | DB CALLS
    |--------------------------------------------------------------------------
    */

    private function insertIntoNewsletter($newsletter) {

        //Convert latest_news_partners to tinyInt
        $receive_from_partner = $newsletter['latest_news_partners'] ? 1 : 0;

        $newsletterData = array(
            'txtName' => $newsletter['txtName'],
            'txtEmail' => $newsletter['txtEmail'],
            'sYear_of_Birth' => $newsletter['sYear_of_Birth'],
            'sCountry' => $newsletter['sCountry'],
            'attend_which_year' => isset($newsletter['attend_which_year']) ? implode(',', $newsletter['attend_which_year']) : '',
            'with_who' => isset($newsletter['with_who']) ? implode(',', $newsletter['with_who']) : '',
            'with_who_other' => $newsletter['with_who_other'],
            'looking_forward' => isset($newsletter['looking_forward']) ? implode(',', $newsletter['looking_forward']) : '',
            'looking_forward_other' => $newsletter['looking_forward_other'],
            'receive_from_partner' => $receive_from_partner
        );

        DB::connection('newsletter')
            ->table('newsletter')
            ->insert($newsletterData);
    }
} 