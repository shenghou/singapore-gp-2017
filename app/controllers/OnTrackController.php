<?php

/**
 * Created by PhpStorm.
 * User: geraldyeo
 * Date: 19/3/14
 * Time: 8:46 AM
 */
class OnTrackController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | On-Track Controller
    |--------------------------------------------------------------------------
    |
    | On-Track page and its sub-pages
    |
    */

    public function showWelcome()
    {
        return Redirect::route('ontrack.2016Calendar');
    }


    public function showCalendar()
    {
        $this->data['angularApp'] = 'genericApp';
        $this->data['calendar'] = $this->getRaceCalendar();

        return View::make('layouts.ontrack.calendar', $this->data);
    }

    public function showInsights()
    {
        $this->data['angularApp'] = 'genericApp';
        return View::make('layouts.ontrack.insights', $this->data);
    }


    public function showSeasonResults()
    {
        return Redirect::to('http://www.formula1.com/results/season/');
    }

    public function showDriverStandings()
    {
        return Redirect::to('http://www.formula1.com/results/driver/');
    }


    public function showSgpHighlights()
    {
        $this->data['angularApp'] = 'genericApp';
//        $this->data['highlights'] = $this->getHighlights();
//
//        return View::make('layouts.ontrack.sgphighlights', $this->data);
        return View::make('layouts.ontrack.memory_lane', $this->data);
    }


    public function showTeamsAndDrivers()
    {
        $drivers = $this->getTeamsAndDrivers();
        $teams = array();

        foreach ($drivers as $driver) {
            $index = str_replace('-', '', $driver->team_url);
            if (!isset($teams[$index])) {
                $teams[$index] = array();
            }

            $teams[$index][] = $driver;
        }

        $this->data['angularApp'] = 'teamsDriversApp';
        $this->data['teamsAndDrivers'] = $teams;

        return View::make('layouts.ontrack.driversandteams', $this->data);
    }

    public function showDriver($team, $driver)
    {
        $driverInfo = $this->getDriverInfo($driver);
        $driverInfo = $driverInfo[0];

        $this->data['angularApp'] = 'teamsDriversApp';
        $this->data['team'] = $team;
        $this->data['driver'] = $driver;
        $this->data['driverInfo'] = $driverInfo;

        return View::make('layouts.ontrack.drivers', $this->data);
    }

    public function showTeam($team, $driver = null)
    {
        if ($driver) {
            return $this->showDriver($team, $driver);
        } else {
            $teamInfo = $this->getTeamInfo($team);
            $teamInfo = $teamInfo[0];
            $teamDrivers = $this->getTeamDrivers($teamInfo->id);

            $this->data['angularApp'] = 'teamsDriversApp';
            $this->data['team'] = $team;
            $this->data['teamInfo'] = $teamInfo;
            $this->data['teamDrivers'] = $teamDrivers;

            return View::make('layouts.ontrack.teams', $this->data);
        }
    }


    public function showCircuitParkMap()
    {
        $this->data['angularApp'] = 'genericApp';

        return View::make('layouts.ontrack.circuitmap', $this->data);
    }

    public function showGallery($year = 2016, $type = 'photos', $cats = null)
    {
        $gallery = array();

        $categories = PhotoCategory::all();
        $this->data['categories'] = $categories;
        $allCategories = array();
        foreach ($categories as $cat) {
            $allCategories[] = $cat->slug;
        }
        if (!$cats) {
            $cats = implode(',', $allCategories);
        }

        // years
        $yearObjs = $this->getPhotoGalleryYears();
        $years = array();
        foreach ($yearObjs as $yr) {
            $years[] = $yr->year;
        }

        if (!is_numeric($year) || !in_array($year, $years)) {
            if ($year == 2016 && $type == 'videos') {
                //$year = 2014;
            } else {
                return Redirect::route('ontrack.gallery');
            }
        }

        if ($type == 'photos') {
            $cats = explode(',', $cats);
            $photos = $this->getPhotoGallery($year, $cats);
            $gallery = array();
            foreach ($photos as $photo) {
                $key = 'y' . $photo->album->year;

                // year
                if (!isset($gallery[$key])) {
                    $gallery[$key] = array();
                }
                $tempKey = $photo->album->date;
                // date
                if (!array_key_exists($tempKey, $gallery[$key])) {
                    $gallery[$key][$tempKey] = array();
                }

                $gallery[$key][$tempKey][] = $photo->toArray();
                krsort($gallery[$key], SORT_NUMERIC);
                //arsort($gallery[$key], SORT_NUMERIC);
            }
        } else if ($type == 'videos') {
            $gallery = $this->getVideoGallery();
        } else {
            return Redirect::route('ontrack.gallery');
        }
        $this->data['angularApp'] = 'galleryApp';
        $this->data['type'] = $type;
        $this->data['years'] = $years;
        $this->data['gallery'] = $gallery;
        $this->data['allCategories'] = implode(',', $allCategories);

        return View::make('layouts.ontrack.gallery', $this->data);
    }

    public function showVideo($id)
    {
        $video = $this->getVideo($id);
        $video = $video[0];

        if (!empty($video->video_youtube_link)) {
            $urlParts = parse_url($video->video_youtube_link);
            parse_str($urlParts['query'], $queryString);
            $video->yt_id = $queryString['v'];
        }

        $this->data['item'] = $video;

        return View::make('layouts.jwplayer', $this->data);
    }

    /*
    |--------------------------------------------------------------------------
    | DB CALLS
    |--------------------------------------------------------------------------
    */

    private function getRaceCalendar()
    {
        return Cache::rememberForever('race_calendar', function () {
            return DB::table('race_calendar')
                ->select('race', 'country', 'date')
                ->orderBy('id')
                ->get();
        });
    }

    private function getHighlights()
    {
        return DB::table('grand_prix_highlights')
            ->select('date', 'content')
            ->orderBy('date', 'desc')
            ->get();
    }

    private function getTeamsAndDrivers()
    {
        return Cache::rememberForever('teams_and_drivers', function () {
            return DB::table('team')
                ->leftJoin('driver', function ($join) {
                    $join->on('team.id', '=', 'driver.team_id')
                        ->where('driver.status', '=', 1);
                })
                ->select('team.full_name AS team_full_name', 'team.name AS team_name', 'team.photo AS team_photo', 'team.seo_url as team_url', 'driver.name AS driver_name', 'driver.photo AS driver_photo', 'driver.seo_url AS driver_url')
                ->where('team.status', '=', 1)
                ->orderBy('team.seq')
                ->orderBy('driver.lead_driver', 'desc')
                ->get();
        });
    }

    private function getTeamInfo($team)
    {
        $name = 'team_info_' . $team;
        return Cache::rememberForever($name, function () use ($team) {
            return DB::table('team')
                ->where('seo_url', '=', $team)
                ->where('status', '=', 1)
                ->get();
        });
    }

    private function getTeamDrivers($teamId)
    {
        $name = 'team_drivers_' . $teamId;
        return Cache::rememberForever($name, function () use ($teamId) {
            return DB::table('driver')
                ->where('team_id', '=', $teamId)
                ->where('status', '=', 1)
                ->orderBy('lead_driver', 'desc')
                ->get();
        });
    }

    private function getDriverInfo($driver)
    {
        $name = 'driver_info_' . $driver;
        return Cache::rememberForever($name, function () use ($driver) {
            return DB::table('driver')
                ->leftJoin('team', function ($join) {
                    $join->on('team.id', '=', 'driver.team_id')
                        ->where('team.status', '=', 1);
                })
                ->select('team.full_name AS team_full_name', 'team.name AS team_name', 'team.seo_url AS team_seo_url', 'driver.*')
                ->where('driver.status', '=', 1)
                ->where('driver.seo_url', '=', $driver)
                ->get();
        });
    }

    private function getPhotoGalleryYears()
    {
        return DB::table('photo_album')
            ->select('year')
            ->groupBy('year')
            ->orderBy('year', 'desc')
            ->get();
    }

    private function getPhotoGallery($year, $cats)
    {
        $photo = Photo::with('category', 'album')
            ->whereHas('category', function ($q) use ($cats) {
                $q->whereIn('slug', $cats);
            })
            ->whereHas('album', function ($q) use ($year) {
                $q->where('year', $year);
                $q->orderBy('date', 'desc');
            })
            ->orderBy('sort', 'desc')->get();
        return $photo;
    }

    private function getVideoGallery()
    {
        return DB::table('video_gallery_mp4')
            ->select('thumbnail_link', 'video_flv_link', 'title', 'video_link', 'id')
            ->orderBy('sort')
            ->get();
    }

    private function getVideo($id)
    {
        return DB::table('video_gallery_mp4')
            ->select('thumbnail_link', 'video_flv_link', 'title', 'video_link', 'video_ogg_link', 'id', 'video_youtube_link')
            ->where('id', '=', $id)
            ->get();
    }

    public function showRaceSchedule()
    {
        return View::make('layouts.ontrack.raceschedule', $this->data);
    }

    public function showPreSeason()
    {
        return View::make('layouts.ontrack.preseason');
    }

    public function showRuleChange()
    {
        return View::make('layouts.ontrack.rulechange');
    }

    public function showRuleChangeIncrease()
    {
        return View::make('layouts.ontrack.rulechangeincrease');
    }

    public function showPenalties()
    {
        return View::make('layouts.ontrack.penalties');
    }

    public function showRuleChangePreRace()
    {
        return View::make('layouts.ontrack.rulechangeprerace');
    }

    public function showPreSeasonTest2()
    {
        return View::make('layouts.ontrack.preseasontest2');
    }

    public function showPreSeasonTest3()
    {
        return View::make('layouts.ontrack.preseasontest3');
    }

    public function showPhotographerMarkSutton()
    {
        return View::make('layouts.ontrack.photographermarksutton');
    }

    public function showQualifyingReport()
    {
        return View::make('layouts.ontrack.qualifyingreport');
    }

    public function showQualifyingReportMalaysian()
    {
        return View::make('layouts.ontrack.malaysianqualifyingreport');
    }

    public function showQualifyingReportChinese()
    {
        return View::make('layouts.ontrack.chinesequalifyingreport');
    }

    public function showQualifyingReportBahrain()
    {
        return View::make('layouts.ontrack.bahrainqualifyingreport');
    }

    public function showQualifyingReportSpanish()
    {
        return View::make('layouts.ontrack.spanishqualifyingreport');
    }

    public function showQualifyingReportMonaco()
    {
        return View::make('layouts.ontrack.monacoqualifyingreport');
    }

    public function showQualifyingReportCanada()
    {
        return View::make('layouts.ontrack.canadaqualifyingreport');
    }

    public function showQualifyingReportAustrian()
    {
        return View::make('layouts.ontrack.austrianqualifyingreport');
    }

    public function showSuspending()
    {
        return View::make('layouts.ontrack.suspending');
    }

    public function showNextGeneration()
    {
        return View::make('layouts.ontrack.nextgeneration');
    }

    public function showPostRaceReport()
    {
        return View::make('layouts.ontrack.postracereport');
    }

    public function showPostRaceReportMalaysian()
    {
        return View::make('layouts.ontrack.postracereportmalaysian');
    }

    public function showPostRaceReportChinese()
    {
        return View::make('layouts.ontrack.postracereportchinese');
    }

    public function showPostRaceReportBahrain()
    {
        return View::make('layouts.ontrack.postracereportbahrain');
    }

    public function showPostRaceReportSpanish()
    {
        return View::make('layouts.ontrack.postracereportspanish');
    }

    public function showPostRaceReportMonaco()
    {
        return View::make('layouts.ontrack.postracereportmonaco');
    }

    public function showPostRaceReportCanada()
    {
        return View::make('layouts.ontrack.postracereportcanada');
    }

    public function showPostRaceReportAustrian()
    {
        return View::make('layouts.ontrack.postracereportaustrian');
    }

    public function showPostRaceReportBritish()
    {
        return View::make('layouts.ontrack.postracereportbritish');
    }

    public function showVirtualsafetycar()
    {
        return View::make('layouts.ontrack.virtualsafetycar');
    }

    public function showFiaPlansToBring()
    {
        return View::make('layouts.ontrack.fiaplanstobring');
    }

    public function showRookiesAustrianF1()
    {
        return View::make('layouts.ontrack.rookiesaustrianf1');
    }

    public function showSilverstoneracepreview()
    {
        return View::make('layouts.ontrack.silverstoneracepreview');
    }

    public function shootlikeapro()
    {
        return View::make('layouts.ontrack.shootlikeapro');
    }

    public function hungariangpqualifyingreport()
    {
        return View::make('layouts.ontrack.hungariangpqualifyingreport');
    }

    public function hungariangppostracereport()
    {
        return View::make('layouts.ontrack.hungariangppostracereport');
    }

    public function belgiangpqualifyingreport()
    {
        return View::make('layouts.ontrack.2015belgiangpqualifyingreport');
    }

    public function belgiangppostracereport2015()
    {
        return View::make('layouts.ontrack.2015belgiangppost-racereport');
    }

    public function alookintoformulaonedriversgymworkouts()
    {
        return View::make('layouts.ontrack.2015a-look-into-formula-one-drivers-gym-workouts');
    }

    public function monzagpqualifyingreport2015()
    {
        return View::make('layouts.ontrack.2015monzagpqualifyingreport');
    }

    public function monzagppostracereport2015()
    {
        return View::make('layouts.ontrack.2015monzagppostracereport');
    }

    public function qaawithalanjones2015()
    {
        return View::make('layouts.ontrack.2015qaawithalanjones');
    }

    public function formula1singaporeairlinessingaporegrandprixpreview2015()
    {
        return View::make('layouts.ontrack.2015_formula1_singapore_airlines_singapore_grand_prix_preview');
    }

    public function singaporegpracereport2015()
    {
        return View::make('layouts.ontrack.2015_formula1_singapore_airlines_singapore_grand_prix_race_report');
    }

    public function singaporegpsuzukaracepreview2015()
    {
        return View::make('layouts.ontrack.2015sgp_suzuka_race_preview');
    }

    public function singaporegpsuzukapostracereport2015()
    {
        return View::make('layouts.ontrack.2015sgp_suzuka_gp_post-race_report');
    }

    public function russiangpqualifyingreport2015()
    {
        return View::make('layouts.ontrack.2015_russian_gp_qualifying_report');
    }

    public function usgpqualifyingreport2015()
    {
        return View::make('layouts.ontrack.2015_us_gp_qualifying_report');
    }

    public function uspostracereport2015()
    {
        return View::make('layouts.ontrack.2015_us_gp_post_race_report');
    }

    public function mexicogpqualifyingreport()
    {
        return View::make('layouts.ontrack.2015_mexico_gp_qualifying_report');
    }

    public function mexicogppostracereport()
    {
        return View::make('layouts.ontrack.2015_mexico_gp_post_race_report');
    }

    public function braziliangpqualifyingreport()
    {
        return View::make('layouts.ontrack.2015_brazilian_gp_qualifying_report');
    }

    public function braziliangppostracereport()
    {
        return View::make('layouts.ontrack.2015_brazilian_gp_post_race_report');
    }

    public function abudhabigpqualifyingreport()
    {
        return View::make('layouts.ontrack.2015_abudhabi_gp_qualifying_report');
    }

    public function abudhabigppostracereport()
    {
        return View::make('layouts.ontrack.2015_abudhabi_gp_post_race_report');
    }

    public function fivehighlightsof2015f1season()
    {
        return View::make('layouts.ontrack.five_highlights_of_the_2015_Formula_One_Season');
    }

    public function fiveThingstowatchoutforinthe2016f1season()
    {
        return View::make('layouts.ontrack.five_things_to_watch_out_for_in_the_2016_f1_season');
    }

    public function formula1barcelonatest1report()
    {
        return View::make('layouts.ontrack.formula_1_barcelona_test_1_report');
    }

    public function fiveThingsWeLearntFromTheBarcelonaTest()
    {
        return View::make('layouts.ontrack.5_things_we_learnt_from_the_barcelona_test');
    }

    public function newEliminationStyleFormula1Qualifying()
    {
        return View::make('layouts.ontrack.2016_0_New_Elimination-style_Formula_1_Qualifying');
    }

    public function fiveThingsYouMissedAtTheAustralianGP()
    {
        return View::make('layouts.ontrack.2016_1_five_things_you_missed_at_the_australian_gp');
    }

    public function qaWithMartinBrundle()
    {
        return View::make('layouts.ontrack.2016_2_qa_with_martin brundle');
    }
    
    public function fivethingsyoumissedatthebahraingp()
    {
        return View::make('layouts.ontrack.2016_3_five_things_you_missed_at_the_bahrain_gp');
    }

    public function chinesegrandprixhighlights()
    {
        return View::make('layouts.ontrack.2016_4_chinese_grand_prix_highlights');
    }
    public function russiangrandprixhighlights()
    {
        return View::make('layouts.ontrack.2016_5_highlights_of_the_formula1_russian_grand_prix');
    }

    public function thrillsspillsandavictoriousverstappen()
    {
        return View::make('layouts.ontrack.2016_6_thrills_spills_and_a_victorious_verstappen');
    }

    public function highlightsoftheformula1monacograndprix()
    {
        return View::make('layouts.ontrack.2016_7_highlight_of_formula1_monaco_grand_prix');
    }

    public function threekeymomentsoftheformula1canadiangrandprix()
    {
        return View::make('layouts.ontrack.2016_8_three_key_moments_of_the_formula1_canadian_grand_prix');
    }

    public function behindthescenebuildingthemarinabaystreetcircuit()
    {
        return View::make('layouts.ontrack.2016_9_behind_the_scenes_building_the_marina_bay_street_circuit');
    }

    public function wheretogetthemostoutofthesingaporegp()
    {
        return View::make('layouts.ontrack.2016_10_where_to_get_the_most_out_of_the_f1_circuit');
    }

    public function willredbullorferraribethefirsttobeatmercedesonaregularbasis()
    {
        return View::make('layouts.ontrack.2016_11_will_redbull_or_ferrari_be_the_first_to_beat_mercedes_on_a_regular_basis');
    }

    public function europegphighlights2016()
    {
        return View::make('layouts.ontrack.2016_12_europegp_highlights');
    }

    public function austriangphighlights2016()
    {
        return View::make('layouts.ontrack.2016_13_austriangp_highlights');
    }

    public function britishgphighlights2016()
    {
        return View::make('layouts.ontrack.2016_14_britishgp_highlights');
    }

    public function hungarygphighlights2016()
    {
        return View::make('layouts.ontrack.2016_15_hungariangp_highlights');
    }

    public function germangphighlights2016()
    {
        return View::make('layouts.ontrack.2016_16_germanygp_highlights');
    }
    public function belgiumgphighlights2016()
    {
        return View::make('layouts.ontrack.2016_17_belgiumgp_highlights');
    }

    public function italygphighlights2016()
    {
        return View::make('layouts.ontrack.2016_18_italygp_highlights');
    }

    public function jamesallenfeedback2016()
    {
        return View::make('layouts.ontrack.2016_19_james_allen_feedback');
    }

    public function malaysiangphighlights2016()
    {
        return View::make('layouts.ontrack.2016_20_malaysiagp_highlights');
    }
}