<?php namespace MyApp\Parse;

class ParseAPI
{
    const APP_ID = 'ZR824TlIcihwkq4hHXjZmftak8wIzqFmq4EkU2Va';
    const REST_KEY = 'Sm9uWuWRZBz860rgUt2glXrQ7Tdj28WHGpU7Z0sF';
    const END_POINT = 'https://api.parse.com/1/classes/';

    static function Call($method, $className, $data = array())
    {
        $curl = curl_init();
        $headers = array(
            "Content-Type: application/json",
            "X-Parse-Application-Id: " . self::APP_ID,
            "X-Parse-REST-API-Key: " . self::REST_KEY
        );

        $url = sprintf("%s%s", self::END_POINT, $className);

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if (is_array($data)) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if (is_array($data)) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        return curl_exec($curl);
    }
} 