<?php namespace MyApp;
/**
 * Modified by Weileong
 * Date: 8/8/14
 * use geoip2, always update the geoip2 db to the latest release
 */

use GeoIp2\Database\Reader;
use Log;
use Exception;

class CheckCountry extends Exception {

    static function getCountryByIP($ip=null) {
        try {
            $reader = new Reader(base_path('vendor/geoip2/geoip2/db') . '/GeoLite2-Country.mmdb');

            if ($ip != null) {
                $record = $reader->country($ip);
            } else {
                $record = $reader->country(self::getIPofUser());
            }
            if ($record != null && $record->country != null) {
                Log::debug("CheckCountry > getCountryByIP > " . $record->country->isoCode);
                return $record->country->isoCode;
            }
        } catch (Exception $e) {
            Log::error("CheckCountry > getCountryByIP > ".$e);
        }
        return "";
    }

    static function isWhitelist($ip) {

        $whiteList[] = "192.168.61.";
        $whiteList[] = "127.0.0.1";

        if (strlen("$ip")<1) {
            return false;
        }

        for ($i=0; $i<sizeOf($whiteList); $i++) {
            if (strpos($ip, $whiteList[$i])!==false) {
                Log::info("CheckCountry > isWhitelist(".$ip.") found!");
                return true;
            }
        }
        return false;
    }

    static function isCountry($isoCode) {

        if (self::isWhitelist(self::getIPofUser())) return true;

        $currentCountry = self::countryOfIP();

        if ($currentCountry == null || $currentCountry == "") return false;
        if ($currentCountry == $isoCode) return true;
        return false;
    }

    static function countryOfIP()
    {
        $country = self::getCountryByIP();

        return $country;
    }

    static function getIPofUser() {
        $ip = null;
        if(isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }
        return $ip;
    }

    static function isIndian() {
        Log::info("Start IN");
        return self::isCountry("IN");
    }

    static function isAustralian() {
        Log::info("Start AU");
        return self::isCountry("AU");
    }

    static function isNetherlands() {
        Log::info("Start NL");
        return self::isCountry("NL");
    }
}