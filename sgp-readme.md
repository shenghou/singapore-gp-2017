# Singapore GP Site
---
## Getting started
### 1. Development virtual machine  

Requirements:

1. Windows, Linux, or OSX
2. VirtualBox 4.3.*
3. Vagrant 1.4.*

The creation of the development virtual machine (VM) is done using the [Vagrant automation tool](http://www.vagrantup.com/), which makes creating and synchronising development and/or production environment easy and reproducible.
   
The provisioning of the VM is done using [Puppet](http://puppetlabs.com/) as the provisioning backend, and the GUI configurator, [PuPHPet](https://puphpet.com/).
   
These tools help take care of the issues of properly sharing environment among developers, and avoid uploading multi-GB VM files by using a common configuration.

To login to your VM you must use $ vagrant ssh or user vagrant using the private key    automatically generated at puphpet/files/dot/ssh/id_rsa.key. This key is generated after your initial $ vagrant up!
   
1. [Download the latest version of VirtualBox from here](https://www.virtualbox.org/wiki/Downloads)
2. [Download the latest version of Vagrant from here.](http://www.vagrantup.com/downloads.html)

### 2. Back-end stack - Laravel

Requirements:
 
1. [Composer (PHP)](https://getcomposer.org/)
2. [TwigBridge](https://github.com/rcrowe/TwigBridge)

The site uses Laravel as its web application framework, making use of Laravel's routing, and MVC methodology. Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).
   
It is using [Twig](http://twig.sensiolabs.org/) as its templating engine, instead of Laravel's [Blade](http://laravel.com/docs/templates#blade-templating). Twig was chosen for its concise syntax, as well as ease of learning and usage. Since it is also used in Symfony, Laravel may be swapped out for Symfony in future.

The caching and session drivers in Laravel is using memcached, as such, the server will need the relevant driver packages installed for Apache and PHP. The default is 'array'.

_N.B. To run `php artisan [command]` on the vagrant machine, please `vagrant ssh` into login to the webserver and navigate to `\var\www\public` or the website root before running the command. The command will not run properly on the local machine._
   
### 3. Front-end stack - AngularJS

Requirements:

1. [angular-cache](http://jmdobry.github.io/angular-cache/)

The site uses AngularJS for its front-end interactivity. AngularJS makes it easy to extend the HTML vocabulary, instead of using a new syntax or front-end templating engines.

Documentation for the AngularJS API can be found on the [AngularJS website](https://docs.angularjs.org/api).

Angular-cache is used to enable a more feature-rich $cacheFactory for AngularJS. Cache is set to expire or refresh every hour. __Please adjust or remove caching as needed.__

### 4. CSS preprocessor - SASS

The styles for the site is creating with [SASS](http://sass-lang.com/), which makes it easy to create reusable styling.

1. [Install SASS with instructions from here.](http://sass-lang.com/install)

### 5. Task runner - Grunt

[Grunt](http://gruntjs.com/getting-started) is being used to automate the repetitive tasks of compilation, minification, and concatenation of javascript and css files. Gruntfile.js is located at the project root, and contains the configuration of various tasks.

1. Install [node](http://nodejs.org/) and [npm](https://npmjs.org/)
2. npm install -g grunt-cli

### 6. Package manager for the web - Bower

[Bower](http://bower.io/) is a package manager for the web. It offers a generic, unopinionated solution to the problem of __front-end package management__, while exposing the package dependency model via an API that can be consumed by a more opinionated build stack. There are no system wide dependencies, no dependencies are shared between different apps, and the dependency tree is flat.

1. Install [node](http://nodejs.org/) and [npm](https://npmjs.org/)
2. npm install -g bower
3. Edit .bowerrc file as needed to update installation location of bower components

### 7. Source code version control - Git  

    git@bitbucket.org:craftandcode/singapore-gp.git
    
Git is the version control of choice, and the development/staging environment contains the bare repository, and the post-receive hook for staging to deployment.

    #!/bin/sh
    git --work-tree=/home/cms/www --git-dir=/var/repo/site.git checkout -f

---
## Project structure

The project structure listed below is the commonly accessed folders during development. There will be other Laravel and NodeJS specific folders and files which can be left alone.

    root/
    |
    |-- app/
    |  |-- config/            # Laravel configs
    |  |-- controllers/       # Controllers for routes
    |  |-- js/                # AngularJS files
    |  |-- lang/              # Localisation files
    |  |-- models/            # Model schemas for database
    |  |-- scss/              # SASS files
    |  |-- views/             # View templates
    |  `-- routes.php         # Site routes
    |
    |-- public/
    |  |-- bower_components/  # Bower.io components
    |  |-- images/            # Optimised images
    |  |-- javascripts/       # Generated js files
    |  |-- stylesheets/       # Generated css files
    |
    `-- index.php             # Laravel app entry point

### Route controllers

The controllers are used to manage the data for views, and therefore calls to the database can be found in controllers. The database methods can be moved into models, to keep the controllers clean.

The use cases for resources include teams and drivers details, race news, press releases, entertainment news, contests, etc.

Instead of creating a separate resource route for each of the use cases, i have initially chosen to integrate the databse calls with the route controllers. The presentation layers have also been removed (interweaving html tag elements with the data), unlike the old code, to keep the presentation and business logic separated. As such, you will find that the data is injected into the view templates on creation:

    View::make('layouts.offtrack.headliners', $this->data);

You can inspect the raw data by printing it out to screen, or using [xdebug](http://xdebug.org/) if available.

### Database migrations

[Migrations](http://laravel.com/docs/migrations) are a type of version control for the database. To synchronise both development and production databases, run the migrations using Artisan CLI:

    php artisan migrate

Migrations can also be rolled back:

    // Rollback The Last Migration Operation
    php artisan migrate:rollback 
    
    // Rollback all migrations
    php artisan migrate:reset 
    
    // Rollback all migrations and run them all again
    php artisan migrate:refresh
    php artisan migrate:refresh --seed

### Views

These are the Twig templates that make up the main layout and also the partials that are used to create the different pages.

    /root/app/views/
    |
    |-- partials/
    |  |-- tickets/
    |     `-- ticketsdetails.twig
    |     `-- table.twig
    |     `-- selectionpanel.twig
    |     `-- salessgselectionpanel.twig
    |     `-- salesi11lselectionpanel.twig
    |     `-- rowthreeday.twig
    |     `-- rowhospitality.twig
    |     `-- row.twig
    |     `-- fineprint.twig
    |  
    |  |-- home/
    |     `-- socialbucket.twig
    |     `-- smallbucket.twig
    |     `-- sectionsocial.twig
    |     `-- sectionontrack.twig
    |     `-- section.twig
    |     `-- resultbucket.twig
    |     `-- mediumbucket.twig
    |     `-- largebucket.twig
    |     `-- contentbucket.twig
    |  
    |  `-- sitemap.twig
    |  `-- quicklinks.twig
    |  `-- navigation.twig
    |  `-- masthead.twig
    |  `-- copyright.twig
    |
    |-- layouts/
    |  |-- win/
    |     `-- ...
    |
    |  |-- tickets/
    |     `-- ...
    |
    |  |-- revup/
    |     `-- ...
    |
    |  |-- ontrack/
    |     `-- ...
    |
    |  |-- offtrack/
    |     `-- ...
    |
    |  |-- misc/
    |     `-- ...
    |
    |  |-- media/
    |     `-- ...
    |
    |  |-- home/
    |     `-- ...
    |
    |  `-- master.twig
    |  `-- jwplayer.twig
    |  `-- gallery.twig
    |
    `-- ress.twig

---

## AngularJS 

### Controllers

All of the controllers handles the data to be presented in the views, for data binding to the view elements. Any DOM manipulation are done in directives.

### Directives

This sections details the different modules, controllers and directives used in different pages of the site. The javascripts makes use of [Browserify](http://browserify.org/), which allows code to be written in the same way as NodeJS, requiring modules with the `require` method.

#### Responsive Menu

    ResponsiveMenu/
    |
    `-- ComponentDirective.js
    `-- index.js

The directive contains the eventlisteners for `click` and `touchstart` to handle opening of the menu and closing when clicked on spaces outside of the menu. It also handles closing if the search is open.

#### Search Bar

    SearchBar/
    |
    `-- ComponentDirective.js
    `-- index.js

The directive handles the opening and closing of the search bar; it also closes if the menu is open.

#### Home - Results Tab Component

    ResultsTabComponent/
    |
    `-- ComponentController.js    # Controller
    `-- ComponentDirective.js     # Directive
    `-- ComponentService.js       # Web service model
    `-- index.js                  # Entry point
    `-- template.html             # Template for tab content

The `ComponentService.js` module contains the methods to query the Race Results API found at route `/results`. The directive in `ComponentDirective.js` compiles the template found in `template.html`.

#### Ticket Details - Photo Gallery

    PhotoGallery/
    |
    `-- ComponentDirective.js
    `-- index.js

The directive handles the pagination and animation of the gallery, as well as linking up each photo with the FancyBox modal app.

#### Ticket Details - Panorama Viewer

    PanoramaViewer/
    |
    `-- ComponentDirective.js
    `-- index.js
    
The directive handles the animation of the viewer.

#### Misc - Fancy Gallery

The directive links up `fancybox` class to FancyBox modal app.

#### Misc - Date Picker & Date Time Picker

The directives link up each specified input element with the [jQuery UI Timepicker addon](http://trentrichardson.com/examples/timepicker/), as well as any plugin options.

#### Misc - Google Map

The directive link up the target element with a Google Map embed. The code is lifted from the old site.

#### Misc - Ticket Statuses

    services/
    |
    `-- TicketStatusService.js
    
The directive queries the Parse end point for the ticket statuses, and adds the css classes to change the various button states.

#### Misc - Mason Link

The directive adds a `click` handler to the whole element. Used for ticketing agents and hotels & travel packages mason square.

