! function e(n, t, o) {
    function a(r, s) {
        if (!t[r]) {
            if (!n[r]) {
                var c = "function" == typeof require && require;
                if (!s && c) return c(r, !0);
                if (i) return i(r, !0);
                throw new Error("Cannot find module '" + r + "'")
            }
            var l = t[r] = {
                exports: {}
            };
            n[r][0].call(l.exports, function(e) {
                var t = n[r][1][e];
                return a(t ? t : e)
            }, l, l.exports, e, n, t, o)
        }
        return t[r].exports
    }
    for (var i = "function" == typeof require && require, r = 0; r < o.length; r++) a(o[r]);
    return a
}({
    1: [function(e, n, t) {
        window.angular;
        n.exports = function(e) {
            var n = {};
            return n.message = "", n.prepForBroadcast = function(e) {
                this.message = e, this.broadcastItem()
            }, n.broadcastItem = function() {
                e.$broadcast("handleBroadcast")
            }, n
        }
    }, {}],
    2: [function(e, n, t) {
        window.angular;
        n.exports = function(e) {
            var n = function(e) {
                    function n(o) {
                        console.log("body clicked", o.target), t(e), $(this).off("click", n), $(this).off("touchstart", n)
                    }
                    e.addClass("toggled"), $(".content").on("click", n), $(".content").on("touchstart", n)
                },
                t = function(e) {
                    e.removeClass("toggled"), $("html,body").scrollTop(0)
                },
                o = function(o, a, i) {
                    function r(a) {
                        a.stopPropagation();
                        var i = e.matchMedia("(max-width: 767px)");
                        i.matches && (s.hasClass("toggled") ? s.hasClass("toggled") && (a.preventDefault(), t(s)) : (a.preventDefault(), n(s), o.handleClick("menu:opened")), !$(a.target).is("a"))
                    }
                    var s = a.children("ul"),
                        c = $("#pull");
                    o.menu = s, c.on("click", r), c.on("touchstart", r)
                },
                a = function(e, n) {
                    e.handleClick = function(e) {
                        n.prepForBroadcast(e)
                    }, e.$on("handleBroadcast", function() {
                        "search:opened" == n.message && t(e.menu)
                    })
                };
            return {
                restrict: "A",
                link: o,
                controller: ["$scope", "eventBusService", a]
            }
        }
    }, {}],
    3: [function(e, n, t) {
        var o = window.angular;
        n.exports = o.module("responsiveMenuComponent", []).factory("eventBusService", ["$rootScope", e("../EventBus")]).directive("responsiveMenu", ["$window", e("./ComponentDirective")])
    }, {
        "../EventBus": 1,
        "./ComponentDirective": 2
    }],
    4: [function(e, n, t) {
        window.angular;
        n.exports = function() {
            function e() {
                var e = !1;
                return function(n) {
                    (/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(n) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(n.substr(0, 4))) && (e = !0)
                }(navigator.userAgent || navigator.vendor || window.opera), e
            }
            var n = function(n, o) {
                    function a(e) {
                        console.log("body clicked"), t(n, o), $(this).off("click", a), $(this).off("touchstart", a)
                    }
                    n.addClass("sb-search-open"), e() || o.focus(), $(document).on("click", a), $(document).on("touchstart", a)
                },
                t = function(e, n) {
                    n.blur(), e.removeClass("sb-search-open")
                },
                o = function(e, o, a) {
                    function i(a) {
                        a.stopPropagation(), s.val($.trim(s.val())), o.hasClass("sb-search-open") ? o.hasClass("sb-search-open") && /^\s*$/.test(s.val()) && (a.preventDefault(), t(o, s)) : (a.preventDefault(), n(o, s), e.handleClick("search:opened"))
                    }

                    function r(e) {
                        e.stopPropagation()
                    }
                    var s = o.find("form > input.sb-search-input");
                    e.elem = o, e.input = s, o.on("click", i), o.on("touchstart", i), s.on("click", r), s.on("touchstart", r)
                },
                a = function(e, n, o) {
                    e.search = {
                        q: ""
                    }, e.submitSearch = function() {
                        if (e.searchForm.$valid && e.search.q.length > 0) {
                            var t = encodeURIComponent("site:www.singaporegp.sg " + e.search.q);
                            n.location.href = "http://www.google.com/search?q=" + t
                        }
                    }, e.handleClick = function(e) {
                        o.prepForBroadcast(e)
                    }, e.$on("handleBroadcast", function() {
                        "menu:opened" == o.message && t(e.elem, e.input)
                    })
                };
            return {
                restrict: "A",
                link: o,
                controller: ["$scope", "$window", "eventBusService", a]
            }
        }
    }, {}],
    5: [function(e, n, t) {
        var o = window.angular;
        n.exports = o.module("searchBarComponent", []).factory("eventBusService", ["$rootScope", e("../EventBus")]).directive("searchBar", [e("./ComponentDirective")])
    }, {
        "../EventBus": 1,
        "./ComponentDirective": 4
    }],
    6: [function(e, n, t) {
        function o(e) {
            var n = new Object;
            if (!e) return n;
            for (var t = e.split(/[;&]/), o = 0; o < t.length; o++) {
                var a = t[o].split("=");
                if (a && 2 == a.length) {
                    var i = unescape(a[0]),
                        r = unescape(a[1]);
                    r = r.replace(/\+/g, " "), n[i] = r
                }
            }
            return n
        }
        var a = document.getElementsByTagName("script"),
            i = a[a.length - 1],
            r = i.src.replace(/^[^\?]+\??/, ""),
            s = o(r),
            c = window.angular;
        c.module("winContestApp", ["ngSanitize", e("./ResponsiveMenu").name, e("./SearchBar").name]).directive("validFile", function() {
            return {
                require: "ngModel",
                link: function(e, n, t, o) {
                    n.bind("change", function() {
                        e.$apply(function() {
                            o.$setViewValue(n.val()), o.$render()
                        })
                    })
                }
            }
        }).controller("contestController", ["$scope", "$window", "$http", function(e, n, t) {
            e.submitted = !1, e.contest = {
                from_where: "website",
                fileupload: ""
            }, e.submitForm = function() {
                var validForm = true;
                $("#contestForm input[type=file]").each(function() {
                    var file = this.files[0];
                    if (this.files.length ==0) {
                        $(this).parent().siblings('.text-error').find('span.error-msg').html('Please attach a file');
                        $(this).parent().siblings('.text-error').find('span.error-msg').show();
                        validForm = false;
                    }else if(this.files.length != 0 && file && file.size > 20971520){
                        $(this).parent().siblings('.text-error').find('span.error-msg').html('Please attach a file with size smaller than 20MB');
                        $(this).parent().siblings('.text-error').find('span.error-msg').show();
                        validForm = false;
                    }
                });
                if(validForm) {
                    e.contest.file = function () {
                        return $("#files").val()
                    }, e.contestForm.$valid && t({
                        method: "POST",
                        url: s.postUrl,
                        data: $.param(e.contest),
                        headers: {
                            "Content-Type": "application/x-www-form-urlencoded"
                        }
                    }).success(function (n) {
                        console.log(n), void 0 !== n && n.success ? e.submitted = n.success : alert(n.errors)
                    })
                }
            };
            var o, a, i = n.location.pathname.split("/"),
                r = "";
            a = "/";

            for (var c = 0; c < i.length; c++) o = i[c], "strun" == o && (o = "Straits-Times-Run-Contest"), "pitstopatorchard" == o && (o = "Pit Stop @ Orchard"), "my-sgp-moment" == o && (o = "My SGP Moment"),"sgp-fan-experience" == o && (o = "Singapore Grand Prix Fan Experience"), "drivers-autograph-session" == o && (o = "Drivers'-Autograph-Session"), o.length > 0 && (a += o + "/", o = decodeURI(o.split("-").join(" ").replace(" and ", " &amp; ")), c < i.length - 1 && (o = "<a href='" + a + "'>" + o + "</a>"), r += o, c < i.length - 1 && (r += "<span>&rang;</span>"));
            e.breadcrumbs = r, e.contest.option = null, e.artistes = ["Kylie Minogue", "Queen + Adam Lambert", "Imagine" +
            " Dragons",  "Bastille", "Halsey",  "KC And The Sunshine Band", "Pentatonix"], e.artistesGetUpClose = ["Bon Jovi", "Jimmy Cliff", " Dirty Loops"], e.artistesStraitsTimes = ["Sebastian Vettel", "Lewis Hamilton", "Fernando Alonso"], e.contest.description = null

        }])
    }, {
        "./ResponsiveMenu": 3,
        "./SearchBar": 5
    }]
}, {}, [6]);