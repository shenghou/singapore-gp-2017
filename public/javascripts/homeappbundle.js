(function e(t, n, r) {
    function s(o, u) {
        if (!n[o]) {
            if (!t[o]) {
                var a = typeof require == "function" && require;
                if (!u && a) return a(o, !0);
                if (i) return i(o, !0);
                throw new Error("Cannot find module '" + o + "'")
            }
            var f = n[o] = {
                exports: {}
            };
            t[o][0].call(f.exports, function(e) {
                var n = t[o][1][e];
                return s(n ? n : e)
            }, f, f.exports, e, t, n, r)
        }
        return n[o].exports
    }
    var i = typeof require == "function" && require;
    for (var o = 0; o < r.length; o++) s(r[o]);
    return s
})({
    1: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = function(a) {
            var r = {};
            return r.message = "", r.prepForBroadcast = function(a) {
                this.message = a, this.broadcastItem()
            }, r.broadcastItem = function() {
                a.$broadcast("handleBroadcast")
            }, r
        };
    }, {}],
    2: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = function(t) {
            var n = function(t) {
                    function n(e) {
                        console.log("body clicked", e.target), o(t), $(this).off("click", n), $(this).off("touchstart", n)
                    }
                    t.addClass("toggled"), $(".content").on("click", n), $(".content").on("touchstart", n)
                },
                o = function(t) {
                    t.removeClass("toggled"), $("html,body").scrollTop(0)
                },
                e = function(e, a) {
                    function c(a) {
                        a.stopPropagation();
                        var c = t.matchMedia("(max-width: 767px)");
                        c.matches && (l.hasClass("toggled") ? l.hasClass("toggled") && (a.preventDefault(), o(l)) : (a.preventDefault(), n(l), e.handleClick("menu:opened")), !$(a.target).is("a"))
                    }
                    var l = a.children("ul"),
                        r = $("#pull");
                    e.menu = l, r.on("click", c), r.on("touchstart", c)
                },
                a = function(t, n) {
                    t.handleClick = function(t) {
                        n.prepForBroadcast(t)
                    }, t.$on("handleBroadcast", function() {
                        "search:opened" == n.message && o(t.menu)
                    })
                };
            return {
                restrict: "A",
                link: e,
                controller: ["$scope", "eventBusService", a]
            }
        };
    }, {}],
    3: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = angular.module("responsiveMenuComponent", []).factory("eventBusService", ["$rootScope", require("../EventBus")]).directive("responsiveMenu", ["$window", require("./ComponentDirective")]);
    }, {
        "../EventBus": 1,
        "./ComponentDirective": 2
    }],
    4: [function(require, module, exports) {
        module.exports = function(e, r) {
            e.currentTab = 0, e.tab1Name = "Race", e.tab2Name = "Driver", e.tab3Name = "Team", e.isTab1 = function() {
                return 1 == e.currentTab
            }, e.isTab2 = function() {
                return 2 == e.currentTab
            }, e.isTab3 = function() {
                return 3 == e.currentTab
            }, e.setCurrentTab = function(t) {
                switch (e.currentTab = t, e.deferred && e.deferred.reject(new Error("Cancelled")), t) {
                    case 1:
                        e.deferred = r.getRace(), e.deferred.promise.then(function(r) {
                            r = r || "null" !== r ? r : {}, e.resultsData = {
                                colgrps: ["15%", "55%", "30%"],
                                headers: ["Pos", "Driver", "Time"],
                                results: r
                            }, e.title = "JAPAN", e.url_link = "https://www.formula1.com/en/results.html/2016/races/953/japan/race-result.html", e.deferred = null
                        }, function(r) {
                            console.error(r), e.deferred = null
                        });
                        break;
                    case 2:
                        e.deferred = r.getDrivers(), e.deferred.promise.then(function(r) {
                            r = r || "null" !== r ? r : {}, e.resultsData = {
                                colgrps: ["15%", "35%", "35%", "15%"],
                                headers: ["Pos", "Driver", "Team", "Points"],
                                results: r
                            }, e.title = "FORMULA 1™ Season 2016 - Driver", e.url_link = "http://www.formula1.com/content/fom-website/en/results.html/2016/drivers.html", e.deferred = null
                        }, function(r) {
                            console.error(r), e.deferred = null
                        });
                        break;
                    case 3:
                        e.deferred = r.getTeams(), e.deferred.promise.then(function(r) {
                            r = r || "null" !== r ? r : {}, e.resultsData = {
                                colgrps: ["15%", "70%", "15%"],
                                headers: ["Pos", "Team", "Points"],
                                results: r
                            }, e.title = "FORMULA 1™ Season 2016 - Team", e.url_link = "http://www.formula1.com/content/fom-website/en/results.html/2016/team.html", e.deferred = null
                        }, function(r) {
                            console.error(r), e.deferred = null
                        })
                }
            }, e.setCurrentTab(1)
        };
    }, {}],
    5: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = function(n, a) {
            var e = function(e, r) {
                var l = angular.element(a);
                l = n(l)(e), angular.element(r[0]).append(l)
            };
            return {
                restrict: "A",
                replace: !0,
                link: e
            }
        };
    }, {}],
    6: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = function(e, r, t) {
            var s = "/",
                c = t("resultsCache"),
                u = function() {
                    var t = r.defer(),
                        u = s + "results?_t=drivers";
                    return e({
                        method: "GET",
                        url: u,
                        cache: c
                    }).success(t.resolve).error(t.reject), t
                },
                a = function() {
                    var t = r.defer(),
                        u = s + "results?_t=teams";
                    return e({
                        method: "GET",
                        url: u,
                        cache: c
                    }).success(t.resolve).error(t.reject), t
                },
                o = function() {
                    var t = r.defer(),
                        u = s + "results?_t=race";
                    return e({
                        method: "GET",
                        url: u,
                        cache: c
                    }).success(t.resolve).error(t.reject), t
                };
            return {
                getRace: o,
                getDrivers: u,
                getTeams: a
            }
        };
    }, {}],
    7: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = angular.module("sgpDirectives", []).constant("template", require("./template.html")).directive("resultsTabs", ["$compile", "template", require("./ComponentDirective")]).controller("RaceResultsController", ["$scope", "RaceResultsService", require("./ComponentController")]);
    }, {
        "./ComponentController": 4,
        "./ComponentDirective": 5,
        "./template.html": 8
    }],
    8: [function(require, module, exports) {
        module.exports = '<nav>\n    <ul>\n        <li data-ng-class="{active: isTab1()}"><a data-ng-click="setCurrentTab(1)">{{ tab1Name }}</a></li><li data-ng-class="{active: isTab2()}"><a data-ng-click="setCurrentTab(2)">{{ tab2Name }}</a></li><li data-ng-class="{active: isTab3()}"><a data-ng-click="setCurrentTab(3)">{{ tab3Name }}</a></li>\n    </ul>\n</nav>\n<div class="content">\n    <h4>{{ title }}</h4>\n    <table>\n        <caption></caption>\n        <colgroup data-ng-repeat="col in resultsData.colgrps track by $index" width="{{ col }}"></colgroup>\n        <thead>\n        <tr>\n            <th data-ng-repeat="header in resultsData.headers">{{ header }}</th>\n        </tr>\n        </thead>\n        <tbody>\n        <tr data-ng-repeat="result in resultsData.results">\n            <td>{{ $index + 1 }}</td>\n            <td>{{ result.name }}</td>\n            <td data-ng-if="result.team">{{ result.team }}</td>\n            <td>{{ result.points }}</td>\n        </tr>\n        </tbody>\n    </table>\n    <p><a ng-href="{{ url_link }}" target="_blank">Read more</a></p>\n</div>';
    }, {}],
    9: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = function() {
            function o() {
                var o = !1;
                return function(e) {
                    (/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(e) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(e.substr(0, 4))) && (o = !0)
                }(navigator.userAgent || navigator.vendor || window.opera), o
            }
            var e = function(e, i) {
                    function t() {
                        console.log("body clicked"), a(e, i), $(this).off("click", t), $(this).off("touchstart", t)
                    }
                    e.addClass("sb-search-open"), o() || i.focus(), $(document).on("click", t), $(document).on("touchstart", t)
                },
                a = function(o, e) {
                    e.blur(), o.removeClass("sb-search-open")
                },
                i = function(o, i) {
                    function t(t) {
                        t.stopPropagation(), c.val($.trim(c.val())), i.hasClass("sb-search-open") ? i.hasClass("sb-search-open") && /^\s*$/.test(c.val()) && (t.preventDefault(), a(i, c)) : (t.preventDefault(), e(i, c), o.handleClick("search:opened"))
                    }

                    function n(o) {
                        o.stopPropagation()
                    }
                    var c = i.find("form > input.sb-search-input");
                    o.elem = i, o.input = c, i.on("click", t), i.on("touchstart", t), c.on("click", n), c.on("touchstart", n)
                },
                t = function(o, e, i) {
                    o.search = {
                        q: ""
                    }, o.submitSearch = function() {
                        if (o.searchForm.$valid && o.search.q.length > 0) {
                            var a = encodeURIComponent("site:www.singaporegp.sg " + o.search.q);
                            e.location.href = "http://www.google.com/search?q=" + a
                        }
                    }, o.handleClick = function(o) {
                        i.prepForBroadcast(o)
                    }, o.$on("handleBroadcast", function() {
                        "menu:opened" == i.message && a(o.elem, o.input)
                    })
                };
            return {
                restrict: "A",
                link: i,
                controller: ["$scope", "$window", "eventBusService", t]
            }
        };
    }, {}],
    10: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = angular.module("searchBarComponent", []).factory("eventBusService", ["$rootScope", require("../EventBus")]).directive("searchBar", [require("./ComponentDirective")]);
    }, {
        "../EventBus": 1,
        "./ComponentDirective": 9
    }],
    11: [function(require, module, exports) {
        require("flipclock");
        var angular = window.angular,
            angularCache = require("angularCache"),
            clock;
        $(function() {
            var e = new Date,
                a = new Date(2016, 8, 18, 20); //upcoming race date

            /*  hide auto recalculate for next year upcoming race
             fs = new Date(2016, 8, 19, 00), // next upcoming race date
             f = new Date(2017, 9, 1, 20), // next upcoming race date
             r = a.getTime() / 1e3 - e.getTime() / 1e3;
             if(e >= fs ) {
             r = f.getTime() / 1e3 - e.getTime() / 1e3;
             } else if(e > a) {
             r = e.getTime() / 1e3 - e.getTime() / 1e3;
             }*/
            r = a.getTime() / 1e3 - e.getTime() / 1e3;

            if(e < a) {
                clock = $(".clock").FlipClock(r, {
                    clockFace: "DailyCounter",
                    countdown: !0,
                    showSeconds: !1
                })
            }else {
                $('.clock-wrapper').hide();
                $('.clock-info').hide();
            }
        }), angular.module("homeApp", ["jmdobry.angular-cache", require("./ResultsTabsComponent").name, require("./ResponsiveMenu").name, require("./SearchBar").name]).config(["$angularCacheFactoryProvider", function(e) {
            e.setCacheDefaults({
                maxAge: 120,
                cacheFlushInterval: 120,
                storageMode: "localStorage"
            })
        }]).factory("RaceResultsService", ["$http", "$q", "$angularCacheFactory", require("./ResultsTabsComponent/ComponentService")]);
    }, {
        "./ResponsiveMenu": 3,
        "./ResultsTabsComponent": 7,
        "./ResultsTabsComponent/ComponentService": 6,
        "./SearchBar": 10,
        "angularCache": 13,
        "flipclock": 12
    }],
    12: [function(require, module, exports) {
        (function(global) {
            __browserify_shim_require__ = require,
                function(t, i, e, s, n) {
                    var r = function() {};
                    r.extend = function(t, i) {
                        "use strict";
                        var e = r.prototype.extend;
                        r._prototyping = !0;
                        var s = new this;
                        e.call(s, t), s.base = function() {}, delete r._prototyping;
                        var n = s.constructor,
                            o = s.constructor = function() {
                                if (!r._prototyping)
                                    if (this._constructing || this.constructor == o) this._constructing = !0, n.apply(this, arguments), delete this._constructing;
                                    else if (null !== arguments[0]) return (arguments[0].extend || e).call(arguments[0], s)
                            };
                        return o.ancestor = this, o.extend = this.extend, o.forEach = this.forEach, o.implement = this.implement, o.prototype = s, o.toString = this.toString, o.valueOf = function(t) {
                            return "object" == t ? o : n.valueOf()
                        }, e.call(o, i), "function" == typeof o.init && o.init(), o
                    }, r.prototype = {
                        extend: function(t, i) {
                            if (arguments.length > 1) {
                                var e = this[t];
                                if (e && "function" == typeof i && (!e.valueOf || e.valueOf() != i.valueOf()) && /\bbase\b/.test(i)) {
                                    var s = i.valueOf();
                                    i = function() {
                                        var t = this.base || r.prototype.base;
                                        this.base = e;
                                        var i = s.apply(this, arguments);
                                        return this.base = t, i
                                    }, i.valueOf = function(t) {
                                        return "object" == t ? i : s
                                    }, i.toString = r.toString
                                }
                                this[t] = i
                            } else if (t) {
                                var n = r.prototype.extend;
                                r._prototyping || "function" == typeof this || (n = this.extend || n);
                                for (var o = {
                                    toSource: null
                                }, a = ["constructor", "toString", "valueOf"], c = r._prototyping ? 0 : 1; h = a[c++];) t[h] != o[h] && n.call(this, h, t[h]);
                                for (var h in t) o[h] || n.call(this, h, t[h])
                            }
                            return this
                        }
                    }, r = r.extend({
                        constructor: function() {
                            this.extend(arguments[0])
                        }
                    }, {
                        ancestor: Object,
                        version: "1.1",
                        forEach: function(t, i, e) {
                            for (var s in t) void 0 === this.prototype[s] && i.call(e, t[s], s, t)
                        },
                        implement: function() {
                            for (var t = 0; t < arguments.length; t++) "function" == typeof arguments[t] ? arguments[t](this.prototype) : this.prototype.extend(arguments[t]);
                            return this
                        },
                        toString: function() {
                            return String(this.valueOf())
                        }
                    });
                    var o;
                    ! function(t) {
                        "use strict";
                        o = function(t, i, e) {
                            return new o.Factory(t, i, e)
                        }, o.Lang = {}, o.Base = r.extend({
                            buildDate: "2013-11-07",
                            version: "0.3.1",
                            constructor: function(i, e) {
                                "object" != typeof i && (i = {}), "object" != typeof e && (e = {}), this.setOptions(t.extend(!0, {}, i, e))
                            },
                            callback: function(t) {
                                if ("function" == typeof t) {
                                    for (var i = [], e = 1; e <= arguments.length; e++) arguments[e] && i.push(arguments[e]);
                                    t.apply(this, i)
                                }
                            },
                            log: function(t) {
                                window.console && console.log && console.log(t)
                            },
                            getOption: function(t) {
                                return this[t] ? this[t] : !1
                            },
                            getOptions: function() {
                                return this
                            },
                            setOption: function(t, i) {
                                this[t] = i
                            },
                            setOptions: function(t) {
                                for (var i in t) "undefined" != typeof t[i] && this.setOption(i, t[i])
                            }
                        }), o.Factory = o.Base.extend({
                            autoStart: !0,
                            callbacks: {
                                destroy: !1,
                                create: !1,
                                init: !1,
                                interval: !1,
                                start: !1,
                                stop: !1,
                                reset: !1
                            },
                            classes: {
                                active: "flip-clock-active",
                                before: "flip-clock-before",
                                divider: "flip-clock-divider",
                                dot: "flip-clock-dot",
                                label: "flip-clock-label",
                                flip: "flip",
                                play: "play",
                                wrapper: "flip-clock-wrapper"
                            },
                            clockFace: "HourlyCounter",
                            defaultClockFace: "HourlyCounter",
                            defaultLanguage: "english",
                            language: "english",
                            lang: !1,
                            face: !0,
                            running: !1,
                            time: !1,
                            timer: !1,
                            lists: [],
                            $wrapper: !1,
                            constructor: function(i, e, s) {
                                this.lists = [], this.running = !1, this.base(s), this.$wrapper = t(i).addClass(this.classes.wrapper), this.time = new o.Time(this, e ? Math.round(e) : 0), this.timer = new o.Timer(this, s), this.lang = this.loadLanguage(this.language), this.face = this.loadClockFace(this.clockFace, s), this.autoStart && this.start()
                            },
                            loadClockFace: function(t, i) {
                                var e, s = "Face";
                                return t = t.ucfirst() + s, e = o[t] ? new o[t](this, i) : new o[this.defaultClockFace + s](this, i), e.build(), e
                            },
                            loadLanguage: function(t) {
                                var i;
                                return i = o.Lang[t.ucfirst()] ? o.Lang[t.ucfirst()] : o.Lang[t] ? o.Lang[t] : o.Lang[this.defaultLanguage]
                            },
                            localize: function(t, i) {
                                var e = this.lang;
                                if (!t) return null;
                                var s = t.toLowerCase();
                                return "object" == typeof i && (e = i), e && e[s] ? e[s] : t
                            },
                            start: function(t) {
                                var i = this;
                                i.running || i.countdown && !(i.countdown && i.time.time > 0) ? i.log("Trying to start timer when countdown already at 0") : (i.face.start(i.time), i.timer.start(function() {
                                    i.flip(), "function" == typeof t && t()
                                }))
                            },
                            stop: function(t) {
                                this.face.stop(), this.timer.stop(t);
                                for (var i in this.lists) this.lists[i].stop()
                            },
                            reset: function(t) {
                                this.timer.reset(t), this.face.reset()
                            },
                            setTime: function(t) {
                                this.time.time = t, this.face.setTime(t)
                            },
                            getTime: function() {
                                return this.time
                            },
                            setCountdown: function(t) {
                                var i = this.running;
                                this.countdown = t ? !0 : !1, i && (this.stop(), this.start())
                            },
                            flip: function() {
                                this.face.flip()
                            }
                        }), o.Face = o.Base.extend({
                            dividers: [],
                            factory: !1,
                            lists: [],
                            constructor: function(t, i) {
                                this.base(i), this.factory = t, this.dividers = []
                            },
                            build: function() {},
                            createDivider: function(i, e, s) {
                                "boolean" != typeof e && e || (s = e, e = i);
                                var n = ['<span class="' + this.factory.classes.dot + ' top"></span>', '<span class="' + this.factory.classes.dot + ' bottom"></span>'].join("");
                                s && (n = ""), i = this.factory.localize(i);
                                var r = ['<span class="' + this.factory.classes.divider + " " + (e ? e : "").toLowerCase() + '">', '<span class="' + this.factory.classes.label + '">' + (i ? i : "") + "</span>", n, "</span>"];
                                return t(r.join(""))
                            },
                            createList: function(t, i) {
                                "object" == typeof t && (i = t, t = 0);
                                var e = new o.List(this.factory, t, i);
                                return e
                            },
                            reset: function() {},
                            setTime: function(t) {
                                this.flip(t)
                            },
                            addDigit: function(t) {
                                var i = this.createList(t, {
                                    classes: {
                                        active: this.factory.classes.active,
                                        before: this.factory.classes.before,
                                        flip: this.factory.classes.flip
                                    }
                                });
                                i.$obj.insertBefore(this.factory.lists[0].$obj), this.factory.lists.unshift(i)
                            },
                            start: function() {},
                            stop: function() {},
                            flip: function(i, e) {
                                var s = this;
                                e || (s.factory.countdown ? (s.factory.time.time <= 0 && s.factory.stop(), s.factory.time.time--) : s.factory.time.time++);
                                var n = s.factory.lists.length - i.length;
                                0 > n && (n = 0);
                                var r = !1;
                                t.each(i, function(t, i) {
                                    t += n;
                                    var o = s.factory.lists[t];
                                    if (o) {
                                        var a = o.digit;
                                        o.select(i), i == a || e || o.play()
                                    } else s.addDigit(i), r = !0
                                });
                                for (var o = 0; o < i.length; o++) o >= n && s.factory.lists[o].digit != i[o] && s.factory.lists[o].select(i[o])
                            }
                        }), o.List = o.Base.extend({
                            digit: 0,
                            classes: {
                                active: "flip-clock-active",
                                before: "flip-clock-before",
                                flip: "flip"
                            },
                            factory: !1,
                            $obj: !1,
                            items: [],
                            constructor: function(t, i) {
                                this.factory = t, this.digit = i, this.$obj = this.createList(), i > 0 && this.select(i), this.factory.$wrapper.append(this.$obj)
                            },
                            select: function(t) {
                                "undefined" == typeof t ? t = this.digit : this.digit = t; {
                                    var i = this.$obj.find('[data-digit="' + t + '"]');
                                    this.$obj.find("." + this.classes.active).removeClass(this.classes.active), this.$obj.find("." + this.classes.before).removeClass(this.classes.before)
                                }
                                this.factory.countdown ? i.is(":last-child") ? this.$obj.find(":first-child").addClass(this.classes.before) : i.next().addClass(this.classes.before) : i.is(":first-child") ? this.$obj.find(":last-child").addClass(this.classes.before) : i.prev().addClass(this.classes.before), i.addClass(this.classes.active)
                            },
                            play: function() {
                                this.$obj.addClass(this.factory.classes.play)
                            },
                            stop: function() {
                                var t = this;
                                setTimeout(function() {
                                    t.$obj.removeClass(t.factory.classes.play)
                                }, this.factory.timer.interval)
                            },
                            createList: function() {
                                for (var i = t('<ul class="' + this.classes.flip + " " + (this.factory.running ? this.factory.classes.play : "") + '" />'), e = 0; 10 > e; e++) {
                                    var s = t(['<li data-digit="' + e + '">', '<a href="#">', '<div class="up">', '<div class="shadow"></div>', '<div class="inn">' + e + "</div>", "</div>", '<div class="down">', '<div class="shadow"></div>', '<div class="inn">' + e + "</div>", "</div>", "</a>", "</li>"].join(""));
                                    this.items.push(s), i.append(s)
                                }
                                return i
                            }
                        }), o.Time = o.Base.extend({
                            minimumDigits: 0,
                            time: 0,
                            factory: !1,
                            constructor: function(t, i, e) {
                                this.base(e), this.factory = t, i && (this.time = i)
                            },
                            convertDigitsToArray: function(t) {
                                var i = [];
                                t = t.toString();
                                for (var e = 0; e < t.length; e++) t[e].match(/^\d*$/g) && i.push(t[e]);
                                return i
                            },
                            digit: function(t) {
                                var i = this.toString(),
                                    e = i.length;
                                return i[e - t] ? i[e - t] : !1
                            },
                            digitize: function(i) {
                                var e = [];
                                return t.each(i, function(t, i) {
                                    i = i.toString(), 1 == i.length && (i = "0" + i);
                                    for (var s = 0; s < i.length; s++) e.push(i[s])
                                }), e.length > this.minimumDigits && (this.minimumDigits = e.length), this.minimumDigits > e.length && e.unshift("0"), e
                            },
                            getDayCounter: function(t) {
                                var i, e = this.getDays();
                                return e >= 100 ? i = [this.getDays(), this.getHours(!0), this.getMinutes(!0)] : e >= 10 && 99 >= e ? i = ["0" + this.getDays(), this.getHours(!0), this.getMinutes(!0)] : 9 >= e && (i = ["00" + this.getDays(), this.getHours(!0), this.getMinutes(!0)]), t && i.push(this.getSeconds(!0)), this.digitize(i)
                            },
                            getDays: function(t) {
                                var i = this.time / 60 / 60 / 24;
                                return t && (i %= 7), Math.floor(i)
                            },
                            getHourCounter: function() {
                                var t = this.digitize([this.getHours(), this.getMinutes(!0), this.getSeconds(!0)]);
                                return t
                            },
                            getHourly: function() {
                                return this.getHourCounter()
                            },
                            getHours: function(t) {
                                var i = this.time / 60 / 60;
                                return t && (i %= 24), Math.floor(i)
                            },
                            getMilitaryTime: function() {
                                var t = new Date,
                                    i = this.digitize([t.getHours(), t.getMinutes(), t.getSeconds()]);
                                return i
                            },
                            getMinutes: function(t) {
                                var i = this.time / 60;
                                return t && (i %= 60), Math.floor(i)
                            },
                            getMinuteCounter: function() {
                                var t = this.digitize([this.getMinutes(), this.getSeconds(!0)]);
                                return t
                            },
                            getSeconds: function(t) {
                                var i = this.time;
                                return t && (60 == i ? i = 0 : i %= 60), Math.ceil(i)
                            },
                            getTime: function() {
                                var t = new Date,
                                    i = t.getHours(),
                                    e = this.digitize([i > 12 ? i - 12 : 0 === i ? 12 : i, t.getMinutes(), t.getSeconds()]);
                                return e
                            },
                            getWeeks: function() {
                                var t = this.time / 60 / 60 / 24 / 7;
                                return mod && (t %= 52), Math.floor(t)
                            },
                            removeLeadingZeros: function(i, e) {
                                var s = 0,
                                    n = [];
                                return t.each(e, function(t) {
                                    i > t && i > 3 ? s += parseInt(e[t], 10) : n.push(e[t])
                                }), 0 === s ? n : e
                            },
                            toString: function() {
                                return this.time.toString()
                            }
                        }), o.Timer = o.Base.extend({
                            callbacks: {
                                destroy: !1,
                                create: !1,
                                init: !1,
                                interval: !1,
                                start: !1,
                                stop: !1,
                                reset: !1
                            },
                            count: 0,
                            factory: !1,
                            interval: 1e3,
                            constructor: function(t, i) {
                                this.base(i), this.factory = t, this.callback(this.callbacks.init), this.callback(this.callbacks.create)
                            },
                            getElapsed: function() {
                                return this.count * this.interval
                            },
                            getElapsedTime: function() {
                                return new Date(this.time + this.getElapsed())
                            },
                            reset: function(t) {
                                clearInterval(this.timer), this.count = 0, this._setInterval(t), this.callback(this.callbacks.reset)
                            },
                            start: function(t) {
                                this.factory.running = !0, this._createTimer(t), this.callback(this.callbacks.start)
                            },
                            stop: function(t) {
                                this.factory.running = !1, this._clearInterval(t), this.callback(this.callbacks.stop), this.callback(t)
                            },
                            _clearInterval: function() {
                                clearInterval(this.timer)
                            },
                            _createTimer: function(t) {
                                this._setInterval(t)
                            },
                            _destroyTimer: function(t) {
                                this._clearInterval(), this.timer = !1, this.callback(t), this.callback(this.callbacks.destroy)
                            },
                            _interval: function(t) {
                                this.callback(this.callbacks.interval), this.callback(t), this.count++
                            },
                            _setInterval: function(t) {
                                var i = this;
                                i.timer = setInterval(function() {
                                    i._interval(t)
                                }, this.interval)
                            }
                        }), String.prototype.ucfirst = function() {
                            return this.substr(0, 1).toUpperCase() + this.substr(1)
                        }, t.fn.FlipClock = function(i, e) {
                            return "object" == typeof i && (e = i, i = 0), new o(t(this), i, e)
                        }, t.fn.flipClock = function(i, e) {
                            return t.fn.FlipClock(i, e)
                        }
                    }(jQuery),
                        function(t) {
                            o.TwentyFourHourClockFace = o.Face.extend({
                                constructor: function(t, i) {
                                    t.countdown = !1, this.base(t, i)
                                },
                                build: function(i) {
                                    var e = this,
                                        s = this.factory.$wrapper.find("ul");
                                    i = i ? i : this.factory.time.time || this.factory.time.getMilitaryTime(), i.length > s.length && t.each(i, function(t, i) {
                                        e.factory.lists.push(e.createList(i))
                                    }), this.dividers.push(this.createDivider()), this.dividers.push(this.createDivider()), t(this.dividers[0]).insertBefore(this.factory.lists[this.factory.lists.length - 2].$obj), t(this.dividers[1]).insertBefore(this.factory.lists[this.factory.lists.length - 4].$obj), this._clearExcessDigits(), this.autoStart && this.start()
                                },
                                flip: function(t) {
                                    t = t ? t : this.factory.time.getMilitaryTime(), this.base(t)
                                },
                                _clearExcessDigits: function() {
                                    for (var t = this.factory.lists[this.factory.lists.length - 2], i = this.factory.lists[this.factory.lists.length - 4], e = 6; 10 > e; e++) t.$obj.find("li:last-child").remove(), i.$obj.find("li:last-child").remove()
                                }
                            })
                        }(jQuery),
                        function(t) {
                            o.CounterFace = o.Face.extend({
                                autoStart: !1,
                                constructor: function(t, i) {
                                    t.timer.interval = 0, t.autoStart = !1, t.running = !0, t.increment = function() {
                                        t.countdown = !1, t.setTime(t.getTime().time + 1)
                                    }, t.decrement = function() {
                                        t.countdown = !0, t.setTime(t.getTime().time - 1)
                                    }, t.setValue = function(i) {
                                        t.setTime(i)
                                    }, t.setCounter = function(i) {
                                        t.setTime(i)
                                    }, this.base(t, i)
                                },
                                build: function() {
                                    var i = this,
                                        e = this.factory.$wrapper.find("ul"),
                                        s = [],
                                        n = this.factory.getTime().digitize([this.factory.getTime().time]);
                                    n.length > e.length && t.each(n, function(t, e) {
                                        var n = i.createList(e);
                                        n.select(e), s.push(n)
                                    }), t.each(s, function(t, i) {
                                        i.play()
                                    }), this.factory.lists = s
                                },
                                flip: function(t) {
                                    var i = this.factory.getTime().digitize([this.factory.getTime().time]);
                                    this.base(i, t)
                                }
                            })
                        }(jQuery),
                        function(t) {
                            o.DailyCounterFace = o.Face.extend({
                                showSeconds: !0,
                                constructor: function(t, i) {
                                    this.base(t, i)
                                },
                                build: function(i, e) {
                                    var s = this,
                                        n = this.factory.$wrapper.find("ul"),
                                        r = [],
                                        o = 0;
                                    e = e ? e : this.factory.time.getDayCounter(this.showSeconds), e.length > n.length && t.each(e, function(t, i) {
                                        r.push(s.createList(i))
                                    }), this.factory.lists = r, this.showSeconds ? t(this.createDivider("Seconds")).insertBefore(this.factory.lists[this.factory.lists.length - 2].$obj) : o = 2, t(this.createDivider("Minutes")).insertBefore(this.factory.lists[this.factory.lists.length - 4 + o].$obj), t(this.createDivider("Hours")).insertBefore(this.factory.lists[this.factory.lists.length - 6 + o].$obj), t(this.createDivider("Days", !0)).insertBefore(this.factory.lists[0].$obj), this._clearExcessDigits(), this.autoStart && this.start()
                                },
                                flip: function(t, i) {
                                    i || (i = this.factory.time.getDayCounter(this.showSeconds)), this.base(i, t)
                                },
                                _clearExcessDigits: function() {
                                    for (var t = this.factory.lists[this.factory.lists.length - 2], i = this.factory.lists[this.factory.lists.length - 4], e = 6; 10 > e; e++) t.$obj.find("li:last-child").remove(), i.$obj.find("li:last-child").remove()
                                }
                            })
                        }(jQuery),
                        function(t) {
                            o.HourlyCounterFace = o.Face.extend({
                                clearExcessDigits: !0,
                                constructor: function(t, i) {
                                    this.base(t, i)
                                },
                                build: function(i, e) {
                                    var s = this,
                                        n = this.factory.$wrapper.find("ul"),
                                        r = [];
                                    e = e ? e : this.factory.time.getHourCounter(), e.length > n.length && t.each(e, function(t, i) {
                                        r.push(s.createList(i))
                                    }), this.factory.lists = r, t(this.createDivider("Seconds")).insertBefore(this.factory.lists[this.factory.lists.length - 2].$obj), t(this.createDivider("Minutes")).insertBefore(this.factory.lists[this.factory.lists.length - 4].$obj), i || t(this.createDivider("Hours", !0)).insertBefore(this.factory.lists[0].$obj), this.clearExcessDigits && this._clearExcessDigits(), this.autoStart && this.start()
                                },
                                flip: function(t, i) {
                                    i || (i = this.factory.time.getHourCounter()), this.base(i, t)
                                },
                                _clearExcessDigits: function() {
                                    for (var t = this.factory.lists[this.factory.lists.length - 2], i = this.factory.lists[this.factory.lists.length - 4], e = 6; 10 > e; e++) t.$obj.find("li:last-child").remove(), i.$obj.find("li:last-child").remove()
                                }
                            })
                        }(jQuery),
                        function() {
                            o.MinuteCounterFace = o.HourlyCounterFace.extend({
                                clearExcessDigits: !1,
                                constructor: function(t, i) {
                                    this.base(t, i)
                                },
                                build: function() {
                                    this.base(!0, this.factory.time.getMinuteCounter())
                                },
                                flip: function(t) {
                                    this.base(t, this.factory.time.getMinuteCounter())
                                }
                            })
                        }(jQuery),
                        function(t) {
                            o.TwelveHourClockFace = o.TwentyFourHourClockFace.extend({
                                meridium: !1,
                                meridiumText: "AM",
                                build: function(i) {
                                    i = i ? i : this.factory.time.time ? this.factory.time.time : this.factory.time.getTime(), this.base(i), this.meridiumText = this._isPM() ? "PM" : "AM", this.meridium = t(['<ul class="flip-clock-meridium">', "<li>", '<a href="#">' + this.meridiumText + "</a>", "</li>", "</ul>"].join("")), this.meridium.insertAfter(this.factory.lists[this.factory.lists.length - 1].$obj)
                                },
                                flip: function() {
                                    this.meridiumText != this._getMeridium() && (this.meridiumText = this._getMeridium(), this.meridium.find("a").html(this.meridiumText)), this.base(this.factory.time.getTime())
                                },
                                _getMeridium: function() {
                                    return (new Date).getHours() >= 12 ? "PM" : "AM"
                                },
                                _isPM: function() {
                                    return "PM" == this._getMeridium() ? !0 : !1
                                },
                                _clearExcessDigits: function() {
                                    for (var t = this.factory.lists[this.factory.lists.length - 2], i = this.factory.lists[this.factory.lists.length - 4], e = 6; 10 > e; e++) t.$obj.find("li:last-child").remove(), i.$obj.find("li:last-child").remove()
                                }
                            })
                        }(jQuery),
                        function() {
                            o.Lang.German = {
                                years: "Jahre",
                                months: "Monate",
                                days: "Tage",
                                hours: "Stunden",
                                minutes: "Minuten",
                                seconds: "Sekunden"
                            }, o.Lang.de = o.Lang.German, o.Lang["de-de"] = o.Lang.German, o.Lang.german = o.Lang.German
                        }(jQuery),
                        function() {
                            o.Lang.English = {
                                years: "Years",
                                months: "Months",
                                days: "Days",
                                hours: "Hours",
                                minutes: "Minutes",
                                seconds: "Seconds"
                            }, o.Lang.en = o.Lang.English, o.Lang["en-us"] = o.Lang.English, o.Lang.english = o.Lang.English
                        }(jQuery),
                        function() {
                            o.Lang.Spanish = {
                                years: "A&#241;os",
                                months: "Meses",
                                days: "D&#205;as",
                                hours: "Horas",
                                minutes: "Minutos",
                                seconds: "Segundo"
                            }, o.Lang.es = o.Lang.Spanish, o.Lang["es-es"] = o.Lang.Spanish, o.Lang.spanish = o.Lang.Spanish
                        }(jQuery),
                        function() {
                            o.Lang.French = {
                                years: "ans",
                                months: "mois",
                                days: "jours",
                                hours: "heures",
                                minutes: "minutes",
                                seconds: "secondes"
                            }, o.Lang.fr = o.Lang.French, o.Lang["fr-ca"] = o.Lang.French, o.Lang.french = o.Lang.French
                        }(jQuery), n("undefined" != typeof flipclock ? flipclock : window.flipclock)
                }.call(global, void 0, void 0, void 0, void 0, function(t) {
                        module.exports = t
                    });
        }).call(this, typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
    }, {}],
    13: [function(require, module, exports) {
        (function(global) {
            __browserify_shim_require__ = require,
                function(e, r, t, n, i) {
                    ! function(e, r, t) {
                        "use strict";

                        function n() {
                            this.$get = function() {
                                function e(e, r, t) {
                                    for (var n = e[t], i = r(n); t > 0;) {
                                        var o = Math.floor((t + 1) / 2) - 1,
                                            a = e[o];
                                        if (i >= r(a)) break;
                                        e[o] = n, e[t] = a, t = o
                                    }
                                }

                                function t(e, r, t) {
                                    for (var n = e.length, i = e[t], o = r(i);;) {
                                        var a = 2 * (t + 1),
                                            s = a - 1,
                                            l = null;
                                        if (n > s) {
                                            var c = e[s],
                                                u = r(c);
                                            o > u && (l = s)
                                        }
                                        if (n > a) {
                                            var p = e[a],
                                                g = r(p);
                                            g < (null === l ? o : r(e[s])) && (l = a)
                                        }
                                        if (null === l) break;
                                        e[t] = e[l], e[l] = i, t = l
                                    }
                                }

                                function n(e) {
                                    if (e && !r.isFunction(e)) throw new Error("BinaryHeap(weightFunc): weightFunc: must be a function!");
                                    e = e || function(e) {
                                            return e
                                        }, this.weightFunc = e, this.heap = []
                                }
                                return n.prototype.push = function(r) {
                                    this.heap.push(r), e(this.heap, this.weightFunc, this.heap.length - 1)
                                }, n.prototype.peek = function() {
                                    return this.heap[0]
                                }, n.prototype.pop = function() {
                                    var e = this.heap[0],
                                        r = this.heap.pop();
                                    return this.heap.length > 0 && (this.heap[0] = r, t(this.heap, this.weightFunc, 0)), e
                                }, n.prototype.remove = function(n) {
                                    for (var i = this.heap.length, o = 0; i > o; o++)
                                        if (r.equals(this.heap[o], n)) {
                                            var a = this.heap[o],
                                                s = this.heap.pop();
                                            return o !== i - 1 && (this.heap[o] = s, e(this.heap, this.weightFunc, o), t(this.heap, this.weightFunc, o)), a
                                        }
                                    return null
                                }, n.prototype.removeAll = function() {
                                    this.heap = []
                                }, n.prototype.size = function() {
                                    return this.heap.length
                                }, n
                            }
                        }

                        function i() {
                            function e(e, t) {
                                t(r.isNumber(e) ? 0 > e ? "must be greater than zero!" : null : "must be a number!")
                            }
                            var n, i = function() {
                                return {
                                    capacity: Number.MAX_VALUE,
                                    maxAge: null,
                                    deleteOnExpire: "none",
                                    onExpire: null,
                                    cacheFlushInterval: null,
                                    recycleFreq: 1e3,
                                    storageMode: "none",
                                    storageImpl: null,
                                    verifyIntegrity: !0,
                                    disabled: !1
                                }
                            };
                            this.setCacheDefaults = function(t) {
                                var o = "$angularCacheFactoryProvider.setCacheDefaults(options): ";
                                if (t = t || {}, !r.isObject(t)) throw new Error(o + "options: must be an object!");
                                if ("disabled" in t && (t.disabled = t.disabled === !0), "capacity" in t && e(t.capacity, function(e) {
                                        if (e) throw new Error(o + "capacity: " + e)
                                    }), "deleteOnExpire" in t) {
                                    if (!r.isString(t.deleteOnExpire)) throw new Error(o + "deleteOnExpire: must be a string!");
                                    if ("none" !== t.deleteOnExpire && "passive" !== t.deleteOnExpire && "aggressive" !== t.deleteOnExpire) throw new Error(o + 'deleteOnExpire: accepted values are "none", "passive" or "aggressive"!')
                                }
                                if ("maxAge" in t && e(t.maxAge, function(e) {
                                        if (e) throw new Error(o + "maxAge: " + e)
                                    }), "recycleFreq" in t && e(t.recycleFreq, function(e) {
                                        if (e) throw new Error(o + "recycleFreq: " + e)
                                    }), "cacheFlushInterval" in t && e(t.cacheFlushInterval, function(e) {
                                        if (e) throw new Error(o + "cacheFlushInterval: " + e)
                                    }), "storageMode" in t) {
                                    if (!r.isString(t.storageMode)) throw new Error(o + "storageMode: must be a string!");
                                    if ("none" !== t.storageMode && "localStorage" !== t.storageMode && "sessionStorage" !== t.storageMode) throw new Error(o + 'storageMode: accepted values are "none", "localStorage" or "sessionStorage"!');
                                    if ("storageImpl" in t) {
                                        if (!r.isObject(t.storageImpl)) throw new Error(o + "storageImpl: must be an object!");
                                        if (!("setItem" in t.storageImpl && "function" == typeof t.storageImpl.setItem)) throw new Error(o + 'storageImpl: must implement "setItem(key, value)"!');
                                        if (!("getItem" in t.storageImpl && "function" == typeof t.storageImpl.getItem)) throw new Error(o + 'storageImpl: must implement "getItem(key)"!');
                                        if (!("removeItem" in t.storageImpl) || "function" != typeof t.storageImpl.removeItem) throw new Error(o + 'storageImpl: must implement "removeItem(key)"!')
                                    }
                                }
                                if ("onExpire" in t && "function" != typeof t.onExpire) throw new Error(o + "onExpire: must be a function!");
                                n = r.extend({}, i(), t)
                            }, this.setCacheDefaults({}), this.$get = ["$window", "BinaryHeap", function(i, o) {
                                function a(e) {
                                    return e && r.isNumber(e) ? e.toString() : e
                                }

                                function s(e) {
                                    var r, t = {};
                                    for (r in e) e.hasOwnProperty(r) && (t[r] = r);
                                    return t
                                }

                                function l(e) {
                                    var r, t = [];
                                    for (r in e) e.hasOwnProperty(r) && t.push(r);
                                    return t
                                }

                                function c(c, u) {
                                    function g(r) {
                                        e(r, function(e) {
                                            if (e) throw new Error("capacity: " + e);
                                            for (k.capacity = r; q.size() > k.capacity;) D.remove(q.peek().key, {
                                                verifyIntegrity: !1
                                            })
                                        })
                                    }

                                    function h(e) {
                                        if (!r.isString(e)) throw new Error("deleteOnExpire: must be a string!");
                                        if ("none" !== e && "passive" !== e && "aggressive" !== e) throw new Error('deleteOnExpire: accepted values are "none", "passive" or "aggressive"!');
                                        k.deleteOnExpire = e
                                    }

                                    function f(r) {
                                        var t = l(M);
                                        if (null === r) {
                                            if (k.maxAge)
                                                for (var n = 0; n < t.length; n++) {
                                                    var i = t[n];
                                                    "maxAge" in M[i] || (delete M[i].expires, S.remove(M[i]))
                                                }
                                            k.maxAge = r
                                        } else e(r, function(e) {
                                            if (e) throw new Error("maxAge: " + e);
                                            if (r !== k.maxAge) {
                                                k.maxAge = r;
                                                for (var n = (new Date).getTime(), i = 0; i < t.length; i++) {
                                                    var o = t[i];
                                                    "maxAge" in M[o] || (S.remove(M[o]), M[o].expires = M[o].created + k.maxAge, S.push(M[o]), M[o].expires < n && D.remove(o, {
                                                        verifyIntegrity: !1
                                                    }))
                                                }
                                            }
                                        })
                                    }

                                    function m() {
                                        for (var e = (new Date).getTime(), r = S.peek(); r && r.expires && r.expires < e;) D.remove(r.key, {
                                            verifyIntegrity: !1
                                        }), k.onExpire && k.onExpire(r.key, r.value), r = S.peek()
                                    }

                                    function d(r) {
                                        null === r ? (k.recycleFreqId && (clearInterval(k.recycleFreqId), delete k.recycleFreqId), k.recycleFreq = n.recycleFreq, k.recycleFreqId = setInterval(m, k.recycleFreq)) : e(r, function(e) {
                                            if (e) throw new Error("recycleFreq: " + e);
                                            k.recycleFreq = r, k.recycleFreqId && clearInterval(k.recycleFreqId), k.recycleFreqId = setInterval(m, k.recycleFreq)
                                        })
                                    }

                                    function v(r) {
                                        null === r ? (k.cacheFlushIntervalId && (clearInterval(k.cacheFlushIntervalId), delete k.cacheFlushIntervalId), k.cacheFlushInterval = r) : e(r, function(e) {
                                            if (e) throw new Error("cacheFlushInterval: " + e);
                                            r !== k.cacheFlushInterval && (k.cacheFlushIntervalId && clearInterval(k.cacheFlushIntervalId), k.cacheFlushInterval = r, k.cacheFlushIntervalId = setInterval(D.removeAll, k.cacheFlushInterval))
                                        })
                                    }

                                    function y(e, t) {
                                        var n, o;
                                        if (!r.isString(e)) throw new Error("storageMode: must be a string!");
                                        if ("none" !== e && "localStorage" !== e && "sessionStorage" !== e) throw new Error('storageMode: accepted values are "none", "localStorage" or "sessionStorage"!');
                                        if (("localStorage" === k.storageMode || "sessionStorage" === k.storageMode) && e !== k.storageMode) {
                                            for (n = l(M), o = 0; o < n.length; o++) J.removeItem(C + ".data." + n[o]);
                                            J.removeItem(C + ".keys")
                                        }
                                        if (k.storageMode = e, t) {
                                            if (!r.isObject(t)) throw new Error("storageImpl: must be an object!");
                                            if (!("setItem" in t && "function" == typeof t.setItem)) throw new Error('storageImpl: must implement "setItem(key, value)"!');
                                            if (!("getItem" in t && "function" == typeof t.getItem)) throw new Error('storageImpl: must implement "getItem(key)"!');
                                            if (!("removeItem" in t) || "function" != typeof t.removeItem) throw new Error('storageImpl: must implement "removeItem(key)"!');
                                            J = t
                                        } else "localStorage" === k.storageMode ? J = i.localStorage : "sessionStorage" === k.storageMode && (J = i.sessionStorage);
                                        if ("none" !== k.storageMode && J)
                                            if (j)
                                                for (n = l(M), o = 0; o < n.length; o++) w(n[o]);
                                            else x()
                                    }

                                    function I(e, t, i) {
                                        if (e = e || {}, i = i || {}, t = !!t, !r.isObject(e)) throw new Error("AngularCache.setOptions(cacheOptions, strict, options): cacheOptions: must be an object!");
                                        if (E(i.verifyIntegrity), t && (e = r.extend({}, n, e)), "disabled" in e && (k.disabled = e.disabled === !0), "verifyIntegrity" in e && (k.verifyIntegrity = e.verifyIntegrity === !0), "capacity" in e && g(e.capacity), "deleteOnExpire" in e && h(e.deleteOnExpire), "maxAge" in e && f(e.maxAge), "recycleFreq" in e && d(e.recycleFreq), "cacheFlushInterval" in e && v(e.cacheFlushInterval), "storageMode" in e && y(e.storageMode, e.storageImpl), "onExpire" in e) {
                                            if (null !== e.onExpire && "function" != typeof e.onExpire) throw new Error("onExpire: must be a function!");
                                            k.onExpire = e.onExpire
                                        }
                                        j = !0
                                    }

                                    function x() {
                                        var e = r.fromJson(J.getItem(C + ".keys"));
                                        if (J.removeItem(C + ".keys"), e && e.length) {
                                            for (var t = 0; t < e.length; t++) {
                                                var n = r.fromJson(J.getItem(C + ".data." + e[t])) || {},
                                                    i = n.maxAge || k.maxAge,
                                                    o = n.deleteOnExpire || k.deleteOnExpire;
                                                if (i && (new Date).getTime() - n.created > i && "aggressive" === o) J.removeItem(C + ".data." + e[t]);
                                                else {
                                                    var a = {
                                                        created: n.created
                                                    };
                                                    n.expires && (a.expires = n.expires), n.accessed && (a.accessed = n.accessed), n.maxAge && (a.maxAge = n.maxAge), n.deleteOnExpire && (a.deleteOnExpire = n.deleteOnExpire), D.put(e[t], n.value, a)
                                                }
                                            }
                                            w(null)
                                        }
                                    }

                                    function w(e) {
                                        "none" !== k.storageMode && J && (J.setItem(C + ".keys", r.toJson(l(M))), e && J.setItem(C + ".data." + e, r.toJson(M[e])))
                                    }

                                    function E(e) {
                                        if ((e || e !== !1 && k.verifyIntegrity) && "none" !== k.storageMode && J) {
                                            var t = l(M);
                                            J.setItem(C + ".keys", r.toJson(t));
                                            for (var n = 0; n < t.length; n++) J.setItem(C + ".data." + t[n], r.toJson(M[t[n]]))
                                        }
                                    }

                                    function b(e, t) {
                                        if ((t || t !== !1 && k.verifyIntegrity) && "none" !== k.storageMode && J) {
                                            var n = J.getItem(C + ".data." + e);
                                            if (!n && e in M) D.remove(e);
                                            else if (n) {
                                                var i = r.fromJson(n),
                                                    o = i ? i.value : null;
                                                o && D.put(e, o)
                                            }
                                        }
                                    }

                                    function A(e) {
                                        if ("none" !== k.storageMode && J) {
                                            var t = e || l(M);
                                            J.setItem(C + ".keys", r.toJson(t))
                                        }
                                    }

                                    function F(e) {
                                        "none" !== k.storageMode && J && e in M && J.setItem(C + ".data." + e, r.toJson(M[e]))
                                    }

                                    function O() {
                                        if ("none" !== k.storageMode && J) {
                                            for (var e = l(M), t = 0; t < e.length; t++) J.removeItem(C + ".data." + e[t]);
                                            J.setItem(C + ".keys", r.toJson([]))
                                        }
                                    }
                                    var k = r.extend({}, {
                                            id: c
                                        }),
                                        M = {},
                                        S = new o(function(e) {
                                            return e.expires
                                        }),
                                        q = new o(function(e) {
                                            return e.accessed
                                        }),
                                        C = "angular-cache.caches." + c,
                                        j = !1,
                                        D = this,
                                        J = null;
                                    u = u || {}, this.put = function(t, n, i) {
                                        if (!k.disabled) {
                                            if (i = i || {}, t = a(t), !r.isString(t)) throw new Error("AngularCache.put(key, value, options): key: must be a string!");
                                            if (i && !r.isObject(i)) throw new Error("AngularCache.put(key, value, options): options: must be an object!");
                                            if (i.maxAge && null !== i.maxAge) e(i.maxAge, function(e) {
                                                if (e) throw new Error("AngularCache.put(key, value, options): maxAge: " + e)
                                            });
                                            else {
                                                if (i.deleteOnExpire && !r.isString(i.deleteOnExpire)) throw new Error("AngularCache.put(key, value, options): deleteOnExpire: must be a string!");
                                                if (i.deleteOnExpire && "none" !== i.deleteOnExpire && "passive" !== i.deleteOnExpire && "aggressive" !== i.deleteOnExpire) throw new Error('AngularCache.put(key, value, options): deleteOnExpire: accepted values are "none", "passive" or "aggressive"!');
                                                if (r.isUndefined(n)) return
                                            }
                                            var o, s, l = (new Date).getTime();
                                            return E(i.verifyIntegrity), M[t] ? (S.remove(M[t]), q.remove(M[t])) : M[t] = {
                                                key: t
                                            }, s = M[t], s.value = n, s.created = parseInt(i.created, 10) || s.created || l, s.accessed = parseInt(i.accessed, 10) || l, i.deleteOnExpire && (s.deleteOnExpire = i.deleteOnExpire), i.maxAge && (s.maxAge = i.maxAge), (s.maxAge || k.maxAge) && (s.expires = s.created + (s.maxAge || k.maxAge)), o = s.deleteOnExpire || k.deleteOnExpire, s.expires && "aggressive" === o && S.push(s), A(), F(t), q.push(s), q.size() > k.capacity && this.remove(q.peek().key, {
                                                verifyIntegrity: !1
                                            }), n
                                        }
                                    }, this.get = function(e, n) {
                                        if (!k.disabled) {
                                            if (r.isArray(e)) {
                                                var i = e,
                                                    o = [];
                                                return r.forEach(i, function(e) {
                                                    var t = D.get(e, n);
                                                    r.isDefined(t) && o.push(t)
                                                }), o
                                            }
                                            if (e = a(e), n = n || {}, !r.isString(e)) throw new Error("AngularCache.get(key, options): key: must be a string!");
                                            if (n && !r.isObject(n)) throw new Error("AngularCache.get(key, options): options: must be an object!");
                                            if (n.onExpire && !r.isFunction(n.onExpire)) throw new Error("AngularCache.get(key, options): onExpire: must be a function!");
                                            if (b(e, n.verifyIntegrity), e in M) {
                                                var s = M[e],
                                                    l = s.value,
                                                    c = (new Date).getTime(),
                                                    u = s.deleteOnExpire || k.deleteOnExpire;
                                                return q.remove(s), s.accessed = c, q.push(s), "passive" === u && "expires" in s && s.expires < c && (this.remove(e, {
                                                    verifyIntegrity: !1
                                                }), k.onExpire ? k.onExpire(e, s.value, n.onExpire) : n.onExpire && n.onExpire(e, s.value), l = t), F(e), l
                                            }
                                        }
                                    }, this.remove = function(e, r) {
                                        r = r || {}, E(r.verifyIntegrity), q.remove(M[e]), S.remove(M[e]), "none" !== k.storageMode && J && J.removeItem(C + ".data." + e), delete M[e], A()
                                    }, this.removeAll = function() {
                                        O(), q.removeAll(), S.removeAll(), M = {}
                                    }, this.removeExpired = function(e) {
                                        e = e || {};
                                        for (var r = (new Date).getTime(), t = l(M), n = {}, i = 0; i < t.length; i++) M[t[i]] && M[t[i]].expires && M[t[i]].expires < r && (n[t[i]] = M[t[i]].value);
                                        for (var o in n) D.remove(o);
                                        if (E(e.verifyIntegrity), e.asArray) {
                                            var a = [];
                                            for (o in n) a.push(n[o]);
                                            return a
                                        }
                                        return n
                                    }, this.destroy = function() {
                                        k.cacheFlushIntervalId && clearInterval(k.cacheFlushIntervalId), k.recycleFreqId && clearInterval(k.recycleFreqId), this.removeAll(), "none" !== k.storageMode && J && (J.removeItem(C + ".keys"), J.removeItem(C)), J = null, M = null, q = null, S = null, k = null, C = null, D = null;
                                        for (var e = l(this), r = 0; r < e.length; r++) this.hasOwnProperty(e[r]) && delete this[e[r]];
                                        p[c] = null, delete p[c]
                                    }, this.info = function(e) {
                                        if (e) {
                                            if (M[e]) {
                                                var t = {
                                                    created: M[e].created,
                                                    accessed: M[e].accessed,
                                                    expires: M[e].expires,
                                                    maxAge: M[e].maxAge || k.maxAge,
                                                    deleteOnExpire: M[e].deleteOnExpire || k.deleteOnExpire,
                                                    isExpired: !1
                                                };
                                                return t.maxAge && (t.isExpired = (new Date).getTime() - t.created > t.maxAge), t
                                            }
                                            return M[e]
                                        }
                                        return r.extend({}, k, {
                                            size: q && q.size() || 0
                                        })
                                    }, this.keySet = function() {
                                        return s(M)
                                    }, this.keys = function() {
                                        return l(M)
                                    }, this.setOptions = I, I(u, !0, {
                                        verifyIntegrity: !1
                                    })
                                }

                                function u(e, t) {
                                    if (e in p) throw new Error("cacheId " + e + " taken!");
                                    if (!r.isString(e)) throw new Error("cacheId must be a string!");
                                    return p[e] = new c(e, t), p[e]
                                }
                                var p = {};
                                return u.info = function() {
                                    for (var e = l(p), t = {
                                        size: e.length,
                                        caches: {}
                                    }, i = 0; i < e.length; i++) {
                                        var o = e[i];
                                        t.caches[o] = p[o].info()
                                    }
                                    return t.cacheDefaults = r.extend({}, n), t
                                }, u.get = function(e) {
                                    if (!r.isString(e)) throw new Error("$angularCacheFactory.get(cacheId): cacheId: must be a string!");
                                    return p[e]
                                }, u.keySet = function() {
                                    return s(p)
                                }, u.keys = function() {
                                    return l(p)
                                }, u.removeAll = function() {
                                    for (var e = l(p), r = 0; r < e.length; r++) p[e[r]].destroy()
                                }, u.clearAll = function() {
                                    for (var e = l(p), r = 0; r < e.length; r++) p[e[r]].removeAll()
                                }, u.enableAll = function() {
                                    for (var e = l(p), r = 0; r < e.length; r++) p[e[r]].setOptions({
                                        disabled: !1
                                    })
                                }, u.disableAll = function() {
                                    for (var e = l(p), r = 0; r < e.length; r++) p[e[r]].setOptions({
                                        disabled: !0
                                    })
                                }, u
                            }]
                        }
                        r.module("jmdobry.binary-heap", []), r.module("jmdobry.binary-heap").provider("BinaryHeap", n), r.module("jmdobry.angular-cache", ["ng", "jmdobry.binary-heap"]), r.module("jmdobry.angular-cache").provider("$angularCacheFactory", i)
                    }(window, window.angular), i("undefined" != typeof angularCache ? angularCache : window.angularCache)
                }.call(global, void 0, void 0, void 0, void 0, function(e) {
                        module.exports = e
                    });
        }).call(this, typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
    }, {}]
}, {}, [11])