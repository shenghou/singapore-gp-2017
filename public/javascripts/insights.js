/**
 * Created by Sheng Hou on 12/7/2016.
 */

$(document).ready(function(){
    $( "select" ).change(function () {
        var year = "year-";
        $( "select option:selected" ).each(function() {
            year += $( this ).val();
            $('.year').hide();
            $('#' + year).show();
        });

    });
})

