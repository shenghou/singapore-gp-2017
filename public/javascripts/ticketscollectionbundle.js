(function e(t, n, r) {
    function s(o, u) {
        if (!n[o]) {
            if (!t[o]) {
                var a = typeof require == "function" && require;
                if (!u && a) return a(o, !0);
                if (i) return i(o, !0);
                throw new Error("Cannot find module '" + o + "'")
            }
            var f = n[o] = {
                exports: {}
            };
            t[o][0].call(f.exports, function(e) {
                var n = t[o][1][e];
                return s(n ? n : e)
            }, f, f.exports, e, t, n, r)
        }
        return n[o].exports
    }
    var i = typeof require == "function" && require;
    for (var o = 0; o < r.length; o++) s(r[o]);
    return s
})({
    1: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = function(a) {
            var r = {};
            return r.message = "", r.prepForBroadcast = function(a) {
                this.message = a, this.broadcastItem()
            }, r.broadcastItem = function() {
                a.$broadcast("handleBroadcast")
            }, r
        };
    }, {}],
    2: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = function(t) {
            var n = function(t) {
                    function n(e) {
                        console.log("body clicked", e.target), o(t), $(this).off("click", n), $(this).off("touchstart", n)
                    }
                    t.addClass("toggled"), $(".content").on("click", n), $(".content").on("touchstart", n)
                },
                o = function(t) {
                    t.removeClass("toggled"), $("html,body").scrollTop(0)
                },
                e = function(e, a) {
                    function c(a) {
                        a.stopPropagation();
                        var c = t.matchMedia("(max-width: 767px)");
                        c.matches && (l.hasClass("toggled") ? l.hasClass("toggled") && (a.preventDefault(), o(l)) : (a.preventDefault(), n(l), e.handleClick("menu:opened")), !$(a.target).is("a"))
                    }
                    var l = a.children("ul"),
                        r = $("#pull");
                    e.menu = l, r.on("click", c), r.on("touchstart", c)
                },
                a = function(t, n) {
                    t.handleClick = function(t) {
                        n.prepForBroadcast(t)
                    }, t.$on("handleBroadcast", function() {
                        "search:opened" == n.message && o(t.menu)
                    })
                };
            return {
                restrict: "A",
                link: e,
                controller: ["$scope", "eventBusService", a]
            }
        };
    }, {}],
    3: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = angular.module("responsiveMenuComponent", []).factory("eventBusService", ["$rootScope", require("../EventBus")]).directive("responsiveMenu", ["$window", require("./ComponentDirective")]);
    }, {
        "../EventBus": 1,
        "./ComponentDirective": 2
    }],
    4: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = function() {
            function o() {
                var o = !1;
                return function(e) {
                    (/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(e) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(e.substr(0, 4))) && (o = !0)
                }(navigator.userAgent || navigator.vendor || window.opera), o
            }
            var e = function(e, i) {
                    function t() {
                        console.log("body clicked"), a(e, i), $(this).off("click", t), $(this).off("touchstart", t)
                    }
                    e.addClass("sb-search-open"), o() || i.focus(), $(document).on("click", t), $(document).on("touchstart", t)
                },
                a = function(o, e) {
                    e.blur(), o.removeClass("sb-search-open")
                },
                i = function(o, i) {
                    function t(t) {
                        t.stopPropagation(), c.val($.trim(c.val())), i.hasClass("sb-search-open") ? i.hasClass("sb-search-open") && /^\s*$/.test(c.val()) && (t.preventDefault(), a(i, c)) : (t.preventDefault(), e(i, c), o.handleClick("search:opened"))
                    }

                    function n(o) {
                        o.stopPropagation()
                    }
                    var c = i.find("form > input.sb-search-input");
                    o.elem = i, o.input = c, i.on("click", t), i.on("touchstart", t), c.on("click", n), c.on("touchstart", n)
                },
                t = function(o, e, i) {
                    o.search = {
                        q: ""
                    }, o.submitSearch = function() {
                        if (o.searchForm.$valid && o.search.q.length > 0) {
                            var a = encodeURIComponent("site:www.singaporegp.sg " + o.search.q);
                            e.location.href = "http://www.google.com/search?q=" + a
                        }
                    }, o.handleClick = function(o) {
                        i.prepForBroadcast(o)
                    }, o.$on("handleBroadcast", function() {
                        "menu:opened" == i.message && a(o.elem, o.input)
                    })
                };
            return {
                restrict: "A",
                link: i,
                controller: ["$scope", "$window", "eventBusService", t]
            }
        };
    }, {}],
    5: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = angular.module("searchBarComponent", []).factory("eventBusService", ["$rootScope", require("../EventBus")]).directive("searchBar", [require("./ComponentDirective")]);
    }, {
        "../EventBus": 1,
        "./ComponentDirective": 4
    }],
    6: [function(require, module, exports) {
        var angular = window.angular;
        angular.module("ticketsCollectionApp", [require("./ResponsiveMenu").name, require("./SearchBar").name]).directive("googleMap", [function() {
            var o = [
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">ANG MO KIO CENTRAL</span> <br> <span style="color:#000000;">Blk 727 Ang Mo Kio Ave 6 <br> #01-4246 <br> Singapore 560727 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=Blk+727+Ang+Mo+Kio+Ave+6&ie=UTF-8&hq=&hnear=0x31da16e7e69624e1:0x8b55948b091a6d0d,727+Ang+Mo+Kio+Avenue+6,+560727&gl=sg&ei=VynnUvT4NMP3rQfv5IDoAw&ved=0CCcQ8gEwAA" target="_blank">View Map</a> </span></h4>', 1.373029, 103.845959],
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">SUNTEC CITY</span> <br> <span style="color:#000000;">3 Temasek Boulevard <br> #03-383, Suntec City Mall (between Tower 3 & 4) <br> Singapore 038983 <br> <a style="color:tomato;" href="https://www.google.com.sg/maps/place/Suntec+City+Mall/@1.2953671,103.857338,17z/data=!3m2!4b1!5s0x31da19af1faffddb:0x64dca853efdf6385!4m2!3m1!1s0x31da19aefcff45db:0x71f09dcf46064f44" target="_blank">View Map</a> </span></h4>', 1.295968, 103.858967],
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">CLEMENTI CENTRAL</span> <br> <span style="color:#000000;">3155 Commonwealth Ave West <br> #05-23/24/25/26 <br> The Clementi Mall <br> Singapore 129588 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=3155+Commonwealth+Ave+West+%2305-23/24/25/26++The+Clementi+Mall++Singapore+129588&ie=UTF-8&ei=XzHnUsjYCYH-rAeO6IDgDA&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.314918, 103.764309],
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">HARBOURFRONT CENTRE</span> <br> <span style="color:#000000;">1 Maritime Square <br> #03-22 HarbourFront Centre <br> Singapore 099253 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=1+Maritime+Square++%2303-22+HarbourFront+Centre+Singapore+099253&ie=UTF-8&ei=ejHnUraFE8qCrgfQ94H4BQ&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.264119, 103.820007],
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">JURONG POINT</span> <br> <span style="color:#000000;">1 Jurong West Central 2 <br> B1-45 Jurong Point (JP1) <br> Singapore 648886 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=1+Jurong+West+Central+2+B1-45+Singapore+648886&ie=UTF-8&ei=rDHnUs3KJIuYrger4IGQDA&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.339341, 103.705387],
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">KILLINEY ROAD</span> <br> <span style="color:#000000;">1 Killiney Road <br> Singapore 239518</span> <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=1+Killiney+Road+Singapore+239518&ie=UTF-8&ei=wTHnUpyfJYH8rAf2-ICICg&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </h4>', 1.300239, 103.840721],
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">ORCHARD</span> <br> <span style="color:#000000;">2 Orchard Turn <br> #B2 -62 ION Orchard <br> Singapore 238801 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=2+Orchard+Turn+%23B2+-62+ION+Orchard+Singapore+238801&ie=UTF-8&ei=5jHnUuqsJcTqrAeY94H4CA&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.304169, 103.831722],
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">RAFFLES PLACE</span> <br> <span style="color:#000000;">10 Collyer Quay <br> #B1-11 Ocean Financial Centre <br> Singapore 049315 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=10+Collyer+Quay+%23B1-11+Singapore+049315&ie=UTF-8&ei=Ilj0UqXtF4ewiAfM_IH4DQ&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.282964, 103.852073],
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">ROBINSON ROAD</span> <br> <span style="color:#000000;">79 Robinson Road <br> #01-06 CPF Building <br> Singapore 068897 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=79+Robinson+Road+%2301-06+CPF+Building+Singapore+068897&ie=UTF-8&ei=BTLnUoLJNoXzrQe2u4HYDw&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.27723, 103.847986],
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">SIGLAP</span> <br> <span style="color:#000000;">10 Palm Avenue <br> Singapore 456532 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=10+Palm+Avenue+Singapore+456532&ie=UTF-8&ei=HjLnUt6jF4eOrQfZvICYCg&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.313425, 103.930083],
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">SINGAPORE POST CENTRE</span> <br> <span style="color:#000000;">10 Eunos Road 8 <br> West Entrance Level 1 <br> #01-02 Singapore Post Centre <br> Singapore 408600 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=10+Eunos+Road+8+West+Entrance+Level+1+%2301-02+Singapore+Post+Centre+Singapore+408600&ie=UTF-8&ei=RTLnUuSxO8GVrgfYzIGoCg&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.319291, 103.894819],
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">TAMPINES CENTRAL</span> <br> <span style="color:#000000;">1 Tampines Central 5 <br> #01-03 CPF Tampines Building <br> Singapore 529508 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=1+Tampines+Central+5++%2301-03+CPF+Tampines+Building++Singapore+529508&ie=UTF-8&ei=YzPnUo3ENMjDrAfbm4DoDA&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.353112, 103.943709],
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">TANGLIN</span> <br> <span style="color:#000000;">56 Tanglin Road <br> Singapore 247964 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=56+Tanglin+Road+Singapore+247964&ie=UTF-8&ei=eTPnUoPBCNDjrAfh3YCQBA&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.305625, 103.822939],
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">WOODLANDS CENTRAL</span> <br> <span style="color:#000000;">900 South Woodlands Drive <br> #03-05/06 Woodlands Civic Centre <br> Singapore 730900 <br> <a style="color:tomato;" href="https://maps.google.com.sg/maps?q=900+South+Woodlands+Drive+%2303-05/06+Woodlands+Civic+Centre+Singapore+730900&ie=UTF-8&ei=sTPnUpHwGI6Krgf2loCwCQ&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.435152, 103.787195],
                    ['<h4 style="color:#000000; margin:0; font-size:12px;"><span style="font-weight:bold;">RAFFLES CITY CONVENTION CENTRE</span> <br> <span style="color:#000000;"> Level 4<br> Swissotel the Stamford<br> 2 Stamford Rd, 178882 <br/> Bras Basah Room <br> <a style="color:tomato;" href="https://www.google.com/maps/place/Swissotel+The+Stamford+Singapore/@1.2935544,103.8534273,17z/data=!3m2!4b1!5s0x31da19a428a16ab3:0xb22ccd0fb009c220!4m2!3m1!1s0x31da19a68764237f:0xf32fa2fffc1d7b8c&ved=0CAcQ_AUoAQ" target="_blank">View Map</a> </span></h4>', 1.2935544, 103.8535598]
                ],
                e = "http://maps.google.com/mapfiles/ms/icons/",
                a = [e + "red-dot.png", e + "purple-dot.png", e + "orange-dot.png", e + "purple-dot.png", e + "orange-dot.png", e + "purple-dot.png", e + "purple-dot.png", e + "purple-dot.png", e + "purple-dot.png", e + "blue-dot.png", e + "blue-dot.png", e + "blue-dot.png", e + "purple-dot.png", e + "red-dot.png", e + "purple-dot.png"],
                n = a.length,
                t = {
                    anchor: new google.maps.Point(15, 33),
                    url: e + "msmarker.shadow.png"
                },
                r = function(e, r) {
                    function s() {
                        var o = new google.maps.LatLngBounds;
                        $.each(g, function(e, a) {
                            o.extend(a.position)
                        }), p.fitBounds(o)
                    }
                    for (var l, p = new google.maps.Map(r[0], {
                        zoom: 1,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        mapTypeControl: !1,
                        streetViewControl: !1,
                        panControl: !1,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.LEFT_BOTTOM
                        }
                    }), i = new google.maps.InfoWindow({
                        maxWidth: 500
                    }), g = new Array, m = 0, c = 0; c < o.length; c++) l = new google.maps.Marker({
                        position: new google.maps.LatLng(o[c][1], o[c][2]),
                        map: p,
                        icon: a[m],
                        shadow: t
                    }), g.push(l), google.maps.event.addListener(l, "click", function() {
                        return function() {
                            p.panTo(this.getPosition()), p.setZoom(17)
                        }
                    }(l, c)), google.maps.event.addListener(l, "mouseover", function(e, a) {
                        return function() {
                            i.setContent(o[a][0]), i.open(p, e)
                        }
                    }(l, c)), m++, m >= n && (m = 0);
                    s()
                };
            return {
                restrict: "A",
                link: r
            }
        }]);
    }, {
        "./ResponsiveMenu": 3,
        "./SearchBar": 5
    }]
}, {}, [6])