(function e(t, n, r) {
    function s(o, u) {
        if (!n[o]) {
            if (!t[o]) {
                var a = typeof require == "function" && require;
                if (!u && a) return a(o, !0);
                if (i) return i(o, !0);
                throw new Error("Cannot find module '" + o + "'")
            }
            var f = n[o] = {
                exports: {}
            };
            t[o][0].call(f.exports, function(e) {
                var n = t[o][1][e];
                return s(n ? n : e)
            }, f, f.exports, e, t, n, r)
        }
        return n[o].exports
    }
    var i = typeof require == "function" && require;
    for (var o = 0; o < r.length; o++) s(r[o]);
    return s
})({
    1: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = function(a) {
            var r = {};
            return r.message = "", r.prepForBroadcast = function(a) {
                this.message = a, this.broadcastItem()
            }, r.broadcastItem = function() {
                a.$broadcast("handleBroadcast")
            }, r
        };
    }, {}],
    2: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = function(t) {
            var n = function(t) {
                    function n(e) {
                        console.log("body clicked", e.target), o(t), $(this).off("click", n), $(this).off("touchstart", n)
                    }
                    t.addClass("toggled"), $(".content").on("click", n), $(".content").on("touchstart", n)
                },
                o = function(t) {
                    t.removeClass("toggled"), $("html,body").scrollTop(0)
                },
                e = function(e, a, c) {
                    function l(a) {
                        a.stopPropagation();
                        var c = t.matchMedia("(max-width: 767px)");
                        c.matches && (r.hasClass("toggled") ? r.hasClass("toggled") && (a.preventDefault(), o(r)) : (a.preventDefault(), n(r), e.handleClick("menu:opened")), !$(a.target).is("a"))
                    }
                    var r = a.children("ul"),
                        s = $("#pull");
                    e.menu = r, s.on("click", l), s.on("touchstart", l)
                },
                a = function(t, n) {
                    t.handleClick = function(t) {
                        n.prepForBroadcast(t)
                    }, t.$on("handleBroadcast", function() {
                        "search:opened" == n.message && o(t.menu)
                    })
                };
            return {
                restrict: "A",
                link: e,
                controller: ["$scope", "eventBusService", a]
            }
        };
    }, {}],
    3: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = angular.module("responsiveMenuComponent", []).factory("eventBusService", ["$rootScope", require("../EventBus")]).directive("responsiveMenu", ["$window", require("./ComponentDirective")]);
    }, {
        "../EventBus": 1,
        "./ComponentDirective": 2
    }],
    4: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = function() {
            function o() {
                var o = !1;
                return function(e) {
                    (/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(e) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(e.substr(0, 4))) && (o = !0)
                }(navigator.userAgent || navigator.vendor || window.opera), o
            }
            var e = function(e, i) {
                    function t(o) {
                        console.log("body clicked"), a(e, i), $(this).off("click", t), $(this).off("touchstart", t)
                    }
                    e.addClass("sb-search-open"), o() || i.focus(), $(document).on("click", t), $(document).on("touchstart", t)
                },
                a = function(o, e) {
                    e.blur(), o.removeClass("sb-search-open")
                },
                i = function(o, i, t) {
                    function n(t) {
                        t.stopPropagation(), s.val($.trim(s.val())), i.hasClass("sb-search-open") ? i.hasClass("sb-search-open") && /^\s*$/.test(s.val()) && (t.preventDefault(), a(i, s)) : (t.preventDefault(), e(i, s), o.handleClick("search:opened"))
                    }

                    function c(o) {
                        o.stopPropagation()
                    }
                    var s = i.find("form > input.sb-search-input");
                    o.elem = i, o.input = s, i.on("click", n), i.on("touchstart", n), s.on("click", c), s.on("touchstart", c)
                },
                t = function(o, e, i) {
                    o.search = {
                        q: ""
                    }, o.submitSearch = function() {
                        if (o.searchForm.$valid && o.search.q.length > 0) {
                            var a = encodeURIComponent("site:www.singaporegp.sg " + o.search.q);
                            e.location.href = "http://www.google.com/search?q=" + a
                        }
                    }, o.handleClick = function(o) {
                        i.prepForBroadcast(o)
                    }, o.$on("handleBroadcast", function() {
                        "menu:opened" == i.message && a(o.elem, o.input)
                    })
                };
            return {
                restrict: "A",
                link: i,
                controller: ["$scope", "$window", "eventBusService", t]
            }
        };
    }, {}],
    5: [function(require, module, exports) {
        var angular = window.angular;
        module.exports = angular.module("searchBarComponent", []).factory("eventBusService", ["$rootScope", require("../EventBus")]).directive("searchBar", [require("./ComponentDirective")]);
    }, {
        "../EventBus": 1,
        "./ComponentDirective": 4
    }],
    6: [function(require, module, exports) {
        function parseQuery(e) {
            var r = new Object;
            if (!e) return r;
            for (var t = e.split(/[;&]/), n = 0; n < t.length; n++) {
                var s = t[n].split("=");
                if (s && 2 == s.length) {
                    var a = decodeURI(s[0]),
                        o = decodeURI(s[1]);
                    o = o.replace(/\+/g, " "), r[a] = o
                }
            }
            return r
        }
        var scripts = document.getElementsByTagName("script"),
            myScript = scripts[scripts.length - 1],
            queryString = myScript.src.replace(/^[^\?]+\??/, ""),
            params = parseQuery(queryString),
            angular = window.angular;
        angular.module("newsletterApp", [require("./ResponsiveMenu").name, require("./SearchBar").name]).controller("newsletterController", ["$scope", "$http", function(e, r) {
            e.submitted = !1, e.yearsRunning = [2016, 2015, 2014, 2013, 2012, 2011, 2010, 2009, 2008], e.newsletter = {
                txtName: "",
                txtEmail: "",
                sYear_of_Birth: "",
                sCountry: "",
                attend_which_year: [],
                with_who: [],
                with_who_other: "",
                looking_forward: [],
                looking_forward_other: "",
                latest_news_partners: ""
            }, e.toggleSelection = function(r, t) {
                var n = e.newsletter[r].indexOf(t);
                n > -1 ? e.newsletter[r].splice(n, 1) : e.newsletter[r].push(t)
            }, e.submitForm = function() {
                e.newsletterForm.$valid && r({
                    method: "POST",
                    url: params.postUrl,
                    data: $.param(e.newsletter),
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                }).success(function(r) {
                    console.log(r), e.submitted = r.success
                })
            }
        }]);
    }, {
        "./ResponsiveMenu": 3,
        "./SearchBar": 5
    }]
}, {}, [6])