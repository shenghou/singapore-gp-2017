<!DOCTYPE html>
<!--[if lt IE 7]>
<html ng-app="" lang="" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html ng-app="" lang="" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html ng-app="" lang="" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html ng-app="" lang="" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Singapore F1 - 2014 Formula 1 Night Race - Singapore Grand Prix</title>
    <meta name="viewport" content="width=device-width">
            <meta name="description" content="Enjoy the Formula 1 racing experience at the 2014 Formula 1 Singapore Airlines Singapore Grand Prix. See Formula One drivers &amp;amp; F1 racing teams in action at the Singapore GP.">
        <meta name="apple-itunes-app" content="app-id=518276170"/>
        <meta name="keywords"
              content="singapore f1, formula 1 singapore,  f1, formula one, singapore grand prix, singapore airlines, singapore gp,  sgp, formula one, f1 night race">
        <meta property="og:image" content="http://singaporegp.sg/images/logo.jpg">
        <meta property="og:title" content="Singapore F1 - 2014 Formula 1 Night Race - Singapore Grand Prix">
        <meta property="og:url" content="">
        <meta property="og:description" content="Enjoy the Formula 1 racing experience at the 2014 Formula 1 Singapore Airlines Singapore Grand Prix. See Formula One drivers &amp;amp; F1 racing teams in action at the Singapore GP.">

        <script src="//www.parsecdn.com/js/parse-1.2.19.min.js"></script>
    
                <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="http://localhost/bower_components/jquery/dist/jquery.min.js"><\/script>')</script>
        <script type="text/javascript" src="http://localhost/javascripts/libs/modernizr.min.js"></script>
                <script type="text/javascript" src="//code.angularjs.org/1.2.16/angular.min.js"></script>
        <script>window.angular || document.write('<script src="http://localhost/bower_components/angular-latest/build/angular.min.js"><\/script>')</script>

        <script type="text/javascript" src="http://localhost/javascripts/libs/smartbanner/jquery.smartbanner.js"></script>
        <link href='http://localhost/javascripts/libs/smartbanner/jquery.smartbanner.css'
              rel='stylesheet' type='text/css' media='screen'>

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,700,700italic,500italic'
              rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="/bower_components/swipejs/swipe.js"></script>
        <style>
        .swipe {
          overflow: hidden;
          visibility: hidden;
          position: relative;
        }
        .swipe-wrap {
          overflow: hidden;
          position: relative;
        }
        .swipe-wrap > div {
          float:left;
          width:100%;
          position: relative;
        }
        </style>
    
    <script type="text/javascript" src="/javascripts/genericbundle.js"></script>
    <link rel="stylesheet" href="/stylesheets/misc-privacypolicy.min.css"/>
    <style type="text/css">
        h2{
            color:black;
        }
        p{
            text-align: left;
        }
    </style>

    <meta name="apple-itunes-app" content="app-id=518276170">
    <meta name="google-play-app" content="app-id=com.singaporegp.f1">
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
    improve your experience.</p>
<![endif]-->

    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

<div class="container">            <div class="header">
        <div class="chunk row">
            <header class="col">
                                    <nav data-responsive-menu>
    <a id="pull" class="icon-menu" href=""></a>
    <ul><li class=""><a href="http://localhost">Home</a></li><li class=""><a href="http://localhost/raceguide">Race Guide</a><ul><li><a href="http://localhost/raceguide/grandstand-and-walkabout-ticketholder-guide">Grandstand and Walkabout Ticketholders</a></li><li><a href="http://localhost/raceguide/sky-club-suites-green-room-ticket-holders">Sky, Club Suites &amp; Green Room Ticketholders</a></li><li><a href="http://localhost/raceguide/paddock-club-ticketholders">Paddock Club Ticketholders</a></li><li><a href="http://localhost/raceguide/transportguide">Transport Guide</a></li></ul></li><li class=""><a href="http://localhost/tickets">Tickets</a><ul><li><a href="http://localhost/tickets/listings">2014 Tickets</a></li><li><a href="http://localhost/tickets/listing/1">Grandstands</a></li><li><a href="http://localhost/tickets/listing/3">Walkabouts</a></li><li><a href="http://localhost/tickets/listing/4">Combination Packages</a></li><li><a href="http://localhost/tickets/listing/10">Wheelchair Accessible Platforms</a></li><li><a href="http://localhost/tickets/sales-channels/singapore">Authorised SISTIC Outlets in Singapore</a></li><li><a href="http://localhost/tickets/sales-channels/international">Authorised International Agents</a></li><li><a href="http://localhost/tickets/packages/hotels-and-travel">Hotels and Travel Packages</a></li><li><a href="http://localhost/tickets/packages/hospitality">Hospitality and Executive Packages</a></li></ul></li><li class=""><a href="http://localhost/on-track">On-track</a><ul><li><a href="http://localhost/on-track/2014-race-calendar">2014 Race Calendar</a></li><li><a href="http://localhost/on-track/2014-race-schedule">2014 Race Schedule</a></li><li><a href="http://localhost/on-track/season-results" target="_blank">Season Results</a></li><li><a href="http://localhost/on-track/driver-standings" target="_blank">Driver Standings</a></li><li><a href="http://localhost/on-track/sgp-highlights">Singapore Grand Prix Highlights</a></li><li><a href="http://localhost/on-track/circuit-park-map">Circuit Park Map</a></li><li><a href="http://localhost/on-track/teams-and-drivers">Teams and Drivers</a></li><li><a href="http://www.raceofficials.singaporegp.sg" target="_blank">Race Officials</a></li></ul></li><li class=""><a href="http://localhost/off-track">Off-track</a><ul><li><a href="http://localhost/off-track/headliners">Entertainment Highlights</a></li><li><a href="http://localhost/off-track/past-headliners">Past Headliners</a></li></ul></li><li class=""><a href="http://localhost/media">Media</a><ul><li><a href="http://localhost/media/press-release">Press Release</a></li><li><a href="http://localhost/media/media-site">Media Site</a></li><li><a href="http://localhost/media/accreditation">Media Accreditation</a></li><li><a href="http://localhost/media/entry-form">Singapore Entry Form</a></li></ul></li><li class=""><a href="http://localhost/fanzone">Fanzone</a><ul><li><a href="http://localhost/contests">Contests</a></li><li><a href="http://localhost/on-track/gallery/2013/photos">Photo Gallery</a></li><li><a href="http://localhost/on-track/gallery/2013/videos">Video Gallery</a></li><li><a href="http://localhost/fanzone/wallpapers">Wallpaper Gallery</a></li><li><a href="http://localhost/fanzone/pit-stop-challenge">Pit Stop Challenge</a></li></ul></li><li class=""><a href="http://localhost/rev-up-singapore">Rev Up Singapore!</a></li></ul></nav>
                            </header>
        </div>
    </div>
        
        <div class="content">
        
    <section class="selection-panel">
        <div class="chunk">
    <div class="row">
        <div class="top-bar col">
            <div class="img-wrapper"><a href="http://localhost"><img src="/images/logo_w_date_long.png" alt=""/></a></div>
        </div>

        <div class="social-bar col">
            <div class="addthis-widget">
                <!-- AddThis Button BEGIN -->
                <div class="addthis_toolbox addthis_default_style addthis_32x32_style" style="width: 250px;margin-left:20px;">
                    <a class="addthis_button_facebook"></a>
                    <a class="addthis_button_twitter"></a>
                    <a class="addthis_button_email"></a>
                    <a class="addthis_button_print"></a>
                    <a class="addthis_button_compact"></a>
                    <a class="addthis_counter addthis_bubble_style"></a>
                </div>
                <script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5363146506329a6f"></script>
                <!-- AddThis Button END -->
            </div>
        </div>
    </div>
</div>    </section>

    <section class="maintenance">
        <div class="chunk row">
            <div class="legalese col">
                <div class="maintenance-text">
                    <h2>ERROR 404 - The page doesn't exist!</h2>
                    <p>
                    We have been unable to find the page that you have requested. Please check that you have entered the correct URL.
                    If the error still persists, please use the <a href="http://localhost">Singapore GP Home page</a> or click on one of the suggested links below:

                    <h4><a href="http://localhost">Singapore GP Home page</a></h4>
                    <h4><a href="http://localhost/tickets">Buy Singapore F1 Tickets</a></h4>
                    <h4><a href="http://localhost/media/press-release">Singapore Grand Prix Press Release</a></h4>
                    <h4><a href="http://localhost/newsletter">Sign up for Singapore GP Newsletter</a></h4>
                    </p>
                                    </div>
            </div>
        </div>
    </section>

    </div>
    
    <script type="text/javascript">
    Parse.initialize("ZR824TlIcihwkq4hHXjZmftak8wIzqFmq4EkU2Va", "QsnntqPa1o5S0a2l4zyMvWAJw6aFAwjddMake0yk");
    </script>

            <div class="footer">
                <div class="row">
            <div class="chunk">
                <div class="quick-links col">
                    <h5>Quick Links</h5>
<ul>
    <li><a class="mobile-app" href="http://localhost/media/mobile-app" target="_blank">Download Mobile App</a></li>
    <li><a class="newsletter" href="http://localhost/newsletter" target="_blank">Subscribe Newsletter</a></li>
    <li><a class="race-officials" href="http://www.raceofficials.singaporegp.sg/" target="_blank">Race Officials</a></li>
    <li><a class="facebook" href="https://www.facebook.com/SingaporeGP" target="_blank">Like us on Facebook</a></li>
    <li><a class="twitter" href="https://twitter.com/F1NightRace" target="_blank">Follow us on Twitter</a></li>
    <li><a class="youtube" href="http://www.youtube.com/user/singaporegrandprix" target="_blank">Watch us on YouTube</a></li>
    <li><a class="pinterest" href="https://www.pinterest.com/SingaporeGP" target="_blank">Follow us on Pinterest</a></li>
</ul>                </div>
            </div>
        </div>
        
        <div class="row">
            <footer class="col">
                                    <div class="chunk row">
                        <div class="site-map col">
                            <h5>Explore this site</h5>

<ul class="level-1">
    <li><h6><a href="http://localhost/on-track">ON-TRACK</a></h6>
        <ul class="level-2">
            <li><a href="http://localhost/on-track/2014-race-calendar">2014 race calendar</a></li>
            <li><a href="http://localhost/on-track/season-results" target="_blank">Season results</a></li>
            <li><a href="http://localhost/on-track/driver-standings" target="_blank">Driver standings</a></li>
            <li><a href="http://localhost/on-track/sgp-highlights">Singapore grand prix highlights</a></li>
            <li><a href="http://localhost/on-track/circuit-park-map">Circuit park map</a></li>
            <li><a href="http://localhost/on-track/teams-and-drivers">Teams &amp; drivers</a></li>
            <li><a href="http://www.raceofficials.singaporegp.sg/" target="_blank">Race Officials</a></li>
        </ul>
    </li>
    <li><h6><a href="http://localhost/off-track">OFF-TRACK</a></h6>
        <ul class="level-2">
            <li><a href="http://localhost/off-track/headliners">Entertainment Highlights</a></li>
            <li><a href="http://localhost/off-track/past-headliners">Past Headliners</a></li>
        </ul>
    </li>
    <li><h6><a href="http://localhost/media">MEDIA</a></h6>
        <ul class="level-2">
            <li><a href="http://localhost/media/press-release">Press releases</a></li>
            <li><a href="http://localhost/media/media-site">Media site</a></li>
            <li><a href="http://localhost/media/accreditation">Media Accreditation</a></li>
            <li><a href="http://localhost/media/entry-form">Singapore Entry Form</a></li>
        </ul>
    </li>
</ul>

<ul class="level-1">
    <li><h6><a href="http://localhost/tickets">TICKETS</a></h6>
        <ul class="level-2">
            <li><a href="http://localhost/tickets/listings">All 2014 tickets</a></li>
            <li><a href="http://localhost/tickets/packages/hospitality">Hospitality and Executive Packages</a></li>
            <li><a href="http://localhost/tickets/sales-channels/singapore">Authorised SISTIC Outlets In Singapore</a></li>
            <li><a href="http://localhost/tickets/sales-channels/international">Authorised International Agents</a></li>
            <li><a href="http://localhost/tickets/packages/hotels-and-travel">Hotels &amp; travel packages</a></li>
        </ul>
    </li>
    <li><h6><a href="http://localhost/fanzone">FANZONE</a></h6>
        <ul class="level-2">
                                                <li><a href="http://localhost/fanzone/pit-stop-challenge">Pit Stop Challenge</a></li>
            <li><a href="http://localhost/fanzone/wallpapers">Wallpaper Gallery</a></li>
            <li><a href="http://localhost/on-track/gallery/2013/photos">Photo Gallery</a></li>
            <li><a href="http://localhost/on-track/gallery/2013/videos">Video Gallery</a></li>
        </ul>
    </li>
</ul>

<ul class="level-1">
    <li><h6><a href="http://localhost/rev-up-singapore">REV UP SINGAPORE!</a></h6></li>
        <ul class="level-2">
            <li><a href="http://localhost/rev-up-singapore/thursday-pit-lane-experience">Thursday Pit Lane Experience</a></li>
            <li><a href="http://localhost/rev-up-singapore/karting-training">Karting Training Programme and Championship</a></li>
            <li><a href="http://localhost/rev-up-singapore/pit-stop-at-the-libraries">Pit Stop @ The Libraries</a></li>
            <li><a href="http://localhost/rev-up-singapore/behind-the-scenes">Behind-the-Scenes Tours</a></li>
            <li><a href="http://localhost/rev-up-singapore/kartnival">Rev Up Singapore! Kartnival</a></li>
            <li><a href="http://localhost/rev-up-singapore/school-visit">Singapore GP School Visit</a></li>
        </ul>
    <li><h6>OTHERS</h6>
        <ul class="level-2">
            <li><a href="http://singaporegp.sg/download/FAQ.pdf" target="_blank">FAQ</a></li>
            <li><a href="http://singaporegp.sg/download/General_TnC_2014.pdf" target="_blank">Terms &amp; conditions</a></li>
            <li><a href="http://localhost/privacy-policy">Privacy policy</a></li>
            <li><a href="http://localhost/job-opportunities">Careers</a></li>
            <li><a href="http://singaporegp.sg/download/2014_indemnity_form.pdf" target="_blank">Indemnity form</a></li>
            <li><a href="http://localhost/contact-us">Contact us</a></li>
            <li><a href="http://localhost/tenders">Tender</a></li>
        </ul>
    </li>
</ul>                        </div>

                        <div class="copy-right col">
                            <div class="sponsors-copyright">
    <h5>Title Sponsor</h5>

    <div class="img-wrapper title-sponsor"><a href="http://www.singaporeair.com/" target="_blank"><img src="/images/footer_sia.jpg" alt=""/></a></div>

    <h5>Supported by &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Held In</h5>

    <div class="img-wrapper">
    	<img id="Image-Maps-Com-image-maps-2014-04-28-052909" src="/images/footer_ys_sgp.png" border="0" width="241" height="66" orgWidth="241" orgHeight="66" usemap="#image-maps-2014-04-28-052909" alt="" />
		<map name="image-maps-2014-04-28-052909" id="ImageMapsCom-image-maps-2014-04-28-052909">
			<area  alt="" title="YS" href="http://www.yoursingapore.com/" target="_blank" shape="rect" coords="0,0,108,66" style="outline:none;"/>
			<area  alt="" title="" href="http://www.singaporegp.sg/" target="_blank" shape="rect" coords="124,0,241,66" style="outline:none;"/>
		</map>
	</div>

    <p>&copy; 2014 Singapore GP Pte. Ltd. All rights reserved. The F1 FORMULA 1 logo, F1, FORMULA 1, FIA FORMULA ONE WORLD
        CHAMPIONSHIP, GRAND PRIX and SINGAPORE GRAND PRIX and related marks are trade marks of Formula One Licensing BV, a
        Formula One group company.</p>
</div>
                        </div>
                    </div>
                            </footer>
        </div>
    </div>
        </div>



    <!-- Google Analytics -->
    <script>
        var staging = /staging/;
        var liveHostname = /singaporegp\.sg/;

        // track if on hosting server and not in staging
        if (liveHostname.test(window.location.hostname) && !staging.test(window.location.hostname)) {
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-3141840-1', 'auto');
            ga('send', 'pageview');
        }
    </script>
    <!-- End Google Analytics -->

<script type="text/javascript">
    angular.element.smartbanner({author:'Official App',icon:'https://lh3.ggpht.com/v2_KuwLNF7cBWLM-fKIXDIlSsKe-fN1q0uBkvK02ooxwRLtB6DJGFCPKYoUZNie1_-gn=w300-rw',inGooglePlay:'Download it for free now!'});
</script>

</body>
</html>
