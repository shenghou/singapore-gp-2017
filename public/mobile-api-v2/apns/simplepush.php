<?php

// Put your device token here (without spaces):
$deviceToken = '4521920e7902e0a66fd489187008a4bcb881f4610e533136c562edef8b255594';

// Put your private key's passphrase here:
$passphrase = '123456';

// Put your alert message here:
$message = "Check the latest Singapore GP contes: BE SPOTTED";

////////////////////////////////////////////////////////////////////////////////

$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', 'F1-Development-Push.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
$fp = stream_socket_client(
	'ssl://gateway.sandbox.push.apple.com:2195', $err,
	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
$body['aps'] = array(
	# 'alert' => $message,
	# 'sound' => 'default'
	"Title" => "2013 FORMULA 1 SHELL BELGIAN GRAND PRIX (Spa-Francorchamps)",
	"Type" => "Results",
	"Id" => 13
);

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result)
	echo 'Message not delivered' . PHP_EOL;
else
	echo 'Message successfully delivered' . PHP_EOL;

// Close the connection to the server
fclose($fp);
