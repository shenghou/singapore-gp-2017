<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Raceguide_Collection_model extends Db_model
{
    public function __construct()
    {
		parent::__construct();
    }

	public function collection()
	{
        $hostname = 'http://'. $_SERVER['SERVER_NAME'];
		$sql = <<<sql
SELECT rgc.id,rgc.title,rgc.img,CONVERT(rgc.sort,SIGNED) as sort,
CONCAT('{$hostname}',rgc.img) as image,
rgc.slug as url
FROM api_raceguide_collection rgc
WHERE rgc.hidden != 1
ORDER BY rgc.sort;
sql;

		$res = $this->query($sql);
		$rtn = array();

		foreach ($res as $row) {
			$rtn[] = $row; // convert from stdobj to array
		}

        // raceguide
        $sql = <<<sql
SELECT id,collection_id,title,sort,img as image,
-- CONCAT('{$hostname}',img) as image,
CONCAT('{$hostname}/',slug) as url
FROM api_raceguide
WHERE hidden != 1
sql;
        $raceguides = $this->query($sql);

        foreach($rtn as &$row){
            $row['sort'] = (int)$row['sort'];

            if(!is_array($row['raceguide']))
                $row['raceguide'] = array();

            foreach($raceguides as $raceguiderow){
                if((int)$raceguiderow['collection_id'] == (int)$row['id']){
                    $raceguiderow['sort'] = (int)$raceguiderow['sort'];
                    array_push($row['raceguide'],$raceguiderow);
                }
            }
        }

		return $rtn;
	}

}
