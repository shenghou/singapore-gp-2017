<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 * to intereactive with database
 */
class Ticketing_model extends Db_model
{
    public function __construct()
    {
		parent::__construct();
    }

	public function listing()
	{
		$sql = <<<sql
SELECT id,
REPLACE(ticketing_category, ' (GROUP BOOKING SPECIAL)', '') AS name,
IF(ticketing_category = REPLACE(ticketing_category, ' (GROUP BOOKING SPECIAL)', ''), '', '(GROUP BOOKING SPECIAL)') AS additional_field,
ticketing_type AS type,
more_detail_name AS more
FROM ticketing_category
WHERE status='Active'
ORDER BY sort
sql;

		$categories = $this->query($sql);

		$sql = <<<sql
SELECT
id,
ticketing_category_id AS category_id,
ticket_subcategory AS name
FROM ticketing_info
WHERE status='Active' AND ticketing_year = 2013
ORDER BY sort
sql;

		$info = $this->query($sql);
		$infos = array();
		foreach ($info as $row) {
			$tid = $row['category_id'];
			if (!isset($infos[$tid])) {
				$infos[$tid] = array();
			}

			unset($row['category_id']);

			$infos[$tid][] = $row;
		}

		$rtn = array();
		foreach ($categories as $category) {
			$id = $category['id'];
			if (isset($infos[$id])) {
				$category['options'] = $infos[$id];
			}
			$category["more"] = strip_tags($category["more"]);
			$rtn[] = $category;
		}
		return $rtn;
	}

	public function category($id, $option)
	{
		$sql = <<<sql
SELECT
tc.ticketing_category AS category,
tc.ticketing_type AS type,
ti.ticket_subcategory AS `option`,
ti.ticket_subcategory_description AS description,
ti.access_to_zones,
ti.accessible_entertainment_stages,
ti.accessible_info,
ti.announcement,
ti.announcement_bottom,
ti.description,
ti.view_type,
ti.view_type_name,
ti.panoramic_view_path,
ti.related_photo_path,
ti.seating_plan_path,
ti.seating_plan_thumb_path,
ti.zone_access_path,
ti.zone_access_thumb_path,
ti.zone_access_disclaimer
FROM ticketing_category AS tc
INNER JOIN ticketing_info ti ON (tc.id = ti.ticketing_category_id AND ti.id = ? AND ti.status = 'Active')
WHERE tc.id = ?
sql;

		$rows = $this->bind($sql, array($option, $id));
		if (empty($rows)) {
			return false;
		}

		$info = $rows[0];
		$info['accessible_entertainment_stages'] = explode(', ', $info['accessible_entertainment_stages']);
		array_walk($info['accessible_entertainment_stages'], 'trim');
		#unset($info["accessible_entertainment_stages"][2]);

		$info['access_to_zones'] = explode(', ', $info['access_to_zones']);
		array_map('trim', $info['access_to_zones']);

		switch ($info['type']) {
			case '3-Day Ticket':
				$table = 'ticketing_three_day';
				$cols = '"" as location, "" as day, t.ticketing_note as note, trim(t.ticketing_price) AS price, ';
				$field = $option;
				break;

			case '3-Day Combination Ticket':
				$table = 'ticketing_three_day_combination';
				$cols = 't.location, t.day_word as day, t.ticketing_note as note, trim(t.ticketing_price) AS price, ';
				$field = $option;
				break;

			case 'Single Day Ticket':
				$table = 'ticketing_single_day';
				$cols = '"" as location, "" as day, t.ticketing_note as note, trim(t.ticketing_price) AS price, ';
				$field = $option;
				break;
		}

		$sql = <<<sql
SELECT
tp.phase_description AS type,
tp.phase_date AS date,
{$cols}
trim(t.ticketing_price) AS price, 
trim(t.ticketing_description) AS description,
t.ticketing_status AS status,
t.ticketing_link AS buy_url,
SUBSTRING_INDEX(t.ticketing_link, '=', -1) AS sistic_code
FROM {$table} t
INNER JOIN ticketing_phase AS tp ON (tp.id = t.ticketing_phase_id AND tp.status=1)
WHERE t.ticketing_info_id = ? AND tp.id = 5
ORDER BY IF(tp.phase_description LIKE 'REGULAR%', 1, 0), t.sort
sql;

		$tickets = $this->bind($sql, array($field));
		if (empty($tickets)) {
			$info["status"] = "Sold Out";
		} else {
			$info["status"] = "Available";
		}

		$info['options'] = array();
		foreach ($tickets as $ticket) {
			$ticket['clean_price'] = (int)preg_replace('{[^0-9]}', '', $ticket['price']);
			$ticket['clean_description'] = preg_match('{Minimum}i', $ticket['description']) ? 4 : 1;
			$ticket['description'] = strip_tags($ticket["description"]);

			if (strtolower($ticket["status"]) == "sold out") {
				$ticket["note" ] = '<div class="label">Sold Out</div>';
				# $ticket["price"] = "";
				$info["status"] = "Sold Out";
			}

			# remove br from hotline<br>
			$ticket["note"] = str_replace("hotline<br>", "hotline ", $ticket["note"]);

			# hack
			if ($id == 6) 
			{
				switch ($option)
				{
					case 16:
						$ticket["note"] = <<<html
<div class="label">Limited Availability</div> Please book via Corporate Sales team by calling +65 6731 5900 or<br>email hospitality@singaporegp.sg
html;
						break;
				}
			}

			$info['options'][] = $ticket;
		}

		$sql = <<<sql
SELECT related_photo_thumb,related_photo,caption
FROM ticketing_related_photo
WHERE ticketing_info_id = ?
order by sort
sql;
		$photos = $this->bind($sql, array($option));
		$info['related_photos'] = array();
		foreach ($photos as $photo) {
			if (empty($photo["related_photo_thumb"])) {
				# $photo["related_photo_thumb"] = str_replace(".jpg", "-sw70.jpg", $photo["related_photo"]);
				$photo["related_photo_thumb"] = $photo["related_photo"];
			}
			$info['related_photos'][] = $photo;
		}

		# another hack
		if ($id == 6 && ($option == 25 || $option == 26 || $option == 27))
		{
			$info["announcement"] = <<<html
To find out more about the<br>hospitality packages available,<br>please contact the Corporate Sales<br>team by calling +65 6731 5900 or<br>email hospitality@singaporegp.sg
html;
		}

		return $info;
	}

	public function get_singapore_region()
	{
		$sql = "select distinct region from ticketing_agent_singapore order by region";
		$res = $this->query($sql);
		$rtn = array();
		foreach ($res as $row)
		{
			$rtn[] = $row["region"];
		}
		return $rtn;
	}

	public function get_singapore_region_listing($region)
	{
		$sql = <<<sql
select *
from ticketing_agent_singapore
where region = ?
order by sort
sql;
		$rtn = array();
		$res = $this->bind($sql, array($region));
		if ($res)
		{
			foreach ($res as $row)
			{
				foreach ($row as $key => $val)
				{
					$row[$key] = str_replace("\r\n", PHP_EOL, $val);
				}
				$rtn[] = $row;
			}
			return $rtn;
		}
		else
		{
			return array();
		}
	}

	public function get_international_country()
	{
		$sql = <<<sql
select region.id, region.region, country.id as country_id, country.country
from ticketing_agent_international as intl
inner join ticketing_agent_inter_region as region on (intl.region_id = region.id)
left join ticketing_agent_inter_country as country on (intl.country_id = country.id)
where intl.sort != 999
group by intl.region_id, intl.country_id
order by region.id, region.region, country.country
sql;
		$res = $this->query($sql);
		$rtn = array();
		foreach ($res as $row)
		{
			$id = $row["id"];
			$name = $row["region"];
			if ($row["id"] != 2 && !empty($row["country"]))
			{
				$id .= ':'.$row["country_id"];
				$name .= " | ".$row["country"];
			}

			$rtn[$id] = $name;
		}
		return $rtn;
	}

	public function get_international_country_listing($map, $country)
	{
		$country = urldecode($country);
		$idx = array_search($country, $map);
		$tmp = explode(":", $idx);

		$whr = array();
		$bind = array();
		if (isset($tmp[0]))
		{
			$whr["region"] = "intl.region_id = ?";
			$bind[] = $tmp[0];
		}

		if (isset($tmp[1]))
		{
			$whr["country"] = "intl.country_id = ?";
			$bind[] = $tmp[1];
		}

		$whrs = implode(" AND ", $whr);

		$sql = <<<sql
select intl.*, region.region, country.country
from ticketing_agent_international as intl
inner join ticketing_agent_inter_region as region on (intl.region_id = region.id)
left join ticketing_agent_inter_country as country on (intl.country_id = country.id)
where {$whrs} and intl.sort != 999
order by intl.sort
sql;

		$rtn = array();
		$res = $this->bind($sql, $bind);
		if ($res)
		{
			foreach ($res as $row)
			{
				foreach ($row as $key => $val)
				{
					$row[$key] = str_replace("\r\n", PHP_EOL, $val);
				}
				$rtn[] = $row;
			}
			return $rtn;
		}
		else
		{
			return array();
		}
	}

}

