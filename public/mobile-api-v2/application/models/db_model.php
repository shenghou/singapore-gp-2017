<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 * to intereactive with database
 */
class Db_model extends CI_Model
{
	protected $cms_path = 'http://web2.singaporegp.sg/sgp_admin/Uploaded';
	protected $ns = array(
		'news' => 'press_release',
		'videos' => 'video_gallery_mp4',
		'album' => 'photo_album',
		'gallery' => 'photo_gallery',
	);

    public function __construct()
    {
		parent::__construct();
    }

	protected function bind($sql, $arr)
	{
		return $this->firing($sql, $arr);
	}

	protected function query($sql)
	{
		return $this->firing($sql);
	}

	public function escape($str)
	{
		return $this->db->escape($str);
	}

	protected function firing($sql, $arr = null)
	{
		try {
			if (empty($arr)) {
				$res = $this->db->query($sql);
			} else {
				$res = $this->db->query($sql, $arr);
			}

			if (is_bool($res))
			{
				return $res;
			}

			$rtn = array();
			foreach ($res->result_array() as $row) {
				# casting
				if (isset($row['id'])) {
					$row['id'] = (int) $row['id'];
				}

				if (isset($row['selected'])) {
					$row['selected'] = (boolean) $row['selected'];
				}

				if (isset($row['year'])) {
					$row['year'] = (int) $row['year'];
				}

				if (isset($row['count'])) {
					$row['count'] = (int) $row['count'];
				}

				if (isset($row['album'])) {
					$row['album'] = (int) $row['album'];
				}

				$rtn[] = $row;
			}
			$res->free_result();
			return $rtn;
		} catch (exception $e) {
			json_error($e->getMessage());
		}
	}
}

