<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 * to intereactive with database
 */
class Videos_model extends Db_Model
{
    public function __construct()
    {
		parent::__construct();
    }

	public function listing()
	{
		$sql = <<<sql
SELECT thumbnail_link, video_flv_link, title, video_link, id, video_youtube_link
FROM {$this->ns['videos']}
ORDER BY featured_video DESC, sort ASC
sql;
		return $this->query($sql);
	}

}

