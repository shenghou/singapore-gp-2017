<?php
if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * to intereactive with database
 */
class Ticketing_model extends Db_model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function listing()
    {
        $sql = <<<sql
SELECT id,
REPLACE(ticketing_category, ' (GROUP BOOKING SPECIAL)', '') AS name,
IF(ticketing_category = REPLACE(ticketing_category, ' (GROUP BOOKING SPECIAL)', ''), '', '(GROUP BOOKING SPECIAL)') AS additional_field,
ticketing_type AS type,
more_detail_name AS more
FROM ticketing_category
WHERE status='Active'
ORDER BY sort
sql;

        $categories = $this->query($sql);

        $sql = <<<sql
SELECT
id,
ticketing_category_id AS category_id,
ticket_subcategory AS name
FROM ticketing_info
WHERE status='Active' AND ticketing_year = 2014 AND id!=47
ORDER BY sort
sql;

        $info = $this->query($sql);
        $infos = array();
        foreach ($info as $row) {
            $tid = $row['category_id'];
            if (!isset($infos[$tid])) {
                $infos[$tid] = array();
            }

            unset($row['category_id']);

            $infos[$tid][] = $row;
        }

        $rtn = array();
        foreach ($categories as $category) {
            $id = $category['id'];
            $category['special'] = false;
            if (isset($infos[$id]))
            {
                $category['options'] = $infos[$id];
            }
            else
            {
                $category["options"] = array();
            }
            $category["more"] = strip_tags($category["more"]);
            $rtn[] = $category;
        }

        # super early bird enable/disable
        $this->load->config("super-early-bird");
        $isSuperEarlyBirdOn = $this->config->item("isSuperEarlyBirdOn");

        if($isSuperEarlyBirdOn){
            $seb = $rtn[0];
            $seb["id"] = 20;
            $seb["name"] = "2017 SUPER EARLY BIRD";
            $seb["additional_field"] = "12 SEP 2016 to 2 DEC 2016";
            $seb["type"] = "3-DAY";
            $seb["special"] = true; // for
            $seb["more"] = "View more details and full pricing"; // for
            $seb["options"] = array(
                array("id" => 42, "name" => "TURN 3 PREMIER GRANDSTAND"),
                array("id" => 47, "name" => "SUPER PIT"),
                array("id" => 1, "name" => "PIT GRANDSTAND"),
                array("id" => 33, "name" => "ZONE 1 WALKABOUT COMBINATION"),
                array("id" => 8, "name" => "CONNAUGHT GRANDSTAND"), //SHENGHOU
                array("id" => 3, "name" => "PADANG GRANDSTAND"),
                array("id" => 9, "name" => "PREMIER WALKABOUT"), //SHENGHOU
                array("id" => 4, "name" => "BAY GRANDSTAND"), //SHENGHOU
                // array("id" => 42, "name" => "TURN 3 PREMIER GRANDSTAND"), //SHENGHOU
            );
            array_unshift($rtn, $seb);
        }


        return $rtn;
    }

    public function listingEarlybird()
    {
        $sql = <<<sql
SELECT id,
REPLACE(ticketing_category, ' (GROUP BOOKING SPECIAL)', '') AS name,
IF(ticketing_category = REPLACE(ticketing_category, ' (GROUP BOOKING SPECIAL)', ''), '', '(GROUP BOOKING SPECIAL)') AS additional_field,
ticketing_type AS type,
more_detail_name AS more
FROM ticketing_category
WHERE status='Active'
AND
id <> '9'
ORDER BY sort
sql;

        $categories = $this->query($sql);

        $sql = <<<sql
SELECT
id,
ticketing_category_id AS category_id,
ticket_subcategory AS name
FROM ticketing_info
WHERE status='Active' AND ticketing_year = 2014 AND ticketing_category_id <> '9'
ORDER BY sort
sql;

        $info = $this->query($sql);
        $infos = array();
        foreach ($info as $row) {
            $tid = $row['category_id'];
            if (!isset($infos[$tid])) {
                $infos[$tid] = array();
            }

            unset($row['category_id']);

            $infos[$tid][] = $row;
        }

        $rtn = array();
        foreach ($categories as $category) {
            $id = $category['id'];
            $category['special'] = false;
            if (isset($infos[$id]))
            {
                $category['options'] = $infos[$id];
            }
            else
            {
                $category["options"] = array();
            }
            $category["more"] = strip_tags($category["more"]);
            $rtn[] = $category;
        }

        ## super early bird enable/disable
        #$this->load->config("super-early-bird");
        #$isSuperEarlyBirdOn = $this->config->item("isSuperEarlyBirdOn");

        #if($isSuperEarlyBirdOn){
        #    $seb = $rtn[0];
        #    $seb["id"] = 20;
        #    $seb["name"] = "2016 SUPER EARLY BIRD";
        #    $seb["additional_field"] = "14 SEP 2015 to 27 SEP 2015";
        #    $seb["type"] = "3-DAY";
        #    $seb["special"] = true; // for
        #    $seb["options"] = array(
        #        array("id" => 1, "name" => "PIT GRANDSTAND"),
        #        array("id" => 7, "name" => "STAMFORD GRANDSTAND"),
        #        array("id" => 3, "name" => "PADANG GRANDSTAND"),
        #        array("id" => 4, "name" => "BAY GRANDSTAND"),
        #        array("id" => 33, "name" => "ZONE 1 WALKABOUT COMBINATION"),
        #    );
        #    array_unshift($rtn, $seb);
        #}


        return $rtn;
    }

    public function listingId($id)
    {

        if($id == 20) {
            $sql = <<<sql
SELECT
id,
ticketing_category_id AS category_id,
ticket_subcategory AS name
FROM ticketing_info
WHERE ticketing_category_id = ? AND status='Active' AND ticketing_year = 2014
ORDER BY sort
sql;
        } else {
            $sql = <<<sql
SELECT
id,
ticketing_category_id AS category_id,
ticket_subcategory AS name
FROM ticketing_info
WHERE ticketing_category_id = ? AND status='Active' AND ticketing_year = 2014
AND id != 47
ORDER BY sort
sql;
        }
        $info = $this->bind($sql, array($id));


        $this->load->config("super-early-bird");
        $isSuperEarlyBirdOn = $this->config->item("isSuperEarlyBirdOn");

        # super early bird enable/disable.
        if($isSuperEarlyBirdOn) {
            if($id == 20):
                array_push($info,
                    array("id" => 42, "name" => "TURN 3 PREMIER GRANDSTAND", "category_id" => 20),
                    array("id" => 47, "name" => "SUPER PIT", "category_id" => 20),
                    array("id" => 1, "name" => "PIT GRANDSTAND", "category_id" => 20),
                    array("id" => 33, "name" => "ZONE 1 WALKABOUT COMBINATION", "category_id" => 20),
                    array("id" => 8, "name" => "CONNAUGHT GRANDSTAND", "category_id" => 20), //SHENGHOU
                    array("id" => 3, "name" => "PADANG GRANDSTAND", "category_id" => 20),
                    array("id" => 9, "name" => "PREMIER WALKABOUT", "category_id" => 20), //SHENGHOU
                    array("id" => 4, "name" => "BAY GRANDSTAND", "category_id" => 20) //SHENGHOU

                );
            endif;
        }


        return $info;
    }

    public function category($id, $option)
    {
        $mutant = $id;
        if ($id == 20) {
            $id = 1;
        }

        if ($option == 33)
        {
            $id = 4;
        }

        if ($option == 9) // premier walkabout
        {
            $id = 3;
        }


        if ($option == 42) // turn 3 premier
        {
            $id = 2;
        }

        if ($option == 47) // super pit gs
        {
            $id = 2;
        }

        $sql = <<<sql
SELECT
tc.ticketing_category AS category,
tc.ticketing_type AS type,
ti.ticket_subcategory AS "option",
ti.is_new as is_new,
ti.ticket_subcategory_description AS description,
ti.access_to_zones,
ti.accessible_entertainment_stages,
ti.accessible_info,
ti.announcement,
ti.announcement_bottom,
ti.description,
ti.view_type,
ti.view_type_name,
ti.panoramic_view_path,
ti.related_photo_path,
ti.seating_plan_path,
ti.seating_plan_thumb_path,
ti.zone_access_path,
ti.zone_access_thumb_path,
ti.zone_access_disclaimer
FROM ticketing_category AS tc
INNER JOIN ticketing_info ti ON (tc.id = ti.ticketing_category_id AND ti.id = ? AND ti.status = 'Active')
WHERE tc.id = ?
sql;



// ti.imagePath was removed in the SELECT query

        $rows = $this->bind($sql, array($option, ($mutant == 20 && ($option == 2)) ? 2 : $id));


        if (empty($rows)) {
            return false;
        }

        $info = $rows[0];
        $info['accessible_entertainment_stages'] = explode(', ', $info['accessible_entertainment_stages']);
        array_walk($info['accessible_entertainment_stages'], 'trim');

        $info['mapPath'] = "http://singaporegp.sg/ticket/map_3d_2014/map_" . $option . ".jpg";

        $info['access_to_zones'] = explode(', ', $info['access_to_zones']);
        array_map('trim', $info['access_to_zones']);

        switch ($info['type']) {
            case '3-Day Ticket':
                $table = 'ticketing_three_day';
                $cols = '"" as location, "" as day, t.ticketing_note as note, trim(t.ticketing_price) AS price, ';
                $field = $option;
                break;

            case '3-Day Combination Ticket':
                $table = 'ticketing_three_day_combination';
                $cols = 't.location, t.day_word as day, t.ticketing_note as note, trim(t.ticketing_price) AS price, ';
                $field = $option;
                break;

            case 'Single Day Ticket':
                $table = 'ticketing_single_day';
                $cols = '"" as location, "" as day, t.ticketing_note as note, trim(t.ticketing_price) AS price, t.id as single_day_id, ';
                $field = $option;
                break;
        }

        $setting = json_decode('{"ticketing":{"isEarlyBird":false}}',true); // json_decode(file_get_contents('https://dl.dropboxusercontent.com/u/45416284/AppSettings/sgpwebsite.json'),true); // null;//

        $isEarlyBird = isset($setting['ticketing']['isEarlyBird']) ? $setting['ticketing']['isEarlyBird'] : false;

        $phaseId = $isEarlyBird ? 4 : 5;

        $sql = <<<sql
SELECT
tp.phase_description AS type,
tp.phase_date AS date,
{$cols}
trim(t.ticketing_price) AS price,
trim(t.ticketing_description) AS description,
t.ticketing_status AS status,
t.ticketing_link AS buy_url,
SUBSTRING_INDEX(SUBSTRING_INDEX(t.ticketing_link, '=', -2), '&', 1) AS sistic_code
FROM {$table} t
INNER JOIN ticketing_phase AS tp ON (tp.id = t.ticketing_phase_id AND tp.status=1)
WHERE t.ticketing_info_id = ? AND tp.id = $phaseId
ORDER BY IF(tp.phase_description LIKE 'REGULAR%', 1, 0), t.sort
sql;

        $tickets = $this->bind($sql, array($field));
        if (empty($tickets)) {
            $info["status"] = "Sold Out";
        } else {
            $info["status"] = "Available";
        }

        $info['options'] = array();
        foreach ($tickets as $ticket) {

            preg_match('(\$[0-9]*\,?[0-9]+\.?[0-9]+)',$ticket['price'],$match); // take only `$4,499.50` if input is `$4,499.50 each`
            $ticket['price'] = count($match) > 0 ? $match[0] : $ticket['price'];

            $ticket['clean_price'] = (int)preg_replace('{[^0-9]}', '', $ticket['price']);
            $ticket['clean_description'] = preg_match('{Minimum}i', $ticket['description']) ? 4 : 1;

            $desc = strip_tags($ticket["description"]);
            if($desc === '1 to 3 Tickets')
                $desc = '1-3 TICKETS';
            else if($desc === 'Minimum 4 Tickets')
                $desc = '4-7 TICKETS';
            else if($desc === 'Minimum 8 Tickets')
                $desc = '8 OR MORE';

            $ticket['description'] = $desc;

            if (strtolower($ticket["status"]) == "sold out") {
                $ticket["note" ] = '<div class="label">Sold Out</div>';
                # $ticket["price"] = "";
                $info["status"] = "Sold Out";
            }

            # remove br from hotline<br>
            $ticket["note"] = str_replace("hotline<br>", "hotline ", $ticket["note"]);

            # hack
            if ($id == 6)
            {
                switch ($option)
                {
                    case 16:
                        $ticket["note"] = <<<html
<div class="label">Limited Availability</div>
html;
                        break;
                }
            }

            $info['options'][] = $ticket;
        }

        $sql = <<<sql
SELECT related_photo_thumb,related_photo,caption
FROM ticketing_related_photo
WHERE ticketing_info_id = ?
order by sort
sql;
        $photos = $this->bind($sql, array($option));
        $info['related_photos'] = array();
        foreach ($photos as $photo) {
            if (empty($photo["related_photo_thumb"])) {
                # $photo["related_photo_thumb"] = str_replace(".jpg", "-sw70.jpg", $photo["related_photo"]);
                $photo["related_photo_thumb"] = $photo["related_photo"];
            }
            $info['related_photos'][] = $photo;
        }

        # another hack
        if ($id == 6 && ($option == 25 || $option == 26 || $option == 27))
        {
            $info["announcement"] = <<<html
Please book via the Corporate Sales Team (+65 6731 5900)
html;
        }

        # super early bird
        if ($mutant == 20)
        {
            $info["options"] = $this->early_bird($option);
            $pos = strpos($info["description"], '<h3');
            if ($pos)
            {
                $info["description"] = substr($info["description"], 0, $pos);
            }

            $info["category"] = str_replace(" (GROUP BOOKING SPECIAL)", "", $info["category"]);
        }

        # split 3 day combination
        if ($id == 4 && ($option == 12))
        {
            foreach ($info["options"] as $idx=>$row)
            {
                $info["options"][$idx]["description"] = "Buy your individual day tickets at the ticketing booth located at The Ciruit Park.";
            }
        }

        return $info;
    }

    public function ebcategory($id, $option)
    {
        $mutant = $id;
        if ($id == 20) {
            $id = 1;
        }

        if ($option == 33)
        {
            $id = 4;
        }

        $sql = <<<sql
SELECT
tc.ticketing_category AS category,
tc.ticketing_type AS type,
ti.ticket_subcategory AS "option",
ti.is_new AS is_new,
ti.ticket_subcategory_description AS description,
ti.access_to_zones,
ti.accessible_entertainment_stages,
ti.accessible_info,
ti.announcement,
ti.announcement_bottom,
ti.description,
ti.view_type,
ti.view_type_name,
ti.panoramic_view_path as panoramic_view_path,
ti.related_photo_path,
ti.seating_plan_path,
ti.seating_plan_thumb_path,
ti.zone_access_path as zone_access_path,
ti.zone_access_thumb_path as zone_access_thumb_path,
ti.zone_access_disclaimer
FROM ticketing_category AS tc
INNER JOIN ticketing_info ti ON (tc.id = ti.ticketing_category_id AND ti.id = ? AND ti.status = 'Active')
WHERE tc.id = ?
sql;

// ti.imagePath was removed in the SELECT query

        $rows = $this->bind($sql, array($option, ($mutant == 20 && ($option == 8 || $option == 2)) ? 2 : $id));

        if (empty($rows)) {
            return false;
        }

        $info = $rows[0];
        $info['accessible_entertainment_stages'] = explode(', ', $info['accessible_entertainment_stages']);
        array_walk($info['accessible_entertainment_stages'], 'trim');

        $info['mapPath'] = "http://singaporegp.sg/ticket/map_3d_2014/map_" . $option . ".jpg";

        $info['access_to_zones'] = explode(', ', $info['access_to_zones']);
        array_map('trim', $info['access_to_zones']);

        switch ($info['type']) {
            case '3-Day Ticket':
                $table = 'ticketing_three_day';
                $cols = '"" as location, "" as day, t.ticketing_note as note, trim(t.ticketing_price) AS price, ';
                $field = $option;
                break;

            case '3-Day Combination Ticket':
                $table = 'ticketing_three_day_combination';
                $cols = 't.location, t.day_word as day, t.ticketing_note as note, trim(t.ticketing_price) AS price, ';
                $field = $option;
                break;

            case 'Single Day Ticket':
                $table = 'ticketing_single_day';
                $cols = '"" as location, "" as day, t.ticketing_note as note, trim(t.ticketing_price) AS price, t.id as single_day_id, ';
                $field = $option;
                break;
        }

        //$setting = json_decode('{"ticketing":{"isEarlyBird":false}}',true); // json_decode(file_get_contents('https://dl.dropboxusercontent.com/u/45416284/AppSettings/sgpwebsite.json'),true); // null;//

        //$isEarlyBird = isset($setting['ticketing']['isEarlyBird']) ? $setting['ticketing']['isEarlyBird'] : false;

        //$phaseId = $isEarlyBird ? 4 : 5;

        $phaseId = 4;

        $sql = <<<sql
SELECT
tp.phase_description AS type,
tp.phase_date AS date,
{$cols}
trim(t.ticketing_price) AS price,
trim(t.ticketing_description) AS description,
t.ticketing_status AS status,
t.ticketing_link AS buy_url,
SUBSTRING_INDEX(SUBSTRING_INDEX(t.ticketing_link, '=', -2), '&', 1) AS sistic_code
FROM {$table} t
INNER JOIN ticketing_phase AS tp ON (tp.id = t.ticketing_phase_id AND tp.status=1)
WHERE t.ticketing_info_id = ? AND tp.id = $phaseId
ORDER BY IF(tp.phase_description LIKE 'REGULAR%', 1, 0), t.sort
sql;

        $tickets = $this->bind($sql, array($field));
        if (empty($tickets)) {
            $info["status"] = "Sold Out";
        } else {
            $info["status"] = "Available";
        }

        $info['options'] = array();
        foreach ($tickets as $ticket) {

            preg_match('(\$[0-9]*\,?[0-9]+\.?[0-9]+)',$ticket['price'],$match); // take only `$4,499.50` if input is `$4,499.50 each`
            $ticket['price'] = count($match) > 0 ? $match[0] : $ticket['price'];

            $ticket['clean_price'] = (int)preg_replace('{[^0-9]}', '', $ticket['price']);
            $ticket['clean_description'] = preg_match('{Minimum}i', $ticket['description']) ? 4 : 1;

            $desc = strip_tags($ticket["description"]);
            if($desc === '1 to 3 Tickets')
                $desc = '1-3 TICKETS';
            else if($desc === 'Minimum 4 Tickets')
                $desc = '4-7 TICKETS';
            else if($desc === 'Minimum 8 Tickets')
                $desc = '8 OR MORE';

            $ticket['description'] = $desc;

            if (strtolower($ticket["status"]) == "sold out") {
                $ticket["note" ] = '<div class="label">Sold Out</div>';
                # $ticket["price"] = "";
                $info["status"] = "Sold Out";
            }

            # remove br from hotline<br>
            $ticket["note"] = str_replace("hotline<br>", "hotline ", $ticket["note"]);

            # hack
            if ($id == 6)
            {
                switch ($option)
                {
                    case 16:
                        $ticket["note"] = <<<html
<div class="label">Limited Availability</div>
html;
                        break;
                }
            }

            $info['options'][] = $ticket;
        }

        $sql = <<<sql
SELECT related_photo_thumb,related_photo,caption
FROM ticketing_related_photo
WHERE ticketing_info_id = ?
order by sort
sql;
        $photos = $this->bind($sql, array($option));
        $info['related_photos'] = array();
        foreach ($photos as $photo) {
            if (empty($photo["related_photo_thumb"])) {
                # $photo["related_photo_thumb"] = str_replace(".jpg", "-sw70.jpg", $photo["related_photo"]);
                $photo["related_photo_thumb"] = $photo["related_photo"];
            }
            $info['related_photos'][] = $photo;
        }

        # another hack
        if ($id == 6 && ($option == 25 || $option == 26 || $option == 27))
        {
            $info["announcement"] = <<<html
Please book via the Corporate Sales Team (+65 6731 5900)
html;
        }


        # split 3 day combination
        if ($id == 4 && ($option == 12))
        {
            foreach ($info["options"] as $idx=>$row)
            {
                $info["options"][$idx]["description"] = "Buy your individual day tickets at the ticketing booth located at The Ciruit Park.";
            }
        }

        return $info;
    }

    public function get_singapore_region()
    {
        $sql = "select distinct region from ticketing_agent_singapore order by region";
        $res = $this->query($sql);
        $rtn = array();
        foreach ($res as $row)
        {
            $rtn[] = $row["region"];
        }
        return $rtn;
    }

    public function get_singapore_region_listing($region)
    {
        $sql = <<<sql
select *
from ticketing_agent_singapore
where region = ?
order by sort
sql;
        $rtn = array();
        $res = $this->bind($sql, array($region));
        if ($res)
        {
            foreach ($res as $row)
            {
                foreach ($row as $key => $val)
                {
                    $row[$key] = str_replace("\r\n", PHP_EOL, $val);
                }
                $rtn[] = $row;
            }
            return $rtn;
        }
        else
        {
            return array();
        }
    }

    public function get_international_country()
    {
        $sql = <<<sql
select region.id, region.region, country.id as country_id, country.country
from ticketing_agent_international as intl
inner join ticketing_agent_inter_region as region on (intl.region_id = region.id)
left join ticketing_agent_inter_country as country on (intl.country_id = country.id)
where intl.sort != 999
group by intl.region_id, intl.country_id
order by region.id, region.region, country.country
sql;
        $res = $this->query($sql);
        $rtn = array();
        foreach ($res as $row)
        {
            $id = $row["id"];
            $name = $row["region"];
            if ($row["id"] != 2 && !empty($row["country"]))
            {
                $id .= ':'.$row["country_id"];
                $name .= " | ".$row["country"];
            }

            $rtn[$id] = $name;
        }
        return $rtn;
    }

    public function get_international_country_listing($map, $country)
    {
        $country = urldecode($country);
        $idx = array_search($country, $map);
        $tmp = explode(":", $idx);

        $whr = array();
        $bind = array();
        if (isset($tmp[0]))
        {
            $whr["region"] = "intl.region_id = ?";
            $bind[] = $tmp[0];
        }

        if (isset($tmp[1]))
        {
            $whr["country"] = "intl.country_id = ?";
            $bind[] = $tmp[1];
        }

        $whrs = implode(" AND ", $whr);

        $sql = <<<sql
select intl.*, region.region, country.country
from ticketing_agent_international as intl
inner join ticketing_agent_inter_region as region on (intl.region_id = region.id)
left join ticketing_agent_inter_country as country on (intl.country_id = country.id)
where {$whrs} and intl.sort != 999
order by intl.sort
sql;

        $rtn = array();
        $res = $this->bind($sql, $bind);
        if ($res)
        {
            foreach ($res as $row)
            {
                foreach ($row as $key => $val)
                {
                    $row[$key] = str_replace("\r\n", PHP_EOL, $val);
                }
                $rtn[] = $row;
            }
            return $rtn;
        }
        else
        {
            return array();
        }
    }

    protected function early_bird($id)
    {
        $this->load->config("super-early-bird");
        $tickets = $this->config->item("ticket");
        $rtn = empty($tickets[$id]) ? array() : $tickets[$id];
        unset($rtn[0]["category"]);
        return $rtn;
    }
}