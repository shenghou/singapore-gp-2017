<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 * to intereactive with database
 */
class News_model extends Db_model
{
    public function __construct()
    {
		parent::__construct();
    }

	public function year($year = null)
	{
		$sql = <<<sql
SELECT press_release_year AS year, COUNT(*) AS count, IF(press_release_year = ?, 1, 0) AS selected
FROM {$this->ns['news']}
WHERE status=1
GROUP BY press_release_year
ORDER BY press_release_year DESC
sql;
		return $this->bind($sql, array($year));
	}

	public function listing($year)
	{
		$sql = <<<sql
SELECT id, press_release_date, press_release_year, title, photo
FROM {$this->ns['news']}
WHERE press_release_year = ? AND status=1
ORDER BY press_release_date DESC, id DESC
sql;
		return $this->bind($sql, array($year));
	}

	public function get($id)
	{
		$sql = <<<sql
SELECT id, press_release_date, press_release_year, title, subtitle, content, photo
FROM {$this->ns['news']}
WHERE id = ? AND status=1
sql;
		return $this->bind($sql, array($id));
	}
}

