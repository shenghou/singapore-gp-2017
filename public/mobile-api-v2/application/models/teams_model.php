<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 * to intereactive with database
 */
class Teams_Model extends Db_model
{
    public function __construct()
    {
		parent::__construct();
    }

	public function listing()
	{
		$sql = <<<sql
SELECT team.id, team.name, team.full_name, team.photo, team.mobile_tnd_photo, team.mobile_photo,
driver.id AS driver_id, driver.name AS driver_name, driver.team_photo as driver_photo
FROM team
LEFT JOIN driver ON (driver.team_id = team.id AND driver.status = 1)
WHERE team.status=1
ORDER BY team.seq, driver.lead_driver DESC
sql;

		$teams = $this->query($sql);
		if (empty($teams)) {
			return false;
		}

		$rtn = array();
		foreach ($teams as $team) {
			$id = $team['id'];
			$driver_photo = $team['driver_photo'];
			if (!isset($rtn[$id])) {
				unset($team['driver_photo']);
				$rtn[$id] = $team;
				$rtn[$id]['drivers'] = array();
				unset($rtn[$id]['driver_id'], $rtn[$id]['driver_name']);
			}
			$rtn[$id]['drivers'][] = array('id'=>$team['driver_id'], 'name'=>$team['driver_name'], 'photo'=>$driver_photo);
		}

		# remove index
		$return = array();
		foreach ($rtn as $row) {
			$return[] = $row;
		}
		return $return;
	}

	public function get($id)
	{
		$sql = <<<sql
SELECT team.id, team.name, team.full_name, team.photo, team.base, team.bio, team.power_unit, team.car, team.mobile_tnd_photo, team.mobile_photo,
driver.id AS driver_id, driver.name AS driver_name
FROM team
LEFT JOIN driver ON (driver.team_id = team.id AND driver.status = 1)
WHERE team.id = ? AND team.status = 1
sql;
		$teams = $this->bind($sql, array($id));
		if (empty($teams)) {
			return false;
		}

		$rtn = null;
		foreach ($teams as $team) {
			$id = $team['id'];
			if (empty($rtn)) {
				$rtn = $team;
				$rtn['drivers'] = array();
				unset($rtn['driver_id'], $rtn['driver_name']);
			}
			$rtn['drivers'][] = array('id'=>$team['driver_id'], 'name'=>$team['driver_name']);
		}

		return $rtn;
	}

	public function getDriver($id)
	{
		$sql = <<<sql
SELECT driver.name, driver.nationality, driver.lead_driver, driver.team_id, driver.team_photo AS photo1, driver.photo AS photo2, driver.dob, driver.debut, driver.history, driver.bio, driver.mobile_photo,
team.id AS team_id, team.name AS team_name
FROM driver
LEFT JOIN team ON (driver.team_id = team.id AND team.status = 1)
WHERE driver.id = ? AND driver.status = 1
sql;

		$rtn = $this->bind($sql, array($id));
        $result = $rtn[0];
        $result['bio'] = utf8_encode($result['bio']);
		return $result;
	}
}

