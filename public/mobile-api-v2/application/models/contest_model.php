<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Contest_model extends Db_model
{
    public function __construct()
    {
		parent::__construct();
    }

	public function listing()
	{
		$sql = <<<sql
select title, web_view_url, photo, replace(description, '&#174;', '®') as description
from contest
where web_view_status = 1
order by sort
sql;
		return $this->query($sql);
	}
}

