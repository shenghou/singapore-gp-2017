<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Raceguides_model extends Db_model
{
    public function __construct()
    {
		parent::__construct();
    }

    public function collection()
    {
        $hostname = 'http://'. $_SERVER['SERVER_NAME'];
        $sql = <<<sql
SELECT *
FROM raceguides
WHERE hidden != 1;
sql;

        $res = $this->query($sql);
//		$rtn = array();
//		foreach ($res as $row) {
//
//			$rtn[] = $row;
//		}
        return $res;
    }
}
