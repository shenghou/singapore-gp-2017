<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 * to intereactive with database
 */
class token_model extends Db_Model
{
    public function __construct()
    {
		parent::__construct();
    }

	public function register($token, $platform)
	{
		$args = get_defined_vars();
		$sql = 'insert into push_token set devicetoken = ?, platform = ?';
		$res = $this->bind($sql, $args);
		error_log(implode(":", $args).PHP_EOL, 3, APPPATH.'/logs/token.log');
		if ($res)
		{
			return array("register"=>true);
		}
		else
		{
			return array("register"=>false);
		}
	}

	public function verify($token, $platform)
	{
		$args = get_defined_vars();
		$sql = 'select 1 as verify from push_token where devicetoken = ? and platform = ?';
		$res = $this->bind($sql, $args);
		if (!empty($res[0]))
		{
			$res[0]["verify"] = (bool) $res[0]["verify"];
			return $res[0];
		}
		else
		{
			return array("verify"=>false);
		}
	}
}

