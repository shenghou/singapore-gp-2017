<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed.');
}

class Races_Model extends Db_model
{
    public function __construct()
    {
		parent::__construct();
    }

	public function calendar()
	{
		$sql = <<<sql
SELECT * 
FROM race_calendar
ORDER BY race_date
sql;

		$res = $this->query($sql);
		$rtn = array();
		foreach ($res as $row) {
//			$row['race'] = preg_replace('{( \(.*\))}', '', $row['race']);
            $dummy = $row;
            $dummy['race'] = utf8_encode($dummy['race']);
            $dummy['date'] = utf8_encode($dummy['date']);

			$rtn[] = $dummy;

		}
		return $rtn;
	}

	public function winner()
	{
		$sql = <<<sql
SELECT 
cal.race AS race_name,
cal.date AS race_date,
driver.name AS driver_name,
res.laps,
CONCAT(SUBSTR(res.result_time, 2),'.',res.result_microsec) AS time,
cal.country,
date_format(cal.race_date, '%d/%m/%Y') as actual_race_date
FROM race_calendar AS cal
LEFT JOIN race_calendar_result AS res ON cal.id = res.race_id AND res.result_note IS NULL
LEFT JOIN driver ON driver.id = res.driver_id
LEFT JOIN team ON team.id = driver.team_id
ORDER BY cal.race_date
sql;
		return $this->query($sql);
	}

	public function standing($id = 1)
	{
		$id = (int)$id;
		$sql = <<<sql
SELECT 
driver.name AS driver_name, team.name AS team_name,
res.position,
COALESCE(res.result_note, CONCAT(SUBSTR(res.result_time, 2),'.',res.result_microsec)) AS time,
res.laps
FROM race_calendar_result AS res
INNER JOIN driver ON driver.id = res.driver_id
INNER JOIN team ON team.id = driver.team_id
WHERE res.race_id = {$id}
ORDER BY res.result_time, res.result_microsec, res.position
sql;
		$res = $this->query($sql);
		$pos = 0;

		foreach ($res as &$row) {
			if ($row['position'] == 'RF') {
				$row['position'] = ++$pos;
			}
		}
		return $res;
	}

	public function teams_standing()
	{
		$sql = <<<sql
SELECT *, @pos:=COALESCE(@pos, 0) + 1 AS position
FROM teams_result
JOIN (SELECT @pos := 0) p
ORDER BY points DESC, teams_result.position, team
sql;
		return $this->query($sql);
	}

	public function drivers_standing()
	{
		$sql = <<<sql
SELECT *, @pos:=COALESCE(@pos, 0) + 1 AS position
FROM drivers_result
JOIN (SELECT @pos := 0) p
ORDER BY points DESC, drivers_result.position, driver, team
sql;
		return $this->query($sql);
	}

	public function latest_result()
	{
		$sql = <<<sql
SELECT 
0 as position, driver.name as driver_name, team.name as team_name,
coalesce(result_note, concat(rcs.result_time, '.', rcs.result_microsec)) as time, rc.race, rcs.laps,
rc.race_date
FROM race_calendar rc
INNER JOIN (SELECT MAX(race_id) as id FROM race_calendar_result) AS r ON rc.id = r.id
INNER JOIN race_calendar_result rcs ON rcs.race_id = r.id
INNER JOIN driver ON driver.id = rcs.driver_id
INNER JOIN team ON team.id = driver.team_id
ORDER BY if(rcs.result_note is null,0,1), rcs.result_time ASC, rcs.laps DESC
LIMIT 5
sql;

		$res = $this->query($sql);
		$rtn = array();
		$pos = 1;
		$race_date = null;
		foreach ($res as $row) {
			if (empty($race_date))
			{
				$race_date = $row['race_date'];
			}
			unset($row['race_date']);
			$row['position'] = $pos++;
			$rtn[] = $row;
		}
		return array($race_date, $rtn);
	}
}
