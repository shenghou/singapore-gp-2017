<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Happening_Model extends Db_model
{
	public function __construct()
	{
		parent::__construct();
	}


	public function ontrack()
	{
		$sql = <<<sql
select r.id, d.race_date as date, concat(r.time, ' - ', r.time_to) as time, concat(r.title, ' ', r.session) as title, r.session, r.shortdescription as excerpt,
r.thumbnail,
concat('<p>', r.shortdescription, '</p>') as content,
IFNULL(STR_TO_DATE(SUBSTRING_INDEX(SUBSTRING_INDEX(time, ' - ', 1), ' ', -1),"%k:%i:%s"),"") as start_time,
r.time_to as end_time,
r.id as detail_id
from happening h
inner join race_schedule r on (h.event_id = r.id)
inner join race_schedule_date d on (r.race_schedule_date_id = d.id)
where h.event_type = '2'
order by r.time desc
sql;
		return $this->query($sql);
	}

	public function offtrack()
	{
		$sql = <<<sql
select d.id, e.id as 'entertainment_id', e.artist, e.additional_label,
e.image as thumbnail,
e.image as image,
str_to_date(concat(d.start_date, ' 2015'), '%Y-%c-%d') as date, d.time, zone, z.id as zone_id,
IFNULL(STR_TO_DATE(SUBSTRING_INDEX(d.time, ' - ', 1),"%H:%i:%s"),"") as start_time,
IFNULL(STR_TO_DATE(SUBSTRING_INDEX(d.time, ' - ', -1),"%H:%i:%s"),"") as end_time,
d.id as detail_id, d.start_date, d.end_date,
performance_category,
e.sort, e.url
from happening h
inner join entertainment_highlights_detail d on (h.event_id = d.id)
inner join entertainment_highlights e on (d.entertainment_id = e.id)
left join zone_writeup z on (d.zone rlike z.zone_title)
where e.year = 'Current' and e.status = 'Active' and event_type =  '1'
order by date, if(d.time = 'TBA', 0, if(substring_index(d.time, '-', 1) = '12:00am ', '23:59pm', str_to_date(SUBSTRING_INDEX(d.time, '-', 1), '%l:%i%p'))),
if(e.performance_category like "%Performances on Stage%", 1,
	if (e.performance_category like "%Roving Performances%", 2, 3)
), e.sort
sql;

		return $this->query($sql);
	}
}
