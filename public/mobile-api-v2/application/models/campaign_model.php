<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed.');
}

class Campaign_Model extends Db_model
{
	//Database constant value
	const contest_submission_normal = 0;
	const contest_submission_bonus_entry = 1;
	const contest_submission_status_fail = 0;
	const contest_submission_status_success = 1;
	const contest_type_webview = 1;
	const contest_type_photo = 2;
	const contest_type_game = 3;
	const contest_status_expired = "E";
	const contest_status_active = "A";
	const contest_status_upcoming = "U";

	public function __construct()
	{
		parent::__construct();
	}

	public function contest_month_current()
	{
		$sql = <<<sql
SELECT mon.month AS current_contest
FROM campaign_current cam
LEFT JOIN monthly_contest mon ON cam.type_id = mon.id
WHERE cam.type =  "Monthly Contest"
sql;
		return $this->query($sql);
	}

	public function grand_draw_current()
	{
		$sql = <<<sql
SELECT type, type_id, status, COALESCE(grand.month,0) AS current_grand_draw
FROM campaign_current cam
LEFT JOIN grand_draw grand ON cam.type_id = grand.id
WHERE cam.type =  "Grand Draw"
sql;
		return $this->query($sql);
	}

	public function contest($year)
	{
		$sql = <<<sql
SELECT title, descp AS 'desc', detail, teaser, status, month, date_start AS start_date, date_end AS end_date, type AS contest_type,
contest_url, contest_min_photo AS 'contest_min_photo_required', contest_total_photo AS 'contest_total_photo_required', contest_photo_caption_required, prize, prize_image_url, prize_disclaimer, share_title, share_descp AS 'share_desc',  share_url, share_title AS 'fb_title', share_url AS 'fb_url'
FROM monthly_contest
WHERE year = {$year}
ORDER BY year, month
sql;
		return $this->query($sql);
	}

	public function grandDraw($year)
	{
		$sql = <<<sql
SELECT title, descp AS 'desc', month, date_start as start_date, date_end as end_date, thumbnail_url, image_url, share_title, share_descp AS 'share_desc', share_url
FROM grand_draw
WHERE year = {$year}
ORDER BY year, month
sql;
		return $this->query($sql);
	}

	public function contestDetail($year, $month)
	{
		$sql = <<<sql
SELECT id, title, descp AS 'desc', detail, teaser, status, month, date_start AS start_date, date_end AS end_date, type AS contest_type, contest_url,
prize, prize_image_url, prize_disclaimer, share_title, share_descp AS 'share_desc', share_url, share_title AS 'fb_title', share_url AS 'fb_url'
FROM monthly_contest
WHERE year = {$year} AND month = {$month}
ORDER BY year, month
sql;
		return $this->query($sql);
	}


	public function grandDrawDetail($year, $month)
	{
		$sql = <<<sql
SELECT title, descp AS 'desc', month, date_start as start_date, date_end as end_date, thumbnail_url, image_url, share_title, share_descp AS 'share_desc', share_url
FROM grand_draw
WHERE year = {$year} AND month = {$month}
ORDER BY year, month
sql;
		return $this->query($sql);
	}

	public function contestSubmited($email, $monthly_contest_id)
	{
		$sql = <<<sql
SELECT name_first, name_last, email, contact, nationality, country, status
FROM monthly_contest_submission
WHERE email = '{$email}' AND monthly_contest_id	 = {$monthly_contest_id}
ORDER BY created_at DESC
sql;
		return $this->query($sql);
	}

	public function submitContest($data)
	{
		//set timezone to singapore
		date_default_timezone_set('Asia/Singapore');

		/*
		 * Contest Index
		 */
		$contest = array(
			'monthly_contest_id' => $data['monthly_contest_id'],
			'name_first' => $data['name_first'],
			'name_last' => $data['name_last'],
			'email' => $data['email'],
			'contact' => $data['contact'],
			'nationality' => empty($data['nationality']) ? "" : $data['nationality'],
			'country' => $data['country'],
			'type' => Campaign_Model::contest_submission_normal,
			'status' => empty($data['status']) ? 0 : $data['status'],
			'created_at' => date('Y-m-d H:i:s')
		);

		//insert into monthly_contest_submission
		$this->db->insert('monthly_contest_submission', $contest);
		$monthly_contest_submission_id = $this->db->insert_id();

		/*
		 * Contest Detail
		 */
		$contest_detail = $data['contest_data_list'];
		$length = count($contest_detail);

		for ($i = 0; $i < $length; $i++) {
			$contest_detail[$i]['monthly_contest_submission_id'] = $monthly_contest_submission_id;
			$contest_detail[$i]['position'] = $i + 1;
			$contest_detail[$i]['created_at'] = date('Y-m-d H:i:s');
		}

		//insert into monthly_contest_submission
		$this->db->insert_batch('monthly_contest_submission_detail', $contest_detail);

		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function submitBonusEntryContest($data)
	{
		//set timezone to singapore
		date_default_timezone_set('Asia/Singapore');

		/*
		 * Contest Index
		 */
		$contest = array(
			'monthly_contest_id' => $data['monthly_contest_id'],
//			'name_first' => $data['name_first'],
//			'name_last' => $data['name_last'],
			'email' => $data['email'],
//			'contact' => $data['contact'],
//			'nationality' => empty($data['nationality']) ? "" : $data['nationality'],
//			'country' => $data['country'],
			'type' => Campaign_Model::contest_submission_bonus_entry,
			'status' => empty($data['status']) ? 0 : $data['status'],
			'created_at' => date('Y-m-d H:i:s')
		);

		//insert into monthly_contest_submission
		$this->db->insert('monthly_contest_submission', $contest);
	}

	public function submitGrandDraw($data)
	{
		//set timezone to singapore
		date_default_timezone_set('Asia/Singapore');

		/*
		 * Grand Draw Index
		 */
		$grand_draw = array(
			'name_first' => $data['name_first'],
			'name_last' => $data['name_last'],
			'email' => $data['email'],
			'contact' => $data['contact'],
			'country' => $data['country'],
			'created_at' => date('Y-m-d H:i:s')
		);

		//insert into grand_draw_submission
		$this->db->insert('grand_draw_submission', $grand_draw);
		$grand_draw_submission_id = $this->db->insert_id();

		/*
		 * Grand Draw Detail
		 */
		$grand_draw_detail = $data['grand_draw_list'];
		$length = count($grand_draw_detail);

		for ($i = 0; $i < $length; $i++) {
			$grand_draw_detail[$i]['grand_draw_submission_id'] = $grand_draw_submission_id;
			$grand_draw_detail[$i]['position'] = $grand_draw_detail[$i]['month'];
			$grand_draw_detail[$i]['created_at'] = date('Y-m-d H:i:s');
			unset($grand_draw_detail[$i]['month']);
		}

		//insert into monthly_contest_submission
		$this->db->insert_batch('grand_draw_submission_detail', $grand_draw_detail);

		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
}