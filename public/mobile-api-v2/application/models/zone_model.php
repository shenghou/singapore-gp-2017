<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 * to intereactive with database
 */
class Zone_model extends Db_model
{
    public function __construct()
    {
		parent::__construct();
    }

	public function listing()
	{
		$sql = <<<sql
SELECT zone_title as name, zone_short_writeup as short_writeup, zone_writeup as writeup
FROM zone_writeup
sql;
		return $this->query($sql);
	}
}

