<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Schedule_Model extends Db_model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function calendar($date = null, $zone = null)
	{

		// paing : this need to confirm, ontrack was required
		$ontrack = $this->get_ontrack($date, $zone);

//        $ids = "171,130,124,127,128,129"; // remove this to get all results
//        $offtrack = $this->get_offtrack($date, $zone, $ids);
		$offtrack = $this->get_offtrack($date, $zone);

		$rtn = array();
		foreach ($ontrack as  $row) {
			$id = $row['date'] . $row['time'];
			$row['type'] = 'ontrack';
			$rtn[$id] = $row;
		}


		$cnt = 0;
		foreach ($offtrack as $row) {
			$id = $row['date'] . ($row['time'] == 'TBA' ? 0 : $row['time']) . '.' . $cnt;
			$row['type'] = 'offtrack';
			$rtn[$id] = $row;
			$cnt++;
		}
		ksort($rtn);
		return array_values($rtn);
	}

	public function ontrack($id)
	{
		$id = (int) $id;
//select r.id, d.race_date as date, concat(r.time, ' - ', r.time_to) as time, r.title, r.session, r.shortdescription as excerpt,
		$sql = <<<sql
select r.id, d.race_date as date, concat(date_format( r.time, '%H:%i' ) , ' – ', date_format( r.time_to, '%H:%i' )) as time, concat(r.title, ' ', r.session) as title, r.session, r.shortdescription as excerpt,
-- replace(r.thumbnail, '.jpg', '-sw130.jpg') as thumbnail,
r.thumbnail,
concat('<p>', r.shortdescription, '</p>') as content,
IFNULL(STR_TO_DATE(SUBSTRING_INDEX(SUBSTRING_INDEX(date_format( time, '%H:%i' ), ' - ', 1), ' ', -1),"%H:%i"),"") as start_time
from race_schedule r
inner join race_schedule_date d on (d.id = r.race_schedule_date_id)
where r.id = ?
sql;
		$res = $this->bind($sql, array($id));
		if (empty($res[0])) {
			return false;
		} else {
			return $res[0];
		}
	}

	public function offtrack($id)
	{
		$id = (int) $id;

		$sql = <<<sql
select e.id, d.id as detail_id , e.artist, e.additional_label,
-- e.image_thumb as thumbnail,
e.image as thumbnail,
str_to_date(concat(d.date, ' 2016'), '%a %d %b %Y') as date,
d.time,
d.estimated_time as start_time,
d.zone as zone, z.id as zone_id, e.performance_description,
e.sort
from entertainment_highlights_detail d
inner join entertainment_highlights e on (e.id = d.entertainment_id)
left join zone_writeup z on (d.zone rlike z.zone_title)
where d.id = ? and e.year = 'Current' and e.status = 'Active'
order by e.sort
sql;

		$res = $this->bind($sql, array($id));
		if (empty($res[0])) {
			return false;
		} else {
			$res[0]["detail_id"] = intval($res[0]["detail_id"]);
			$res[0]["artist"] = str_replace(array("\U","\u"), "", strip_tags($res[0]["artist"]));
			$res[0]["zone"] = str_replace(array(" - "), array(" – "), strip_tags($res[0]["zone"]));
			$res[0]["additional_label"] = strip_tags($res[0]["additional_label"]);
			$res[0]['disclaimer'] = '* Artiste line-up and set times are subject to change and are dependent on final FIA track schedule. Please check our website and mobile app regularly for updates to the daily schedule.';

			# reformat time
			if (strtoupper($res[0]["time"]) != "TBA")
			{
				/*$tmp = explode(" - ", $res[0]["time"]);
				if (isset($tmp[1]))
				{
					$t1 = date("H:i:s", strtotime($tmp[0]));
					$t2 = date("H:i:s", strtotime($tmp[1]));
					$res[0]["time"] = sprintf("%s - %s", $t1, $t2);
				}
				else
				{
					$res[0]["time"] = "TBA";
				}*/
				$htmls = array("<br />","<br>","<br/>", "<b>", "</b>", "</ b>");
				$res[0]["time"] = str_ireplace($htmls, "", $res[0]["time"]);
				$res[0]["time"] = str_replace("&ndash;", "-", $res[0]["time"]);
			}
			return $res[0];
		}
	}

	public function offtrackDetail($id)
	{
		$id = (int) $id;

		$sql = <<<sql
select e.id, d.id as detail_id , e.artist, e.additional_label,
"offtrack" as "type",
-- e.image_thumb as thumbnail,
e.image as thumbnail,
str_to_date(concat(d.date, ' 2016'), '%a %d %b %Y') as date,
d.time,
d.estimated_time as start_time,
d.zone as zone, z.id as zone_id, e.short_description as performance_description,
e.performance_category, e.sort
from entertainment_highlights_detail d
left join entertainment_highlights e on (e.id = d.entertainment_id)
left join zone_writeup z on (d.zone rlike z.zone_title)
where e.id = ? and e.year = 'Current' and e.status = 'Active'
ORDER BY e.performance_category, e.sort, d.date
sql;

		$res = $this->bind($sql, array($id));
		if (empty($res[0])) {
			return false;
		} else {
			$es = array();
			$esArray = array();

			foreach($res as $row){
				$row["detail_id"] = intval($row["detail_id"]);
				$row["artist"] = str_replace(array("\U","\u"), "", strip_tags($row["artist"]));
				$row["zone"] = str_replace(array(" - "), array(" – "), strip_tags($row["zone"]));
				$row["additional_label"] = strip_tags($row["additional_label"]);
				$row['disclaimer'] = '* Artiste line-up and set times are subject to change and are dependent on final FIA track schedule. Please check our website and mobile app regularly for updates to the daily schedule.';

				# reformat time
				if (strtoupper($row["time"]) != "TBA")
				{
					/*$tmp = explode(" - ", $row["time"]);
                    if (isset($tmp[1]))
                    {
                        $t1 = date("H:i:s", strtotime($tmp[0]));
                        $t2 = date("H:i:s", strtotime($tmp[1]));
                        $row["time"] = sprintf("%s - %s", $t1, $t2);
                    }
                    else
                    {
                        $row["time"] = "TBA";
                    }*/
					$htmls = array("<br />","<br>","<br/>", "<b>", "</b>", "</ b>");
					$row["time"] = str_ireplace($htmls, "", $row["time"]);
					$row["time"] = str_replace("&ndash;", "-", $row["time"]);
				}

//                $obj["detail_id"] = $row["detail_id"];
//                $obj["date"] = $row["date"];
//                $obj["time"] = $row["time"];
//                $obj["zone"] = $row["zone"];
//                $obj["start_time"] = $row["start_time"];
//                $obj["zone_id"] = $row["zone_id"];
				$obj = $row;

				array_push($esArray,$obj);
				array_push($es, $row);
			}

			$finalRes = $es[0];
			$finalRes['entertainment_schedule'] = $esArray;

			return $finalRes ;
		}
	}


	public function get_ontrack($date = null, $zone = null)
	{
		$whr = null;
		if ($date && $date != "all") {
			$whr = sprintf("WHERE d.race_date = %s", $this->escape($date));
		}

		$sql = <<<sql
select "ontrack" as type, r.id, d.race_date as date, concat(date_format( r.time, '%H:%i' ), ' - ', date_format( r.time_to, '%H:%i' )) as time,
IFNULL(date_format( r.time, '%H:%i' ),"") as start_time,
concat(r.title, ' ', r.session) as title,
r.session, r.shortdescription as excerpt,
-- replace(r.thumbnail, '.jpg', '-sw130.jpg') as thumbnail
r.thumbnail,
			CONCAT ('', '') as artist,
			CONCAT ('', '') as additional_label,
			CONCAT ('', '') as image,
			CONCAT ('', '') as 'zone',
			CONCAT ('', '') as zone_id,
			CONCAT ('', '') as end_time,
			CONCAT ('', r.id) as detail_id,
-- 			CONCAT ('', '') as start_date,
-- 			CONCAT ('', '') as end_date,
-- 			CONCAT ('', '') as performance_category
			CONCAT (0) as sort,
			CONCAT ('', '') as url
from race_schedule r
inner join race_schedule_date d on (d.id = r.race_schedule_date_id)
{$whr}
order by d.race_date, r.time, r.time_to;
sql;

		return $this->query($sql);
	}

	public function get_offtrack($date = null, $zone = null, $limitedArtist = null)
	{

		$limitedArtist = $limitedArtist !== null ? "and e.id IN ($limitedArtist)": '';

		$whr = array();
		if ($date && $date != "all") {
			// ang : start_date is datetime, cast to date so the comparison can work
			$whr["date"] = sprintf("date(d.start_date) = '%s'", strtoupper(date("Y-m-d", strtotime($date))));
		}

		if ($zone && $zone != "all") {
			$whr["zone"] = sprintf("z.id = %s", (int)$zone);
		}

		$whrs = empty($whr) ? null : (" AND ".implode(" AND ", $whr));

		$sql = <<<sql
select "offtrack" as type, e.id, e.artist, e.additional_label,
e.image as thumbnail,
e.image as image,
coalesce(str_to_date(concat(d.start_date, ' 2016'), '%Y-%m-%d'),"") as date, coalesce(d.time,"") as time, coalesce(zone,"") as zone, coalesce(z.id,"") as zone_id,
-- IFNULL(STR_TO_DATE(SUBSTRING_INDEX(SUBSTRING_INDEX(time, ' - ', 1), ' ', -1),"%l.%i%p"),"") as start_time,
d.estimated_time as start_time,
IFNULL(STR_TO_DATE(SUBSTRING_INDEX(d.time, ' - ', -1),"%H:%i:%s"),"") as end_time,
coalesce(d.id,"") as detail_id, coalesce(d.start_date,'') as start_date, coalesce(d.end_date,"") as end_date,
coalesce(performance_category,"") as performance_category,
coalesce(e.sort,"") as sort, coalesce(e.url,"") as url
from entertainment_highlights e
left join entertainment_highlights_detail d on (e.id = d.entertainment_id)
left join zone_writeup z on (d.zone rlike z.zone_title)
where e.year = 'Current' and e.status = 'Active' {$whrs}
{$limitedArtist}
order by date, if(d.time = 'TBA', 0, if(substring_index(d.time, '-', 1) = '12:00am ', '23:59pm', str_to_date(SUBSTRING_INDEX(d.time, '-', 1), '%l:%i%p'))),
if(e.performance_category like "%Performances on Stage%", 1,
	if (e.performance_category like "%Roving Performances%", 2, 3)
), e.sort
sql;

		$res = $this->query($sql);
		$rtn = array();
		foreach ($res as $idx=>$val) {
			$val["detail_id"] = intval($val["detail_id"]);
			$val["artist"] = str_replace(array("\U","\u"), "", strip_tags($val["artist"]));
			$val["additional_label"] = strip_tags($val["additional_label"]);
			$val["zone"] = str_replace("–", "-", $val["zone"]);
			$val["zone_id"] = empty($val["zone_id"]) ? "" : $val["zone_id"];
//            $val["start_time"] = empty($val["start_time"]) ? "" : $val["start_time"];
			# reformat time
			if (strtoupper($val["time"]) != "TBA")
			{
				/*$tmp = explode(" - ", $val["time"]);
				if (isset($tmp[1]))
				{
					$t1 = date("H:i:s", strtotime($tmp[0]));
					$t2 = date("H:i:s", strtotime($tmp[1]));
					$val["time"] = sprintf("%s - %s", $t1, $t2);
				}
				else
				{
					$val["time"] = "TBA";
				}*/
				$htmls = array("<br />","<br>","<br/>", "<b>", "</b>", "</ b>");
				$val["time"] = str_ireplace($htmls, "", $val["time"]);
				$val["time"] = str_replace("&ndash;", "-", $val["time"]);
			}

			$rtn[$idx] = $val;
		}
		return $rtn;
	}

	public function get_highlights()
	{
		$sql = <<<sql
select id, replace(image_path, 'http://173.230.156.228/sgpstaging', 'http://www.singaporegp.sg') as banner, type, webview_url, title, display_website
from singaporef1_highlights
where status = 1
order by sort
sql;
		return $this->query($sql);
	}
}

