<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 * to intereactive with database
 */
class Album_model extends Db_model
{
	protected $cms_path = 'http://web2.singaporegp.sg/sgp_admin/Uploaded';

    public function __construct()
    {
		parent::__construct();
    }

	public function year($year = null)
	{
		$sql = <<<sql
SELECT year, COUNT(*) AS album, IF(year = ?, 1, 0) AS selected
FROM {$this->ns['album']}
GROUP BY year
ORDER BY year DESC
sql;
		return $this->bind($sql, array($year));
	}

	public function listing($year)
	{
		$sql = <<<sql
SELECT album.id, album.AlbumDate AS date, album.Date_word AS name, 
CONCAT('{$this->cms_path}/', cover.image_path_large) AS photo,
CONCAT('{$this->cms_path}/', cover.image_path_thumb) AS thumbnail
FROM {$this->ns['album']} album
LEFT JOIN {$this->ns['gallery']} cover ON album.id = cover.photo_album_id AND cover.PhotoAlbumCover = 1
WHERE album.year = ?
ORDER BY album.AlbumDate DESC
sql;

		return $this->bind($sql, array($year));
	}

	public function first_photo($ids, &$albums)
	{
		if (empty($ids)) {
			return false;
		}

		$sqls = array();
		foreach ($ids as $id) {
			$sqls[] = <<<sql
(SELECT cover.photo_album_id AS album_id,
CONCAT('{$this->cms_path}/', cover.image_path_large) AS photo,
CONCAT('{$this->cms_path}/', cover.image_path_thumb) AS thumbnail
FROM {$this->ns['gallery']} cover
WHERE cover.photo_album_id = ?
ORDER BY sort, image_path_thumb
LIMIT 1)
sql;
		}

		$sql = implode(' UNION ', $sqls);
		$result = $this->bind($sql, $ids);
		if (empty($result)) {
			return false;
		}

		$rtn = array();
		foreach ($result as $row) {
			$id = $row['album_id'];
			$idx = array_search($id, $ids);
			unset($row['album_id']);
			$albums[$idx] = array_merge($albums[$idx], $row);
		}
	}

	public function get($id, $categories, $category = null, $start = 0, $limit = 10)
	{
		$rtn = array(0=>0, 1=>array(), 2=>array(), 3=>null);
		$cats = array();
		$bind = array($id);

		$filter = null;
		if (!empty($category)) {
			$filter = ' AND gallery.CategoryID = ?';
			$bind[] = $category;
		}

		# album name
		$sql = 'select title as name from photo_album where id = ?';
		$res = $this->bind($sql, $bind);
		$rtn[3] = $res[0]['name'];

		# category
		$sql = <<<sql
SELECT CategoryID AS id, COUNT(*) AS count
FROM {$this->ns['gallery']} gallery
WHERE gallery.photo_album_id = ?
GROUP BY CategoryID WITH ROLLUP
sql;

		$result = $this->bind($sql, $bind);
		if (!empty($result)) {
			$rtn[1] = array();

			$count = array();
			$tmp = array_pop($result);
			$rtn[0] = $tmp['count'];

			foreach ($result as $row) {
				$id = $row['id'];
				$count[$id] = $row['count'];
			}

			foreach ($categories as $cat) {
				if (isset($count[$cat['id']])) {
					$cat['count'] = $count[$cat['id']];
				} else {
					$cat['count'] = 0;
				}
				$cat['selected'] = ($category == $cat['id']);
				$rtn[1][] = $cat;
				$cats[$cat['id']] = $cat['name'];
			}
		}

		# actual listing
		$sql = <<<sql
SELECT
CategoryID AS category,
CONCAT('{$this->cms_path}/', gallery.image_path_large) AS photo,
CONCAT('{$this->cms_path}/', gallery.image_path_thumb) AS thumbnail
FROM {$this->ns['gallery']} gallery
WHERE gallery.photo_album_id = ?
{$filter}
ORDER BY sort, image_path_thumb
LIMIT {$start}, {$limit}
sql;
		$result = $this->bind($sql, $bind);
		if (!empty($result)) {
			foreach ($result as $row) {
				$cat_id = $row['category'];
				unset($row['category']);
				if (isset($cats[$cat_id])) {
					$row['category'] = $cats[$cat_id];
				} else {
					$row["category"] = "";
				}
				$rtn[2][] = $row;
			}
		}
		return $rtn;
	}
}

