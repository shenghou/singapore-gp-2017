<?php
header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');
header("HTTP/1.1 404 Not Found");
echo json_encode(
	array(
		'status' => FALSE,
		'error' => 'Unknown resource',
	)
);

