<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Cache extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
        $base_url = "http://".$_SERVER['SERVER_NAME']."/mobile-api-v2";

		$latest_news = "$base_url/index.php/news/index/year/2014";
		$latest_news_md5 = md5($latest_news);

		$latest_offtrack = "$base_url/index.php/schedule/type/offtrack/date/all/zone/all";
		$latest_offtrack_md5 = md5($latest_offtrack);

		$latest_ontrack = "$base_url/index.php/schedule/type/ontrack/date/all/zone/all";
		$latest_ontrack_md5 = md5($latest_ontrack);

		$urls = array(
			"$base_url/index.php/zone/access",
			"$base_url/index.php/races/calendar",
			"$base_url/index.php/races/teams/standing",
			"$base_url/index.php/races/drivers/standing",
			"$base_url/index.php/races/standing/id/1",
			"$base_url/index.php/races/standing/id/2",
			"$base_url/index.php/races/standing/id/3",
			"$base_url/index.php/races/standing/id/4",
			"$base_url/index.php/races/standing/id/5",
			"$base_url/index.php/races/standing/id/6",
			"$base_url/index.php/races/standing/id/7",
			"$base_url/index.php/races/standing/id/9",
			"$base_url/index.php/races/standing/id/10",
			"$base_url/index.php/races/standing/id/12",
			"$base_url/index.php/races/standing/id/13",
			"$base_url/index.php/races/standing/id/14",
			"$base_url/index.php/races/standing/id/15",
			"$base_url/index.php/races/standing/id/16",
			"$base_url/index.php/races/standing/id/17",
			"$base_url/index.php/races/standing/id/18",
			"$base_url/index.php/races/standing/id/19",
			"$base_url/index.php/races/standing/id/20",
			"$base_url/index.php/races/standing/id/21",
			"$base_url/index.php/races/latest/result",
			"$base_url/index.php/races/winner",

			"$base_url/index.php/schedule/type/overview/date/all/zone/all",
			"$base_url/index.php/schedule/type/overview/date/all/zone/1",
			"$base_url/index.php/schedule/type/overview/date/all/zone/2",
			"$base_url/index.php/schedule/type/overview/date/all/zone/3",
			"$base_url/index.php/schedule/type/overview/date/all/zone/4",
			"$base_url/index.php/schedule/type/overview/date/2013-09-20/zone/all",
			"$base_url/index.php/schedule/type/overview/date/2013-09-20/zone/1",
			"$base_url/index.php/schedule/type/overview/date/2013-09-20/zone/2",
			"$base_url/index.php/schedule/type/overview/date/2013-09-20/zone/3",
			"$base_url/index.php/schedule/type/overview/date/2013-09-20/zone/4",
			"$base_url/index.php/schedule/type/overview/date/2013-09-21/zone/all",
			"$base_url/index.php/schedule/type/overview/date/2013-09-21/zone/1",
			"$base_url/index.php/schedule/type/overview/date/2013-09-21/zone/2",
			"$base_url/index.php/schedule/type/overview/date/2013-09-21/zone/3",
			"$base_url/index.php/schedule/type/overview/date/2013-09-21/zone/4",
			"$base_url/index.php/schedule/type/overview/date/2013-09-22/zone/all",
			"$base_url/index.php/schedule/type/overview/date/2013-09-22/zone/1",
			"$base_url/index.php/schedule/type/overview/date/2013-09-22/zone/2",
			"$base_url/index.php/schedule/type/overview/date/2013-09-22/zone/3",
			"$base_url/index.php/schedule/type/overview/date/2013-09-22/zone/4",
			"$base_url/index.php/schedule/type/overview/date/2013-09-23/zone/all",
			"$base_url/index.php/schedule/type/overview/date/2013-09-23/zone/1",
			"$base_url/index.php/schedule/type/overview/date/2013-09-23/zone/2",
			"$base_url/index.php/schedule/type/overview/date/2013-09-23/zone/3",
			"$base_url/index.php/schedule/type/overview/date/2013-09-23/zone/4",

			$latest_offtrack,
			$latest_ontrack,
			"$base_url/index.php/schedule/type/offtrack/date/all/zone/1",
			"$base_url/index.php/schedule/type/offtrack/date/all/zone/2",
			"$base_url/index.php/schedule/type/offtrack/date/all/zone/3",
			"$base_url/index.php/schedule/type/offtrack/date/all/zone/4",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-20/zone/all",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-20/zone/1",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-20/zone/2",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-20/zone/3",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-20/zone/4",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-21/zone/all",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-21/zone/1",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-21/zone/2",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-21/zone/3",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-21/zone/4",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-22/zone/all",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-22/zone/1",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-22/zone/2",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-22/zone/3",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-23/zone/all",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-23/zone/1",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-23/zone/2",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-23/zone/3",
			"$base_url/index.php/schedule/type/offtrack/date/2013-09-23/zone/4",

			"$base_url/index.php/schedule/type/ontrack/date/all/zone/all",
			"$base_url/index.php/schedule/type/ontrack/date/all/zone/1",
			"$base_url/index.php/schedule/type/ontrack/date/all/zone/2",
			"$base_url/index.php/schedule/type/ontrack/date/all/zone/3",
			"$base_url/index.php/schedule/type/ontrack/date/all/zone/4",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-20/zone/all",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-20/zone/1",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-20/zone/2",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-20/zone/3",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-20/zone/4",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-21/zone/all",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-21/zone/1",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-21/zone/2",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-21/zone/3",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-21/zone/4",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-22/zone/all",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-22/zone/1",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-22/zone/2",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-22/zone/3",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-23/zone/1",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-23/zone/2",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-23/zone/3",
			"$base_url/index.php/schedule/type/ontrack/date/2013-09-23/zone/4",

			"$base_url/index.php/schedule/type/offtrack",
			"$base_url/index.php/schedule/type/ontrack",
			"$base_url/index.php/ticketing/agent/singapore",
			"$base_url/index.php/ticketing/agent/singapore/region/central",
			"$base_url/index.php/ticketing/agent/singapore/region/east",
			"$base_url/index.php/ticketing/agent/singapore/region/north",
			"$base_url/index.php/ticketing/agent/singapore/region/west",
			$latest_news
		);

		$res = $this->mget($urls);

		# looping for latest_news
		$obj = $res[$latest_news_md5];
		$news_urls = array();
		foreach ($obj->results->listing as $row)
		{
			$url = sprintf("$base_url/index.php/news/id/%s", $row->id);
			$news_urls[] = $url;
		}
		$ress = $this->mget($news_urls);

		#looping for off-track
		$obj = $res[$latest_offtrack_md5];
		$offtrack_urls = array();
		foreach ($obj->results->listing as $row)
		{
			$url = sprintf("$base_url/index.php/schedule/offtrack/id/%s", $row->id);
			$offtrack_urls[] = $url;
		}
		$offtrack_res = $this->mget($offtrack_urls);

		#looping for on-track
		$obj = $res[$latest_ontrack_md5];
		$ontrack_urls = array();
		foreach ($obj->results->listing as $row)
		{
			$url = sprintf("$base_url/index.php/schedule/ontrack/id/%s", $row->id);
			$ontrack_urls[] = $url;
		}
		$ontrack_res = $this->mget($ontrack_urls);
        $ontrack_res = $ontrack_res ? $ontrack_res : array();

		$rtn = array_merge($res, $ress, $offtrack_res, $ontrack_res);
		json_response($rtn);
	}

	public function mget ($urls)
	{
		$ch = array();
		foreach ($urls as $url)
		{
			$ch[$url] = curl_init();
			curl_setopt($ch[$url], CURLOPT_URL, $url);
			curl_setopt($ch[$url], CURLOPT_RETURNTRANSFER, true);
			if (!isset($mh))
			{
				$mh = curl_multi_init();
			}
			curl_multi_add_handle($mh, $ch[$url]);
		}

		if (empty($mh))
		{
			return false;
		}

		$active = null;
		do
		{
			$mrc = curl_multi_exec($mh, $active);
		} while ($mrc == CURLM_CALL_MULTI_PERFORM);

		while ($active && $mrc == CURLM_OK)
		{
			if (curl_multi_select($mh) != -1)
			{
				do
				{
					$mrc = curl_multi_exec($mh, $active);
				} while ($mrc == CURLM_CALL_MULTI_PERFORM);
			}
		}

		$rtn = array();
		foreach ($ch as $url=>$connect)
		{
			$key = md5($url);
			$rtn[$key] = json_decode(curl_multi_getcontent($ch[$url]));
			curl_multi_remove_handle($mh, $ch[$url]);
		}
		curl_multi_close($mh);
		return $rtn;
	}
}

