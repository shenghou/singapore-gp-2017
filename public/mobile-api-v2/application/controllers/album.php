<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Album extends CI_Controller
{
	protected $categories = array(
		array('id' => 3, 'name' => 'Circuit Park'),
		array('id' => 2, 'name' => 'Drivers'),
		array('id' => 5, 'name' => 'Entertainment'),
		array('id' => 4, 'name' => 'Pre-Race'),
		array('id' => 1, 'name' => 'Race'),
		array('id' => 6, 'name' => 'Roving Performances')
	);

	public function __construct()
	{
		parent::__construct();
		$this->load->model('album_model');
	}

	public function index()
	{
		$this->args = $this->uri->uri_to_assoc(1);

		if (empty($this->args['year'])) {
			$this->args['year'] = date('Y', strtotime('last year'));
		}

		$result = new stdClass;
		$result->name = sprintf('%s FORMULA 1 SINGTEL SINGAPORE GRAND PRIX', $this->args["year"]);
		$result->years = $this->album_model->year($this->args['year']);

		$result->categories = $this->categories;

		$result->listing = $this->album_model->listing($this->args['year']);

		# ensure all album has a cover
		$missing = array();
		foreach ($result->listing as $idx => $row) {
			if (empty($row['photo'])) {
				$missing[$idx] = $row['id'];
			}
		}

		if (count($missing)) {
			$this->album_model->first_photo($missing, $result->listing);
		}

		json_response($result);
	}

	public function id($id)
	{
		$this->args = $this->uri->uri_to_assoc(2);

		if (!isset($this->args['category'])) {
			$this->args['category'] = null;
		}

		if (!isset($this->args['start'])) {
			$this->args['start'] = 0;
		}

		if (!isset($this->args['limit'])) {
			$this->args['limit'] = 10;
		}

		$result = new stdClass;
		$result->start = (int) $this->args['start'];
		$result->limit = (int) $this->args['limit'];

		list($result->total, $result->categories, $result->photos, $result->name) =
		$this->album_model->get(
			$id,
			$this->categories,
			$this->args['category'],
			$this->args['start'],
			$this->args['limit']
		);

		json_response($result);
	}
}

