<?php

error_reporting(0);

if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Ticketing extends CI_Controller
{
    private function isEarlyBird(){
        return false;
    }

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ticketing_model');
        $this->load->model('schedule_model');
    }

    /**
     * Might become obsolete in favour of --withTitle-- method
     */
	public function index()
	{
		$result = new stdClass;
		$result->category = $this->ticketing_model->listing();
//        $result->highlight = array(); //$this->listing();

//        $result->highlight = $this->listing(); //
//        $path = FCPATH.'application/libraries/TicketListing.json';
//        $json = json_decode(file_get_contents($path));
//
//        header('Content-type: application/json;charset=utf-8');
//        header('Content-type: application/json');
        die( json_encode($result) );
	}

    /**
     * Added after 2014 race to support multiple year ticket listing with HeaderBar
     */
    public function withTitle()
    {
        $result = new stdClass;

        if (Ticketing::isEarlyBird()) {
            $result->category = array(
                array(
                    "left"=> "2016",
                    "right"=> "Early Bird Price",//Early Bird Price
                    "category" => $this->ticketing_model->listingEarlybird()
                )
            );
        }
        else {
            $result->category = array(
                array(
                    "left"=> "2016",
                    "right"=> "Regular Price Tickets",//"Early Bird Price Until \n30 April 2015", //Early Bird Price From
                    "category" => $this->ticketing_model->listing()
                )
            );
        }

        $result->highlight = $this->listing();

//        $path = FCPATH.'application/libraries/TicketListingWithTitles.json';
//        $json = json_decode(file_get_contents($path));
//        header('Content-type: application/json;charset=utf-8');
//        header('Content-type: application/json');
//        die( json_encode($json) );

        json_response($result);
    }

    //Early bird session
    public function earlyBird()
    {
        $result = new stdClass;

        if (Ticketing::isEarlyBird()) {
            $result->category = array(
                array(
                    "left"=> "2016",
                    "right"=> "Early Bird Price",//Early Bird Price
                    "category" => $this->ticketing_model->listingEarlybird()
                )
            );
        }
        else {
            $result->category = array(
                array(
                    //"left"=> "2016",
                    //"right"=> "Regular Price Tickets",//"Early Bird Price Until \n30 April 2015", //Early Bird Price From
                    "category" => $this->ticketing_model->listing()
                )
            );
        }

        $result->highlight = $this->listing();

        json_response($result);
    }

	public function category()
	{
		$this->args = $this->uri->uri_to_assoc(2);
		$id = (int) $this->args['category'];
		$option = (int) $this->args['option'];
        if(isset($this->args['option'])){

            if (Ticketing::isEarlyBird()) {
                $result = $this->ticketing_model->ebcategory($id, $option);
            }
            else {
                $result = $this->ticketing_model->category($id, $option);
            }

            $result["description"] = preg_replace("@#id-@", "&option_id=", $result["description"]);
            json_response($result);

        }else{
            /**
             * Return all subcategory detail information if no option is passed
             */

            $cat_id = $this->ticketing_model->listingId($id);

            foreach($cat_id as $option){
                $oid = $option['id'];
                if (Ticketing::isEarlyBird()) {
                    $res = $this->ticketing_model->category($id, $oid);
                }
                else {
                    $res = $this->ticketing_model->category($id, $oid);
                }
                $res['option'] = utf8_encode($res['option']);
                $res['description'] = $res['description'];
                $result[$oid] = $res;
                unset($res);
            }

            json_response($result);
        }

	}

    public function ebcategory()
    {
        $this->args = $this->uri->uri_to_assoc(2);
        $id = (int) $this->args['ebcategory'];
        $option = (int) $this->args['option'];
        if(isset($this->args['option'])){

            if (Ticketing::isEarlyBird()) {
                $result = $this->ticketing_model->ebcategory($id, $option);
            }
            else {
                $result = $this->ticketing_model->category($id, $option);
            }
            $result["description"] = preg_replace("@#id-@", "&option_id=", $result["description"]);
            json_response($result);

        }else{

            $cat_id = $this->ticketing_model->listingId($id);

            foreach($cat_id as $option){
                $oid = $option['id'];

                if (Ticketing::isEarlyBird()) {
                    $res = $this->ticketing_model->ebcategory($id, $oid);
                }
                else {
                    $res = $this->ticketing_model->category($id, $oid);
                    // hardcode to replace zone access map
                    if ($id == 20) {
                        $res['zone_access_path'] = str_replace('2016_Bay_Grandstand_2208x768px.jpg', '2017_Bay_Grandstand_SEB_1365x768px.jpg', $res['zone_access_path']);
                        $res['zone_access_thumb_path'] = str_replace('2016_Bay_Grandstand_2208x768px.jpg', '2017_Bay_Grandstand_SEB_1365x768px.jpg', $res['zone_access_thumb_path']);

                        $res['zone_access_path'] = str_replace('2016_Connaught_Grandstand_2208x768px.jpg', '2017_Connaught_Grandstand_2208x768px.jpg', $res['zone_access_path']);
                        $res['zone_access_thumb_path'] = str_replace('2016_Connaught_Grandstand_2208x768px.jpg', '2017_Connaught_Grandstand_2208x768px.jpg', $res['zone_access_thumb_path']);

                        $res['zone_access_path'] = str_replace('2016_Pit_2208x768px.jpg', '2017_Pit_SEB_1365x768px.jpg', $res['zone_access_path']);
                        $res['zone_access_thumb_path'] = str_replace('2016_Pit_2208x768px.jpg', '2017_Pit_SEB_1365x768px.jpg', $res['zone_access_thumb_path']);

                        $res['zone_access_path'] = str_replace('2016_Zone1Walkabout_Combination_2208x768px.jpg', '2017_Zone1Walkabout_Combination_2208x768px.jpg', $res['zone_access_path']);
                        $res['zone_access_thumb_path'] = str_replace('2016_Zone1Walkabout_Combination_2208x768px.jpg', '2017_Zone1Walkabout_Combination_2208x768px.jpg', $res['zone_access_thumb_path']);
                    }
                }

                $res['option'] = utf8_encode($res['option']);
                $res['description'] = $res['description'];
                $result[$oid] = $res;
                unset($res);
            }

            json_response($result);
        }
    }

	public function agent()
	{
		$this->args = $this->uri->uri_to_assoc(2);
		$agent = $this->args["agent"];
		$region = isset($this->args["region"]) ? $this->args["region"] : null;
		$country = isset($this->args["country"]) ? $this->args["country"] : null;

		$result = new stdClass;
		switch ($agent)
		{
			case "singapore":
				$result->region = $this->ticketing_model->get_singapore_region();
				if (!empty($region))
				{
					$result->listing = $this->ticketing_model->get_singapore_region_listing($region);
				}
				break;

			case "international":
				$res = $this->ticketing_model->get_international_country();
				$result->country = array_values($res);

				if (!empty($country))
				{
					$result->listing = $this->ticketing_model->get_international_country_listing($res, $country);
				}
				break;
		}
		json_response($result);
	}

    /**
     * @return array
     *
     * @description To combine ticketing and highlights in one api
     */
    public function listing()
    {
        $listing = $this->schedule_model->get_highlights();

        $array = array();

        foreach($listing as $list){
            $obj = $list;

            // inject SGP website if image path is relative
            if(strpos($list['banner'],'http://') === false){
                $obj['banner']= 'http://singaporegp.sg/'.$obj['banner'];
            }
            array_push($array,$obj);
        }
        return $array;
    }
}