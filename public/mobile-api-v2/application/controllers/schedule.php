<?php
if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Schedule extends CI_Controller
{
    protected $args = array();
    protected $ids = "171,130,124,127,128,129";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('schedule_model');
    }

    public function calendar()
    {
        $result = new stdClass;
        $result->listing = $this->schedule_model->calendar();
        json_response($result);
    }

    public function type()
    {
        $result = new stdClass;

        $this->args = $this->uri->uri_to_assoc(2);
        $type = isset($this->args["type"]) ? $this->args["type"]  : "overview";
        $date = isset($this->args["date"]) ? $this->args["date"] : "all";
        $zone = isset($this->args["zone"]) ? $this->args["zone"] : "all";

        switch ($type) {
            case 'ontrack':
                $result->listing = $this->get_ontrack($date);
                break;

            case 'offtrack':
                $result->listing = $this->get_offtrack($date, $zone);
                break;

            case "offtrackunique":
                $result->listing = $this->get_offtrackUnique($date, $zone);
                break;

            default:
                $result->listing = $this->schedule_model->calendar($date, $zone);
                break;

        }

        $finalresult = array();
        foreach($result->listing as $row){
            $tmpRow = $row;
            $tmpRow ['detail_id'] = (int)$row['detail_id'];
            $tmpRow ['sort'] = (int)$row['sort'];
            array_push($finalresult,$tmpRow);
        }

        $result->listing = $finalresult;
        json_response($result);
    }

    public function ontrack($id)
    {
        $this->args = $this->uri->uri_to_assoc(3);
        $id = (int) $this->args['id'];
        $result = $this->schedule_model->ontrack($id);
        json_response($result);
    }

    public function offtrack()
    {
        $this->args = $this->uri->uri_to_assoc(3);
        $id = (int) $this->args['id'];
        $result = $this->schedule_model->offtrack($id);
        json_response($result);
    }

    public function offtrackDetail()
    {
        $this->args = $this->uri->uri_to_assoc(3);
        $id = (int) $this->args['id'];
        $result = $this->schedule_model->offtrackDetail($id);
        json_response($result);
    }

    private function get_offtrack($date = null, $zone = null)
    {
        $result = new stdClass;
//		$result->listing = $this->schedule_model->get_offtrack($date, $zone, $this->ids);
        $result->listing = $this->schedule_model->get_offtrack($date, $zone);

        $finalresult = array();
        foreach($result->listing as $row){
            $tmpRow = $row;
            $tmpRow ['sort'] = (int)$row['sort'];
            array_push($finalresult,$tmpRow);
        }

        $result->listing = $finalresult;

        json_response($result);
    }

    private function get_offtrackUnique($date = null, $zone = null)
    {
        $result = new stdClass;
//        $rows = $this->schedule_model->get_offtrack($date, $zone, $this->ids);
        $rows = $this->schedule_model->get_offtrack($date, $zone);
        $finalResult = array();
        $zoneById = array();

        // filter out duplicate records
        foreach($rows as $row){
            $found = false;
            foreach($finalResult as $fRow){
                if($fRow['id'] === $row['id']){
                    $found = true;
                    break;
                }
            }

            // track zones to put together
            $zoneById[$row['id']] = is_array($zoneById[$row['id']]) ? $zoneById[$row['id']] : array();
            if(!in_array($row["zone_id"],$zoneById[$row['id']])){
                array_push($zoneById[$row['id']],$row["zone_id"]);
            }

            //hijack sort value to convert from string to int
            $row["sort"] = intval($row["sort"]);

            if($found)
                continue;
            else
                array_push($finalResult,$row);
        }

//        $finalResult = $this->sortByGivenIdArray($this->ids,$finalResult);
        if(!$finalResult)
            json_response();

        // convert zone array to string
//        $zoneById = array_unique($zoneById);
        foreach($zoneById as &$row){
            $row = implode(',',$row);
        }

        // inject zone list into result
        foreach($finalResult as &$row){
            $row['zone_list'] = $zoneById[$row['id']];
        }

        $result->listing = $finalResult;
//        $result->listing = $zoneById;

        $result->categories = array(
            array(
                'cat_ref'=>'HEADLINING ACTS',
                'cat_name'=>'HEADLINING ACTS'
            ),
            array(
                'cat_ref'=>'MUSIC (LOCAL)',
                'cat_name'=>'MUSIC (LOCAL)'
            ),
            array(
                'cat_ref'=>'MUSIC (INTERNATIONAL)',
                'cat_name'=>'MUSIC (INTERNATIONAL)'
            ),
            array(
                'cat_ref'=>'ROVING ACTS',
                'cat_name'=>'ROVING ACTS'
            )
        );

        json_response($result);
    }

    private function get_ontrack($date = null)
    {
        $result = new stdClass;
        $result->listing = $this->schedule_model->get_ontrack($date);
//        $result->listing = array();

        $finalListing = array();
        foreach($result->listing as $row){
            $tmpRow = $row;
            $tmpRow ['detail_id'] = (int)$row['detail_id'];
            $tmpRow ['sort'] = (int)$row['sort'];
            array_push($finalListing,$tmpRow);
        }

        $result->listing = $finalListing;

        json_response($result);
    }

    private function sortByGivenIdArray($string,$array){
        $ids = explode(',',$string);
        if(count($ids) != count($array))
            return false;

        $finalResult = array();
        for($i=0;$i<count($ids);$i++){
            foreach($array as $row){
                if($row["id"] == $ids[$i]){
                    array_push($finalResult, $row);
                    break;
                }
            }
        }
        return $finalResult;
    }
}