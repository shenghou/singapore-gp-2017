<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Token extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('token_model');
	}

	public function register()
	{
		$this->args = $this->uri->uri_to_assoc(1);
		$token = $this->args["token"];
		$platform = $this->args["platform"];

		$result = $this->token_model->register($token, $platform);
		json_response($result);
	}

	public function verify($token, $platform)
	{
		$this->args = $this->uri->uri_to_assoc(1);
		$token = $this->args["token"];
		$platform = $this->args["platform"];

		$result = $this->token_model->verify($token, $platform);
		json_response($result);
	}
}

