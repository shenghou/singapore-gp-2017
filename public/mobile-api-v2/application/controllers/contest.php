<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Contest extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('contest_model');
	}

	public function index()
	{
		$result = new stdClass;
		$result->listing = $this->contest_model->listing();
		$result->default_text = "Thank you for your interest. Please check back here soon for more contests.";
		json_response($result);
	}
}

