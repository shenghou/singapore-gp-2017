<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Zone extends CI_Controller
{
	protected $args = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->model('zone_model');
	}

	public function access()
	{
		$result = new stdClass;
		$result->listing = $this->zone_model->listing();
		json_response($result);
	}
}

