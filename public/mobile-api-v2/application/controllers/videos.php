<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Videos extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('videos_model');
	}

	public function index()
	{
		$result = $this->videos_model->listing();
		json_response($result);
	}
}

