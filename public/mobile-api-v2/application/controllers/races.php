<?php
error_reporting(0);
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Races extends CI_Controller
{
	protected $args = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->model('races_model');
	}

	public function standing()
	{
		$this->args = $this->uri->uri_to_assoc(1);
		$id = $this->args['id'];
		$result = new stdClass;
		$result->listing = $this->races_model->standing($id);
		json_response($result);
	}

	public function winner()
	{
		$result = new stdClass;
		$result->listing = $this->races_model->winner();
		json_response($result);
	}

	public function calendar()
	{
		$result = new stdClass;
		$result->listing = $this->races_model->calendar();
		json_response($result);
	}

	public function teams($type = 'standing')
	{
		$result = new stdClass;
		$result->listing = $this->races_model->teams_standing();
		json_response($result);
	}

	public function drivers($type = 'standing')
	{
		$result = new stdClass;
		$result->listing = $this->races_model->drivers_standing();
		json_response($result);
	}

	public function latest($type = 'result')
	{
		$result = new stdClass;
		list($result->race_date, $result->listing) = $this->races_model->latest_result();
		json_response($result);
	}
}

