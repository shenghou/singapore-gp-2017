<?php

error_reporting(0);

if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Teams extends CI_Controller
{
	protected $args = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->model('teams_model');
	}

	public function index()
	{
		$result = new stdClass;
		$result->listing = $this->teams_model->listing();
		json_response($result);
	}

	public function id()
	{
		$this->args = $this->uri->uri_to_assoc(2);
		$id = $this->args['id'];
		$result = $this->teams_model->get($id);
		json_response($result);
	}

	public function driver($id)
	{
		$this->args = $this->uri->uri_to_assoc(3);
		$id = $this->args['id'];
		$result = $this->teams_model->getDriver($id);
		json_response($result);
	}
}

