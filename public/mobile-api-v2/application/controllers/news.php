<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class News extends CI_Controller
{
	protected $args = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->model('news_model');
	}

	public function index()
	{
		$this->args = $this->uri->uri_to_assoc(1);

		if (!isset($this->args['year'])) {
			$this->args['year'] = date('Y');
		}

		$result = new stdClass;
		$result->years = $this->news_model->year($this->args['year']);
		$result->listing = $this->news_model->listing($this->args['year']);
		json_response($result);
	}

	public function id($id)
	{
		$result = $this->news_model->get($id);
		json_response($result);
	}
}

