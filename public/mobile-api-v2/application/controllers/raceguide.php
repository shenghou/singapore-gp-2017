<?php

error_reporting(0);

if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Raceguide extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('raceguide_collection_model');
        $this->load->model('raceguides_model');
    }

	public function index()
	{
		$result = new stdClass;
		$result->listing = $this->raceguide_collection_model->collection();
        $finalResult = array();

        foreach($result->listing as $row){
            $rrr = $row;
            if(strpos($rrr['url'],'http://') === false)
                $rrr['url'] = 'http://'. $_SERVER['SERVER_NAME'].'/raceguide/'.$rrr['url'];
            array_push($finalResult,$rrr);
        }

        $result->listing = $finalResult;
		json_response($result);
	}
}