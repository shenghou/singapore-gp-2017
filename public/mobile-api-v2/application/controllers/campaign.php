<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Campaign extends REST_Controller
{
	//Constant variable
	const year = 2016;
	const server = "http://singaporegp.sg"; //Host name
	const server_media = "http://singaporegp.sg/media"; //Host media path
	const server_web_service = "http://localhost"; //Host to call web service

	public function __construct()
	{
		parent::__construct();
		$this->load->model('campaign_model');
	}

	public function index_get()
	{
		$result = new stdClass;

		//Get Current Contest
		$current_contest = $this->campaign_model->contest_month_current();
		$current_contest = empty($current_contest) ? 0 : $current_contest[0]['current_contest'];

		//Get Current Grand Draw and Grand Draw status
		$grand_draw = $this->campaign_model->grand_draw_current();

		if (empty($grand_draw)) {
			$grand_draw_status = "";
			$current_grand_draw = 0;
		} else {
			$grand_draw_status = $grand_draw[0]['status'];
			$current_grand_draw = $grand_draw[0]['current_grand_draw'];
		}

		//Get Contest
		$contest_list = $this->campaign_model->contest(Campaign::year);

		//Get Grand Draw
		$grand_draw_list = $this->campaign_model->grandDraw(Campaign::year);

		//Add default thumbnail image if empty
		$length = count($grand_draw_list);

		for ($i = 0; $i < $length; $i++) {
			if (empty($grand_draw_list[$i]['thumbnail_url'])) {
				$grand_draw_list[$i]['thumbnail_url'] = Campaign::server_media . "/mobile-campaign/2016/grand-draw/default_thumbnail.jpg";
			}
		}

		//Map output structure
		$result->tnc = Campaign::server . "/terms";
		$result->tnc_contest = Campaign::server . "/campaign/contest/terms";
		$result->tnc_grand_draw = Campaign::server . "/campaign/grand-draw/terms";
		$result->tnc_contest_popout = Campaign::server . "/campaign/contest/terms_raw";
		$result->tnc_grand_draw_popout = Campaign::server . "/campaign/grand-draw/terms_raw";
		$result->how_to_participate_contest = Campaign::server . "/campaign/contest/how-to-participate";
		$result->how_to_participate_grand_draw = Campaign::server . "/campaign/grand-draw/how-to-participate";
		$result->disclaimer_contest = "";
		$result->disclaimer_grand_draw = "";
		$result->current_contest = strval($current_contest);
		$result->current_grand_draw = strval($current_grand_draw);
		$result->grand_draw_status = strval($grand_draw_status);
		$result->grand_draw_info = "**For March to July entries, the entries will be stored locally. The entries will be erased if this app is uninstalled.";
		$result->grand_draw_save_message_url = Campaign::server_media ."/mobile-campaign/2016/grand-draw/grand_draw_save_message.png";
		$result->grand_draw_submit_message_url = Campaign::server_media ."/mobile-campaign/2016/grand-draw/grand_draw_submit_message.png";

		//Result List
		$result->contest_list = $contest_list;
		$result->grand_draw_list = $grand_draw_list;

		json_response($result);
	}

	public function contest_get($year, $month)
	{
		$result = new stdClass;
		$contest = $this->campaign_model->contestDetail($year, $month);
		$result->contest = empty($contest) ? "" : $contest[0];
		json_response($result);
	}

	public function grandDraw_get($year, $month)
	{
		$result = new stdClass;
		$grand_draw = $this->campaign_model->grandDrawDetail($year, $month);
		if (!empty($grand_draw) && empty($grand_draw[0]['thumbnail_url'])) {
			$grand_draw[0]['thumbnail_url'] = Campaign::server_media . "/mobile-campaign/2016/grand-draw/default_thumbnail.png";
		}
		$result->grand_draw = empty($grand_draw) ? "" : $grand_draw[0];
		json_response($result);
	}

	public function submitContest_post()
	{
		//Get input
		$data = array(
			'name_first' => $this->post('name_first'),
			'name_last' => $this->post('name_last'),
			'contact' => $this->post('contact'),
			'email' => $this->post('email'),
			'country' => $this->post('country'),
			'nationality' => $this->post('nationality'),
			'contest_month' => $this->post('contest_month'),
			'contest_data_list' => $this->post('contest_data_list'),
			'contest_file_list' => $this->post('contest_file_list'),
			'subscribe_newsletter' => $this->post('subscribe_newsletter'),
			'subscribe_newsletter_partner' => $this->post('subscribe_newsletter_partner')
		);

		//Init Return
		$result = new stdClass;

		//Validation
		if ($data['contest_month'] == false) {
			json_error("Contest month cannot be empty!");
		}
		$this->validate($data);

		//Find contest month
		$contest = $this->campaign_model->contestDetail(Campaign::year, $data['contest_month']);

		//Show error if contest not found
		if (empty($contest)) {
			json_error("Contest month does not exist in system!");
		}

		//Get the first result of contest list
		$contest = $contest[0];

		/*
		 * Validate contest status
		 *		E = Expired
		 * 		A = Active
		 * 		U = Upcoming
		 */
		if ($contest['status'] === Campaign_Model::contest_status_expired) {
			json_error("You're too late, this contest has been expired.");
		}
		if ($contest['status'] === Campaign_Model::contest_status_upcoming) {
			json_error("Sit tight, this contest has not started yet.");
		}

		/*
		 * Validate the contest data base on contest type
		 * 		1 = Webview
		 * 		2 = Photo
		 * 		3 = Game
		 */
		switch ($contest['contest_type']) {
			case Campaign_Model::contest_type_webview :
				if ($data['contest_data_list'] == false) {
					json_error("Contest submission data cannot be empty.");
				}
				if ($data['contest_data_list'] . gettype() != "Array") {
					json_error("Contest submission data format invalid.");
				}

				$length = count($data['contest_data_list']);
				$data['status'] = Campaign_Model::contest_submission_status_success;

				for ($i = 0; $i < $length; $i++) {
					$contest_data = $data['contest_data_list'][$i];

					if (!isset($contest_data['status'])) {
						$error = "Contest data's status cannot be empty for Q%s.";
						json_error(sprintf($error, $i + 1));
					}
					if ($contest_data['status']) {
						$contest_data['status'] = Campaign_Model::contest_submission_status_success;
					} else {
						$contest_data['status'] = Campaign_Model::contest_submission_status_fail;
						$data['status'] = Campaign_Model::contest_submission_status_fail;
					}

					if (empty($contest_data['data'])) {
						$error = "Contest data's answer cannot be empty for Q%s.";
						json_error(sprintf($error, $i + 1));
					}
				}
				break;

			case Campaign_Model::contest_type_photo :
				if (empty($data['contest_file_list'])) {
					json_error("Contest file cannot be empty!");
				}

				$length = count($data['contest_file_list']);

				for ($i = 0; $i < $length; $i++) {
					$contest_file = $data['contest_file_list'][$i];

					if (empty($contest_file['contest_file_format'])) {
						json_error("Contest photo format cannot be empty!");
					}
					if (empty($contest_file['contest_file'])) {
						json_error("Contest photo data cannot be empty!");
					}
				}
				break;

			case Campaign_Model::contest_type_game :
				if (empty($data['contest_data_list'])) {
					json_error("Contest data cannot be empty!");
				}

				if (empty($data['contest_data_list'][0]['data'])) {
					json_error("Game score cannot be empty!");
				}

				$result->game_score = $data['contest_data_list'][0]['data'];
				//Covert game score
				$data['contest_data_list'][0]['data_display'] = Campaign::secondsToWords($result->game_score);
				$result->game_score_formatted = $data['contest_data_list'][0]['data_display'];
				break;

			default:
				$error = "Monthly contest type invalid for value \"%s\".";
				json_error(sprintf($error, $contest['contest_type']));
		}

		//Write file to directory if is photo contest
		if ($contest['contest_type'] == Campaign_Model::contest_type_photo) {
			$contest_file_list = $data['contest_file_list'];
			$length = count($contest_file_list);

			for ($i = 0; $i < $length; $i++) {
				$filePath = $this->writeByteArrayToFile($contest_file_list[$i], $data['contest_month']);
				$contest_data['file'] = $filePath;
				$contest_data['data'] = $contest_file_list[$i]['contest_file_caption'];
				$contest_data_list[] = $contest_data;
			}

			$data['contest_data_list'] = $contest_data_list;
		}

		//Submit contest
		$data['monthly_contest_id'] = $contest['id'];
		$data['contest_type'] = $contest['contest_type'];
		$this->campaign_model->submitContest($data);

		//Subscribe newsletter
		if ($contest['contest_type'] != Campaign_Model::contest_type_webview) {
			$data['subscribe_newsletter'] = ($data['subscribe_newsletter'] == 'true') ? 1 : 0;
			$data['subscribe_newsletter_partner'] = ($data['subscribe_newsletter_partner'] == 'true') ? 1 : 0;

			if ($data['subscribe_newsletter']) {
				$this->subscribeNewsletter($data);
			} elseif ($data['subscribe_newsletter_partner']) {
				$this->subscribeNewsletter($data);
			}
		}

		//Response
		$result->name_first = $data['name_first'];
		$result->name_last = $data['name_last'];
		$result->contact = $data['contact'];
		$result->email = $data['email'];
		$result->country = $data['country'];
		$result->nationality = $data['nationality'];
		$result->contest_month = $data['contest_month'];
		$result->subscribe_newsletter = $data['subscribe_newsletter'];
		$result->subscribe_newsletter_partner = $data['subscribe_newsletter_partner'];

		json_response($result);
	}

	public function submitContestBonusEntry_post()
	{
		//Get input
		$input = array(
			'email' => $this->post('email'),
			'contest_month' => $this->post('contest_month'),
		);

		//Validation
		if ($input['contest_month'] == false) {
			json_error("Contest month cannot be empty!");
		}
		if ($input['email'] == false) {
			json_error("Email cannot be empty!");
		}

		//Find contest month
		$contest = $this->campaign_model->contestDetail(Campaign::year, $input['contest_month']);

		//Show error if contest not found
		if (empty($contest)) {
			json_error("Contest month does not exist in system!");
		}

		//Get the first result of contest list
		$contest = $contest[0];

		$data['monthly_contest_id'] = $contest['id'];
		$data['contest_type'] = $contest['contest_type'];
		$data['email'] = $input['email'];

		//Submit bonus entry
		$this->campaign_model->submitBonusEntryContest($data);

		//Response
		json_response();
	}

	public function submitGrandDraw_post()
	{
		//Get input
		$data = array(
			'name_first' => $this->post('name_first'),
			'name_last' => $this->post('name_last'),
			'contact' => $this->post('contact'),
			'email' => $this->post('email'),
			'country' => $this->post('country'),
			'nationality' => $this->post('nationality'),
			'grand_draw_list' => $this->post('grand_draw_list')
		);

		//Validation
		$this->validate($data);

		//Validate Data
		if (empty($data['grand_draw_list'])) {
			json_error("Grand draw answer cannot be empty!");
		}

		if (count($data['grand_draw_list']) != 6) {
			json_error("Grand draw answer not complete! Please enter your answer from march to august.");
		}

		//Check Answer
		$data['grand_draw_list'] = $this->validGrandDrawAnswer($data['grand_draw_list']);

		//Submit
		$this->campaign_model->submitGrandDraw($data);

		json_response();
	}

	private function subscribeNewsletter($data)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, Campaign::server_web_service . "/campaign/newsletter");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		curl_close($ch);
	}

	function secondsToWords($t)
	{
		$hour = $t/3600;
		$minute = $t/60%60;
		$second = $t%60;
		$split = explode('.', $t);
		$millisecond = empty($split[1]) ? 0 : $split[1];
		return sprintf('%02d:%02d:%02d.%02d', $hour, $minute, $second, str_replace('.','', $millisecond));
	}

	private function writeByteArrayToFile($contest_file, $contest_month = 4)
	{
		//Determine staging or production
		$filePath = Campaign::server;
		$uploadPath = "";
		$prefix = "";

		//Decide which file path to go base on contest month
		if ($contest_month == 4) {
			$uploadPath = "/Uploaded/mobile-campaign/2016/201604-photo_contest_april/";
			$prefix = "MC-201604";
		}
		if ($contest_month == 6) {
			$uploadPath = "/Uploaded/mobile-campaign/2016/201606-photo_contest_june/";
			$prefix = "MC-201606";
		}

		$filename = uniqid($prefix, $more_entropy = true) . "." . $contest_file['contest_file_format'];
		$fileUrl = $filePath . $uploadPath . $filename;
		$uploadPath = "../" . $uploadPath . $filename;
		$base64 = base64_decode($contest_file['contest_file']);
		file_put_contents($uploadPath, $base64);

		return $fileUrl;
	}

	private function validate($data)
	{
		if ($data['name_first'] == false) {
			json_error("First name cannot be empty!");
		}
		if ($data['name_last'] == false) {
			json_error("Last name cannot be empty!");
		}
		if ($data['contact'] == false) {
			json_error("Contact number cannot be empty!");
		}
		if ($data['email'] == false) {
			json_error("Email cannot be empty!");
		}
		if ($data['country'] == false) {
			json_error("Country cannot be empty!");
		}
	}

	private function validGrandDrawAnswer($grand_draw_list){
		$grand_draw_list[0]['status'] = ($grand_draw_list[0]['data'] == '33') ? true : false;
		$grand_draw_list[1]['status'] = ($grand_draw_list[1]['data'] == '44') ? true : false;
		$grand_draw_list[2]['status'] = ($grand_draw_list[2]['data'] == '55') ? true : false;
		$grand_draw_list[3]['status'] = ($grand_draw_list[3]['data'] == '66') ? true : false;
		$grand_draw_list[4]['status'] = ($grand_draw_list[4]['data'] == '77') ? true : false;
		$grand_draw_list[5]['status'] = ($grand_draw_list[5]['data'] == '88') ? true : false;
		return $grand_draw_list;
	}
}