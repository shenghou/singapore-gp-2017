<?php
if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Highlights extends CI_Controller
{
	protected $args = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->model('schedule_model');
	}

	public function listing()
	{
        $listing = $this->schedule_model->get_highlights();

//        // inject SGP website if image path is relative
        $array = array();
        foreach($listing as $list){
            if(strpos($list['banner'],'http://') === false){
                $obj = $list;
                $obj['banner']= 'http://singaporegp.sg/'.$obj['banner'];
                array_push($array,$obj);
            }
        }
        $result = new stdClass;
        $result->listing = $array;
		json_response($result);
	}
}

