<?php
/**
 * debug print_r
 */
error_reporting(0);

function pr($val)
{
	echo '<pre>';
	print_r($val);
	echo '<pre>';
	exit;
}

/**
 * to print response in json
 * @params mixed $result - result from database
 * @return void
 */
function json_response($result)
{
	$msg = new stdclass;
	$msg->success = true;

	if (!empty($result)) {
		$msg->results = $result;
	}

	$ci =& get_instance();
	$msg->time = $ci->benchmark->elapsed_time('total_execution_time_start');
	header('Content-type: application/json;charset=utf-8');
    header('Content-type: application/json');
//	die( json_encode($msg, JSON_UNESCAPED_SLASHES) );
	die( json_encode($msg) );
}

/**
 * to print error response in json
 * @params mixed $result - result from database
 * @return void
 */
function json_error($message)
{
	$msg = new stdclass;
	$msg->error = true;
	$msg->error_message = $message;

	$ci =& get_instance();
	$msg->time = $ci->benchmark->elapsed_time('total_execution_time_start');
	header('Content-type: application/json;charset=utf-8');
    header('Content-type: application/json');
	die( json_encode($msg) );
}