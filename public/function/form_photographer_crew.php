﻿<?php
require_once '../function/helper.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {

	$sqls = array();
	$error = false;
	$ok = false;

	$types = array(
		'crew' => 'accredited_photographer_crew_2013',
		'av' => 'accredited_av_2013'
	);

	while (empty($error) && empty($ok)) {
		# (multi) insert into accredited_photographer_crew_2013
		if (empty($_POST['crew'])) {
			$error = true;
			break;
		}

		foreach ($types as $type => $table) {
			$tmp = array();
			foreach ($_POST[$type] as $col => $arr) {
				if (!is_array($arr)) {
					$error = true;
					break 3;
				}

				foreach ($arr as $idx=>$val) {
					$tmp[$idx][$col] = $val;
				}
			}

			if (empty($tmp)) {
				$error = true;
				break 2;
			}

			foreach ($tmp as $idx=>$arr) {
				$key = sprintf('%s_%d', $type, $idx);

				if ($type == 'av') {
					$arr['seq'] = $idx + 1;
				}

				$sqls[$key] = db_set_insert_sql($table, $arr);
			}
		}

		# (single) then insert into accredited_av_incharge_2013
		if (empty($_POST['incharge'])) {
			$error = true;
			break;
		}

		$sqls['incharge'] = db_set_insert_sql('accredited_av_incharge_2013', $_POST['incharge']);

		# (multi) then insert into accredited_av_2013
		if (empty($_POST['av'])) {
			$error = true;
			break;
		}

		$ok = true;
	}

	if ($error) {
		json_error();
	}

	if (db_transaction($sqls)) {
		json_response();
	} else {
		json_error();
	}
}

# code uniquly identified the transaction
$code = substr(md5(time().$_SERVER['REMOTE_ADDR']), 0, 12);
?>
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js ie lt-ie7 ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie lt-ie8 ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie lt-ie9 ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Singapore Entry Online Submission - Singapore Grand Prix</title>
<meta name="description" content="Latest Singapore F1 news, information & press releases here. Download F1 press releases & recent formula one news & results from the Singapore Grand Prix." />
<meta name="keywords" content="singapore f1 news, f1 press release, singapore f1 results, grand prix newsletter sign up, formula 1 newsletter subscription, media release, racing news" />
<meta property="og:image" content="http://www.singaporegp.sg/images/logo.jpg">
<meta property="og:title" content="Singapore Entry Online Submission - Singapore Grand Prix">
<meta property="og:url" content="http://www.singaporegp.sg/media/form_journalist.php">
<meta property="og:description" content="Latest Singapore F1 news, information & press releases here. Download F1 press releases & recent formula one news & results from the Singapore Grand Prix.">
<meta name="viewport" content="width=device-width">
<link rel="image_src" href="http://www.singaporegp.sg/images/logo.jpg">
<link rel="canonical" href="http://www.singaporegp.sg/media/form_journalist.php">
<link rel="stylesheet" type="text/css" href="../css/master.css">
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.22/themes/ui-darkness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="../plugins/fancybox/jquery.fancybox.css?v=2.0.6" media="screen" />
<link rel="shortcut icon" href="../favicon.ico">
<script src="../js/modernizr-2.5.3.min.js"></script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-3141840-1']);
  _gaq.push(['_setDomainName', 'singaporegp.sg']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head>
<body id="media">
<div id="main">
  <header>
    <?php include("../include/inc-header.html"); ?>
  </header>
  <nav>
    <?php include("../include/inc-menu.html"); ?>
  </nav>
  <section>
    <header>
      <h1>Singapore Entry Form<br>
        - Accredited Photographers &amp; Television Crew</h1>
      <?php include("../include/inc-social.html"); ?>
    </header>
    <aside>
<article>
<p><u><strong>SINGAPORE ENTRY ONLINE SUBMISSION</strong></u></p>
<form method="post" onkeypress="return event.keyCode != 13;" id="form" name="form" action="http://web2.singaporegp.sg/media/form_photographer_crew.php">
	<header>
		<p>Each accredited crew member must submit the following information. If submitting for a group, please indicate the number of personnel:
			<select class="txt11-black" id="crew_count" onchange="addCrew()">
				  <option value="1">01</option>
				  <option value="2">02</option>
				  <option value="3">03</option>
				  <option value="4">04</option>
				  <option value="5">05</option>
				  <option value="6">06</option>
				  <option value="7">07</option>
				  <option value="8">08</option>
				  <option value="9">09</option>
				  <option value="10">10</option>
				  <option value="11">11</option>
				  <option value="12">12</option>
				  <option value="13">13</option>
				  <option value="14">14</option>
				  <option value="15">15</option>
				  <option value="16">16</option>
				  <option value="17">17</option>
				  <option value="18">18</option>
				  <option value="19">19</option>
				  <option value="20">20</option>
			</select>
		</p>
		<div class="clear"></div>
	</header>
	<p>Please fill in this information required to facilitate immigration clearance upon your arrival for the 2013 FORMULA 1 SINGAPORE GRAND PRIX:</p>
	<div class="mediacrew crew_clone">
<h3>PERSONAL INFORMATION</h3>
<p>
	<label>Full name as in passport</label>
	<input type="text" name="crew[full_name][]" maxlength="255">
</p>
<p>
	<label>Passport number</label>
	<input type="text" name="crew[passport_number][]" maxlength="32">
</p>
<p>
	<label>Date of Issue</label>
	<input type="text" name="crew[passport_issue_date][]" style="width:130px;" class="calendar">
</p>
<p>
	<label>Date of Expiry</label>
	<input type="text" name="crew[passport_expiry_date][]" style="width:130px;" class="calendar">
</p>
<p>
	<label>Country of origin</label>
	<?php require_once '../function/countries.php'; ?>
	<?php echo set_countries_dropdown('crew[country_of_origin][]') ;?>
</p>
<h3>COMPANY INFORMATION</h3>
<p>
	<label>News organisation represented</label>
	<input type="text" name="crew[news_organisation][]" maxlength="255">
</p>
<div class="clear"></div>
<p>
	<label>Designation</label>
	<input type="text" name="crew[designation][]" maxlength="255">
</p>
<h3>FLIGHT INFORMATION</h3>
<p>
	<label>Arrival Flight Number</label>
	<input type="text" name="crew[flight_arrival_number][]" maxlength="255">
</p>
<p>
	<label>Estimated Date &amp; Time of Arrival</label>
	<input type="text" name="crew[flight_arrival_date][]" style="width:130px;" class="datetime">
</p>
<p>
	<label>Departure Flight Number</label>
	<input type="text" name="crew[flight_departure_number][]" maxlength="255">
</p>
<p>
	<label>Estimated Date &amp; of Departure</label>
	<input type="text" name="crew[flight_departure_date][]" style="width:130px;" class="datetime">
	<input type="hidden" name="crew[code][]" value="<?php echo $code;?>">
</p>

	</div>
	<div id="crew_item"></div>

	<p>All photographers and television crew will also have to complete the equipment declaration form below listing the equipment you are intending to bring in to facilitate the entry of the items.</p>
	<div>
		<header>
			<h3>Audio-Visual Equipment Carried by Media Team From</h3>
			<p>
				<?php echo set_countries_dropdown('incharge[country_from]'); ?>
			</p>
			<div class="clear"></div>
		</header>
		<table border="0" align="center" cellpadding="0" cellspacing="0">
		  <tr>
			<td><p>
				<label>Full name as in passport</label>
				<input type="text" name="incharge[full_name]">
			  </p>
			  <p>
				<label>Passport number</label>
				<input type="text" name="incharge[passport_number]">
			  </p>
			  <p>
				<label>News organisation represented</label>
				<input type="text" name="incharge[news_organisation]">
			  </p>
			</td>
		  </tr>
		</table>
		<input type="hidden" name="incharge[code]" value="<?php echo $code;?>">
        <hr>
		<table border="0" align="center" cellpadding="0" cellspacing="0" class="av_clone">
		  <tr>
			<td width="4%"><p><strong class="seq">1.</strong></p></td>
			<td width="96%"><p>
				<label>Item</label>
				<input type="text" name="av[item][]">
			  </p>
			  <p>
				<label>Serial Number</label>
				<input type="text" name="av[serial_number][]">
			  </p>
			  <p>
				<label>Quantity</label>
				<input type="text" name="av[quantity][]">
			  </p>
			  <p>
				<label>Approx. Value<br>
				  (Singapore Dollars)</label>
				<input type="text" name="av[approx_value][]">
			  </p>
			 </td>
		  </tr>
		  <input type="hidden" name="av[code][]" value="<?php echo $code;?>">
		</table>
		<hr>
		<div id="av_item"></div>
		<a href="javascript:;" onclick="addAV()" class="more">Add more</a> </div>
		<input type="hidden" name="submit" value="1">
		<input type="button" name="submit" id="submit" value="Submit" style="margin-top:10px; padding:7px;" onclick="validate();">
        </form>


<div class="success">
	<p style="padding:25px">Your Singapore Entry Form has been successfully submitted. <br>We look forward to seeing you at the 2013 FORMULA 1 SINGAPORE GRAND PRIX!</p>
</div>
<div class="error">
	<p style="padding:25px;">An error has has occurred.<br>Please try again later</p>
</div>

      </article>
      <?php include("../include/inc-banner.html"); ?>
      <div class="clear"></div>
    </aside>
  </section>
  <footer>
    <?php include("../include/inc-footer.html"); ?>
  </footer>
</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="js/jquery-1.7.2.min.js"><\/script>')</script> 
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.22/jquery-ui.min.js"></script> 
<script>window.jQuery || document.write('<script src="js/jquery-ui-1.8.22.custom.min.js"><\/script>')</script> 
<script type="text/javascript" src="../js/navigation.js"></script> 
<script type="text/javascript" src="../js/jquery.mousewheel-3.0.6.pack.js"></script> 

<script type="text/javascript" src="../plugins/fancybox/jquery.fancybox.js?v=2.0.6"></script>
<script type="text/javascript" src="../js/action.js"></script>
<script type="text/javascript" src="../js/jquery-ui-timepicker-addon.js"></script>

<script>
var opt = {
	dateFormat: 'yy-mm-dd',
	changeMonth: true,
	changeYear: true,
	showOn: "both",
	buttonImage: "../sgp_admin/images/calendar_icon.gif",
	buttonImageOnly: true
};

$(function() {
	bindCalendar();
	bindDateTime();
});

function bindCalendar()
{
	$(".calendar").each(function() {
		$(this).datepicker(opt);
	});
}

function bindDateTime()
{
	$(".datetime").each(function() {
		$(this).datetimepicker(opt);
	});
}

function addAV()
{
	var av = $('.av_clone:last');
	var cnt = $('.av_clone').length;
	var klone = av.clone(false);
	klone.appendTo('#av_item').find('input:not[name^="code"]').val('');
	$('.av_clone:last').find('.seq').html(cnt+1+'.');
}

function addCrew()
{
	var cnt = $('#crew_count').val();
	var total = $('.crew_clone').length;
	if (total == cnt) {
		return true;
	}

	if (total > cnt) {
		var offset = 0;
		$('.crew_clone').each(function() {
			offset++;
			if (offset > cnt) {
				$(this).remove();
			}
		});
	} else {
		var offset = total;
		while (offset < cnt) {
			var klone = $('.crew_clone:last').clone(false)
			klone
			.find(".calendar, .datetime").removeAttr('id').removeClass('hasDatepicker').removeData('datepicker').unbind()
			.next().remove();

			klone.appendTo('#crew_item')
			.find('input:not[name^="code"],select').val('')
			.find(".calendar").datepicker(opt)
			.find(".datetime").datetimepicker(opt);

			offset++;
		}
		bindCalendar();
		bindDateTime();
	}
}

function validate()
{
	var ok = true;
	var expr = '#form input[name^="crew"], #form select[name^="crew"], #form input[name^="incharge"], #form select[name^="incharge"], #form input[name^="av"]';
	$(expr).each(function() {
		var attr = $(this).attr('name').replace(/^(crew|av)\[(.*)\]\[\]/, '$2');
		attr = attr.replace(/incharge\[(.*)\]/, '$1');

		if ($(this).val()) {
			return true;
		}

		switch (attr) {
			case 'full_name':
				alert("Please key in your full name as in passport!");
				break;

			case 'passport_number':
				alert("Please key in your passport number!");
				break;

			case 'passport_issue_date':
				alert("Please key in your passport date of issue!");
				break;

			case 'passport_expiry_date':
				alert("Please key in your passport date of expiry!");
				break;

			case 'country_of_origin':
				alert("Please key in your country of origin!");
				break;

			case 'country_from':
				alert("Please key in where is your audio-visual equipment carried from!");
				break;

			case 'news_organisation':
				alert("Please key in your news organisation presented!");
				break;

			case 'designation':
				alert("Please key in your designation!")
				break;

			case 'flight_arrival_number':
				alert("Please key in your arrival flight number!")
				break;

			case 'flight_arrival_date':
				alert("Please key in your estimated date & time of arrival!");
				break;

			case 'flight_departure_number':
				alert("Please key in your departure flight number!")
				break;

			case 'flight_departure_date':
				alert("Please key in your estimated date & time of departure!");
				break;

			case 'item':
				alert("Please key in your audio-visual equipment item!");
				break;

			case 'serial_number':
				alert("Please key in your audio-visual equipment item serial number!");
				break;

			case 'quantity':
				alert("Please key in your audio-visual equipment item quantity!");
				break;

			case 'approx_value':
				alert("Please key in your audio-visual equipment item approx. value in Singapore Dollars!");
				break;

			default:
			alert( $(this).attr('name'));
				alert('Please fill in this field!');
				break;
		}

		$(this).focus();
		$(window).scrollTop($(this).position().top);
		ok = false;
		return false;
	});

	if (!ok) {
		return false;
	}

	$.ajax({
		type: 'POST',
		dataType: 'json',
		data: $('#form').serialize(),
		beforeSend: function() {
			$("#submit").val("Submitting...").attr("disabled", true);
		},
		success: function(data, textStatus, xhr) {
			if (typeof data.success === "undefined") {
				showFancyBox('.error');
			} else {
				showFancyBox('.success');
			}
			enableSubmitButton();
		},
		error: function(xhr, textStatus, errorThrown) {
			showFancyBox('.error');
			enableSubmitButton();
		}
	});
}

function showFancyBox(selector)
{
	$.fancybox({
		'content': $(selector).html(),
		'minWidth'  : 250,
		'minHeight' : 100
	});
}

function enableSubmitButton()
{
	$("#submit").val("Submit").removeAttr("disabled");
}
</script>

<link rel="stylesheet" type="text/css" href="form-2013-tpl.css">
</body>
</html>
