<?php
# environment setup
if ( strpos($_SERVER['NAME'],'singaporegp.sg') === false) {
	define('ENVIRONMENT', 'staging');
} else {
	define('ENVIRONMENT', 'production');
}

function db_read_connect()
{
	static $db_connect;
	if (!empty($db_connect)) {
		return $db_connect;
	}

	switch (ENVIRONMENT) {
		case 'staging':
			$host = 'localhost';
			$user = 'cms';
			$pass = '321nobraccar0317';
			$db_name = 'sgp_cms';
			break;

		default:
			$host = 'localhost';
			$user = 'cms';
			$pass = '321nobraccar0317';
			$db_name = 'sgp_cms';
			break;
	}

	$db_connect = mysql_connect($host, $user, $pass);
	mysql_select_db($db_name, $db_connect);

	return $db_connect;
}

function db_write_connect($all = true)
{
	static $db_connects;
	if (!empty($db_connects)) {
		return $db_connects;
	}

	switch (ENVIRONMENT) {
		case 'staging':
			$hosts = array('localhost');
			$user = 'cms';
			$pass = '321nobraccar0317';
			$db_name = 'sgp_cms';
			break;

		default:
			if ($all) {
				$hosts = array('192.168.10.191', '192.168.10.192', '192.168.10.193');
			} else {
				$hosts = array('localhost');
			}

			$user = 'root';
			$pass = '321nobraccar0317';
			$db_name = 'sgp_cms';
			break;
	}

	foreach ($hosts as $host) {
		$db_connects[$host] = mysql_connect($host, $user, $pass);
		if (empty($db_connects[$host])) {
			# echo mysql_error();
		}
		mysql_select_db($db_name, $db_connects[$host]);
	}

	return $db_connects;
}

function db_select_all($sql, $connection = null)
{
	if (empty($connection)) {
		$connection = db_read_connect();
	}

	$res = mysql_query($sql, $connection);
	if (!$res) {
		return false;
	}

	$rtn = array();
	while ($row = mysql_fetch_assoc($res)) {
		$rtn[] = $row;
	}
	mysql_free_result($res);
	return $rtn;
}

function db_rget($sql)
{
	$conn = db_read_connect();

	$res = mysql_query($sql, $conn);
	if (!$res) {
		return false;
	}

	$row = mysql_fetch_assoc($res);
	mysql_free_result($res);
	return $row;
}

function db_get($sql)
{
	$db_connects = db_write_connect();
	$connection = current($db_connects);

	$res = mysql_query($sql, $connection);
	if (!$res) {
		return false;
	}

	$row = mysql_fetch_assoc($res);
	mysql_free_result($res);
	return $row;
}

function db_update($opt)
{
	$connections = db_write_connect($all = true);
	$connect = current($connections);
	$sql = sprintf(
		'UPDATE %s SET %s WHERE %s',
		$opt['table'],
		db_value_pair($opt['value'], $connect),
		db_value_pair($opt['where'], $connect, ' AND ')
	);

	return db_write_all($sql);
}

function db_update_raw($query)
{
    $connections = db_write_connect($all = true);
    $connect = current($connections);
    return db_write_all($query);
}

function db_insert($opt)
{
	# set up insert sql
	$sql = db_set_insert_sql($opt['table'], $opt['value']);
	return db_write_all($sql);
}

function db_write_all($sql)
{
	# insert into all db (production)
	# however, current setting is insert into first db only
	# cause there is make no sense to wait for all db to commit & rollback
	$db_connects = db_write_connect($all = true);

	foreach ($db_connects as $connect) {
		if (!mysql_query($sql, $connect)) {
			# echo mysql_error($connect);
			return false;
		}
	}

	return true;
}

function db_last_id()
{
	$db_connects = db_write_connect($all = true);
	return mysql_insert_id(current($db_connects));
}

function db_transaction($sqls)
{
	if (empty($sqls) || !is_array($sqls)) {
		return false;
	}

	$db_connects = db_write_connect();
	$connection = current($db_connects);
	mysql_query('START TRANSACTION', $connection);

	$ok = true;
	foreach ($sqls as $sql) {
		if (!mysql_query($sql, $connection)) {
			$ok = false;
			break;
		}
	}

	if ($ok) {
		return mysql_query('COMMIT', $connection);
	} else {
		# echo mysql_error($connection);
		mysql_query('ROLLBACK', $connection);
		return false;
	}
}

function db_set_insert_sql($table, $value)
{
	# write or read
	$db_connects = db_write_connect();
	$connection = current($db_connects);

	# load db column, and map accordingly
	$db_cols = db_desc_table($connection, $table);

	# map value to db column
	$posts = array();
	foreach ($db_cols as $key => $col) {
		if (isset($value[$key])) {
			$posts[$col] = mysql_real_escape_string($value[$key], $connection);
		}
	}

	$posts = array_filter($posts);

	# insertion
	$cols = implode(',', array_keys($posts));
	$vals = db_value_enclose(array_values($posts), $connection);
	return sprintf('INSERT INTO %s (%s) VALUES (%s)', $table, $cols, $vals);
}

function db_desc_table($db_connect, $table)
{
	$sql = sprintf('SHOW COLUMNS FROM %s', $table);
	$res = mysql_query($sql, $db_connect);
	if (!$res) {
		# echo mysql_error($db_connect);
		return false;
	}

	$rtn = array();
	while ($row = mysql_fetch_assoc($res)) {
		$col = $row['Field'];
		$rtn[$col] = $col;
	}
	mysql_free_result($res);
	return $rtn;
}

function db_value_enclose($arr, $connect)
{
	$expr = '"%s"';
	if (is_array($arr)) {
		$rtn = array();
		foreach ($arr as $val) {
			$rtn[] = sprintf($expr, mysql_real_escape_string($val, $connect));
		}
		return implode(',', $rtn);
	} else {
		return sprintf($expr, $arr);
	}
}

function db_value_pair($arr, $connect, $delimiter = ',')
{
	$rtn = array();
	foreach ($arr as $key => $val) {
		$rtn[] = sprintf('%s = "%s"', $key, mysql_real_escape_string($val, $connect));
	}
	return implode($delimiter, $rtn);
}

/**
 * to print response in json
 * @params mixed $result - result from database
 * @return void
 */
function json_response($result)
{
	$msg = new stdclass;
	$msg->success = true;

	if (!empty($result)) {
		$msg->results = $result;
	}

	header('Content-type: application/json;charset=utf-8');
	die( json_encode($msg) );
}

/**
 * to print error response in json
 * @params string $message - error message
 * @return void
 */
function json_error($message)
{
	$msg = new stdclass;
	$msg->error = true;
	$msg->error_message = $message;

	header('Content-type: application/json;charset=utf-8');
	die( json_encode($msg) );
}

function pr($vars)
{
	echo '<pre>';
	print_r($vars);
	echo '</pre>';
	exit;
}

function dump($vars)
{
	echo '<pre>';
	var_dump($vars);
	echo '</pre>';
	exit;
}

function db_query_all($connects, $sql)
{
	$rtn = array();
	foreach ($connects as $connect) {
		$res = mysql_query($sql, $connect);
		while ($row = mysql_fetch_assoc($res)) {
			$rtn[] = $row;
		}
		mysql_free_result($res);
	}
	return $rtn;
}

function save_as_csv($rows, $cols, $file)
{
	csv_header($file);

	if (empty($rows) || empty($cols)) {
		return false;
	}

	$csv = fopen('php://memory', 'rw');
	fputcsv($csv, $cols);
	foreach ($rows as $row) {
		fputcsv($csv, $row);
	}
	rewind($csv);
	echo stream_get_contents($csv);
	fclose($csv);
}

function multi_csv($rows, $file)
{
	csv_header($file);

	$csv = fopen('php://memory', 'rw');
	foreach ($rows as $idx => $arr) {
		foreach ($arr as $type => $srows) {
			$cols = false;
			foreach ($srows as $row) {
				if (empty($cols)) {
					fputcsv($csv, array_keys($row));
					$cols = true;
				}
				fputcsv($csv, $row);
			}
		}
	}

	rewind($csv);
	echo stream_get_contents($csv);
	fclose($csv);
}

function csv_header($file)
{
	header('Content-type: text/csv');
	header(sprintf('Content-disposition: attachment;filename=%s', $file));
}

function permalink($str)
{
	$match = array('+', '&', ' ', '_', '.', 'ä', 'ö', 'ü');
	$replace = array('and', 'and', '-', '-', '-', 'a', 'o', 'u');
	$tmp = strtolower(str_replace($match, $replace, $str));
	return trim(preg_replace('/[^\*,\-0-9A-z]|\^/', '', $tmp), '-');
}

function set_thumbnail_url($url, $prefix, $size)
{
	$postfix = '.jpg';
	return str_replace($postfix, '-'.$prefix.$size.$postfix, $url);
}


//From ticket-details.php
function replaceWithMinString($string){
    $newString = str_replace("Minimum", "Min.", $string);
    return $newString;
}
function replaceEachWithEmpty($string){
    $newString = str_replace(" each", "", $string);
    $newString = str_replace("每张", "", $newString);
    return $newString;
}

