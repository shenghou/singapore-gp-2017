﻿<?php
require_once '../function/helper.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {
	$opt = array(
		'table' => 'accredited_journalist_2013',
		'value' => $_POST,
		'check_referer' => 'form_journalist.php'
	);

	if (db_insert($opt)) {
		json_response();
	} else {
		json_error();
	}
}
?>
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js ie lt-ie7 ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie lt-ie8 ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie lt-ie9 ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Singapore Entry Online Submission - Singapore Grand Prix</title>
<meta name="description" content="Latest Singapore F1 news, information & press releases here. Download F1 press releases & recent formula one news & results from the Singapore Grand Prix." />
<meta name="keywords" content="singapore f1 news, f1 press release, singapore f1 results, grand prix newsletter sign up, formula 1 newsletter subscription, media release, racing news" />
<meta property="og:image" content="http://www.singaporegp.sg/images/logo.jpg">
<meta property="og:title" content="Singapore Entry Online Submission - Singapore Grand Prix">
<meta property="og:url" content="http://www.singaporegp.sg/media/form_journalist.php">
<meta property="og:description" content="Latest Singapore F1 news, information & press releases here. Download F1 press releases & recent formula one news & results from the Singapore Grand Prix.">
<meta name="viewport" content="width=device-width">
<link rel="image_src" href="http://www.singaporegp.sg/images/logo.jpg">
<link rel="canonical" href="http://www.singaporegp.sg/media/form_journalist.php">
<link rel="stylesheet" type="text/css" href="../css/master.css">
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.22/themes/ui-darkness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="../plugins/fancybox/jquery.fancybox.css?v=2.0.6" media="screen" />
<link rel="shortcut icon" href="../favicon.ico">
<script src="../js/modernizr-2.5.3.min.js"></script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-3141840-1']);
  _gaq.push(['_setDomainName', 'singaporegp.sg']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head>
<body id="media">
<div id="main">
  <header>
    <?php include("../include/inc-header.html"); ?>
  </header>
  <nav>
    <?php include("../include/inc-menu.html"); ?>
  </nav>
  <section>
    <header>
      <h1>Singapore Entry Form - ACCREDITED JOURNALIST</h1>
      <?php include("../include/inc-social.html"); ?>
    </header>
    <aside>
<article>
<p><u><strong>SINGAPORE ENTRY ONLINE SUBMISSION</strong></u><br />Please fill in this information required to facilitate immigration clearance upon your arrival for the 2013 FORMULA 1 SINGAPORE GRAND PRIX:</p>
<form method="post" onkeypress="return event.keyCode != 13;" id="form" action="http://web2.singaporegp.sg/media/form_journalist.php">
	<h3>PERSONAL INFORMATION</h3>
	<p>
		<label>Full name as in passport</label>
		<input type="text" name="full_name" id="full_name" maxlength="255">
	</p>
	<p>
		<label>Passport number</label>
		<input type="text" name="passport_number" id="passport_number" maxlength="32">
	</p>
	<p>
		<label>Date of Issue</label>
		<input type="text" name="passport_issue_date" id="passport_issue_date" style="width:130px;">
	</p>
	<p>
		<label>Date of Expiry</label>
		<input type="text" name="passport_expiry_date" id="passport_expiry_date" style="width:130px;">
	</p>
	<p>
		<label>Country of origin</label>
		<?php require_once '../function/countries.php'; ?>
		<?php echo set_countries_dropdown('country_of_origin') ;?>
	</p>
	<h3>COMPANY INFORMATION</h3>
	<p>
		<label>News organisation represented</label>
		<input type="text" name="news_organisation" id="news_organisation" maxlength="255">
	</p>
	<div class="clear"></div>
	<p>
		<label>Designation</label>
		<input type="text" name="designation" id="designation" maxlength="255">
	</p>
	<h3>FLIGHT INFORMATION</h3>
	<p>
		<label>Arrival Flight Number</label>
		<input type="text" name="flight_arrival_number" id="flight_arrival_number" maxlength="255">
	</p>
	<p>
		<label>Estimated Date &amp; Time of Arrival</label>
		<input type="text" name="flight_arrival_date" id="flight_arrival_date" style="width:130px;">
	</p>
	<p>
		<label>Departure Flight Number</label>
		<input type="text" name="flight_departure_number" id="flight_departure_number" maxlength="255">
	</p>
	<p>
		<label>Estimated Date &amp; of Departure</label>
		<input type="text" name="flight_departure_date" id="flight_departure_date" style="width:130px;">
	</p>
	<div class="clear"></div>
	<input type="hidden" name="submit" value="1">
	<input type="button" id="submit" value="Submit" style="padding:7px;" onclick="validate();">
</form>

<div class="success">
	<p style="padding:25px">Your Singapore Entry Form has been successfully submitted. <br>We look forward to seeing you at the 2013 FORMULA 1 SINGAPORE GRAND PRIX!</p>
</div>
<div class="error">
	<p style="padding:25px;">An error has has occurred.<br>Please try again later</p>
</div>
</article>
      <?php include("../include/inc-banner.html"); ?>
      <div class="clear"></div>
    </aside>
  </section>
  <footer>
    <?php include("../include/inc-footer.html"); ?>
  </footer>
</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.7.2.min.js"><\/script>')</script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.22/jquery-ui.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-ui-1.8.22.custom.min.js"><\/script>')</script>
<script type="text/javascript" src="../js/navigation.js"></script>
<script type="text/javascript" src="../js/jquery.mousewheel-3.0.6.pack.js"></script>

<script type="text/javascript" src="../plugins/fancybox/jquery.fancybox.js?v=2.0.6"></script>
<script type="text/javascript" src="../js/action.js"></script>
<script type="text/javascript" src="../js/jquery-ui-timepicker-addon.js"></script>

<script>
$(function() {
	var opt = {
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		showOn: "both",
		buttonImage: "../sgp_admin/images/calendar_icon.gif",
		buttonImageOnly: true
	};

	$( "#passport_issue_date" ).datepicker(opt);
	$( "#passport_expiry_date" ).datepicker(opt);

	$('#flight_arrival_date').datetimepicker(opt);
	$('#flight_departure_date').datetimepicker(opt);
});

function validate()
{
	var full_name = $('#full_name');
	if (!full_name.val()) {
		alert("Please key in your full name as in passport!");
		full_name.focus();
		return false;
	}

	var passport_number = $('#passport_number');
	if (!passport_number.val()) {
		alert("Please key in your passport number!");
		passport_number.focus();
		return false;
	}

	var passport_issue_date = $('#passport_issue_date');
	if (!passport_issue_date.val()) {
		alert("Please key in your passport date of issue!");
		passport_issue_date.focus();
		return false;
	}

	var passport_expiry_date = $('#passport_expiry_date');
	if (!passport_expiry_date.val()) {
		alert("Please key in your passport date of expiry!");
		passport_expiry_date.focus();
		return false;
	}

	var country_of_origin = $('#country_of_origin');
	if (!country_of_origin.val()) {
		alert("Please key in your country of origin!");
		country_of_origin.focus();
		return false;
	}

	var news_organisation = $('#news_organisation');
	if (!news_organisation.val()) {
		alert("Please key in your news organisation presented!");
		news_organisation.focus();
		return false;
	}

	var designation = $('#designation');
	if (!designation.val()) {
		alert("Please key in your designation!")
		designation.focus();
		return false;
	}

	var flight_arrival_number = $('#flight_arrival_number');
	if (!flight_arrival_number.val()) {
		alert("Please key in your arrival flight number!")
		flight_arrival_number.focus();
		return false;
	}

	var flight_arrival_date = $('#flight_arrival_date');
	if (!flight_arrival_date.val()) {
		alert("Please key in your estimated date & time of arrival!");
		flight_arrival_date.focus();
		return false;
	}

	var flight_departure_number = $('#flight_departure_number');
	if (!flight_departure_number.val()) {
		alert("Please key in your departure flight number!")
		flight_departure_number.focus();
		return false;
	}

	var flight_departure_date = $('#flight_departure_date');
	if (!flight_departure_date.val()) {
		alert("Please key in your estimated date & time of departure!");
		flight_departure_date.focus();
		return false;
	}

	$.ajax({
		type: 'POST',
		dataType: 'json',
		data: $('#form').serialize(),
		beforeSend: function() {
			$("#submit").val("Submitting...").attr("disabled", true);
		},
		success: function(data, textStatus, xhr) {
			if (typeof data.success === "undefined") {
				showFancyBox('.error');
			} else {
				showFancyBox('.success');
			}
			enableSubmitButton();
		},
		error: function(xhr, textStatus, errorThrown) {
			showFancyBox('.error');
			enableSubmitButton();
		}
	});
}

function showFancyBox(selector)
{
	$.fancybox({
		'content': $(selector).html(),
		'minWidth'  : 250,
		'minHeight' : 100
	});
}

function enableSubmitButton()
{
	$("#submit").val("Submit").removeAttr("disabled");
}
</script>

<link rel="stylesheet" type="text/css" href="form-2013-tpl.css">
</body>
</html>
