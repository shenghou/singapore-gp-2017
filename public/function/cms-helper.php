<?php
function generate_form($form, $row)
{
	$toggle_tinymce = array();
	$tinymce_width = 640;

	foreach ($form as $key => $element) {
		$value = isset($row[$key]) ? htmlspecialchars($row[$key]) : null;
		$pure_value = isset($row[$key]) ? $row[$key] : null;
		$type = isset($element['type']) ? $element['type'] : 'default';
		$style = isset($element['style']) ? $element['style'] : null;

		switch ($element['type']) {
			case 'callback':
				$func = $element['callback'];
				switch ($func) {
					case 'set_countries_dropdown';
						 $entry = $func($key, null, $value);
					break;

					case 'set_team':
						$entry = $func($key, $value);
						break;
				}
				break;

			case 'default_textarea':
				$entry = sprintf('<textarea name="%s"%s>%s</textarea>', $key, $style, $pure_value);
				break;

			case 'textarea':
				if (!empty($element['tinymce']))  {
					$toggle_tinymce[] = $key;
				}

				if (isset($element['tinymce_width'])) {
					$tinymce_width = $element['tinymce_width'];
				}

				printf('<tr><td colspan="2"><h4>%s:</h4><textarea name="%s">%s</textarea></td></tr>', $element['label'], $key, $value);
				continue 2;
				break;

			case 'radio':
				$entry = null;
				if (!is_array($element['selection'])) {
					continue;
				}

				$idx = 0;
				foreach ($element['selection'] as $val => $label) {
					$element_id = sprintf('radio_%s_%d', $key, $idx);
					$entry .= sprintf('<input type="radio" id="%s" name="%s" value="%s" %s><label for="%s">%s</label>',
					$element_id, $key, $val, ($val == $value ? 'checked' : null), $element_id, $label);
					$idx++;
				}
				break;

			case 'select':
				$entry = sprintf('<select name="%s">', $key);
				if (isset($element['prepend'])) {
					$entry .= '<option value="">--</option>';
				}
				$entry .= set_select_option($element['selection'], $value);
				$entry .= '</select>';
				break;

			case 'select_optgroup':
				$entry = sprintf('<select name="%s">', $key);
				if (isset($element['prepend'])) {
					$entry .= '<option value="">--</option>';
				}
				$entry .= set_select_optgroup($element['selection'], $value);
				$entry .= '</select>';
				break;


			default:
				$entry = sprintf('<input type="text" name="%s" maxlength="%s" value="%s" style="%s">',
				$key, $element['maxlength'], $value, $style);
				break;
		}

		if (isset($element['tips'])) {
			$tips = $element['tips'];
		} else {
			$tips = null;
		}

		echo <<<tr
<tr>
	<th>{$element['label']}:</th>
	<td>{$entry}{$tips}</td>
</tr>
tr;
	}

	if (!empty($toggle_tinymce)) {
		tinymce_js($toggle_tinymce, $tinymce_width);
	}
}

function set_team($name, $selected)
{
	$sql = 'SELECT id AS team_id, name FROM team WHERE status=1 ORDER BY seq';
	$teams = db_select_all($sql);
	foreach ($teams as $team) {
		$id = $team['team_id'];
		$label = $team['name'];
		$same = $id == $selected ? ' selected' : null;
		$opt .= sprintf('<option value="%s"%s>%s</option>'.PHP_EOL, $id, $same, $label);
	}

	return <<<select
<select name="{$name}">
	<option value="">Select Team</option>
	{$opt}
</select>
select;
}

function set_select($name, $options, $selected = null)
{
	if (!is_array($options)) {
		$tmp = array($options => $options);
		$options = $tmp;
	}

	$opt = null;
	foreach ($options as $val => $label) {
		$is_selected = $val == $selected ? ' selected' : null;
		$opt .= sprintf('<option value="%s"%s>%s</option>', $val, $is_selected, $label);
	}

	return <<<select
<select name="${name}">
{$opt}
</select>
select;
}

function generate_thumbnail($source, $thumbnail, $max_width, $max_height)
{
    list($source_width, $source_height, $source_type) = getimagesize($source);
    switch ($source_type) {
        case IMAGETYPE_GIF:
            $source_gd_image = imagecreatefromgif($source);
            break;
        case IMAGETYPE_JPEG:
            $source_gd_image = imagecreatefromjpeg($source);
            break;
        case IMAGETYPE_PNG:
            $source_gd_image = imagecreatefrompng($source);
            break;
    }
    if ($source_gd_image === false) {
        return false;
    }
    $source_ratio = $source_width / $source_height;
    $thumbnail_ratio = $max_width / $max_height;
    if ($source_width <= $max_width && $source_height <= $max_height) {
        $thumbnail_width = $source_width;
        $thumbnail_height = $source_height;
    } elseif ($thumbnail_ratio > $source_ratio) {
        $thumbnail_width = (int) ($max_height * $source_ratio);
        $thumbnail_height = $max_height;
    } else {
        $thumbnail_width = $max_width;
        $thumbnail_height = (int) ($max_width / $source_ratio);
    }
    $thumbnail_gd_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
    imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_width, $thumbnail_height, $source_width, $source_height);
    imagejpeg($thumbnail_gd_image, $thumbnail, 90);
    imagedestroy($source_gd_image);
    imagedestroy($thumbnail_gd_image);
    return true;
}

function tinymce_js($elements, $width = 640, $height = 320)
{
	$name = is_array($elements) ? implode(',', $elements) : $elements;
	echo <<<js
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script>
tinyMCE.init({
	// General options
	mode: "exact",
	elements: "{$name}",
	theme : "advanced",
	plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

	// Theme options
	theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect",
	theme_advanced_buttons2 : "fontsizeselectcut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview",
	theme_advanced_buttons3 : "forecolor,backcolor,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",
	theme_advanced_buttons4 : "print,|,ltr,rtl,|,fullscreen,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : true,
	theme_advanced_blockformats : "p,div,address,pre,h1,h2,h3,h4,h5,h6",
	forced_root_block : '',

	// Skin options
	skin : "o2k7",
	skin_variant : "silver",
		
	// Example content CSS (should be your site CSS)
	content_css : "../css/page.css,../css/txtcolor.css",

	// Drop lists for link/image/media/template dialogs
	template_external_list_url : "js/template_list.js",
	external_link_list_url : "js/link_list.js",
	external_image_list_url : "js/image_list.js",
	media_external_list_url : "js/media_list.js",
	convert_urls : false,
	forced_root_block : 'p',

	// Replace values for the template plugin
	template_replace_values : {
		username : "Some User",
		staffid : "991234"
	}
});
</script>

js;
}

function check_session_exists()
{
	session_start();
	if (empty($_SESSION['cms_username'])) {
		header('Location:login.php');
		exit();
	}
}

function set_query_string($keys)
{
	$qs = array();
	foreach ($keys as $key) {
		$qs[$key] = isset($_REQUEST[$key]) ? $_REQUEST[$key] : null;
	}
	return http_build_query($qs);
}

function set_select_option($selection, $selected)
{
	if (!is_array($selection)) {
		return false;
	}

	$entry = null;
	foreach ($selection as $val => $label) {
		$entry .= sprintf(
			'<option value="%s" %s>%s</option>',
			$val,
			($val == $selected ? 'selected' : null),
			$label
		);
	}
	return $entry;
}

function set_select_optgroup($selection, $selected)
{
	if (!is_array($selection)) {
		return false;
	}

	$entry = null;
	foreach ($selection as $group => $arr) {
		$entry .= sprintf('<optgroup label="%s">', $group);
		$entry .= set_select_option($arr, $selected);
		$entry .= '</optgroup>';
	}
	return $entry;
}

