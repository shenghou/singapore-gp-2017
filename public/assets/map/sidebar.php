<div id="sidebar-wrapper">
  <div id="sidebar">
    <div id="tab-container" class="tab-container">
      <ul class='etabs'>
        <li class='tab legend'><a href="#legends" class="hiddenTab">Legends</a></li>
        <li class='tab zone'><a href="#zones" class="hiddenTab">Zones</a></li> 
      </ul>
      <div id="legends">
        <ul class="ul_container">
          <li class="legend-label"></li>
          <li class="no-sprite no-hover" id="fnb-icon"              ><div class="icon-wrapper"><img src="images2/icons/fnb.png"></div><div class="label-wrapper">Food & Beverage</div></li>
          <li class="no-sprite no-hover" id="merchandise-icon"      ><div class="icon-wrapper"><img src="images2/icons/merchandise.png"></div><div class="label-wrapper">Merchandise</div></li>
          <li class="no-sprite no-hover" id="f1-village-icon"       ><div class="icon-wrapper"><img src="images2/icons/f1village.png"></div><div class="label-wrapper">F1 Village</div></li>
          <li class="no-sprite no-hover" id="atm-icon"              ><div class="icon-wrapper"><img src="images2/icons/atm.png"></div><div class="label-wrapper">ATM</div></li>
          <li class="no-sprite no-hover" id="first-aid-icon"        ><div class="icon-wrapper"><img src="images2/icons/firstaid.png"></div><div class="label-wrapper">First Aid</div></li>
          <li class="no-sprite no-hover" id="information-icon"      ><div class="icon-wrapper"><img src="images2/icons/info.png"></div><div class="label-wrapper">Information / Lost & Found</div></li>
          <li class="no-sprite no-hover" id="taxi-icon"             ><div class="icon-wrapper"><img src="images2/icons/taxi.png"></div><div class="label-wrapper">Taxi Stand</div></li>
          <li class="no-sprite no-hover" id="ticketing-booth-icon"  ><div class="icon-wrapper"><img src="images2/icons/turns.png"></div><div class="label-wrapper">Turns</div></li>
            <li class="no-sprite no-hover" id="ticketing-booth-icon"  ><div class="icon-wrapper"><img src="images2/icons/ticketbooth.png"></div><div class="label-wrapper">Ticketing Booth</div></li>
            <li class="no-sprite no-hover" id="gate-icon"       ><div class="icon-wrapper"><img src="images2/icons/gate.png"></div><div class="label-wrapper">Gate</div></li>
            <li class="no-sprite no-hover" id="staff-gate-icon"       ><div class="icon-wrapper"><img src="images2/icons/staffgate.png"></div><div class="label-wrapper">Staff Gate</div></li>
            <li class="no-sprite no-hover" id="toilet"       ><div class="icon-wrapper"><img src="images2/icons/toilet.png"></div><div class="label-wrapper">Toilet</div></li>
            <li class="no-sprite no-hover" id="handicap-toilet"       ><div class="icon-wrapper"><img src="images2/icons/handicaptoilet.png"></div><div class="label-wrapper">Handicap Toilet</div></li>
          <!--<li class="legend-label2">Keys</li>-->
            <li class="no-sprite no-hover" id="stage"      ><div class="icon-wrapper"><img src="images2/icons/stage.png"></div><div class="label-wrapper">Stages</div></li>
            <li class="no-sprite no-hover" id="grandstandsuites-platform-icon"      ><div class="icon-wrapper"><img src="images2/icons/grandstandsuites.png"></div><div class="label-wrapper">Grandstand Suites(Group)</div></li>
            <li class="no-sprite no-hover" id="suites-icon"><div class="icon-wrapper"><img src="images2/icons/suites.png"/></div><div class="label-wrapper">Suites</div></li>
            <li class="no-sprite no-hover" id="wheelchair"      ><div class="icon-wrapper"><img src="images2/icons/wheelchair.png"></div><div class="label-wrapper">Wheelchair Platform</div></li>
            <li class="no-sprite no-hover" id="mrt-icon"><div class="icon-wrapper"><img style="width: 12px;height: 13px;"src="images2/icons/mrt.png"/></div>             <div class="label-wrapper">MRT</div></li>
		  <li class="no-sprite no-hover" id="paddock-club-icon"><div class="icon-wrapper"><img style="height: 20px;width: 21px;margin-left: 4px;margin-top: 3px;" src="images2/icons/paddock-club.png"/></div>                                       <div class="label-wrapper">Paddock Club</div></li>
          <li class="no-sprite no-hover" id="support-paddock-icon"><div class="icon-wrapper"><img class="unglow" src="images2/icons/supportpaddock.png"/></div>          <div class="label-wrapper">Support Paddock</div></li>
            <li class="no-sprite no-hover" id="podium"><div class="icon-wrapper"><img class="icon-wrappew" src="images2/icons/podium.png"/></div>            <div class="label-wrapper">Podium</div></li><?php //@TODO: Check id, paddock-club-l-icon ? ?>
          
          <!-- <li id="walking-1-icon"><div class="icon-wrapper"><img src="images2/icons/wheelchair.png"/><span>Walking Route from Zone 1 to Zone 4</span></li>
          <li id="walking-2-icon"><div class="icon-wrapper"><img src="images2/icons/wheelchair.png"/><span>Alternate One Way Route from Zone 1 to the Padang Stage in Zone 4</span></li>-->
        </ul>

      </div>
      <div id="zones">
        <ul class="ul_container">
          <li class="zone-header"></li>
          <li class="clearfix no-hover" id="zone-1"><div class="icon-wrapper"><img src="images2/icons/zone1.png"/></div>     <div class="label-wrapper">Zone 1</div></li>
          <li class="clearfix no-hover" id="zone-2"><div class="icon-wrapper"><img src="images2/icons/zone2.png"/></div>     <div class="label-wrapper">Zone 2</div></li>
          <li class="clearfix no-hover" id="zone-3"><div class="icon-wrapper"><img src="images2/icons/zone3.png"/></div> <div class="label-wrapper">Zone 3</div></li>
          <li class="clearfix no-hover" id="zone-4"><div class="icon-wrapper"><img src="images2/icons/zone4.png"/></div>     <div class="label-wrapper">Zone 4 / Zone 4 Walkabout</div></li>
          <li class="clearfix no-hover" id="premiere-walkabout-zone"><div class="icon-wrapper"><img src="images2/icons/premier.png"/></div>     <div class="label-wrapper">Premier Walkabout Zone</div></li>
          <li class="clearfix no-hover" id="fan-zone"><div class="icon-wrapper"><img src="images2/icons/fanzone.png"/></div>   <div class="label-wrapper">Fan Zone</div></li>
          <li class="clearfix no-hover" id="paddock-club-zone"><div class="icon-wrapper"><img src="images2/icons/paddock.png"/></div><div class="label-wrapper">Paddock Club Zone</div></li>
          <li class="clearfix no-hover" id="restricted-zone"><div class="icon-wrapper"><img src="images2/icons/restricted.png"/></div><div class="label-wrapper">Restricted Zone</div></li>
          <li class="clearfix no-hover" id="performance-area-icon"><div class="icon-wrapper"><img src="images2/icons/performance.png"/></div>   <div class="label-wrapper">Performance Area</div></li>
          
          <!--<li class="zone-header2"></li>-->
          <li class="clearfix no-hover" id="start-icon">                <div class="icon-wrapper"><img src="images2/icons/start.png"/></div>           <div class="label-wrapper">Start</div></li>
          <li class="clearfix no-hover" id="finish-icon">               <div class="icon-wrapper"><img src="images2/icons/finish.png"/></div>          <div class="label-wrapper">Finish</div></li>
          <li class="clearfix no-hover" id="race-directions-icon">      <div class="icon-wrapper"><img src="images2/icons/racedirection.png"/></div>   <div class="label-wrapper">Race Direction</div></li>
          <li class="clearfix no-hover" id="track-turn-icon">           <div class="icon-wrapper"><img src="images2/icons/trackturn.png"/></div>       <div class="label-wrapper">Track Turn</div></li>
          <li class="clearfix no-hover" id="superscreen-icon">          <div class="icon-wrapper"><img src="images2/icons/superscreen.png"/></div>     <div class="label-wrapper">Superscreen</div></li>
          <li class="clearfix no-hover" id="gate-icon">                 <div class="icon-wrapper"><img src="images2/icons/gate.png"/></div>            <div class="label-wrapper">Gate</div></li>
          <li class="clearfix no-hover" id="overpass-icon">             <div class="icon-wrapper"><img src="images2/icons/overpass.png"/></div>        <div class="label-wrapper">Overpass</div></li>
          <li class="clearfix no-hover" id="underpass-icon">            <div class="icon-wrapper"><img src="images2/icons/underpass.png"/></div>       <div class="label-wrapper">Underpass</div></li>
          <li class="clearfix no-hover" id="viewing-platform-icon">     <div class="icon-wrapper"><img src="images2/icons/viewingplatform.png"/></div> <div class="label-wrapper">Viewing Platform</div></li>
          <li class="clearfix no-hover" id="wheelchair-platform-icon">  <div class="icon-wrapper"><img src="images2/icons/wheelchair.png"/></div>      <div class="label-wrapper">Wheelchair Platform</div></li>
        </ul>
      </div>
    </div>
    <div id="sidebar-controls-wrapper">
      <div id="btn-zoom-in" class="control">
        <div class="label">Zoom in</div>
      </div>
      <div id="btn-zoom-out" class="control not_clickable">
        <div class="label">Zoom out</div>
      </div>
      <div id="btn-show-sidebar" class="control">
        <div class="label">Map legend</div>
      </div>
    </div>
  </div>
</div>