<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Club/Sky Suites</title>

<!-- First, add jQuery (and jQuery UI if using custom easing or animation -->
<script src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.0.custom.min.js"></script>

<!-- Second, add the Timer and Easing plugins -->
<script type="text/javascript" src="js/jquery.timers-1.2.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>

<!-- Third, add the GalleryView Javascript and CSS files -->
<script type="text/javascript" src="js/jquery.galleryview-3.0-dev.js"></script>
<link type="text/css" rel="stylesheet" href="css/jquery.galleryview-3.0-dev.css" />

<!-- Lastly, call the galleryView() function on your unordered list(s) -->
<script type="text/javascript" src="js/gallerypage.js"></script>

<link type="text/css" rel="stylesheet" href="css/gallerypage.css" />
</head>
<?php

// Turn off all error reporting
error_reporting(0);
?>

<body>
<?php

$g = $_GET['g'];
$gurl = 'images2/' . $g . '/captions.xml';
$gxml = simplexml_load_file($gurl);

$s = $_GET['s'];
$surl = 'images2/' . $s . '/captions.xml';
$sxml = simplexml_load_file($surl);

$c = $_GET['c'];
$curl = 'images2/' . $c . '/captions.xml';
$cxml = simplexml_load_file($curl) 
      
?>
	<ul id="myGallery">
	
	<li><img data-frame="images2/<?php echo $g?>/1.jpg" src="images2/<?php echo $_GET['g']?>/1.jpg" data-description="<?php echo $gxml->captions->caption[0]->text;?>" /></li>
    <li><img data-frame="images2/<?php echo $g?>/2.jpg" src="images2/<?php echo $_GET['g']?>/2.jpg" data-description="<?php echo $gxml->captions->caption[1]->text;?>" /></li>
    <li><img data-frame="images2/<?php echo $g?>/3.jpg" src="images2/<?php echo $_GET['g']?>/3.jpg" data-description="<?php echo $gxml->captions->caption[2]->text;?>" /></li>
	<!--<li><img data-frame="images2/<?php echo $g?>/4.jpg" src="images2/<?php echo $_GET['g']?>/4.jpg" data-description="<?php echo $gxml->captions->caption[3]->text;?>" /></li>
	<li><img data-frame="images2/<?php echo $g?>/5.jpg" src="images2/<?php echo $_GET['g']?>/5.jpg" data-description="<?php echo $gxml->captions->caption[4]->text;?>" /></li>
	<li><img data-frame="images2/<?php echo $g?>/6.jpg" src="images2/<?php echo $_GET['g']?>/6.jpg" data-description="<?php echo $gxml->captions->caption[5]->text;?>" /></li>-->
<!--	<li><img data-frame="images2/<?php echo $g?>/7.jpg" src="images2/<?php echo $_GET['g']?>/7.jpg" data-description="<?php echo $gxml->captions->caption[5]->text;?>" /></li> 

	<li><img data-frame="images2/<?php echo $c?>/1.jpg" src="images2/<?php echo $_GET['c']?>/1.jpg" data-description="<?php echo $cxml->captions->caption[0]->text;?>" /></li>
    <li><img data-frame="images2/<?php echo $c?>/2.jpg" src="images2/<?php echo $_GET['c']?>/2.jpg" data-description="<?php echo $cxml->captions->caption[1]->text;?>" /></li>
    <li><img data-frame="images2/<?php echo $c?>/3.jpg" src="images2/<?php echo $_GET['c']?>/3.jpg" data-description="<?php echo $cxml->captions->caption[2]->text;?>" /></li>
	<li><img data-frame="images2/<?php echo $c?>/4.jpg" src="images2/<?php echo $_GET['c']?>/4.jpg" data-description="<?php echo $cxml->captions->caption[3]->text;?>" /></li>
	<li><img data-frame="images2/<?php echo $c?>/5.jpg" src="images2/<?php echo $_GET['c']?>/5.jpg" data-description="<?php echo $cxml->captions->caption[4]->text;?>" /></li>
	<li><img data-frame="images2/<?php echo $c?>/6.jpg" src="images2/<?php echo $_GET['c']?>/6.jpg" data-description="<?php echo $cxml->captions->caption[5]->text;?>" /></li>
	<li><img data-frame="images2/<?php echo $c?>/7.jpg" src="images2/<?php echo $_GET['c']?>/7.jpg" data-description="<?php echo $cxml->captions->caption[5]->text;?>" /></li>
	
	<li><img data-frame="images2/<?php echo $s?>/1.jpg" src="images2/<?php echo $_GET['s']?>/1.jpg" data-description="<?php echo $sxml->captions->caption[0]->text;?>" /></li>
    <li><img data-frame="images2/<?php echo $s?>/2.jpg" src="images2/<?php echo $_GET['s']?>/2.jpg" data-description="<?php echo $sxml->captions->caption[1]->text;?>" /></li>
    <li><img data-frame="images2/<?php echo $s?>/3.jpg" src="images2/<?php echo $_GET['s']?>/3.jpg" data-description="<?php echo $sxml->captions->caption[2]->text;?>" /></li>
	<li><img data-frame="images2/<?php echo $s?>/4.jpg" src="images2/<?php echo $_GET['s']?>/4.jpg" data-description="<?php echo $sxml->captions->caption[3]->text;?>" /></li>
	<li><img data-frame="images2/<?php echo $s?>/5.jpg" src="images2/<?php echo $_GET['s']?>/5.jpg" data-description="<?php echo $sxml->captions->caption[4]->text;?>" /></li>
	<li><img data-frame="images2/<?php echo $s?>/6.jpg" src="images2/<?php echo $_GET['s']?>/6.jpg" data-description="<?php echo $sxml->captions->caption[5]->text;?>" /></li>-->
        </ul>
		<div class="gallery-submenu">
		<ul>
		<li><a href="#" id="seating-plan">Floor Plan<br />& Zone Access</a></li>
		</ul>
		</div>
		<div class="gallery-seating-plan">
		<img src="images2/<?php echo $g?>/seating-plan.jpg" />
		</div>
</body>
</html>
