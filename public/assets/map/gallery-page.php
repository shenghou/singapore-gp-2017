<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bay Grandstand</title>

<!-- First, add jQuery (and jQuery UI if using custom easing or animation -->
<script src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.0.custom.min.js"></script>

<!-- Second, add the Timer and Easing plugins -->
<script type="text/javascript" src="js/jquery.timers-1.2.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>

<!-- Third, add the GalleryView Javascript and CSS files -->
<script type="text/javascript" src="js/jquery.galleryview-3.0-dev.js"></script>
<link type="text/css" rel="stylesheet" href="css/jquery.galleryview-3.0-dev.css" />

<!-- Lastly, call the galleryView() function on your unordered list(s) -->
<script type="text/javascript" src="js/gallerypage.js"></script>

<link type="text/css" rel="stylesheet" href="css/gallerypage.css" />
</head>
<?php

// Turn off all error reporting
error_reporting(0);
?>

<body>
<?php
$g = $_GET['g'];
$gurl = 'images2/' . $g . '/captions.xml';
$xml = simplexml_load_file($gurl);
echo '<script>console.log("$gurl=","'.$gurl.'");</script>';

?>
  <ul id="myGallery">
    <?php echo '<script>console.log("$g=","'.$g.'");</script>'; ?>
    <?php /*if ($g == 'grandstand-connaught'): ?>
    <li><img data-frame="images2/<?php echo $g?>/pano.jpg" src="images2/<?php echo $g; ?>/pano.jpg" data-description="" /></li>
    <?php endif;*/ ?>
    <li><img data-frame="images2/<?php echo $g?>/1.jpg" src="images2/<?php echo $g; ?>/1.jpg" data-description="<?php echo $xml->captions->caption[0]->text;?>" /></li>
    <li><img data-frame="images2/<?php echo $g?>/2.jpg" src="images2/<?php echo $g; ?>/2.jpg" data-description="<?php echo $xml->captions->caption[1]->text;?>" /></li>
    <?php $filename = ("images2/".$g."/3.jpg");
    if (file_exists($filename)) { ?>
    <li><img data-frame="images2/<?php echo $g?>/3.jpg" src="images2/<?php echo $g; ?>/3.jpg" data-description="<?php echo $xml->captions->caption[2]->text;?>" /></li>
    <?php } else {
      //echo "The file $filename does not exist";
    } ?>
    <?php $filename = ("images2/".$g."/4.jpg");
    if (file_exists($filename)) { ?>
    <li><img data-frame="images2/<?php echo $g?>/4.jpg" src="images2/<?php echo $g; ?>/4.jpg" data-description="<?php echo $xml->captions->caption[3]->text;?>" /></li>
    <?php } else {
      //echo "The file $filename does not exist";
    } ?>
    <?php 
    $filename = ("images2/".$g."/5.jpg");
    if (file_exists($filename)) { 
      ?><li><img data-frame="<?php echo $filename; ?>" src="<?php echo $filename; ?>" data-description="<?php echo $xml->captions->caption[4]->text; ?>" /></li><?php
    } else {
      //echo "The file $filename does not exist";
    }
    
    $filename = ("images2/".$g."/6.jpg");
    if (file_exists($filename)) {
      ?><li><img data-frame="<?php echo $filename; ?>" src="<?php echo $filename; ?>" data-description="<?php echo $xml->captions->caption[5]->text; ?>" /></li><?php
    } else {
      //echo "The file $filename does not exist";
    }
    
    $filename = ("images2/".$g."/7.jpg");
    if (file_exists($filename)) {
      ?><li><img data-frame="<?php echo $filename; ?>" src="<?php echo $filename; ?>" data-description="<?php echo $xml->captions->caption[6]->text; ?>" /></li><?php
    } else {
      //echo "The file $filename does not exist";
    }
  $filename = ("images2/".$g."/8.jpg");
    if (file_exists($filename)) {
      ?><li><img data-frame="<?php echo $filename; ?>" src="<?php echo $filename; ?>" data-description="<?php echo $xml->captions->caption[7]->text; ?>" /></li><?php
    } else {
      //echo "The file $filename does not exist";
    }
    ?>
    ?>
  

  </ul>
  <div class="panorama-overlay">
  </div>
  <div class="gallery-submenu">
  <ul>
    <li><a href="#" id="seating-plan">Seating Plan<br />& Zone Access</a></li>
  </ul>
  </div>
  <div class="gallery-seating-plan">
    <img src="images2/<?php echo $g?>/seating-plan.jpg" />
  </div>
</body>
</html>
