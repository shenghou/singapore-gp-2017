$(function(){

$.fn.scrollTo = function( target, options, callback ){
  if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
  var settings = $.extend({
    scrollTarget  : target,
    offsetTop     : 150,
    duration      : 500,
    easing        : 'swing'
  }, options);
  return this.each(function(){
    var scrollPane = $(this);
    var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
    var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
    scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
      if (typeof callback == 'function') { callback.call(this); }
    });
  });
}

$("a.zone1link").live('click',function(){ 
		linkzone(1);
		return false;
    });
	
$("a.zone2link").live('click',function(){ 
		linkzone(2);
		return false;
    });

$("a.zone3link").live('click',function(){ 
		linkzone(3);
		return false;
    });
	
$("a.zone4link").live('click',function(){ 
		linkzone(4);
		return false;
    });
	
$("a.combinationp").live('click',function(){ 
		window.open("http://www.singaporegp.sg/ticket/ticket-details.php?category_id=4", '_blank');
//		linkload(4);
		return false;
    });

$("a.singlesunday").live('click',function(){ 
		window.open("http://www.singaporegp.sg/ticket/ticket-details.php?category_id=5", '_blank');
//		linkload(5);
		return false;
    });
	
		function linkload(lno)
	{
	var linkstr1 = "link-text.php?l=" + lno + " #link-text";
	$('div.link-text').empty();
	$('div.link-text').load(linkstr1);
	$('div.gallery-text-container').scrollTo('#link-start');
	};
	
	function linkzone(zno)
	{
	var linkzonestr = "#zone-" + zno + "-overlay";
	$(linkzonestr).fadeIn('slow');
//	var linkzoneopen = window.open(linkzonestr, '', 'height=400, width=500');
//		if (window.focus)
//			{
//				linkzoneopen.focus();
//				}
//	$(this).target = "_blank";
 //       window.open(linkzonestr);
        return false;
		};

	});