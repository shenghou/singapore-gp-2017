	$(function(){
	$('#myGallery').galleryView({
    frame_width: 8,
    frame_height: 8,
    frame_gap: 22,
	panel_width: 590, 				//INT - width of gallery panel (in pixels)
	panel_height: 390,
	enable_overlays: true,
    pan_images: true,
	show_panel_nav: false,
    show_filmstrip_nav: false,
    show_infobar: false,
	filmstrip_size: 4, 				//INT - number of frames to show in filmstrip-only gallery
		filmstrip_style: 'scroll', 		//STRING - type of filmstrip to use (scroll = display one line of frames, scroll filmstrip if necessary, showall = display multiple rows of frames if necessary)
		filmstrip_position: 'left', 	//STRING - position of filmstrip within gallery (bottom, top, left, right)
		frame_width: 110, 				//INT - width of filmstrip frames (in pixels)
		frame_height: 76, 				//INT - width of filmstrip frames (in pixels)
		frame_opacity: 0.5, 
});
$('div.gv_overlay').css({'left' : '0px', 'bottom' : '0px'});
$('div.gv_galleryWrap').css('height','422px');
$('div.gv_filmstripWrap').css({'height' : '370px', 'width' : '116px'});
$('div.gv_gallery').css('height','422px');
$('div.gv_panelWrap').css('height','422px');
$('div.gallery-seating-plan-combo').css('width','900px');
$('#seating-plan').live('click',function(){ 
	$('div.gallery-seating-plan-combo').fadeToggle('fast');
	$('div.gv_panelWrap').fadeToggle('fast');
	$('div.gallery-bg-2', window.parent.document).fadeToggle('fast');
	$('div.gallery-bg-2-2', window.parent.document).fadeToggle('fast');
	$('div.gallery-bg-3', window.parent.document).fadeToggle('fast');
	$('div.gallery-seating-plan', window.parent.document).fadeToggle('fast');
	return false;
	
//	$('div.gallery-bg-2', window.parent.document).css({'background-image': 'url("images2/bg-gallery-2-2.png")', 'width' : '979px'});
//	$('div.gallery-bg-3', window.parent.document).fadeOut('fast');
});
$("div.gv_frame, div.gallery-seating-plan-combo").live('click',function(){ 
	$('div.gallery-seating-plan-combo').fadeOut('fast');
	$('div.gallery-bg-2-2', window.parent.document).fadeOut('fast');
	$('div.gallery-bg-2', window.parent.document).fadeIn('fast');
	$('div.gallery-bg-3', window.parent.document).fadeIn('fast');
	$('div.gv_panelWrap').fadeIn('fast');
});


});
