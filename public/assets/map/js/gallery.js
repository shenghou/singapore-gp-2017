
$(function(){

function galleryload() {
$('#gallery-overlay').fadeIn('slow');
    $('div.gallery-bg-1').css('display', 'block');
    $('div.gallery-bg-1').animate({ opacity: 0 }, 0);
    $('div.gallery-bg-1').animate({ opacity: 1, top: "86px" }, 'slow');
    $('div.gallery-bg-2').css('display', 'block');
    $('div.gallery-bg-2').animate({ opacity: 0 }, 0);
    $('div.gallery-bg-2').delay(500).animate({ opacity: 1, left: "143px" }, 'slow');
    $('div.gallery-bg-3').css('display', 'block');
    $('div.gallery-bg-3').animate({ opacity: 0 }, 0);
    $('div.gallery-bg-3').delay(1000).animate({ opacity: 1, left: "790px",top:"44px" }, 'slow');
    $('div.gallery-text').css('display', 'block');
    $('div.gallery-text').animate({ opacity: 0 }, 0);
    $('div.gallery-text').delay(2500).animate({ opacity: 1}, 'slow');
    $('div.gallery-text-container').css('display', 'block');
    $('div.gallery-text-container').animate({ opacity: 0 }, 0);
    $('div.gallery-text-container').delay(2500).animate({ opacity: 1}, 'slow');
    $('div.link-text').css('display', 'block');
    $('div.link-text').animate({ opacity: 0 }, 0);
    $('div.link-text').delay(2500).animate({ opacity: 1}, 'slow');
    $('div.gallery-title').delay(1200).fadeIn('slow');
         $('#thisGallery').delay(2000).fadeIn('slow');
     };

$('#trispan-lifestyle-area').bind('touchstart click', function(){
    fullgalleryloadstage('trispan-lifestyle-area', 28);
    return false;
    });
  

  
  $('#grandstand-bay').bind('touchstart click', function(){
    fullgalleryload('grandstand-bay', 0);
    return false;
    });
  
  $('#grandstand-esplanade').bind('touchstart click', function(){
    fullgalleryloadwaterfront('grandstand-esplanade-waterfront', 1);
    return false;
    });
  
  $('#grandstand-connaught').bind('touchstart click', function(){
    fullgalleryload('grandstand-connaught', 2);
    return false;
    });
  $('#grandstand-padang-1').bind('touchstart click', function(){
    // @TODO: Caption not showing
    fullgalleryload('grandstand-padang-left', 3);
    return false;
  });
  
  $('#grandstand-padang-2').bind('touchstart click', function(){
    // @TODO: Caption not showing
    fullgalleryload('grandstand-padang-right', 3);
    return false;
  });
  
  $('#grandstand-stamford').bind('touchstart click', function(){
    fullgalleryload('grandstand-stamford', 4);
    return false;
    });
  
  $('#grandstand-turn-7').bind('touchstart click', function(){
    fullgalleryloadturn7('grandstand-turn-7', 5);
    return false;
    });
  
  $('#grandstand-turn-3').bind('touchstart click', function(){
    fullgalleryload('grandstand-turn-3', 6);
    return false;
    });
  
  $('#grandstand-turn-2').bind('touchstart click', function(){
    fullgalleryload('grandstand-turn-2', 7);
    return false;
    });
  
  $('#grandstand-turn-1').bind('touchstart click', function(){
    fullgalleryload('grandstand-turn-1', 8);
    return false;
    });
  
  $('#grandstand-pit').bind('touchstart click', function(){
    fullgalleryload('grandstand-pit', 9);
    return false;
    });

  $('#super-pit').bind('touchstart click', function(){
    fullgalleryload('super-pit', 33);
    return false;
    });


  $('#grandstand-fanstand').bind('touchstart click', function(){
    fullgalleryload2('grandstand-fanstand', 27);
    return false;
    });
  
  $('#stage-sail').bind('touchstart click', function(){
    fullgalleryloadstage('stage-sail', 29);
    return false;
    });
  $('#stage-coyote').bind('touchstart click', function(){
    fullgalleryloadstage('stage-coyote', 30);
    return false;
    });
  $('#stage-mambo').bind('touchstart click', function(){
    fullgalleryloadstage('stage-mambo', 31);
    return false;
    });
    
  $('#stage-village').bind('touchstart click', function(){
    fullgalleryloadstage('stage-village', 10);
    return false;
    });
  
  $('#stage-paddock-club-lifestyle').bind('touchstart click', function(){
    fullgalleryloadstage('stage-paddock-club-lifestyle', 11);
    return false;
    });
  
  $('#stage-marina-pit-lifestyle-stage').bind('touchstart click', function(){
    fullgalleryloadstage('stage-marina-pit-lifestyle-stage', 12);
    return false;
    });
	
  $('#stage-marina-pit-lifestyle-area').bind('touchstart click', function(){
    fullgalleryloadstage('stage-marina-pit-lifestyle-area', 12);
    return false;
    });
  
  $('#stage-esplanade-outdoor-theatre').bind('touchstart click', function(){
    fullgalleryloadstageesp('stage-esplanade-outdoor-theatre', 13);
    return false;
    });
  
  $('#stage-esplanade-park').bind('touchstart click', function(){
    fullgalleryloadstage('stage-esplanade-park', 14);
    return false;
    });
  
  $('#stage-padang').bind('touchstart click', function(){
    fullgalleryloadstagepadang('stage-padang', 15);
    return false;
    });
  
  $('#suites-the-green-room').bind('touchstart click', function(){
    fullgallerygreenroom('suites-the-green-room', 'suites-sky', 'suites-club', 16);
    return false;
    });
  
  $('#suites-turn-2').bind('touchstart click', function(){
    fullgalleryloadclub('suites-club', 18);
    return false;
    });
  
  $('#suites-turn-1').bind('touchstart click', function(){
    fullgalleryloadsky('suites-sky', 19);
    return false;
    });
  
  $('#suites-pit').bind('touchstart click', function(){
    fullgalleryloadsky('suites-sky', 19);
    return false;
    });
  
  $('#handicap-marina-pit').bind('touchstart click', function(){
    fullgalleryloadnp('handicap-marina-pit', 20);
    return false;
    });
  
  $('#handicap-empress-place').bind('touchstart click', function(){
    fullgalleryloadempress('handicap-empress-place', 21);
    return false;
    });
  
  $('#club-paddock').bind('touchstart click', function(){
    fullgalleryloadpaddock('club-paddock', 22);
    return false;
    });
	
   $('#club-paddock-icon').bind('touchstart click', function(){
    fullgalleryloadpaddock('club-paddock-icon', 22);
    return false;
    });
  
  $("a.combo-bay").live('click',function(){ 
    combogalleryload('combo-bay', 23);
    return false;
    });
  
  $("a.combo-padang").live('click',function(){ 
    combogalleryload('combo-padang', 24);
    return false;
    });
  
  $("a.combo-turn-1").live('click',function(){ 
    combogalleryload('combo-turn-1', 25);
    return false;
    });
  
  $("a.combo-zone-1").live('click',function(){ 
    combogalleryload('combo-zone-1', 26);
    return false;
    });
  
  $("a.sky-suites-link").live('click',function(){
    skysuitesload();
    return false;
    });
  
  $("a.club-suites-link").live('click',function(){ 
    clubsuitesload();
    return false;
    });
  
  $("a.green-room-link").live('click',function(){ 
    greenroomload();
    return false;
    });


  $("a.pit-grandstand-link").live('click',function(){
    pitgrandstandload();
    return false;
  });

  $("a.super-pit-link").live('click',function(){
    superpitload();
    return false;
  });

  $("a.closegallery").live('click',function(){ 
    closegallery();
    return false;
    });
  
  $("#gallery-close").live('click',function(){ 
    closegallery();
    return false;
    });
  
  $("#gallery-close-2").live('click',function(){ 
    closegallery();
    return false;
    });

  $('#turn-1-wheel-chair').bind('touchstart click', function(){
    fullgalleryload('turn-1-wheel-chair', 32);
    return false;
    });
  
  function fullgalleryload(gname, gno)
  {
  var loadstr1 = "gallery-text.php?t=" + gno + " #gallery-text";
  var loadstr2 = "gallery-text.php?t=" + gno + " #gallery-title";
  var loadstr3 = '<iframe src="gallery-page.php?g=' + gname + '" scrolling="no" id="gallery-frame"></iframe>';
  $('div.gallery-text').load(loadstr1);
    $('div.gallery-title').load(loadstr2);
    $('#thisGallery').append(loadstr3);
    galleryload();
  };
  
    function fullgalleryload2(gname, gno)
  {
  var loadstr1 = "gallery-text.php?t=" + gno + " #gallery-text";
  var loadstr2 = "gallery-text.php?t=" + gno + " #gallery-title";
  var loadstr3 = '<iframe src="gallery-page2.php?g=' + gname + '" scrolling="no" id="gallery-frame"></iframe>';
  $('div.gallery-text').load(loadstr1);
    $('div.gallery-title').load(loadstr2);
    $('#thisGallery').append(loadstr3);
    galleryload();
  };
  
  function fullgalleryloadpaddock(gname, gno)
  {
  var loadstr1 = "gallery-text.php?t=" + gno + " #gallery-text";
  var loadstr2 = "gallery-text.php?t=" + gno + " #gallery-title";
  var loadstr3 = '<iframe src="gallery-page-paddock-club.php?g=' + gname + '" scrolling="no" id="gallery-frame"></iframe>';
  $('div.gallery-text').load(loadstr1);
    $('div.gallery-title').load(loadstr2);
    $('#thisGallery').append(loadstr3);
    galleryload();
  };
  
  function fullgalleryloadnp(gname, gno)
  {
  var loadstr1 = "gallery-text.php?t=" + gno + " #gallery-text";
  var loadstr2 = "gallery-text.php?t=" + gno + " #gallery-title";
  var loadstr3 = '<iframe src="gallery-page-np.php?g=' + gname + '" scrolling="no" id="gallery-frame"></iframe>';
  $('div.gallery-text').load(loadstr1);
    $('div.gallery-title').load(loadstr2);
    $('#thisGallery').append(loadstr3);
    galleryload();
  };
  
  function fullgalleryloadempress(gname, gno)
  {
  var loadstr1 = "gallery-text.php?t=" + gno + " #gallery-text";
  var loadstr2 = "gallery-text.php?t=" + gno + " #gallery-title";
  var loadstr3 = '<iframe src="gallery-page-empress-place.php?g=' + gname + '" scrolling="no" id="gallery-frame"></iframe>';
  $('div.gallery-text').load(loadstr1);
    $('div.gallery-title').load(loadstr2);
    $('#thisGallery').append(loadstr3);
    galleryload();
  };
  
  function fullgalleryloadturn7(gname, gno)
  {
  var loadstr1 = "gallery-text.php?t=" + gno + " #gallery-text";
  var loadstr2 = "gallery-text.php?t=" + gno + " #gallery-title";
  var loadstr3 = '<iframe src="gallery-page.php?g=' + gname + '" scrolling="no" id="gallery-frame"></iframe>';
  $('div.gallery-text').load(loadstr1);
    $('div.gallery-title-2').load(loadstr2);
    $('#thisGallery').append(loadstr3);
    galleryload();
    $('div.gallery-title-2').delay(1200).fadeIn('slow');
  };
  
  function fullgalleryloadstage(gname, gno)
  {
  var loadstr1 = "gallery-text.php?t=" + gno + " #gallery-text";
  var loadstr2 = "gallery-text.php?t=" + gno + " #gallery-title";
  var loadstr3 = '<iframe src="gallery-page-stage.php?g=' + gname + '" scrolling="no" id="gallery-frame"></iframe>';
  $('div.gallery-text').load(loadstr1);
    $('div.gallery-title').load(loadstr2);
    $('#thisGallery').append(loadstr3);
    galleryload();
  };
  
  function fullgalleryloadstagepadang(gname, gno)
  {
  var loadstr1 = "gallery-text.php?t=" + gno + " #gallery-text";
  var loadstr2 = "gallery-text.php?t=" + gno + " #gallery-title";
  var loadstr3 = '<iframe src="gallery-page-stage-padang.php?g=' + gname + '" scrolling="no" id="gallery-frame"></iframe>';
  $('div.gallery-text').load(loadstr1);
    $('div.gallery-title').load(loadstr2);
    $('#thisGallery').append(loadstr3);
    galleryload();
  };
  
  function fullgalleryloadstageesp(gname, gno)
  {
  var loadstr1 = "gallery-text.php?t=" + gno + " #gallery-text";
  var loadstr2 = "gallery-text.php?t=" + gno + " #gallery-title";
  var loadstr3 = '<iframe src="gallery-page-stage.php?g=' + gname + '" scrolling="no" id="gallery-frame"></iframe>';
  $('div.gallery-text').load(loadstr1);
  $('div.gallery-title-2').load(loadstr2);
  $('#thisGallery').append(loadstr3);
  galleryload();
  $('div.gallery-title-2').delay(1200).fadeIn('slow');
  };
  
  
  function fullgalleryloadwaterfront(gname, gno)
  {
  var loadstr1 = "gallery-text.php?t=" + gno + " #gallery-text";
  var loadstr2 = "gallery-text.php?t=" + gno + " #gallery-title";
  var loadstr3 = '<iframe src="gallery-page.php?g=' + gname + '" scrolling="no" id="gallery-frame"></iframe>';
  $('div.gallery-text').load(loadstr1);
    $('div.gallery-title-2').load(loadstr2);
    $('#thisGallery').append(loadstr3);
    galleryload();
    $('div.gallery-title-2').delay(1200).fadeIn('slow');
  };
  
  function fullgalleryloadsky(sname, sno)
  {
  var loadstr1 = "gallery-text.php?t=" + sno + " #gallery-text";
  var loadstr2 = "gallery-text.php?t=" + sno + " #gallery-title";
  var loadstr3 = '<iframe src="gallery-page-suites-sky.php?s=' + sname + '" scrolling="no" id="gallery-frame"></iframe>';
  $('div.gallery-text').load(loadstr1);
    $('div.gallery-title').load(loadstr2);
    $('#thisGallery').append(loadstr3);
    galleryload();
  };
  
  function fullgalleryloadclub(cname, cno)
  {
  var loadstr1 = "gallery-text.php?t=" + cno + " #gallery-text";
  var loadstr2 = "gallery-text.php?t=" + cno + " #gallery-title";
  var loadstr3 = '<iframe src="gallery-page-suites-club.php?c=' + cname + '" scrolling="no" id="gallery-frame"></iframe>';
  $('div.gallery-text').load(loadstr1);
    $('div.gallery-title').load(loadstr2);
    $('#thisGallery').append(loadstr3);
    galleryload();
  };
  
  
  function fullgalleryloadsuites(sname, cname, sno)
  {
  var loadstr1 = "gallery-text.php?t=" + sno + " #gallery-text";
  var loadstr2 = "gallery-text.php?t=" + sno + " #gallery-title";
  var loadstr3 = '<iframe src="gallery-page-suites.php?s=' + sname + '&c=' + cname + '" scrolling="no" id="gallery-frame"></iframe>';
  $('div.gallery-text').load(loadstr1);
    $('div.gallery-title').load(loadstr2);
    $('#thisGallery').append(loadstr3);
    galleryload();
  };
  
  function fullgallerygreenroom(gname, sname, cname, sno)
  {
  var loadstr1 = "gallery-text.php?t=" + sno + " #gallery-text";
  var loadstr2 = "gallery-text.php?t=" + sno + " #gallery-title";
  var loadstr3 = '<iframe src="gallery-page-green-room-suites.php?g=' + gname + '&s=' + sname + ' scrolling="no" id="gallery-frame"></iframe>';
  $('div.gallery-text').load(loadstr1);
    $('div.gallery-title').load(loadstr2);
    $('#thisGallery').append(loadstr3);
    galleryload();
  };
  
  function combogalleryload(cbname, cbno)
  {
  var loadstrc1 = "gallery-text.php?t=" + cbno + " #gallery-text";
  var loadstrc2 = "gallery-text.php?t=" + cbno + " #gallery-title";
  var loadstrc3 = '<iframe src="gallery-page-combo.php?g=' + cbname + '" scrolling="no" id="combo-frame"></iframe>';
  $('div.gallery-text').fadeOut('fast');
  $('div.link-text').empty();
  $('div.gallery-title').fadeOut('fast');
  $('div.gallery-text-combo').load(loadstrc1);
    $('div.gallery-title-combo').load(loadstrc2);
    $('#thisGallery').fadeOut('fast');
    $('#comboGallery').append(loadstrc3);
    $('div.gallery-text-combo').delay(100).fadeIn('slow');
    $('div.gallery-title-combo').delay(100).fadeIn('slow');
    $('#comboGallery').delay(100).fadeIn('slow');
  };
  
  function greenroomload()
  {
  var loadstr1 = 'gallery-text.php?t=16" #gallery-text';
  $('div.gallery-text').fadeOut('fast');
  $('div.gallery-bg-2-2').fadeOut('fast');
  $('div.gallery-bg-2').fadeIn('fast');
  $('div.gallery-bg-3').fadeIn('fast');
  $('#thisGallery').fadeOut('fast');
  var loadstr3 = '<iframe src="gallery-page-green-room-suites.php?g=suites-the-green-room" scrolling="no" id="gallery-frame"></iframe>';
  $('#thisGallery').empty();
  $('div.gallery-text').load(loadstr1);
  $('#thisGallery').append(loadstr3);
  $('#thisGallery').fadeIn('slow');
  $('div.gallery-text').fadeIn('slow');
  
  }
  function skysuitesload()
  {
  var loadstr1 = 'gallery-text.php?t=19" #gallery-text';
  $('div.gallery-text').fadeOut('fast');
  $('div.gallery-bg-2-2').fadeOut('fast');
  $('div.gallery-bg-2').fadeIn('fast');
  $('div.gallery-bg-3').fadeIn('fast');
  $('#thisGallery').fadeOut('fast');
  var loadstr3 = '<iframe src="gallery-page-suites-sky.php?s=suites-sky" scrolling="no" id="gallery-frame"></iframe>';
  $('#thisGallery').empty();
  $('div.gallery-text').load(loadstr1);
  $('#thisGallery').append(loadstr3);
  $('#thisGallery').fadeIn('slow');
  $('div.gallery-text').fadeIn('slow');
  
  }
  function clubsuitesload()
  {
  var loadstr1 = 'gallery-text.php?t=18" #gallery-text';
  $('div.gallery-text').fadeOut('fast');
  $('div.gallery-bg-2-2').fadeOut('fast');
  $('div.gallery-bg-2').fadeIn('fast');
  $('div.gallery-bg-3').fadeIn('fast');
  $('#thisGallery').fadeOut('fast');
  var loadstr3 = '<iframe src="gallery-page-suites-club.php?c=suites-club" scrolling="no" id="gallery-frame"></iframe>';
  $('#thisGallery').empty();
  $('div.gallery-text').load(loadstr1);
  $('#thisGallery').append(loadstr3);
  $('#thisGallery').fadeIn('slow');
  $('div.gallery-text').fadeIn('slow');
  
  }

  function pitgrandstandload()
  {
    var loadstr1 = 'gallery-text.php?t=9" #gallery-text';
    $('div.gallery-text').fadeOut('fast');
    $('div.gallery-bg-2-2').fadeOut('fast');
    $('div.gallery-bg-2').fadeIn('fast');
    $('div.gallery-bg-3').fadeIn('fast');
    $('#thisGallery').fadeOut('fast');
    var loadstr3 = '<iframe src="gallery-page-super-pit.php?c=grandstand-pit" scrolling="no" id="gallery-frame"></iframe>';
    $('#thisGallery').empty();
    $('div.gallery-text').load(loadstr1);
    $('#thisGallery').append(loadstr3);
    $('#thisGallery').fadeIn('slow');
    $('div.gallery-text').fadeIn('slow');

  }

  function superpitload()
  {
    var loadstr1 = 'gallery-text.php?t=33" #gallery-text';
    $('div.gallery-text').fadeOut('fast');
    $('div.gallery-bg-2-2').fadeOut('fast');
    $('div.gallery-bg-2').fadeIn('fast');
    $('div.gallery-bg-3').fadeIn('fast');
    $('#thisGallery').fadeOut('fast');
    var loadstr3 = '<iframe src="gallery-page-super-pit.php?c=super-pit" scrolling="no" id="gallery-frame"></iframe>';
    $('#thisGallery').empty();
    $('div.gallery-text').load(loadstr1);
    $('#thisGallery').append(loadstr3);
    $('#thisGallery').fadeIn('slow');
    $('div.gallery-text').fadeIn('slow');

  }



  $('#gallery-overlay').click(function () {
    closegallery();
    });
  
  
  
  function closegallery() {
    
    $('div.gallery-bg-3').fadeOut('fast');
    $('div.gallery-text').fadeOut('fast');
    $('div.gallery-bg-2').delay(300).fadeOut('fast');
    $('div.gallery-bg-2-2').delay(300).fadeOut('fast');
    $('div.gallery-bg-1').animate({ opacity: 0, top: "682px" }, 'slow');
    $('div.gallery-bg-1').css('display', 'none');
    $('div.gallery-title').delay(220).fadeOut('fast');
    $('div.gallery-title-2').delay(220).fadeOut('fast');
    $('#gallery-overlay').fadeOut('slow');
        $('#thisGallery').delay(250).fadeOut('slow');
    $('#thisGallery').empty();
    $('#comboGallery').delay(250).fadeOut('slow');
    $('#comboGallery').empty();
    $('div.gallery-text').empty();
    $('div.gallery-title').empty();
    $('div.gallery-title-2').empty();
    $('div.gallery-text-combo').empty();
    $('div.gallery-title-combo').empty();
    $('div.link-text').empty();
    };
  


});