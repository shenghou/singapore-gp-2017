$(function(){


		//Group 
		$('#legends li').mousedown(function(){
			$(this).css("font-weight","bold");
		});

		$('#legends li').mouseout(function(){
			$(this).css("font-weight","normal");
		});

		$('#zones li').mouseover(function(){
			$(this).css("font-weight","bold");
		});

		$('#zones li').mouseout(function(){
			$(this).css("font-weight","normal");
		});

	  $('#start-icon').mouseover(function () {
        // $('#nav-start-text').fadeIn('fast');
		$('#start-glow').fadeIn('fast');
//		$('#start-glow').delay(1500).fadeOut('fast');
      });
	  
	  $('#start-icon').mouseout(function () {
        // $('#nav-start-text, #start-glow').fadeOut(500);
      });
	  
	  $('#finish-icon').mouseover(function () {
        // $('#nav-finish-text').fadeIn('fast');
        $('#finish-glow').fadeIn('fast');
//		$('#finish-glow').delay(1500).fadeOut('fast');
      });
	  
	  $('#finish-icon').mouseout(function () {
        // $('#nav-finish-text, #finish-glow').fadeOut(500);
      });
	  
	  $('#race-directions-icon').mouseover(function () {
        // $('#nav-race-directions-text').fadeIn('fast');
		$('#directions-glow').fadeIn('fast');
//		$('#directions-glow').delay(1500).fadeOut('fast');
      });
	  
	  $('#race-directions-icon').mouseout(function () {
        // $('#nav-race-directions-text, #directions-glow').fadeOut(500);
      });
	  
	  $('#track-turn-icon').mouseover(function () {
        // $('#nav-track-turn-text').fadeIn('fast');
		$('#turn-glow').fadeIn('fast');
      });
	  
	  $('#track-turn-icon').mouseout(function () {
        // $('#nav-track-turn-text, #turn-glow').fadeOut(500);
      });
	  
	  $('#superscreen-icon').mouseover(function () {
        // $('#nav-superscreen-text').fadeIn('fast');
		$('#superscreen-glow').fadeIn('fast');
//		$('#turn-glow').delay(1500).fadeOut('fast');
      });
	  
	  $('#superscreen-icon').mouseout(function () {
        $('#nav-superscreen-text, #superscreen-glow').fadeOut(500);
      });
	  
	  $('#gate-icon').mouseover(function () {
        // $('#nav-gate-text').fadeIn('fast');
		$('#gate-glow').fadeIn('fast');
//		$('#gate-glow').delay(1500).fadeOut('fast');
      });
	  
	  $('#gate-icon').mouseout(function () {
        // $('#nav-gate-text, #gate-glow').fadeOut(500);
      });
	  
	  $('#mrt-icon').mouseover(function () {
        // $('#nav-mrt-text').fadeIn('fast');
		//$('#mrt-glow').fadeIn('fast');
		alert("mrt");
//		$('#mrt-glow').delay(1500).fadeOut('fast');
      });
	  
	  $('#mrt-icon').mouseout(function () {
        // $('#nav-mrt-text, #mrt-glow').fadeOut(500);
      });
	  
	  $('#overpass-icon').mouseover(function () {
        // $('#nav-overpass-text').fadeIn('fast');
		$('#overpass-glow').fadeIn('fast');
//		$('#overpass-glow').delay(1500).fadeOut('fast');
		
      });
	  
	  $('#overpass-icon').mouseout(function () {
        // $('#nav-overpass-text, #overpass-glow').fadeOut(500);
      });
	  
	  $('#underpass-icon').mouseover(function () {
        // $('#nav-underpass-text').fadeIn('fast');
		$('#underpass-glow').fadeIn('fast');
//		$('#underpass-glow').delay(1500).fadeOut('fast');
      });
	  
	  $('#underpass-icon').mouseout(function () {
        // $('#nav-underpass-text, #underpass-glow').fadeOut(500);
      });
	  
	  $('#viewing-platform-icon').mouseover(function () {
        // $('#nav-viewing-platform-text').fadeIn('fast');
		$('#viewing-glow').fadeIn('fast');
//		$('#viewing-glow').delay(1500).fadeOut('fast');
 		$('#viewing-platform-icon').css("font-weight","bold").fadeIn('fast');
      });
	  
	  $('#viewing-platform-icon').mouseout(function () {
        // $('#nav-viewing-platform-text, #viewing-glow').fadeOut(500);
         $('#viewing-platform-icon').css("font-weight","normal").fadeIn('slow');
      });
	  
	  $('#wheelchair-platform-icon').mouseover(function () {
        // $('#nav-wheelchair-platform-text').fadeIn('fast');
		$('#wheelchair-glow').fadeIn('fast');
//		$('#wheelchair-glow').delay(1500).fadeOut('fast');
      });
	  
	  $('#wheelchair-platform-icon').mouseout(function () {
        // $('#nav-wheelchair-platform-text, #wheelchair-glow').fadeOut(500);
      });
	  
	  $('#grandstand-icon').mouseover(function () {
        // $('#nav-grandstand-text').fadeIn('fast');
		$('#grandstand-glow').fadeIn('fast');
//		$('#grandstand-glow').delay(1500).fadeOut('fast');
      });
	  
	  $('#grandstand-icon').mouseout(function () {
        // $('#nav-grandstand-text, #grandstand-glow').fadeOut(500);
      });
	  
	  $('#suites-icon').mouseover(function () {
        // $('#nav-suites-text').fadeIn('fast');
		$('#suite-glow').fadeIn('fast');
//		$('#suites-glow').delay(1500).fadeOut('fast');
      });
	  
	  $('#suites-icon').mouseout(function () {
        // $('#nav-suites-text, #suite-glow').fadeOut(500);
      });
	  
	  $('#paddock-club-icon').mouseover(function () {
        // $('#nav-paddock-club-text').fadeIn('fast');
		$('#paddock-club-glow').fadeIn('fast');
//		$('#paddock-club-glow').delay(1500).fadeOut('fast');
      });
	  
	  $('#paddock-club-icon').mouseout(function () {
        // $('#nav-paddock-club-text, #paddock-club-glow').fadeOut(500);
      });
	  
	  $('#entertainment-stage-icon').mouseover(function () {
        // $('#nav-entertainment-stage-text').fadeIn('fast');
		$('#stage-glow').fadeIn('fast');
//		$('#stage-glow').delay(1500).fadeOut('fast');
      });
	  
	  $('#entertainment-stage-icon').mouseout(function () {
        // $('#nav-entertainment-stage-text, #stage-glow').fadeOut(500);
      });
	  
	  $('#performance-area-icon').mouseover(function () {
        // $('#nav-performance-area-text').fadeIn('fast');
      });
	  
	  $('#performance-area-icon').mouseout(function () {
        // $('#nav-performance-area-text').fadeOut(500);
      });

	  $('#route-1-icon').mouseover(function () {
        // $('#nav-route-1-text').fadeIn('fast');
		$('#route-1-glow').fadeIn('fast');
      });
	  
	  $('#route-1-icon').mouseout(function () {
        // $('#nav-route-1-text').fadeOut(500);
		$('#route-1-glow').fadeOut(500);
      });
	  
	  $('#route-2-icon').mouseover(function () {
        // $('#nav-route-2-text').fadeIn('fast');
		// $('#nav-restrictedzone-text-2').fadeIn('fast');
		$('#route-2-glow').fadeIn('fast');
      });
	  
	  $('#route-2-icon').mouseout(function () {
        // $('#nav-route-2-text').fadeOut(500);
		// $('#nav-restrictedzone-text-2').fadeOut(500);
		$('#route-2-glow').fadeOut(500);
      });
	 
	  $('#f1-village-icon').mouseover(function () {
        // $('#nav-f1-village-text').fadeIn('fast');
      });
	  
	  $('#f1-village-icon').mouseout(function () {
        // $('#nav-f1-village-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#f1-village-icon').toggle(function() {
		$('#f1-village').fadeIn('fast');
		$('#f1-village-glow').fadeIn('fast');
		$('#f1-village-glow').delay(1500).fadeOut('fast');
		
		// $('#f1-village-overlay').fadeIn('fast');
		
		}, function() {
		$('#f1-village').fadeOut('fast');
		
		// $('#f1-village-overlay').fadeOut('fast');
		
		});
		});
	  
	  $('#fnb-icon').mouseover(function () {
        // $('#nav-fnb-text').fadeIn('fast');
      });
	  
	  $('#fnb-icon').mouseout(function () {
        // $('#nav-fnb-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#fnb-icon').toggle(function() {
		$('#fnb').fadeIn('fast');
		$('#fnb-glow').fadeIn('fast');
		$('#fnb-glow').delay(1500).fadeOut('fast');
			// $('#fnb-overlay').fadeIn('fast');
		}, function() {
		$('#fnb').fadeOut('fast');
		// $('#fnb-overlay').fadeOut('fast');
		});
		});
			  
	  $('#merchandise-icon').mouseover(function () {
        // $('#nav-merchandise-text').fadeIn('fast');
      });
	  
	  $('#merchandise-icon').mouseout(function () {
        // $('#nav-merchandise-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#merchandise-icon').toggle(function() {
		$('#merchandise').fadeIn('fast');
		$('#merchandise-glow').fadeIn('fast');
		$('#merchandise-glow').delay(1500).fadeOut('fast');
		// $('#merchandise-overlay').fadeIn('fast');
		}, function() {
		$('#merchandise').fadeOut('fast');
		// $('#merchandise-overlay').fadeOut('fast');
		});
		});
	  
	  $('#first-aid-icon').mouseover(function () {
        // $('#nav-first-aid-text').fadeIn('fast');
      });
	  
	  $('#first-aid-icon').mouseout(function () {
        // $('#nav-first-aid-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#first-aid-icon').toggle(function() {
		$('#first-aid').fadeIn('fast');
		$('#first-aid-glow').fadeIn('fast');
		$('#first-aid-glow').delay(1500).fadeOut('fast');
		// $('#first-aid-overlay').fadeIn('fast');
		}, function() {
		$('#first-aid').fadeOut('fast');
		// $('#first-aid-overlay').fadeOut('fast');
		});
		});
	  
	  $('#atm-icon').mouseover(function () {
        // $('#nav-atm-text').fadeIn('fast');
      });
	  
	  $('#atm-icon').mouseout(function () {
        // $('#nav-atm-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#atm-icon').toggle(function() {
		$('#atm').fadeIn('fast');
		$('#atm-glow').fadeIn('fast');
		$('#atm-glow').delay(1500).fadeOut('fast');
		// $('#atm-overlay').fadeIn('fast');
		}, function() {
		$('#atm').fadeOut('fast');
		// $('#atm-overlay').fadeOut('fast');
		});
		});
	  
	  $('#information-icon').mouseover(function () {
        // $('#nav-information-text').fadeIn('fast');
      });
	  
	  $('#information-icon').mouseout(function () {
        // $('#nav-information-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#information-icon').toggle(function() {
		$('#information').fadeIn('fast');
		$('#information-glow').fadeIn('fast');
		$('#information-glow').delay(1500).fadeOut('fast');
		// $('#information-overlay').fadeIn('fast');
		}, function() {
		$('#information').fadeOut('fast');
		// $('#information-overlay').fadeOut('fast');
		});
		});
	  
	  $('#toilet-icon').mouseover(function () {
        // $('#nav-toilet-text').fadeIn('fast');
      });
	  
	  $('#toilet-icon').mouseout(function () {
        // $('#nav-toilet-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#toilet-icon').toggle(function() {
		$('#toilet').fadeIn('fast');
		$('#toilet-glow').fadeIn('fast');
		$('#toilet-glow').delay(1500).fadeOut('fast');
		// $('#toilet-overlay').fadeIn('fast');
		}, function() {
		$('#toilet').fadeOut('fast');
		// $('#toilet-overlay').fadeOut('fast');
		});
		});
	  
	  $('#handicap-toilet-icon').mouseover(function () {
        // $('#nav-handicap-toilet-text').fadeIn('fast');
      });
	  
	  $('#handicap-toilet-icon').mouseout(function () {
        // $('#nav-handicap-toilet-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#handicap-toilet-icon').toggle(function() {
		$('#handicap-toilet').fadeIn('fast');
		$('#handicap-toilet-glow').fadeIn('fast');
		$('#handicap-toilet-glow').delay(1500).fadeOut('fast');
		// $('#handicap-toilet-overlay').fadeIn('fast');
		}, function() {
		$('#handicap-toilet').fadeOut('fast');
		// $('#handicap-toilet-overlay').fadeOut('fast');
		});
		});
	  
	  $('#paddock-club-l-icon').mouseover(function () {
        // $('#nav-paddock-club-lifestyle-text').fadeIn('fast');
      });
	  
	  $('#paddock-club-l-icon').mouseout(function () {
        // $('#nav-paddock-club-lifestyle-text').fadeOut(500);
      });
	  
	   $(function() {
	   $('#paddock-club-l-icon').toggle(function() {
        $('#paddock-club-l').fadeIn('fast');
		$('#paddock-club-l-glow').fadeIn('fast');
		$('#paddock-club-l-glow').delay(1500).fadeOut('fast');
		// $('#paddock-club-overlay').fadeIn('fast');
		}, function() {
		$('#paddock-club-l').fadeOut('fast');
		// $('#paddock-club-overlay').fadeOut('fast');
		});
		});
	  
	  $('#taxi-icon').mouseover(function () {
        // $('#nav-taxi-text').fadeIn('fast');
      });
	  
	  $('#taxi-icon').mouseout(function () {
        // $('#nav-taxi-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#taxi-icon').toggle(function() {
		$('#taxi').fadeIn('fast');
		$('#taxi-glow').fadeIn('fast');
		$('#taxi-glow').delay(1500).fadeOut('fast');
		// $('#taxi-overlay').fadeIn('fast');
		}, function() {
		$('#taxi').fadeOut('fast');
		// $('#taxi-overlay').fadeOut('fast');
		});
		});
	  
	  $('#support-paddock-icon').mouseover(function () {
        // $('#nav-support-paddock-text').fadeIn('fast');
      });
	  
	  $('#support-paddock-icon').mouseout(function () {
        // $('#nav-support-paddock-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#support-paddock-icon').toggle(function() {
		$('#support-paddock').fadeIn('fast');
		$('#support-paddock-glow').fadeIn('fast');
		$('#support-paddock-glow').delay(1500).fadeOut('fast');
		// $('#support-paddock-overlay').fadeIn('fast');
		}, function() {
		$('#support-paddock').fadeOut('fast');
		// $('#support-paddock-overlay').fadeOut('fast');
		});
		});
		
		  $('#paddock-icon').mouseover(function () {
        // $('#nav-paddock-text').fadeIn('fast');
      });
	  
	  $('#paddock-icon').mouseout(function () {
        // $('#nav-paddock-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#paddock-icon').toggle(function() {
		$('#paddock').fadeIn('fast');
		$('#paddock-glow').fadeIn('fast');
		$('#paddock-glow').delay(1500).fadeOut('fast');
		// $('#paddock-overlay').fadeIn('fast');
		}, function() {
		$('#paddock').fadeOut('fast');
		// $('#paddock-overlay').fadeOut('fast');
		});
		});
	  
	  $('#ticketing-booth-icon').mouseover(function () {
         $('#nav-ticketing-booth-text').fadeIn('fast');
      });
	  
	  $('#ticketing-booth-icon').mouseout(function () {
         $('#nav-ticketing-booth-text').fadeOut(500);
      });
	  
 	  $(function() {
		$('#ticketing-booth-icon').toggle(function() {
		$('#ticketing-booth').fadeIn('fast');
		$('#ticketing-booth-glow').fadeIn('fast');
		$('#ticketing-booth-glow').delay(500).fadeOut('fast');
		 $('#ticketing-booth-overlay').fadeIn('fast');
		}, function() {
		$('#ticketing-booth').fadeOut('fast');
			 $('#ticketing-booth-overlay').fadeOut('fast');
		});
		});
		

		
		  $('#ticket-collection-icon').mouseover(function () {
         $('#nav-ticket-collection-text').fadeIn('fast');
      });
	  
	  $('#ticket-collection-icon').mouseout(function () {
         $('#nav-ticket-collection-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#ticket-collection-icon').toggle(function() {
		$('#ticket-collection').fadeIn('fast');
		$('#ticket-collection-glow').fadeIn('fast');
		$('#ticket-collection-glow').delay(500).fadeOut('fast');
			$('#ticket-collection-overlay').fadeIn('fast');
		}, function() {
		$('#ticket-collection').fadeOut('fast');
		 $('#ticket-collection-overlay').fadeOut('fast');
		});
		});
		
		
	 $('#media-centre-icon').mouseover(function () {
        $('#nav-media-centre-text').fadeIn('fast');
      });
	  
	  $('#media-centre-icon').mouseout(function () {
         $('#nav-media-centre-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#media-centre-icon').toggle(function() {
		$('#media-centre').fadeIn('fast');
		$('#media-centre-glow').fadeIn('fast');
		$('#media-centre-glow').delay(500).fadeOut('fast');
	   
		}, function() {
		$('#media-centre').fadeOut('fast');
		 $('#media-centre-overlay').fadeOut('fast');
		});
		});
		
			  $('#classic-car-icon').mouseover(function () {
         $('#nav-classic-car-text').fadeIn('fast');
      });
	  
	  $('#classic-car-icon').mouseout(function () {
         $('#nav-classic-car-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#classic-car-icon').toggle(function() {
		$('#classic-car').fadeIn('fast');
		 $('#classic-car-glow').fadeIn('fast');
		 $('#classic-car-glow').delay(500).fadeOut('fast');
		 $('#classic-car-overlay').fadeIn('fast');
		}, function() {
		$('#classic-car').fadeOut('fast');
		 $('#classic-car-overlay').fadeOut('fast');
		});
		});
		
		$('#staff-gate-icon').mouseover(function () {
         $('#nav-staff-gate-text').fadeIn('fast');
      });
	  
	  $('#staff-gate-icon').mouseout(function () {
        $('#nav-staff-gate-text').fadeOut(500);
      });
	  
	  $(function() {
		$('#staff-gate-icon').toggle(function() {
		$('#staff-gate').fadeIn('fast');
		// $('#staff-gate-glow').fadeIn('fast');
		// $('#staff-gate-glow').delay(500).fadeOut('fast');
		// $('#staff-gate-overlay').fadeIn('fast');
		}, function() {
		$('#staff-gate').fadeOut('fast');
		// $('#staff-gate-overlay').fadeOut('fast');
		});
		});
	  
	  $('#restrictedzone-icon').mouseover(function () {
        // $('#nav-restrictedzone-text').fadeIn('fast');
      });
	  
	  $('#restrictedzone-icon').mouseout(function () {
        // $('#nav-restrictedzone-text').fadeOut(500);
      });
	  
	  
	  $('#close-map, #overlay-instructions, #overlay-instructions-mobile').bind('click', function(){
         $('#overlay-instructions, #overlay-instructions-mobile, #close-map').fadeOut('fast');
 		return false;
       });
	   
	   	  $('.zone').bind('click', function(){
         $('.zone').fadeOut('fast');
 //		return false;
       });
	    $('#zone-close').live('click', function(){
		alert("a");
         $('.zone').fadeOut('fast');
		return false;
       });

});