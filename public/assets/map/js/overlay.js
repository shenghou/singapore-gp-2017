//<![CDATA[
	$(document).ready(function()
	{		
		$('#tab-container').tabs();
	
	});

	
	var isiPad = /ipad/i.test(navigator.userAgent.toLowerCase());
		$(window).load(function() { // makes sure the whole site is loaded
			$("#status").fadeOut(); // will first fade out the loading animation
			$("#preloader").delay(350).fadeOut("slow"); // will fade out the white DIV that covers the website.
			$("#navblock-1").delay(1200).fadeIn("slow");
			$("#navblock-2").delay(1400).fadeIn("slow");
			$("#navblock-3-1").delay(1700).fadeIn("slow");
			$("#navblock-3-2").delay(1800).fadeIn("slow");
			$("#navblock-3-3").delay(1900).fadeIn("slow");
			$("#navblock-3-4").delay(2000).fadeIn("slow");
			$("#navblock-3-5").delay(2100).fadeIn("slow");
			$("#navblock-4-1").delay(1000).fadeIn("slow");
			$("#navblock-4-2").delay(2500).fadeIn("slow");
			$("#nav-top-icons").delay(2800).fadeIn("slow");
			$("#nav-text-top").delay(3800).fadeIn("slow");
			$("#nav-mid-icon").delay(3800).fadeIn("slow");
			$("#nav-text-bottom").delay(3400).fadeIn("slow");
			
		
		})
	//]]>