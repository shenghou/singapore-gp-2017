<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>F1 2016 Interactive Map</title>


<!-- IE6-8 support of HTML5 elements --> <!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="js/hammer.js"></script>
<script type="text/javascript" src="js/jquery.hammer.js"></script>
<!--<script src="js/map.js"></script>-->
<script src="js/jquery.imagemapster.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.0.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.mlens-1.0.min.js"></script>

<!-- Second, add the Timer and Easing plugins -->
<script type="text/javascript" src="js/jquery.timers-1.2.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script> 

<!-- Third, add the GalleryView Javascript and CSS files -->
<script type="text/javascript" src="js/jquery.galleryview-3.0-dev.js"></script> 
<link type="text/css" rel="stylesheet" href="css/jquery.galleryview-3.0-dev.css" />
<script src="js/detectmobilebrowser.js"></script>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="css/sidebar.css">

<script src="js/gallery.js"></script> 
<script src="js/link.js"></script> 
<script src="js/jquery.hashchange.js" type="text/javascript"></script>
<!--<script src="js/jquery.easytabs.min.js" type="text/javascript"></script>-->

<script src="js/jquery.mousewheel.js" type="text/javascript"></script>
<script src="js/jquery.mapbox.js" type="text/javascript"></script>
<script src="js/jquery.wheelzoom.js" type="text/javascript"></script>
<script src="js/jquery.zoomable-1.1.js" type="text/javascript"></script>
<script src='js/jquery.elevatezoom.js' type="text/javascript"></script>
<script type="text/javascript">

$(window).load(function() {
//code for tutorial map 
$("#sidebar").hide();
$('.show_tutorial').hide();
$("#overlay-instructions").fadeIn("slow");
//$("#main-map area").click(function(e){
//var iframe=document.getElementById('gallery-frame');
//alert(iframe+" .gv_frame img");
//var $iframe_con = $("#gallery-frame").contents();
//gv_frame=$iframe_con.find('gv_frame');


// $(".gv_frame img").click(function(e){
  
// var width = $(".gv_panel img").width();
// var height = $(".gv_panel img").height();
// alert("width:"+width+" and height :"+height);
// });
//});
$('.layeroverlay').click(function(e){  
//alert("aa");  
 $('.layeroverlay').fadeOut("slow");
 $("#sidebar").fadeIn("slow");
 $('.show_tutorial').fadeIn("slow");
});

//code for tutorial map ends here
});

$(document).ready(function(){
var $i=0;
var $j=0;

 $('#zone-close').live('click', function(){
    //alert("a");
 $('.zone').fadeOut('fast');
return false;
 });
var windowHeight = $("#trackmap").height();
var windowWidth = $("#trackmap").width();
var drag_height=windowHeight+200;
var drag_width=windowWidth+200;
console.log("window height:"+windowHeight+" window_width:"+windowWidth);
console.log("div height:"+drag_height+" div_width:"+drag_width);
$("#container").width(drag_width).height(drag_height);
// mouse over issue on the popups
// img = document.getElementById('imageid'); 
//or however you get a handle to the IMG


//$(".gv_panel img")
$("#exceptmagnifier").on('click', '.show_tutorial', function () {
$(this).removeClass("show_tutorial");
 $(this).addClass("hide_tutorial");
 $("#overlay-instructions").fadeIn("slow");
});

$("#exceptmagnifier").on('click', '.hide_tutorial', function () {
$(this).removeClass("hide_tutorial");
 $(this).addClass("show_tutorial");
 $("#overlay-instructions").fadeOut("slow");
});

$(".ul_container").on('click', '.unchecked', function () {
  $(this).removeClass("unchecked");
  $(this).addClass("checked");
  
  var id_get= $(this).attr("id");
  var style_track=$("#trackmap").attr('style');
  $("#trackmap_"+id_get).delay(1000).attr('style',style_track );
  $("#"+id_get+"-glow").fadeIn("1000");
});

 $(".ul_container").on('click', '.checked', function () {
  $(this).removeClass("checked");
  $(this).addClass("unchecked");
  var id_get= $(this).attr("id");
  $("#"+id_get+"-glow").fadeOut("1000");
});

/*$('#f1-village-icon').hover(
 function () {     
         var style_track=$("#trackmap").attr('style');
      $('#trackmap_f1-village').delay(1000).attr('style',style_track );
        $("#f1-village-glow").fadeIn("1000");
         }, 
function () {
        $("#f1-village-glow").fadeOut("1000");
         }
     );

$('#fnb-icon').hover(
 function () {     
         var style_track=$("#trackmap").attr('style');
      $('#trackmap_fnb').delay(1000).attr('style',style_track );
        $("#fnb-glow").fadeIn("1000");
         }, 
function () {
        $("#fnb-glow").fadeOut("1000");
         }
     );

$('#merchandise-icon').hover(
 function () {     
         var style_track=$("#trackmap").attr('style');
      $('#trackmap_merchandise').delay(1000).attr('style',style_track );
        $("#merchandise-glow").fadeIn("1000");
         }, 
function () {
        $("#merchandise-glow").fadeOut("1000");
         }
     );

$('#first-aid-icon').hover(
 function () {     
         var style_track=$("#trackmap").attr('style');
      $('#trackmap_first-aid').delay(1000).attr('style',style_track );
        $("#first-aid-glow").fadeIn("1000");
         }, 
function () {
        $("#first-aid-glow").fadeOut("1000");
         }
     );*/
  
// @Ryan: old code 
/*
$(".magnifier").on('click', '.hide_arrow', function () {
  $(this).removeClass("hide_arrow");
                $(this).addClass("show_arrow");
                 $(".magnifier_container").animate({marginRight:"150px"});
$(".show_arrow").attr("src","images2/hide_arrow.png")
});

$(".magnifier").on('click', '.show_arrow', function () {
  $(this).removeClass("show_arrow");
                $(this).addClass("hide_arrow");
                  $(".magnifier_container").animate({marginRight:"70px"});
$(".hide_arrow").attr("src","images2/Button%20Arrows.png")
});
  */
 
 $(".magnifier").on('click', '.hide_arrow', function () {
  $(this).removeClass("hide_arrow");
  $(this).addClass("show_arrow");
  $(".magnifier_container").animate({marginRight:"180px"});
  //$("#sidebar").animate({marginRight:"0px"});
  $(".show_arrow").attr("src","images2/hide_arrow.png");
});

$(".magnifier").on('click', '.show_arrow', function () {
  $(this).removeClass("show_arrow");
  $(this).addClass("hide_arrow");
  $(".magnifier_container").animate({marginRight:"70px"});
  //$("#sidebar").animate({marginRight:"-180px"});
  $(".hide_arrow").attr("src","images2/Button%20Arrows.png");
});

/*$('.hide_arrow').click(function() {
  alert("arrow");
  console.log("aa");
    var $marginLefty = $("#sidebar");
    $marginLefty.animate({
      marginRight: parseInt($marginLefty.css('marginRight'),10) == 0 ?
        $marginLefty.outerWidth() :
        0
    });
  });
*/
$('#start-icon').hover(
  function () {     
    var style_track=$("#trackmap").attr('style');
    $('#trackmap_start').delay(1000).attr('style',style_track );
    //$("#start-glow").fadeIn("1000");
   // $(this).find("img").attr("src","images2/icons/start_glow.png");
  }, 
  function () {
   // $("#start-glow").fadeOut("1000");
   // $(this).find("img").attr("src","images2/icons/start.png");
  }
);
   
$('#finish-icon').hover(
 function () {     
         var style_track=$("#trackmap").attr('style');
      $('#trackmap_finish').delay(1000).attr('style',style_track );
      //  $("#finish-glow").fadeIn("1000");
       // $(this).find("img").attr("src","images2/icons/finish_glow.png");

         }, 
function () {
      //  $("#finish-glow").fadeOut("1000");
       // $(this).find("img").attr("src","images2/icons/finish.png"); 
     }
     );
   
$('#race-directions-icon').hover(
 function () {     
         var style_track=$("#trackmap").attr('style');
      $('#trackmap_direct').delay(1000).attr('style',style_track );
       // $("#directions-glow").fadeIn("1000");
        // $(this).find("img").attr("src","images2/icons/racedirection_glow.png");
       }, 
function () {
       // $("#directions-glow").fadeOut("1000");
       // $(this).find("img").attr("src","images2/icons/racedirection.png"); 
     }
     );
   
$('#track-turn-icon').hover(
 function () {     
         var style_track=$("#trackmap").attr('style');
      $('#trackmap_turn').delay(1000).attr('style',style_track );
       // $("#turn-glow").fadeIn("1000");
        // $(this).find("img").attr("src","images2/icons/trackturn_glow.png");
       }, 
function () {
      //  $("#turn-glow").fadeOut("1000");
        // $(this).find("img").attr("src","images2/icons/trackturn.png");
      }
     );
   
$('#superscreen-icon').hover(
 function () {     
         var style_track=$("#trackmap").attr('style');
      $('#trackmap_superscreen').delay(1000).attr('style',style_track );
       // $("#superscreen-glow").fadeIn("1000");
       // $(this).find("img").attr("src","images2/icons/superscreen_glow.png");
         }, 
function () {
       // $("#superscreen-glow").fadeOut("1000");
       // $(this).find("img").attr("src","images2/icons/superscreen.png"); 
     }
     );

$('#overpass-icon').hover(
 function () {     
         var style_track=$("#trackmap").attr('style');
      $('#trackmap_overpass').delay(1000).attr('style',style_track );
       // $("#overpass-glow").fadeIn("1000");
        //$(this).find("img").attr("src","images2/icons/overpass_glow.png");
         }, 
function () {
       // $("#overpass-glow").fadeOut("1000");
        // $(this).find("img").attr("src","images2/icons/overpass.png");
      }
     );
   
$('#underpass-icon').hover(
 function () {     
         var style_track=$("#trackmap").attr('style');
      $('#trackmap_underpass').delay(1000).attr('style',style_track );
       // $("#underpass-glow").fadeIn("1000");
       //  $(this).find("img").attr("src","images2/icons/underpass_glow.png");
       }, 
function () {
       // $("#underpass-glow").fadeOut("1000");
       // $(this).find("img").attr("src","images2/icons/underpass.png");
        }
     );
   
$('#wheelchair-platform-icon').hover(
 function () {     
         var style_track=$("#trackmap").attr('style');
      $('#trackmap_wheelchair').delay(1000).attr('style',style_track );
      //  $("#wheelchair-glow").fadeIn("1000");
       //  $(this).find("img").attr("src","images2/icons/wheelchair_glow.png");
       }, 
function () {
       // $("#wheelchair-glow").fadeOut("1000");
      //  $(this).find("img").attr("src","images2/icons/wheelchair.png"); 
    }
     );
   
/*
$('#grandstand-platform-icon').hover(
  function () {     
    var style_track=$("#trackmap").attr('style');
    $('#trackmap_grandstand').delay(1000).attr('style',style_track );
    $("#grandstand-glow").fadeIn("1000");
    //$(this).find("img").attr("src","images2/icons/grandstand_glow.png");
  }, 
  function () {
    $("#grandstand-glow").fadeOut("1000");
    //$(this).find("img").attr("src","images2/icons/grandstand.png");
  }
);
*/

/*
$('#suites-icon').hover(
  function() {
    var style_track=$("#trackmap").attr('style');
    $('#trackmap_suite').delay(1000).attr('style',style_track );
    $("#suite-glow").fadeIn("1000");
    //$(this).find("img").attr("src","images2/icons/suites_glow.png");
  }, 
  function () {
    $("#suite-glow").fadeOut("1000");
    //$(this).find("img").attr("src","images2/icons/suites.png");
  }
);
*/

/*
$('#paddock-club-icon').hover(
 function () {     
         var style_track=$("#trackmap").attr('style');
      $('#trackmap_paddock').delay(1000).attr('style',style_track );
        $("#paddock-club-glow").fadeIn("1000");
        //$(this).find("img").attr("src","images2/icons/paddock_glow.png");
         }, 
function () {
        $("#paddock-club-glow").fadeOut("1000");
        //$(this).find("img").attr("src","images2/icons/paddock-club.png");
        }
     );
*/
$('#viewing-platform-icon').hover(
         function () {
         // alert("hideend");
         var style_track=$("#trackmap").attr('style');
//alert(style_track);
$('#trackmap_view').delay(1000).attr('style',style_track );
        //   $("#viewing-glow").fadeIn("1000");
       // $(this).find("img").attr("src","images2/icons/viewingplatform_glow.png");
         }, 
         function () {
        //   $("#viewing-glow").fadeOut("1000");
       // $(this).find("img").attr("src","images2/icons/viewingplatform.png");
     }
     );
$('#gate-icon').hover(
         function () {
         // alert("hideend");
         var style_track=$("#trackmap").attr('style');
//alert(style_track);
$('#trackmap_gate').delay(1000).attr('style',style_track );
        //   $("#gate-glow").fadeIn("1000");
        // $(this).find("img").attr("src","images2/icons/gate_glow.png");
       }, 
         function () {
        //   $("#gate-glow").fadeOut("1000");
       // $(this).find("img").attr("src","images2/icons/gate.png"); 
     }
     );


/*$('#mrt-icon').hover(
  function (){
    var style_track=$("#trackmap").attr('style');
    //alert(style_track);
    $('#trackmap_mrt').delay(1000).attr('style',style_track );
    // alert("hideend");
    $("#mrt-glow").fadeIn("1000");
    //$(this).find("img").attr("src","images2/icons/mrt_glow.Png");
  }, 
  function () {
    $("#mrt-glow").fadeOut("1000");
   // $(this).find("img").attr("src","images2/icons/mrt.png");
  }
);*/
/*$('#overpass-icon').hover(
         function () {
   var style_track=$("#trackmap").attr('style');
//alert(style_track);
$('#trackmap_gate').delay(1000).attr('style',style_track );
         // alert("hideend");
           $("#overpass-glow").fadeIn("1000");
         }, 
         function () {
           $("#overpass-glow").fadeOut("1000");
         }
     );*/
//$('#image').zoomable();

$('#trackmap').zoomable();
$('.map-icon').zoomable();
var wrapperOffset = $("#drag_cont").offset();
//$('#exceptmagnifier').draggable();
 $('#exceptmagnifier').draggable({
revert:false,
scroll:false,
 containment: "parent"}); 
  

$("#zoom_in, #btn-zoom-in").on( "click", function() {
  $i++;
  //if($j>2){$j--;}
/* windowHeight = $("#trackmap").height();
windowWidth = $("#trackmap").width();
wrapperOffset = $("#trackmap").offset();
console.log("Left"+wrapperOffset.left-200);
console.log("TOp"+wrapperOffset.top-200);
console.log("Left end"+wrapperOffset.left+" TOp end"+wrapperOffset.top); */
//$('#exceptmagnifier').draggable();
/* $('#exceptmagnifier').draggable({
revert:false,
scroll:false,
 containment: [
        wrapperOffset.left-200,
        wrapperOffset.top-200,
        wrapperOffset.left,
        wrapperOffset.top
    ]});  */
  $("#zoom_out").removeAttr('disabled');
  $('#btn-zoom-out').removeClass('not_clickable');
  //alert("no of clicks variable i:"+$i);
  //alert("no of clicks variable j:"+$j);
  $('#trackmap').zoomable('zoomIn');
  //$('#trackmap_atm-icon').zoomable();
  //$('#trackmap_atm-icon').zoomable('zoomIn');
  $('.map-icon').zoomable('zoomIn');
  $('#trackmap_merchandise').zoomable('zoomIn');
 // $('.map-icon').draggable();
  $(".zoomLens").attr('style', 'display: none !important');
  $(".zoomContainer").css("display", "none"); 
  var style_track=$("#trackmap").attr('style');
  // alert(style_track);
  // $(".dynamic img").delay(1000).attr('style',style_track );
  
  // alert("no of clicks:"+$i);
  if ($i>2){
    $("#zoom_in").attr('disabled','disabled');
  $('#btn-zoom-in').addClass('not_clickable');
  }

});

$("#zoom_out, #btn-zoom-out").on( "click", function() {
  //$j++;
  $i--;
 
  $('#btn-zoom-in').removeClass('not_clickable');
  $("#zoom_in").removeAttr('disabled');
  $("#zoom_out").removeAttr('disabled');
  //alert("no of clicks variable i:"+$i);
  //alert("no of clicks variable j:"+$j);
  //
  //$('#trackmap_atm-icon').zoomable('zoomOut');
  $('.map-icon').zoomable('zoomOut');
  
  //alert("no of clicks:"+$j);
  // var style_track=$("#trackmap").attr('style');
  // $(".dynamic img").delay(1000).attr('style',style_track );
  $('#trackmap').zoomable('zoomOut');
  
  $(".zoomLens").attr('style', 'display: none !important');
  $(".zoomContainer").css("display", "none"); 

/*    windowHeight = $("#trackmap").height();
windowWidth = $("#trackmap").width();
drag_height=windowHeight-200;
drag_width=windowWidth-200;

wrapperOffset = $("#drag_cont").offset();
console.log(wrapperOffset.left-200);
console.log(wrapperOffset.top-200);
console.log("Left end"+wrapperOffset.left+" TOp end"+wrapperOffset.top); */
//$('#exceptmagnifier').draggable();
/* $('#exceptmagnifier').draggable({
revert:false,
scroll:false,
 containment: [
        wrapperOffset.left-200,
        wrapperOffset.top-200,
        wrapperOffset.left,
        wrapperOffset.top
    ]});  */
  if($i<1){
    $("#zoom_out").attr('disabled','disabled');
  $('#btn-zoom-out').addClass('not_clickable');
  }else{
    $("#zoom_out").removeAttr('disabled');
  }
});

$("#trackmap").on( "click", function() {
  $(".zoomLens").attr('style', 'display: none !important');
  $(".zoomContainer").css("display", "none"); 
});

   /*$( "#magnifier" ).on( "click", function() {
   // alert("magnifier");
   $(".zoomLens").css("display", "block");
   $(".zoomContainer").css("display", "block"); 
     $("#trackmap").elevateZoom({
  constrainType:"height",
   constrainSize:274,
    zoomType: "lens",lensShape : "round",
  lensSize    : 300,
     containLensZoom: true,
      gallery:'gallery_01',
       cursor: 'pointer',
       scrollZoom : true,
   galleryActiveClass: "active"}); 
   });*/
  
  var sidebarAnimating = false;
  var sidebarIsShown = false;
  $('#sidebar-controls-wrapper #btn-show-sidebar').click(function(){
    $('#sidebar-controls-wrapper .control .label').fadeOut();
    if (!sidebarAnimating){
      sidebarAnimating = true;
      if (sidebarIsShown){
        $("#sidebar").animate({marginRight:"-180px", easing:"easeInOutCubic"}, 400, function(){
          sidebarIsShown = false;
          sidebarAnimating = false;
          $('#sidebar-controls-wrapper #btn-show-sidebar').removeClass('opened');
          $("#sidebar-wrapper").css('width', '30px');
        });
      }else{
        $("#sidebar-wrapper").css('width', '210px');
        $("#sidebar").animate({marginRight:"0px", easing:"easeInOutCubic"}, 400, function(){
          sidebarIsShown = true;
          sidebarAnimating = false;
          $('#sidebar-controls-wrapper #btn-show-sidebar').addClass('opened');
        });
      }
    }
  });
  
  $('#sidebar-controls-wrapper .control .label').fadeOut();
  $('#sidebar-controls-wrapper .control').hover(function(){
    var label = $(this).find('.label').fadeIn();
    //console.log(label);
    //label.fadeIn();
  },function(){
    var label = $(this).find('.label').fadeOut();
    //console.log(label);
    //label.fadeOut();
  });
  
  
  
});
</script>
<style type="text/css">
 #gallery_01 img{border:2px solid white;}
 
 /*Change the colour*/
 .active img{border:2px solid #333 !important;}
 </style>
<base target="_blank">
</head>
<body>
<!-- Preloader  -->
<div id="preloader">
  <div id="status">&nbsp;</div>
</div>


<div id="container" > 

<!-- <div id="magnifier"><img src="images2/zoom-icon.png"></div> -->
<div id="exceptmagnifier" style="width: 1278px;height: 768px;">
<div id="thisGallery" class="hidden">
  
</div>
<div id="drag_cont" style="margin: auto;position: absolute;left:0;right: 0;top: 0;bottom: 0;width: 1208px;height: 768px;">
<!--<div style="position: relative;height: 90px;width: 10px;position: fixed;left: 84.5%;margin-top: 30%;z-index: 98999;">-->
<div class="magnifier_container" style="position: relative;height: 90px;width: 10px;/* position: fixed; *//* left: 92.5%; */
margin-top: 30%;z-index: 98999;margin-right: 70px;float: right;">
<!--<div id="magnifier" class="magnifier"><img style="height: 30px; width: 30px;"src="images2/Magnifying glass.png"></div>-->
<div class="magnifier">
<input style="height: 30px; width: 30px;" type="button" id="zoom_in" value="-" title="Zoom in" class="ui-button ui-widget ui-state-default ui-corner-all zoomin" role="button" src="images2/+.png">
<!--<input type="button" value="+" onclick="$('#trackmap').zoomable('zoomIn')" title="Zoom in" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" style="margin-top: 15px;">--></div>
<div  class="magnifier" style="top: 270px;">
  <input style="height: 30px; width: 30px;" id="zoom_out" disabled="disabled" type="button"  title="Zoom out" class="ui-button ui-widget ui-state-default ui-corner-all zoomout" role="button" src="images2/-.png">
  <!--<input type="button" value="-" onclick="$('#trackmap').zoomable('zoomOut')" title="Zoom out" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" style="margin-top: 15px;">--></div>
<div class="magnifier"><img class="hide_arrow magnifier" style="height: 30px; width: 30px;" class="hide_arrow magnifier" src="images2/Button Arrows.png"></div>
</div>
<div class="show_tutorial"></div>
  <!--<div id="magnifier" style="top: 340px;height: 55px;  width: 35px;  padding: 10px;">
   <input type="button" value="Reset" onclick="$('#trackmap').zoomable('reset')" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" style=" margin-top: 15px;"> </div>-->
<div id="comboGallery" class="hidden">
  
</div>

<?php 
include 'elements.php';
 ?>

<!-- <div id="navblock-4-2" class="navlayer hidden"></div> -->
<div id="nav-text"><?php echo file_get_contents("date.txt"); ?></div>

<div id="nav-restrictedzone-text">
<img src="images2/nav-restricted-zone-text.png">
</div>
<!-- 


<div id="nav-text-bottom" class="navlayer hidden"><img src="images2/nav-bottom-icons.png" usemap="#nav-bottom-map"></div>
<div id="nav-text-top" class="navtexttop hidden"><img src="images2/nav-text-top-old.png" usemap="#nav-top-map"></div>



 -->
<div id="gallery-overlay" class="hidden">

&nbsp;

</div>
<div class="gallery-bg gallery-bg-1">
<div class="gallery-bg gallery-bg-2">

</div>
<div class="gallery-bg gallery-bg-2-2">
  <?php /*<img src="images2/bg-gallery-2-2.png" usemap="#close-gallery-map-2">*/ ?>
  <img src="images2/bg-gallery-2-3.png" usemap="#close-gallery-map-4" />
</div>
</div>
<div class="gallery-bg gallery-bg-3">
<?php /*<img src="images2/bg-gallery-3-2.png" usemap="#close-gallery-map">*/ ?>
<img src="images2/bg-gallery-3-4.png" usemap="#close-gallery-map-3">
<div class="gallery-text-container hidden">
<div class="gallery-text">

</div>
<div class="gallery-text-combo">
</div>
<div id="link-start">
&nbsp;
</div>
<div class="link-text">
</div>
</div>
</div>
<div class="gallery-title hidden">

</div>
<div class="gallery-title-combo hidden">
</div>
<div class="gallery-title-2 hidden">

</div>

<div id="overlay-instructions" class="layeroverlay"><img width="1208" height="768" src="images2/overlay-instructions-2014.jpg"></div>
<div id="overlay-instructions-mobile" class="layer"><img src="images2/overlay-instructions-mobile.png"></div>
<div id="close-map"><a href="#" id="close-map-tutorial"><img src="images2/close-map-tutorial.png"></a></div>
<div id="zone-1-overlay" class="zone layer hidden"><img src="images2/Zone-1.png" usemap="zone-close"></div>
<div id="zone-2-overlay" class="zone layer hidden"><img src="images2/Zone-2.png" usemap="zone-close"></div>
<div id="zone-3-overlay" class="zone layer hidden"><img src="images2/Zone-3.png" usemap="zone-close"></div>
<div id="zone-4-overlay" class="zone layer hidden"><img src="images2/Zone-4.png" usemap="zone-close"></div>
</div>
<?php 
include 'sidebar.php';
 ?>
 </div>
</div>
<!-- Preloader -->
<script type="text/javascript" src="js/overlay.js"></script>
<!--<map id="main-map" name="main-map">
<area shape="rect" coords="565,377,605,399" href="#" id="grandstand-bay" icon="grandstand-bay"    />
<area shape="rect" coords="418,337,478,359" href="#" id="grandstand-esplanade"    />
<area shape="rect" coords="350,197,410,219" href="#" id="grandstand-connaught"    />
<area shape="rect" coords="227,174,274,196" href="#" id="grandstand-padang-1"    />
<area shape="rect" coords="302,141,349,163" href="#" id="grandstand-padang-2"    />
<area shape="rect" coords="471,148,526,170" href="#" id="grandstand-stamford"    />
<area shape="rect" coords="497,126,562,148" href="#" id="grandstand-turn-7"    />
<area shape="rect" coords="947,222,1010,239" href="#" id="grandstand-turn-3"    />
<area shape="rect" coords="1078,269,1120,287" href="#" id="grandstand-turn-2"    />
<area shape="rect" coords="1077,304,1119,322" href="#" id="grandstand-turn-1"    />
<area shape="rect" coords="1027,417,1061,435" href="#" id="grandstand-pit"    />
<area shape="rect" coords="1010,545,1061,565" href="#" id="grandstand-fanstand"    />
<area shape="rect" coords="1096,202,1149,224" href="#" id="stage-village"    />
<area shape="rect" coords="926,311,994,333" href="#" id="stage-paddock-club-lifestyle"    />
<area shape="rect" coords="1077,365,1124,387" href="#" id="stage-marina-pit-lifestyle-area"    />
<area shape="rect" coords="293,320,372,342" href="#" id="stage-esplanade-outdoor-theatre"    />
<area shape="rect" coords="197,252,251,272" href="#" id="stage-esplanade-park"    />
<area shape="rect" coords="359,149,407,168" href="#" id="stage-padang"    />
<area shape="rect" coords="946,204,1014,222" href="#" id="suites-the-green-room"    />
<area shape="rect" coords="1079,256,1138,270" href="#" id="suites-turn-2"    />
<area shape="rect" coords="1079,292,1138,306" href="#" id="suites-turn-1"    />
<area shape="rect" coords="1045,406,1104,420" href="#" id="suites-pit"    />
<area shape="rect" coords="1011,367,1057,284" href="#" id="handicap-marina-pit"    />
<area shape="rect" coords="79,251,117,269" href="#" id="handicap-empress-place"    />
<area shape="rect" coords="960,417,998,443" href="#" id="club-paddock"    />
</map>-->
<!--to adjust coords: x and y coords roughly half that of pixel on photoshop.-->
<map id="main-map" name="main-map">
<area shape="rect" coords="580,468,620,488" href="#" id="grandstand-bay" icon="grandstand-bay">
<area shape="rect" coords="445,388,514,408" href="#" id="grandstand-esplanade">
<area shape="rect" coords="386,266,425,290" href="#" id="grandstand-connaught">
<!--<area shape="rect" coords="227,224,274,246" href="#" id="grandstand-padang-1">-->
<area shape="rect" coords="255,86,292,106" href="#" id="grandstand-padang-1">
<!--<area shape="rect" coords="302,191,349,213" href="#" id="grandstand-padang-2">-->
<area shape="rect" coords="194,116,233,138" href="#" id="grandstand-padang-2">
<area shape="rect" coords="514,109,551,131" href="#" id="grandstand-stamford">
<!-- <area shape="rect" coords="497,176,562,198" href="#" id="grandstand-turn-7"> -->
<area shape="rect" coords="958,230,1014,250" href="#" id="grandstand-turn-3">
<area shape="rect" coords="1117,278,1155,298" href="#" id="grandstand-turn-2">
<area shape="rect" coords="1117,333,1155,353" href="#" id="grandstand-turn-1">
<area shape="rect" coords="1082,500,1121,525" href="#" id="grandstand-pit">
    <!-- <area shape="rect" coords="1010,595,1061,615" href="#" id="grandstand-fanstand"> -->
<area shape="rect" coords="1151,198,1183,218" href="#" id="stage-village">
    
<area shape="rect" coords="150,216,182,237" href="#" id="stage-coyote">
<area shape="rect" coords="363,164,397,190" href="#" id="stage-mambo">
<area shape="rect" coords="1151,137,1180,157" href="#" id="stage-sail">
    
 <!--<area shape="rect" coords="1077,415,1124,437" href="#" id="stage-marina-pit-lifestyle-stage">-->
    <!--<area shape="rect" coords="1074,410.5,1132.5,424.5" href="#" id="stage-marina-pit-lifestyle-stage">-->
    <!--<area shape="rect" coords="1077,394,1119,372" href="" id="stage-marina-pit-lifestyle-area">-->
<area shape="rect" coords="315,406,374,430" href="#" id="stage-esplanade-outdoor-theatre">
    <!--<area shape="rect" coords="197,302,251,322" href="#" id="stage-esplanade-park">-->
<!-- <area shape="rect" coords="209.5,314.5,261.5,327" href="#" id="stage-esplanade-park"> -->
<area shape="rect" coords="359,126,391,148" href="#" id="stage-padang">
<area shape="rect" coords="1116,310,1184,331" href="#" id="suites-the-green-room">
<area shape="rect" coords="1116,256,1183,278" href="#" id="suites-turn-2">
    <!--<area shape="rect" coords="1106,326,1172,345" href="#" id="suites-turn-1"> -->
    <area shape="rect" coords="1082,474,1149,495" href="#" id="suites-pit">
    <!--<area shape="rect" coords="1011,417,1057,434" href="#" id="handicap-marina-pit">-->
<!--<area shape="rect" coords="1016.5,407,1057,420" href="#" id="handicap-marina-pit">-->
<area shape="rect" coords="1116,381,1170,403" href="javascript:void(0)" id="turn-1-wheel-chair">
<!--<area shape="rect" coords="79,301,117,319" href="#" id="handicap-empress-place">-->
<area shape="rect" coords="76,278,134,300" href="#" id="handicap-empress-place">
<!--<area shape="rect" coords="960,417,998,443" href="" id="club-paddock">-->
<area shape="rect" coords="962,389,1037,410" href="" id="club-paddock">
<!--<area shape="rect" coords="941,394.5,1005.5,413.5" href="#" id="stage-paddock-club-lifestyle">-->
<!-- <area shape="rect" coords="1040.5,234.5,1086,264.5" href="#" id="trispan-lifestyle-area"> -->
<area shape="rect" coords="950,450,990,466" href="#" id="club-paddock-icon">
</map>


<map id="close-gallery-map" name="close-gallery-map">
<area shape="rect" coords="217,54,367,97" href="#" id="gallery-close"    />
</map>
<map id="close-gallery-map-2" name="close-gallery-map-2">
<area shape="rect" coords="924,43,974,93" href="#" id="gallery-close-2"    />
</map>
<map id="close-gallery-map-3" name="close-gallery-map-3">
<area shape="rect" coords="217,2,367,45" href="#" id="gallery-close"    />
</map>
<map id="close-gallery-map-4" name="close-gallery-map-4">
<area shape="rect" coords="835,47,887,97" href="#" id="gallery-close"    />
</map>
<map id="zone-close" name="zone-close">
<area shape="rect" coords="984,122,1034,172" href="#" id="zone-close"    />
</map>
</body>


</html>