<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Club/Sky Suites</title>

<!-- First, add jQuery (and jQuery UI if using custom easing or animation -->
<script src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.0.custom.min.js"></script>

<!-- Second, add the Timer and Easing plugins -->
<script type="text/javascript" src="js/jquery.timers-1.2.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>

<!-- Third, add the GalleryView Javascript and CSS files -->
<script type="text/javascript" src="js/jquery.galleryview-3.0-dev.js"></script>
<link type="text/css" rel="stylesheet" href="css/jquery.galleryview-3.0-dev.css" />
<script type="text/javascript">
$(function(){
	$('#skyGallery').galleryView({
    frame_width: 8,
    frame_height: 8,
    frame_gap: 22,
	panel_width: 590, 				//INT - width of gallery panel (in pixels)
	panel_height: 390,
	enable_overlays: true,
    pan_images: true,
	show_panel_nav: false,
    show_filmstrip_nav: false,
    show_infobar: false,
	filmstrip_size: 4, 				//INT - number of frames to show in filmstrip-only gallery
		filmstrip_style: 'scroll', 		//STRING - type of filmstrip to use (scroll = display one line of frames, scroll filmstrip if necessary, showall = display multiple rows of frames if necessary)
		filmstrip_position: 'left', 	//STRING - position of filmstrip within gallery (bottom, top, left, right)
		frame_width: 110, 				//INT - width of filmstrip frames (in pixels)
		frame_height: 76, 				//INT - width of filmstrip frames (in pixels)
		frame_opacity: 0.5, 
});
$('div.gv_overlay').css({'left' : '0px', 'bottom' : '0px'});
$('div.gv_galleryWrap').css('height','422px');
$('div.gv_filmstripWrap').css({'height' : '345px', 'width' : '116px'});
$('div.gv_gallery').css('height','422px');
$('div.gv_panelWrap').css('height','422px');
$('div.gallery-seating-plan').css('width','900px');
});
</script>
<!-- Lastly, call the galleryView() function on your unordered list(s) -->
<script type="text/javascript" src="js/gallerypagesuites.js"></script>

<link type="text/css" rel="stylesheet" href="css/gallerypage.css" />
</head>
<?php

// Turn off all error reporting
error_reporting(0);
?>

<body>
<?php
$s = $_GET['s'];
$surl = 'images2/' . $s . '/captions.xml';
$sxml = simplexml_load_file($surl);
    
?>
	<ul id="skyGallery">
	<li><img data-frame="images2/<?php echo $s?>/1.jpg" src="images2/<?php echo $_GET['s']?>/1.jpg" data-description="<?php echo $sxml->captions->caption[0]->text;?>" /></li>
    <li><img data-frame="images2/<?php echo $s?>/2.jpg" src="images2/<?php echo $_GET['s']?>/2.jpg" data-description="<?php echo $sxml->captions->caption[1]->text;?>" /></li>
    <li><img data-frame="images2/<?php echo $s?>/3.jpg" src="images2/<?php echo $_GET['s']?>/3.jpg" data-description="<?php echo $sxml->captions->caption[2]->text;?>" /></li>
	<!-- <li><img data-frame="images2/<?php echo $s?>/4.jpg" src="images2/<?php echo $_GET['s']?>/4.jpg" data-description="<?php echo $sxml->captions->caption[3]->text;?>" /></li>
	<li><img data-frame="images2/<?php echo $s?>/5.jpg" src="images2/<?php echo $_GET['s']?>/5.jpg" data-description="<?php echo $sxml->captions->caption[4]->text;?>" /></li>
	<li><img data-frame="images2/<?php echo $s?>/6.jpg" src="images2/<?php echo $_GET['s']?>/6.jpg" data-description="<?php echo $sxml->captions->caption[5]->text;?>" /></li>-->
	   <!-- <li><img data-frame="images2/<?php echo $s?>/7.jpg" src="images2/<?php echo $_GET['s']?>/7.jpg" data-description="<?php echo $sxml->captions->caption[6]->text;?>" /></li>
	    <li><img data-frame="images2/<?php echo $s?>/8.jpg" src="images2/<?php echo $_GET['s']?>/8.jpg" data-description="<?php echo $sxml->captions->caption[7]->text;?>" /></li> -->

        </ul>
		<div class="gallery-submenu">
<!--		<p>Seating Plan<br />& Zone Access</p>  -->
		<ul>
		<li><a href="#" id="seating-plan-sky">Floor Plan<br />& Zone Access</a></li>
		</ul>
		</div>
		<div class="gallery-seating-plan-suites">
		
		</div>
</body>
</html>
