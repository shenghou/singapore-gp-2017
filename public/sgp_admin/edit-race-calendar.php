<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");

$id=$_REQUEST['id'];

if(isset($_POST['submit'])) {
	$error=0;

	
	$race=$_POST['race'];
	$date=$_POST['date'];
	$race_date=$_POST['race_date'];
	$country=$_POST['country'];
	
	
	if($race=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Race.";
	}
	if($date=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Date.";
	}
	
	if($race_date=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Actual Race Date.";
	}

	if($error!=1) {

			
			$q="UPDATE race_calendar SET race='".mysql_real_escape_string($race)."',date='".mysql_real_escape_string($date)."', 
			race_date='".mysql_real_escape_string($race_date)."', country='".mysql_real_escape_string($country)."' WHERE id='".mysql_real_escape_string($id)."'";
			$result=mysql_query($q,$link) or die(mysql_error());

			if (isset($link1) && is_resource($link1)) {
				mysql_query($q, $link1) or die(mysql_error());
			}

			if (isset($link2) && is_resource($link2)) {
				mysql_query($q, $link2) or die(mysql_error());
			}

			header('Location:race-calendar-listing.php?page='.$_REQUEST[page].'&orderby='.$_REQUEST[orderby].'&search_value='.$_REQUEST[search_value].'');
			exit();
			
		
	}

} else {
	
	$q="SELECT * FROM race_calendar WHERE id='$_REQUEST[id]'";
	$result=mysql_query($q,$link) or die(mysql_error());
	$row=mysql_fetch_array($result);
	
	$race=$row['race'];
	$date=$row['date'];
	$race_date=$row['race_date'];
	$country=$row['country'];
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />

<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Edit Race Calendar </div>
		<div id="back_to_list"><a href="race-calendar-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post">
			<div>
				
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Race  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="race"  value="<?php echo $race; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>

				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Date  : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="date"  value="<?php echo $date; ?>" style="width:500px" id="role_name"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Actual Race Date  : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="race_date"  value="<?php echo $race_date; ?>" style="width:500px" id="role_name"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Country  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="country"  value="<?php echo $country; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Update Race Calendar"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				</div>
				</form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

