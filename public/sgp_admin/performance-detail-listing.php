<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
	exit();
}

if(isset($_REQUEST['search_value'])) {
	$search_value=$_REQUEST['search_value'];
	if($search_value!="") {
		$q_search="AND (date LIKE '%$search_value%' OR time LIKE '%$search_value%' OR zone LIKE '%$search_value%')";

	}
}


include("include/configure.php");

$entertainment_id=$_REQUEST['entertainment_id'];

$q_enter="SELECT artist,short_description,image_thumb FROM entertainment_highlights WHERE id='".mysql_real_escape_string($entertainment_id)."'";
$result_enter=mysql_query($q_enter,$link) or die(mysql_error());
$row_enter=mysql_fetch_array($result_enter);




if($_GET['orderby']=="") {
	$orderby="id";
} else {
	$orderby=$_GET['orderby'];
}


if(isset($_POST['SaveChange'])) {
	$count_photo=count($_POST['photo_id']);
	for($i=0;$i<$count_photo;$i++) {
		/*
		$category_insert=$_POST['category'][$i];
		$caption_insert=$_POST['caption'][$i];
		*/
		$photo_id_insert=$_POST['photo_id'][$i];
		/*
		$sort_insert=$_POST['sort'][$i];
		*/
		$delete_photo_id=$_POST['delete_image'.$photo_id_insert];
		
		
		
		if($delete_photo_id!="") {
			/*
			unlink("Uploaded/".$delete_photo_id);
			$q_d="SELECT * FROM photo_gallery WHERE id='$photo_id_insert'";
			$result_d=mysql_query($q_d,$link) or die(mysql_error());
			$row_d=mysql_fetch_array($result_d);
			
			unlink("Uploaded/".$row_d['image_path_large']);
			
			*/
			
			$q_update="DELETE FROM entertainment_highlights_gallery WHERE id='$photo_id_insert'";
			$result_update=mysql_query($q_update,$link) or die(mysql_error());
			
			if (isset($link1) && is_resource($link1)) {
				mysql_query($q_update, $link1) or die(mysql_error());
			}

			if (isset($link2) && is_resource($link2)) {
				mysql_query($q_update, $link2) or die(mysql_error());
			}

		
		}
		/*
		else {
			$q_update="UPDATE photo_gallery SET caption='$caption_insert',sort='$sort_insert',CategoryID='$category_insert' WHERE id='$photo_id_insert'";
			$result_update=mysql_query($q_update,$link) or die(mysql_error());
			
			$q_update="UPDATE photo_gallery SET caption='$caption_insert',sort='$sort_insert',CategoryID='$category_insert' WHERE id='$photo_id_insert'";
			$result_update=mysql_query($q_update,$link1) or die(mysql_error());
			
			$q_update="UPDATE photo_gallery SET caption='$caption_insert',sort='$sort_insert',CategoryID='$category_insert' WHERE id='$photo_id_insert'";
			$result_update=mysql_query($q_update,$link2) or die(mysql_error());
		}
		*/
	
	}
	
	
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/ajax.js"></script>
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container" style="width:810px;">
		<div id="title">Performance Detail Listing</div>
		
		<div class="searching_field">
		<form action="<?php $PHP_SELF; ?>" method="get">
		<table width="248" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="46">Search</td>
			<td width="133"><input type="text" name="search_value" value="<?php echo stripslashes($search_value); ?>"></td>
			<td width="69"><input type="submit" name="search" value="search"></td>
		</tr>
		</table>
		<input type="hidden" name="entertainment_id" value="<?php echo $entertainment_id; ?>">
		</form>
		</div>
		<div class="clearboth"></div>
		
		<div style="font-weight:bold; font-size:14px; padding:5px 5px 5px 0;"><?php echo $row_enter['artist']; ?></div>
		<div><img src="<?php echo $row_enter['image_thumb']; ?>" align="left" width="130" height="130" style="margin-right:10px; margin-bottom:10px; border:1px solid #CCC"><?php echo $row_enter['short_description']; ?></div>
		<div style="clear:both"></div>
		
		<div class="functionlink" style="margin:30px 0 0 0; font-size:12px"><strong><a href="add-performance-detail.php?entertainment_id=<?php echo $entertainment_id; ?>">Add Performance Detail</a></strong></div>
		<div id="content_list">
			<table width="760" align="left" cellpadding="5" cellspacing="0" style="border-top:1px solid #B6BEBE;">
			<tr class="blue_column">
				<td width="26" class="functionlink bottomline leftline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=id&search_value=$search_value&entertainment_id=$entertainment_id"; ?>">No</a></strong></td>
				<td width="86" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=date&search_value=$search_value&entertainment_id=$entertainment_id"; ?>">Date</a></strong></td>
				<td width="101" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=time&search_value=$search_value&entertainment_id=$entertainment_id"; ?>">Time</a></strong></td>
				<td width="131" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=zone&search_value=$search_value&entertainment_id=$entertainment_id"; ?>">Zone</a></strong></td>
				<td width="162" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=zone_link&search_value=$search_value&entertainment_id=$entertainment_id"; ?>">Zone Link</a></strong></td>
				<td width="108" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=estimated_time&search_value=$search_value&entertainment_id=$entertainment_id"; ?>">Estimated Duration</a></strong></td>
				<td width="21" class="bottomline" align="center"><strong>Edit</strong></td>
				<td width="43" class="bottomline" align="center"><strong>Delete</strong></td>
			</tr>
			<?php 
			   if($page!=1 && $page!="") {
						$pagecount=$page-1;
						$num=$pagecount * $limit;
			   	}
				$q="SELECT * FROM  entertainment_highlights_detail WHERE entertainment_id='".mysql_real_escape_string($entertainment_id)."' $q_search ORDER BY $orderby";
			   $result=mysql_query($q,$link) or die(mysql_error());
			   while($row=mysql_fetch_array($result)) {
			  	 	$num=$num+1;
					

					
			   		?>
					<tr>
						<td width="26" class="bottomline leftline" valign="top"><?php echo $num; ?></td>
						<td width="86" class="bottomline" valign="top"><?php echo $row['date']; ?></td>
						<td width="101" class="bottomline" valign="top"><?php echo $row['time']; ?></td>
						<td width="131" class="bottomline" valign="top"><?php echo $row['zone']; ?></td>
						<td width="162" class="bottomline" valign="top"><?php echo $row['zone_link']; ?></td>
						<td width="108" class="bottomline" valign="top"><?php if($row['estimated_time']=="") { echo "-"; } else { echo $row['estimated_time']; } ?></td>
						<td width="21" class="functionlink bottomline" align="center" valign="top">
							<a href="edit-performance-detail.php?id=<?php echo $row['id']; ?>&page=<?php echo $page; ?>&orderby=<?php echo $orderby; ?>&search_value=<?php echo $search_value; ?>&entertainment_id=<?php echo $entertainment_id; ?>"><img src="images/edit.jpg" border="none"></a>
						
						</td>
						<td width="43" class="functionlink bottomline" align="center" valign="top">
						
							<a href="javascript:;" onClick="DeletePerformanceDetail('<?php echo $row['id']; ?>')"><img src="images/delete.gif" border="none"></a>
						
						</td>
					</tr>
					<?php
			   }
			?>
			</table>
			
			
			 <div class="clearboth"></div>
				<table border="0" cellspacing="0" cellpadding="0" class="page_table" align="left">
				<tr>
				<td align="left" class="style5"><?php
				$lrange = max(1,$page-(($range-1)/2));
				$rrange = min($totalpage,$page+(($range-1)/2));
				if (($rrange - $lrange) < ($range - 1))
					{
						if ($lrange == 1)
							{
								$rrange = min($lrange + ($range-1), $totalpage);
							}
						else
							{
								$lrange = max($rrange - ($range-1), 0);
							}
					}
				if($totalpage>1)
				{
					if ($page > 1)//previous page
						{
							$prev=$page-1;
							echo " <a href='".$PHP_SELF."?page=1&orderby=$orderby&search_value=$search_value'><< First</a> | "; 
							echo " <a href='".$PHP_SELF."?page=$prev&orderby=$orderby&search_value=$search_value'>< Prev</a> | ";
						}
					else
						{
							echo " <strong><< First</strong> | <strong>< Prev</strong> |";
						}
					for($i = 1; $i <= $totalpage; $i++)//the range of page
						{
							if ($i == $page)
								{
									echo " <span class='actual_page'><strong>$i</strong></span>";
								}
							else
								{
									if ($lrange <= $i and $i <= $rrange)
										{
											echo " <a href='".$PHP_SELF."?page=$i&orderby=$orderby&search_value=$search_value' class='bluecolor word fontsubtitle'>$i</a> ";
										}
								}
						}
					if ($page < $totalpage)//next page
						{
							$next=$page+1;
							echo "| <a href='".$PHP_SELF."?page=$next&orderby=$orderby&search_value=$search_value'>Next ></a> |";
							echo " <a href='".$PHP_SELF."?page=$totalpage&orderby=$orderby&search_value=$search_value'>Last >></a>";
							
						}
					else
						{
							echo " | <strong>Next ></strong> | <strong>Last >></strong>";
						}
				}//end of if $totalpage > 1
			?></td>
			  </tr>
			</table>
			
			<br>
			<br>
			
			<div id="title">Manage Entertainment Highlights Gallery </div>
			<div id="back_to_list"><a href="add-entertainment-highlights-gallery.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>&entertainment_id=<?php echo $_REQUEST['entertainment_id']; ?>">Add Photo</a>
		
		</div>
			
			
			
			
			
			
			
			
			<form action="<?php $PHP_SELF; ?>" method="post">
			<div>
				
				<?php
				
					$q_photo="SELECT * FROM entertainment_highlights_gallery WHERE entertainment_id='".mysql_real_escape_string($entertainment_id)."' $q_search_value ORDER BY id";
					$result_photo=mysql_query($q_photo,$link) or die(mysql_error());
					while($row_photo=mysql_fetch_array($result_photo)) {
						$num1=$num1+1;
						$photo_id=$row_photo['id'];
						
						?>
						<div style="float:left;display:inline;margin:20px 10px 0 0"> 
						<table width="260" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="260" valign="top">
								<div style="background-color:#E6EEEE;padding:10px 10px 10px 15px;border:1px solid #CCCCCC">
								<?php echo "<img src='".$row_photo['image']."' width='200px'>"; ?>
								<table border="0" cellpadding="0" cellspacing="0">
									<?php /*
									<tr>
										<td width="200" valign="middle" height="40px">Caption<input type="text" name="caption[]" value="<?php echo $row_photo['caption']; ?>" style="width:200px"></td>
									</tr>
									<tr>
										<td width="200" valign="middle" height="40px">Category
										<select name="category[]">
										<option value="">Please Select</option>
										<?php
											$q_category="SELECT * FROM category_gallery ORDER BY category";
											$result_category=mysql_query($q_category,$link) or die(mysql_error());
											while($row_category=mysql_fetch_array($result_category)) {
												echo "<option value='".$row_category['id']."'";
												if($row_category['id']==$row_photo['CategoryID']) {
													echo "Selected";
												}
												echo ">".$row_category['category']."</option>";
											}
										?>
										</select>
										</td>
									</tr>
									*/
									
									?>
									<tr>
									  <td width="200" valign="middle"><input type="checkbox" name="delete_image<?php echo $photo_id; ?>" value="<? echo $row_photo['image_thumb']; ?>"> Delete</td>
									</tr>
									<?php if($row_photo['video']!="") { ?>
									<tr>
									  <td width="200" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="edit-entertainment-highlights-gallery.php?photo_id=<?php echo $photo_id; ?>&entertainment_id=<?php echo $_REQUEST['entertainment_id']; ?>">Edit</a></td>
									</tr>
									<?php } ?>
									<?php /*
									<tr>
									  <td width="200" valign="middle"><input type="radio" name="photo_album_cover" value="<?php echo $photo_id; ?>" onClick="SetPhotoAlbum(this.value)" <?php if($PhotoAlbumCover==1) { echo "checked"; } ?>> Photo Album Cover</td>
									</tr>
									<tr>
										<td width="200" valign="middle" height="40px">Sort <input type="text" name="sort[]" value="<?php echo $row_photo['sort']; ?>" style="width:100px"></td>
									</tr>
									*/
									?>
								  </table>
								</div>
								
						  </td>
						  </tr>
				  		</table>
						</div>
						<input type="hidden" name="photo_id[]" value="<?php echo $photo_id ?>">
				<?php 
					if($num1%3==0)
						echo "<div class='clearboth'></div>";
				} ?>		
				</div>
				<div class="clearboth"></div>
				<div style="text-align:center;margin:20px 0 0 0"><input type="submit" name="SaveChange" value="Save Change"></div>
				
		  		</form>
	
			
			
			
			
			
			
			
			
			
			
			
			
			
			
				
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

