<?php
session_start();
if (empty($_SESSION['cms_username'])) {
	header('Location:login.php');
	exit;
}

require_once '../function/helper.php';
$connects = db_write_connect($all = true);

# export
$mode = sprintf(
	'%s:%s', 
	(empty($_GET['year']) ? date('Y') : (int) $_GET['year']),
	(empty($_GET['type']) ? 'journalist' : (string) $_GET['type'])
);

switch ($mode) {
	case '2013:journalist':
		$sql = "select * from accredited_journalist_2013";
		$file = 'accredited_journalist_2013.csv';
		break;

	case '2013:photographer':
		$sql = "select * from accredited_photographer_crew_2013";
		$file = 'accredited_photographer_and_tv_crew_2013.csv';
		break;

	case '2013:av':
		$sqls = array(
			"incharge" => "select * from accredited_av_incharge_2013",
			"item" => "select * from accredited_av_2013"
		);
		$file = 'accredited_av_equipment_2013.csv';

		$res = array();
		foreach ($sqls as $type => $sql) {
			$res[$type] = db_query_all($connects, $sql);
		}

		# merge result based on "code" column
		$list = array();
		foreach ($res as $type => $rows) {
			foreach ($rows as $row) {
				$idx = $row['code'];

				if (!isset($list[$idx])) {
					$list[$idx] = array();
				}

				if (!isset($list[$idx][$type])) {
					$list[$idx][$type] = array();
				}

				$list[$idx][$type][] = $row;
			}
		}
		multi_csv($list, $file);
		exit;
		break;
}

# for normal usage
$res = db_query_all($connects, $sql);
save_as_csv($res, array_keys($res[0]), $file);

