<?php 
session_start();

include("include/configure.php");

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
	exit();
}

if(isset($_REQUEST['search_value'])) {
	$search_value=$_REQUEST['search_value'];
	if($search_value!="") {
		$q_search="AND (thumbnail_link LIKE '%$search_value%' OR video_link LIKE '%$search_value%' OR title LIKE '%$search_value%')";

	}
}


if(isset($_REQUEST['savesequence'])) {
	
	$sort_update=$_REQUEST['sort'];
	$sortid_update=$_REQUEST['sortid'];
	$count_update=count($sortid_update);

	for($i=0;$i<$count_update;$i++) {
	
		

		$q_update="UPDATE video_gallery_mp4 SET sort='".mysql_real_escape_string($sort_update[$i])."' WHERE id='".mysql_real_escape_string($sortid_update[$i])."'";
		$result_update=mysql_query($q_update,$link) or die(mysql_error());
		
		
		
	}
	
	
}





if(empty($_REQUEST['page']))
{
	$page=1;
}
else
{
	if($_REQUEST['page']=="")
	{
		$page=1;
	}
	else
	{
		$page=$_REQUEST['page'];
	}
}
			
$site = $_SERVER['PHP_SELF'];
$range=25;//the range of pages display
$limit=20;//the limit of rows display in a page

$sqlcount="SELECT id FROM video_gallery_mp4 WHERE id!='' $q_search ORDER BY sort";
$resultcount=mysql_query($sqlcount,$link)or die(mysql_error());
$count=mysql_num_rows($resultcount);
$totalpage=ceil($count/$limit);
					
$limitvalue=($page*$limit)-$limit;

if($_GET['orderby']=="") {
	$orderby="sort";
} else {
	$orderby=$_GET['orderby'];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/ajax.js"></script>
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Video Gallery  Listing</div>
		
		<div class="searching_field">
		<form action="<?php $PHP_SELF; ?>" method="post">
		<table width="248" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="46">Search</td>
			<td width="133"><input type="text" name="search_value" value="<?php echo stripslashes($search_value); ?>"></td>
			<td width="69"><input type="submit" name="search" value="search"></td>
		</tr>
		</table>
		
		</div>
		<div class="clearboth"></div>
		<div class="functionlink"><a href="add-new-video-gallery.php">Add Video Gallery</a></div>
		<div id="content_list">
			<table width="760" align="left" cellpadding="5" cellspacing="0" style="border-top:1px solid #B6BEBE;">
			<tr class="blue_column">
				<td width="27" height="26" class="functionlink bottomline leftline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=sort&search_value=$search_value"; ?>">No</a></strong></td>
				<td width="136" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=thumbnail_link&search_value=$search_value"; ?>">Video Thumbnail</a></strong></td>
				<td width="147" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=title&search_value=$search_value"; ?>">Title</a></strong></td>
				<td width="134" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=video_link&search_value=$search_value"; ?>">Video MP4 Link</a></strong></td>
				<td width="119" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=video_flv_link&search_value=$search_value"; ?>">Video FLV Link</a></strong></td>
				<td width="58" class="functionlink bottomline"><strong>Display Order</strong></td>
				<td width="21" class="bottomline" align="center"><strong>Edit</strong></td>
				<td width="36" class="bottomline" align="center"><strong>Delete</strong></td>
			</tr>
			<?php 
			   if($page!=1 && $page!="") {
						$pagecount=$page-1;
						$num=$pagecount * $limit;
			   	}
				$q="SELECT * FROM video_gallery_mp4 WHERE id!='' $q_search ORDER BY $orderby LIMIT $limitvalue,$limit";
			   $result=mysql_query($q,$link) or die(mysql_error());
			   while($row=mysql_fetch_array($result)) {
			  	 	$num=$num+1;
					
					
					if($row['video_link']=="") {
						$row['video_link']=$row['video_youtube_link'];
						
					}
					
					if($row['video_flv_link']=="") {
						$row['video_flv_link']=$row['video_youtube_link'];
					}
					
					
					
			   		?>
					<tr>
						<td width="27" class="bottomline leftline" valign="top">
							<?php echo $num; ?>
							<input type="radio" name="featured_video" value="<?php echo $row['id']; ?>" onClick="ChangeFeaturedVideo('<?php echo $row['id']; ?>')" <?php if($row['featured_video']=="1") { echo "checked"; } ?>>
						</td>
						<td width="136" class="bottomline" valign="top"><img src="<?php echo $row['thumbnail_link']; ?>" width="150"></td>
						<td width="147" class="bottomline" valign="top"><?php echo $row['title']; ?></td>
						<td width="134" class="bottomline" valign="top"><a href="<?php echo $row['video_link']; ?>" target="_blank"><?php echo $row['video_link']; ?></a></td>
						<td width="119" class="bottomline" valign="top"><a href="<?php echo $row['video_flv_link']; ?>" target="_blank"><?php echo $row['video_flv_link']; ?></a></td>
						<td width="58" class="functionlink bottomline" align="center" valign="top">
						<input type="text" name="sort[]" value="<?php echo $row['sort']; ?>" size="6">
						</td>
						<td width="21" class="functionlink bottomline" align="center" valign="top">
							<a href="edit-video-gallery.php?id=<?php echo $row['id']; ?>&page=<?php echo $page; ?>&orderby=<?php echo $orderby; ?>&search_value=<?php echo $search_value; ?>"><img src="images/edit.jpg" border="none"></a>
						
						</td>
						<td width="36" class="functionlink bottomline" align="center" valign="top">
						
							<a href="javascript:;" onClick="DeleteVideoGallery('<?php echo $row['id']; ?>')"><img src="images/delete.gif" border="none"></a>
						
						</td>
					</tr>
					<input type="hidden" name="sortid[]" value="<?php echo $row['id']; ?>">
					<?php
			   }
			?>
			</table>
			
			
			 <div class="clearboth"></div>
			  <br/><br/>
           		 <input type="submit" name="savesequence" value="Update Display Order">
				</form>
             	  <br/><br/>
				
				<table border="0" cellspacing="0" cellpadding="0" class="page_table" align="left">
				<tr>
				<td align="left" class="style5"><?php
				$lrange = max(1,$page-(($range-1)/2));
				$rrange = min($totalpage,$page+(($range-1)/2));
				if (($rrange - $lrange) < ($range - 1))
					{
						if ($lrange == 1)
							{
								$rrange = min($lrange + ($range-1), $totalpage);
							}
						else
							{
								$lrange = max($rrange - ($range-1), 0);
							}
					}
				if($totalpage>1)
				{
					if ($page > 1)//previous page
						{
							$prev=$page-1;
							echo " <a href='".$PHP_SELF."?page=1&orderby=$orderby&search_value=$search_value'><< First</a> | "; 
							echo " <a href='".$PHP_SELF."?page=$prev&orderby=$orderby&search_value=$search_value'>< Prev</a> | ";
						}
					else
						{
							echo " <strong><< First</strong> | <strong>< Prev</strong> |";
						}
					for($i = 1; $i <= $totalpage; $i++)//the range of page
						{
							if ($i == $page)
								{
									echo " <span class='actual_page'><strong>$i</strong></span>";
								}
							else
								{
									if ($lrange <= $i and $i <= $rrange)
										{
											echo " <a href='".$PHP_SELF."?page=$i&orderby=$orderby&search_value=$search_value' class='bluecolor word fontsubtitle'>$i</a> ";
										}
								}
						}
					if ($page < $totalpage)//next page
						{
							$next=$page+1;
							echo "| <a href='".$PHP_SELF."?page=$next&orderby=$orderby&search_value=$search_value'>Next ></a> |";
							echo " <a href='".$PHP_SELF."?page=$totalpage&orderby=$orderby&search_value=$search_value'>Last >></a>";
							
						}
					else
						{
							echo " | <strong>Next ></strong> | <strong>Last >></strong>";
						}
				}//end of if $totalpage > 1
			?></td>
			  </tr>
			</table>
				
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

