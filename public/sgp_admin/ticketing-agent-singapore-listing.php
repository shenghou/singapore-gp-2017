<?php 
session_start();

include("include/configure.php");

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
	exit();
}

if(isset($_REQUEST['search_value'])) {
	$search_value=$_REQUEST['search_value'];
	if($search_value!="") {
		$q_search="AND (region LIKE '%$search_value%' OR location_name LIKE '%$search_value%' OR location_address LIKE '%$search_value%' OR operating_hours LIKE '%$search_value%')";

	}
}


if(isset($_REQUEST['savesequence'])) {
	
	$sort_update=$_REQUEST['sort'];
	$sortid_update=$_REQUEST['sortid'];
	$count_update=count($sortid_update);

	for($i=0;$i<$count_update;$i++) {
	
		

		$q_update="UPDATE ticketing_agent_singapore SET sort='".mysql_real_escape_string($sort_update[$i])."' WHERE id='".mysql_real_escape_string($sortid_update[$i])."'";
		$result_update=mysql_query($q_update,$link) or die(mysql_error());

		
	}
	
	
}


if(isset($_POST['update_status'])) {
	$module_status=$_POST['status'];
	$module_writeup=$_POST['write_up'];
	$module_writeup_chinese=$_POST['write_up_chinese'];
	
	$q_m_status="UPDATE status_module SET status='".mysql_real_escape_string($module_status)."',write_up='".mysql_real_escape_string($module_writeup)."',write_up_chinese='".mysql_real_escape_string($module_writeup_chinese)."' WHERE  status_module_name='Ticketing Agents Singapore'";
	$result_m_status=mysql_query($q_m_status,$link) or die(mysql_error());
	

}



if(empty($_REQUEST['page']))
{
	$page=1;
}
else
{
	if($_REQUEST['page']=="")
	{
		$page=1;
	}
	else
	{
		$page=$_REQUEST['page'];
	}
}
			
$site = $_SERVER['PHP_SELF'];
$range=25;//the range of pages display
$limit=25;//the limit of rows display in a page

$sqlcount="SELECT id FROM ticketing_agent_singapore WHERE id!='' $q_search ORDER BY id";
$resultcount=mysql_query($sqlcount,$link)or die(mysql_error());
$count=mysql_num_rows($resultcount);
$totalpage=ceil($count/$limit);
					
$limitvalue=($page*$limit)-$limit;

if($_GET['orderby']=="") {
	$orderby="region,sort";
} else {
	$orderby=$_GET['orderby'];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/ajax.js"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript">

tinyMCE.init({

        // General options
		
		
        mode: "exact",

    	elements: "write_up,write_up_chinese",

        theme : "advanced",

        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

file_browser_callback : 'tinyBrowser',

        // Theme options

        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect",

       	theme_advanced_buttons2 : "fontsizeselectcut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview",

       theme_advanced_buttons3 : "forecolor,backcolor,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",

       theme_advanced_buttons4 : "print,|,ltr,rtl,|,fullscreen,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",

        theme_advanced_toolbar_location : "top",

        theme_advanced_toolbar_align : "left",
		
		

        theme_advanced_statusbar_location : "bottom",

        theme_advanced_resizing : true,

		theme_advanced_blockformats : "p,div,address,pre,h1,h2,h3,h4,h5,h6",

		forced_root_block : '',

        // Skin options

        skin : "o2k7",

        skin_variant : "silver",

		
        // Example content CSS (should be your site CSS)

        content_css : "../css/page.css,../css/txtcolor.css",



        // Drop lists for link/image/media/template dialogs

        template_external_list_url : "js/template_list.js",

        external_link_list_url : "js/link_list.js",

        external_image_list_url : "js/image_list.js",

        media_external_list_url : "js/media_list.js",

		convert_urls : false,
		
		forced_root_block : 'p',
		
		

        // Replace values for the template plugin

        template_replace_values : {

                username : "Some User",

                staffid : "991234"

        }
		

});

</script>
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Authorized Ticketing Agents (Singapore) Listing</div>
		
		<div class="searching_field">
<form method="post">
		<table width="248" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="46">Search</td>
			<td width="133"><input type="text" name="search_value" value="<?php echo stripslashes($search_value); ?>"></td>
			<td width="69"><input type="submit" name="search" value="search"></td>
		</tr>
		</table>
		
		</div>
		<div class="clearboth"></div>
</form>	

<form method="post">
		<?php
		$q_m_status="SELECT status,write_up,write_up_chinese FROM status_module WHERE  status_module_name='Ticketing Agents Singapore'";
		$result_m_status=mysql_query($q_m_status,$link) or die(mysql_error());
		$row_m_status=mysql_fetch_array($result_m_status);
		?>
		<table width="773" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="109">Status:</td>
			<td width="641">
			<select name="status">
				<option value="Active" <?php if($row_m_status['status']=="Active") { echo "selected"; } ?>>Active</option>
				<option value="Inactive" <?php if($row_m_status['status']=="Inactive") { echo "selected"; } ?>>In Active</option>
			</select>
			</td>
			<td width="23"></td>
		</tr>
		<tr>
			<td width="109"></td>
			<td width="641">&nbsp;
			
			</td>
			<td width="23"></td>
		</tr>
		<tr>
			<td width="109">Write Up:</td>
			<td width="641">
			<textarea name="write_up"><?php echo $row_m_status['write_up']; ?></textarea>
			</td>
			<td width="23"></td>
		</tr>
		<tr>
			<td width="109"></td>
			<td width="641">&nbsp;
			
			</td>
			<td width="23"></td>
		</tr>
		<tr>
			<td width="109">Write Up Chinese:</td>
			<td width="641">
			<textarea name="write_up_chinese"><?php echo $row_m_status['write_up_chinese']; ?></textarea>
			</td>
			<td width="23"></td>
		</tr>
		<tr>
			<td width="109"></td>
			<td width="641">&nbsp;
			
			</td>
			<td width="23"></td>
		</tr>
		<tr>
			<td width="109"></td>
			<td>
			<input type="submit" name="update_status" value="Update Status">
			</td>
			<td width="23"></td>
		</tr>
		</table>
</form>
		
		<div class="clearboth"></div>
		<br>
		<br>

<form method="post">
		<div class="functionlink"><a href="add-new-ticketing-agent-singapore.php">Add New Authorized Ticketing Agents (Singapore)</a></div>
		<div id="content_list">
			<table width="760" align="left" cellpadding="5" cellspacing="0" style="border-top:1px solid #B6BEBE;">
			<tr class="blue_column">
				<td width="33" class="functionlink bottomline leftline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=region,sort&search_value=$search_value"; ?>">No</a></strong></td>
				<td width="70" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=region&search_value=$search_value"; ?>">Region</a></strong></td>
				<td width="185" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=location_name&search_value=$search_value"; ?>">Location Name</a></strong></td>
				<td width="177" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=location_address&search_value=$search_value"; ?>">Location Address</a></strong></td>
				<td width="153" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=operating_hours&search_value=$search_value"; ?>">Operating Hours</a></strong></td>
				<td width="62" class="functionlink bottomline"><strong>Display Order</strong></td>
				<td width="27" class="bottomline" align="center"><strong>Edit</strong></td>
				<td width="43" class="bottomline" align="center"><strong>Delete</strong></td>
			</tr>
			<?php 
			   if($page!=1 && $page!="") {
						$pagecount=$page-1;
						$num=$pagecount * $limit;
			   	}
				$q="SELECT * FROM ticketing_agent_singapore WHERE id!='' $q_search ORDER BY $orderby LIMIT $limitvalue,$limit";
			   $result=mysql_query($q,$link) or die(mysql_error());
			   while($row=mysql_fetch_array($result)) {
			  	 	$num=$num+1;
					
					$ori = array("\n");
					$replace_word=array("<br>");
						
					$row['location_address']= str_replace($ori, $replace_word, $row['location_address']);
					$row['operating_hours']= str_replace($ori, $replace_word, $row['operating_hours']);
					
			   		?>
					<tr>
						<td width="33" class="bottomline leftline"><?php echo $num; ?></td>
						<td width="70" class="bottomline"><?php echo $row['region']; ?></td>
						<td width="185" class="bottomline"><?php echo $row['location_name']; ?></td>
						<td width="177" class="bottomline"><?php echo $row['location_address']; ?></td>
						<td width="153" class="bottomline"><?php echo $row['operating_hours']; ?></td>
						<td width="62" class="functionlink bottomline" valign="top" align="center">
						<input type="text" name="sort[]" value="<?php echo $row['sort']; ?>" size="6">
						</td>
						<td width="27" class="functionlink bottomline" align="center">
							<a href="edit-ticketing-agent-singapore.php?id=<?php echo $row['id']; ?>&page=<?php echo $page; ?>&orderby=<?php echo $orderby; ?>&search_value=<?php echo $search_value; ?>"><img src="images/edit.jpg" border="none"></a>
						
						</td>
						<td width="43" class="functionlink bottomline" align="center">
						
							<a href="javascript:;" onClick="DeleteTicketingAgentSingapore('<?php echo $row['id']; ?>')"><img src="images/delete.gif" border="none"></a>
						
						</td>
					</tr>
					<input type="hidden" name="sortid[]" value="<?php echo $row['id']; ?>">
					<?php
			   }
			?>
			</table>
			
			
			 <div class="clearboth"></div>
			  <br/><br/>
           		
             	<input type="submit" name="savesequence" value="Update Display Order">
</form>
				<table border="0" cellspacing="0" cellpadding="0" class="page_table" align="left">
				<tr>
				<td align="left" class="style5"><?php
				$lrange = max(1,$page-(($range-1)/2));
				$rrange = min($totalpage,$page+(($range-1)/2));
				if (($rrange - $lrange) < ($range - 1))
					{
						if ($lrange == 1)
							{
								$rrange = min($lrange + ($range-1), $totalpage);
							}
						else
							{
								$lrange = max($rrange - ($range-1), 0);
							}
					}
				if($totalpage>1)
				{
					if ($page > 1)//previous page
						{
							$prev=$page-1;
							echo " <a href='".$PHP_SELF."?page=1&orderby=$orderby&search_value=$search_value'><< First</a> | "; 
							echo " <a href='".$PHP_SELF."?page=$prev&orderby=$orderby&search_value=$search_value'>< Prev</a> | ";
						}
					else
						{
							echo " <strong><< First</strong> | <strong>< Prev</strong> |";
						}
					for($i = 1; $i <= $totalpage; $i++)//the range of page
						{
							if ($i == $page)
								{
									echo " <span class='actual_page'><strong>$i</strong></span>";
								}
							else
								{
									if ($lrange <= $i and $i <= $rrange)
										{
											echo " <a href='".$PHP_SELF."?page=$i&orderby=$orderby&search_value=$search_value' class='bluecolor word fontsubtitle'>$i</a> ";
										}
								}
						}
					if ($page < $totalpage)//next page
						{
							$next=$page+1;
							echo "| <a href='".$PHP_SELF."?page=$next&orderby=$orderby&search_value=$search_value'>Next ></a> |";
							echo " <a href='".$PHP_SELF."?page=$totalpage&orderby=$orderby&search_value=$search_value'>Last >></a>";
							
						}
					else
						{
							echo " | <strong>Next ></strong> | <strong>Last >></strong>";
						}
				}//end of if $totalpage > 1
			?></td>
			  </tr>
			</table>
				
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

