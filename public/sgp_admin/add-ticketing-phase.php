<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");

$id=$_REQUEST['id'];

if(isset($_POST['submit'])) {
	$error=0;
	
	$phase_description=$_POST['phase_description'];
	$phase_description_chinese=$_POST['phase_description_chinese'];
	$phase_date=$_POST['phase_date'];
	$phase_date_chinese=$_POST['phase_date_chinese'];
	

	if($phase_description=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Ticketing Phase Description.";
	}
	if($phase_description_chinese=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Ticketing Phase Chinese Description.";
	}
	if($phase_date=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Phase Date.";
	}
	if($phase_date_chinese=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Phase Chinese Date.";
	}
	
	
	
	if($error!=1) {

			$datetime=date("Y-m-d H:i:s");
			

			
			$q="INSERT INTO ticketing_phase (phase_description,phase_description_chinese,phase_date,phase_date_chinese,status,updater) VALUES ('".mysql_real_escape_string(addslashes($phase_description))."','".mysql_real_escape_string(addslashes($phase_description_chinese))."','".mysql_real_escape_string(addslashes($phase_date))."','".mysql_real_escape_string(addslashes($phase_date_chinese))."','1','".mysql_real_escape_string($_SESSION[cms_username])."')";
			$result=mysql_query($q,$link) or die(mysql_error());

			header('Location:ticketing-phase-listing.php?page='.$_REQUEST[page].'&orderby='.$_REQUEST[orderby].'&search_value='.$_REQUEST[search_value].'');
			exit();
			
		
	}

} 


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Add New Ticketing Phase</div>
		<div id="back_to_list"><a href="ticketing-phase-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post">
			<div>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Ticket Phase Description  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="phase_description"  value="<?php echo stripslashes($phase_description); ?>" style="width:500px" >
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Ticket Phase Chinese Description  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="phase_description_chinese"  value="<?php echo $phase_description_chinese; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Phase Date  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="phase_date"  value="<?php echo $phase_date; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Phase Date Chinese : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="phase_date_chinese"  value="<?php echo $phase_date_chinese; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				
				
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Add New Ticketing Phase"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				
			  </div>
		  </form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

