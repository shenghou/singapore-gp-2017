<?php 
session_start();
if(!($_SERVER && isset($_SESSION['cms_username'])))
{
	header('Location:login.php');
	exit;
}
include("include/configure.php");

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<body>
<div id="container">
	
	<div id="top_container" align="center" style="border-bottom:1px solid #BAB9B9 "><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		
	  <div id="title" style="text-align:center ">Hi, <?php echo $_SESSION['cms_name']; ?>.<br> 
	    Welcome to the Content Management System<br>
 for
  <br>
 <?php echo date('Y');?> Formula <?php echo date('Y');?> !</div>
		<div id="content" style="text-align:center">
				<div>
					<br>
					Your are currently logged in as the <strong><?php echo $_SESSION['cms_username']; ?></strong>
					<br>
					<br>
					Today is <?php echo date("l, F d, Y"); ?>
					<br>
				</div>
				
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

