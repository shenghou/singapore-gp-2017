<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");

$errorword = "";
$id=(isset($_REQUEST['id'])?$_REQUEST['id']:"");
$CategoryID=(isset($_POST['CategoryID'])?$_POST['CategoryID']:"");


if(isset($_POST['submit'])) {
	$error=0;
	$news_title=$_POST['news_title'];
	$news_date=$_POST['news_date'];
	$content=$_POST['content'];
	
	
	if($news_title=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert News Title.";
	}
	if($news_date=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert News Date.";
	}
	if($content=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Content.";
	}
	
	if($error!=1) {
	
		$news_date_array=explode("/",$news_date);
		$news_date="$news_date_array[2]-$news_date_array[1]-$news_date_array[0]";
		
		
			$q="INSERT INTO news (news_title,news_date,content,creator) VALUES ('$news_title','$news_date','$content','$_SESSION[tdsi_username]')";
			$result=mysqli_query($link, $q) or die(mysqli_error());

			if (isset($link1) && is_resource($link1)) {
				mysqli_query($link1, $q) or die(mysqli_error());
			}

			if (isset($link2) && is_resource($link2)) {
				mysqli_query($link2, $q) or die(mysqli_error());
			}

			header('Location:list-news.php');
			exit();
		
	}

}


$q="SELECT * FROM photo_album WHERE id='$id'";
$result=mysqli_query($link, $q) or die(mysqli_error());
$row=mysqli_fetch_array($result);

$title=$row['title'];
$description=$row['description'];

$ori = array("\n");
$replace_word=array("<br>");
						
$description = str_replace($ori, $replace_word, $description);

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="uploadify/uploadify.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery.uploadify.js"></script>
<script type="text/javascript">

$(document).ready(function() {
	
	$("#fileUpload2").fileUpload({
		'uploader': 'uploadify/uploader.swf',
		'cancelImg': 'uploadify/cancel.png',
		'script': 'uploadify/upload.php',
		'folder': 'files',
		'multi': true,
		'fileDesc': 'Image Files',
		'fileExt': '*.jpg;*.jpeg;*.gif',
		'buttonText': 'Select Files',
		'displayData': 'percentage',
		'simUploadLimit': 2,
		'maxQueueSize': 9,
		
		'scriptData': {'id':'<?php echo $id; ?>', 'CategoryID': '<?php echo $CategoryID; ?>'},
		'onAllComplete': function(event,data){
			location.href='view-photo-album.php?id=<?php echo $id; ?>';
	     }
	});
});

function formSubmit() {
	$('#form1').submit();
}

</script>
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Add to Photo </div>
		<div id="back_to_list"><a href="view-photo-album.php?id=<?php echo $id; ?>">Back to List</a></div>
		<div id="content">
			<form id="form1" action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
			<div>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				
				<tr>
					<td width="126" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="126" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Title   : </strong></td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;"><?php echo $title; ?></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="126" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Description   : </strong></td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;"><?php echo $description; ?></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="126" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Category   : </strong></td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;">
						<select name="CategoryID" id="CategoryID" onchange="formSubmit();">
							<option value="">Please Select</option>
							<option value="1" <?=("1"==$CategoryID?"selected":"")?>>Race</option>
							<option value="2" <?=("2"==$CategoryID?"selected":"")?>>Drivers</option>
							<option value="3" <?=("3"==$CategoryID?"selected":"")?>>Circuit Park</option>
							<option value="4" <?=("4"==$CategoryID?"selected":"")?>>Pre-Race</option>
							<option value="5" <?=("5"==$CategoryID?"selected":"")?>>Entertainment</option>
							<option value="6" <?=("6"==$CategoryID?"selected":"")?>>Roving Performances</option>
						</select>

					</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>


				
				<tr>
					<td bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Upload Photo Gallery</strong></td>
					<td style="border-bottom:1px dotted #BAB9B9;">
					
					<div id="fileUpload2">You have a problem with your javascript</div>
					<div>Click <strong>"Select Files"</strong> to upload multiple photos <i>(* Maximum 20 photos allowed)</i></div>
					<br>
					<br>
			
					<div class="functionlink"><a href="javascript:$('#fileUpload2').fileUploadStart()"><img src="images/start_over.png" border="none"></a>   <a href="javascript:$('#fileUpload2').fileUploadClearQueue()"><img src="images/clear_queue_over.png" border="none"></a></div>
					<p></p>
					</td>
				<td style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				
					<td></td>
					<td></td>
				<td>&nbsp;</td>
				</tr>
				</table>
			  </div>
		  </form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

