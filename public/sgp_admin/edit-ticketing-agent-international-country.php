<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");

$id=$_REQUEST['id'];

if(isset($_POST['submit'])) {
	$error=0;

	
	$region_id=$_POST['region_id'];
	$country=$_POST['country'];
	$country_chinese=$_POST['country_chinese'];
	
	
	
	if($region_id=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please select Region.";
	}
	if($country=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Country.";
	}
	
	
	
	if($error!=1) {

			
			$q="UPDATE ticketing_agent_inter_country SET region_id='".mysql_real_escape_string($region_id)."',country='".mysql_real_escape_string($country)."',country_chinese='".mysql_real_escape_string($country_chinese)."',updater='".mysql_real_escape_string($_SESSION[cms_username])."' WHERE id='".mysql_real_escape_string($id)."'";
			$result=mysql_query($q,$link) or die(mysql_error());

			
			
			header('Location:ticketing-agent-international-country-listing.php?page='.$_REQUEST[page].'&orderby='.$_REQUEST[orderby].'&search_value='.$_REQUEST[search_value].'');
			exit();
			
		
	}

} else {
	
	$q="SELECT * FROM ticketing_agent_inter_country WHERE id='$_REQUEST[id]'";
	$result=mysql_query($q,$link) or die(mysql_error());
	$row=mysql_fetch_array($result);
	
	$region_id=$row['region_id'];
	$country=$row['country'];
	$country_chinese=$row['country_chinese'];
	
	
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />

<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Edit Authorized Ticketing Agents (International) Country </div>
		<div id="back_to_list"><a href="ticketing-agent-international-country-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post">
			<div>
				
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Region  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="region_id">
						<option value="">Please Select</option>
						<?php 
						$q_region="SELECT id,region FROM ticketing_agent_inter_region ORDER BY id";
						$result_region=mysql_query($q_region,$link) or die(mysql_error());
						while($row_region=mysql_fetch_array($result_region)) {
							echo "<option value='".$row_region['id']."'";
							if($region_id==$row_region['id']) { echo "selected"; }
							echo ">".$row_region['region']."</option>";
						
						}
						?>
					</select>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Country  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="country"  value="<?php echo $country; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Country Chinese  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="country_chinese"  value="<?php echo $country_chinese; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Update Authorized Ticketing Agents (International) Country"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
			  </div>
		  </form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

