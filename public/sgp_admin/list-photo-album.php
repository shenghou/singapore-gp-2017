<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
	exit();
}

$search_value = (isset($_GET['search_value'])?$_GET['search_value']:"");

$q_search = "";

if(isset($_REQUEST['search_value'])) {
	$search_value=$_REQUEST['search_value'];
	if($search_value!="") {
		$q_search="AND (title LIKE '%$search_value%' OR description LIKE '%$search_value%')";

	}
}


include("include/configure.php");


if(empty($_REQUEST['page']))
{
	$page=1;
}
else
{
	if($_REQUEST['page']=="")
	{
		$page=1;
	}
	else
	{
		$page=$_REQUEST['page'];
	}
}
			
$site = $_SERVER['PHP_SELF'];
$range=25;//the range of pages display
$limit=20;//the limit of rows display in a page

$sqlcount="SELECT id FROM photo_album WHERE id!='' $q_search ORDER BY year DESC,date";
$resultcount=mysqli_query($link, $sqlcount)or die(mysqli_error($link));
$count=mysqli_num_rows($resultcount);
$totalpage=ceil($count/$limit);
					
$limitvalue=($page*$limit)-$limit;

$orderby="year DESC,date";

if(isset($_GET['orderby'])) {
	$orderby=$_GET['orderby'];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/ajax.js"></script>
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Manage Photo Gallery </div>
		
		
		
		<div class="searching_field">
		<form action="<?php $_SERVER['PHP_SELF']; ?>" method="get">
		<table width="248" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="46">Search</td>
			<td width="133"><input type="text" name="search_value" value="<?php echo stripslashes($search_value); ?>"></td>
			<td width="69"><input type="submit" name="search" value="search"></td>
		</tr>
		</table>
		</form>
		</div>
		<div class="clearboth"></div>
		<?php
			$record_year_from = date("Y")."-01-01";
			$can_edit_album = false;
			if($_SESSION['cms_role']==2) {
				$record_year_from="1900";
				$can_edit_album = true;
				?>
			<div class="functionlink"><a href="add-photo-album.php">Add New Photo Album</a></div>
		<?php } ?>

		<div class="clearboth"></div>
		
		<div id="content_list">
			<table width="760" align="left" cellpadding="5" cellspacing="0" style="border-top:1px solid #B6BEBE;">
			<tr class="blue_column">
				<td width="40" class="functionlink bottomline leftline"><strong><a href="<?php echo "".$_SERVER['PHP_SELF']."?orderby=year DESC,date&search_value=$search_value"; ?>">No</a></strong></td>
				<td width="97" class="functionlink bottomline"><strong><a href="<?php echo "".$_SERVER['PHP_SELF']."?orderby=AlbumDate&search_value=$search_value"; ?>">Album Date</a></strong></td>
				<td width="231" class="functionlink bottomline"><strong><a href="<?php echo "".$_SERVER['PHP_SELF']."?orderby=title&search_value=$search_value"; ?>">Title</a></strong></td>
				<td width="293" class="functionlink bottomline"><strong><a href="<?php echo "".$_SERVER['PHP_SELF']."?orderby=description&search_value=$search_value"; ?>">Description</a></strong></td>
				<td width="47" class="bottomline"><strong>Manage Album </strong></td>
				<td width="47" class="bottomline"><strong>Edit Album</strong></td>
			</tr>
			<?php 
			   if($page!=1 && $page!="") {
						$pagecount=$page-1;
						$num=$pagecount * $limit;
			   	}
				$q="SELECT * FROM photo_album WHERE id!='' and AlbumDate > '$record_year_from' $q_search ORDER BY $orderby LIMIT $limitvalue,$limit";
			$result=mysqli_query($link, $q) or die(mysqli_error($link));
				$num=0;
			   while($row=mysqli_fetch_array($result)) {
			  	 	$num=$num+1;
					
					$ori = array("\n");
					$replace_word=array("<br>");

					$row['description'] = str_replace($ori, $replace_word, $row['description']);
			   		?>
					<tr>
						<td width="40" class="bottomline leftline" valign="top"><?php echo $num; ?></td>
						<td width="97" class="bottomline" valign="top"><?php echo date('j/n/Y', strtotime($row['AlbumDate'])); ?></td>
						<td width="231" class="bottomline" valign="top"><?php echo $row['title']; ?></td>
						<td width="293" class="bottomline" valign="top"><?php echo $row['description']; ?>&nbsp;</td>
						<td width="47" class="bottomline" valign="top" align="center"><a href="view-photo-album.php?id=<?php echo $row['id']; ?>"><img src="images/view_icon.gif" border="none"></a>
						<td width="47" class="bottomline" valign="top" align="center">
							<?php if ($can_edit_album) { ?>
							<a href="edit-photo-album.php?id=<?php echo $row['id']; ?>"><img src="images/edit.jpg" border="none"></a>
							<?php } ?>
							</td>
					</tr>
					<?php
			   }
			?>
			</table>
			
			
			 <div class="clearboth"></div>
				<table border="0" cellspacing="0" cellpadding="0" class="page_table" align="left">
				<tr>
				<td align="left" class="style5"><?php
				$lrange = max(1,$page-(($range-1)/2));
				$rrange = min($totalpage,$page+(($range-1)/2));
				if (($rrange - $lrange) < ($range - 1))
					{
						if ($lrange == 1)
							{
								$rrange = min($lrange + ($range-1), $totalpage);
							}
						else
							{
								$lrange = max($rrange - ($range-1), 0);
							}
					}
				if($totalpage>1)
				{
					if ($page > 1)//previous page
						{
							$prev=$page-1;
							echo " <a href='".$_SERVER['PHP_SELF']."?page=1&orderby=$orderby&search_value=$search_value'><< First</a> | "; 
							echo " <a href='".$_SERVER['PHP_SELF']."?page=$prev&orderby=$orderby&search_value=$search_value'>< Prev</a> | ";
						}
					else
						{
							echo " <strong><< First</strong> | <strong>< Prev</strong> |";
						}
					for($i = 1; $i <= $totalpage; $i++)//the range of page
						{
							if ($i == $page)
								{
									echo " <span class='actual_page'><strong>$i</strong></span>";
								}
							else
								{
									if ($lrange <= $i and $i <= $rrange)
										{
											echo " <a href='".$_SERVER['PHP_SELF']."?page=$i&orderby=$orderby&search_value=$search_value' class='bluecolor word fontsubtitle'>$i</a> ";
										}
								}
						}
					if ($page < $totalpage)//next page
						{
							$next=$page+1;
							echo "| <a href='".$_SERVER['PHP_SELF']."?page=$next&orderby=$orderby&search_value=$search_value'>Next ></a> |";
							echo " <a href='".$_SERVER['PHP_SELF']."?page=$totalpage&orderby=$orderby&search_value=$search_value'>Last >></a>";
							
						}
					else
						{
							echo " | <strong>Next ></strong> | <strong>Last >></strong>";
						}
				}//end of if $totalpage > 1
			?></td>
			  </tr>
			</table>
				
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

