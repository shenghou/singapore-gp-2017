<?php 
require_once '../function/helper.php';
require_once '../function/cms-helper.php';

check_session_exists();

# form init
$spaces = str_repeat('&nbsp;', 5);
$region = array(
	'NORTH' => 'NORTH'.$spaces, 
	'CENTRAL' => 'CENTRAL'.$spaces,
	'EAST' => 'EAST'.$spaces,
	'WEST' => 'WEST'.$spaces
);

$form = array(
	'region' => array('mandatory'=>true, 'label'=>'Region *', 'type'=>'radio', 'selection'=>$region),
	'location_name' => array('mandatory'=>true, 'label'=>'Location Name *', 'maxlength'=>255),
	'additional_info' => array('label'=>'Additional Info', 'maxlength'=>255),
	'location_address' => array('mandatory'=>true, 'label'=>'Location Address *', 'type'=>'default_textarea', 'maxlength'=>255),
	'operating_hours' => array('mandatory'=>true, 'label'=>'Operating Hours *', 'type'=>'default_textarea', 'maxlength'=>255),
	'latitude' => array('mandatory'=>true, 'label'=>'Latitude *', 'maxlength'=>20, 'style'=>'width:120px;'),
	'longitude' => array('mandatory'=>true, 'label'=>'Longitude *', 'maxlength'=>20, 'style'=>'width:120px;'),
);

$status = null;
$table = 'ticketing_agent_singapore';
$errors = array();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	foreach ($form as $key => $arr) {
		if (!empty($arr['mandatory']) && empty($_POST[$key])) {
			$errors[] = sprintf('%s are mandatory', str_replace(' *', '', $arr['label']));
			$status = false;
		}
	}

	if (empty($errors)) { 
		$opt['table'] = $table;
		$opt['value'] = $_POST;
		$status = db_insert($opt);
		if ($status) {
			$id = db_last_id();
			header(sprintf('location: edit-ticketing-agent-singapore.php?id=%d', $id));
		} else {
			$row = $_POST;
		}
	} else {
		$row = $_POST;
	}
}

# page setting
$h1 = 'Authorized Ticketing Agents (Singapore)';
$sort_opt = array('page', 'orderby', 'search_value');
$sort_url = sprintf('ticketing-agent-singapore-listing.php?%s', set_query_string($sort_opt));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - <?php echo $h1;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/cms.css" rel="stylesheet" type="text/css" />
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
<?php
if (is_bool($status)) {
	if ($status === true) {
		echo '<div class="success">Insert Successfully!</div>';
	} else {
		if (empty($errors)) {
			$msg = 'Unable to insert, please try again later.';
		} else {
			$msg = sprintf('<ul><li>%s</ul>', implode('<li>', $errors));
		}

		printf('<div class="fail">%s</div>', $msg);
	}
}
?>
		<h1>Add New <?php echo $h1;?></h1>
		<div id="back_to_list">
			<a href="<?php echo $sort_url;?>">Back to List</a>
		</div>

<!-- form -->
<form method="post">
<table border="0" align="left" cellpadding="6" cellspacing="0" class="form">
<?php
if (empty($row)) {
	$row = array_fill_keys(array_keys($form), null);
}

generate_form($form, $row);
?>
</table>

<div class="button-div">
	<button class="submit-button" type="submit">Insert</button>
	<button class="reset-button" type="reset">Reset</button>
</div>
</form>

<h4>* Latitude and Longitude can find at <a href="http://itouchmap.com/latlong.html" target="_blank">here</a></h4>
<h4>* Mandatory Field</h4>
<!-- end form -->

	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

