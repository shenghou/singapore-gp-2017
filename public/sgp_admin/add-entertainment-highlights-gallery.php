<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");
include ("include/resize.image.class.php");
include_once("_CrossFileUploadManager.php");

function getExtension($str) {

$i = strrpos($str,".");
if (!$i) 
{ 
	return ""; 
}
$l = strlen($str) - $i;
$ext = substr($str,$i+1,$l);

return $ext;

}

$entertainment_id=$_REQUEST['entertainment_id'];

if(isset($_POST['submit'])) {
	$error=0;

	
	
	if($_FILES['image']['name'][0]=="" && $_FILES['image']['name'][1]=="" && $_FILES['image']['name'][2]=="" && $_FILES['image']['name'][3]=="" && $_FILES['image']['name'][4]=="" && $_FILES['image']['name'][5]=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please upload Image.";
	}
	
	
	if($error!=1) {
	
			$target_paths="../Uploaded/entertainment_gallery/".$entertainment_id."/";
			$target_path_database="Uploaded/entertainment_gallery/".$entertainment_id."/";
			$target_video_paths="../Uploaded/entertainment_gallery/".$entertainment_id."/video/";
			$target_video_path_database="Uploaded/entertainment_gallery/".$entertainment_id."/video/";
			if(!is_dir($target_paths)) {
				if(mkdir($target_paths,777))
				{
					chmod($target_paths,0777); 
				}
			}if(!is_dir($target_video_paths)) {
				if(mkdir($target_video_paths,777))
				{
					chmod($target_video_paths,0777); 
				}
			}//end if(!is_dir($target_path)) {

            $imageArray = array();

            $vidCounter = 0;
			for($i=0;$i<6;$i++) {


				if($_FILES['image']['name'][$i]!="") {

					$imagename=explode(".",$_FILES['image']['name'][$i]);
					$countimagename=count($imagename);
					$imagefilename="";
					for($j=0;$j<$countimagename;$j++) {
					
						if($j!=$countimagename-1) {
							$imagefilename.=$imagename[$j];
						}
					}
					
					$image = new Resize_Image;
					
					$image->new_width = 1024;
					$image->new_height = 1024;
					$image->image_to_resize=$_FILES['image']['tmp_name'][$i]; // Full Path to the file
					
					$image->ratio = true; // Keep Aspect Ratio?
					$image->new_image_name=$imagefilename;
					$image->save_folder =$target_paths;
					$process = $image->resize();
					$media_path = "".$process['new_file_path']; 
					
					$media_path_array_large=explode("/",$media_path);
					
					
					
					$image = new Resize_Image;
					$image->new_width = 74;
					$image->new_height = 74;
					$image->image_to_resize=$_FILES['image']['tmp_name'][$i]; // Full Path to the file
					$image->ratio = true; // Keep Aspect Ratio?
					$image->new_image_name="tn_".$imagefilename;
					$image->save_folder =$target_paths;
					$process = $image->resize();
					$media_path = "".$process['new_file_path']; 
					
					$media_path_array_thumb=explode("/",$media_path);
					
					$image = new Resize_Image;
					$image->new_width = 60;
					$image->new_height = 60;
					$image->image_to_resize=$_FILES['image']['tmp_name'][$i]; // Full Path to the file
					$image->ratio = true; // Keep Aspect Ratio?
					$image->new_image_name="mobile_".$imagefilename;
					$image->save_folder =$target_paths;
					$process = $image->resize();
					$media_path = "".$process['new_file_path']; 
					
					
					$media_path_array_mobile=explode("/",$media_path);


						$path_image[]="http://web2.singaporegp.sg/Uploaded/entertainment_gallery/".$entertainment_id."/".$media_path_array_large[4];
						$path_image_thumb[]="http://web2.singaporegp.sg/Uploaded/entertainment_gallery/".$entertainment_id."/".$media_path_array_thumb[4];
						$path_image_mobile[]="http://web2.singaporegp.sg/Uploaded/entertainment_gallery/".$entertainment_id."/".$media_path_array_mobile[4];

                        // to clone to Live
                        array_push($imageArray,"http://128.199.250.199/Uploaded/entertainment_gallery/".$entertainment_id."/".$media_path_array_large[4]);
                        array_push($imageArray,"http://128.199.250.199/Uploaded/entertainment_gallery/".$entertainment_id."/".$media_path_array_thumb[4]);
                        array_push($imageArray,"http://128.199.250.199/Uploaded/entertainment_gallery/".$entertainment_id."/".$media_path_array_mobile[4]);
					
				}else{
					$path_image[]="";
					$path_image_thumb[]="";
					$path_image_mobile[]="";
				}


				if($_POST['videocheck'][$i] == 'true')
				{
					
					if($_FILES['video_mobile']['name'][$vidCounter]!="") {
						 $target = $target_video_paths . basename( $_FILES['video_mobile']['name'][$vidCounter]) ;
						 $video_mobile_path_array = basename( $_FILES['video_mobile']['name'][$vidCounter]) ;
						 if(move_uploaded_file($_FILES['video_mobile']['tmp_name'][$vidCounter], $target)) 
						 {
							echo "The file ". basename( $_FILES['video_mobile']['name'][$vidCounter]). " has been uploaded";
						 } 
						 else 
						 {
							echo "Sorry, there was a problem uploading your file.";
							$error = 1;
						 }
                            $path_video_mobile[] = "http://web2.singaporegp.sg/Uploaded/entertainment_gallery/".$entertainment_id."/video/".$video_mobile_path_array;
                            array_push($imageArray,"http://128.199.250.199/Uploaded/entertainment_gallery/".$entertainment_id."/video/".$video_mobile_path_array);
                    }
					else if($_POST['video_mobile_text'][$vidCounter]!="") {
						$path_video_mobile[]=$_POST['video_mobile_text'][$vidCounter];
					}
					else
					{
						$path_video_mobile[] = "";
					}
					if($_FILES['video']['name'][$vidCounter]!="") {
						 $target = $target_video_paths . basename( $_FILES['video']['name'][$vidCounter]) ; 
						 $video_path_array = basename( $_FILES['video']['name'][$vidCounter]) ;
						 if(move_uploaded_file($_FILES['video']['tmp_name'][$vidCounter], $target)) 
						 {
							echo "The file ". basename( $_FILES['video']['name'][$vidCounter]). " has been uploaded";
						 } 
						 else 
						 {
							echo "Sorry, there was a problem uploading your file.";
							$error = 1;
						 }
                        $path_video[] = "http://web2.singaporegp.sg/Uploaded/entertainment_gallery/".$entertainment_id."/video/".$video_path_array;
                        array_push($imageArray,"http://128.199.250.199/Uploaded/entertainment_gallery/".$entertainment_id."/video/".$video_path_array);

                    }
					else if($_POST['video_text'][$vidCounter]!="") {
						$path_video[]=$_POST['video_text'][$vidCounter];
					}
					else
					{
						$path_video[] = "";
					}
					if($_FILES['video_ogg']['name'][$vidCounter]!="") {
						 $target = $target_video_paths . basename( $_FILES['video_ogg']['name'][$vidCounter]) ;
						 $video_ogg_path_array = basename( $_FILES['video_ogg']['name'][$vidCounter]) ;
						 if(move_uploaded_file($_FILES['video_ogg']['tmp_name'][$vidCounter], $target)) 
						 {
							echo "The file ". basename( $_FILES['video_ogg']['name'][$vidCounter]). " has been uploaded";
						 } 
						 else 
						 {
							echo "Sorry, there was a problem uploading your file.";
							$error = 1;
						 }

                        $path_video_ogg[] = "http://web2.singaporegp.sg/Uploaded/entertainment_gallery/".$entertainment_id."/video/".$video_ogg_path_array;
                        array_push($imageArray,"http://128.199.250.199/Uploaded/entertainment_gallery/".$entertainment_id."/video/".$video_ogg_path_array);

                    }
					else if($_POST['video_ogg_text'][$vidCounter]!="") {
						$path_video_ogg[]=$_POST['video_ogg_text'][$vidCounter];
					}
					else
					{
						$path_video_ogg[] = "";
					}
					$vidCounter++;
				}
				else
				{
					$path_video[] = "";
					$path_video_mobile[] = "";
					$path_video_ogg[] = "";
				}
				
			}//for($i=0;$i<4;$i++) {

            // copy over to LIVE
            CrossFileUploadManager::uploadFiles($imageArray);

            if($error == 0)
			{
				echo "<script>alert('Upload Successful');</script>";
			}
			else
			{
				echo "<script>alert('Upload Fail');</script>";
			}
	
			$count=count($path_image_thumb);
			
			for($i=0;$i<6;$i++) {
				if($path_image_thumb[$i] != "")
				{
					$q="INSERT INTO entertainment_highlights_gallery (entertainment_id,image_thumb,image,image_mobile,video,video_mobile,video_ogg,updater) VALUES ('".mysql_real_escape_string($entertainment_id)."','".mysql_real_escape_string($path_image_thumb[$i])."','".mysql_real_escape_string($path_image[$i])."','".mysql_real_escape_string($path_image_mobile[$i])."','".mysql_real_escape_string($path_video[$i])."','".mysql_real_escape_string($path_video_mobile[$i])."','".mysql_real_escape_string($path_video_ogg[$i])."','".mysql_real_escape_string($_SESSION[tdsi_username])."')";
					mysql_query($q,$link) or die(mysql_error());
					if (isset($link1) && is_resource($link1)) {
						mysql_query($q, $link1) or die(mysql_error());
					}

					if (isset($link2) && is_resource($link2)) {
						mysql_query($q, $link2) or die(mysql_error());
					}

				}
			}//end for($i=0;$i<4;$i++) {
			
			header('Location:performance-detail-listing.php?entertainment_id='.$entertainment_id.'');
			exit();
	
	}//if($error!=1) {

	
	
	
}//if(isset($_POST['submit'])) {


$q_enter="SELECT artist,short_description,image_thumb FROM entertainment_highlights WHERE id='".mysql_real_escape_string($entertainment_id)."'";
$result_enter=mysql_query($q_enter,$link) or die(mysql_error());
$row_enter=mysql_fetch_array($result_enter);

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="uploadify/uploadify.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery.uploadify.js"></script>
<script type="text/javascript">

$(document).ready(function() {
	
	$("#fileUpload2").fileUpload({
		'uploader': 'uploadify/uploader.swf',
		'cancelImg': 'uploadify/cancel.png',
		'script': 'uploadify/upload.php',
		'folder': 'files',
		'multi': true,
		'fileDesc': 'Image Files',
		'fileExt': '*.jpg;*.jpeg;*.gif',
		'buttonText': 'Select Files',
		'displayData': 'percentage',
		'simUploadLimit': 1,
		'maxQueueSize': 9,
		
		'scriptData': {'id':'<?php echo $id; ?>'},
		'onAllComplete': function(event,data){
			location.href='view-photo-album.php?id=<?php echo $id; ?>';
	     }
	});

	
});

function ToggleInputType_1()
{
	
	if(document.getElementById('ToggleInputType_1_image').style.display == 'block')
	{
		document.getElementById('ToggleInputType_1_image').style.display = 'none';
		document.getElementById('ToggleInputType_1_video').style.display = 'block';
	}
	else
	{
		document.getElementById('ToggleInputType_1_image').style.display = 'block';
		document.getElementById('ToggleInputType_1_video').style.display = 'none';
	}
}

function ToggleInputType_2()
{
	
	if(document.getElementById('ToggleInputType_2_image').style.display == 'block')
	{
		document.getElementById('ToggleInputType_2_image').style.display = 'none';
		document.getElementById('ToggleInputType_2_video').style.display = 'block';
	}
	else
	{
		document.getElementById('ToggleInputType_2_image').style.display = 'block';
		document.getElementById('ToggleInputType_2_video').style.display = 'none';
	}
}
</script>
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Add Entertainment Highlights Gallery </div>
		<div id="back_to_list"><a href="performance-detail-listing.php?entertainment_id=<?php echo $entertainment_id; ?>">Back to List</a></div>
		<div id="content">
			<form action="<?php $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
			<div>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<tr>
					<td width="752">
					<br>
					</td>
					
				</tr>
				<tr>
					<td width="752" style="border-top:1px dotted #BAB9B9;border-bottom:1px dotted #BAB9B9;">
					<div style="font-weight:bold; font-size:14px; padding:5px 5px 5px 0;"><?php echo $row_enter['artist']; ?></div>
					<div><img src="<?php echo $row_enter['image_thumb']; ?>" align="left" width="130" height="94" style="margin-right:10px; margin-bottom:10px; border:1px solid #CCC"><?php echo $row_enter['short_description']; ?></div>
					<div style="clear:both"></div>
					</td>
					
				</tr>
				
				
				</table>
			  </div>
				<div class="clearboth"></div>
			<div>
			
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				
				<tr>
					<td width="126" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="126" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Image 1 : <br>
					  (Any Dimension)</strong></td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;">
                    <input accept="image/x-png, image/png, image/gif, image/jpeg" type="file" name="image[]" value="" />
                    <input type="hidden" name="videocheck[]" value="false" />
                    </td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="126" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Image 2 : <br>
				    (Any Dimension)					</strong></td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;">
                    <input accept="image/x-png, image/png, image/gif, image/jpeg" type="file" name="image[]" value="" />
                    <input type="hidden" name="videocheck[]" value="false" />
                    </td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="126" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>
                    <input type="radio" name="inputType_1" value="image" checked onchange="ToggleInputType_1()" />Image/ 3 : <br>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (Any Dimension) <br>
				    <input type="radio" name="inputType_1" value="video" onchange="ToggleInputType_1()" />	Video :			</strong></td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;">
                    <div id="ToggleInputType_1_image" style="display:block">
                    <input accept="image/x-png, image/png, image/gif, image/jpeg" type="file" name="image[]" value="" />
                    <input type="hidden" name="videocheck[]" value="false" />
                    </div>
					<div id="ToggleInputType_1_video" style="display:none">Video: <input accept="video/x-flv" type="file" name="video[]" value="" />
                    </br> Video(Mobile): <input accept="video/*" type="file" name="video_mobile[]" value="" />
					 </br> Video(OGG): <input accept="video/*" type="file" name="video_ogg[]" value="" />
					 <br>
					 <br>
					 Video: <input type="text" name="video_text[]" value="" style="width:300px"/>
                    </br> Video(Mobile): <input type="text" name="video_mobile_text[]" value="" style="width:300px"/>
					 </br> Video(OGG): <input type="text" name="video_ogg_text[]" value="" style="width:300px"/>
					 <br>
					 <br>
                    </br> Thumbnail: <input accept="image/x-png, image/png, image/gif, image/jpeg" type="file" name="image[]" value="" />
                    <input type="hidden" name="videocheck[]" value="true" /></div></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="126" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>
                    <input type="radio" name="inputType_2" value="image" checked onchange="ToggleInputType_2()"/>Image/ 3 : <br>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (Any Dimension) <br>
				    <input type="radio" name="inputType_2" value="video" onchange="ToggleInputType_2()"/>	Video :			</strong></td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;">
                    <div id="ToggleInputType_2_image" style="display:block">
                    <input accept="image/x-png, image/png, image/gif, image/jpeg" type="file" name="image[]" value="" />
                    <input type="hidden" name="videocheck[]" value="false" />
                    </div>
					<div id="ToggleInputType_2_video" style="display:none">Video: <input accept="video/x-flv" type="file" name="video[]" value="" />
                    </br> Video(Mobile): <input accept="video/*" type="file" name="video_mobile[]" value="" />
					</br> Video(OGG): <input accept="video/*" type="file" name="video_ogg[]" value="" />
					 <br>
					 <br>
					 Video: <input type="text" name="video_text[]" value="" style="width:300px"/>
                    </br> Video(Mobile): <input type="text" name="video_mobile_text[]" value="" style="width:300px"/>
					 </br> Video(OGG): <input type="text" name="video_ogg_text[]" value="" style="width:300px"/>
					 <br>
					 <br>
                    </br> Thumbnail: <input accept="image/x-png, image/png, image/gif, image/jpeg" type="file" name="image[]" value="" />
                    <input type="hidden" name="videocheck[]" value="true" /></div>
                    </td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="126" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"></td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;"><input type="submit" name="submit" value="Upload Entertainment Highlights Gallery"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				
				
					<td></td>
					<td></td>
				<td>&nbsp;</td>
				</tr>
				</table>
			  </div>
			  <input type="hidden" name="entertainment_id" value="<?php echo $entertainment_id; ?>">
		  </form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

