<?php 
require_once '../function/helper.php';
require_once '../function/cms-helper.php';

check_session_exists();

# form init
$spaces = str_repeat('&nbsp;', 5);

$form = array(
	'region_id' => array('mandatory'=>true, 'label'=>'Region *', 'type'=>'select', 'prepend'=>true),
	'country_id' => array('mandatory'=>false, 'label'=>'Country *', 'type'=>'select_optgroup', 'prepend'=>true),
	'agency_name' => array('mandatory'=>true, 'label'=>'Agency Name *', 'maxlength'=>255),
	'agency_name_chinese' => array('mandatory'=>true, 'label'=>'Agency Name (Chinese) *', 'maxlength'=>255),
	'additional_info' => array('label'=>'Additional Info', 'maxlength'=>255),
	'contact_person' => array('label'=>'Contact Person', 'maxlength'=>255),
	'contact_person_chinese' => array('label'=>'Contact Person (Chinese)', 'maxlength'=>255),
	'contact_no1' => array('label'=>'Contact No 1', 'maxlength'=>255),
	'contact_no2' => array('label'=>'Contact No 2', 'maxlength'=>255),

	'email1' => array('label'=>'Email 1', 'maxlength'=>255),
	'email_word1' => array('label'=>'Email Word 1', 'maxlength'=>255),

	'email2' => array('label'=>'Email 2', 'maxlength'=>255),
	'email_word2' => array('label'=>'Email Word 2', 'maxlength'=>255),

	'website1' => array('label'=>'Website 1 (URL)', 'maxlength'=>255),
	'website_word1' => array('label'=>'Website 1 (label)', 'maxlength'=>255),

	'website2' => array('label'=>'Website 2 (URL)', 'maxlength'=>255),
	'website_word2' => array('label'=>'Website 2 (label)', 'maxlength'=>255),

	'latitude' => array('mandatory'=>true, 'label'=>'Latitude *', 'maxlength'=>20, 'style'=>'width:120px;'),
	'longitude' => array('mandatory'=>true, 'label'=>'Longitude *', 'maxlength'=>20, 'style'=>'width:120px;'),
);

$connections = db_write_connect($all = true);
$connection = current($connections);

$status = null;
$table = 'ticketing_agent_international';
$errors = array();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if (!isset($_POST['country_id'])) {
		$_POST['country_id'] = 0;
	}

	foreach ($form as $key => $arr) {
		if (!empty($arr['mandatory']) && !isset($_POST[$key])) {
			$errors[] = sprintf('%s are mandatory', str_replace(' *', '', $arr['label']));
			$status = false;
		}
	}

	if (empty($errors)) { 
		$opt['table'] = $table;
		$opt['value'] = $_POST;
		$status = db_insert($opt);
		if ($status) {
			$id = db_last_id();
			header(sprintf('location: edit-ticketing-agent-international.php?id=%d', $id));
		} else {
			$row = $_POST;
		}
	} else {
		$row = $_POST;
	}
}

# page setting
$h1 = 'Authorized Ticketing Agents (International)';
$sort_opt = array('page', 'orderby', 'search_value');
$sort_url = sprintf('ticketing-agent-international-listing.php?%s', set_query_string($sort_opt));

# list of region
$sql = 'SELECT id,region FROM ticketing_agent_inter_region';
$res = db_select_all($sql, $connection);
$region = array();
foreach ($res as $arr) {
	$region[$arr['id']] = $arr['region'];
}
$form['region_id']['selection'] = $region;

# list of countries
$sql = <<<sql
SELECT c.id,c.country,r.region
FROM ticketing_agent_inter_country c
INNER JOIN ticketing_agent_inter_region r ON c.region_id = r.id
ORDER BY c.region_id
sql;

$res = db_select_all($sql, $connection);
$countries = array();
foreach ($res as $arr) {
	$re = $arr['region'];
	$id = $arr['id'];
	if (!isset($countries[$re])) {
		$countries[$re] = array();
	}
	$countries[$re][$id] = $arr['country'];
}
$form['country_id']['selection'] = $countries;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - <?php echo $h1;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/cms.css" rel="stylesheet" type="text/css" />
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
<?php
if (is_bool($status)) {
	if ($status === true) {
		echo '<div class="success">Insert Successfully!</div>';
	} else {
		if (empty($errors)) {
			$msg = 'Unable to insert, please try again later.';
		} else {
			$msg = sprintf('<ul><li>%s</ul>', implode('<li>', $errors));
		}

		printf('<div class="fail">%s</div>', $msg);
	}
}
?>
		<h1>Add New <?php echo $h1;?></h1>
		<div id="back_to_list">
			<a href="<?php echo $sort_url;?>">Back to List</a>
		</div>

<!-- form -->
<form method="post">
<table border="0" align="left" cellpadding="6" cellspacing="0" class="form">
<?php
if (empty($row)) {
	$row = array_fill_keys(array_keys($form), null);
}

generate_form($form, $row);
?>
</table>

<div class="button-div">
	<button class="submit-button" type="submit">Insert</button>
	<button class="reset-button" type="reset">Reset</button>
</div>
</form>

<h4>* Latitude and Longitude can find at <a href="http://itouchmap.com/latlong.html" target="_blank">here</a></h4>
<h4>* Mandatory Field</h4>
<!-- end form -->

	</div>
	<div class="clearboth"></div>
</div>
</body>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
$(document).ready(function() {
	$("select[name='region']").change(function() {
		var region = $(this).val();
		var label = $("select[name='region'] option:selected").text();

		if (region == '--') {
			$("select[name='country'] option").removeAttr("disabled");
		} else {
			$("select[name='country'] option").each(function() {
				if ($(this).parent().prop("label") == label) {
					$(this).removeAttr("disabled");
				} else {
					$(this).prop("disabled", "disabled");
				}
			});

		}
	});
});
</script>

</html>

