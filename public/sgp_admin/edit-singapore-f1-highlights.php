<?php 
require_once '../function/helper.php';
require_once '../function/cms-helper.php';
require_once 'include/configure.php';

check_session_exists();
$status_opt = array(1=>'Active', 0=>'Inactive');

$id=$_REQUEST['id'];

if(isset($_POST['submit'])) {
	$error=0;

	$image_link=$_POST['image_link'];
	$title=$_POST['title'];
	$type=$_POST['type'];
	$status=$_POST['status'];
	$display_website=$_POST['display_website'];
	$webview_url=$_POST['webview_url'];

	if($title=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Title.";
	} 
	if($image_link=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Singapore F1 Highlights Image Link.";
	}

	if($type=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Type.";
	}

	if($status=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Status.";
	}
	if($display_website=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please select Show On Website.";
	}
	
	
	
	
	if($error!=1) {
	
		if($_FILES['image_path']['name']!="") {
			
			$target_paths="../Uploaded/singapore_f1_highlights/";
			$target_path_database="Uploaded/singapore_f1_highlights/";
			if(!is_dir($target_paths)) {
				if(mkdir($target_paths,777))
				{
					chmod($target_paths,0777); 
				}
			}//end if(!is_dir($target_path)) {
			
			
			
			
			
			$target_path=$target_paths.basename( $_FILES['image_path']['name']); 
			move_uploaded_file($_FILES['image_path']['tmp_name'], $target_path);
			
			if ($_SERVER["SERVER_ADDR"] == "173.230.156.228") {
				$image_path="http://173.230.156.228/sgpstaging/".$target_path_database.basename( $_FILES['image_path']['name']);
			} else {
				$image_path="http://web2.singaporegp.sg/".$target_path_database.basename( $_FILES['image_path']['name']);
			}

			$q_image_path=",image_path='".mysql_real_escape_string($image_path)."'";
			
	
		}
		
		
		
		if($_FILES['banner_ipad_landscape']['name']!="") {
			
			$target_paths="../Uploaded/singapore_f1_highlights/";
			$target_path_database="Uploaded/singapore_f1_highlights/";
			if(!is_dir($target_paths)) {
				if(mkdir($target_paths,777))
				{
					chmod($target_paths,0777); 
				}
			}//end if(!is_dir($target_path)) {
			
			
			
			
			
			$target_path=$target_paths.basename( $_FILES['banner_ipad_landscape']['name']); 
			move_uploaded_file($_FILES['banner_ipad_landscape']['tmp_name'], $target_path);
			
			if ($_SERVER["SERVER_ADDR"] == "173.230.156.228") {
				$banner_ipad_landscape="http://173.230.156.228/sgpstaging/".$target_path_database.basename( $_FILES['banner_ipad_landscape']['name']);
			} else {
				$banner_ipad_landscape="http://web2.singaporegp.sg/".$target_path_database.basename( $_FILES['banner_ipad_landscape']['name']);
			}
			$q_banner_ipad_landscape=",banner_ipad_landscape='".mysql_real_escape_string($banner_ipad_landscape)."'";
		}
		
		
		
		
		if($_FILES['banner_ipad_portrait']['name']!="") {
			
			$target_paths="../Uploaded/singapore_f1_highlights/";
			$target_path_database="Uploaded/singapore_f1_highlights/";
			if(!is_dir($target_paths)) {
				if(mkdir($target_paths,777))
				{
					chmod($target_paths,0777); 
				}
			}//end if(!is_dir($target_path)) {
			
			
			
			
			
			$target_path=$target_paths.basename( $_FILES['banner_ipad_portrait']['name']); 
			move_uploaded_file($_FILES['banner_ipad_portrait']['tmp_name'], $target_path);
			
			if ($_SERVER["SERVER_ADDR"] == "173.230.156.228") {
				$banner_ipad_portrait="http://173.230.156.228/sgpstaging/".$target_path_database.basename( $_FILES['banner_ipad_portrait']['name']);
			} else {
				$banner_ipad_portrait="http://web2.singaporegp.sg/".$target_path_database.basename( $_FILES['banner_ipad_portrait']['name']);
			}

			$q_banner_ipad_portrait=",banner_ipad_portrait='".mysql_real_escape_string($banner_ipad_portrait)."'";
		}

		$q="UPDATE singaporef1_highlights SET image_link='".mysql_real_escape_string($image_link)."', 
		status=".mysql_real_escape_string($status).",
		type='".mysql_real_escape_string($type)."',
		webview_url='".mysql_real_escape_string($webview_url)."', title='".mysql_real_escape_string($title)."', display_website=".mysql_real_escape_string($display_website)." $q_image_path$q_banner_ipad_landscape$q_banner_ipad_portrait WHERE id='".mysql_real_escape_string($id)."'";
		# var_dump($q);exit;
		mysql_query($q,$link) or die(mysql_error());

		if (isset($link1) && is_resource($link1)) {
			mysql_query($q,$link1) or die(mysql_error());
		}

		if (isset($link2) && is_resource($link2)) {
			mysql_query($q,$link2) or die(mysql_error());
		}

		header('Location:singapore-f1-highlights-listing.php');
		exit();
	}

} else {
	
	$q="SELECT * FROM singaporef1_highlights WHERE id='$_REQUEST[id]'";
	$result=mysql_query($q,$link) or die(mysql_error());
	$row=mysql_fetch_assoc($result);
	
	$image_path=$row['image_path'];
	$image_link=$row['image_link'];
	$title=$row['title'];
	$banner_ipad_landscape=$row['banner_ipad_landscape'];
	$banner_ipad_portrait=$row['banner_ipad_portrait'];
	$type=$row['type'];
	$status=$row['status'];
	$display_website=$row['display_website'];
	$webview_url=$row['webview_url'];
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Edit Singapore F1 Highlights</div>
		<div id="back_to_list"><a href="singapore-f1-highlights-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
			<div>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Title   : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					  <input type="text" name="title"  value="<?php echo $title; ?>" style="width:500px" id="username">
					 
				  </td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Singapore F1 Highlights Image : *<br>
				    (607 x 280)					</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="image_path" value=""></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				  <td style="border-bottom:1px dotted #BAB9B9;"><img src="<?php echo $image_path; ?>" width="551"></td>
				  <td style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				  </tr>
				  <tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Singapore F1 Highlights Banner Ipad (Landscape) : *<br>
				    (1024 x 472)					</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="banner_ipad_landscape" value=""></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				  <td style="border-bottom:1px dotted #BAB9B9;"><img src="<?php echo $banner_ipad_landscape; ?>" width="551"></td>
				  <td style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				  </tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Singapore F1 Highlights Banner Ipad (Portrait) : *<br>
				    (768 x 354)					</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="banner_ipad_portrait" value=""></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				  <td style="border-bottom:1px dotted #BAB9B9;"><img src="<?php echo $banner_ipad_portrait; ?>" width="551"></td>
				  <td style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				  </tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Singapore F1 Highlights Image Link  : *</strong></td>
				  <td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="image_link"  value="<?php echo $image_link; ?>" style="width:500px" id="username">
				    <br>
				    eg. http://www.singaporegp.sg/autograph.php </td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>

				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Show On Website  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
						<?php echo set_select('display_website', $status_opt, $row['display_website']);?>
					</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>

				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Type  : *<br> 
				    (for mobile apps) </strong></td>
				  <td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="type"  value="<?php echo $type; ?>" style="width:500px" id="username">
				    <br>
				    eg. tickets or contest, news</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php
				/*
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Webapp URL  : <br>
				    (for mobile apps) </strong></td>
				  <td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="webapp_url"  value="<?php echo $webapp_url; ?>" style="width:500px" id="username">
				    <br>				    </td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				*/
				?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Show on Mobile : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
						<?php echo set_select('status', $status_opt, $row['status']);?>
					</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Mobile app url or Object ID: </strong></td>
				  <td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="webview_url"  value="<?php echo $webview_url; ?>" style="width:500px" id="webview_url">
				    <br>
				    eg. http://www.singaporegp.sg/contest/mobile-be-spotted.php<br>
				    eg. 2013-09-20
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Edit Singapore F1 Highlights"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				
			  </div>
				</form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

