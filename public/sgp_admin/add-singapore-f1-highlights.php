<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");

if(isset($_POST['submit'])) {
	$error=0;
	
	$image_path=$_POST['image_path'];
	$image_link=$_POST['image_link'];
	$title=$_POST['title'];
	
	if($title=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Title.";
	} 
	if($image_link=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Image Link.";
	} 
	
	
	if($error!=1) {

			$datetime=date("Y-m-d H:i:s");
	
			$q="INSERT INTO singaporef1_highlights (image_path,image_link,updater,title) VALUES ('".mysql_real_escape_string($image_path)."','".mysql_real_escape_string($image_link)."','".mysql_real_escape_string($_SESSION[cms_username])."','".mysql_real_escape_string($title)."')";
			mysql_query($q,$link) or die(mysql_error());

			if (isset($link1) && is_resource($link1)) {
				mysql_query($q,$link1) or die(mysql_error());
			}

			if (isset($link2) && is_resource($link2)) {
				mysql_query($q,$link2) or die(mysql_error());
			}

			header('Location:singapore-f1-highlights-listing.php');
			exit();
			
		
	}

}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Add New Singapore F1 highlights</div>
		<div id="back_to_list"><a href="singapore-f1-highlights-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post">
			<div>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Title   : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><p>
					  <input type="text" name="title"  value="<?php echo $title; ?>" style="width:500px" id="username">
					  <br>
					  * eg: http://www.singaporegp.sg/images/home_highlights/2012-win-drivers-autograph.jpg</p>
				  </td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Image Path   : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><p>
					  <input type="text" name="image_path"  value="<?php echo $image_path; ?>" style="width:500px" id="username">
					  <br>
					  * eg: http://www.singaporegp.sg/images/home_highlights/2012-win-drivers-autograph.jpg</p>
				  </td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Image Link  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="image_link"  value="<?php echo $image_link; ?>" style="width:500px "></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Add New Singapore F1 Highlights"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				
				</div>
				</form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

