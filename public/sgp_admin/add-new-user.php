<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");

$successword="";
$errorword="";
$username = (isset($_GET['username'])?$_GET['username']:"");
$password = (isset($_GET['password'])?$_GET['password']:"");
$name = (isset($_GET['name'])?$_GET['name']:"");
$role = (isset($_GET['role'])?$_GET['role']:"");

if(isset($_POST['submit'])) {
	$error=0;
	
	$username=$_POST['username'];
	$password=$_POST['password'];
	$name=$_POST['name'];
	$role=$_POST['role'];

	
	
	if($username=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Username.";
	}
	if($password=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Password.";
	} else {
		if(strlen($password)< 6) {
			$error=1;
			$errorword.="<br>";
			$errorword.="Password less than 6 characters.";
		}
	}
	if($name=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Name.";
	}
	if($role=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please select Role.";
	}
	
	
	if($error!=1) {

		$datetime=date("Y-m-d H:i:s");
		$password_encrypt=md5($password);
		$q="INSERT INTO admin_user (username,password,name,role,updater) VALUES ('$username','$password_encrypt','$name','$role','$_SESSION[cms_username]')";
		$result=mysqli_query($link, $q) or die(mysqli_error($link));

		if (isset($link1)) {
			mysqli_query($link1, $q) or die(mysqli_error($link1));
		}

		if (isset($link2)) {
			mysqli_query($link2, $q) or die(mysqli_error($link2));
		}


		header('Location:user-listing.php');
		exit();

		
	}

}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Add New User </div>
		<div id="back_to_list"><a href="user-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post">
			<div>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				<?php if($successword!="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;"></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><?php echo $successword; ?></td>
					<td width="3"  style="border-bottom:1px dotted #BAB9B9;"></td>
				</tr>
				<?php } ?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Username  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="username"  value="<?php echo $username; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Password : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="password" name="password"  value="<?php echo $password; ?>" style="width:500px "></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Name : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="name" style="width:500px "></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Role : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="role" id="role">
						<option value="">=Please Select=</option>
						<option value="2" <?php if($role=="2") { echo "selected"; } ?>>Administrator</option>
						<option value="1"  <?php if($role=="1") { echo "selected"; } ?>>User</option>
					</select>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Add New User"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				
				</div>
				</form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

