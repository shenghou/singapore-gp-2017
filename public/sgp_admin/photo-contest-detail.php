<?php 
session_start();

if($_COOKIE["cms_username"]=="") {
	header('Location:login.php');
	exit();
}
include("include/configure.php");

$photo_id=$_REQUEST['photo_id'];
$q="SELECT * FROM photo_contest WHERE id='".mysql_real_escape_string($photo_id)."'";
$result=mysql_query($q,$link) or die(mysql_error());
$row=mysql_fetch_array($result);


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/ajax.js"></script>
<body style="background:none">
<div style="width:600px">
	<table width="600" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="600"  valign="top">
		<div style="padding:10px 10px 10px 10px;text-align:center">
		<a href="<?php echo $row['photograph_path']; ?>" target="_blank"><img src="<?php echo $row['photograph_path']; ?>" border="none" width="590"></a>
		</div>
		 
		</td>
	</tr>
	<tr>
		<td>
		<div style="padding:10px 10px 10px 10px;text-align:left">
		<table border="0" cellpadding="8" cellspacing="0" width="600">
										<tr>
											<td width="109" valign="top" height="5px"></td>
											<td width="199" valign="top" height="5px"></td>
										</tr>
										<tr>
											<td width="109" valign="top" style="background-color:#D6D6D6"><strong>Full Name:</strong></td>
											<td width="199" valign="top" style="background-color:#D6D6D6"><?php echo stripslashes($row['full_name']); ?></td>
										</tr>
										<tr>
											<td valign="top"><strong>2012 Ticket transaction number:</strong></td>
											<td valign="top"><?php echo stripslashes($row['ticket_transaction_number']); ?></td>
										</tr>
										<tr>
											<td valign="top" style="background-color:#D6D6D6"><strong>Email Address:</strong></td>
											<td valign="top" style="background-color:#D6D6D6"><?php echo stripslashes($row['email_address']); ?></td>
										</tr>
										<tr>
											<td valign="top"><strong>Contact Number:</strong></td>
											<td valign="top"><?php echo stripslashes($row['contact_number']); ?></td>
										</tr>
										<tr>
											<td valign="top" style="background-color:#D6D6D6"><strong>Title of photograph:</strong></td>
											<td valign="top" style="background-color:#D6D6D6"><?php echo stripslashes($row['title_photograph']); ?></td>
										</tr>
										<tr>
											<td valign="top"><strong>Caption of photograph:</strong></td>
											<td valign="top"><?php echo stripslashes($row['caption_photograph']); ?></td>
										</tr>
										<tr>
											<td valign="top" style="background-color:#D6D6D6"><strong>Location of photograph:</strong></td>
											<td valign="top" style="background-color:#D6D6D6"><?php echo stripslashes($row['photograph_location']); ?></td>
										</tr>
										<tr>
											<td valign="top"><strong>Dimension:</strong></td>
											<td valign="top"><?php echo stripslashes($row['width']); ?> * <?php echo stripslashes($row['height']); ?></td>
										</tr>
										<tr>
											<td valign="top"  style="background-color:#D6D6D6"><strong>File Size:</strong></td>
											<td valign="top"  style="background-color:#D6D6D6"><?php echo $filesize=number_format($row['filesize']/1024/1024,2,'.',''); ?> MB</td>
										</tr>
										<tr>
											<td valign="top"><strong>From Where :</strong></td>
											<td valign="top"><?php echo stripslashes($row['from_where']); ?></td>
										</tr>
										<tr>
											<td valign="top"  style="background-color:#D6D6D6"><strong>Date Time :</strong></td>
											<td valign="top"  style="background-color:#D6D6D6"><?php echo stripslashes($row['datetime']); ?></td>
										</tr>
										</table>
										</div>
		</td>
	</tr>
	</table>
</div>
</body>
</html>

