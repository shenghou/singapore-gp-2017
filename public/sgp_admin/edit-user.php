<?php 
session_start();

if($_SESSION['cms_username']=="" || $_SESSION['cms_role']!="2") {
	header('Location:login.php');
}
include("include/configure.php");
$successword = '';
$errorword = '';

$id=(isset($_REQUEST['id'])?$_REQUEST['id']:"");

if(isset($_POST['submit'])) {
	$error=0;

	
	$username=$_POST['username'];
	$new_password=$_POST['new_password'];
	$confirm_new_password=$_POST['confirm_new_password'];
	$name=$_POST['name'];
	$role=$_POST['role'];
	
	if($username=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Username.";
	}
	if($new_password!="" || $confirm_new_password!="") {
		
		if($new_password=="") {
			$error=1;
			$errorword.="<br>";
			$errorword.="Please insert New Password.";
		}
		if($confirm_new_password=="") {
			$error=1;
			$errorword.="<br>";
			$errorword.="Please insert Confirm New Password.";
		}
		if($new_password!="" && $confirm_new_password!="") {
			if($confirm_new_password!=$new_password) {
				$error=1;
				$errorword.="<br>";
				$errorword.="New Password and Confirm New Password are different.";
			}
		}
	}
	
	
	if($error!=1) {

		if($new_password!="") {
			$password_encrypt=md5($new_password);
			$q_password=",password='$password_encrypt'";
		}

		$q="UPDATE admin_user SET username='$username',name='$name',role='$role'$q_password WHERE id='$id'";
		$result=mysqli_query($link, $q) or die(mysqli_error($link));

		if (isset($link1)) {
			mysqli_query($link1, $q) or die(mysqli_error($link1));
		}

		if (isset($link2)) {
			mysqli_query($link2, $q) or die(mysqli_error($link2));
		}

		header('Location:user-listing.php?id='.$id);
		exit();
	}

} else {
	
	$q="SELECT * FROM admin_user WHERE id='$_REQUEST[id]'";
	$result=mysqli_query($link, $q) or die(mysqli_error($link));
	$row=mysqli_fetch_array($result);
	
	$username=$row['username'];
	$name=$row['name'];
	$role=$row['role'];
	
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />

<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Edit User </div>
		<div id="back_to_list"><a href="user-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post">
			<div>
				
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				<?php if($successword!="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;"></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><?php echo $successword; ?></td>
					<td width="3"  style="border-bottom:1px dotted #BAB9B9;"></td>
				</tr>
				<?php } ?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Username  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="username"  value="<?php echo $username; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>New Password  :</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="password" name="new_password"  value="" style="width:500px" id="role_name"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Confirm New Password  :</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="password" name="confirm_new_password"  value="" style="width:500px" id="role_name"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Name : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="name" style="width:500px" value="<?php echo $name; ?>"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Role : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="role" id="role">
						<option value="">=Please Select=</option>
						<option value="2" <?php if($role=="2") { echo "selected"; } ?>>Administrator</option>
						<option value="1"  <?php if($role=="1") { echo "selected"; } ?>>User</option>
					</select>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Update User"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				</div>
				</form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

