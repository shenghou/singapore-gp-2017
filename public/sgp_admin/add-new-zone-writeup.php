<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");

$id=$_REQUEST['id'];

if(isset($_POST['submit'])) {
	$error=0;

	
	$zone_title=$_POST['zone_title'];
	$zone_title_chinese=$_POST['zone_title_chinese'];
	$zone_writeup=$_POST['zone_writeup'];
	$zone_short_writeup=$_POST['zone_short_writeup'];
	$zone_short_writeup_chinese=$_POST['zone_short_writeup_chinese'];
	$zone_writeup_chinese=$_POST['zone_writeup_chinese'];
	
	
	if($zone_title=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Zone Title.";
	}
	if($zone_title_chinese=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Zone Title Chinese.";
	}
	if($zone_short_writeup=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Zone Short Write Up.";
	}
	if($zone_short_writeup_chinese=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Zone Short Write Up Chinese.";
	}
	if($zone_writeup=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Zone Write Up.";
	}
	if($zone_writeup_chinese=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Zone Write Up Chinese.";
	}
	
	
	
	if($error!=1) {

			$q="INSERT INTO zone_writeup (zone_title,zone_title_chinese,zone_writeup,zone_short_writeup,zone_short_writeup_chinese,zone_writeup_chinese,updater) VALUES ('".mysql_real_escape_string($zone_title)."','".mysql_real_escape_string($zone_title_chinese)."','".mysql_real_escape_string($zone_writeup)."','".mysql_real_escape_string($zone_short_writeup)."','".mysql_real_escape_string($zone_short_writeup_chinese)."','".mysql_real_escape_string($zone_writeup_chinese)."','".mysql_real_escape_string($_SESSION['cms_username'])."')";
			$result=mysql_query($q,$link) or die(mysql_error());
			
			//$q="INSERT INTO zone_writeup (zone_title,zone_title_chinese,zone_writeup,zone_short_writeup,zone_short_writeup_chinese,zone_writeup_chinese,updater) VALUES ('".mysql_real_escape_string($zone_title)."','".mysql_real_escape_string($zone_title_chinese)."','".mysql_real_escape_string($zone_writeup)."','".mysql_real_escape_string($zone_short_writeup)."','".mysql_real_escape_string($zone_short_writeup_chinese)."','".mysql_real_escape_string($zone_writeup_chinese)."','".mysql_real_escape_string($_SESSION['cms_username']."')";
			//$result=mysql_query($q,$link1) or die(mysql_error());
			
			//$q="INSERT INTO zone_writeup (zone_title,zone_title_chinese,zone_writeup,zone_short_writeup,zone_short_writeup_chinese,zone_writeup_chinese,updater) VALUES ('".mysql_real_escape_string($zone_title)."','".mysql_real_escape_string($zone_title_chinese)."','".mysql_real_escape_string($zone_writeup)."','".mysql_real_escape_string($zone_short_writeup)."','".mysql_real_escape_string($zone_short_writeup_chinese)."','".mysql_real_escape_string($zone_writeup_chinese)."','".mysql_real_escape_string($_SESSION['cms_username']."')";
			//$result=mysql_query($q,$link2) or die(mysql_error());
			
			
			
			header('Location:zone-writeup-listing.php?page='.$_REQUEST[page].'&orderby='.$_REQUEST[orderby].'&search_value='.$_REQUEST[search_value].'');
			exit();
			
		
	}

} 

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript">

tinyMCE.init({

        // General options
		
		
        mode: "exact",

    	elements: "zone_writeup,zone_writeup_chinese",

        theme : "advanced",

        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",


        // Theme options

        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect",

       	theme_advanced_buttons2 : "fontsizeselectcut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview",

       theme_advanced_buttons3 : "forecolor,backcolor,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",

       theme_advanced_buttons4 : "print,|,ltr,rtl,|,fullscreen,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",

        theme_advanced_toolbar_location : "top",

        theme_advanced_toolbar_align : "left",
		
		

        theme_advanced_statusbar_location : "bottom",

        theme_advanced_resizing : true,

		theme_advanced_blockformats : "p,div,address,pre,h1,h2,h3,h4,h5,h6",

		forced_root_block : '',

        // Skin options

        skin : "o2k7",

        skin_variant : "silver",

		
        // Example content CSS (should be your site CSS)

        content_css : "../css/page.css,../css/txtcolor.css",



        // Drop lists for link/image/media/template dialogs

        template_external_list_url : "js/template_list.js",

        external_link_list_url : "js/link_list.js",

        external_image_list_url : "js/image_list.js",

        media_external_list_url : "js/media_list.js",

		convert_urls : false,
		
		forced_root_block : 'p',
		
		

        // Replace values for the template plugin

        template_replace_values : {

                username : "Some User",

                staffid : "991234"

        }
		

});

</script>
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Edit Zone Write Up </div>
		<div id="back_to_list"><a href="zone-writeup-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post">
			<div>
				
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Zone Title   : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="zone_title"  value="<?php echo $zone_title; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Zone Title Chinese  : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="zone_title_chinese"  value="<?php echo $zone_title_chinese; ?>" style="width:500px" id="role_name"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Zone Short Write Up : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="zone_short_writeup" style="width:500px;height:100px"><?php echo $zone_short_writeup; ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Zone Short Write Up Chinese : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="zone_short_writeup_chinese" style="width:500px;height:100px"><?php echo $zone_short_writeup_chinese; ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Zone Write Up  : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="zone_writeup" style="width:500px;height:350px;font-size:11px"><?php echo stripslashes($zone_writeup); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Zone Write Up Chinese  : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="zone_writeup_chinese" style="width:500px;height:350px;font-size:11px"><?php echo stripslashes($zone_writeup_chinese); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Add New Zone Write Up"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				</div>
				</form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

