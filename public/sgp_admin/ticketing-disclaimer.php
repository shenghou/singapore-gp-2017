<?php 
require_once '../function/helper.php';
require_once '../function/cms-helper.php';

session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
	exit();
}

# insert or update
$permalink = 'ticketing-disclaimer';
$table = 'page';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$opt['table'] = $table;
	$opt['value'] = array('body' => $_POST['body'], 'body_cn' => $_POST['body_cn'], 'name' => $_POST['name']);
	$opt['where'] = array('permalink' => $permalink);
	$status = db_update($opt);
} else {
	$status = -1;
}

# load record
$row = db_get(sprintf('SELECT * FROM %s WHERE permalink = "%s"', $table, $permalink));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Team Intro Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/cms.css" rel="stylesheet" type="text/css" />
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<?php
		if (is_bool($status)) {
			if ($status === true) {
				echo '<div class="success">Update Successfully!</div>';
			} else {
				echo '<div class="fail">Unable to update, please try again later.</div>';
			}
		}
		?>
		<h1>Team Intro</h1>
		<form method="post">

<table border="0" align="left" cellpadding="6" cellspacing="0" class="form">
<?php
$form = array(
	'name' => array('label'=>'Title', 'maxlength'=>100),
	'body' => array('label'=>'Body', 'type'=>'textarea', 'tinymce'=>true, 'tinymce_width'=>400),
	'body_cn' => array('label'=>'Body (Chinese)', 'type'=>'textarea', 'tinymce'=>true, 'tinymce_width'=>400),
);
generate_form($form, $row);
?>
</table>
<div class="button-div">
	<button class="submit-button" type="submit">Update</button>
	<button class="reset-button" type="reset">Reset</button>
</div>

		</form>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

