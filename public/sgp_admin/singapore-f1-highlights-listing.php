<?php 
require_once '../function/helper.php';
require_once '../function/cms-helper.php';
require_once 'include/configure.php';

check_session_exists();

if(isset($_REQUEST['search_value'])) {
	$search_value=$_REQUEST['search_value'];
	if($search_value!="") {
		$q_search="AND (image_path LIKE '%$search_value%' OR image_link LIKE '%$search_value%')";
	}
}

# update singaporef1_highlights set status = if(sort = 999, 0, 1);
if(isset($_POST['savesequence'])) {

	$sort_update   = $_POST['sort'];
	$sortid_update = $_POST['sortid'];
	$status_update = $_POST['status'];
	$display_website_update = $_POST['display_website'];
	$count_update  = count($sortid_update);

	for($i=0;$i<$count_update;$i++) {
		$sort = mysql_real_escape_string($sort_update[$i]);
		if (empty($sort))
		{	
			$sort = 999;
		}

		$display_website = mysql_real_escape_string($display_website_update[$i]);
		$status = mysql_real_escape_string($status_update[$i]);
		$esc_id = mysql_real_escape_string($sortid_update[$i]);

		$sql = <<<sql
UPDATE singaporef1_highlights
SET sort={$sort}, status={$status}, display_website={$display_website}
WHERE id={$esc_id}
sql;
		mysql_query($sql,$link) or die(mysql_error());

		if (isset($link1) && is_resource($link1)) {
			mysql_query($sql, $link1) or die(mysql_error());
		}

		if (isset($link2) && is_resource($link2)) {
			mysql_query($sql, $link2) or die(mysql_error());
		}

	}
}

if(empty($_REQUEST['page'])) {
	$page=1;
}
else {
	if($_REQUEST['page']=="") {
		$page=1;
	} else {
		$page=$_REQUEST['page'];
	}
}
			
$range=25;//the range of pages display
$limit=50;//the limit of rows display in a page

$sql = <<<sql
SELECT COUNT(*) AS total
FROM singaporef1_highlights
WHERE 1 $q_search
sql;
$resultcount=mysql_query($sql,$link)or die(mysql_error());
$count=mysql_num_rows($resultcount);
$totalpage=ceil($count/$limit);
					
$offset = ($page*$limit)-$limit;

if($_GET['orderby']=="") {
	$orderby = 'display_website desc, sort';
} else {
	$orderby=$_GET['orderby'];
}

# status
$status_opt = array(1=>'Active', 0=>'Inactive');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/ajax.js"></script>
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Singapore F1 Highlights Listing </div>
		
		<div class="searching_field">
<form method="get">
		<table width="248" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="46">Search</td>
			<td width="133"><input type="text" name="search_value" value="<?php echo stripslashes($search_value); ?>"></td>
			<td width="69"><input type="submit" name="search" value="search"></td>
		</tr>
		</table>
</form>

		</div>
		<div class="clearboth"></div>
<form method="post">
		<div class="functionlink"><a href="add-new-singapore-f1-highlights.php">Add New Singapore F1 Highlights</a></div>
		<div id="content_list">
			<table width="760" align="left" cellpadding="5" cellspacing="0" style="border-top:1px solid #B6BEBE;">
			<tr class="blue_column">
				<td width="35" height="26" class="functionlink bottomline leftline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=sort&search_value=$search_value"; ?>">No</a></strong></td>
				<td width="331" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=title&search_value=$search_value"; ?>">Singapore F1 Highlights Image</a></strong></td>
				<td class="functionlink bottomline"><strong>WWW</strong></td>
				<td class="functionlink bottomline"><strong>Mobile</strong></td>
				<td width="53" class="functionlink bottomline"><strong>Display Order</strong></td>
				<td width="21" class="bottomline" align="center"><strong>Edit</strong></td>
				<td width="39" class="bottomline" align="center"><strong>Delete</strong></td>
			</tr>
			<?php 
			   if($page!=1 && $page!="") {
						$pagecount=$page-1;
						$num=$pagecount * $limit;
			   	}
$sql = <<<sql
SELECT *
FROM singaporef1_highlights
WHERE 1 $q_search
ORDER BY $orderby
LIMIT $offset, $limit
sql;

$result=mysql_query($sql, $link) or die(mysql_error());
while($row=mysql_fetch_assoc($result)) {
	$num=$num+1;
			   		?>
					<tr>
						<td width="35" class="bottomline leftline"><?php echo $num; ?></td>
						<td width="331" class="bottomline">
							<img src="<?php echo $row['image_path']; ?>" width="330">
							<br><?php echo $row['image_link']; ?>
						</td>
						<td class="functionlink bottomline" align="center">
							<?php echo set_select('display_website[]', $status_opt, $row['display_website']);?>
						</td>
						<td class="functionlink bottomline" align="center">
							<?php echo set_select('status[]', $status_opt, $row['status']);?>
						</td>
						<td width="53" class="functionlink bottomline" align="center"><input type="text" name="sort[]" value="<?php echo $row['sort']; ?>" size="3"></td>
						<td width="21" class="functionlink bottomline" align="center">
							<a href="edit-singapore-f1-highlights.php?id=<?php echo $row['id']; ?>&page=<?php echo $page; ?>&orderby=<?php echo $orderby; ?>&search_value=<?php echo $search_value; ?>"><img src="images/edit.jpg" border="none"></a>
						
						</td>
						<td width="39" class="functionlink bottomline" align="center">
						
							<a href="javascript:;" onClick="DeleteSingaporeHighlights('<?php echo $row['id']; ?>')"><img src="images/delete.gif" border="none"></a>
						
						</td>
					</tr>
					<input type="hidden" name="sortid[]" value="<?php echo $row['id']; ?>">
					<?php
			   }
			?>
			</table>
			 <div class="clearboth"></div>
			  <br>
			 <input type="submit" name="savesequence" value="Update Display Order & Status">
</form>
				<table border="0" cellspacing="0" cellpadding="0" class="page_table" align="left">
				<tr>
				<td align="left" class="style5"><?php
				$lrange = max(1,$page-(($range-1)/2));
				$rrange = min($totalpage,$page+(($range-1)/2));
				if (($rrange - $lrange) < ($range - 1))
					{
						if ($lrange == 1)
							{
								$rrange = min($lrange + ($range-1), $totalpage);
							}
						else
							{
								$lrange = max($rrange - ($range-1), 0);
							}
					}
				if($totalpage>1)
				{
					if ($page > 1)//previous page
						{
							$prev=$page-1;
							echo " <a href='".$PHP_SELF."?page=1&orderby=$orderby&search_value=$search_value'><< First</a> | "; 
							echo " <a href='".$PHP_SELF."?page=$prev&orderby=$orderby&search_value=$search_value'>< Prev</a> | ";
						}
					else
						{
							echo " <strong><< First</strong> | <strong>< Prev</strong> |";
						}
					for($i = 1; $i <= $totalpage; $i++)//the range of page
						{
							if ($i == $page)
								{
									echo " <span class='actual_page'><strong>$i</strong></span>";
								}
							else
								{
									if ($lrange <= $i and $i <= $rrange)
										{
											echo " <a href='".$PHP_SELF."?page=$i&orderby=$orderby&search_value=$search_value' class='bluecolor word fontsubtitle'>$i</a> ";
										}
								}
						}
					if ($page < $totalpage)//next page
						{
							$next=$page+1;
							echo "| <a href='".$PHP_SELF."?page=$next&orderby=$orderby&search_value=$search_value'>Next ></a> |";
							echo " <a href='".$PHP_SELF."?page=$totalpage&orderby=$orderby&search_value=$search_value'>Last >></a>";
							
						}
					else
						{
							echo " | <strong>Next ></strong> | <strong>Last >></strong>";
						}
				}//end of if $totalpage > 1
			?></td>
			  </tr>
			</table>
				
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

