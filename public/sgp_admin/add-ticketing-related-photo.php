<?php 
require_once '../function/cms-helper.php';
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");

$ticketing_info_id=$_REQUEST['ticketing_info_id'];

if(isset($_POST['submit'])) {
	$error=0;

/*
2013-04-10
no need thumbnail
	if($_FILES['image_thumb']['name'][0]=="" && $_FILES['image_thumb']['name'][1]=="" && $_FILES['image_thumb']['name'][2]=="" && $_FILES['image_thumb']['name'][3]=="" && $_FILES['image_thumb']['name'][4]=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please upload Image Thumb.";
	}
*/

	if($_FILES['image']['name'][0]=="" && $_FILES['image']['name'][1]=="" && $_FILES['image']['name'][2]=="" && $_FILES['image']['name'][3]=="" && $_FILES['image']['name'][4]=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please upload Image.";
	}
	
	
	if($error!=1) {
	
			$target_paths="../Uploaded/ticketing_related_gallery/".$ticketing_info_id."/";
			$target_path_database="Uploaded/ticketing_related_gallery/".$ticketing_info_id."/";
			if(!is_dir($target_paths)) {
				if(mkdir($target_paths,777))
				{
					chmod($target_paths,0777); 
				}
			}//end if(!is_dir($target_path)) {
			
			
			for($i=0;$i<5;$i++) {
/*
2013-04-10
this no longer required,
thumbnail will regenerated

				if($_FILES['image_thumb']['name'][$i]!="") {
				
					$target_path_image_thumb=$target_paths.basename( $_FILES['image_thumb']['name'][$i]); 
					move_uploaded_file($_FILES['image_thumb']['tmp_name'][$i], $target_path_image_thumb);
					
					$path_image_thumb[]="http://173.230.156.228/sgpstaging/".$target_path_database.basename($_FILES['image_thumb']['name'][$i]);
					
				}//if($_FILES['image_thumb']['name'][$i]!="") {
*/	
				if($_FILES['image']['name'][$i]!="") {
					
					$target_path_image=$target_paths.basename( $_FILES['image']['name'][$i]); 
					move_uploaded_file($_FILES['image']['tmp_name'][$i], $target_path_image);

					# generate two thumbnails
					list($width, $height, $type) = getimagesize($target_path_image);
					$ratio = $width / $height;

					$the_width = 70;
					$the_height = round( $the_width / $ratio);
					$tb = str_replace('.jpg', '-sw70.jpg', $target_path_image);
					generate_thumbnail($target_path_image, $tb, $the_width, $the_height);

					$the_width = 200;
					$the_height = round( 200 / $ratio);
					$tb = str_replace('.jpg', '-sw200.jpg', $target_path_image);
					generate_thumbnail($target_path_image, $tb, $the_width, $the_height);
					
					$the_height = 390;
					$the_width = round( $the_height * $ratio);
					$tb = str_replace('.jpg', '-sh'.$the_height.'.jpg', $target_path_image);
					generate_thumbnail($target_path_image, $tb, $the_width, $the_height);
					
					if ($_SERVER["SERVER_ADDR"] == "173.230.156.228") {
						$path_image[]="http://173.230.156.228/sgpstaging/".$target_path_database.basename($_FILES['image']['name'][$i]);
					} else {
						$path_image[]="http://web2.singaporegp.sg/".$target_path_database.basename($_FILES['image']['name'][$i]);
					}
					
				}//if($_FILES['image']['name'][$i]!="") {
			}//for($i=0;$i<4;$i++) {
			
			$q_check="SELECT sort FROM ticketing_related_photo WHERE ticketing_info_id='".mysql_real_escape_string($ticketing_info_id)."' ORDER BY sort DESC";
			$result_check=mysql_query($q_check,$link) or die(mysql_error());
			$row_check=mysql_fetch_array($result_check);
			
			$sort=$row_check['sort']+1;
	
			$count=count($path_image);
			
			for($i=0;$i<$count;$i++) {
				
				$q="INSERT INTO ticketing_related_photo (ticketing_info_id,related_photo_thumb,related_photo,updater,sort) VALUES ('".mysql_real_escape_string($ticketing_info_id)."','".mysql_real_escape_string($path_image_thumb[$i])."','".mysql_real_escape_string($path_image[$i])."','".mysql_real_escape_string($_SESSION[tdsi_username])."','".mysql_real_escape_string($sort)."')";
				$result=mysql_query($q,$link) or die(mysql_error());

if (isset($link1) && is_resource($link1)) {
	mysql_query($q, $link1) or die(mysql_error());
}

if (isset($link2) && is_resource($link2)) {
	mysql_query($q, $link2) or die(mysql_error());
}

			}//end for($i=0;$i<4;$i++) {
			
			header('Location:ticketing-price-listing.php?ticketing_info_id='.$ticketing_info_id.'');
			exit();
	
	}//if($error!=1) {

	
	
	
}//if(isset($_POST['submit'])) {


$q_enter="SELECT ticketing_category_id,ticket_subcategory,access_to_zones,accessible_entertainment_stages,ticket_subcategory_chinese FROM ticketing_info WHERE id='".mysql_real_escape_string($ticketing_info_id)."'";
$result_enter=mysql_query($q_enter,$link) or die(mysql_error());
$row_enter=mysql_fetch_array($result_enter);

$q_category="SELECT ticketing_type FROM ticketing_category WHERE id='".mysql_real_escape_string($row_enter['ticketing_category_id'])."'";
$result_category=mysql_query($q_category,$link) or die(mysql_error());
$row_category=mysql_fetch_array($result_category);

$ticketing_type=$row_category['ticketing_type'];

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="uploadify/uploadify.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery.uploadify.js"></script>
<script type="text/javascript">

$(document).ready(function() {
	
	$("#fileUpload2").fileUpload({
		'uploader': 'uploadify/uploader.swf',
		'cancelImg': 'uploadify/cancel.png',
		'script': 'uploadify/upload.php',
		'folder': 'files',
		'multi': true,
		'fileDesc': 'Image Files',
		'fileExt': '*.jpg;*.jpeg;*.gif',
		'buttonText': 'Select Files',
		'displayData': 'percentage',
		'simUploadLimit': 1,
		'maxQueueSize': 9,
		
		'scriptData': {'id':'<?php echo $id; ?>'},
		'onAllComplete': function(event,data){
			location.href='view-photo-album.php?id=<?php echo $id; ?>';
	     }
	});

	
});

</script>
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Add Ticketing Related Photo </div>
		<div id="back_to_list"><a href="ticketing-price-listing.php?ticketing_info_id=<?php echo $ticketing_info_id; ?>">Back to List</a></div>
		<div id="content">
			<form action="<?php $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
			
				<div>
		<table width="758" border="0" cellpadding="5" cellspacing="0" style="font-size:12px;border-top:1px solid #B6BEBE;margin-top:20px">
		<tr>
			<td width="204" class="functionlink bottomline leftline blue_column"><strong>Ticketing Category: </strong></td>
			<td width="534" style="border-bottom:1px solid #B6BEBE; border-right:1px solid #B6BEBE">
			<?php
				$q1="SELECT ticketing_category FROM ticketing_category WHERE id='".mysql_real_escape_string($row_enter['ticketing_category_id'])."'";
				$result1=mysql_query($q1,$link) or die(mysql_error());
				$row1=mysql_fetch_array($result1);
				
				echo $row1['ticketing_category'];
			?>
			</td>
		</tr>
		<tr>
			<td class="functionlink bottomline leftline blue_column"><strong>Ticketing SubCategory: </strong></td>
			<td style="border-bottom:1px solid #B6BEBE; border-right:1px solid #B6BEBE"><?php echo $row_enter['ticket_subcategory']; ?></td>
		</tr>
		<tr>
			<td class="functionlink bottomline leftline blue_column"><strong>Ticketing SubCategory Chinese: </strong></td>
			<td style="border-bottom:1px solid #B6BEBE; border-right:1px solid #B6BEBE"><?php echo $row_enter['ticket_subcategory_chinese']; ?></td>
		</tr>
		<tr>
			<td class="functionlink bottomline leftline blue_column"><strong>Access to Zones: </strong></td>
			<td style="border-bottom:1px solid #B6BEBE; border-right:1px solid #B6BEBE"><?php echo $row_enter['access_to_zones']; ?></td>
		</tr>
		<tr>
			<td class="functionlink bottomline leftline blue_column"><strong>Accessible Entertainment Stages: </strong></td>
			<td style="border-bottom:1px solid #B6BEBE; border-right:1px solid #B6BEBE"><?php echo $row_enter['accessible_entertainment_stages']; ?></td>
		</tr>
		</table>
		
		
			  </div>
				<div class="clearboth"></div>
			<div>
<h4>NOTE: Thumbnails with fixed width 70, and fixed width 200  will be generated</h4>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<tr>
					<td width="126" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="126" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Image 1 : <br>
				    (639 x 425)					</strong></td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="image[]" value=""></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="126" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Image 2 : <br>
				    (639 x 425)					</strong></td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="image[]" value=""></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="126" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Image 3 : <br>
				    (639 x 425)					</strong></td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="image[]" value=""></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="126" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Image 4 : <br>
				    (639 x 425)					</strong></td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="image[]" value=""></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="126" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Image 5 : <br>
				    (639 x 425)					</strong></td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="image[]" value=""></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="126" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"></td>
					<td width="587" style="border-bottom:1px dotted #BAB9B9;"><input type="submit" name="submit" value="Upload Ticketing Related Photo"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				
				
					<td></td>
					<td></td>
				<td>&nbsp;</td>
				</tr>
				</table>
			  </div>
			  <input type="hidden" name="entertainment_id" value="<?php echo $entertainment_id; ?>">
		  </form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

