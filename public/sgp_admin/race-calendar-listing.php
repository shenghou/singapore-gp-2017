<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
	exit();
}

if(isset($_REQUEST['search_value'])) {
	$search_value=$_REQUEST['search_value'];
	if($search_value!="") {
		$q_search="AND (race LIKE '%$search_value%' OR date LIKE '%$search_value%')";

	}
}




include("include/configure.php");

if(isset($_POST['update_status'])) {
	$module_status=$_POST['status'];
	
	$q_m_status="UPDATE status_module SET status='".mysql_real_escape_string($module_status)."' WHERE  status_module_name='Race Calendar'";
	$result_m_status=mysql_query($q_m_status,$link) or die(mysql_error());

}


if(empty($_REQUEST['page']))
{
	$page=1;
}
else
{
	if($_REQUEST['page']=="")
	{
		$page=1;
	}
	else
	{
		$page=$_REQUEST['page'];
	}
}
			
$site = $_SERVER['PHP_SELF'];
$range=25;//the range of pages display
$limit=20;//the limit of rows display in a page

$sqlcount="SELECT id FROM race_calendar WHERE id!='' $q_search ORDER BY id";
$resultcount=mysql_query($sqlcount,$link)or die(mysql_error());
$count=mysql_num_rows($resultcount);
$totalpage=ceil($count/$limit);
					
$limitvalue=($page*$limit)-$limit;

if($_GET['orderby']=="") {
	$orderby="id";
} else {
	$orderby=$_GET['orderby'];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/ajax.js"></script>
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Race Calendar Listing </div>
		
		<div class="searching_field">
		<form action="<?php $PHP_SELF; ?>" method="get">
		<table width="248" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="46">Search</td>
			<td width="133"><input type="text" name="search_value" value="<?php echo stripslashes($search_value); ?>"></td>
			<td width="69"><input type="submit" name="search" value="search"></td>
		</tr>
		</table>
		</form>
		</div>
		<div class="clearboth"></div>
		
		<form action="<?php $PHP_SELF; ?>" method="post">
		<?php
		$q_m_status="SELECT status FROM status_module WHERE  status_module_name='Race Calendar'";
		$result_m_status=mysql_query($q_m_status,$link) or die(mysql_error());
		$row_m_status=mysql_fetch_array($result_m_status);
		?>
		<table width="248" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="46">Status:</td>
			<td width="133">
			<select name="status">
				<option value="Active" <?php if($row_m_status['status']=="Active") { echo "selected"; } ?>>Active</option>
				<option value="Inactive" <?php if($row_m_status['status']=="Inactive") { echo "selected"; } ?>>In Active</option>
			</select>
			</td>
			<td width="69"></td>
		</tr>
		<tr>
			<td width="46"></td>
			<td width="133">
			&nbsp;
			</td>
			<td width="69"></td>
		</tr>
		<tr>
			
			<td width="133" colspan="2">
			<input type="submit" name="update_status" value="Update Status">
			</td>
			<td width="69"></td>
		</tr>
		</table>
		</form>
		
		<div class="clearboth"></div>
		<br>
		<br>
		
		
		
		
		<div class="functionlink"><a href="add-new-race-calendar.php">Add New Race Calendar</a></div>
		<div id="content_list">
			<table width="760" align="left" cellpadding="5" cellspacing="0" style="border-top:1px solid #B6BEBE;">
			<tr class="blue_column">
				<td width="41" class="functionlink bottomline leftline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=id&search_value=$search_value"; ?>">No</a></strong></td>
				<td width="402" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=race&search_value=$search_value"; ?>">Race</a></strong></td>
				<td width="182" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=date&search_value=$search_value"; ?>">Date</a></strong></td>
				<td width="34" class="bottomline" align="center"><strong>Edit</strong></td>
				<td width="49" class="bottomline" align="center"><strong>Delete</strong></td>
			</tr>
			<?php 
			   if($page!=1 && $page!="") {
						$pagecount=$page-1;
						$num=$pagecount * $limit;
			   	}
				$q="SELECT * FROM race_calendar WHERE id!='' $q_search ORDER BY $orderby LIMIT $limitvalue,$limit";
			   $result=mysql_query($q,$link) or die(mysql_error());
			   while($row=mysql_fetch_array($result)) {
			  	 	$num=$num+1;
					
					
					
			   		?>
					<tr>
						<td width="41" class="bottomline leftline"><?php echo $num; ?></td>
						<td width="402" class="bottomline"><?php echo $row['race']; ?></td>
						<td width="182" class="bottomline"><?php echo $row['date']; ?></td>
						
						<td width="34" class="functionlink bottomline" align="center">
							<a href="edit-race-calendar.php?id=<?php echo $row['id']; ?>&page=<?php echo $page; ?>&orderby=<?php echo $orderby; ?>&search_value=<?php echo $search_value; ?>"><img src="images/edit.jpg" border="none"></a>
						
						</td>
						<td width="49" class="functionlink bottomline" align="center">
						
							<a href="javascript:;" onClick="DeleteRaceCalendar('<?php echo $row['id']; ?>')"><img src="images/delete.gif" border="none"></a>
						
						</td>
					</tr>
					<?php
			   }
			?>
			</table>
			
			
			 <div class="clearboth"></div>
				<table border="0" cellspacing="0" cellpadding="0" class="page_table" align="left">
				<tr>
				<td align="left" class="style5"><?php
				$lrange = max(1,$page-(($range-1)/2));
				$rrange = min($totalpage,$page+(($range-1)/2));
				if (($rrange - $lrange) < ($range - 1))
					{
						if ($lrange == 1)
							{
								$rrange = min($lrange + ($range-1), $totalpage);
							}
						else
							{
								$lrange = max($rrange - ($range-1), 0);
							}
					}
				if($totalpage>1)
				{
					if ($page > 1)//previous page
						{
							$prev=$page-1;
							echo " <a href='".$PHP_SELF."?page=1&orderby=$orderby&search_value=$search_value'><< First</a> | "; 
							echo " <a href='".$PHP_SELF."?page=$prev&orderby=$orderby&search_value=$search_value'>< Prev</a> | ";
						}
					else
						{
							echo " <strong><< First</strong> | <strong>< Prev</strong> |";
						}
					for($i = 1; $i <= $totalpage; $i++)//the range of page
						{
							if ($i == $page)
								{
									echo " <span class='actual_page'><strong>$i</strong></span>";
								}
							else
								{
									if ($lrange <= $i and $i <= $rrange)
										{
											echo " <a href='".$PHP_SELF."?page=$i&orderby=$orderby&search_value=$search_value' class='bluecolor word fontsubtitle'>$i</a> ";
										}
								}
						}
					if ($page < $totalpage)//next page
						{
							$next=$page+1;
							echo "| <a href='".$PHP_SELF."?page=$next&orderby=$orderby&search_value=$search_value'>Next ></a> |";
							echo " <a href='".$PHP_SELF."?page=$totalpage&orderby=$orderby&search_value=$search_value'>Last >></a>";
							
						}
					else
						{
							echo " | <strong>Next ></strong> | <strong>Last >></strong>";
						}
				}//end of if $totalpage > 1
			?></td>
			  </tr>
			</table>
				
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

