<?php 

include("include/configure.php");
require_once '../function/helper.php';
require_once '../function/cms-helper.php';

check_session_exists();

$q_search = '1 = 1';
if(isset($_REQUEST['search_value'])) {
	$search_value=$_REQUEST['search_value'];
	if($search_value!="") {
		$q_search="AND (team LIKE '%$search_value%' OR points LIKE '%$search_value%' OR driver LIKE '%$search_value%')";

	}
}

if(isset($_POST['update'])) {

	$points=$_POST['points'];
	$id_update=$_POST['id'];
	
	$count_update=count($id_update);
	
	for($i=0;$i<$count_update;$i++) {
		list($team, $driver) = explode('|', $_POST['driver'][$i]);
	
		$sql = "UPDATE drivers_result SET team='".mysql_real_escape_string($team)."',points='".mysql_real_escape_string($points[$i])."',driver='".mysql_real_escape_string($driver)."' WHERE id='".mysql_real_escape_string($id_update[$i])."'";
		mysql_query($sql, $link) or die(mysql_error());

		if (isset($link1) && is_resource($link1)) {
			mysql_query($sql, $link1) or die(mysql_error());
		}

		if (isset($link2) && is_resource($link2)) {
			mysql_query($sql, $link2) or die(mysql_error());
		}
	}
	$successword="<strong>Driver Result update successfully.</strong><br><br>";
}



if(isset($_POST['update_status'])) {
	$module_status=$_POST['status'];
	
	$sql = "UPDATE status_module SET status='".mysql_real_escape_string($module_status)."' WHERE  status_module_name='Driver Result'";
	mysql_query($sql, $link) or die(mysql_error());

	if (isset($link1) && is_resource($link1)) {
		mysql_query($sql, $link1) or die(mysql_error());
	}

	if (isset($link2) && is_resource($link2)) {
		mysql_query($sql, $link2) or die(mysql_error());
	}
}

if(empty($_REQUEST['page']))
{
	$page=1;
}
else
{
	if($_REQUEST['page']=="")
	{
		$page=1;
	}
	else
	{
		$page=$_REQUEST['page'];
	}
}
			
$site = $_SERVER['PHP_SELF'];
$range=25;//the range of pages display
$limit=25;//the limit of rows display in a page

$sqlcount="SELECT id FROM drivers_result WHERE $q_search";
$resultcount=mysql_query($sqlcount,$link)or die(mysql_error());
$count=mysql_num_rows($resultcount);
$totalpage=ceil($count/$limit);
					
$limitvalue=($page*$limit)-$limit;

if (empty($_GET['orderby'])) {
	$orderby = 'points desc';
} else {
	$orderby=$_GET['orderby'];
}

$connections = db_write_connect($all = true);
$connection = current($connections);
#
$sql = 'SELECT team.name As team_name, driver.name as driver FROM driver LEFT JOIN team On driver.team_id = team.id ORDER BY team.seq, driver.lead_driver DESC';
$rows = db_select_all($sql, $connection);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/ajax.js"></script>
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Driver Result Listing </div>
		
		<div class="searching_field">
		<form action="<?php $PHP_SELF; ?>" method="get">
		<table width="248" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="46">Search</td>
			<td width="133"><input type="text" name="search_value" value="<?php echo stripslashes($search_value); ?>"></td>
			<td width="69"><input type="submit" name="search" value="search"></td>
		</tr>
		</table>
		</form>
		</div>
		<div class="clearboth"></div>
		
		
		
		
		<form action="<?php $PHP_SELF; ?>" method="post">
		<?php
		$q_m_status="SELECT status FROM status_module WHERE  status_module_name='Driver Result'";
		$result_m_status=mysql_query($q_m_status,$link);
		$row_m_status=mysql_fetch_assoc($result_m_status);
		?>
		<table width="248" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="46">Status:</td>
			<td width="133">
			<select name="status">
				<option value="Active" <?php if($row_m_status['status']=="Active") { echo "selected"; } ?>>Active</option>
				<option value="Inactive" <?php if($row_m_status['status']=="Inactive") { echo "selected"; } ?>>In Active</option>
			</select>
			</td>
			<td width="69"></td>
		</tr>
		<tr>
			<td width="46"></td>
			<td width="133">
			&nbsp;
			</td>
			<td width="69"></td>
		</tr>
		<tr>
			
			<td width="133" colspan="2">
			<input type="submit" name="update_status" value="Update Status">
			</td>
			<td width="69"></td>
		</tr>
		</table>
		</form>
		
		<div class="clearboth"></div>
		<br>
		<br>
		
		
		
		<div class="functionlink"><a href="add-new-driver-result.php">Add New Driver Result</a></div>
		<form action="<?php $PHP_SELF; ?>" method="post">
		<div id="content_list">
			<div><?php echo $successword; ?></div>
			<table width="760" align="left" cellpadding="5" cellspacing="0" style="border-top:1px solid #B6BEBE;">
			<tr class="blue_column">
				<td width="38" height="26" class="functionlink bottomline leftline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=id&search_value=$search_value"; ?>">No</a></strong></td>
				<td width="219" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=driver&search_value=$search_value"; ?>">Team & Driver</a></strong></td>
				<td width="157" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=points&search_value=$search_value"; ?>">Points</a></strong></td>
			</tr>
			<?php 
			   if($page!=1 && $page!="") {
						$pagecount=$page-1;
						$num=$pagecount * $limit;
			   	}
				$sql = "SELECT * FROM drivers_result WHERE $q_search ORDER BY $orderby LIMIT $limitvalue,$limit";
				$result = mysql_query($sql, $link);

			   while($row=mysql_fetch_assoc($result)) {
			  	 	$num=$num+1;
					
					
					
			   		?>
					<tr>
						<td width="38" class="bottomline leftline"><?php echo $num; ?></td>
<?php
/*
						<td width="212" class="bottomline">
						<input type="text" name="team[]" value="<?php echo $row['team']; ?>" style="width:212px">
						</td>
						
						<td width="219" class="bottomline">
						<input type="text" name="driver[]" value="<?php echo $row['driver']; ?>" style="width:219px">
						</td>
*/
?>
<td class="bottomline">
<select name="driver[]">
<?php
foreach ($rows as $_row) {
	$same = $row['driver'] == $_row['driver'] ? ' selected' : null;
	$val = implode('|', $_row);
	printf('<option value="%s"%s>%s</option>'.PHP_EOL, $val, $same, $_row['team_name'] . ' - ' . $_row['driver']);
}
?>
</select>
</td>
						<td width="157" class="bottomline">
						<input type="text" name="points[]" value="<?php echo $row['points']; ?>" style="width:157px">
						</td>
					</tr>
					<input type="hidden" name="id[]" value="<?php echo $row['id']; ?>">
					<?php
			   }
			?>
			</table>
			
			
			 <div class="clearboth"></div>
				<table border="0" cellspacing="0" cellpadding="0" class="page_table" align="left">
				<tr>
				<td align="left" class="style5"><?php
				$lrange = max(1,$page-(($range-1)/2));
				$rrange = min($totalpage,$page+(($range-1)/2));
				if (($rrange - $lrange) < ($range - 1))
					{
						if ($lrange == 1)
							{
								$rrange = min($lrange + ($range-1), $totalpage);
							}
						else
							{
								$lrange = max($rrange - ($range-1), 0);
							}
					}
				if($totalpage>1)
				{
					if ($page > 1)//previous page
						{
							$prev=$page-1;
							echo " <a href='".$PHP_SELF."?page=1&orderby=$orderby&search_value=$search_value'><< First</a> | "; 
							echo " <a href='".$PHP_SELF."?page=$prev&orderby=$orderby&search_value=$search_value'>< Prev</a> | ";
						}
					else
						{
							echo " <strong><< First</strong> | <strong>< Prev</strong> |";
						}
					for($i = 1; $i <= $totalpage; $i++)//the range of page
						{
							if ($i == $page)
								{
									echo " <span class='actual_page'><strong>$i</strong></span>";
								}
							else
								{
									if ($lrange <= $i and $i <= $rrange)
										{
											echo " <a href='".$PHP_SELF."?page=$i&orderby=$orderby&search_value=$search_value' class='bluecolor word fontsubtitle'>$i</a> ";
										}
								}
						}
					if ($page < $totalpage)//next page
						{
							$next=$page+1;
							echo "| <a href='".$PHP_SELF."?page=$next&orderby=$orderby&search_value=$search_value'>Next ></a> |";
							echo " <a href='".$PHP_SELF."?page=$totalpage&orderby=$orderby&search_value=$search_value'>Last >></a>";
							
						}
					else
						{
							echo " | <strong>Next ></strong> | <strong>Last >></strong>";
						}
				}//end of if $totalpage > 1
			?></td>
			  </tr>
			</table>
				
		</div>
		<br>
		<br>
		<input type="submit" name="update" value="Update">
		</form>	
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

