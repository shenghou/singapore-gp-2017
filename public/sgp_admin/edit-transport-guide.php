<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");

$id=$_REQUEST['id'];

if(isset($_POST['submit'])) {
	$error=0;
	
	$transport_guide_location_id=$_POST['transport_guide_location_id'];
	$transport_guide=$_POST['transport_guide'];
	$mrt=$_POST['mrt'];
	$shuttle_bus=$_POST['shuttle_bus'];
	$taxi=$_POST['taxi'];
	$other_transport=$_POST['other_transport'];
	$note=$_POST['note'];

	

	
	if($transport_guide_location_id=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please select Location Name.";
	}
	if($transport_guide=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Transport Guide.";
	}
	
	
	
	if($error!=1) {

			$datetime=date("Y-m-d H:i:s");
			
			$q="UPDATE transport_guide SET transport_guide_location_id='".mysql_real_escape_string($transport_guide_location_id)."',transport_guide='".mysql_real_escape_string($transport_guide)."',mrt='".mysql_real_escape_string($mrt)."',shuttle_bus='".mysql_real_escape_string($shuttle_bus)."',taxi='".mysql_real_escape_string($taxi)."',other_transport='".mysql_real_escape_string($other_transport)."',note='".mysql_real_escape_string($note)."',updater='".mysql_real_escape_string($_SESSION[cms_username])."' WHERE id='".mysql_real_escape_string($id)."'";
			$result=mysql_query($q,$link) or die(mysql_error());

			if (is_resource($link1))
			{
				mysql_query($q, $link1) or die(mysql_error());
			}

			if (is_resource($link2))
			{
				mysql_query($q, $link2) or die(mysql_error());
			}

			header('Location:transport-guide-listing.php?page='.$_REQUEST[page].'&orderby='.$_REQUEST[orderby].'&search_value='.$_REQUEST[search_value].'');
			exit();
			
		
	}

} else {
	$q="SELECT * FROM transport_guide WHERE id='".mysql_real_escape_string($_REQUEST[id])."'";
	$result=mysql_query($q,$link) or die(mysql_error());
	$row=mysql_fetch_array($result);
	
	$transport_guide_location_id=$row['transport_guide_location_id'];
	$transport_guide=$row['transport_guide'];
	$mrt=$row['mrt'];
	$shuttle_bus=$row['shuttle_bus'];
	$taxi=$row['taxi'];
	$other_transport=$row['other_transport'];
	$note=$row['note'];
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript">

tinyMCE.init({

        // General options
		
		
        mode: "exact",

    	elements: "transport_guide,mrt,shuttle_bus,taxi,other_transport",

        theme : "advanced",

        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

file_browser_callback : 'tinyBrowser',

        // Theme options

        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect",

       	theme_advanced_buttons2 : "fontsizeselectcut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview",

       theme_advanced_buttons3 : "forecolor,backcolor,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",

       theme_advanced_buttons4 : "print,|,ltr,rtl,|,fullscreen,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",

        theme_advanced_toolbar_location : "top",

        theme_advanced_toolbar_align : "left",
		
		

        theme_advanced_statusbar_location : "bottom",

        theme_advanced_resizing : true,

		theme_advanced_blockformats : "p,div,address,pre,h1,h2,h3,h4,h5,h6",

		forced_root_block : '',

        // Skin options

        skin : "o2k7",

        skin_variant : "silver",

		
        // Example content CSS (should be your site CSS)

        content_css : "../css/page.css,../css/txtcolor.css",



        // Drop lists for link/image/media/template dialogs

        template_external_list_url : "js/template_list.js",

        external_link_list_url : "js/link_list.js",

        external_image_list_url : "js/image_list.js",

        media_external_list_url : "js/media_list.js",

		convert_urls : false,
		
		forced_root_block : 'p',
		
		

        // Replace values for the template plugin

        template_replace_values : {

                username : "Some User",

                staffid : "991234"

        }
		

});

</script>
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Edit  Transport Guide</div>
		<div id="back_to_list"><a href="transport-guide-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post">
			<div>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Location Name  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="transport_guide_location_id">
						<option value="">Please Select</option>
						<?php
						$q1="SELECT id,location FROM transport_guide_location ORDER BY id";
						$result1=mysql_query($q1,$link) or die(mysql_error());
						while($row1=mysql_fetch_array($result1)) {
							echo "<option value='".$row1['id']."'";
							if($transport_guide_location_id==$row1['id']) { echo "selected"; }
							echo ">".$row1['location']."</option>";
						}
						?>
					</select>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Transport Guide  : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="transport_guide" style="width:500px;height:250px;font-size:11px"><?php echo stripslashes($transport_guide); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>MRT  :  </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="mrt" style="width:500px;height:350px;font-size:11px"><?php echo stripslashes($mrt); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Shuttle Bus  :  </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="shuttle_bus" style="width:500px;height:350px;font-size:11px"><?php echo stripslashes($shuttle_bus); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Taxi  :  </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="taxi" style="width:500px;height:350px;font-size:11px"><?php echo stripslashes($taxi); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Other Transport  :  </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="other_transport" style="width:500px;height:350px;font-size:11px"><?php echo stripslashes($other_transport); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Note  :  </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="note" style="width:500px;height:100px;font-size:11px"><?php echo stripslashes($note); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Update Transport Guide"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				
			  </div>
		  </form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

