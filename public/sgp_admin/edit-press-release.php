<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");
require_once '../function/helper.php';
require_once '../function/cms-helper.php';

$id=$_REQUEST['id'];

if(isset($_POST['submit'])) {
	$error=0;

	
	$press_release_date=$_POST['press_release_date'];
	$title=$_POST['title'];
	$subtitle=$_POST['subtitle'];
	$content=$_POST['content'];
	$meta_keyword=$_POST['meta_keyword'];
	$meta_description=$_POST['meta_description'];
	
	if($press_release_date=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please select Press Release Date.";
	}
	if($title=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Title.";
	}
	if($content=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Content.";
	}
	
	
	
	if($error!=1) {
			
			$press_release_date_array=explode("/",$press_release_date);
			$press_release_date1="$press_release_date_array[2]-$press_release_date_array[1]-$press_release_date_array[0]";
			
			
			$q="UPDATE press_release SET press_release_date='".mysql_real_escape_string($press_release_date1)."',press_release_year='".mysql_real_escape_string($press_release_date_array[2])."',title='".mysql_real_escape_string($title)."',subtitle='".mysql_real_escape_string($subtitle)."',content='".mysql_real_escape_string($content)."',updater='".mysql_real_escape_string($_SESSION[cms_username])."',meta_keyword='".mysql_real_escape_string($meta_keyword)."',meta_description='".mysql_real_escape_string($meta_description)."' WHERE id='".mysql_real_escape_string($id)."'";
//			$result=mysql_query($q,$link) or die(mysql_error());
            db_update_raw($q);
		
			header('Location:press-release-listing.php?page='.$_REQUEST[page].'&orderby='.$_REQUEST[orderby].'&search_value='.$_REQUEST[search_value].'');
			exit();
			
		
	}

} else {
	
	$q="SELECT * FROM press_release WHERE id='$_REQUEST[id]'";
	$result=mysql_query($q,$link) or die(mysql_error());
	$row=mysql_fetch_array($result);
	
	$press_release_date=$row['press_release_date'];
	$press_release_year=$row['press_release_year'];
	$title=$row['title'];
	$subtitle=$row['subtitle'];
	$content=$row['content'];
	$meta_keyword=$row['meta_keyword'];
	$meta_description=$row['meta_description'];
	
	if($press_release_date!="") {
			$Dated_array="";
			$Dated_array=explode("-",$press_release_date);
			$press_release_date="$Dated_array[2]/$Dated_array[1]/$Dated_array[0]";
	}
	
	
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/datePicker.css">
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/date.js"></script>
<script type="text/javascript" src="js/jquery.datePicker.js"></script>
<script type="text/javascript">

$(function()
{
	$('.date-pick').datePicker(
	{
		startDate: '04/08/1000',
		endDate: '04/08/3010'
	}
	);
});
</script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript">

tinyMCE.init({

        // General options
		
		
        mode: "exact",

    	elements: "content,subtitle",

        theme : "advanced",

        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",


        // Theme options

        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect",

       	theme_advanced_buttons2 : "fontsizeselectcut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview",

       theme_advanced_buttons3 : "forecolor,backcolor,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",

       theme_advanced_buttons4 : "print,|,ltr,rtl,|,fullscreen,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",

        theme_advanced_toolbar_location : "top",

        theme_advanced_toolbar_align : "left",
		
		

        theme_advanced_statusbar_location : "bottom",

        theme_advanced_resizing : true,

		theme_advanced_blockformats : "p,div,address,pre,h1,h2,h3,h4,h5,h6",

		forced_root_block : '',

        // Skin options

        skin : "o2k7",

        skin_variant : "silver",

		
        // Example content CSS (should be your site CSS)

        content_css : "../css/page.css,../css/txtcolor.css",



        // Drop lists for link/image/media/template dialogs

        template_external_list_url : "js/template_list.js",

        external_link_list_url : "js/link_list.js",

        external_image_list_url : "js/image_list.js",

        media_external_list_url : "js/media_list.js",

		convert_urls : false,
		
		forced_root_block : 'p',
		
		

        // Replace values for the template plugin

        template_replace_values : {

                username : "Some User",

                staffid : "991234"

        }
		

});

</script>
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Edit Press Release</div>
		<div id="back_to_list"><a href="press-release-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post">
			<div>
				
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Date of Release: *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="press_release_date"  value="<?php echo $press_release_date; ?>" style="width:500px" class="date-pick"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Title  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="title"  value="<?php echo $title; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Sub Title  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="subtitle" style="width:500px;height:250px;font-size:11px"><?php echo stripslashes($subtitle); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Content  : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="content" style="width:500px;height:350px;font-size:11px"><?php echo stripslashes($content); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Meta Keyword  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="meta_keyword" style="width:500px;height:100px;font-size:11px"><?php echo stripslashes($meta_keyword); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Meta Description  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="meta_description" style="width:500px;height:100px;font-size:11px"><?php echo stripslashes($meta_description); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Update Press Release"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				</div>
				</form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

