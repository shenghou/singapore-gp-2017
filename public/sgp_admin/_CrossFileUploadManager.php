<?php
class CrossFileUploadManager{

    public static function uploadFiles($data=[]){

        $url = 'http://web2.singaporegp.sg/Uploaded/fileuploader.php';

        $postParam = '';
        $n=sizeof($data);
        for ($i=0;$i<$n;$i++) {
            $append = $i > 0 ? '&' : '';
            $postParam .= $append."files[$i]=".$data[$i];
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postParam);
        $response = curl_exec($ch);

//        $output = array();
//        $output['request'] = $data;

//        $file = 'CrossFileUploadManagerLog.log';
//        $current = file_get_contents($file);
//        $current .= "\n".time().' :: '.json_encode($output);
//        file_put_contents($file, $current);

        return $response;

    }
}