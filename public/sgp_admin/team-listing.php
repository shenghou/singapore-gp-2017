<?php
require_once '../function/helper.php';
require_once '../function/cms-helper.php';

check_session_exists();

$connections = db_write_connect($all = true);
$connection = current($connections);

$sql = 'SELECT * FROM team ORDER BY seq';
$rows = db_select_all($sql, $connection);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Team Listing</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/cms.css" rel="stylesheet" type="text/css" />
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>

	<div id="right_container">
		<h1>Team Listing</h1>
		<div class="functionlink"><a href="add-team.php">Add New Team</a></div>

<div id="message">
</div>

<div id="content_list">
	<table width="450" align="left" cellpadding="5" cellspacing="0" style="border-top:1px solid #B6BEBE;">
		<tr class="blue_column">
			<td width="35" class="functionlink bottomline leftline"><strong>Seq.</strong></td>
			<td class="functionlink bottomline"><strong>Team</strong></td>
			<td class="functionlink bottomline"><strong>Status</strong></td>
			<td class="functionlink bottomline"><strong>Action</strong></td>
		</tr>
<?php
if (!empty($rows)) {
	foreach ($rows as $row) {
		$name = htmlspecialchars($row['full_name']);
		$status = sprintf('<b class="%s">%s</b>', ($row['status'] ? 'active' : 'pending'), ($row['status'] ? 'active' : 'pending'));

		echo <<<row

<tr>
	<td class="bottomline leftline">{$row['seq']}.</td>
	<td class="bottomline">{$name}</td>
	<td class="bottomline">{$status}</td>
	<td class="bottomline"><a href="edit-team.php?id={$row['id']}"><img src="images/edit.jpg"></a></td>
</tr>

row;
	}
}
?>
	</table>
</div>

	</div>
	<div class="clearboth"></div>
</div>

</body>
</html>

