<?php
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");
include ("include/resize.image.class.php");

$successword='';
$errorword='';

$id=(isset($_REQUEST['id'])?$_REQUEST['id']:"");
$AlbumDate_Variable[2]=(isset($_POST['Album_Day'])?$_POST['Album_Day']:"");
$AlbumDate_Variable[1]=(isset($_POST['Album_Month'])?$_POST['Album_Month']:"");
$AlbumDate_Variable[0]=(isset($_POST['Album_Year'])?$_POST['Album_Year']:"");
$title=(isset($_POST['title'])?$_POST['title']:"");
$description=(isset($_POST['description'])?$_POST['description']:"");

if(isset($_POST['submit'])) {
	$error=0;


	$AlbumDate_Variable[2]=$Album_Day=$_POST['Album_Day'];
	$AlbumDate_Variable[1]=$Album_Month=$_POST['Album_Month'];
	$AlbumDate_Variable[0]=$Album_Year=$_POST['Album_Year'];
	$title=$_POST['title'];
	$description=$_POST['description'];

	$AlbumDate="$AlbumDate_Variable[0]-$AlbumDate_Variable[1]-$AlbumDate_Variable[2]";
	$Date_variable=date("d-F-Y",strtotime($AlbumDate));
	$Year=$Album_Year;
	$Date=$Album_Day;

	$Date_word=date("l, d F Y",strtotime($AlbumDate));

	if($title=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Title.";
	}
	if($description=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Description.";
	}


	if($error!=1) {

		$q="INSERT INTO photo_album (title,description,updater,AlbumDate,Date_word,Date_variable,year,date) VALUES ('".mysqli_real_escape_string($link, $title)."','".mysqli_real_escape_string($link, $description)."','".mysqli_real_escape_string($link, $_SESSION['cms_username'])."','".mysqli_real_escape_string($link, $AlbumDate)."','".mysqli_real_escape_string($link, $Date_word)."','".mysqli_real_escape_string($link, $Date_variable)."','".mysqli_real_escape_string($link, $Album_Year)."','".mysqli_real_escape_string($link, $Date)."')";
		$result=mysqli_query($link, $q) or die(mysqli_error($link));

		if (isset($link1)) {
			mysqli_query($link1, $q) or die(mysqli_error($link1));
		}

		if (isset($link2)) {
			mysqli_query($link2, $q) or die(mysqli_error($link2));
		}

		header('Location:list-photo-album.php');
		exit();


	}

}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Admin Panel - Main Page</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript">

	tinyMCE.init({

		// General options


		mode: "exact",

		elements: "content,subtitle",

		theme : "advanced",

		plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",


		// Theme options

		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect",

		theme_advanced_buttons2 : "fontsizeselectcut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview",

		theme_advanced_buttons3 : "forecolor,backcolor,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",

		theme_advanced_buttons4 : "print,|,ltr,rtl,|,fullscreen,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",

		theme_advanced_toolbar_location : "top",

		theme_advanced_toolbar_align : "left",



		theme_advanced_statusbar_location : "bottom",

		theme_advanced_resizing : true,

		theme_advanced_blockformats : "p,div,address,pre,h1,h2,h3,h4,h5,h6",

		forced_root_block : '',

		// Skin options

		skin : "o2k7",

		skin_variant : "silver",


		// Example content CSS (should be your site CSS)

		content_css : "../css/page.css,../css/txtcolor.css",



		// Drop lists for link/image/media/template dialogs

		template_external_list_url : "js/template_list.js",

		external_link_list_url : "js/link_list.js",

		external_image_list_url : "js/image_list.js",

		media_external_list_url : "js/media_list.js",

		convert_urls : false,

		forced_root_block : 'p',



		// Replace values for the template plugin

		template_replace_values : {

			username : "Some User",

			staffid : "991234"

		}


	});

</script>
<body>
<div id="container">

	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Add Photo Album</div>
		<div id="back_to_list"><a href="list-photo-album.php?id<?=$id?>">Back to List</a></div>
		<div id="contents">
			<form action="add-photo-album.php" method="post" enctype="multipart/form-data">
				<div>

					<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
						<?php if($successword=="") { ?>
							<tr>
								<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
								<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
								<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
							</tr>
						<?php } ?>

						<tr>
							<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Album Date  : *</strong></td>
							<td width="551" style="border-bottom:1px dotted #BAB9B9;">
								<select name="Album_Day">
									<option value="01" <?php if($AlbumDate_Variable[2]=="01") { echo "selected"; } ?>>01</option>
									<option value="02" <?php if($AlbumDate_Variable[2]=="02") { echo "selected"; } ?>>02</option>
									<option value="03" <?php if($AlbumDate_Variable[2]=="03") { echo "selected"; } ?>>03</option>
									<option value="04" <?php if($AlbumDate_Variable[2]=="04") { echo "selected"; } ?>>04</option>
									<option value="05" <?php if($AlbumDate_Variable[2]=="05") { echo "selected"; } ?>>05</option>
									<option value="06" <?php if($AlbumDate_Variable[2]=="06") { echo "selected"; } ?>>06</option>
									<option value="07" <?php if($AlbumDate_Variable[2]=="07") { echo "selected"; } ?>>07</option>
									<option value="08" <?php if($AlbumDate_Variable[2]=="08") { echo "selected"; } ?>>08</option>
									<option value="09" <?php if($AlbumDate_Variable[2]=="09") { echo "selected"; } ?>>09</option>
									<?php
									for($i=10;$i<=31;$i++) {
										echo "<option value='".$i."'";
										if($AlbumDate_Variable[2]==$i) { echo "selected"; }
										echo ">".$i."</option>";
									}
									?>
								</select>
								-
								<select name="Album_Month">
									<option value="01" <?php if($AlbumDate_Variable[1]=="01") { echo "selected"; } ?>>Jan</option>
									<option value="02" <?php if($AlbumDate_Variable[1]=="02") { echo "selected"; } ?>>Feb</option>
									<option value="03" <?php if($AlbumDate_Variable[1]=="03") { echo "selected"; } ?>>Mar</option>
									<option value="04" <?php if($AlbumDate_Variable[1]=="04") { echo "selected"; } ?>>Apr</option>
									<option value="05" <?php if($AlbumDate_Variable[1]=="05") { echo "selected"; } ?>>May</option>
									<option value="06" <?php if($AlbumDate_Variable[1]=="06") { echo "selected"; } ?>>Jun</option>
									<option value="07" <?php if($AlbumDate_Variable[1]=="07") { echo "selected"; } ?>>Jul</option>
									<option value="08" <?php if($AlbumDate_Variable[1]=="08") { echo "selected"; } ?>>Aug</option>
									<option value="09" <?php if($AlbumDate_Variable[1]=="09") { echo "selected"; } ?>>Sep</option>
									<option value="10" <?php if($AlbumDate_Variable[1]=="10") { echo "selected"; } ?>>Oct</option>
									<option value="11" <?php if($AlbumDate_Variable[1]=="11") { echo "selected"; } ?>>Nov</option>
									<option value="12" <?php if($AlbumDate_Variable[1]=="12") { echo "selected"; } ?>>Dec</option>
								</select>
								-
								<select name="Album_Year">
									<?php
									$thisyear=date("Y");
									for($i=2008;$i<=$thisyear;$i++) {
										echo "<option value='".$i."'";
										if($AlbumDate_Variable[0]==$i) { echo "selected"; }
										echo ">".$i."</option>";
									}
									?>
								</select>
							</td>
							<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
						</tr>

						<tr>
							<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Title  : *</strong></td>
							<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="title"  value="<?php echo $title; ?>" style="width:500px" id="username"></td>
							<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
						</tr>

						<tr>
							<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Description  : *</strong></td>
							<td width="551" style="border-bottom:1px dotted #BAB9B9;">
								<textarea name="description" style="width:500px;height:100px;font-size:11px"><?php echo stripslashes($description); ?></textarea>
							</td>
							<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
						</tr>

						<tr>
							<td colspan="3">* Mandatory Field</td>
						</tr>

						<tr>
							<td></td>
							<td><input type="submit" name="submit" value="Update Photo Album"></td>
							<td>&nbsp;</td>
						</tr>
					</table>



				</div>
			</form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

