<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");
require_once '../function/cms-helper.php';
include_once("_CrossFileUploadManager.php");

$id=$_REQUEST['id'];

if(isset($_POST['submit'])) {
	$error=0;
	
	$ticketing_year=$_POST['ticketing_year'];
	$ticketing_category_id=$_POST['ticketing_category_id'];
	$ticket_subcategory=$_POST['ticket_subcategory'];
	$ticket_subcategory_chinese=$_POST['ticket_subcategory_chinese'];
	$ticket_subcategory_description=$_POST['ticket_subcategory_description'];
	$ticket_subcategory_description_chinese=$_POST['ticket_subcategory_description_chinese'];
	$access_to_zones_array=$_POST['access_to_zones_array'];
	$accessible_entertainment_stages_array=$_POST['accessible_entertainment_stages_array'];
	$announcement=$_POST['announcement'];
	$announcement_chinese=$_POST['announcement_chinese'];
	$description=$_POST['description'];
	$description_chinese=$_POST['description_chinese'];
	$zone_access_disclaimer=$_POST['zone_access_disclaimer'];
	$announcement_bottom=$_POST['announcement_bottom'];
	$announcement_bottom_chinese=$_POST['announcement_bottom_chinese'];
	$status=$_POST['status'];
	$view_type=$_POST['view_type'];
	$view_type_name=$_POST['view_type_name'];
	$view_type_name_chinese=$_POST['view_type_name_chinese'];
	$zone_access_disclaimer_chinese=$_POST['zone_access_disclaimer_chinese'];
	$accessible_info=$_POST['accessible_info'];
	$accessible_info_cn=$_POST['accessible_info_cn'];
	

	
	if($ticketing_category_id=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please select Ticket Category.";
	}
	if($ticket_subcategory=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Ticket SubCategory.";
	}
	if($ticket_subcategory_chinese=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Ticket SubCategory Chinese.";
	}
	
	if($description=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Description.";
	}
	if($description_chinese=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Description Chinese.";
	}
	
	
	
	if($error!=1) {
            $imageArray = array();

			$datetime=date("Y-m-d H:i:s");
			
			
			$access_to_zones=implode(", ",$access_to_zones_array);
			$accessible_entertainment_stages=implode(", ",$accessible_entertainment_stages_array);
			
			
			$q="SELECT sort FROM ticketing_info WHERE ticketing_category_id='".mysql_real_escape_string($ticketing_category_id)."' ORDER BY sort DESC";
			$result=mysql_query($q,$link) or die(mysql_error());
			$row=mysql_fetch_array($result);
			
			$sort=$row['sort'];
			$sort=$sort+1;
			
			
			$q="INSERT INTO ticketing_info (
			ticketing_category_id,
			ticket_subcategory,
			ticket_subcategory_chinese,
			ticket_subcategory_description,
			ticket_subcategory_description_chinese,
			access_to_zones,
			accessible_entertainment_stages,
			announcement,
			announcement_chinese,
			description,
			description_chinese,
			zone_access_disclaimer,
			updater,
			sort,
			ticketing_year,
			announcement_bottom,
			announcement_bottom_chinese,
			status,
			view_type,
			view_type_name,
			view_type_name_chinese,
			zone_access_disclaimer_chinese,
			accessible_info,
			accessible_info_cn
			) VALUES ('".
			mysql_real_escape_string($ticketing_category_id)."','".
			mysql_real_escape_string($ticket_subcategory)."','".
			mysql_real_escape_string($ticket_subcategory_chinese)."','".
			mysql_real_escape_string($ticket_subcategory_description)."','".
			mysql_real_escape_string($ticket_subcategory_description_chinese)."','".
			mysql_real_escape_string($access_to_zones)."','".
			mysql_real_escape_string($accessible_entertainment_stages)."','".
			mysql_real_escape_string($announcement)."','".
			mysql_real_escape_string($announcement_chinese)."','".
			mysql_real_escape_string($description)."','".
			mysql_real_escape_string($description_chinese)."','".
			mysql_real_escape_string($zone_access_disclaimer)."','".
			mysql_real_escape_string($_SESSION[cms_username])."','".
			mysql_real_escape_string($sort)."','".
			mysql_real_escape_string($ticketing_year)."','".
			mysql_real_escape_string($announcement_bottom)."','".
			mysql_real_escape_string($announcement_bottom_chinese)."','".
			mysql_real_escape_string($status)."','".
			mysql_real_escape_string($view_type)."','".
			mysql_real_escape_string($view_type_name)."','".
			mysql_real_escape_string($view_type_name_chinese)."','".
			mysql_real_escape_string($zone_access_disclaimer_chinese)."','".
			mysql_real_escape_string($accessible_info)."','".
			mysql_real_escape_string($accessible_info_cn)."'".
			")";
			mysql_query($q,$link) or die(mysql_error());

			if (isset($link1) && is_resource($link1)) {
				mysql_query($q,$link1) or die(mysql_error());
			}

			if (isset($link2) && is_resource($link2)) {
				mysql_query($q,$link2) or die(mysql_error());
			}

			$q="SELECT id FROM ticketing_info WHERE ticketing_category_id='".mysql_real_escape_string($ticketing_category_id)."' AND ticket_subcategory='".mysql_real_escape_string($ticket_subcategory)."' ORDER BY id DESC";
			$result=mysql_query($q,$link) or die(mysql_error());
			$row=mysql_fetch_array($result);
			$id=$row['id'];
			
			$target_paths="../Uploaded/ticketing/".$id."/";
			$target_path_database="Uploaded/ticketing/".$id."/";
			if(!is_dir($target_paths)) {
				if(mkdir($target_paths,777))
				{
					chmod($target_paths,0777); 
				}
			}//end if(!is_dir($target_path)) {

			if($_FILES['panoramic_view_path']['name']!="") {
	
					$panoramic_view_path1=$target_paths.basename( $_FILES['panoramic_view_path']['name']); 
					move_uploaded_file($_FILES['panoramic_view_path']['tmp_name'], $panoramic_view_path1);
					$panoramic_view_path="".$target_path_database.basename( $_FILES['panoramic_view_path']['name']);
					$q_panoramic_view_path=",panoramic_view_path='".mysql_real_escape_string($panoramic_view_path)."'";
					$upload_photo=1;

                    array_push($imageArray,"http://128.199.250.199/".$target_path_database.basename( $_FILES['panoramic_view_path']['name']));

            }
			
			if($_FILES['related_photo_path']['name']!="") {
	
					$related_photo_path1=$target_paths.basename( $_FILES['related_photo_path']['name']); 
					move_uploaded_file($_FILES['related_photo_path']['tmp_name'], $related_photo_path1);

                    $related_photo_path="http://web2.singaporegp.sg/".$target_path_database.basename( $_FILES['related_photo_path']['name']);
                    array_push($imageArray,"http://128.199.250.199/".$target_path_database.basename( $_FILES['related_photo_path']['name']));

					$q_related_photo_path=",related_photo_path='".mysql_real_escape_string($related_photo_path)."'";
					$upload_photo=1;
				
			}
			if($_FILES['seating_plan_path']['name']!="") {
	
					$seating_plan_path1=$target_paths.basename( $_FILES['seating_plan_path']['name']); 
					move_uploaded_file($_FILES['seating_plan_path']['tmp_name'], $seating_plan_path1);

                    $seating_plan_path="http://web2.singaporegp.sg/".$target_path_database.basename( $_FILES['seating_plan_path']['name']);
                    array_push($imageArray,"http://128.199.250.199/".$target_path_database.basename( $_FILES['seating_plan_path']['name']));

					$q_seating_plan_path=",seating_plan_path='".mysql_real_escape_string($seating_plan_path)."'";
					$upload_photo=1;
			}
			if($_FILES['seating_plan_thumb_path']['name']!="") {
	
					$seating_plan_thumb_path1=$target_paths.basename( $_FILES['seating_plan_thumb_path']['name']); 
					move_uploaded_file($_FILES['seating_plan_thumb_path']['tmp_name'], $seating_plan_thumb_path1);

                    $seating_plan_thumb_path="http://web2.singaporegp.sg/".$target_path_database.basename( $_FILES['seating_plan_thumb_path']['name']);
                    array_push($imageArray,"http://128.199.250.199/".$target_path_database.basename( $_FILES['seating_plan_thumb_path']['name']));

					$q_seating_plan_thumb_path=",seating_plan_thumb_path='".mysql_real_escape_string($seating_plan_thumb_path)."'";
					$upload_photo=1;
			}
			if($_FILES['zone_access_path']['name']!="") {
	
					$zone_access_path1=$target_paths.basename( $_FILES['zone_access_path']['name']); 
					move_uploaded_file($_FILES['zone_access_path']['tmp_name'], $zone_access_path1);

                    $zone_access_path="http://web2.singaporegp.sg/".$target_path_database.basename( $_FILES['zone_access_path']['name']);
                    array_push($imageArray,"http://128.199.250.199/".$target_path_database.basename( $_FILES['zone_access_path']['name']));

                    $q_zone_access_path=",zone_access_path='".mysql_real_escape_string($zone_access_path)."'";
					$upload_photo=1;

					# generate two thumbnails
					list($width, $height, $type) = getimagesize($zone_access_path1);
					$ratio = $width / $height;

					$the_height = 77;
					$the_width = round( $the_height * $ratio);
					$tb = str_replace('.jpg', '-sh'.$the_height.'.jpg', $zone_access_path1);
					generate_thumbnail($zone_access_path1, $tb, $the_width, $the_height);
			}

			if($_FILES['zone_access_thumb_path']['name']!="") {
	
					$zone_access_thumb_path1=$target_paths.basename( $_FILES['zone_access_thumb_path']['name']); 
					move_uploaded_file($_FILES['zone_access_thumb_path']['tmp_name'], $zone_access_thumb_path1);

                    $zone_access_thumb_path="http://web2.singaporegp.sg/".$target_path_database.basename( $_FILES['zone_access_thumb_path']['name']);
                    array_push($imageArray,"http://128.199.250.199/".$target_path_database.basename( $_FILES['zone_access_thumb_path']['name']));

					$q_zone_access_thumb_path=",zone_access_thumb_path='".mysql_real_escape_string($zone_access_thumb_path)."'";
					$upload_photo=1;
			}

			if($upload_photo==1) {
				$q="UPDATE ticketing_info SET updater='".mysql_real_escape_string($_SESSION[cms_username])."'$q_panoramic_view_path$q_related_photo_path$q_seating_plan_path$q_seating_plan_thumb_path$q_zone_access_path$q_zone_access_thumb_path WHERE id='".mysql_real_escape_string($id)."'";
				mysql_query($q,$link) or die(mysql_error());

				if (isset($link1) && is_resource($link1)) {
					mysql_query($q,$link1) or die(mysql_error());
				}

				if (isset($link2) && is_resource($link2)) {
					mysql_query($q,$link2) or die(mysql_error());
				}
			}

            // copy over to LIVE
            CrossFileUploadManager::uploadFiles($imageArray);

            header('Location:ticketing-info-listing.php?page='.$_REQUEST[page].'&orderby='.$_REQUEST[orderby].'&search_value='.$_REQUEST[search_value].'');
			exit();
			
		
	}

} else {
	$q="SELECT * FROM ticketing_info WHERE id='".mysql_real_escape_string($_REQUEST[id])."'";
	$result=mysql_query($q,$link) or die(mysql_error());
	$row=mysql_fetch_array($result);
	
	$ticketing_category_id=$row['ticketing_category_id'];
	$ticket_subcategory=$row['ticket_subcategory'];
	$ticket_subcategory_chinese=$row['ticket_subcategory_chinese'];
	$ticket_subcategory_description=$row['ticket_subcategory_description'];
	$ticket_subcategory_description_chinese=$row['ticket_subcategory_description_chinese'];
	$access_to_zones=$row['access_to_zones'];
	$accessible_entertainment_stages=$row['accessible_entertainment_stages'];
	$announcement=$row['announcement'];
	$announcement_chinese=$row['announcement_chinese'];
	$description=$row['description'];
	$description_chinese=$row['description_chinese'];
	$panoramic_view_path=$row['panoramic_view_path'];
	$related_photo_path=$row['related_photo_path'];
	$seating_plan_path=$row['seating_plan_path'];
	$seating_plan_thumb_path=$row['seating_plan_thumb_path'];
	$zone_access_path=$row['zone_access_path'];
	$zone_access_thumb_path=$row['zone_access_thumb_path'];
	$zone_access_disclaimer=$row['zone_access_disclaimer'];
	$announcement_bottom=$row['announcement_bottom'];
	$announcement_bottom_chinese=$row['announcement_bottom_chinese'];

}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript">

tinyMCE.init({

        // General options
		
		
        mode: "exact",

    	elements: "description,description_chinese,zone_access_disclaimer,zone_access_disclaimer_chinese,announcement,announcement_chinese",

        theme : "advanced",

        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",


        // Theme options

        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect",

       	theme_advanced_buttons2 : "fontsizeselectcut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview",

       theme_advanced_buttons3 : "forecolor,backcolor,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",

       theme_advanced_buttons4 : "print,|,ltr,rtl,|,fullscreen,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",

        theme_advanced_toolbar_location : "top",

        theme_advanced_toolbar_align : "left",
		
		

        theme_advanced_statusbar_location : "bottom",

        theme_advanced_resizing : true,

		theme_advanced_blockformats : "p,div,address,pre,h1,h2,h3,h4,h5,h6",

		forced_root_block : '',

        // Skin options

        skin : "o2k7",

        skin_variant : "silver",

		
        // Example content CSS (should be your site CSS)

        content_css : "../css/page.css,../css/txtcolor.css",



        // Drop lists for link/image/media/template dialogs

        template_external_list_url : "js/template_list.js",

        external_link_list_url : "js/link_list.js",

        external_image_list_url : "js/image_list.js",

        media_external_list_url : "js/media_list.js",

		convert_urls : false,
		
		forced_root_block : 'p',
		
		

        // Replace values for the template plugin

        template_replace_values : {

                username : "Some User",

                staffid : "991234"

        }
		

});

</script>
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Add New Ticketing Info </div>
		<div id="back_to_list"><a href="ticketing-info-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
			<div>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Ticket Year  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="ticketing_year">
						<option value="2012" <?php if($ticketing_year=="2012") { echo "selected"; } ?>>2012</option>
						<option value="2013" <?php if($ticketing_year=="2013") { echo "selected"; } ?>>2013</option>
						<option value="2014" <?php if($ticketing_year=="2014") { echo "selected"; } ?>>2014</option>
					</select>
					
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Ticket Category  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="ticketing_category_id">
						<option value="">Please Select</option>
						<?php 
							$q="SELECT * FROM ticketing_category ORDER BY sort";
							$result=mysql_query($q,$link) or die(mysql_error());
							while($row=mysql_fetch_array($result)) {
								echo "<option value='".$row['id']."'";
								if($ticketing_category_id==$row['id']) { echo "selected"; }
								echo ">".$row['ticketing_category']."</option>";
							}
						?>
					</select>
					
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Ticket SubCategory : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="ticket_subcategory"  value="<?php echo $ticket_subcategory; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Ticket SubCategory<br> 
				    Chinese: *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="ticket_subcategory_chinese"  value="<?php echo $ticket_subcategory_chinese; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
<?php
/*
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Ticket SubCategory Description  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="ticket_subcategory_description" style="width:500px;height:100px"><?php echo $ticket_subcategory_description; ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Ticket SubCategory Description Chinese: </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="ticket_subcategory_description_chinese" style="width:500px;height:100px"><?php echo $ticket_subcategory_description_chinese; ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Access to Zones: </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="checkbox" name="access_to_zones_array[]" value="1" <?php if($access_to_zones_array[0]=="1" || $access_to_zones_array[1]=="1" || $access_to_zones_array[2]=="1" || $access_to_zones_array[3]=="1") { echo "checked"; } ?>> Zone 1
					<input type="checkbox" name="access_to_zones_array[]" value="2" <?php if($access_to_zones_array[0]=="2" || $access_to_zones_array[1]=="2" || $access_to_zones_array[2]=="2" || $access_to_zones_array[3]=="2") { echo "checked"; } ?>> Zone 2
					<input type="checkbox" name="access_to_zones_array[]" value="3" <?php if($access_to_zones_array[0]=="3" || $access_to_zones_array[1]=="3" || $access_to_zones_array[2]=="3" || $access_to_zones_array[3]=="3") { echo "checked"; } ?>> Zone 3
					<input type="checkbox" name="access_to_zones_array[]" value="4" <?php if($access_to_zones_array[0]=="4" || $access_to_zones_array[1]=="4" || $access_to_zones_array[2]=="4" || $access_to_zones_array[3]=="4") { echo "checked"; } ?>> Zone 4
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
*/
?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Accessible Entertainment Stages: </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="checkbox" name="accessible_entertainment_stages_array[]" value="1" <?php if($accessible_entertainment_stages_array[0]=="1" || $accessible_entertainment_stages_array[1]=="1" || $accessible_entertainment_stages_array[2]=="1" || $accessible_entertainment_stages_array[3]=="1") { echo "checked"; } ?>> Zone 1
					<input type="checkbox" name="accessible_entertainment_stages_array[]" value="2" <?php if($accessible_entertainment_stages_array[0]=="2" || $accessible_entertainment_stages_array[1]=="2" || $accessible_entertainment_stages_array[2]=="2" || $accessible_entertainment_stages_array[3]=="2") { echo "checked"; } ?>> Zone 2
					<input type="checkbox" name="accessible_entertainment_stages_array[]" value="3" <?php if($accessible_entertainment_stages_array[0]=="3" || $accessible_entertainment_stages_array[1]=="3" || $accessible_entertainment_stages_array[2]=="3" || $accessible_entertainment_stages_array[3]=="3") { echo "checked"; } ?>> Zone 3
					<input type="checkbox" name="accessible_entertainment_stages_array[]" value="4" <?php if($accessible_entertainment_stages_array[0]=="4" || $accessible_entertainment_stages_array[1]=="4" || $accessible_entertainment_stages_array[2]=="4" || $accessible_entertainment_stages_array[3]=="4") { echo "checked"; } ?>> Zone 4
					</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Accessible Entertainment Stages Additional info: </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
						<input type="text" name="accessible_info"  value="<?php echo htmlspecialchars($accessible_info); ?>" style="width:500px">
					</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Accessible Entertainment Stages Additional info Chinese: </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
						<input type="text" name="accessible_info_cn"  value="<?php echo htmlspecialchars($accessible_info_cn); ?>" style="width:500px">
					</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Availability Text: </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					
					<textarea name="announcement" style="width:500px;height:150px;font-size:11px"><?php echo stripslashes($announcement); ?></textarea>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Availability Text Chinese: </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					
					<textarea name="announcement_chinese" style="width:500px;height:150px;font-size:11px"><?php echo stripslashes($announcement_chinese); ?></textarea>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
<?php
/*
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Announcement Bottom: </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="announcement_bottom"  value="<?php echo stripslashes($announcement_bottom); ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Announcement Bottom Chinese: </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="announcement_bottom_chinese"  value="<?php echo stripslashes($announcement_bottom_chinese); ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
*/
?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Description  : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="description" style="width:500px;height:350px;font-size:11px"><?php echo stripslashes($description); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Description Chinese : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="description_chinese" style="width:500px;height:350px;font-size:11px"><?php echo stripslashes($description_chinese); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
<?php
/*
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Zone Access Disclaimer : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="zone_access_disclaimer" style="width:500px;height:350px;font-size:11px"><?php echo stripslashes($zone_access_disclaimer); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Zone Access Disclaimer Chinese: </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="zone_access_disclaimer_chinese" style="width:500px;height:350px;font-size:11px"><?php echo stripslashes($zone_access_disclaimer_chinese); ?></textarea></td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
*/
?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Panoramic View : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="panoramic_view_path"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
<?php
/*
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Related Photo : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="related_photo_path"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Seating Plan : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="seating_plan_path"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Seating Plan Thumb:</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="seating_plan_thumb_path"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
*/
?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Zone Access/Seating Plan:</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="zone_access_path"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
<?php
/*
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Zone Access Thumb/Seating Plan Thumb: </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="zone_access_thumb_path"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
*/
?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Status : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="status">
						<option value="Active" <?php if($status=="Active") { echo "selected"; } ?>>Active</option>
						<option value="Inactive" <?php if($status=="Inactive") { echo "selected"; } ?>>Inactive</option>
					</select>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>View Type : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="view_type">
						<option value="Panoramic View" <?php if($view_type=="Panoramic View") { echo "selected"; } ?>>Panoramic View</option>
						<option value="Interior View" <?php if($view_type=="Interior View") { echo "selected"; } ?>>Interior View</option>
						<option value="Photo" <?php if($view_type=="Photo") { echo "selected"; } ?>>Photo</option>
					</select>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>View Type Name: </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="view_type_name"  value="<?php echo stripslashes($view_type_name); ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>View Type Name Chinese: </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="view_type_name_chinese"  value="<?php echo stripslashes($view_type_name_chinese); ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Add New Ticketing Info"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				
			  </div>
		  </form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

