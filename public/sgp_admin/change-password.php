<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");

if(isset($_POST['submit'])) {
	$error=0;

	
	$old_password=$_POST['old_password'];
	$new_password=$_POST['new_password'];
	$confirm_new_password=$_POST['confirm_new_password'];
	
	if($old_password=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Old Password.";
	}
	if($new_password=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert New Password.";
	}
	if($confirm_new_password=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Confirm New Password.";
	}
	if($new_password!="" && $confirm_new_password!="") {
		
		if($new_password!=$confirm_new_password) {
			$error=1;
			$errorword.="<br>";
			$errorword.="New Password and Confirm New Password are different.";
		}	
	}
	
	if($error!=1) {
			$password_encrypt=md5($old_password);
			$q="SELECT username FROM admin_user WHERE username='$_SESSION[cms_username]' AND password='$password_encrypt'";
			$result=mysql_query($q,$link) or die(mysql_error());
			$count=mysql_num_rows($result);
			
			if($count==0) {
				$error=1;
				$errorword.="<br>";
				$errorword.="Old Password is not correct, please try again.";
			}
			else {
				$password_encrypt=md5($new_password);
				
				$q="UPDATE admin_user SET password='$password_encrypt' WHERE username='$_SESSION[cms_username]'";
				$result=mysql_query($q,$link) or die(mysql_error());

				
				
				$successword="Your password change successfully.";
				
			}

			

			
			
		
	}

}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />


<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Change Password </div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post">
			<div>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				<?php if($successword!="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;"></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><?php echo $successword; ?></td>
					<td width="3"  style="border-bottom:1px dotted #BAB9B9;"></td>
				</tr>
				<?php } ?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Old Password: *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="password" name="old_password" style="width:500px "></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>New Password: *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="password" name="new_password" style="width:500px "></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Confirm New Password: *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="password" name="confirm_new_password" style="width:500px "></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Update Password"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				</div>
				</form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

