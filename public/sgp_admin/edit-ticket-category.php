<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");

$id=$_REQUEST['id'];

if(isset($_POST['submit'])) {
	$error=0;
	
	$ticketing_category=$_POST['ticketing_category'];
	$ticketing_category_chinese=$_POST['ticketing_category_chinese'];
	$ticketing_type=$_POST['ticketing_type'];
	$status=$_POST['status'];
	$special_detail_name=$_POST['special_detail_name'];
	$special_detail_name_chinese=$_POST['special_detail_name_chinese'];
	$special_detail_date=$_POST['special_detail_date'];
	$special_detail_date_chinese=$_POST['special_detail_date_chinese'];
	
	
	$special_regular_detail_name=$_POST['special_regular_detail_name'];
	$special_regular_detail_name_chinese=$_POST['special_regular_detail_name_chinese'];
	$special_regular_detail_date=$_POST['special_regular_detail_date'];
	$special_regular_detail_date_chinese=$_POST['special_regular_detail_date_chinese'];
	
	
	
	
	$more_detail_name=$_POST['more_detail_name'];
	$more_detail_name_chinese=$_POST['more_detail_name_chinese'];

	if($ticketing_type=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Ticket Type.";
	}
	if($ticketing_category=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Ticket Category.";
	}
	if($ticketing_category_chinese=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Ticket Category Chinese.";
	}
	
	
	
	if($error!=1) {

			$datetime=date("Y-m-d H:i:s");
			
			$q="UPDATE ticketing_category SET ticketing_type='".mysql_real_escape_string($ticketing_type)."',ticketing_category='".mysql_real_escape_string($ticketing_category)."',ticketing_category_chinese='".mysql_real_escape_string($ticketing_category_chinese)."',special_detail_name='".mysql_real_escape_string($special_detail_name)."',special_detail_name_chinese='".mysql_real_escape_string($special_detail_name_chinese)."',special_detail_date='".mysql_real_escape_string($special_detail_date)."',special_detail_date_chinese='".mysql_real_escape_string($special_detail_date_chinese)."',special_regular_detail_name='".mysql_real_escape_string($special_regular_detail_name)."',special_regular_detail_name_chinese='".mysql_real_escape_string($special_regular_detail_name_chinese)."',special_regular_detail_date='".mysql_real_escape_string($special_regular_detail_date)."',special_regular_detail_date_chinese='".mysql_real_escape_string($special_regular_detail_date_chinese)."',updater='".mysql_real_escape_string($_SESSION[cms_username])."',status='".mysql_real_escape_string($status)."',more_detail_name='".mysql_real_escape_string($more_detail_name)."',more_detail_name_chinese='".mysql_real_escape_string($more_detail_name_chinese)."' WHERE id='".mysql_real_escape_string($id)."'";
			$result=mysql_query($q,$link) or die(mysql_error());

			
			
			header('Location:ticket-category-listing.php?page='.$_REQUEST[page].'&orderby='.$_REQUEST[orderby].'&search_value='.$_REQUEST[search_value].'');
			exit();
			
		
	}

} else {
	$q="SELECT * FROM ticketing_category WHERE id='".mysql_real_escape_string($_REQUEST[id])."'";
	$result=mysql_query($q,$link) or die(mysql_error());
	$row=mysql_fetch_array($result);
	
	$ticketing_category=$row['ticketing_category'];
	$ticketing_category_chinese=$row['ticketing_category_chinese'];
	$special_detail_name=$row['special_detail_name'];
	$special_detail_name_chinese=$row['special_detail_name_chinese'];
	$special_detail_date=$row['special_detail_date'];
	$special_detail_date_chinese=$row['special_detail_date_chinese'];
	
	
	
	$special_regular_detail_name=$row['special_regular_detail_name'];
	$special_regular_detail_name_chinese=$row['special_regular_detail_name_chinese'];
	$special_regular_detail_date=$row['special_regular_detail_date'];
	$special_regular_detail_date_chinese=$row['special_regular_detail_date_chinese'];
	
	
	
	$ticketing_type=$row['ticketing_type'];
	$status=$row['status'];
	$more_detail_name=$row['more_detail_name'];
	$more_detail_name_chinese=$row['more_detail_name_chinese'];

}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Edit Ticket Category </div>
		<div id="back_to_list"><a href="ticket-category-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post">
			<div>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Ticket Type  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="ticketing_type">
						<option value="">Please Select</option>
						<option value="3-Day Ticket" <?php if($ticketing_type=="3-Day Ticket") { echo "selected"; } ?>>3-Day Ticket</option>
						<option value="3-Day Combination Ticket" <?php if($ticketing_type=="3-Day Combination Ticket") { echo "selected"; } ?>>3-Day Combination Ticket</option>
						<option value="Single Day Ticket" <?php if($ticketing_type=="Single Day Ticket") { echo "selected"; } ?>>Single Day Ticket</option>
					</select>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Ticket Category  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="ticketing_category"  value="<?php echo $ticketing_category; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Ticket Category Chinese  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="ticketing_category_chinese"  value="<?php echo $ticketing_category_chinese; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
                
                
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Special Price Name  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="special_detail_name"  value="<?php echo $special_detail_name; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Special Price Name Chinese  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="special_detail_name_chinese"  value="<?php echo $special_detail_name_chinese; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>

				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Special Price Date  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="special_detail_date"  value="<?php echo $special_detail_date; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Special Price Date Chinese  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="special_detail_date_chinese"  value="<?php echo $special_detail_date_chinese; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				
				
				
				
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Special Price Name (Regular) : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="special_regular_detail_name"  value="<?php echo $special_regular_detail_name; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Special Price Name Chinese (Regular) : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="special_regular_detail_name_chinese"  value="<?php echo $special_regular_detail_name_chinese; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>

				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Special Price Date (Regular) : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="special_regular_detail_date"  value="<?php echo $special_regular_detail_date; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Special Price Date Chinese (Regular)  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="special_regular_detail_date_chinese"  value="<?php echo $special_regular_detail_date_chinese; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				
				
				
				
				
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>More Detail Name  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="more_detail_name"  value="<?php echo $more_detail_name; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>More Detail Name Chinese  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="more_detail_name_chinese"  value="<?php echo $more_detail_name_chinese; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>

				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Status  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="status">
						<option value="Active" <?php if($status=="Active") { echo "selected"; } ?>>Active</option>
						<option value="Inactive" <?php if($status=="Inactive") { echo "selected"; } ?>>Inactive</option>
					</select>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				
				
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Update Ticket Category"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				
			  </div>
		  </form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

