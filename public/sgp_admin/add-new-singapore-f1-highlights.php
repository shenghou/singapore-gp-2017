<?php 
require_once '../function/helper.php';
require_once '../function/cms-helper.php';
require_once 'include/configure.php';

check_session_exists();
$status_opt = array(1=>'Active', 0=>'Inactive');

if(isset($_POST['submit'])) {
	$error=0;
	
	$image_link=$_POST['image_link'];
	$title=$_POST['title'];
	$type=$_POST['type'];
	$status=$_POST['status'];
	$webapp_url=$_POST['webapp_url'];
	$display_website=$_POST['display_website'];
	$webview_url=$_POST['webview_url'];

	if($title=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Title.";
	} 
	if($image_link=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Singapore F1 Highlights Image Link.";
	}
	if($_FILES['image_path']['name']=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please upload Singapore F1 Highlights Image.";
	
	}
	if($_FILES['banner_ipad_landscape']['name']=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please upload Singapore F1 Highlights Banner Ipad (Landscape).";
	
	}
	if($_FILES['banner_ipad_portrait']['name']=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please upload Singapore F1 Highlights Banner Ipad (Portrait).";
	
	}

	if($status=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Status.";
	}
	if($display_website=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please select Show On Website.";
	}

	if($error!=1) {
	
		$target_paths="../Uploaded/singapore_f1_highlights/";
		$target_path_database="Uploaded/singapore_f1_highlights/";
		if(!is_dir($target_paths)) {
			if(mkdir($target_paths,777))
			{
				chmod($target_paths,0777); 
			}
		}

		$target_path=$target_paths.basename( $_FILES['image_path']['name']); 
		move_uploaded_file($_FILES['image_path']['tmp_name'], $target_path);
		
		if ($_SERVER["SERVER_ADDR"] == "173.230.156.228") {
			$image_path="http://173.230.156.228/sgpstaging/".$target_path_database.basename( $_FILES['image_path']['name']);
		} else {
			$image_path="http://web2.singaporegp.sg/".$target_path_database.basename( $_FILES['image_path']['name']);
		}

		$target_path=$target_paths.basename( $_FILES['banner_ipad_landscape']['name']); 
		move_uploaded_file($_FILES['banner_ipad_landscape']['tmp_name'], $target_path);

		if ($_SERVER["SERVER_ADDR"] == "173.230.156.228") {
			$image_banner_ipad_landscape="http://173.230.156.228/sgpstaging/".$target_path_database.basename( $_FILES['banner_ipad_landscape']['name']);
		} else {
			$image_banner_ipad_landscape="http://web2.singaporegp.sg/".$target_path_database.basename( $_FILES['banner_ipad_landscape']['name']);
		}

		$target_path=$target_paths.basename( $_FILES['banner_ipad_portrait']['name']); 
		move_uploaded_file($_FILES['banner_ipad_portrait']['tmp_name'], $target_path);

		if ($_SERVER["SERVER_ADDR"] == "173.230.156.228") {
			$image_banner_ipad_portrait="http://173.230.156.228/sgpstaging/".$target_path_database.basename( $_FILES['banner_ipad_portrait']['name']);
		} else {
			$image_banner_ipad_portrait="http://web2.singaporegp.sg/".$target_path_database.basename( $_FILES['banner_ipad_portrait']['name']);
		}

		$datetime=date("Y-m-d H:i:s");
		
			
			$q="SELECT sort FROM singaporef1_highlights ORDER BY sort DESC";
			$result=mysql_query($q,$link) or die(mysql_error());
			$row=mysql_fetch_array($result);
			
			$sort=$row['sort'];
			$sort=$sort+1;
			
			
			
			$q="INSERT INTO singaporef1_highlights (image_path,image_link,title,updater,banner_ipad_landscape,banner_ipad_portrait,type,webapp_url,status,sort,display_website,webview_url) VALUES ('".mysql_real_escape_string($image_path)."','".mysql_real_escape_string($image_link)."','".mysql_real_escape_string($title)."','".mysql_real_escape_string($_SESSION[cms_username])."','".mysql_real_escape_string($image_banner_ipad_landscape)."','".mysql_real_escape_string($image_banner_ipad_portrait)."','".mysql_real_escape_string($type)."','".mysql_real_escape_string($webapp_url)."','".mysql_real_escape_string($status)."','".mysql_real_escape_string($sort)."','".mysql_real_escape_string($display_website)."','".mysql_real_escape_string($webview_url)."')";
			mysql_query($q,$link) or die(mysql_error());

			if (isset($link1) && is_resource($link1)) {
				mysql_query($q,$link1) or die(mysql_error());
			}

			if (isset($link2) && is_resource($link2)) {
				mysql_query($q,$link2) or die(mysql_error());
			}

			header('Location:singapore-f1-highlights-listing.php');
			exit();
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Add New Singapore F1 Highlights</div>
		<div id="back_to_list"><a href="singapore-f1-highlights-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form method="post" enctype="multipart/form-data">
			<div>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Title   : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					  <input type="text" name="title"  value="<?php echo $title; ?>" style="width:500px" id="username">
					 
				  </td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Singapore F1 Highlights Image : *<br>
				    (607 x 280)					</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="image_path" value=""></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Singapore F1 Highlights Banner Ipad (Landscape) : *<br>
				    (1024 x 472)					</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="banner_ipad_landscape" value=""></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Singapore F1 Highlights Banner Ipad (Portrait) : *<br>
				    (768 x 354)					</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="banner_ipad_portrait" value=""></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Singapore F1 Highlights Image Link  : *</strong></td>
				  <td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="image_link"  value="<?php echo $image_link; ?>" style="width:500px" id="username">
				    <br>
				    eg. http://www.singaporegp.sg/autograph.php </td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Show on Website : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
						<?php echo set_select('display_website', $status_opt);?>
					</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Show On Mobile Website  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
						<?php echo set_select('status', $status_opt);?>
					</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Mobile app url or Object ID: </strong></td>
				  <td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="webview_url"  value="<?php echo $webview_url; ?>" style="width:500px" id="webview_url">
				    <br>
				    eg. http://www.singaporegp.sg/contest/mobile-be-spotted.php<br>
				    eg. 2013-09-20
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Add New Singapore F1 Highlights"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				
				</div>
				</form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

