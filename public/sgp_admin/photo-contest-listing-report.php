<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
	exit();
}
include("include/configure.php");

if(isset($_POST['search'])) {
	$StartDate=$_POST['StartDate'];
	$EndDate=$_POST['EndDate'];
	
	if($StartDate!="" && $EndDate!="") {
		$StartDate_array=explode("/",$StartDate);
		$EndDate_array=explode("/",$EndDate);
		$StartDate_final="$StartDate_array[2]-$StartDate_array[1]-$StartDate_array[0] 00:00:00";
		$EndDate_final="$EndDate_array[2]-$EndDate_array[1]-$EndDate_array[0] 23:59:59";
		$q_reg_date="WHERE datetime BETWEEN '".mysql_real_escape_string($StartDate_final)."' AND '".mysql_real_escape_string($EndDate_final)."'";
	}
	
	
	
	$data="";
	$data.="No\t";
	$data.="Full Name\t";
	$data.="Ticket Transaction Number\t";
	$data.="Email Address\t";
	$data.="Contact Number\t";
	$data.="Title Photograph\t";
	$data.="Caption Photograph\t";
	$data.="Photograph Location\t";
	$data.="Photograph Path\t";
	$data.="From Where\t";
	$data.="Date Time\t";
	
	$data.="\n";
	
	$q="SELECT * FROM photo_contest_2013 ORDER BY id";
	$result=mysql_query($q,$link) or die(mysql_error());
	while($row=mysql_fetch_array($result)) {
	
		$num=$num+1;
		$full_name= stripslashes(str_replace("\r\n",'',$row['full_name']));
		$ticket_transaction_number=stripslashes($row['ticket_transaction_number']);
		$email_address= stripslashes(str_replace("\r\n",'',$row['email_address']));
		$contact_number= stripslashes(str_replace("\r\n",'',$row['contact_number']));
		$title_photograph= stripslashes(str_replace("\r\n",'',$row['title_photograph']));
		$caption_photograph= stripslashes(str_replace("\r\n",'',$row['caption_photograph']));
		$photograph_location= stripslashes(str_replace("\r\n",'',$row['photograph_location']));
		$photograph_path= stripslashes(str_replace("\r\n",'',$row['photograph_path']));
		$from_where= stripslashes(str_replace("\r\n",'',$row['from_where']));
		$datetime= stripslashes(str_replace("\r\n",'',$row['datetime']));
		
		
		
	
		$data.="$num\t";
		$data.="$full_name\t";
		$data.="$ticket_transaction_number\t";
		$data.="$email_address\t";
		$data.="($contact_number)\t";
		$data.="$title_photograph\t";
		$data.="$caption_photograph\t";
		$data.="$photograph_location\t";
		$data.="$photograph_path\t";
		$data.="$from_where\t";
		$data.="$datetime\t";
		
		$data.="\n";
	}
	

	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0
	header ('Content-Type: application/vnd.ms-excel');
	header ('Content-Disposition: attachment; filename="Photo-Contest-Report.xls"');
	header ('Content-Transfer-Encoding: binary');
	
	
	
	echo "$data";
	

	exit();
	
}



?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - View Photo Contest Report</title>
<link rel="stylesheet" type="text/css" media="screen" href="css/datePicker.css">
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/date.js"></script>
<script type="text/javascript" src="js/jquery.datePicker.js"></script>
<script type="text/javascript">

$(function()
{
	$('.date-pick').datePicker(
	{
		startDate: '04/08/1000',
		endDate: '04/08/3010'
	}
	);
});
</script>
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<body style="margin:0 auto; background:none">

	<div style="margin:0 auto;width:640px">
		<table border="0" cellpadding="4" cellspacing="0"  width="640px" align="center">
		<tr>
			<td style="font-size:13px; font-weight:bold ;font-family:Verdana, Arial, Helvetica, sans-serif" colspan="4" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;View Photo Contest Report
			</td>
		</tr>
		
		<tr>
			<td style="font-size:11px;font-family:Verdana, Arial, Helvetica, sans-serif" colspan="4">
			<br>
			<form action="photo-contest-listing-report.php" method="post">
			<table width="723" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="35">From</td>
			  <td width="260"><input type="text" name="StartDate" value="<?php echo $StartDate; ?>" class="date-pick" readonly="readonly" size="10"> (dd/mm/yyyy)</td>
				<td width="26">To</td>
			  <td width="260"><input type="text" name="EndDate" value="<?php echo $EndDate; ?>" class="date-pick" readonly="readonly" size="10"> (dd/mm/yyyy)</td>
				<td width="142"><input type="submit" name="search" value="Search"></td>
			</tr>
			</table>
			
			
			
			</form>
			</td>
		</tr>
		</table>
	</div>
		
		
		
	<div class="clearboth"></div>

</body>
</html>

