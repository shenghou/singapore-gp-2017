<?php 
session_start();

if($_SESSION['cms_username']=="" || $_SESSION['cms_role']!="2") {
	header('Location:login.php');
	exit();
}

$q_search = (isset($_GET['q_search'])?$_GET['q_search']:"");
$orderby = (isset($_GET['orderby'])?$_GET['orderby']:"username");
$search_value = (isset($_GET['search_value'])?$_GET['search_value']:"");

if(isset($_REQUEST['search_value'])) {
	$search_value=$_REQUEST['search_value'];
	if($search_value!="") {
		$q_search="AND (username LIKE '%$search_value%' OR name LIKE '%$search_value%')";

	}
}


include("include/configure.php");


if(empty($_REQUEST['page']))
{
	$page=1;
}
else
{
	if($_REQUEST['page']=="")
	{
		$page=1;
	}
	else
	{
		$page=$_REQUEST['page'];
	}
}
			
$site = $_SERVER['PHP_SELF'];
$range=25;//the range of pages display
$limit=20;//the limit of rows display in a page

$sqlcount="SELECT id FROM admin_user WHERE id!='' $q_search ORDER BY username DESC";
$resultcount=mysqli_query($link, $sqlcount)or die(mysqli_error($link));
$count=mysqli_num_rows($resultcount);
$totalpage=ceil($count/$limit);
					
$limitvalue=($page*$limit)-$limit;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/ajax.js"></script>
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">User Listing </div>
		
		<div class="searching_field">
		<form action="<?php $PHP_SELF; ?>" method="get">
		<table width="248" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="46">Search</td>
			<td width="133"><input type="text" name="search_value" value="<?php echo stripslashes($search_value); ?>"></td>
			<td width="69"><input type="submit" name="search" value="search"></td>
		</tr>
		</table>
		</form>
		</div>
		<div class="clearboth"></div>
		<div class="functionlink"><a href="add-new-user.php">Add New User</a></div>
		<div id="content_list">
			<table width="760" align="left" cellpadding="5" cellspacing="0" style="border-top:1px solid #B6BEBE;">
			<tr class="blue_column">
				<td width="32" class="functionlink bottomline leftline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=id DESC&search_value=$search_value"; ?>">No</a></strong></td>
				<td width="172" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=username&search_value=$search_value"; ?>">Username</a></strong></td>
				<td width="325" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=name&search_value=$search_value"; ?>">Name</a></strong></td>
				<td width="103" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=role&search_value=$search_value"; ?>">Role</a></strong></td>
				<td width="27" class="bottomline" align="center"><strong>Edit</strong></td>
				<td width="39" class="bottomline" align="center"><strong>Delete</strong></td>
			</tr>
			<?php
			$num=0;
			   if($page!=1 && $page!="") {
						$pagecount=$page-1;
						$num=$pagecount * $limit;
			   	}
				$q="SELECT * FROM admin_user WHERE id!='' $q_search ORDER BY $orderby LIMIT $limitvalue,$limit";
			   $result=mysqli_query($link, $q) or die(mysqli_error($link));
			   while($row=mysqli_fetch_array($result)) {
			  	 	$num=$num+1;
					
					
					
			   		?>
					<tr>
						<td width="32" class="bottomline leftline"><?php echo $num; ?></td>
						<td width="172" class="bottomline"><?php echo $row['username']; ?></td>
						<td width="325" class="bottomline"><?php echo $row['name']; ?></td>
						<td width="103" class="bottomline">
						<?php
						if($row['role']==2)
							echo "Administrator";
						else 
							echo "User"; 
						?>
						</td>
						<td width="27" class="functionlink bottomline" align="center">
							<a href="edit-user.php?id=<?php echo $row['id']; ?>&page=<?php echo $page; ?>&orderby=<?php echo $orderby; ?>&search_value=<?php echo $search_value; ?>"><img src="images/edit.jpg" border="none"></a>
						
						</td>
						<td width="39" class="functionlink bottomline" align="center">
						
							<a href="javascript:;" onClick="DeleteUser('<?php echo $row['id']; ?>')"><img src="images/delete.gif" border="none"></a>
						
						</td>
					</tr>
					<?php
			   }
			?>
			</table>
			
			
			 <div class="clearboth"></div>
				<table border="0" cellspacing="0" cellpadding="0" class="page_table" align="left">
				<tr>
				<td align="left" class="style5"><?php
				$lrange = max(1,$page-(($range-1)/2));
				$rrange = min($totalpage,$page+(($range-1)/2));
				if (($rrange - $lrange) < ($range - 1))
					{
						if ($lrange == 1)
							{
								$rrange = min($lrange + ($range-1), $totalpage);
							}
						else
							{
								$lrange = max($rrange - ($range-1), 0);
							}
					}
				if($totalpage>1)
				{
					if ($page > 1)//previous page
						{
							$prev=$page-1;
							echo " <a href='".$PHP_SELF."?page=1&orderby=$orderby&search_value=$search_value'><< First</a> | "; 
							echo " <a href='".$PHP_SELF."?page=$prev&orderby=$orderby&search_value=$search_value'>< Prev</a> | ";
						}
					else
						{
							echo " <strong><< First</strong> | <strong>< Prev</strong> |";
						}
					for($i = 1; $i <= $totalpage; $i++)//the range of page
						{
							if ($i == $page)
								{
									echo " <span class='actual_page'><strong>$i</strong></span>";
								}
							else
								{
									if ($lrange <= $i and $i <= $rrange)
										{
											echo " <a href='".$PHP_SELF."?page=$i&orderby=$orderby&search_value=$search_value' class='bluecolor word fontsubtitle'>$i</a> ";
										}
								}
						}
					if ($page < $totalpage)//next page
						{
							$next=$page+1;
							echo "| <a href='".$PHP_SELF."?page=$next&orderby=$orderby&search_value=$search_value'>Next ></a> |";
							echo " <a href='".$PHP_SELF."?page=$totalpage&orderby=$orderby&search_value=$search_value'>Last >></a>";
							
						}
					else
						{
							echo " | <strong>Next ></strong> | <strong>Last >></strong>";
						}
				}//end of if $totalpage > 1
			?></td>
			  </tr>
			</table>
				
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

