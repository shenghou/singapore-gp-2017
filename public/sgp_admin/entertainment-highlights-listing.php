<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
	exit();
}

if(isset($_REQUEST['search_value'])) {
	$search_value=$_REQUEST['search_value'];
	if($search_value!="") {
		$q_search="AND (artist LIKE '%$search_value%' OR short_description LIKE '%$search_value%' OR year='$search_value')";

	}
}


include("include/configure.php");



if(isset($_REQUEST['savesequence'])) {
	
	$sort_update=$_REQUEST['sort'];
	$sortid_update=$_REQUEST['sortid'];
	$status_e_update=$_REQUEST['status_e'];
	$count_update=count($sortid_update);

	for($i=0;$i<$count_update;$i++) {
		$q_update="UPDATE entertainment_highlights SET sort='".mysql_real_escape_string($sort_update[$i])."' WHERE id='".mysql_real_escape_string($sortid_update[$i])."'";
		mysql_query($q_update,$link) or die(mysql_error());

		if (isset($link1) && is_resource($link1)) {
			mysql_query($q_update, $link1) or die(mysql_error());
		}

		if (isset($link2) && is_resource($link2)) {
			mysql_query($q_update, $link2) or die(mysql_error());
		}

		$q_update="UPDATE entertainment_highlights SET status='".mysql_real_escape_string($status_e_update[$i])."' WHERE id='".mysql_real_escape_string($sortid_update[$i])."'";
		mysql_query($q_update,$link) or die(mysql_error());

		if (isset($link1) && is_resource($link1)) {
			mysql_query($q_update, $link1) or die(mysql_error());
		}

		if (isset($link2) && is_resource($link2)) {
			mysql_query($q_update, $link2) or die(mysql_error());
		}
	}
	
	
}


if(isset($_POST['update_status'])) {
	$module_status=$_POST['status'];
	$module_writeup=$_POST['write_up'];
	
	$q_m_status="UPDATE status_module SET status='".mysql_real_escape_string($module_status)."',write_up='".mysql_real_escape_string($module_writeup)."' WHERE  status_module_name='Entertainment'";
	mysql_query($q_m_status,$link) or die(mysql_error());

	if (isset($link1) && is_resource($link1)) {
		mysql_query($q_m_status, $link1) or die(mysql_error());
	}

	if (isset($link2) && is_resource($link2)) {
		mysql_query($q_m_status, $link2) or die(mysql_error());
	}
}



if(empty($_REQUEST['page']))
{
	$page=1;
}
else
{
	if($_REQUEST['page']=="")
	{
		$page=1;
	}
	else
	{
		$page=$_REQUEST['page'];
	}
}
			
$site = $_SERVER['PHP_SELF'];
$range=25;//the range of pages display
$limit=20;//the limit of rows display in a page

$sqlcount="SELECT id FROM entertainment_highlights WHERE id!='' $q_search ORDER BY id";
$resultcount=mysql_query($sqlcount,$link)or die(mysql_error());
$count=mysql_num_rows($resultcount);
$totalpage=ceil($count/$limit);
					
$limitvalue=($page*$limit)-$limit;

# order by sort instead
if (empty($_GET['orderby'])) {
	$orderby = <<<sql
if(status="Active", 0, 1),
if(performance_category like "%Performances on Stage%", 1,
if (performance_category like "%Roving Performances%", 2, 3)
), sort, year
sql;
} else {
	$orderby = $_GET['orderby'];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/ajax.js"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript">

tinyMCE.init({

        // General options
		
		
        mode: "exact",

    	elements: "write_up,write_up_chinese",

        theme : "advanced",

        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

file_browser_callback : 'tinyBrowser',

        // Theme options

        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect",

       	theme_advanced_buttons2 : "fontsizeselectcut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview",

       theme_advanced_buttons3 : "forecolor,backcolor,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",

       theme_advanced_buttons4 : "print,|,ltr,rtl,|,fullscreen,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",

        theme_advanced_toolbar_location : "top",

        theme_advanced_toolbar_align : "left",
		
		

        theme_advanced_statusbar_location : "bottom",

        theme_advanced_resizing : true,

		theme_advanced_blockformats : "p,div,address,pre,h1,h2,h3,h4,h5,h6",

		forced_root_block : '',

        // Skin options

        skin : "o2k7",

        skin_variant : "silver",

		
        // Example content CSS (should be your site CSS)

        content_css : "../css/page.css,../css/txtcolor.css",



        // Drop lists for link/image/media/template dialogs

        template_external_list_url : "js/template_list.js",

        external_link_list_url : "js/link_list.js",

        external_image_list_url : "js/image_list.js",

        media_external_list_url : "js/media_list.js",

		convert_urls : false,
		
		forced_root_block : 'p',
		
		

        // Replace values for the template plugin

        template_replace_values : {

                username : "Some User",

                staffid : "991234"

        }
		

});

</script>
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Entertainment Highlights  Listing</div>
		
		<div class="searching_field">
		<form action="<?php $PHP_SELF; ?>" method="get">
		<table width="248" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="46">Search</td>
			<td width="133"><input type="text" name="search_value" value="<?php echo stripslashes($search_value); ?>"></td>
			<td width="69"><input type="submit" name="search" value="search"></td>
		</tr>
		</table>
		</form>
		</div>
		<div class="clearboth"></div>
		
		<?php if($_SESSION['cms_role']==2) { ?>
		
		
		
		<form action="<?php $PHP_SELF; ?>" method="post">
		<?php
		$q_m_status="SELECT status,write_up,write_up_chinese FROM status_module WHERE  status_module_name='Entertainment'";
		$result_m_status=mysql_query($q_m_status,$link) or die(mysql_error());
		$row_m_status=mysql_fetch_array($result_m_status);
		?>
		<table width="773" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="109">Status:</td>
			<td width="641">
			<select name="status">
				<option value="Active" <?php if($row_m_status['status']=="Active") { echo "selected"; } ?>>Active</option>
				<option value="Inactive" <?php if($row_m_status['status']=="Inactive") { echo "selected"; } ?>>In Active</option>
			</select>
			</td>
			<td width="23"></td>
		</tr>
		<tr>
			<td width="109"></td>
			<td width="641">&nbsp;
			
			</td>
			<td width="23"></td>
		</tr>
		<tr>
			<td width="109">Write Up:</td>
			<td width="641">
			<textarea name="write_up"><?php echo $row_m_status['write_up']; ?></textarea>
			</td>
			<td width="23"></td>
		</tr>
		<tr>
			<td width="109"></td>
			<td width="641">&nbsp;
			
			</td>
			<td width="23"></td>
		</tr>
		
		<tr>
			<td width="109"></td>
			<td>
			<input type="submit" name="update_status" value="Update Status">
			</td>
			<td width="23"></td>
		</tr>
		</table>
		</form>
		
		<div class="clearboth"></div>
		<br>
		<br>
		
		
		
		<div class="functionlink"><a href="add-entertainment-highlights.php">Add New Entertainment Highlights</a></div>
		<div class="functionlink"><a href="../generate_pdf/schedule_pdf/schedule_pdf.php" target="_blank">Update Schedule PDF</a></div>
		<?php } ?>
		<div id="content_list">
			<form action="<?php $PHP_SELF; ?>" method="post">
			<table width="760" align="left" cellpadding="5" cellspacing="0" style="border-top:1px solid #B6BEBE;">
			<tr class="blue_column">
				<td width="24" class="functionlink bottomline leftline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=year DESC,sort&search_value=$search_value"; ?>">No</a></strong></td>
				<td width="75" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=year&search_value=$search_value"; ?>">Year</a></strong></td>
				<td width="272" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=artist&search_value=$search_value"; ?>">Artist</a></strong></td>
				<td width="131" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=image_thumb&search_value=$search_value"; ?>">Image Thumb</a></strong></td>
				<td width="76" class="functionlink bottomline" align="center"><strong>Performance Detail/ Photo Gallery </strong></td>
				<td width="75" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=status&search_value=$search_value"; ?>">Status</a></strong></td>

				<td width="53" class="functionlink bottomline"><strong>Display Order</strong></td>
				<?php if($_SESSION['cms_role']==2) { ?>
				<td width="21" class="bottomline" align="center"><strong>Edit</strong></td>
				<td width="40" class="bottomline" align="center"><strong>Delete</strong></td>
				<?php } ?>
			</tr>
			<?php 
			   if($page!=1 && $page!="") {
						$pagecount=$page-1;
						$num=$pagecount * $limit;
			   	}
				$q="SELECT * FROM  entertainment_highlights WHERE id!='' $q_search ORDER BY $orderby LIMIT $limitvalue,$limit";
			   $result=mysql_query($q,$link) or die(mysql_error());
			   while($row=mysql_fetch_array($result)) {
			  	 	$num=$num+1;
					

					
			   		?>
					<tr>
						<td width="24" class="bottomline leftline" valign="top"><?php echo $num; ?></td>
						<td width="75" class="bottomline" valign="top"><?php echo $row['year']; ?></td>
						<td width="272" class="bottomline" valign="top"><?php echo $row['artist']; ?></td>
						<td width="131" class="bottomline" valign="top"><img src="<?php echo $row['image_thumb']; ?>" width="130px" ></td>
						<td width="76" class="bottomline functionlink" valign="top" align="center"><a href="performance-detail-listing.php?entertainment_id=<?php echo $row['id']; ?>" target="_blank">View</a></td>
												<td width="75" class="bottomline" valign="top">
												<select name="status_e[]">
													<option value="Active" <?php if($row['status']=="Active") { echo "selected"; } ?>>Active</option>
													<option value="Inactive" <?php if($row['status']=="Inactive") { echo "selected"; } ?>>Inactive</option>
												</select>
												</td>

						<td width="53" class="functionlink bottomline" align="center" valign="top">
						<input type="text" name="sort[]" value="<?php echo $row['sort']; ?>" size="6">
						</td>
						<?php if($_SESSION['cms_role']==2) { ?>
						<td width="21" class="functionlink bottomline" align="center" valign="top">
							<a href="edit-entertainment-highlights.php?id=<?php echo $row['id']; ?>&page=<?php echo $page; ?>&orderby=<?php echo htmlspecialchars($orderby); ?>&search_value=<?php echo $search_value; ?>"><img src="images/edit.jpg" border="none"></a>
						
						</td>
						<td width="40" class="functionlink bottomline" align="center" valign="top">
						
							<a href="javascript:;" onClick="DeleteEntertainmentHighlights('<?php echo $row['id']; ?>')"><img src="images/delete.gif" border="none"></a>
						
						</td>
						<?php } ?>
					</tr>
					<input type="hidden" name="sortid[]" value="<?php echo $row['id']; ?>">
					<?php
			   }
			?>
			</table>
			
			
			 <div class="clearboth"></div>
			   <br>
			   <?php if($_SESSION['cms_role']==2) { ?>
			 	<input type="submit" name="savesequence" value="Update">
			
				<?php } ?>
				</form>
				<table border="0" cellspacing="0" cellpadding="0" class="page_table" align="left">
				<tr>
				<td align="left" class="style5"><?php
				$lrange = max(1,$page-(($range-1)/2));
				$rrange = min($totalpage,$page+(($range-1)/2));
				if (($rrange - $lrange) < ($range - 1))
					{
						if ($lrange == 1)
							{
								$rrange = min($lrange + ($range-1), $totalpage);
							}
						else
							{
								$lrange = max($rrange - ($range-1), 0);
							}
					}
				if($totalpage>1)
				{
					if ($page > 1)//previous page
						{
							$prev=$page-1;
							echo " <a href='".$PHP_SELF."?page=1&orderby=$orderby&search_value=$search_value'><< First</a> | "; 
							echo " <a href='".$PHP_SELF."?page=$prev&orderby=$orderby&search_value=$search_value'>< Prev</a> | ";
						}
					else
						{
							echo " <strong><< First</strong> | <strong>< Prev</strong> |";
						}
					for($i = 1; $i <= $totalpage; $i++)//the range of page
						{
							if ($i == $page)
								{
									echo " <span class='actual_page'><strong>$i</strong></span>";
								}
							else
								{
									if ($lrange <= $i and $i <= $rrange)
										{
											echo " <a href='".$PHP_SELF."?page=$i&orderby=$orderby&search_value=$search_value' class='bluecolor word fontsubtitle'>$i</a> ";
										}
								}
						}
					if ($page < $totalpage)//next page
						{
							$next=$page+1;
							echo "| <a href='".$PHP_SELF."?page=$next&orderby=$orderby&search_value=$search_value'>Next ></a> |";
							echo " <a href='".$PHP_SELF."?page=$totalpage&orderby=$orderby&search_value=$search_value'>Last >></a>";
							
						}
					else
						{
							echo " | <strong>Next ></strong> | <strong>Last >></strong>";
						}
				}//end of if $totalpage > 1
			?></td>
			  </tr>
			</table>
				
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

