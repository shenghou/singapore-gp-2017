<?php 
session_start();

if($_SESSION['cms_username']=="")
{
	header('Location:/sgp_admin/login.php');
}

include "../include/configure.php";
$lists = array(
"newsletter.newsletter" => "Newsletter",
"sgp_cms.four_day_contest" => "Four Day Contest",
"sgp_cms.mark_webber_contest" => "Mark Webber Contest",
"sgp_cms.accredited_av_2013" => "Accredited AV 2013",
"sgp_cms.accredited_av_incharge_2013" => "Accredited AV Incharge 2013",
"sgp_cms.accredited_journalist_2013" => "Acceredited Journalist 2013",
"sgp_cms.accredited_photographer_crew_2013" => "Acceredited Photographer Crew 2013",
"sgp_cms.seb2013_contest" => "Super Early Bird Contest",
"sgp_cms.survey_tple" => "TPLE Survey",
"sgp_cms.survey_paddock" => "Paddock Survey",
"sgp_cms.survey_grandstand" => "Grandstand Survey",
"singaporegp.survey_grandstand" => "Grandstand Survey (Staging)",
"sgp_cms.survey_paddock" => "Paddock Survey",
"sgp_cms.survey_paddock_hosted_suites" => "Paddock Hosted Suites Survey",
"sgp_cms.merchandise_2013_contest" => "Merchanise 2013 Contest",
"sgp_cms.photo_contest_2013" => "Photo 2013 Contest",
"sgp_cms.course_crowded_space" => "Course Crowded Space",
);

$type = $_GET["type"];
if (empty($type))
{
	echo "<ol>";
	foreach ($lists as $list=>$label)
	{
		printf('<li><a href="?type=%s">%s</a>'.PHP_EOL, $list, $label);
	}
	echo "</ol>";
	exit;
}

if (!isset($lists[$type]))
{
	die("undefined contest");
}

switch ($type)
{
	case "sgp_cms.survey_grandstand":
	case "singaporegp.survey_grandstand":
		$sql = <<<sql
select *,
if(find_in_set("2013", past_attends), 1, 0) as y2013,
if(find_in_set("2012", past_attends), 1, 0) as y2012,
if(find_in_set("2011", past_attends), 1, 0) as y2011,
if(find_in_set("2010", past_attends), 1, 0) as y2010,
if(find_in_set("2009", past_attends), 1, 0) as y2009,
if(find_in_set("2008", past_attends), 1, 0) as y2008
from {$type}
sql;
		break;
	
	default:
		$sql = sprintf("select * from %s", $type);
		break;
}

# customised sql
$fp = fopen('php://temp/maxmemory:'. (1024*1024), 'r+');
$links = array("link", "link1", "link2");
$rows = array();
foreach ($links as $conn)
{
	if (is_resource($$conn))
	{
		$res = mysql_query($sql, $$conn) or die(mysql_error());
		while ($row = mysql_fetch_assoc($res))
		{
			if (empty($cols))
			{
				$cols = array_keys($row);
            	fputcsv($fp, $cols);
			}
			fputcsv($fp, $row);
		}
		mysql_free_result($res);
	}
}

$file = sprintf("%s-%s", $type, date("Y-m-d"));
header('Content-type: text/csv');
header(sprintf('Content-Disposition: attachment; filename=%s.csv', $file));
header('Pragma: no-cache');
header('Expires: 0');

rewind($fp);
echo stream_get_contents($fp);
fclose($fp);

