<?php 
require_once '../function/helper.php';
require_once '../function/cms-helper.php';

$status_option = array(
	'Active' => 'Active',
	'Inactive' => 'Inactive'
);

session_start();

include("include/configure.php");

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
	exit();
}

if(isset($_REQUEST['search_value'])) {
	$search_value=$_REQUEST['search_value'];
	if($search_value!="") {
		$q_search="AND (ticketing_category LIKE '%$search_value%' OR ticket_subcategory LIKE '%$search_value%' OR ticket_subcategory_chinese LIKE '%$search_value%' OR access_to_zones LIKE '%$search_value%' OR accessible_entertainment_stages LIKE '%$search_value%')";

	}
}


if(isset($_REQUEST['savesequence'])) {
	$sort_update=$_POST['sort'];
	$sortid_update=$_POST['sortid'];
	$count_update=count($sortid_update);

	for($i=0;$i<$count_update;$i++) {
		$opt = array(
			'table' => 'ticketing_info',
			'value' => array('sort' => $sort_update[$i], 'status' => $_POST['status'][$i]),
			'where' => array('id' => $sortid_update[$i])
		);
		db_update($opt);
	}
}

if(empty($_REQUEST['page']))
{
	$page=1;
}
else
{
	if($_REQUEST['page']=="")
	{
		$page=1;
	}
	else
	{
		$page=$_REQUEST['page'];
	}
}
			
$site = $_SERVER['PHP_SELF'];
$range=25;//the range of pages display
$limit=25;//the limit of rows display in a page

$sqlcount="SELECT b.id as id FROM ticketing_category a,ticketing_info b WHERE a.id=b.ticketing_category_id $q_search ORDER BY a.sort,b.sort,b.id";
$resultcount=mysql_query($sqlcount,$link)or die(mysql_error());
$count=mysql_num_rows($resultcount);
$totalpage=ceil($count/$limit);
					
$limitvalue=($page*$limit)-$limit;

if($_GET['orderby']=="") {
	$orderby="a.sort,b.sort,b.id";
} else {
	$orderby=$_GET['orderby'];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/ajax.js"></script>
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Ticketing Info  Listing</div>
		
		<div class="searching_field">
		<form method="post">
		<table width="248" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="46">Search</td>
			<td width="133"><input type="text" name="search_value" value="<?php echo stripslashes($search_value); ?>"></td>
			<td width="69"><input type="submit" name="search" value="search"></td>
		</tr>
		</table>
		
		</div>
		<div class="clearboth"></div>
		<div class="functionlink"><a href="add-ticketing-info.php">Add Ticketing Info</a></div>
		<div id="content_list">
			<table width="760" align="left" cellpadding="5" cellspacing="0" style="border-top:1px solid #B6BEBE;">
			<tr class="blue_column">
				<td width="30" class="functionlink bottomline leftline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=a.sort,b.sort,b.id&search_value=$search_value"; ?>">No</a></strong></td>
				<td width="126" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=a.ticketing_category&search_value=$search_value"; ?>">Ticket Category</a></strong></td>
				<td width="123" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=b.ticket_subcategory&search_value=$search_value"; ?>">Ticket SubCategory</a></strong></td>
<?php
/*
				<td width="82" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=b.access_to_zones&search_value=$search_value"; ?>">Access To Zones</a></strong></td>
*/
?>
				<td width="108" class="functionlink bottomline"><strong><a href="<?php echo "".$PHP_SELF."?orderby=b.accessible_entertainment_stages&search_value=$search_value"; ?>">Accessible Entertainment Stages</a></strong></td>
				<td width="58" class="functionlink bottomline"><strong>Display Order</strong></td>
				<td width="76" class="functionlink bottomline" align="center"><strong>Ticketing Price/<strong>Related Photo</strong></strong></td>
				<td width="58" class="functionlink bottomline" align="center"><strong>Status</strong></td>
				<td width="23" class="bottomline" align="center"><strong>Edit</strong></td>
				<td width="42" class="bottomline" align="center"><strong>Delete</strong></td>
			</tr>
			<?php 
			   if($page!=1 && $page!="") {
						$pagecount=$page-1;
						$num=$pagecount * $limit;
			   	}
				$q="SELECT b.id as id,b.sort as sort,ticketing_category,ticket_subcategory,ticket_subcategory_chinese,access_to_zones,accessible_entertainment_stages,ticketing_year,b.status as status FROM ticketing_category a,ticketing_info b WHERE a.id=b.ticketing_category_id $q_search ORDER BY ticketing_year,$orderby LIMIT $limitvalue,$limit";
			   $result=mysql_query($q,$link) or die(mysql_error());
			   while($row=mysql_fetch_array($result)) {
			  	 	$num=$num+1;
					
					
					
			   		?>
					<tr>
						<td width="30" class="bottomline leftline" valign="top"><?php echo $num; ?></td>
						<td width="126" class="bottomline"  valign="top"><?php echo $row['ticketing_category']; ?> <br>(<?php echo $row['ticketing_year']; ?>)</td>
						<td width="123" class="bottomline"  valign="top"><?php echo $row['ticket_subcategory']; ?></td>
<?php
/*<td width="82" class="bottomline"  valign="top"><?php echo $row['access_to_zones']; ?></td>
*/
?>
						<td width="108" class="bottomline"  valign="top"><?php echo $row['accessible_entertainment_stages']; ?></td>
						
						<td width="58" class="functionlink bottomline" align="center"  valign="top">
						<input type="text" name="sort[]" value="<?php echo $row['sort']; ?>" size="6">
						</td>
						<td width="76" class="bottomline functionlink" valign="top" align="center"><a href="ticketing-price-listing.php?ticketing_info_id=<?php echo $row['id']; ?>" target="_blank">View</a></td>
<td width="58" class="functionlink bottomline" align="center" valign="top">
<?php echo set_select('status[]', $status_option, $row['status']); ?>
</td>
						<td width="23" class="functionlink bottomline" align="center"  valign="top">
							<a href="edit-ticketing-info.php?id=<?php echo $row['id']; ?>&page=<?php echo $page; ?>&orderby=<?php echo $orderby; ?>&search_value=<?php echo $search_value; ?>"><img src="images/edit.jpg" border="none"></a>
						
						</td>
						<td width="42" class="functionlink bottomline" align="center"  valign="top">
						
							<a href="javascript:;" onClick="DeleteTicketingInfo('<?php echo $row['id']; ?>')"><img src="images/delete.gif" border="none"></a>
						
						</td>
					</tr>
					<input type="hidden" name="sortid[]" value="<?php echo $row['id']; ?>">
					<?php
			   }
			?>
			</table>
			
			
			 <div class="clearboth"></div>
			  <br/><br/>
           		
             	<input type="submit" name="savesequence" value="Quick Update">
				</form>
				<table border="0" cellspacing="0" cellpadding="0" class="page_table" align="left">
				<tr>
				<td align="left" class="style5"><?php
				$lrange = max(1,$page-(($range-1)/2));
				$rrange = min($totalpage,$page+(($range-1)/2));
				if (($rrange - $lrange) < ($range - 1))
					{
						if ($lrange == 1)
							{
								$rrange = min($lrange + ($range-1), $totalpage);
							}
						else
							{
								$lrange = max($rrange - ($range-1), 0);
							}
					}
				if($totalpage>1)
				{
					if ($page > 1)//previous page
						{
							$prev=$page-1;
							echo " <a href='".$PHP_SELF."?page=1&orderby=$orderby&search_value=$search_value'><< First</a> | "; 
							echo " <a href='".$PHP_SELF."?page=$prev&orderby=$orderby&search_value=$search_value'>< Prev</a> | ";
						}
					else
						{
							echo " <strong><< First</strong> | <strong>< Prev</strong> |";
						}
					for($i = 1; $i <= $totalpage; $i++)//the range of page
						{
							if ($i == $page)
								{
									echo " <span class='actual_page'><strong>$i</strong></span>";
								}
							else
								{
									if ($lrange <= $i and $i <= $rrange)
										{
											echo " <a href='".$PHP_SELF."?page=$i&orderby=$orderby&search_value=$search_value' class='bluecolor word fontsubtitle'>$i</a> ";
										}
								}
						}
					if ($page < $totalpage)//next page
						{
							$next=$page+1;
							echo "| <a href='".$PHP_SELF."?page=$next&orderby=$orderby&search_value=$search_value'>Next ></a> |";
							echo " <a href='".$PHP_SELF."?page=$totalpage&orderby=$orderby&search_value=$search_value'>Last >></a>";
							
						}
					else
						{
							echo " | <strong>Next ></strong> | <strong>Last >></strong>";
						}
				}//end of if $totalpage > 1
			?></td>
			  </tr>
			</table>
				
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

