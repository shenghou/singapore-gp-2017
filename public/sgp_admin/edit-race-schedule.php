<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");
include ("include/resize.image.class.php");
require_once '../function/cms-helper.php';

$id=(isset($_GET['id'])?$_GET['id']:"");
$successword = "";
$errorword = "";

if(isset($_POST['submit'])) {
	$error=0;

	
	$race_schedule_date_id=$_POST['race_schedule_date_id'];
	$refer_id=$_POST['refer_id'];
	$time=$_POST['time'];
	$time_to=$_POST['time_to'];
	$title=$_POST['title'];
	$session=$_POST['session'];
	$content=$_POST['content'];
	$shortdescription=$_POST['shortdescription'];
	$thumbnail=$_POST['thumbnail'];
	
	
	
	if($race_schedule_date_id=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please select Race Schedule Date.";
	}
	if($time=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Time.";
	}
	if($title=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Title.";
	}
	if($session=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Session.";
	}
	if($shortdescription=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Short Description.";
	}
	if($thumbnail=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert thumbnail.";
	}
	
	
	if($error!=1) {
	
			
			
			/*
			$target_paths="../race_schedule_thumbnail//";
			$target_path_database="race_schedule_thumbnail/";
			if(!is_dir($target_paths)) {
				if(mkdir($target_paths,777))
				{
					chmod($target_paths,0777);
				}
			}//end if(!is_dir($target_path)) {
		
	
			
			if($_FILES['thumbnail']['name']!="") {
					
					
					$imagename=explode(".",$_FILES['thumbnail']['name']);
					$countimagename=count($imagename);
					$imagefilename="";
					for($i=0;$i<$countimagename;$i++) {
					
						if($i!=$countimagename-1) {
							$imagefilename.=$imagename[$i];
						}
					}
					
					
					$image = new Resize_Image;
					$image->new_width = 1024;
					$image->new_height = 1024;
					$image->image_to_resize=$_FILES['thumbnail']['tmp_name']; // Full Path to the file
					$image->ratio = true; // Keep Aspect Ratio?
					$image->new_image_name=$imagefilename;
					$image->save_folder =$target_paths;
					$process = $image->resize();
					$media_path = "".$process['new_file_path']; 
					
					$media_path_array_large=explode("/",$media_path);
					if ($_SERVER["SERVER_ADDR"] == "173.230.156.228") {
						$path_image="http://173.230.156.228/sgpstaging/race_schedule_thumbnail/".$media_path_array_large[3];
					} else {
						$path_image="http://web2.singaporegp.sg/race_schedule_thumbnail/".$media_path_array_large[3];
					}

					# generate thumbnails
					list($width, $height, $type) = getimagesize($media_path);
					$ratio = $width / $height;

					$the_width = 130;
					$the_height = round( $the_width * $ratio);
					$tb = str_replace('.jpg', '-sw'.$the_width.'.jpg', $media_path);
					generate_thumbnail($media_path, $tb, $the_width, $the_height);
					
					$q="UPDATE race_schedule SET thumbnail='".mysqli_real_escape_string($link, $path_image)."' WHERE id='".mysqli_real_escape_string($link, $id)."'";
					$result=mysqli_query($link, $q) or die(mysqli_error());

					if (isset($link1) && is_resource($link1)) {
						mysql_query($q,$link1) or die(mysqli_error());
					}

					if (isset($link2) && is_resource($link2)) {
						mysql_query($q,$link2) or die(mysqli_error());
					}

			}
			*/
			
			
			if($time_to=="") {
				$q_time_to=",time_to=NULL";
			} else {
				$q_time_to=",time_to='".mysqli_real_escape_string($link, $time_to)."'";
			}
			
			$q="UPDATE race_schedule SET race_schedule_date_id='".mysqli_real_escape_string($link, $race_schedule_date_id)."',refer_id='".mysqli_real_escape_string($link, $refer_id)."',time='".mysqli_real_escape_string($link, $time)."',title='".mysqli_real_escape_string($link, $title)."',session='".mysqli_real_escape_string($link, $session)."',content='".mysqli_real_escape_string($link, $content)."',updater='".mysqli_real_escape_string($link, $_SESSION[cms_username])."',shortdescription='".mysqli_real_escape_string($link, $shortdescription)."'$q_time_to, thumbnail='".mysqli_real_escape_string($link, $thumbnail)."' WHERE id='".mysqli_real_escape_string($link, $id)."'";
			$result=mysqli_query($link, $q) or die(mysqli_error());

if (isset($link1) && is_resource($link1)) {
	mysql_query($q,$link1) or die(mysqli_error());
}

if (isset($link2) && is_resource($link2)) {
	mysql_query($q,$link2) or die(mysqli_error());
}

			header('Location:race-schedule-listing.php?page='.$_REQUEST[page].'&orderby='.$_REQUEST[orderby].'&search_value='.$_REQUEST[search_value].'');
			exit();
			
		
	}

} else {
	
	$q="SELECT * FROM race_schedule WHERE id='".mysqli_real_escape_string($link, $id)."'";
	$result=mysqli_query($link, $q) or die(mysqli_error());
	$row=mysqli_fetch_array($result);
	
	$race_schedule_date_id=$row['race_schedule_date_id'];
	$refer_id=$row['refer_id'];
	$time=$row['time'];
	$time_to=$row['time_to'];
	$title=$row['title'];
	$session=$row['session'];
	$content=$row['content'];
	$shortdescription=$row['shortdescription'];
	$thumbnail=$row['thumbnail'];
	
	
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript">

tinyMCE.init({

        // General options
		
		
        mode: "exact",

    	elements: "content,subtitle",

        theme : "advanced",

        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",


        // Theme options

        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect",

       	theme_advanced_buttons2 : "fontsizeselectcut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview",

       theme_advanced_buttons3 : "forecolor,backcolor,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",

       theme_advanced_buttons4 : "print,|,ltr,rtl,|,fullscreen,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",

        theme_advanced_toolbar_location : "top",

        theme_advanced_toolbar_align : "left",
		
		

        theme_advanced_statusbar_location : "bottom",

        theme_advanced_resizing : true,

		theme_advanced_blockformats : "p,div,address,pre,h1,h2,h3,h4,h5,h6",

		forced_root_block : '',

        // Skin options

        skin : "o2k7",

        skin_variant : "silver",

		
        // Example content CSS (should be your site CSS)

        content_css : "../css/page.css,../css/txtcolor.css",



        // Drop lists for link/image/media/template dialogs

        template_external_list_url : "js/template_list.js",

        external_link_list_url : "js/link_list.js",

        external_image_list_url : "js/image_list.js",

        media_external_list_url : "js/media_list.js",

		convert_urls : false,
		
		forced_root_block : 'p',
		
		

        // Replace values for the template plugin

        template_replace_values : {

                username : "Some User",

                staffid : "991234"

        }
		

});

</script>
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Edit Race Schedule</div>
		<div id="back_to_list"><a href="race-schedule-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
			<div>
				
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Race Schedule Date  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="race_schedule_date_id">
						<option value=''>Please Select</option>
						<?php 
						$q1="SELECT id,schedule_date FROM race_schedule_date ORDER BY id";
						$result1=mysqli_query($link, $q1) or die(mysqli_error());
						while($row1=mysqli_fetch_array($result1)) {
							echo "<option value='".$row1['id']."'";
							if($race_schedule_date_id==$row1['id']) { echo "selected"; } 
							echo ">".$row1['schedule_date']."</option>";
						}
						?>
					</select>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Time  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
						From <input type="text" name="time"  value="<?php echo $time; ?>" style="width:100px" id="username">
						To <input type="text" name="time_to"  value="<?php echo $time_to; ?>" style="width:100px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Title  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="title"  value="<?php echo $title; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Session  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="session"  value="<?php echo $session; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Short Description  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<textarea name="shortdescription" style="width:500px;height:100px;font-size:11px"><?php echo stripslashes($shortdescription); ?></textarea>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Content  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<textarea name="content" style="width:500px;height:350px;font-size:11px"><?php echo stripslashes($content); ?></textarea>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Refer Content:</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="refer_id">
						<option value=''>Please Select</option>
						<?php 
						$q1="SELECT id,title FROM race_schedule WHERE content!='' ORDER BY id";
						$result1=mysqli_query($link, $q1) or die(mysqli_error());
						while($row1=mysqli_fetch_array($result1)) {
							echo "<option value='".$row1['id']."'";
							if($refer_id==$row1['id']) { echo "selected"; } 
							echo ">".$row1['title']."</option>";
						}
						?>
					</select>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Thumbnail: * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="thumbnail" value="<?php echo $thumbnail; ?>" style="width: 100%">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><img src="<?php echo $thumbnail; ?>" width="130"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Update Race Schedule"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
			  </div>
		  </form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

