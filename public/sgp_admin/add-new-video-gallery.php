<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");

$id=$_REQUEST['id'];

if(isset($_POST['submit'])) {
	$error=0;
	
	$title=$_POST['title'];
	$video_link=$_POST['video_link'];
	$video_flv_link=$_POST['video_flv_link'];
	$video_ogg_link=$_POST['video_ogg_link'];
	$video_youtube_link=$_POST['video_youtube_link'];
	
	if($title=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Title.";
	}
	if($_FILES['thumbnail_link']['name']=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please upload Video Thumbnail.";
	}
	if($video_link=="" && $video_youtube_link=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Video Link MP4.";
	}
	if($video_flv_link=="" && $video_youtube_link=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Video Link FLV.";
	}
	if($video_ogg_link=="" && $video_youtube_link=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Video Link OGG.";
	}
	
	
	if($error!=1) {
	
			if($_FILES['thumbnail_link']['name']!="") {
			
			$target_paths="../Uploaded/video_gallery/";
			$target_path_database="Uploaded/video_gallery/";
			if(!is_dir($target_paths)) {
				if(mkdir($target_paths,777))
				{
					chmod($target_paths,0777); 
				}
			}//end if(!is_dir($target_path)) {
			
			
			
			
			
			$target_path=$target_paths.basename( $_FILES['thumbnail_link']['name']); 
			move_uploaded_file($_FILES['thumbnail_link']['tmp_name'], $target_path);

			if ($_SERVER["SERVER_ADDR"] == "173.230.156.228") {
				$image_path="http://173.230.156.228/sgpstaging/".$target_path_database.basename( $_FILES['thumbnail_link']['name']);
			} else {
				$image_path="http://web2.singaporegp.sg/".$target_path_database.basename( $_FILES['thumbnail_link']['name']);
			}

			}
			
			

			$datetime=date("Y-m-d H:i:s");
			
			$q="SELECT sort FROM video_gallery_mp4 ORDER BY sort DESC";
			$result=mysql_query($q,$link) or die(mysql_error());
			$row=mysql_fetch_array($result);
			
			$sort=$row['sort'];
			$sort=$sort+1;
			
			
			$q="INSERT INTO video_gallery_mp4 (video_link,thumbnail_link,title,video_flv_link,video_ogg_link,sort,video_youtube_link) VALUES ('".mysql_real_escape_string($video_link)."','".mysql_real_escape_string($image_path)."','".mysql_real_escape_string($title)."','".mysql_real_escape_string($video_flv_link)."','".mysql_real_escape_string($video_ogg_link)."','".mysql_real_escape_string($sort)."','".mysql_real_escape_string($video_youtube_link)."')";
			$result=mysql_query($q,$link) or die(mysql_error());

			if (isset($link1) && is_resource($link1)) {
				mysql_query($q, $link1) or die(mysql_error());
			}

			if (isset($link2) && is_resource($link2)) {
				mysql_query($q, $link2) or die(mysql_error());
			}

			header('Location:video-gallery-listing.php?page='.$_REQUEST[page].'&orderby='.$_REQUEST[orderby].'&search_value='.$_REQUEST[search_value].'');
			exit();
			
		
	}

} 
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Add Video Gallery </div>
		<div id="back_to_list"><a href="video-gallery-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
			<div>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Title   : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					  <input type="text" name="title"  value="<?php echo $title; ?>" style="width:500px" id="username">
					 
				  </td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Video Thumbnail : *<br>
				    (260 x 156)					</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="thumbnail_link" value=""></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Video Link MP4 : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="video_link"  value="<?php echo $video_link; ?>" style="width:500px "></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Video Link FLV : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="video_flv_link"  value="<?php echo $video_flv_link; ?>" style="width:500px "></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Video Link OGG : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="video_ogg_link"  value="<?php echo $video_ogg_link; ?>" style="width:500px "></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Video Link Youtube : *</strong></td>
				  <td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="video_youtube_link"  value="<?php echo $video_youtube_link; ?>" style="width:500px ">
				    <br>
				    http://www.youtube.com/watch?v=18ZOkMpqWn4</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Update Video Gallery"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				
				</div>
				</form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

