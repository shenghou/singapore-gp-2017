<?php 
session_start();

$errorword="";

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");

$id=(isset($_REQUEST['id'])?$_REQUEST['id']:"");
$q_search_value=(isset($_GET['q_search_value'])?$_GET['q_search_value']:"");
$search_value=(isset($_GET['search_value'])?$_GET['search_value']:"");

if ($search_value!="") {
	$q_search_value="AND CategoryID='$_GET[search_value]'";
}

if(isset($_POST['submit'])) {
	$count_photo=count($_POST['photo_id']);
	for($i=0;$i<$count_photo;$i++) {
		
		$category_insert=$_POST['category'][$i];
		$caption_insert=$_POST['caption'][$i];
		$photo_id_insert=$_POST['photo_id'][$i];
		$sort_insert=$_POST['sort'][$i];
		//$sort_fan_zone_thumbnail=$_POST['sort_fan_zone_thumbnail'][$i];
		$delete_photo_id=(isset($_POST['delete_image'.$photo_id_insert])?$_POST['delete_image'.$photo_id_insert]:"");

		if($delete_photo_id!="") {
			/*unlink("Uploaded/".$delete_photo_id);

			$q_d="SELECT * FROM photo_gallery WHERE id='$photo_id_insert'";
			$result_d=mysqli_query($link, $q_d) or die(mysqli_error($link));
			$row_d=mysqli_fetch_array($result_d);
			
			unlink("Uploaded/".$row_d['image_path_large']);
			*/
			
			$q_update="DELETE FROM photo_gallery WHERE id='$photo_id_insert'";
			$result_update=mysqli_query($link, $q_update) or die(mysqli_error($link));
			
			if (isset($link1)) {
				mysqli_query($link1, $q_update) or die(mysqli_error($link1));
			}

			if (isset($link2)) {
				mysqli_query($link2, $q_update) or die(mysqli_error($link2));
			}
		
		}
		else {
			$q_update="UPDATE photo_gallery SET caption='$caption_insert',sort='$sort_insert',CategoryID='$category_insert' WHERE id='$photo_id_insert'";
			$result_update=mysqli_query($link, $q_update) or die(mysqli_error($link));

			if (isset($link1)) {
				mysqli_query($link1, $q_update) or die(mysqli_error($link1));
			}

			if (isset($link2)) {
				mysqli_query($link2, $q_update) or die(mysqli_error($link2));
			}
		}
	
	}
	
	
}


$q="SELECT * FROM photo_album WHERE id='$id'";
$result=mysqli_query($link, $q) or die(mysqli_error($link));
$row=mysqli_fetch_array($result);

$title=$row['title'];
$description=$row['description'];
$AlbumDate=$row['AlbumDate'];

$ori = array("\n");
$replace_word=array("<br>");
	
$description = str_replace($ori, $replace_word, $description);



?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/ajax.js"></script>
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container" style="width:810px;">
		<div id="title">Manage Album </div>
		<div id="back_to_list"><a href="list-photo-album.php?">Back to List</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="add-photo-gallery.php?id=<?=$id?>">Add Photo</a>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Category:  
		<select name="category_search" onChange="document.location.href=this.options[this.selectedIndex].value;">
		<option value="<?php echo $_SERVER['PHP_SELF']; ?>?id=<?php echo $_GET['id']; ?>">View All</option>
		<option value="<?php echo $_SERVER['PHP_SELF']; ?>?id=<?php echo $_GET['id']; ?>&search_value=nocategory" 
		<?php if($search_value=="nocategory") { echo "selected"; } ?>
		>No Category Selected</option>
		<?php 
		$q_category_search="SELECT * FROM category_gallery ORDER BY category";
		$result_category_search=mysqli_query($link, $q_category_search) or die(mysqli_error($link));
		while($row_category_search=mysqli_fetch_array($result_category_search)) {
			echo "<option value='".$_SERVER['PHP_SELF']."?id=".$_GET['id']."&search_value=".$row_category_search['id']."'";
			if($search_value==$row_category_search['id']) { echo "selected"; }
			echo ">".$row_category_search['category']."</option>";
		}
		?>
		</select>
		</div>
		<div id="content">
			<form action="<?php $PHP_SELF; ?>" method="post">
			<div>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				
				<tr>
					<td width="148" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="565" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="148" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Album Date  : </strong></td>
					<td width="565" style="border-bottom:1px dotted #BAB9B9;"><?php echo date('j/n/Y', strtotime($row['AlbumDate'])); ?></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="148" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Title   : </strong></td>
					<td width="565" style="border-bottom:1px dotted #BAB9B9;"><?php echo $title; ?></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="148" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;" valign="top"><strong>Description   : </strong></td>
					<td width="565" style="border-bottom:1px dotted #BAB9B9;" valign="top"><?php echo $description; ?></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				</table>
			  </div>
				<div class="clearboth"></div>
				<div style="text-align:center;margin:20px 0 0 0"><input type="submit" name="submit" value="Save Change"></div>
				<div>
				
				<?php
				
					$q_photo="SELECT * FROM photo_gallery WHERE photo_album_id='$id' $q_search_value ORDER BY sort desc";
					$result_photo=mysqli_query($link, $q_photo) or die(mysqli_error($link));
					$num=0;
					while($row_photo=mysqli_fetch_array($result_photo)) {
						$num=$num+1;
						$photo_id=$row_photo['id'];
						$PhotoAlbumCover=$row_photo['PhotoAlbumCover'];
						$FanZoneThumbnail=$row_photo['fan_zone_thumbnail'];
						?>
						<div style="float:left;display:inline;margin:20px 10px 0 0"> 
						<table width="260" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="260" height="150" valign="top">
								<div style="background-color:#E6EEEE;padding:10px 10px 10px 15px;height:310px;border:1px solid #CCCCCC">
									<img src="<?=$row_photo['image_path_thumb']?>" style="height: 140px; max-width: 230px;">
								<table border="0" cellpadding="0" cellspacing="0">
									
									<tr>
										<td width="200" valign="middle" height="40px">Caption<input type="text" name="caption[]" value="<?php echo $row_photo['caption']; ?>" style="width:200px"></td>
									</tr>
									<tr>
										<td width="200" valign="middle" height="40px">
											Category
										<select name="category[]">
											<option value="">Please Select</option>
											<option value="1" <?=("1"==$row_photo['CategoryID']?"selected":"")?>>Race</option>
											<option value="2" <?=("2"==$row_photo['CategoryID']?"selected":"")?>>Drivers</option>
											<option value="3" <?=("3"==$row_photo['CategoryID']?"selected":"")?>>Circuit Park</option>
											<option value="4" <?=("4"==$row_photo['CategoryID']?"selected":"")?>>Pre-Race</option>
											<option value="5" <?=("5"==$row_photo['CategoryID']?"selected":"")?>>Entertainment</option>
											<option value="6" <?=("6"==$row_photo['CategoryID']?"selected":"")?>>Roving Performances</option>
										</select>
										</td>
									</tr>
									<tr>
									  <td width="200" valign="middle"><input type="checkbox" name="delete_image<?php echo $photo_id; ?>" value="<? echo $row_photo['image_path_thumb']; ?>"> Delete</td>
									</tr>
									<!--<tr>
									  <td width="200" valign="middle"><input type="radio" name="photo_album_cover" value="<?php echo $photo_id; ?>" onClick="SetPhotoAlbum(this.value)" <?php if($PhotoAlbumCover==1) { echo "checked"; } ?>> Photo Album Cover</td>
									</tr>-->
									<tr>
										<td width="200" valign="middle" height="40px">Sort <input type="text" name="sort[]" value="<?php echo $row_photo['sort']; ?>" style="width:100px"></td>
									</tr>
								  </table>
								  </div>
								  <div style="margin-bottom:10px"></div>
								 <div style="background-color:#E6EEEE;padding:10px 10px 10px 15px;height:80px;border:1px solid #CCCCCC">
								 <!-- <table border="0" cellpadding="0" cellspacing="0">
								   <tr>
									  <td width="200" valign="middle"><input type="checkbox" name="fan_zone_thumbnail" value="<?php echo $photo_id; ?>" onClick="SetFanZoneThumbnail(this.value)" <?php if($FanZoneThumbnail==1) { echo "checked"; } ?>> Fan Zone Thumbnail</td>
									</tr>
									 <tr>
										<td width="200" valign="middle" height="40px">Sort <input type="text" name="sort_fan_zone_thumbnail[]" value="<?php echo $row_photo['sort_fan_zone_thumbnail']; ?>" style="width:100px"></td>
									</tr>
								  </table>-->
								 </div>
								
						  </td>
						  </tr>
				  		</table>
						</div>
						<input type="hidden" name="photo_id[]" value="<?php echo $photo_id ?>">
				<?php 
					if($num%3==0)
						echo "<div class='clearboth'></div>";
				} ?>		
				</div>
				<div class="clearboth"></div>
				<div style="text-align:center;margin:20px 0 0 0"><input type="submit" name="submit" value="Save Change"></div>
				
		  </form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

