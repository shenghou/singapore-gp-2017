<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");

$id=$_REQUEST['id'];

if(isset($_POST['submit'])) {
	$error=0;

	
	$title=$_POST['title'];
	$position=$_POST['position'];
	$time=$_POST['time'];
	$driver=$_POST['driver'];
	
	
	if($title=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Title.";
	}
	if($position=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Position.";
	}
	
	if($driver=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Driver.";
	}
	if($time=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Time.";
	}
	
	
	
	if($error!=1) {
	
			$q="UPDATE race_result SET title='".mysql_real_escape_string($title)."'";
			$result=mysql_query($q,$link) or die(mysql_error());


			
			$q="UPDATE race_result SET position='".mysql_real_escape_string($position)."',driver='".mysql_real_escape_string($driver)."',time='".mysql_real_escape_string($time)."' WHERE id='".mysql_real_escape_string($id)."'";
			$result=mysql_query($q,$link) or die(mysql_error());

		
			header('Location:race-result-listing.php?page='.$_REQUEST[page].'&orderby='.$_REQUEST[orderby].'&search_value='.$_REQUEST[search_value].'');
			exit();
			
		
	}

} else {
	
	$q="SELECT * FROM race_result WHERE id='$_REQUEST[id]'";
	$result=mysql_query($q,$link) or die(mysql_error());
	$row=mysql_fetch_array($result);
	
	$position=$row['position'];
	$title=$row['title'];
	$driver=$row['driver'];
	$time=$row['time'];
	
	
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />

<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Edit Race Result </div>
		<div id="back_to_list"><a href="race-result-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post">
			<div>
				
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Title  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="title"  value="<?php echo $title; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Position  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="position"  value="<?php echo $position; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Driver  : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="driver"  value="<?php echo $driver; ?>" style="width:500px" id="role_name"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Time  : * </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="time"  value="<?php echo $time; ?>" style="width:500px" id="role_name"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Update Race Result"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				</div>
				</form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

