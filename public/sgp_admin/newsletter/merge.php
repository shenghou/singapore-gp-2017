<?php 
session_start();

if($_SESSION['cms_username']=="")
{
	header('Location:/sgp_admin/login.php');
}

if (isset($_FILES["cm"]))
{
	include "../include/configure.php";

	# all our emails
	$sql = "select * from newsletter.newsletter";
	$links = array("link", "link1", "link2");
	$emails = array();
	$col = "txtEmail";
	foreach ($links as $conn)
	{
		if (is_resource($$conn))
		{
			$res = mysql_query($sql, $$conn) or die(mysql_error());
			while ($row = mysql_fetch_assoc($res))
			{
				$email = strtolower($row[$col]);
				if (!isset($emails[$email]))
				{
					$emails[$email] = "sgp";
				}
			}
			mysql_free_result($res);
		}
	}
	#var_dump(count($emails));

	# all cm email
	if (($handle = fopen($_FILES["cm"]["tmp_name"], "r")) !== FALSE)
	{
		$cols = array();
		$key = "Email Address";
		while (($data = fgetcsv($handle, 8192, ",")) !== FALSE)
		{
			if (empty($cols))
			{
				$cols = $data;
				continue;
			}

			$row = array_combine($cols, $data);
			$email = strtolower($row[$key]);

			if (isset($emails[$email]))
			{
				if (strtolower($row["Status"]) != "active")
				{
					unset($emails[$email]);
				}
			}
			else
			{
				if (strtolower($row["Status"]) == "active")
				{
					$emails[$row[$key]] = "cm";
				}
			}
		}
		fclose($handle);
	}

	#var_dump(count($emails));exit;
	$fp = fopen('php://temp/maxmemory:'. (1024*1024), 'r+');
	$file = sprintf("newsletter-merge-%s", date("Y-m-d"));
	header('Content-type: text/csv');
	header(sprintf('Content-Disposition: attachment; filename=%s.csv', $file));
	header('Pragma: no-cache');
	header('Expires: 0');

	fputcsv($fp, array("email", "type"));
	foreach ($emails as $email => $type)
	{
		fputcsv($fp, array($email, $type));
	}

	rewind($fp);
	echo stream_get_contents($fp);
	fclose($fp);

}
else
{
	echo <<<form
<form name="cm-upload" method="POST" enctype="multipart/form-data">
	<input type="file" name="cm">
	<button type="submit">Upload</button>
</form>

form;
}

