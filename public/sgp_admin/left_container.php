<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td height="23" colspan="2" valign="top"><img src="images/module.gif"> &nbsp;<strong>Modules</strong></td>
		</tr>
		<?php if($_SESSION['cms_role']==2) { ?>
		
		<tr>
			<td colspan="2" height="32" valign="bottom"><strong>Users Module</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-new-user.php">Add New User</a></td>
		</tr>-->
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="user-listing.php">User Listing</a></td>
		</tr>
		<tr>
			<td valign="bottom" class="left_navi_black" style="border-bottom:1px solid #BAB9B9" colspan="2">&nbsp;</td>
		</tr>
		
		
		
		<tr>
			<td colspan="2" height="32" valign="bottom"><strong>Home Screen Module</strong></td>
		</tr>
		<tr>
			<td colspan="2" height="18" valign="bottom"><strong>Singapore F1 Highlights</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="38" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-new-singapore-f1-highlights.php">Add New Singapore F1 Highlights</a></td>
		</tr>-->
		<tr>
			<td width="18" height="38" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="singapore-f1-highlights-listing.php">Singapore F1 Highlights Listing</a></td>
		</tr>
		
		
		
		
		<tr>
			<td colspan="2" height="19" valign="bottom"><strong>Race Calendar</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-new-race-calendar.php">Add New Race Calendar</a></td>
		</tr>-->
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="race-calendar-listing.php">Race Calendar Listing</a></td>
		</tr>
		
		
		<tr>
			<td colspan="2" height="19" valign="bottom"><strong>Team Result</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-new-team-result.php">Add New Team Result</a></td>
		</tr>-->
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="team-result-listing.php">Team Result Listing</a></td>
		</tr>
		
		
		
		
		
		<tr>
			<td colspan="2" height="19" valign="bottom"><strong>Driver Result</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-new-driver-result.php">Add New Driver Result</a></td>
		</tr>-->
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="driver-result-listing.php">Driver Result Listing</a></td>
		</tr>
		
		
		
		<tr>
			<td colspan="2" height="19" valign="bottom"><strong>Race Result</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-new-race-result.php">Add New Race Result</a></td>
		</tr>-->
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="race-result-listing.php">Race Result Listing</a></td>
		</tr>
<?php
/*
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="more-fl-results.php">More F1 Results</a></td>
		</tr>
*/
?>
		<tr>
			<td valign="bottom" class="left_navi_black" style="border-bottom:1px solid #BAB9B9" colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" height="33" valign="bottom"><strong>Race Module</strong></td>
		</tr>
		<tr>
			<td colspan="2" height="29" valign="bottom"><strong>Formula 1 Singapore Race Schedule</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-new-race-schedule-date.php">Add New Race Schedule Date</a></td>
		</tr>-->
<?php
/*
		<tr>
			<td width="18" height="20" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="schedule-status.php">Schedule Status</a></td>
		</tr>
*/
?>
		<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="race-schedule-date-listing.php">Race Schedule Date Listing</a></td>
		</tr>
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="race-schedule-listing.php">Race Schedule Listing</a></td>
		</tr>

		<tr>
			<td colspan="2" height="33" valign="bottom"><strong>Team &amp; Driver Module</strong></td>
		</tr>
		<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black">
				<ul style="list-style:none; line-height:15px; margin-left:0px;">
					<li style="margin-left:-40px"><a href="team-intro.php">Team Intro</a><br>
					<li style="margin-left:-40px"><a href="team-listing.php">Team Listing</a><br>
					<li style="margin-left:-40px"><a href="driver-listing.php">Driver Listing</a>
				</ul>
			</td>
		</tr>
		
		<!--<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-race-schedule.php">Add New Race Schedule</a></td>
		</tr>-->
		<tr>
			<td colspan="2" height="19" valign="bottom"><strong>Transport Guide</strong></td>
		</tr>
		<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="edit-transport-guide-content.php">Edit Transport Guide Content</a></td>
		</tr>
		<!--<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-transport-guide-location-listing.php">Add Transport Guide Location</a></td>
		</tr>-->
		<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="transport-guide-location-listing.php">Transport Guide Location Listing</a></td>
		</tr>
		
		
		<!--<tr>
			<td width="18" height="22" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-transport-guide.php">Add Transport Guide</a></td>
		</tr>-->
		<tr>
			<td width="18" height="22" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="transport-guide-listing.php">Transport Guide Listing</a></td>
		</tr>
		
		
		
		
		<tr>
			<td colspan="2" height="29" valign="bottom"><strong>Singapore Grand Prix Highlights</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-new-singapore-grand-prix.php">Add New Singapore Grand Prix Highlights</a></td>
		</tr>-->
		<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="singapore-grand-prix-listing.php">Singapore Grand Prix Highlights Listing</a></td>
		</tr>
		
		<tr>
			<td colspan="2" height="29" valign="bottom"><strong>Entertainment Highlights</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-entertainment-highlights.php">Add New Entertainment Highlights</a></td>
		</tr>-->
		<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="entertainment-time-slot-listing.php">Entertainment Time Slot Listing</a></td>
		</tr>
		<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="entertainment-highlights-listing.php">Entertainment Highlights Listing</a></td>
		</tr>
		
		
		
		
		<tr>
			<td colspan="2" height="29" valign="bottom"><strong>Zone Write Up</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-entertainment-highlights.php">Add New Entertainment Highlights</a></td>
		</tr>-->
		<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="zone-writeup-listing.php">Zone Write Up<br> 
		    Listing</a></td>
		</tr>
		
		
		<tr>
			<td valign="bottom" class="left_navi_black" style="border-bottom:1px solid #BAB9B9" colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" height="33" valign="bottom"><strong>Tickets Module</strong></td>
		</tr>
		<tr>
			<td width="18" height="25" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black">
				<a href="ticketing-disclaimer.php">Disclaimer</a>
			</td>
		</tr>
		<tr>
			<td colspan="2" height="29" valign="bottom"><strong>Authorized Ticketing Agents (Singapore)</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="45" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-new-ticketing-agent-singapore.php">Add New Authorized Ticketing Agents (Singapore)</a></td>
		</tr>-->
		<tr>
			<td width="18" height="55" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="ticketing-agent-singapore-listing.php">Authorized Ticketing Agents (Singapore) Listing</a></td>
		</tr>
		<tr>
			<td colspan="2" height="29" valign="bottom"><strong>Authorized Ticketing Agents (International)</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="45" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-new-ticketing-agent-international-region.php">Add New Authorized Ticketing Agents (International) Region</a></td>
		</tr>-->
		<tr>
			<td width="18" height="55" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="ticketing-agent-international-region-listing.php">Authorized Ticketing Agents (International) Region Listing</a></td>
		</tr>
		
		
		<!--<tr>
			<td width="18" height="55" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-new-ticketing-agent-international-country.php">Add New Authorized Ticketing Agents (International) Country</a></td>
		</tr>-->
		<tr>
			<td width="18" height="55" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="ticketing-agent-international-country-listing.php">Authorized Ticketing Agents (International) Country Listing</a></td>
		</tr>
		
		
		
		<!--<tr>
			<td width="18" height="55" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-new-ticketing-agent-international.php">Add New Authorized Ticketing Agents (International)</a></td>
		</tr>-->
		<tr>
			<td width="18" height="55" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="ticketing-agent-international-listing.php">Authorized Ticketing Agents (International) Listing</a></td>
		</tr>
		<tr>
			<td colspan="2" height="20" valign="bottom"><strong>Ticket Price</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-ticket-category.php">Add Ticket Category</a></td>
		</tr>-->
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="ticketing-phase-listing.php">Ticketing Phase Listing</a></td>
		</tr>
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="ticket-category-listing.php">Ticket Category Listing</a></td>
		</tr>
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="ticketing-info-listing.php">Ticketing Info Listing</a></td>
		</tr>
		<tr>
			<td valign="bottom" class="left_navi_black" style="border-bottom:1px solid #BAB9B9" colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" height="33" valign="bottom"><strong>Media Module</strong></td>
		</tr>
		<tr>
			<td colspan="2" height="10" valign="bottom"><strong>Press Release</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-press-release.php">Add Press Release</a></td>
		</tr>-->
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="press-release-listing.php">Press Release Listing</a></td>
		</tr>
		<tr>
			<td colspan="2" height="29" valign="bottom"><strong>Media Accreditation Information</strong></td>
		</tr>
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="edit-media-accreditation.php">Edit Media Accreditation</a></td>
		</tr>
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="export-media-accreditation.php?year=2013&type=journalist">Export Journalist Year 2013</a></td>
		</tr>
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="export-media-accreditation.php?year=2013&type=photographer">Export Photographer & TV Crew 2013</a></td>
		</tr>
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="export-media-accreditation.php?year=2013&type=av">Export AV Equipment 2013</a></td>
		</tr>
		<tr>
			<td colspan="2" height="29" valign="bottom"><strong>Tips</strong></td>
		</tr>
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="edit-tips-title.php">Edit Tips Title</a></td>
		</tr>
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="tips-listing.php">Tips Listing</a></td>
		</tr>

		<tr>
			<td colspan="2" height="15" valign="bottom"><strong>Video Gallery</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-new-video-gallery.php">Add Video Gallery</a></td>
		</tr>-->
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="video-gallery-listing.php">Video Gallery Listing</a></td>
		</tr>
		<tr>
			<td colspan="2" height="15" valign="bottom"><strong>Wallpaper Gallery</strong></td>
		</tr>
		<!--<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="add-new-video-gallery.php">Add Video Gallery</a></td>
		</tr>-->
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="wallpaper-gallery-listing.php">Wallpapers Listing</a></td>
		</tr>

			<tr>
				<td valign="bottom" class="left_navi_black" style="border-bottom:1px solid #BAB9B9" colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" height="33" valign="bottom"><strong>Photo Contest Module</strong></td>
			</tr>
			<tr>
				<td width="18" height="18" valign="top">&nbsp;</td>
				<td width="121" valign="bottom" class="left_navi_black"><a href="photo-contest-listing.php">Photo Contest</a></td>
			</tr>


		<?php } ?>

		<tr>
			<td valign="bottom" class="left_navi_black" style="border-bottom:1px solid #BAB9B9" colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" height="33" valign="bottom"><strong>Gallery Module</strong></td>
		</tr>
		<tr>
			<td colspan="2" height="15" valign="bottom"><strong>Photo Gallery</strong></td>
		</tr>
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="list-photo-album.php">Photo Gallery Listing</a></td>
		</tr>

		<!--<tr>
			<td valign="bottom" class="left_navi_black" style="border-bottom:1px solid #BAB9B9" colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" height="33" valign="bottom"><strong>Password Module</strong></td>
		</tr>
		<tr>
			<td width="18" height="18" valign="top">&nbsp;</td>
			<td width="121" valign="bottom" class="left_navi_black"><a href="change-password.php">Change Password</a></td>
		</tr>-->
		
		
		<tr>
			<td colspan="2" height="35" valign="bottom"><strong>Logout</strong></td>
		</tr>
		
		<tr>
			<td width="18" height="35" valign="top">&nbsp;</td>
			<td width="121" valign="top" class="left_navi_black"><a href="logout.php?process=1">Logout</a></td>
		</tr>
		</table>
