<?php 
require_once '../function/helper.php';
require_once '../function/cms-helper.php';
require_once 'include/configure.php';

check_session_exists();

$status_option = array(
	'' => '--',
	'Available' => 'Available',
	'Sold Out' => 'Sold Out',
	'Inactive' => 'Inactive'
);

if(isset($_REQUEST['search_value'])) {
	$search_value=$_REQUEST['search_value'];
	if($search_value!="") {
		$q_search="AND (date LIKE '%$search_value%' OR time LIKE '%$search_value%' OR zone LIKE '%$search_value%')";
	}
}

$ticketing_info_id=$_REQUEST['ticketing_info_id'];

$q_enter="SELECT ticketing_category_id,ticket_subcategory,access_to_zones,accessible_entertainment_stages,ticket_subcategory_chinese FROM ticketing_info WHERE id='".mysql_real_escape_string($ticketing_info_id)."'";
$result_enter=mysql_query($q_enter,$link) or die(mysql_error());
$row_enter=mysql_fetch_assoc($result_enter);
mysql_free_result($result_enter);

$q_category="SELECT ticketing_type FROM ticketing_category WHERE id='".mysql_real_escape_string($row_enter['ticketing_category_id'])."'";
$result_category=mysql_query($q_category,$link) or die(mysql_error());
$row_category=mysql_fetch_assoc($result_category);
mysql_free_result($result_category);

$ticketing_type=$row_category['ticketing_type'];


if($_GET['orderby']=="") {
	$orderby="id";
} else {
	$orderby=$_GET['orderby'];
}

$q_phase="SELECT id,phase_description,phase_date FROM ticketing_phase ORDER BY id";
$result_phase=mysql_query($q_phase,$link) or die(mysql_error());
while($row_phase=mysql_fetch_assoc($result_phase)) {
	$phase_description[]=$row_phase['phase_description'];
	$phase_date[]=$row_phase['phase_date'];
	$phase_id[]=$row_phase['id'];
}

if (isset($_POST['savesequence'])) {
	$sort_update=$_POST['sort'];
	$sortid_update=$_POST['sortid'];
	$count_update=count($sortid_update);

	if($ticketing_type=="3-Day Ticket") {
		$q_data="ticketing_three_day";
	}
	else if($ticketing_type=="3-Day Combination Ticket") {
		$q_data="ticketing_three_day_combination";
	}
	else if($ticketing_type=="Single Day Ticket") {
		$q_data="ticketing_single_day";
	}

	for($i=0;$i<$count_update;$i++) {
		$opt = array(
			'table' => $q_data,
			'value' => array('sort' => $sort_update[$i], 'ticketing_status' => $_POST['ticketing_status'][$i]),
			'where' => array('id' => $sortid_update[$i])
		);

		if (isset($_POST['ticketing_link'])) {
			$opt['value']['ticketing_link'] = $_POST['ticketing_link'];
		}

		if (isset($_POST['ticketing_link_chinese'])) {
			$opt['value']['ticketing_link_chinese'] = $_POST['ticketing_link_chinese'];
		}

		if (isset($_POST['ticketing_note'])) {
			$opt['value']['ticketing_note'] = $_POST['ticketing_note'];
		}

		if (isset($_POST['ticketing_note_chinese'])) {
			$opt['value']['ticketing_note_chinese'] = $_POST['ticketing_note_chinese'];
		}

		db_update($opt);
	}
}

	
if(isset($_POST['submit'])) {
	$count_photo=count($_POST['photo_id']);
	for($i=0;$i<$count_photo;$i++) {
		
		$caption_insert=$_POST['caption'][$i];
		$caption_chinese_insert=$_POST['caption_chinese'][$i];
		$photo_id_insert=$_POST['photo_id'][$i];
		$sort_insert=$_POST['sort'][$i];
		
		$delete_photo_id=$_POST['delete_image'.$photo_id_insert];

		if($delete_photo_id!="") {
			/*
			unlink("Uploaded/".$delete_photo_id);
			$q_d="SELECT * FROM photo_gallery WHERE id='$photo_id_insert'";
			$result_d=mysql_query($q_d,$link) or die(mysql_error());
			$row_d=mysql_fetch_assoc($result_d);
			
			unlink("Uploaded/".$row_d['image_path_large']);
			
			*/

			# todo
			$q_update="DELETE FROM ticketing_related_photo WHERE id='$photo_id_insert'";
			$result_update=mysql_query($q_update,$link) or die(mysql_error());

            if (isset($link1) && is_resource($link1)) {
                mysql_query($q_update, $link1) or die(mysql_error());
            }

            if (isset($link2) && is_resource($link2)) {
                mysql_query($q_update, $link2) or die(mysql_error());
            }
		}
		
		else {
			# todo
			$q_update="UPDATE ticketing_related_photo SET caption='".addslashes($caption_insert)."',sort='".addslashes($sort_insert)."',caption_chinese='".addslashes($caption_chinese_insert)."' WHERE id='$photo_id_insert'";
			$result_update=mysql_query($q_update,$link) or die(mysql_error());

if (isset($link1) && is_resource($link1)) {
	mysql_query($q_update, $link1) or die(mysql_error());
}

if (isset($link2) && is_resource($link2)) {
	mysql_query($q_update, $link2) or die(mysql_error());
}
		}
		
	
	}
	
	
}





?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/ajax.js"></script>
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container" style="width:810px;">
		<div id="title">Ticketing Price  Listing</div>
		
		<div class="searching_field">
		<table width="248" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="46">Search</td>
			<td width="133"><input type="text" name="search_value" value="<?php echo stripslashes($search_value); ?>"></td>
			<td width="69"><input type="submit" name="search" value="search"></td>
		</tr>
		</table>
		<input type="hidden" name="ticketing_info_id" value="<?php echo $ticketing_info_id; ?>">
		
		</div>
		<div class="clearboth"></div>
		

		<div>
		<table width="758" border="0" cellpadding="5" cellspacing="0" style="font-size:12px;border-top:1px solid #B6BEBE;margin-top:20px">
		<tr>
			<td width="204" class="functionlink bottomline leftline blue_column"><strong>Ticketing Category: </strong></td>
			<td width="534" style="border-bottom:1px solid #B6BEBE; border-right:1px solid #B6BEBE">
			<?php
				$q1="SELECT ticketing_category FROM ticketing_category WHERE id='".mysql_real_escape_string($row_enter['ticketing_category_id'])."'";
				$result1=mysql_query($q1,$link) or die(mysql_error());
				$row1=mysql_fetch_assoc($result1);
				
				echo $row1['ticketing_category'];
			?>
			</td>
		</tr>
		<tr>
			<td class="functionlink bottomline leftline blue_column"><strong>Ticketing SubCategory: </strong></td>
			<td style="border-bottom:1px solid #B6BEBE; border-right:1px solid #B6BEBE"><?php echo $row_enter['ticket_subcategory']; ?></td>
		</tr>
		<tr>
			<td class="functionlink bottomline leftline blue_column"><strong>Ticketing SubCategory Chinese: </strong></td>
			<td style="border-bottom:1px solid #B6BEBE; border-right:1px solid #B6BEBE"><?php echo $row_enter['ticket_subcategory_chinese']; ?></td>
		</tr>
		<tr>
			<td class="functionlink bottomline leftline blue_column"><strong>Access to Zones: </strong></td>
			<td style="border-bottom:1px solid #B6BEBE; border-right:1px solid #B6BEBE"><?php echo $row_enter['access_to_zones']; ?></td>
		</tr>
		<tr>
			<td class="functionlink bottomline leftline blue_column"><strong>Accessible Entertainment Stages: </strong></td>
			<td style="border-bottom:1px solid #B6BEBE; border-right:1px solid #B6BEBE"><?php echo $row_enter['accessible_entertainment_stages']; ?></td>
		</tr>
		</table>
		
		</div>
		<div style="clear:both"></div>
		
		<div class="functionlink" style="margin:30px 0 0 0; font-size:12px"><strong><a href="add-ticketing-price.php?ticketing_info_id=<?php echo $ticketing_info_id; ?>">Add Ticketing Price</a></strong></div>
		
		<div id="content_list">

<?php
foreach ($phase_description as $idx => $null) {
	$sort_expr = '?orderby=%s&search_value=%s&ticketing_info_id=%s';
	$default_sort = sprintf($sort_expr, 'sort,id', $search_value, $ticketing_info_id);
	$group_sort = sprintf($sort_expr, 'ticketing_group_booking', $search_value, $ticketing_info_id);
	$desc_sort = sprintf($sort_expr, 'ticketing_description', $search_value, $ticketing_info_id);
	$price_sort = sprintf($sort_expr, 'ticketing_price', $search_value, $ticketing_info_id);
	$status_sort = sprintf($sort_expr, 'ticketing_status', $search_value, $ticketing_info_id);
	?>
<form method="post">
<div><strong><?php echo $phase_description[$idx]; ?> (<?php echo $phase_date[$idx]; ?>)</strong></div>
<table width="760" align="left" cellpadding="5" cellspacing="0" style="border-top:1px solid #B6BEBE;">
<tr class="blue_column">
	<td width="27" class="functionlink bottomline leftline"><strong><a href="<?php echo $default_sort; ?>">No</a></strong></td>
	<td width="156" class="functionlink bottomline"><strong><a href="<?php echo $group_sort; ?>">Group Booking</a></strong></td>
	<?php
	if($ticketing_type=="3-Day Combination Ticket") {
		$day_sort = sprintf($sort_expr, 'day_word', $search_value, $ticketing_info_id);
	?>
		<td width="99" class="functionlink bottomline"><strong><a href="<?php echo $day_sort; ?>">Day</a></strong></td>
	<?php
	}
	?>
	<td width="166" class="functionlink bottomline"><strong><a href="<?php echo $desc_sort; ?>">Description</a></strong></td>
	<td width="99" class="functionlink bottomline"><strong><a href="<?php echo $price_desc; ?>">Price</a></strong></td>
	<td width="88" class="functionlink bottomline"><strong><a href="<?php echo $status_sort;?>">Status</a></strong></td>
	<td width="85" class="functionlink bottomline"><strong>Display Order</strong></td>
	<td width="21" class="bottomline" align="center"><strong>Edit</strong></td>
	<td width="36" class="bottomline" align="center"><strong>Delete</strong></td>
</tr>
	<?php

	if($page!=1 && $page!="") {
		$pagecount=$page-1;
		$num=$pagecount * $limit;
	}

	$esc_id = mysql_real_escape_string($ticketing_info_id);
	$esc_phase = mysql_real_escape_string($phase_id[$idx]);
	$form = false;

	switch ($ticketing_type) {
		case '3-Day Ticket':
			$table = 'ticketing_three_day';
			break;

		case '3-Day Combination Ticket':
			$table = 'ticketing_three_day_combination';
			break;

		case 'Single Day Ticket':
			$table = 'ticketing_single_day';
			break;
	}

	$sql = <<<sql
SELECT * FROM {$table}
WHERE ticketing_info_id={$esc_id} 
AND  ticketing_phase_id={$esc_phase}
$q_search ORDER BY sort,id
sql;

	$result=mysql_query($sql, $link) or die(mysql_error());
	$batch_update_form = null;
	while($row=mysql_fetch_assoc($result)) {
		$num=$num+1;
		if (empty($form)) {
			$esc_ticketing_link = htmlspecialchars($row['ticketing_link']);
			$esc_ticketing_link_chinese = htmlspecialchars($row['ticketing_link_chinese']);
			$esc_ticketing_note = $row['ticketing_note'];
			$esc_ticketing_note_chinese = $row['ticketing_note_chinese'];
			$batch_update_form = <<<form
<h4>Apply the following to all ticket price</h4>
<table>
	<tr>
		<th>Ticket Link:</th>
		<td>
			<input type="text" name="ticketing_link" value="{$esc_ticketing_link}" size="100%">
		</td>
	</tr>
	<tr>
		<th>Ticket Link Chinese:</th>
		<td>
			<input type="text" name="ticketing_link_chinese" value="{$esc_ticketing_link_chinese}" size="100%">
		</td>
	</tr>
	<tr>
		<th>Ticket Note :</th>
		<td>
			<textarea name="ticketing_note" rows="5" cols="100" >{$esc_ticketing_note}</textarea>
		</td>
	</tr>
	<tr>
		<th>Ticket Note Chinese :</th>
		<td>
			<textarea name="ticketing_note_chinese" rows="5" cols="100">{$esc_ticketing_note_chinese}</textarea>
		</td>
	</tr>
</table>

form;
		}
		$form = true;
	?>
<tr>
	<td width="27" class="bottomline leftline" valign="top"><?php echo $num; ?></td>
	<td width="156" class="bottomline" valign="top"><?php if($row['ticketing_group_booking']=="1") { echo "Group Booking Special"; } else { echo "-"; } ?></td>
	<?php if($ticketing_type=="3-Day Combination Ticket") { ?>
		<td width="99" class="bottomline" valign="top"><?php echo $row['day_word']; ?></td>
	<?php } ?>
	<td width="166" class="bottomline" valign="top"><?php echo $row['ticketing_description']; ?></td>
	<td width="99" class="bottomline" valign="top"><?php echo $row['ticketing_price']; ?></td>
	<td width="88" class="bottomline" valign="top">
		<?php echo set_select('ticketing_status[]', $status_option, $row['ticketing_status']); ?>
	</td>
	<td width="85" class="functionlink bottomline" align="center"  valign="top">
		<input type="text" name="sort[]" value="<?php echo $row['sort']; ?>" size="6">
	</td>
	<td width="21" class="functionlink bottomline" align="center" valign="top">
		<a href="edit-ticketing-price.php?id=<?php echo $row['id']; ?>&page=<?php echo $page; ?>&orderby=<?php echo $orderby; ?>&search_value=<?php echo $search_value; ?>&ticketing_info_id=<?php echo $ticketing_info_id; ?>"><img src="images/edit.jpg" border="none"></a>
	</td>
	<td width="36" class="functionlink bottomline" align="center" valign="top">
		<a href="javascript:;" onClick="DeleteTicketingPrice('<?php echo $row['id']; ?>','<?php echo $row['ticketing_info_id']; ?>')"><img src="images/delete.gif" border="none"></a>
	</td>
</tr>
<input type="hidden" name="sortid[]" value="<?php echo $row['id']; ?>">
	<?php
	}
	?>
</table>
<div class="clearboth"></div>
<?php
echo $batch_update_form;
unset($batch_update_form);
?>
<br>
<input type="hidden" name="ticketing_type" value="<?php echo $ticketing_type; ?>">
<input type="submit" name="savesequence" value="Quick Update">
</form>	
<br>
<br>
<?php
}
?>

	<div id="title">Manage Ticketing Related Photo</div>
				<div id="back_to_list"><a href="add-ticketing-related-photo.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>&ticketing_info_id=<?php echo $_REQUEST['ticketing_info_id']; ?>">Add Photo</a>
			</div>
			
			
			<form method="post">
			<div>
		
		
		</div>
		
				<div class="clearboth"></div>
				<div>
				
				<?php
				
					$q_photo="SELECT * FROM ticketing_related_photo WHERE ticketing_info_id='".mysql_real_escape_string($ticketing_info_id)."' $q_search_value ORDER BY sort,id";
					$result_photo=mysql_query($q_photo,$link) or die(mysql_error());
					while($row_photo=mysql_fetch_assoc($result_photo)) {
						$num=$num+1;
						$photo_id=$row_photo['id'];
						
						?>
						<div style="float:left;display:inline;margin:20px 10px 0 0"> 
						<table width="260" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="260" valign="top">
								<div style="background-color:#E6EEEE;padding:10px 10px 10px 15px;border:1px solid #CCCCCC">
								<?php echo "<img src='".str_replace('.jpg', '-sw200.jpg', $row_photo['related_photo'])."' width='200px'>"; ?>
								<table border="0" cellpadding="0" cellspacing="0">
									
									<tr>
									  <td width="200" valign="middle" height="40px"><input type="checkbox" name="delete_image<?php echo $photo_id; ?>" value="<? echo $row_photo['related_photo']; ?>"> Delete</td>
									</tr>
									<tr>
										<td width="200" valign="middle"><strong>Caption</strong> <br><input type="text" name="caption[]" value="<?php echo $row_photo['caption']; ?>" style="width:220px"></td>
									</tr>
									<tr>
										<td width="200" valign="middle"><strong>Caption Chinese</strong><br><input type="text" name="caption_chinese[]" value="<?php echo $row_photo['caption_chinese']; ?>" style="width:220px"></td>
									</tr>
									<tr>
										<td width="200" valign="middle" height="40px"><strong>Sort</strong> <br><input type="text" name="sort[]" value="<?php echo $row_photo['sort']; ?>" style="width:100px"></td>
									</tr>
									
								  </table>
								</div>
								
						  </td>
						  </tr>
				  		</table>
						</div>
						<input type="hidden" name="photo_id[]" value="<?php echo $photo_id ?>">
				<?php 
					if($num%3==0)
						echo "<div class='clearboth'></div>";
				} ?>		
				</div>
				<div class="clearboth"></div>
				<div style="text-align:center;margin:20px 0 0 0"><input type="submit" name="submit" value="Save Change"></div>
				
		  </form>
		
		</div>
	</div>
	<div class="clearboth"></div>
	
	
</div>
</body>
</html>

