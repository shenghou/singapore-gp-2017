<?php 
require_once '../function/helper.php';
require_once '../function/cms-helper.php';

session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
	exit();
}

$table = 'team';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$opt['table'] = $table;
	$opt['value'] = $_POST;
	$status = db_insert($opt);
	if ($status) {
		$id = db_last_id();
		header(sprintf('location: edit-team.php?id=%d', $id));
	} else {
		$status = -1;
		$row = $_POST;
	}
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Team</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/cms.css" rel="stylesheet" type="text/css" />
<body>
<div id="container">
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<?php
		if (is_bool($status)) {
			if ($status === true) {
				echo '<div class="success">Insert Successfully!</div>';
			} else {
				echo '<div class="fail">Unable to insert, please try again later.</div>';
			}
		}
		?>
		<h1>Add New Team</h1>
		<div id="back_to_list">
			<a href="team-listing.php">Back to List</a>
		</div>
		<form method="post">

<table border="0" align="left" cellpadding="6" cellspacing="0" class="form">
<?php
$form = array(
	'full_name' => array('label'=>'Team Full Name', 'maxlength'=>255),
	'name' => array('label'=>'Team Short Name', 'maxlength'=>100),
	'seo_url' => array('label'=>'SEO URL', 'maxlength'=>100, 'tips'=>'red-bull-racing'),
	'photo' => array('label'=>'Photo', 'maxlength'=>255),
	'base' => array('label'=>'Base', 'maxlength'=>255),
	'bio' => array('label'=>'Team Bio', 'type'=>'textarea', 'tinymce'=>true, 'tinymce_width'=>400),
	'status' => array('label'=>'Status', 'type'=>'radio', 'selection'=>array(0=>'pending', 1=>'active')),
	'seq' => array('label'=>'Seq', 'type'=>'text', 'maxlength='=>2, 'style'=>'width:15px'),
);

if (empty($row)) {
	$row = array_fill_keys(array_keys($form), null);
}

generate_form($form, $row);
?>
</table>

<div class="button-div">
	<button class="submit-button" type="submit">Insert</button>
	<button class="reset-button" type="reset">Reset</button>
</div>
					
		</form>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

