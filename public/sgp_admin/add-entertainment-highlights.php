<?php 
session_start();

if($_SESSION['cms_username']=="") {
	header('Location:login.php');
}
include("include/configure.php");
include ("include/resize.image.class.php");
include_once("_CrossFileUploadManager.php");

$id=$_REQUEST['id'];

if(isset($_POST['submit'])) {
	$error=0;
	
	$year=$_POST['year'];
	$artist=$_POST['artist'];
	$website=$_POST['website'];
	$performance_description=$_POST['performance_description'];
	$short_description=$_POST['short_description'];
	$url=$_POST['url'];
	$performance_category_array=$_POST['performance_category'];
	$video_flv=$_POST['video_flv'];
	$video_mp4=$_POST['video_mp4'];
	$youtube=$_POST['youtube'];
	$status=$_POST['status'];
	
	$date=$_POST['date'];
	$time=$_POST['time'];
	$zone=$_POST['zone'];
	$zone_link=$_POST['zone_link'];
	$estimated_time=$_POST['estimated_time'];
	$time_slot_id=$_POST['time_slot_id'];
	$start_date= 0 ;
	$end_date= 0;

	

	if($year=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Year.";
	}
	if($artist=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Artist.";
	}

	# 2013-06-02 - the following fields are not longer mandatory
	/*
	if($short_description=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Short Description.";
	}
	if($performance_description=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert Description.";
	}
	*/

/*
	if($url=="") {
		$error=1;
		$errorword.="<br>";
		$errorword.="Please insert SEO URL.";
	}
*/

	/*
	# for past events, the following three columns are not mandatory
	foreach (array('date', 'time', 'zone') as $check_column) {
		if (strtolower($year) == 'current' && empty($$check_column)) {
			$error = true;
			$errorword .= sprintf('<br>Please insert %s'.PHP_EOL, ucwords($check_column));
		}
	}
	*/

	if($error!=1) {
	
	
	if($performance_category_array!="") {
			$performance_category=implode(",",$performance_category_array);
		}

			if($_FILES['image']['name']!="") {
				
				$target_paths="../Uploaded/entertainment_highlights/";
				$target_path_database="Uploaded/entertainment_highlights/";
				if(!is_dir($target_paths)) {
					if(mkdir($target_paths,777))
					{
						chmod($target_paths,0777); 
					}
				}//end if(!is_dir($target_path)) {
				
				
				
					$imagename=explode(".",$_FILES['image']['name']);
					$countimagename=count($imagename);
					$imagefilename="";
					for($i=0;$i<$countimagename;$i++) {
					
						if($i!=$countimagename-1) {
							$imagefilename.=$imagename[$i];
						}
					}
					
					
					$image = new Resize_Image;
					$image->new_width = 310;
					$image->new_height = 310;
					$image->image_to_resize=$_FILES['image']['tmp_name']; // Full Path to the file
					$image->ratio = true; // Keep Aspect Ratio?
					$image->new_image_name=$imagefilename;
					$image->save_folder =$target_paths;
					$process = $image->resize();
					$media_path = "".$process['new_file_path']; 
					
					$media_path_array_large=explode("/",$media_path);
					
					
					
					$image = new Resize_Image;
					$image->new_width = 130;
					$image->new_height = 130;
					$image->image_to_resize=$_FILES['image']['tmp_name']; // Full Path to the file
					$image->ratio = true; // Keep Aspect Ratio?
					$image->new_image_name="tn_".$imagefilename;
					$image->save_folder =$target_paths;
					$process = $image->resize();
					$media_path = "".$process['new_file_path']; 
					
					$media_path_array_thumb=explode("/",$media_path);
					
					$image = new Resize_Image;
					$image->new_width = 60;
					$image->new_height = 60;
					$image->image_to_resize=$_FILES['image']['tmp_name']; // Full Path to the file
					$image->ratio = true; // Keep Aspect Ratio?
					$image->new_image_name="mobile_".$imagefilename;
					$image->save_folder =$target_paths;
					$process = $image->resize();
					$media_path = "".$process['new_file_path']; 
					
					
					$media_path_array_mobile=explode("/",$media_path);
					
                    $path_image="http://web2.singaporegp.sg/Uploaded/entertainment_highlights/".$media_path_array_large[3];
                    $path_image_thumb="http://web2.singaporegp.sg/Uploaded/entertainment_highlights/".$media_path_array_thumb[3];
                    $path_image_mobile="http://web2.singaporegp.sg/Uploaded/entertainment_highlights/".$media_path_array_mobile[3];

                    $imageArray = array();

                    // to clone to Live
                    array_push($imageArray,"http://128.199.250.199/Uploaded/entertainment_highlights/".$media_path_array_large[3]);
                    array_push($imageArray,"http://128.199.250.199/Uploaded/entertainment_highlights/".$media_path_array_thumb[3]);
                    array_push($imageArray,"http://128.199.250.199/Uploaded/entertainment_highlights/".$media_path_array_mobile[3]);

                    CrossFileUploadManager::uploadFiles($imageArray);
            }
			
			

			$datetime=date("Y-m-d H:i:s");
			
			$q="SELECT sort FROM entertainment_highlights WHERE year='".mysql_real_escape_string($year)."'";
			$result=mysql_query($q,$link) or die(mysql_error());
			$row=mysql_fetch_assoc($result);
			
			$sort=$row['sort'];
			if($sort=="") {
				$sort=1;
			} else {
				$sort=$sort+1;
			}
			
			$q="INSERT INTO entertainment_highlights (additional_label, artist,website,performance_description,short_description,image_thumb,image,updater,url,year,sort,performance_category,video_flv,video_mp4,image_mobile,status,youtube) ".
			"VALUES ('".mysql_real_escape_string($_POST['additional_label'])."', '".mysql_real_escape_string($artist)."','".mysql_real_escape_string($website)."','".mysql_real_escape_string($performance_description)."','".mysql_real_escape_string($short_description)."','".mysql_real_escape_string($path_image_thumb)."','".mysql_real_escape_string($path_image)."','".mysql_real_escape_string($_SESSION[cms_username])."','".mysql_real_escape_string($url)."','".mysql_real_escape_string($year)."','".mysql_real_escape_string($sort)."','".mysql_real_escape_string($performance_category)."','".mysql_real_escape_string($video_flv)."','".mysql_real_escape_string($video_mp4)."','".mysql_real_escape_string($path_image_mobile)."','".mysql_real_escape_string($status)."','".mysql_real_escape_string($youtube)."')";
			mysql_query($q,$link) or die(mysql_error());
			if (isset($link1) && is_resource($link1)) {
				mysql_query($q, $link1) or die(mysql_error());
			}

			if (isset($link2) && is_resource($link2)) {
				mysql_query($q, $link2) or die(mysql_error());
			}

			$q="SELECT id FROM entertainment_highlights ORDER BY id DESC";
			$result=mysql_query($q,$link) or die(mysql_error());
			$row=mysql_fetch_assoc($result);

			$entertainment_id=$row['id'];

			if($time_slot_id=="") {
				$q_timeslot="NULL";
			} else {
				$q_timeslot="'".mysql_real_escape_string($time_slot_id)."'";
			}
			
			
			$q="INSERT INTO entertainment_highlights_detail (date,time,zone,zone_link,updater,entertainment_id,estimated_time,time_slot_id,start_date,end_date) VALUES ('".mysql_real_escape_string($date)."','".mysql_real_escape_string($time)."','".mysql_real_escape_string($zone)."','".mysql_real_escape_string($zone_link)."','".mysql_real_escape_string($_SESSION[cms_username])."','".mysql_real_escape_string($entertainment_id)."','".mysql_real_escape_string($estimated_time)."',$q_timeslot,'".mysql_real_escape_string($start_date)."','".mysql_real_escape_string($end_date)."')";
			$result=mysql_query($q,$link) or die(mysql_error());

			if (isset($link1) && is_resource($link1)) {
				mysql_query($q, $link1) or die(mysql_error());
			}

			if (isset($link2) && is_resource($link2)) {
				mysql_query($q, $link2) or die(mysql_error());
			}

			header('Location:entertainment-highlights-listing.php?page='.$_REQUEST[page].'&orderby='.$_REQUEST[orderby].'&search_value='.$_REQUEST[search_value].'');
			exit();
			
		
	}

} else {
	$q="SELECT * FROM entertainment_highlights WHERE id='".mysql_real_escape_string($_REQUEST[id])."'";
	$result=mysql_query($q,$link) or die(mysql_error());
	$row=mysql_fetch_assoc($result);
	
	$artist=$row['artist'];
	$website=$row['website'];
	$performance_description=$row['performance_description'];
	$short_description=$row['short_description'];
	$image_thumb=$row['image_thumb'];
	$image=$row['image'];
	$url=$row['url'];
	$start_date=$row['start_date'];
	$end_date=$row['end_date'];
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Admin Panel - Main Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript">

tinyMCE.init({

        // General options
		
		
        mode: "exact",

    	elements: "short_description,performance_description",

        theme : "advanced",

        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",


        // Theme options

        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect",

       	theme_advanced_buttons2 : "fontsizeselectcut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview",

       theme_advanced_buttons3 : "forecolor,backcolor,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",

       theme_advanced_buttons4 : "print,|,ltr,rtl,|,fullscreen,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",

        theme_advanced_toolbar_location : "top",

        theme_advanced_toolbar_align : "left",
		
		

        theme_advanced_statusbar_location : "bottom",

        theme_advanced_resizing : true,

		theme_advanced_blockformats : "p,div,address,pre,h1,h2,h3,h4,h5,h6",

		forced_root_block : '',

        // Skin options

        skin : "o2k7",

        skin_variant : "silver",

		
        // Example content CSS (should be your site CSS)

        content_css : "../css/page.css,../css/txtcolor.css",



        // Drop lists for link/image/media/template dialogs

        template_external_list_url : "js/template_list.js",

        external_link_list_url : "js/link_list.js",

        external_image_list_url : "js/image_list.js",

        media_external_list_url : "js/media_list.js",

		convert_urls : false,
		
		forced_root_block : 'p',
		
		

        // Replace values for the template plugin

        template_replace_values : {

                username : "Some User",

                staffid : "991234"

        }
		

});

</script>
<body>
<div id="container">
	
	<div id="top_container" align="center"><img src="images/header-admin.jpg"></div>
	<div id="left_container">
		<?php include("left_container.php"); ?>
	</div>
	<div id="right_container">
		<div id="title">Add New   Entertainment Highlights</div>
		<div id="back_to_list"><a href="entertainment-highlights-listing.php?page=<?php echo $_REQUEST['page']; ?>&orderby=<?php echo $_REQUEST['orderby']; ?>&search_value=<?php echo $_REQUEST['search_value']; ?>">Back to List</a></div>
		<div id="contents">
			<form action="<?php $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
			<div>
				<table width="752" border="0" align="left" cellpadding="6" cellspacing="0">
				<?php if($successword=="") { ?>
				<tr>
					<td width="162" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><font color="red"><?php echo $errorword; ?></font>&nbsp;</td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<?php } ?>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Year  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="year"  style="width:500px" id="year">
						<option value="Current" <?php if($year=="Current") { echo "selected"; } ?>>Current</option>
						<option value="Past" <?php if($year=="Past") { echo "selected"; } ?>>Past</option>
					</select>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Artist  : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="artist"  value="<?php echo $artist; ?>" style="width:500px" id="username">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Additional Label  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="text" name="additional_label"  value="<?php echo $additional_label; ?>" style="width:500px">
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Short Description  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="short_description" style="width:500px;height:250px;font-size:11px"><?php echo stripslashes($short_description); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Description  :  </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><textarea name="performance_description" style="width:500px;height:350px;font-size:11px"><?php echo stripslashes($performance_description); ?></textarea></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Performance Category : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<input type="checkbox" name="performance_category[]" value="Roving Performances" <?php if($performance_category[0]=="Roving Performances" || $performance_category[1]=="Roving Performances" || $performance_category[2]=="Roving Performances") { echo "checked"; } ?>> Roving Performances
					<input type="checkbox" name="performance_category[]" value="Performances on Stage" <?php if($performance_category[0]=="Performances on Stage" || $performance_category[1]=="Performances on Stage" || $performance_category[2]=="Performances on Stage") { echo "checked"; } ?>> Performances on Stage
					<input type="checkbox" name="performance_category[]" value="Site Installations" <?php if($performance_category[0]=="Site Installations" || $performance_category[1]=="Site Installations" || $performance_category[2]=="Site Installations") { echo "checked"; } ?>> Site Installations
					<input type="checkbox" name="performance_category[]" value="Magicians" <?php if($performance_category[0]=="Magicians" || $performance_category[1]=="Magicians" || $performance_category[2]=="Magicians") { echo "checked"; } ?>> Magicians
					<input type="checkbox" name="performance_category[]" value="Theatrical Acts" <?php if($performance_category[0]=="Theatrical Acts" || $performance_category[1]=="Theatrical Acts" || $performance_category[2]=="Theatrical Acts") { echo "checked"; } ?>> Theatrical Acts
					<input type="checkbox" name="performance_category[]" value="International Music Acts / Indie Acts" <?php if($performance_category[0]=="International Music Acts / Indie Acts" || $performance_category[1]=="International Music Acts / Indie Acts" || $performance_category[2]=="International Music Acts / Indie Acts") { echo "checked"; } ?>> International Music Acts / Indie Acts
					<input type="checkbox" name="performance_category[]" value="Roving Artistes" <?php if($performance_category[0]=="Roving Artistes" || $performance_category[1]=="Roving Artistes" || $performance_category[2]=="Roving Artistes") { echo "checked"; } ?>> Roving Artistes
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Image :  <br>
			      (310 x 310)					</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="file" name="image"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Website :</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="website"  value="<?php echo $website; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>

				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Video FLV :</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="video_flv"  value="<?php echo $video_flv; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>

				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Video MP4 :</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="video_mp4"  value="<?php echo $video_mp4; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>

				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Youtube :</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="youtube"  value="<?php echo $youtube; ?>" style="width:500px" id="youtube"></td>
					<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>

				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>SEO URL :</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="url"  value="<?php echo $url; ?>" style="width:500px" id="username"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Status : *</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="status">
						<option value="Active" <?php if($status=="Active") { echo "selected"; } ?>>Active</option>
						<option value="Inactive" <?php if($status=="Inactive") { echo "selected"; } ?>>Inactive</option>
					</select>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				  <td style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				  <td style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				  </tr>
				  <tr>
				  <td style="border-bottom:1px dotted #BAB9B9;" colspan="3"><strong>Performance Detail</strong></td>
				  </tr>
				  <tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Date  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="date">
						<option value="TBA" <?php if ($date == 'TBA') echo 'selected';?>>TBA</option>
						<option value="FRI 19 SEP" <?php if($date=="FRI 19 SEP") { echo "selected"; } ?>>FRI 19 SEP (2014)</option>
						<option value="SAT 20 SEP" <?php if($date=="SAT 20 SEP") { echo "selected"; } ?>>SAT 20 SEP (2014)</option>
						<option value="SUN 21 SEP"  <?php if($date=="SUN 21 SEP") { echo "selected"; } ?>>SUN 21 SEP (2014)</option>
						<option value="MON 22 SEP"  <?php if($date=="MON 22 SEP") { echo "selected"; } ?>>MON 22 SEP (2014)</option>
						<option value="FRI 20 SEP" <?php if($date=="FRI 20 SEP") { echo "selected"; } ?>>FRI 20 SEP (2013)</option>
						<option value="SAT 21 SEP" <?php if($date=="SAT 21 SEP") { echo "selected"; } ?>>SAT 21 SEP (2013)</option>
						<option value="SUN 22 SEP"  <?php if($date=="SUN 22 SEP") { echo "selected"; } ?>>SUN 22 SEP (2013)</option>
						<option value="MON 23 SEP"  <?php if($date=="MON 23 SEP") { echo "selected"; } ?>>MON 23 SEP (2013)</option>
					</select>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Time : </strong></td>
<?php
if (empty($time)) {
	$time = 'TBA';
}
?>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="time"  value="<?php echo $time; ?>" style="width:500px" id="time"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Time Slot : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;">
					<select name="time_slot_id">
						<option value="">Please Select</option>
						<?php
							$q_timeslot="SELECT id,time_to,time,race_schedule_date_id FROM entertainment_time_slot ORDER BY race_schedule_date_id,time,time_to";
							$result_timeslot=mysql_query($q_timeslot,$link) or die(mysql_error());
							while($row_timeslot=mysql_fetch_assoc($result_timeslot)) {
							
								
								$q_timeslot1="SELECT schedule_date FROM race_schedule_date WHERE id='".mysql_real_escape_string($row_timeslot['race_schedule_date_id'])."'";
								$result_timeslot1=mysql_query($q_timeslot1,$link) or die(mysql_error());
								$row_timeslot1=mysql_fetch_assoc($result_timeslot1);
								
								$timedisplay="";
								if($row_timeslot['time_to']=="") { $timedisplay.=date('H:i', strtotime($row_timeslot['time'])); } else { $timedisplay.=date('H:i', strtotime($row_timeslot['time'])); $timedisplay.=" - "; $timedisplay.=date('H:i', strtotime($row_timeslot['time_to'])); }
							
								echo "<option value='".$row_timeslot['id']."'";
								if($row_timeslot[id]==$time_slot_id) { echo "selected"; }
								echo ">".$row_timeslot1['schedule_date']." (".$timedisplay.")</option>";
							
							}
						
						?>
					</select>
					</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
<?php
/*
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Start Date</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="start_date"  value="<?php echo $start_date; ?>" style="width:250px" id="time"> (YYYY-MM-DD HH:MM:SS)</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>End Date</strong></td>
				  <td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="end_date"  value="<?php echo $end_date; ?>" style="width:250px" id="time">
(YYYY-MM-DD HH:MM:SS)</td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
*/
?>
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Zone :</strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><select name="zone">
						<option value="">Please Select</option>
						<option value="ZONE 1 – VILLAGE STAGE" <?php if($zone=="ZONE 1 – VILLAGE STAGE") { echo "selected"; } ?>>ZONE 1 – VILLAGE STAGE</option>
						<option value="ZONE 1 – MARINA STAGE" <?php if($zone=="ZONE 1 – MARINA STAGE") { echo "selected"; } ?>>ZONE 1 – MARINA STAGE</option>
						<option value="ZONE 1 – ROVING" <?php if($zone=="ZONE 1 – ROVING") { echo "selected"; } ?>>ZONE 1 – ROVING</option>
						<option value="ZONE 1 – INSTALLATION" <?php if($zone=="ZONE 1 – INSTALLATION") { echo "selected"; } ?>>ZONE 1 – INSTALLATION</option>
						<option value="ZONE 1 – TRISPAN LIFESTYLE AREA" <?php if($zone=="ZONE 1 – TRISPAN LIFESTYLE AREA") { echo "selected"; } ?>>ZONE 1 – TRISPAN LIFESTYLE AREA</option>
						<option value="ZONE 3 – ROVING" <?php if($zone=="ZONE 3 – ROVING") { echo "selected"; } ?>>ZONE 3 – ROVING</option>
						<option value="ZONE 3 – INSTALLATION" <?php if($zone=="ZONE 3 – INSTALLATION") { echo "selected"; } ?>>ZONE 3 – INSTALLATION</option>
						<option value="ZONE 4 – PADANG STAGE" <?php if($zone=="ZONE 4 – PADANG STAGE") { echo "selected"; } ?>>ZONE 4 – PADANG STAGE</option>
						<option value="ZONE 4 – ESPLANADE PARK STAGE" <?php if($zone=="ZONE 4 – ESPLANADE PARK STAGE") { echo "selected"; } ?>>ZONE 4 – ESPLANADE PARK STAGE</option>
						<option value="ZONE 4 – ESPLANADE OUTDOOR THEATRE STAGE" <?php if($zone=="ZONE 4 – ESPLANADE OUTDOOR THEATRE STAGE") { echo "selected"; } ?>>ZONE 4 – ESPLANADE OUTDOOR THEATRE STAGE</option>
						<option value="ZONE 4 – ROVING" <?php if($zone=="ZONE 4 – ROVING") { echo "selected"; } ?>>ZONE 4 – ROVING</option>
						<option value="ZONE 4 – INSTALLATION" <?php if($zone=="ZONE 4 – INSTALLATION") { echo "selected"; } ?>>ZONE 4 – INSTALLATION</option>
						</select></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="162" bgcolor="#E6EEEE" style="border-bottom:1px dotted #BAB9B9;"><strong>Estimated Duration  : </strong></td>
					<td width="551" style="border-bottom:1px dotted #BAB9B9;"><input type="text" name="estimated_time"  value="<?php echo $estimated_time; ?>" style="width:500px" id="estimated_time"></td>
				<td width="3" style="border-bottom:1px dotted #BAB9B9;">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3">* Mandatory Field</td>
				  </tr>
				
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Add New Entertainment Highlights"></td>
				<td>&nbsp;</td>
				</tr>
				</table>
				
				
				
				
			  </div>
		  </form>
		</div>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>

