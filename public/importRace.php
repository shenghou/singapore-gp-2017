<?php
//include_once '/home/singaporegp/sgpstaging/include/configure.php';

if($_SERVER['HTTP_HOST'] == '128.199.201.29')
    include_once '/srv/f1/public/include/configure.php';
else
//    include_once '/var/www/public/include/configure.php';
    include_once 'include/configure.php';

mysql_set_charset('utf8',$link);
mysql_select_db('sgp_cms');


error_reporting(0);

if($_POST['result']){
    $text = $_POST['result'];
    $calId = $_POST['calendarId'];
    $raceUrl = $_POST['raceUrl'];

    $newString = preg_replace('/\t/',',',$text);

    $newString = preg_replace('/^/m',"$calId,",$newString);
    $newString = preg_replace('/(.)$/m',",$raceUrl",$newString);

    function map_driver($driver, $link)
    {
        $driver = trim(utf8_encode($driver));

        if ($driver == 'Kimi RÃ¤ikkÃ¶nen') {
            $driver = 'Kimi Räikkönen';
        }

        $sql = sprintf('select id, team_id from driver where name = "%s"', $driver);
        $res = mysql_query($sql, $link) or die($sql);
        # var_dump($res);
        $row = mysql_fetch_assoc($res);
        if (!$row) {
            die($sql);
        }

        return array($row['id'], $row['team_id']);
    }

    $cols = array('race_id', 'position', 'no', 'driver_id', 'team_id', 'laps', 'result_time', 'grid', 'pts', 'f1_url');
    $best_time = $nest_micro = array();
    $allowed = array('race_id', 'driver_id', 'result_time', 'result_microsec', 'result_note', 'laps', 'position', 'f1_url');

    $insert_file = "";

    foreach(preg_split("/((\r?\n)|(\r\n?))/", $newString) as $data){

        $data = explode(',',$data);
        if(count($data)<10){
            $data[9] = $data[8];
            $data[8] = 0;
        }

        $arr = array_combine($cols, $data);

        list($arr['driver_id'], $arr['team_id']) = map_driver($arr['driver_id'], $link);

        # RF = race finish
        $arr['position'] = $arr['position'] == 'Ret' || $arr['position'] == 'DNS' ? $arr['position'] : 'RF';

        # best time
        $race_id =$arr['race_id'];
        if (empty($best_time[$race_id])) {
            $tmp = explode('.', $arr['result_time']);
            $arr['result_microsec'] = $best_micro[$race_id] = $tmp[1];
            $arr['result_time'] = $best_time[$race_id] = $tmp[0];
        } else {
            if (preg_match('/secs/', $arr['result_time'])) {
                $sec = trim(preg_replace('/[^0-9\.]/', '', $arr['result_time']));
                list($diff, $micro) = explode('.', $sec);
                $arr['result_note'] = $arr['result_time'];
                $arr['result_time'] = date('H:i:s', strtotime(sprintf('+%s seconds', $diff), strtotime($best_time[$race_id])));
            } else {
                $arr['result_note'] = $arr['result_time'];
                $arr['result_time'] = '3:00:00';
            }
        }

        $final = array();
        foreach ($allowed as $col) {
            if (isset($arr[$col])) {
                $final[$col] = '"'.$arr[$col].'"';
            } else {
                $final[$col] = 'null';
            }
        }
        $secondValue = implode(',', array_filter($final));
        $sql = sprintf('insert into race_calendar_result (%s) values (%s);'.PHP_EOL,
            implode(',', $allowed),
            $secondValue);

        // insert into db
        mysql_query($sql, $link) or die($sql);
    }
}



?>

<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12 column">
            <form role="form" method="POST">
                <div class="form-group">
                    <label for="calendarId">Category Id</label>
                    <input type="number" class="form-control" name="calendarId" placeholder="Enter Category Id">
                </div>
                <div class="form-group">
                    <label for="calendarId">Race URL</label>
                    <input type="url" class="form-control" name="raceUrl" placeholder="Race URL">
                </div>
                <div class="form-group">
                    <label for="result">Raw results</label>
                    <textarea class="form-control" name="result" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
    <div class="row">
        <?php echo $newString;?>
    </div>
</div>
</body>
</html>