module.exports = function (grunt) {

    // 1. All configuration goes here
    grunt.initConfig({
                         /**
                          * Get package meta data
                          */
                         pkg: grunt.file.readJSON('package.json'),

                         /**
                          * Set project object
                          */
                         project: {
                             app   : 'app',
                             public: 'public',
                             bower : '<%= project.public %>/bower_components',
                             scss  : '<%= project.app %>/scss',
                             js    : [
                                 '<%= project.app %>/js/*.js'
                             ]
                         },

                         ngmin: {
                             all: {
                                 expand: true,
                                 cwd   : 'app/js',
                                 src   : ['**/*.js'],
                                 dest  : 'app/js-ngmin'
                             }
                         },

                         /**
                          * browserify
                          */
                         browserify: {
                             generic                 : {
                                 src : 'app/js/genericapp.js', // A single entry point for our app
                                 dest: 'public/javascripts/genericbundle.js' // Compile to a single file to add a script tag for in your HTML
                             },
                             gallery                 : {
                                 src : 'app/js/galleryapp.js', // A single entry point for our app
                                 dest: 'public/javascripts/gallerybundle.js' // Compile to a single file to add a script tag for in your HTML
                             },
                             home                    : {
                                 src : 'app/js/homeapp.js',
                                 dest: 'public/javascripts/homeappbundle.js'
                             },
                             ticketshome             : {
                                 src : 'app/js/ticketshomeapp.js',
                                 dest: 'public/javascripts/ticketshomebundle.js'
                             },
                             ticketslistings         : {
                                 src : 'app/js/ticketslistingsapp.js',
                                 dest: 'public/javascripts/ticketslistingsbundle.js'
                             },
                             ticketsdetails          : {
                                 src : 'app/js/ticketsdetailsapp.js',
                                 dest: 'public/javascripts/ticketsdetailsbundle.js'
                             },
                             ticketssalessg          : {
                                 src : 'app/js/ticketssalessgapp.js',
                                 dest: 'public/javascripts/ticketssalessgbundle.js'
                             },
                             ticketssalesi11l        : {
                                 src : 'app/js/ticketssalesi11lapp.js',
                                 dest: 'public/javascripts/ticketssalesi11lbundle.js'
                             },
                             ticketscollection       : {
                                 src : 'app/js/ticketscollectionapp.js',
                                 dest: 'public/javascripts/ticketscollectionbundle.js'
                             },
//                             ticketshotelstravel: {
//                                 src : 'app/js/ticketshotelstravelapp.js',
//                                 dest: 'public/javascripts/ticketshotelstravelbundle.js'
//                             },
                             ticketshospitality      : {
                                 src : 'app/js/ticketshospitalityapp.js',
                                 dest: 'public/javascripts/ticketshospitalitybundle.js'
                             },
//                             ontrack            : {
//                                 src : 'app/js/ontrackapp.js',
//                                 dest: 'public/javascripts/ontrackbundle.js'
//                             },
                             ontrackTeamsDrivers     : {
                                 src : 'app/js/ontrackteamsdriversapp.js',
                                 dest: 'public/javascripts/ontrackteamsdriversbundle.js'
                             },
                             offtrackHeadliners      : {
                                 src : 'app/js/offtrackheadlinersapp.js',
                                 dest: 'public/javascripts/offtrackheadlinersbundle.js'
                             },
                             offtrackHeadlinerDetails: {
                                 src : 'app/js/offtrackheadlinerdetailsapp.js',
                                 dest: 'public/javascripts/offtrackheadlinerdetailsbundle.js'
                             },
                             mediaPressRelease       : {
                                 src : 'app/js/mediapressreleaseapp.js',
                                 dest: 'public/javascripts/mediapressreleasebundle.js'
                             },
                             mediaEntryForm          : {
                                 src : 'app/js/mediaentryformapp.js',
                                 dest: 'public/javascripts/mediaentryformbundle.js'
                             },
                             winContest              : {
                                 src : 'app/js/wincontestapp.js',
                                 dest: 'public/javascripts/wincontestbundle.js'
                             },
                             winChallenge            : {
                                 src : 'app/js/winchallengeapp.js',
                                 dest: 'public/javascripts/winchallengebundle.js'
                             },
                             miscNewsletter          : {
                                 src : 'app/js/miscnewsletterapp.js',
                                 dest: 'public/javascripts/miscnewsletterbundle.js'
                             },
                             revupformbundle         : {
                                src: 'app/js/revupformapp.js',
                                dest: 'public/javascripts/revupformbundle.js'
                             },
                             revupbundle         : {
                                 src: 'app/js/revupapp.js',
                                 dest: 'public/javascripts/revupbundle.js'
                             }
                         },

                         /**
                          * Sass
                          */
                         sass: {
                             dev : {
                                 options: {
                                     loadPath: '.',
                                     style   : 'expanded',
                                     noCache : true
                                 },
                                 files  : {
                                     'public/stylesheets/home.css'                       : '<%= project.scss %>/home.scss',
                                     'public/stylesheets/tickets-home.css'               : '<%= project.scss %>/tickets-home.scss',
                                     'public/stylesheets/tickets-listings.css'           : '<%= project.scss %>/tickets-listings.scss',
                                     'public/stylesheets/tickets-details.css'            : '<%= project.scss %>/tickets-details.scss',
                                     'public/stylesheets/tickets-sales-sg.css'           : '<%= project.scss %>/tickets-sales-sg.scss',
                                     'public/stylesheets/tickets-sales-international.css': '<%= project.scss %>/tickets-sales-international.scss',
                                     'public/stylesheets/tickets-hotelsandtravel.css'    : '<%= project.scss %>/tickets-hotelsandtravel.scss',
                                     'public/stylesheets/tickets-hospitality.css'        : '<%= project.scss %>/tickets-hospitality.scss',
                                     'public/stylesheets/tickets-selfcollection.css'     : '<%= project.scss %>/tickets-selfcollection.scss',
                                     'public/stylesheets/tickets-zones.css'              : '<%= project.scss %>/tickets-zones.scss',
                                     'public/stylesheets/ontrack-home.css'               : '<%= project.scss %>/ontrack-home.scss',
                                     'public/stylesheets/ontrack-racecalendar.css'       : '<%= project.scss %>/ontrack-racecalendar.scss',
                                     'public/stylesheets/ontrack-driversandteams.css'    : '<%= project.scss %>/ontrack-driversandteams.scss',
                                     'public/stylesheets/ontrack-teams.css'              : '<%= project.scss %>/ontrack-teams.scss',
                                     'public/stylesheets/ontrack-drivers.css'            : '<%= project.scss %>/ontrack-drivers.scss',
                                     'public/stylesheets/ontrack-circuitmap.css'         : '<%= project.scss %>/ontrack-circuitmap.scss',
                                     'public/stylesheets/ontrack-gallery.css'            : '<%= project.scss %>/ontrack-gallery.scss',
                                     'public/stylesheets/ontrack-highlights.css'         : '<%= project.scss %>/ontrack-highlights.scss',
                                     'public/stylesheets/offtrack-headliners.css'        : '<%= project.scss %>/offtrack-headliners.scss',
                                     'public/stylesheets/offtrack-pastheadliners.css'    : '<%= project.scss %>/offtrack-pastheadliners.scss',
                                     'public/stylesheets/offtrack-headlinerdetails.css'  : '<%= project.scss %>/offtrack-headlinerdetails.scss',
                                     'public/stylesheets/offtrack-gallery.css'           : '<%= project.scss %>/offtrack-gallery.scss',
                                     'public/stylesheets/win-home.css'                   : '<%= project.scss %>/win-home.scss',
                                     'public/stylesheets/win-challenge.css'              : '<%= project.scss %>/win-challenge.scss',
                                     'public/stylesheets/win-contest.css'                : '<%= project.scss %>/win-contest.scss',
                                     'public/stylesheets/win-wallpapers.css'             : '<%= project.scss %>/win-wallpapers.scss',
                                     'public/stylesheets/media-pressrelease.css'         : '<%= project.scss %>/media-pressrelease.scss',
                                     'public/stylesheets/media-pressreleasedetails.css'  : '<%= project.scss %>/media-pressreleasedetails.scss',
                                     'public/stylesheets/media-chooseentryform.css'      : '<%= project.scss %>/media-chooseentryform.scss',
                                     'public/stylesheets/media-entryform.css'            : '<%= project.scss %>/media-entryform.scss',
                                     'public/stylesheets/media-mobileapp.css'            : '<%= project.scss %>/media-mobileapp.scss',
                                     'public/stylesheets/misc-privacypolicy.css'         : '<%= project.scss %>/misc-privacypolicy.scss',
                                     'public/stylesheets/misc-newsletter.css'            : '<%= project.scss %>/misc-newsletter.scss',
                                     'public/stylesheets/misc-contactus.css'             : '<%= project.scss %>/misc-contactus.scss',
                                     'public/stylesheets/revup.css'                      : '<%= project.scss %>/revup.scss',
                                     'public/stylesheets/maintenance.css'                : '<%= project.scss %>/maintenance.scss',
                                     'public/stylesheets/raceguide.css'                  : '<%= project.scss %>/raceguide.scss',
                                     'public/stylesheets/raceguide-activities.css'       : '<%= project.scss %>/raceguide-activities.scss'
                                 }
                             },
                             dist: {
                                 options: {
                                     loadPath: '.',
                                     style   : 'compressed'
                                 },
                                 files  : {
                                     'public/stylesheets/home.css'                       : '<%= project.scss %>/home.scss',
                                     'public/stylesheets/tickets-home.css'               : '<%= project.scss %>/tickets-home.scss',
                                     'public/stylesheets/tickets-listings.css'           : '<%= project.scss %>/tickets-listings.scss',
                                     'public/stylesheets/tickets-details.css'            : '<%= project.scss %>/tickets-details.scss',
                                     'public/stylesheets/tickets-sales-sg.css'           : '<%= project.scss %>/tickets-sales-sg.scss',
                                     'public/stylesheets/tickets-sales-international.css': '<%= project.scss %>/tickets-sales-international.scss',
                                     'public/stylesheets/tickets-hotelsandtravel.css'    : '<%= project.scss %>/tickets-hotelsandtravel.scss',
                                     'public/stylesheets/tickets-hospitality.css'        : '<%= project.scss %>/tickets-hospitality.scss',
                                     'public/stylesheets/tickets-selfcollection.css'     : '<%= project.scss %>/tickets-selfcollection.scss',
                                     'public/stylesheets/tickets-zones.css'              : '<%= project.scss %>/tickets-zones.scss',
                                     'public/stylesheets/ontrack-home.css'               : '<%= project.scss %>/ontrack-home.scss',
                                     'public/stylesheets/ontrack-racecalendar.css'       : '<%= project.scss %>/ontrack-racecalendar.scss',
                                     'public/stylesheets/ontrack-driversandteams.css'    : '<%= project.scss %>/ontrack-driversandteams.scss',
                                     'public/stylesheets/ontrack-teams.css'              : '<%= project.scss %>/ontrack-teams.scss',
                                     'public/stylesheets/ontrack-drivers.css'            : '<%= project.scss %>/ontrack-drivers.scss',
                                     'public/stylesheets/ontrack-circuitmap.css'         : '<%= project.scss %>/ontrack-circuitmap.scss',
                                     'public/stylesheets/ontrack-gallery.css'            : '<%= project.scss %>/ontrack-gallery.scss',
                                     'public/stylesheets/ontrack-highlights.css'         : '<%= project.scss %>/ontrack-highlights.scss',
                                     'public/stylesheets/offtrack-headliners.css'        : '<%= project.scss %>/offtrack-headliners.scss',
                                     'public/stylesheets/offtrack-pastheadliners.css'    : '<%= project.scss %>/offtrack-pastheadliners.scss',
                                     'public/stylesheets/offtrack-headlinerdetails.css'  : '<%= project.scss %>/offtrack-headlinerdetails.scss',
                                     'public/stylesheets/offtrack-gallery.css'           : '<%= project.scss %>/offtrack-gallery.scss',
                                     'public/stylesheets/win-home.css'                   : '<%= project.scss %>/win-home.scss',
                                     'public/stylesheets/win-challenge.css'              : '<%= project.scss %>/win-challenge.scss',
                                     'public/stylesheets/win-contest.css'                : '<%= project.scss %>/win-contest.scss',
                                     'public/stylesheets/win-wallpapers.css'             : '<%= project.scss %>/win-wallpapers.scss',
                                     'public/stylesheets/media-pressrelease.css'         : '<%= project.scss %>/media-pressrelease.scss',
                                     'public/stylesheets/media-pressreleasedetails.css'  : '<%= project.scss %>/media-pressreleasedetails.scss',
                                     'public/stylesheets/media-chooseentryform.css'      : '<%= project.scss %>/media-chooseentryform.scss',
                                     'public/stylesheets/media-entryform.css'            : '<%= project.scss %>/media-entryform.scss',
                                     'public/stylesheets/media-mobileapp.css'            : '<%= project.scss %>/media-mobileapp.scss',
                                     'public/stylesheets/misc-privacypolicy.css'         : '<%= project.scss %>/misc-privacypolicy.scss',
                                     'public/stylesheets/misc-newsletter.css'            : '<%= project.scss %>/misc-newsletter.scss',
                                     'public/stylesheets/misc-contactus.css'             : '<%= project.scss %>/misc-contactus.scss',
                                     'public/stylesheets/revup.css'                      : '<%= project.scss %>/revup.scss',
                                     'public/stylesheets/maintenance.css'                : '<%= project.scss %>/maintenance.scss',
                                     'public/stylesheets/raceguide.css'                  : '<%= project.scss %>/raceguide.scss',
                                     'public/stylesheets/raceguide-activities.css'       : '<%= project.scss %>/raceguide-activities.scss'
                                 }
                             }
                         },

                         /**
                          * AutoPrefixer
                          * CSS vendor properties prefixer
                          * https://github.com/nDmitry/grunt-autoprefixer
                          */
                         autoprefixer: {
                             options: {
                                 // Task-specific options go here.
                             },
                             all    : {
                                 files: {
                                     'public/stylesheets/home.css'                       : 'public/stylesheets/home.css',
                                     'public/stylesheets/tickets-home.css'               : 'public/stylesheets/tickets-home.css',
                                     'public/stylesheets/tickets-listings.css'           : 'public/stylesheets/tickets-listings.css',
                                     'public/stylesheets/tickets-details.css'            : 'public/stylesheets/tickets-details.css',
                                     'public/stylesheets/tickets-sales-sg.css'           : 'public/stylesheets/tickets-sales-sg.css',
                                     'public/stylesheets/tickets-sales-international.css': 'public/stylesheets/tickets-sales-international.css',
                                     'public/stylesheets/tickets-hotelsandtravel.css'    : 'public/stylesheets/tickets-hotelsandtravel.css',
                                     'public/stylesheets/tickets-hospitality.css'        : 'public/stylesheets/tickets-hospitality.css',
                                     'public/stylesheets/tickets-selfcollection.css'     : 'public/stylesheets/tickets-selfcollection.css',
                                     'public/stylesheets/tickets-zones.css'              : 'public/stylesheets/tickets-zones.css',
                                     'public/stylesheets/ontrack-home.css'               : 'public/stylesheets/ontrack-home.css',
                                     'public/stylesheets/ontrack-racecalendar.css'       : 'public/stylesheets/ontrack-racecalendar.css',
                                     'public/stylesheets/ontrack-driversandteams.css'    : 'public/stylesheets/ontrack-driversandteams.css',
                                     'public/stylesheets/ontrack-teams.css'              : 'public/stylesheets/ontrack-teams.css',
                                     'public/stylesheets/ontrack-drivers.css'            : 'public/stylesheets/ontrack-drivers.css',
                                     'public/stylesheets/ontrack-circuitmap.css'         : 'public/stylesheets/ontrack-circuitmap.css',
                                     'public/stylesheets/ontrack-gallery.css'            : 'public/stylesheets/ontrack-gallery.css',
                                     'public/stylesheets/ontrack-highlights.css'         : 'public/stylesheets/ontrack-highlights.css',
                                     'public/stylesheets/offtrack-headliners.css'        : 'public/stylesheets/offtrack-headliners.css',
                                     'public/stylesheets/offtrack-pastheadliners.css'    : 'public/stylesheets/offtrack-pastheadliners.css',
                                     'public/stylesheets/offtrack-headlinerdetails.css'  : 'public/stylesheets/offtrack-headlinerdetails.css',
                                     'public/stylesheets/offtrack-gallery.css'           : 'public/stylesheets/offtrack-gallery.css',
                                     'public/stylesheets/win-home.css'                   : 'public/stylesheets/win-home.css',
                                     'public/stylesheets/win-challenge.css'              : 'public/stylesheets/win-challenge.css',
                                     'public/stylesheets/win-contest.css'                : 'public/stylesheets/win-contest.css',
                                     'public/stylesheets/win-wallpapers.css'             : 'public/stylesheets/win-wallpapers.css',
                                     'public/stylesheets/media-pressrelease.css'         : 'public/stylesheets/media-pressrelease.css',
                                     'public/stylesheets/media-pressreleasedetails.css'  : 'public/stylesheets/media-pressreleasedetails.css',
                                     'public/stylesheets/media-chooseentryform.css'      : 'public/stylesheets/media-chooseentryform.css',
                                     'public/stylesheets/media-entryform.css'            : 'public/stylesheets/media-entryform.css',
                                     'public/stylesheets/media-mobileapp.css'            : 'public/stylesheets/media-mobileapp.css',
                                     'public/stylesheets/misc-privacypolicy.css'         : 'public/stylesheets/misc-privacypolicy.css',
                                     'public/stylesheets/misc-newsletter.css'            : 'public/stylesheets/misc-newsletter.css',
                                     'public/stylesheets/misc-contactus.css'             : 'public/stylesheets/misc-contactus.css',
                                     'public/stylesheets/revup.css'                      : 'public/stylesheets/revup.css',
                                     'public/stylesheets/maintenance.css'                : 'public/stylesheets/maintenance.css',
                                     'public/stylesheets/raceguide.css'                  : 'public/stylesheets/raceguide.css',
                                     'public/stylesheets/raceguide-activities.css'       : 'public/stylesheets/raceguide-activities.css'
                                 }
                             }
                         },

                         /**
                          * CSSMin
                          * CSS minification
                          * https://github.com/gruntjs/grunt-contrib-cssmin
                          */
                         cssmin: {
                             dev : {
                                 files: {
                                     'public/stylesheets/home.min.css'                       : ['public/stylesheets/home.css'],
                                     'public/stylesheets/tickets-home.min.css'               : ['public/stylesheets/tickets-home.css'],
                                     'public/stylesheets/tickets-listings.min.css'           : ['public/stylesheets/tickets-listings.css'],
                                     'public/stylesheets/tickets-details.min.css'            : ['public/stylesheets/tickets-details.css'],
                                     'public/stylesheets/tickets-sales-sg.min.css'           : ['public/stylesheets/tickets-sales-sg.css'],
                                     'public/stylesheets/tickets-sales-international.min.css': ['public/stylesheets/tickets-sales-international.css'],
                                     'public/stylesheets/tickets-hotelsandtravel.min.css'    : ['public/stylesheets/tickets-hotelsandtravel.css'],
                                     'public/stylesheets/tickets-hospitality.min.css'        : ['public/stylesheets/tickets-hospitality.css'],
                                     'public/stylesheets/tickets-selfcollection.min.css'     : ['public/stylesheets/tickets-selfcollection.css'],
                                     'public/stylesheets/tickets-zones.min.css'              : ['public/stylesheets/tickets-zones.css'],
                                     'public/stylesheets/ontrack-home.min.css'               : ['public/stylesheets/ontrack-home.css'],
                                     'public/stylesheets/ontrack-racecalendar.min.css'       : ['public/stylesheets/ontrack-racecalendar.css'],
                                     'public/stylesheets/ontrack-driversandteams.min.css'    : ['public/stylesheets/ontrack-driversandteams.css'],
                                     'public/stylesheets/ontrack-teams.min.css'              : ['public/stylesheets/ontrack-teams.css'],
                                     'public/stylesheets/ontrack-drivers.min.css'            : ['public/stylesheets/ontrack-drivers.css'],
                                     'public/stylesheets/ontrack-circuitmap.min.css'         : ['public/stylesheets/ontrack-circuitmap.css'],
                                     'public/stylesheets/ontrack-gallery.min.css'            : ['public/stylesheets/ontrack-gallery.css'],
                                     'public/stylesheets/ontrack-highlights.min.css'         : ['public/stylesheets/ontrack-highlights.css'],
                                     'public/stylesheets/offtrack-headliners.min.css'        : ['public/stylesheets/offtrack-headliners.css'],
                                     'public/stylesheets/offtrack-pastheadliners.min.css'    : ['public/stylesheets/offtrack-pastheadliners.css'],
                                     'public/stylesheets/offtrack-headlinerdetails.min.css'  : ['public/stylesheets/offtrack-headlinerdetails.css'],
                                     'public/stylesheets/offtrack-gallery.min.css'           : ['public/stylesheets/offtrack-gallery.css'],
                                     'public/stylesheets/win-home.min.css'                   : ['public/stylesheets/win-home.css'],
                                     'public/stylesheets/win-challenge.min.css'              : ['public/stylesheets/win-challenge.css'],
                                     'public/stylesheets/win-contest.min.css'                : ['public/stylesheets/win-contest.css'],
                                     'public/stylesheets/win-wallpapers.min.css'             : ['public/stylesheets/win-wallpapers.css'],
                                     'public/stylesheets/media-pressrelease.min.css'         : ['public/stylesheets/media-pressrelease.css'],
                                     'public/stylesheets/media-pressreleasedetails.min.css'  : ['public/stylesheets/media-pressreleasedetails.css'],
                                     'public/stylesheets/media-chooseentryform.min.css'      : ['public/stylesheets/media-chooseentryform.css'],
                                     'public/stylesheets/media-entryform.min.css'            : ['public/stylesheets/media-entryform.css'],
                                     'public/stylesheets/media-mobileapp.min.css'            : ['public/stylesheets/media-mobileapp.css'],
                                     'public/stylesheets/misc-privacypolicy.min.css'         : ['public/stylesheets/misc-privacypolicy.css'],
                                     'public/stylesheets/misc-newsletter.min.css'            : ['public/stylesheets/misc-newsletter.css'],
                                     'public/stylesheets/misc-contactus.min.css'             : ['public/stylesheets/misc-contactus.css'],
                                     'public/stylesheets/revup.min.css'                      : ['public/stylesheets/revup.css'],
                                     'public/stylesheets/maintenance.min.css'                : ['public/stylesheets/maintenance.css'],
                                     'public/stylesheets/raceguide.min.css'                  : ['public/stylesheets/raceguide.css'],
                                     'public/stylesheets/raceguide-activities.min.css'       : ['public/stylesheets/raceguide-activities.css']
                                 }
                             },
                             dist: {
                                 options: {
                                     banner: '/* SGP Site Minified CSS */'
                                 },
                                 files  : {
                                     'public/stylesheets/home.min.css'                       : ['public/stylesheets/home.css'],
                                     'public/stylesheets/tickets-home.min.css'               : ['public/stylesheets/tickets-home.css'],
                                     'public/stylesheets/tickets-listings.min.css'           : ['public/stylesheets/tickets-listings.css'],
                                     'public/stylesheets/tickets-details.min.css'            : ['public/stylesheets/tickets-details.css'],
                                     'public/stylesheets/tickets-sales-sg.min.css'           : ['public/stylesheets/tickets-sales-sg.css'],
                                     'public/stylesheets/tickets-sales-international.min.css': ['public/stylesheets/tickets-sales-international.css'],
                                     'public/stylesheets/tickets-hotelsandtravel.min.css'    : ['public/stylesheets/tickets-hotelsandtravel.css'],
                                     'public/stylesheets/tickets-hospitality.min.css'        : ['public/stylesheets/tickets-hospitality.css'],
                                     'public/stylesheets/tickets-selfcollection.min.css'     : ['public/stylesheets/tickets-selfcollection.css'],
                                     'public/stylesheets/tickets-zones.min.css'              : ['public/stylesheets/tickets-zones.css'],
                                     'public/stylesheets/ontrack-home.min.css'               : ['public/stylesheets/ontrack-home.css'],
                                     'public/stylesheets/ontrack-racecalendar.min.css'       : ['public/stylesheets/ontrack-racecalendar.css'],
                                     'public/stylesheets/ontrack-driversandteams.min.css'    : ['public/stylesheets/ontrack-driversandteams.css'],
                                     'public/stylesheets/ontrack-teams.min.css'              : ['public/stylesheets/ontrack-teams.css'],
                                     'public/stylesheets/ontrack-drivers.min.css'            : ['public/stylesheets/ontrack-drivers.css'],
                                     'public/stylesheets/ontrack-circuitmap.min.css'         : ['public/stylesheets/ontrack-circuitmap.css'],
                                     'public/stylesheets/ontrack-gallery.min.css'            : ['public/stylesheets/ontrack-gallery.css'],
                                     'public/stylesheets/ontrack-highlights.min.css'         : ['public/stylesheets/ontrack-highlights.css'],
                                     'public/stylesheets/offtrack-headliners.min.css'        : ['public/stylesheets/offtrack-headliners.css'],
                                     'public/stylesheets/offtrack-pastheadliners.min.css'    : ['public/stylesheets/offtrack-pastheadliners.css'],
                                     'public/stylesheets/offtrack-headlinerdetails.min.css'  : ['public/stylesheets/offtrack-headlinerdetails.css'],
                                     'public/stylesheets/offtrack-gallery.min.css'           : ['public/stylesheets/offtrack-gallery.css'],
                                     'public/stylesheets/win-home.min.css'                   : ['public/stylesheets/win-home.css'],
                                     'public/stylesheets/win-challenge.min.css'              : ['public/stylesheets/win-challenge.css'],
                                     'public/stylesheets/win-contest.min.css'                : ['public/stylesheets/win-contest.css'],
                                     'public/stylesheets/win-wallpapers.min.css'             : ['public/stylesheets/win-wallpapers.css'],
                                     'public/stylesheets/media-pressrelease.min.css'         : ['public/stylesheets/media-pressrelease.css'],
                                     'public/stylesheets/media-pressreleasedetails.min.css'  : ['public/stylesheets/media-pressreleasedetails.css'],
                                     'public/stylesheets/media-chooseentryform.min.css'      : ['public/stylesheets/media-chooseentryform.css'],
                                     'public/stylesheets/media-entryform.min.css'            : ['public/stylesheets/media-entryform.css'],
                                     'public/stylesheets/media-mobileapp.min.css'            : ['public/stylesheets/media-mobileapp.css'],
                                     'public/stylesheets/misc-privacypolicy.min.css'         : ['public/stylesheets/misc-privacypolicy.css'],
                                     'public/stylesheets/misc-newsletter.min.css'            : ['public/stylesheets/misc-newsletter.css'],
                                     'public/stylesheets/misc-contactus.min.css'             : ['public/stylesheets/misc-contactus.css'],
                                     'public/stylesheets/revup.min.css'                      : ['public/stylesheets/revup.css'],
                                     'public/stylesheets/maintenance.min.css'                : ['public/stylesheets/maintenance.css'],
                                     'public/stylesheets/raceguide.min.css'                  : ['public/stylesheets/raceguide.css'],
                                     'public/stylesheets/raceguide-activities.min.css'       : ['public/stylesheets/raceguide-activities.css']
                                 }
                             }
                         },

                         /**
                          * Concat
                          */
                         concat: {
                             // Configuration for concatinating files goes here.
                         },

                         /**
                          * ImageMin
                          * Minify PNG and JPEG images.
                          * https://github.com/gruntjs/grunt-contrib-imagemin
                          */
                         imagemin: {
                             options: { cache: false },
                             all    : {
                                 files: [
                                     {
                                         expand: true,                                      // Enable dynamic expansion
                                         cwd   : '<%= project.app %>/unoptimised_images/',  // Src matches are relative to this path
                                         src   : ['**/*.{png,jpg,gif}'],                    // Actual patterns to match
                                         dest  : '<%= project.public %>/images/'            // Destination path prefix
                                     }
                                 ]
                             }
                         },

                         /**
                          * Copy
                          * Copy files and folders.
                          * https://github.com/gruntjs/grunt-contrib-copy
                          */
                         copy: {
                             imagemin: {
                                 files: [
                                     {
                                         expand: true,
                                         cwd   : '<%= project.app %>/unoptimised_images/',
                                         src   : ['**/*.{png,jpg,gif}'],
                                         dest  : '<%= project.public %>/images/'
                                     }
                                 ]
                             }
                         },

                         /**
                          * Clean
                          * Clear files and folders.
                          * https://github.com/gruntjs/grunt-contrib-clean
                          */
                         clean: {
                             sasscache: {
                                 src: ['.sass-cache/']
                             },
                             images   : {
                                 src: ['public/images/']
                             }
                         },

                         /**
                          * Watch
                          */
                         watch: {
                             browserify: {
                                 files: '<%= project.app %>/js/{,**/}*.{js,html}',
                                 tasks: ['browserify']
                             },
                             sass      : {
                                 files: '<%= project.app %>/scss/{,**/}*.{scss,sass}',
                                 tasks: ['sass:dev', 'autoprefixer:all', 'cssmin:dev']
                             }
                         }

                     });

    /**
     * Load Grunt plugins
     */
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    /**
     * Default task
     * Run `grunt` on the command line
     */
    grunt.registerTask('default', [
        'browserify',
        'sass:dev',
        'autoprefixer:all',
        'cssmin:dev',
        'watch'
    ]);

    /**
     * Build task
     * Run `grunt build` on the command line
     * Then compress all JS/CSS files
     */
    grunt.registerTask('build', [
        'sass:dist'
    ]);

    grunt.registerTask('images', [
        'clean:images',
        'imagemin'
    ]);
};